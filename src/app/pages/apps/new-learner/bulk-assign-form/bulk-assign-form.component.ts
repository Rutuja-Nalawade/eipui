import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { Allocation } from './allocation';
import { Bulkbearch } from './bulkbearch';
import { ModalDismissReasons, NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DomSanitizer } from '@angular/platform-browser';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';

@Component({
  selector: 'app-bulk-assign-form',
  templateUrl: './bulk-assign-form.component.html',
  styleUrls: ['./bulk-assign-form.component.scss']
})
export class BulkAssignFormComponent implements OnInit {
  @ViewChild('assignMore', { static: true }) assignMore: ElementRef;
  showAssignDetails: boolean = false;
  studentList: {
    studentId: number
    selectedName: string;
    selected: boolean;
    imageCode: string;
    color: string;
  }[] = [];
  tempStudents: {
    studentId: number
    selectedName: string;
  }[] = [];
  remove: {
    studentId: number
    selectedName: string;
    selected: boolean;
    color: string;
  }[] = [];
  orgId = parseInt(sessionStorage.getItem("organizationId"));
  pageIndex: number = 0;
  pageSize: number = 100;
  pageIndexError: number = 0;
  colorClassArrayHashColor = ['#FFBC4B', '#0086FF', '#0091FF', '#F15223', '#59B284'];
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  model = new Allocation();
  bulksearch = new Bulkbearch();
  AllowcatedAcademicYearId: any;
  AllowcatedCalenderId: any;
  AllowcatedcourseId: any;
  AllowcatedBatchId: any;
  AllowcatedBoardId: any;
  AllowcatedGradeId: any;
  AllowcatedGroupId: any;
  mouseOverContainer: any;
  multiselectionEnable: boolean = false;
  AlwacademicYear = [];
  AlwacademicCalender = [];
  AlwcourseList = [];
  AlwbatchList = [];
  AlwboardList = [];
  AlwgradeList = [];
  AlwgroupList = [];
  maxLength: number = 4

  academicYear = [];
  academicCalender = [];
  courseList = [];
  batchList = [];
  boardList = [];
  gradeList = [];
  groupList = [];
  masterSelected: boolean;
  selectedStudents: any = [];
  closeResult: string;
  SelectedAcademicYearId: any;
  SelectedAcademicCalenderId: any;
  SelectedcourseId: any;
  SelectedBatchId: any;
  SelectedBoardId: any;
  SelectedGradeId: any;
  SelectedGroupId: any;
  totalPages = 0;

  currentUserId: Number = null;
  loaderBasicInfo = false
  loading = false;
  academicFormGroup: FormGroup;
  Gender: string = '';
  Performance: any = [];
  Language: any = [];
  Vocational: any = [];
  studentAcademicInfo: any;
  orgBatchList: any;
  academicCalendarList: any;
  orgCourseList = [];
  complsorySubjects = [];
  optionalSubject = [];
  electiveSubject = [];
  selectedOptionalSubject = [];
  getbatchHasGroupList: any;
  isCheckTrue = false;
  orgBoardList = [];
  orgGradeList = [];
  orgBatchGroupList = [];
  orgGroupCategoreyList = [];
  academicYearList = [];
  selectedGroupCategory = [];
  selectedBatchGroup = [];
  submitted = false;
  selectedCourseUniqueId = "";
  isSelectElectiveSub = true;
  selectedElectiveSub = [];
  selectedOptioanlSub = [];
  electiveSubCount = 0;
  optioanlSubCount = 0;
  isDropDownOpen: boolean = false;
  modalReference: NgbModalRef;
  ngbSmModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false,
    centered: true,
    windowClass: 'custom-class'
  };
  oSubject = [];
  selectedCourseName: any;
  selectedBatchName: any;
  selectedBoardName: any;
  selectedGradeName: any;
  selectedGroupName: any;
  isShowQuickForm: Boolean = false;

  @Output() closebulkassignform = new EventEmitter<string>();
  constructor(private spinner: NgxSpinnerService,
    private toastService: ToastsupportService,
    private _courseService: CourseServiceService,
    private boardService: BoardServiceService,
    private batchService: BatchesServiceService,
    private gradeService: GradeServiceService,
    private modalService: NgbModal,
    private _userService: UserServiceService,
    private _DomSanitizationService: DomSanitizer,
    private formBuilder: FormBuilder,
    private _batchService: BatchesServiceService,
    private _boardService: BoardServiceService,
    private _gradeService: GradeServiceService,
    private _subjectService: SubjectServiceService
  ) { }

  ngOnInit() {
    this.initStudentBasicInfoForm();
    this.getCourseListByAcademicCalendar("");
    this.getCourseList();
    this.getUnAsignedStudents()
  }

  getUnAsignedStudents() {
    this.spinner.show()
    //this.studentList = []
    let searchDto = {
      "courseIds": [],
      "groupCategoriesIds": [],
      "batchGroupIds": [],
      "batchIds": [],
      "gradeIds": [],
      "boardIds": [],
      "subjectIds": [],
      "studentUniqueIds": [],
      "page": this.pageIndex,
      "size": this.pageSize,
      "unassigned": true,
      "states": [1, 2],
      "responseFields": {
        "courseField": true,
        "batchFiled": true,
        "boardField": true,
        "gradeField": true,
        "subjectField": true,
        "groupCategoryField": true,
        "batchGroupFiled": false
      }
    };
    this._userService.getUsers(searchDto, "students").subscribe(
      (res) => {
        if (res) {
          // this.studentList = res;
          this.totalPages = res.totalPages;
          if (res.students && res.students.length > 0) {
            // this.studentList = [];
            res.students.forEach((element, key) => {
              let dto = {
                studentId: element.student.uniqueId,
                selectedName: element.student.name,
                selected: false,
                imageCode: "",
                color: '#000000',
              };
              this.studentList.push(dto);
              // this.getImage(element.student.uniqueId, key)
              // console.log("res", res)
              // console.log("this.studentList", this.studentList)
            });
          }
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   // console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  checkUncheckAll() {
    // console.log("")
    for (var i = 0; i < this.studentList.length; i++) {
      this.studentList[i].selected = this.masterSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.masterSelected = this.studentList.every(function (item: any) {
      return item.selected == true;
    })
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.selectedStudents = [];
    for (var i = 0; i < this.studentList.length; i++) {
      if (this.studentList[i].selected)
        this.selectedStudents.push(this.studentList[i]);
    }
    this.selectedStudents = this.selectedStudents;
    // console.log("this.selectedStudents", this.selectedStudents)
  }

  getCourseList() {
    this._courseService.getCourseDetailsedInfoList(3).subscribe(
      res => {
        if (res.success == true) {
          // console.log("get courseList => ",res)
          let AlwcourseList = res.list;
          if (AlwcourseList.length > 0) {
            this.AlwcourseList = AlwcourseList;
          }
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }

  onStudentsScroll() {
    // this.spinner.show();
    // console.log('Student scroll end and called');
    if (!(this.totalPages == (this.pageIndex + 1))) {
      this.pageIndex = this.pageIndex + 1;
      this.getUnAsignedStudents();
    }
  }

  onCourseChange(courseid) {
    let ids = [courseid]
    this.boardService.getBoardByCourseUid(ids).subscribe(
      (res) => {
        if (res) {

          let boardList = res;
          if (boardList != null && boardList.length > 0) {
            let boardData = [];
            for (let i = 0; i < boardList.length; i++) {
              const element = boardList[i];
              if (element.boards && element.boards.length > 0) {
                boardData = element.boards;
              }
            }
            this.boardList = boardData;
          }
          this.SelectedGroupId = null;
          this.SelectedBatchId = null;
          this.SelectedBoardId = null;
          this.SelectedGradeId = null;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   // console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });


    // to call batch by course
    this.batchService.getBatchByCourse(courseid).subscribe(
      (res) => {
        if (res.success == true) {
          this.batchList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }

      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }

  onBatchChange(BatchId) {

    // console.log("BatchId"+BatchId);
    //to get group by batch
    this.batchService.getBatchGroupList(BatchId).subscribe(
      (res) => {
        if (res) {

          this.groupList = res[0].batchGroup;
          // console.log("group list", this.groupList);
          this.SelectedGroupId = null;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log('Client-side error occured.');
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }

  onBoardChange(BoardId) {
    // call function to get grades
    let ids = [BoardId]
    this.gradeService.getAcademicGradeListByBoard(ids).subscribe(
      (res) => {
        if (res.success == true) {
          for (let i = 0; i < res.list[0].courseList.length; i++) {
            const element = res.list[0].courseList[i];
            // if (this.selectedCourseUniqueId == element.courseDto.uniqueId) {
            // gradeList=element.grades
            this.gradeList = element.grades;
            // }
          }
          // this.gradeList = res;
          this.SelectedGradeId = null;
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }

  serachStudent() {
    this.spinner.show();
    this.bulksearch.selectedcourseId = this.SelectedcourseId;
    this.bulksearch.selectedBoardId = this.SelectedBoardId;
    this.bulksearch.selectedBatchId = this.SelectedBatchId;
    this.bulksearch.selectedGradeId = this.SelectedGradeId;
    this.bulksearch.selectedGroupId = this.SelectedGroupId;
    this.bulksearch.academicYearId = this.SelectedAcademicYearId;
    this.bulksearch.academicCalenderId = this.SelectedAcademicCalenderId;
    this.bulksearch.orgId = sessionStorage.getItem('orgUniqueId');
    this.bulksearch.pageIndex = this.pageIndex;

    let searchDto = {
      "courseIds": [this.SelectedcourseId],
      "groupCategoriesIds": [],
      "batchGroupIds": [],
      "batchIds": [this.SelectedBatchId],
      "gradeIds": [this.SelectedGradeId],
      "boardIds": [this.SelectedBoardId],
      "subjectIds": [],
      "studentUniqueIds": [],
      "page": this.pageIndex,
      "size": this.pageSize,
      "unassigned": true,
      "states": [1, 2],
      "responseFields": {
        "courseField": false,
        "batchFiled": false,
        "boardField": false,
        "gradeField": false,
        "subjectField": false,
        "groupCategoryField": false,
        "batchGroupFiled": false
      }
    };
    this._userService.getUsers(searchDto, "students").subscribe(
      (res) => {
        if (res) {
          // this.studentList = res;
          this.totalPages = res.totalPages;
          if (res.students && res.students.length > 0) {
            // this.studentList = [];
            res.students.forEach((element, key) => {
              let dto = {
                studentId: element.student.uniqueId,
                selectedName: element.student.name,
                selected: false,
                imageCode: "",
                color: '#ffffff',
              };
              this.studentList.push(dto);
              this.getImage(element.student.uniqueId, key)
              // console.log("res", res)
              // console.log("this.studentList", this.studentList)
            });
          }
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   // console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });

  }

  clearSearch() {
    this.SelectedAcademicYearId = null;
    this.SelectedAcademicCalenderId = null;
    this.SelectedGroupId = null;
    this.SelectedcourseId = null;
    this.SelectedBatchId = null;
    this.SelectedBoardId = null;
    this.SelectedGradeId = null;
  }
  onAcademicCalenderChange() {

    this._courseService.getCourseDetailsedInfoList(3).subscribe(
      (res) => {
        if (res) {
          if (res.success == true) {
            // console.log("get courseList => ",res)
            this.courseList = res.list;
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
          // console.log("Course List", this.courseList);

        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log('Client-side error occured.');
        // } else {

        //   this.toastService.ShowError(error);
        // }
      });

  }
  onAlwCourseChange(courseid) {
    // console.log("courseid"+courseid);

    //to call board by cours
    let ids = [courseid]
    this.boardService.getBoardByCourseUid(ids).subscribe(
      (res) => {
        if (res) {
          let orgBoardList = res;
          if (orgBoardList != null && orgBoardList.length > 0) {
            let boardList = [];
            for (let i = 0; i < orgBoardList.length; i++) {
              const element = orgBoardList[i];
              if (element.boards && element.boards.length > 0) {
                boardList = element.boards;
              }
            }
            this.AlwboardList = boardList;
          }
          // this.AlwboardList = res;
          console.log("Board list", res);

        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        //   this.toastService.ShowError(error);
        // }
      });


    // to call batch by course
    this.batchService.getBatchByCourse(courseid).subscribe(
      (res) => {
        if (res.success == true) {
          this.AlwbatchList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        //   this.toastService.ShowError(error);
        // }
      });

  }

  onAlwBoardChange(BoardId) {

    // console.log("BoardId" + BoardId);

    //call function to get grades
    let ids = [BoardId]
    this.gradeService.getAcademicGradeListByBoard(ids).subscribe(
      (res) => {
        if (res.success == true) {
          let gradeList = [];
          for (let i = 0; i < res.list[0].courseList.length; i++) {
            const element = res.list[0].courseList[i];

            // gradeList=element.grades
            // this.orgGradeList = element.grades;
            if (element.grades && element.grades.length > 0) {

              gradeList = (gradeList.length == 0) ? element.grades : gradeList.concat(element.grades);
            }
          }
          // this.orgGradeList = gradeList;
          this.AlwgradeList = gradeList;
          // this.AlwgradeList = res;

          // console.log("grade list", this.gradeList);

        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        //   this.toastService.ShowError(error);
        // }
      });

  }

  onAlwBatchChange(BatchId) {

    // console.log("BatchId"+BatchId);
    //to get group by batch

    this.batchService.getBatchGroupList(BatchId).subscribe(
      (res) => {
        if (res && res.length > 0) {
          this.AlwgroupList = res[0].batchGroup;

          // console.log("group list", this.groupList);

        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        //   this.toastService.ShowError(error);
        // }
      });

  }

  allocatedetails() {
    if (this.studentList.length == 0) {
      this.toastService.ShowWarning("Students not found for assign!");
      return;
    } else if (this.selectedStudents.length == 0) {
      this.toastService.ShowWarning("Please select atleast one student!");
      return;
    } else if (!this.AllowcatedAcademicYearId && !this.AllowcatedCalenderId && !this.AllowcatedcourseId && !this.AllowcatedBoardId && !this.AllowcatedBatchId && !this.AllowcatedGroupId && !this.AllowcatedGradeId) {
      this.toastService.ShowWarning("Please select atleast one option for assign!");
      return;
    }
    this.selectedStudents.forEach(element => {
      let dto = {
        studentId: element.studentId,
        selectedName: element.selectedName
      }

      this.tempStudents.push(dto);
      // console.log('studentList' + JSON.stringify(this.tempStudents))
    });
    this.spinner.show();
    this.model.selectedAcademicYearId = (this.AllowcatedAcademicYearId) ? this.AllowcatedAcademicYearId : 0;
    this.model.selectedAcademicCalenderId = (this.AllowcatedCalenderId) ? this.AllowcatedCalenderId : 0;
    this.model.selectedcourseId = (this.AllowcatedcourseId) ? this.AllowcatedcourseId : 0;
    this.model.selectedBoardId = (this.AllowcatedBoardId) ? this.AllowcatedBoardId : 0;
    this.model.selectedBatchId = (this.AllowcatedBatchId) ? this.AllowcatedBatchId : 0;
    this.model.selectedGradeId = (this.AllowcatedGradeId) ? this.AllowcatedGradeId : 0;
    this.model.selectedGroupId = (this.AllowcatedGroupId) ? this.AllowcatedGroupId : 0;
    this.model.studentList = this.tempStudents;
    this._userService.allocateDetails(this.model).subscribe(
      (res) => {
        if (res) {

          // console.log("Student Save Response", res);
          this.toastService.showSuccess("Allocation details saved successfully");
          this.selectedStudents = [];
          this.tempStudents = [];


        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured." + error);
        // } else {
        //   this.spinner.hide();

        //   this.toastService.ShowError(error);
        //   // this.selectedStudents = [];
        //   this.tempStudents = [];
        // }
        this.spinner.hide();
        this.tempStudents = [];
      });

  }

  opensearchmodal(content) {
    this.onAcademicCalenderChange();

    this.modalService.open(content, { windowClass: "modalClass", ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });

  }

  private getDismissReason(reason: any): string {

    if (reason === ModalDismissReasons.ESC) {

      return 'by pressing ESC';

    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {

      return 'by clicking on a backdrop';

    } else {

      return `with:  ${reason}`;

    }

  }

  setMouseover(string) {
    this.mouseOverContainer = string;
    // console.log("Mouse over container"+string);
  }
  close() {
    this.closebulkassignform.emit("")
  }
  click() {
    console.log("clicked");

  }
  getImage(uniqueId, index) {
    this._userService.getImage("student", uniqueId, false).subscribe(
      response => {
        if (response != null && response) {
          if (response.image) {
            this.studentList[index]['imageCode'] = response.image;
          }
        }
      });
  }



  initStudentBasicInfoForm() {
    this.academicFormGroup = this.formBuilder.group({
      academicYearUniqueId: ['', Validators.nullValidator],
      academicCalendarUniqueId: ['', Validators.nullValidator],
      courseUniqueId: [null, Validators.required],
      boardUniqueId: [null, Validators.required],
      // batch: ['', Validators.required],
      divisionUniqueId: [null, Validators.required],
      // grade: ['', Validators.required],
      semesterUniqueId: [null, Validators.required],
      batchHasGroupList: this.formBuilder.array([]),
      electiveSubjectList: [],
      selectedComplsorySubject: ['', Validators.required],
      selectedOptionalSubject: [],
      userUniqueId: ['', Validators.nullValidator],
      year: ['', Validators.nullValidator],
      batchGroup: [null, Validators.required],
      gruopCategory: [null, Validators.required],
      // createdOrUpdatedByUniqueId: [this.userData.uniqueId, Validators.nullValidator]
    })
  }

  getBatchList(courseId: string) {

    if (courseId != null && courseId != undefined && courseId != "") {
      this.spinner.show();
      this._batchService.getBatchByCourse(courseId).subscribe(
        res => {
          if (res.success == true) {
            this.orgBatchList = res.list;
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        }
      );
    }
  }

  getCourseListByAcademicCalendar(academicCalendarUniqueId: string) {
    this._courseService.getCourseDetailsedInfoList(3).subscribe(
      res => {
        if (res.success == true) {
          // console.log("get courseList => ",res)
          this.orgCourseList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      }
    );
  }
  getCourseSubjects(CoursId: string) {

    this.selectedOptionalSubject = []
    this.optionalSubject = [];

    if (CoursId != null && CoursId != undefined && CoursId != "") {
      this.spinner.show();
      this._subjectService.getCourseSubjects(CoursId).subscribe(
        (res) => {
          if (res && res.success) {

            let data = (res.list.length > 0) ? res.list[0] : res.list;
            this.complsorySubjects = (data.compulsorySubjects && data.compulsorySubjects.length > 0) ? this.patchSubjectList(data.compulsorySubjects) : [];
            let electiveSubject = data.electiveGroupsAndSubjects;
            console.log(electiveSubject);
            let complsorySubjectUid = this.complsorySubjects.map(x => x.uniqueId);
            this.academicFormGroup.controls['selectedComplsorySubject'].setValue(complsorySubjectUid);

            let electiveGroupList = <FormArray>this.academicFormGroup.controls['electiveSubjectList'];
            electiveGroupList.controls = [];
            if (electiveSubject != null && electiveSubject.length > 0) {
              let electiveSubjectGroupList = [];
              let count = 0;
              for (let i = 0; i < electiveSubject.length; i++) {
                let formGroup = {
                  name: electiveSubject[i].name,
                  uniqueId: electiveSubject[i].uniqueId,
                  electiveSubjects: this.patchSubjects(electiveSubject[i].electiveSubjects)
                };
                count = count + electiveSubject[i].electiveSubjects.length;
                electiveSubjectGroupList.push(formGroup);
              }
              this.electiveSubCount = count;
              this.electiveSubject = electiveSubjectGroupList;
              // this.academicFormGroup.setControl('electiveSubjectList', this.formBuilder.array(electiveSubjectGroupList));
            }

            data.optionalSubjects.forEach(element => {
              let elemnt = {
                uniqueId: element.uniqueId,
                subjectName: element.name,
                csUniqueId: element.courseHasSubjectUniqueId,
                studentSubjectUniqueId: ""
              };
              if (this.selectedOptioanlSub.includes(element.courseHasSubjectUniqueId)) {
                this.selectedOptionalSubject.push(elemnt);
              } else {
                this.optionalSubject.push(elemnt);
              }
            });
            this.optioanlSubCount = data.optionalSubjects.length;
            this.oSubject = this.optionalSubject;
          } else {
            this.toastService.ShowWarning(res.responseMessage);
          }
          this.spinner.hide();

        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   // console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }
  }
  patchSubjects(subjectList: Array<any>) {
    let electiveSubjectList = [];

    if (subjectList != null && subjectList.length > 0) {
      for (let j = 0; j < subjectList.length; j++) {
        let subject = {
          uniqueId: subjectList[j].uniqueId,
          subjectName: subjectList[j].name,
          csUniqueId: subjectList[j].courseHasSubjectUniqueId,
          studentSubjectUniqueId: "",
          selected: (this.selectedElectiveSub.includes(subjectList[j].courseHasSubjectUniqueId)) ? true : false
        };
        electiveSubjectList.push(subject);
      }
    }
    return electiveSubjectList;
  }
  patchSubjectList(subjectList: Array<any>) {
    let subjectListData = [];

    if (subjectList != null && subjectList.length > 0) {
      for (let j = 0; j < subjectList.length; j++) {
        let subject = {
          uniqueId: subjectList[j].uniqueId,
          subjectName: subjectList[j].name,
          csUniqueId: subjectList[j].courseHasSubjectUniqueId,
          studentSubjectUniqueId: "",
          disabled: (this.showAssignDetails) ? true : false
        };
        subjectListData.push(subject);
      }
    }
    return subjectListData;
  }
  initElectiveSubjectGroup(): FormGroup {
    return this.formBuilder.group({
      groupTitle: ['', Validators.nullValidator],
      electiveSubject: this.formBuilder.array([this.initElectiveSubjectList()])
    });
  }
  initElectiveSubjectList(): FormGroup {
    return this.formBuilder.group({
      name: ['', Validators.nullValidator],
      uniqueId: ['', Validators.nullValidator],
      courseHasSubjectId: ['', Validators.nullValidator],
      selected: [false, Validators.nullValidator]
    });
  }
  // -----------------------------Start Get category List By groupCategoryList---------------------------
  getcategorylist(groupCategoryList) {

    let isLinkedArray = this.formBuilder.array([]);
    for (let j = 0; j < groupCategoryList.length; j++) {
      let subject = this.formBuilder.group({
        isLinked: groupCategoryList[j].isLinked,
        uniqueId: groupCategoryList[j].uniqueId,
        name: groupCategoryList[j].name,
      });
      isLinkedArray.push(subject);
    }
    return isLinkedArray;
  }
  getBoardList(courseUniqueId) {
    this.spinner.show();
    let courseArray = [courseUniqueId]
    return this._boardService.getBoardByCourseUid(courseArray).subscribe(
      res => {
        if (res) {
          this.orgBoardList = res[0].boards;
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);

        // }
        this.spinner.hide();
        this.orgBoardList = [];
      }
    );
  }

  getGradeList(uniqueId) {
    this.spinner.show();
    let uniqueIds = [uniqueId];
    return this._gradeService.getAcademicGradeListByBoard(uniqueIds).subscribe(
      res => {
        if (res.success == true) {
          // this.orgGradeList = res[0].grades;
          let gradeList = []
          for (let i = 0; i < res.list[0].courseList.length; i++) {
            const element = res.list[0].courseList[i];
            if (this.selectedCourseUniqueId == element.courseDto.uniqueId) {
              // gradeList=element.grades
              this.orgGradeList = element.grades;
            }
          }
          // this.orgGradeList = gradeList;
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);

        // }  
        this.orgGradeList = [];
        this.spinner.hide();
      }
    );
  }
  get af() {
    return this.academicFormGroup.controls;
  }
  selectGroupOfBatch(batchData, group) {

    for (let i = 0; i < this.academicFormGroup.value.batchHasGroupList.length; i++) {
      if (this.academicFormGroup.value.batchHasGroupList[i].uniqueId == batchData.value.uniqueId) {
        for (let j = 0; j < this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList.length; j++) {
          if (this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList[j].uniqueId == group.value.uniqueId) {
            this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList[j].isLinked = true
          }
          else {
            this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList[j].isLinked = false
          }
        }
      }
    }
  }
  saveStudentAcademicInfo() {
    if (this.showAssignDetails) {

      if (this.selectedStudents.length <= 0) {
        this.toastService.ShowWarning("Please select at least one learner");
        return false;
      }
      this.submitted = true;
      let selectedUsersUniqueId = [];

      if (this.academicFormGroup.invalid) {
        this.scrollToError();
        return false;
      }
      let opSub = this.academicFormGroup.get('selectedOptionalSubject') as FormArray;
      if (this.optioanlSubCount > 0 && (!opSub.value || opSub.value.length == 0)) {
        this.toastService.ShowWarning("Please select optional subject.");
        return;
      }
      let elSub = this.academicFormGroup.get('electiveSubjectList') as FormArray;
      if (this.electiveSubCount > 0 && (!elSub.value || elSub.value.length == 0)) {
        this.toastService.ShowWarning("Please select elective subjects.");
        return;
      }
      this.spinner.show();
      let electiveSubjectGroup = [];
      if (this.electiveSubject.length > 0) {
        for (let i = 0; i < this.electiveSubject.length; i++) {
          const element = this.electiveSubject[i];
          let electiveDto = {
            electiveGroup: {
              name: element.name,
              uniqueId: element.uniqueId
            },
            subjects: []
          }
          for (let j = 0; j < element.electiveSubjects.length; j++) {
            const element2 = element.electiveSubjects[j];
            if (element2.selected) {
              electiveDto.subjects.push({
                subjectName: element2.subjectName,
                csUniqueId: element2.csUniqueId,
                studentSubjectUniqueId: ""
              })
              electiveSubjectGroup.push(electiveDto)
            }
          }
        }
      }

      for (let w = 0; w < this.selectedStudents.length; w++) {
        selectedUsersUniqueId.push(this.selectedStudents[w].studentId);
      }

      var arrayControl = this.academicFormGroup.get('selectedOptionalSubject') as FormArray;
      console.log(arrayControl);
      let finalOptionalSubject = [];
      if (arrayControl.value) {
        finalOptionalSubject = this.oSubject.filter(function (item) {
          return arrayControl.value.indexOf(item.uniqueId) !== -1;
        });
      }

      console.log(finalOptionalSubject);

      var arrayControl = this.academicFormGroup.get('electiveSubjectList') as FormArray;
      let finalElective = [];
      if (arrayControl.value) {
        finalElective = this.electiveSubject.map(result => {
          result.electiveSubjects = result.electiveSubjects.filter(course => (arrayControl.value.includes(course.uniqueId)))
          return result
        })
      }

      console.log(finalElective);

      let final = []
      for (let k = 0; k < finalElective.length; k++) {
        for (let u = 0; u < finalElective[k].electiveSubjects.length; u++) {
          if (finalElective[k].electiveSubjects[u] != undefined) {
            final.push(finalElective[k])
          }
        }
      }

      console.log(final)


      var result = final.reduce((unique, o) => {
        if (!unique.some(obj => obj.name === o.name && obj.value === o.value)) {
          unique.push(o);
        }
        return unique;
      }, []);
      console.log(result);


      let electiveSubjectGroupNew = [];
      if (result.length > 0) {
        for (let i = 0; i < result.length; i++) {
          const element = result[i];
          let electiveDto = {
            electiveGroup: {
              name: element.name,
              uniqueId: element.uniqueId
            },
            subjects: []
          }
          for (let j = 0; j < element.electiveSubjects.length; j++) {
            const element2 = element.electiveSubjects[j];
            electiveDto.subjects.push({
              subjectName: element2.subjectName,
              csUniqueId: element2.csUniqueId,
              studentSubjectUniqueId: ""
            })
            electiveSubjectGroupNew.push(electiveDto)

          }
        }
      }

      console.log(electiveSubjectGroupNew);


      let academicData = this.academicFormGroup.value
      let dto = {
        studentUniqueIds: selectedUsersUniqueId,
        course: {
          uniqueId: academicData.courseUniqueId
        },
        batch: {
          uniqueId: academicData.divisionUniqueId
        },
        board: {
          uniqueId: academicData.boardUniqueId
        },
        grade: {
          uniqueId: academicData.semesterUniqueId
        },
        batchGroupDetails: [
          {
            groupCategory: this.selectedGroupCategory[0],
            batchGroups: this.selectedBatchGroup
          }
        ],
        subjects: {
          compulsorySubjects: this.complsorySubjects,
          optionalSubjects: finalOptionalSubject,
          electiveSubjects: electiveSubjectGroupNew
        }

      }
      // this.academicFormGroup.controls['selectedOptionalSubject'].setValue(JSON.stringify(this.selectedOptionalSubject));
      // this.academicFormGroup.controls['selectedComplsorySubject'].setValue(JSON.stringify(this.complsorySubjects));

      this._userService.assignAcademicInfo(dto).subscribe(
        response => {
          this.toastService.showSuccess("Student academic info saved successfully.");
          this.spinner.hide();
          this.submitted = false;
          this.openSendNowModal();
          // this.showAcademicDetails.emit("");
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        }
      );
    } else {
      this.showAssignDetails = true;
    }
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');

    this.scrollTo(firstElementWithError);
  }

  //drop event here 
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex)
    } else {
      moveItemInArray(this.selectedOptionalSubject, event.previousIndex, event.currentIndex);
    }
  }
  onSelectionChange(index: number, subjectIndex: number, event: any) {
    // let electiveSubjectListArray = <FormArray>this.academicFormGroup.controls['electiveSubjectList'];

    let electiveSubjectGroup = this.electiveSubject[index];

    let electiveSubject = electiveSubjectGroup.electiveSubjects;
    if (event == true) {
      for (let i = 0; i < electiveSubject.length; i++) {
        let formGroup = electiveSubject[i];
        console.log(formGroup);
        if (subjectIndex == i) {
          formGroup.selected = !formGroup.selected;
        } else {
          formGroup.selected = false;
        }
      }
    }
  }
  getDatafromCrourseList(event) {
    this.selectedCourseName = event.name;
    this.selectedElectiveSub = [];
    this.selectedOptioanlSub = [];
    this.orgBatchGroupList = [];
    this.orgBatchList = [];
    this.orgGradeList = [];
    this.orgBoardList = [];
    this.orgGroupCategoreyList = [];
    this.oSubject = [];
    this.electiveSubject = [];
    this.complsorySubjects = [];
    this.electiveSubCount = 0;
    this.optioanlSubCount = 0;
    this.academicFormGroup.controls['divisionUniqueId'].setValue(null);
    this.academicFormGroup.controls['boardUniqueId'].setValue(null);
    this.academicFormGroup.controls['semesterUniqueId'].setValue(null);
    this.academicFormGroup.controls['gruopCategory'].setValue(null);
    this.academicFormGroup.controls['batchGroup'].setValue(null);
    this.academicFormGroup.controls['selectedOptionalSubject'].setValue(null);
    this.academicFormGroup.controls['selectedComplsorySubject'].setValue(null);
    this.academicFormGroup.controls['electiveSubjectList'].setValue(null);
    this.getCourseSubjects(event.uniqueId)
    this.getBatchList(event.uniqueId)
    this.getBoardList(event.uniqueId)
    this.selectedCourseUniqueId = event.uniqueId
  }
  getCustomBatch(event) {
    this.selectedBatchName = event.name;
    if (event != null && event != undefined && event != '') {
      // this.getCustomizedBatch(event.uniqueId)
      // this.getBatchGroupList(event.uniqueId)
      this.getGroupCategoryList(event.uniqueId, "")
    }
  }
  removeSubjectFromList(event, index, number, item) {
    this.selectedOptionalSubject.splice(index, 1);
    this.optionalSubject.push(item);
  }
  changeBoard(event) {
    this.selectedBoardName = event.name;
    this.orgGradeList = [];
    this.academicFormGroup.controls['semesterUniqueId'].setValue(null);
    this.getGradeList(event.uniqueId)
  }
  getBatchGroupList(uniqueId) {
    this.spinner.show();
    return this._batchService.getBatchGroupList(uniqueId).subscribe(
      res => {
        if (res) {
          this.orgBatchGroupList = res[0].batchGroup;
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);

        // }
        this.spinner.hide();
        this.orgBatchGroupList = [];
      }
    );
  }
  getGroupCategoryList(uId, categoryuid) {
    this.spinner.show();
    return this._batchService.getCtegoryGroupByBatchUid([uId]).subscribe(
      res => {
        if (res && res.success == true) {
          this.orgGroupCategoreyList = res.batchCategoryWiseGroups[0].batchGroupList;
          if (categoryuid) {
            res.batchCategoryWiseGroups[0].batchGroupList.forEach(element => {
              if (element.categoryUniqueId == categoryuid) {
                this.orgBatchGroupList = element.batchGroups;
              }
            });
          }

          console.log("group Category", this.orgGroupCategoreyList)
        }
        else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);

        // }
        this.spinner.hide();
        this.orgGroupCategoreyList = [];
      }
    );
  }
  changeGroupCategory(event) {
    this.selectedGroupCategory = [];
    this.selectedBatchGroup = [];
    this.orgBatchGroupList = (event.batchGroups) ? event.batchGroups : [];
    this.academicFormGroup.controls['batchGroup'].setValue(null);
    this.selectedGroupCategory.push(event)
  }
  changeBatchGroup(event) {
    this.selectedBatchGroup = [];
    this.selectedBatchGroup.push(event)
  }

  openDropdown() {
    this.isDropDownOpen = true;
  }

  openSendNowModal() {
    this.modalReference = this.modalService.open(this.assignMore, this.ngbSmModalOptions);
  }

  closeModal() {
    this.studentList = [];
    this.getUnAsignedStudents();
    this.showAssignDetails = false;
    this.selectedStudents = [];
    this.modalReference.close();
  }

  changeGrade(event) {
    this.selectedGradeName = event.name;
  }

  closeDropdown() {
    this.isDropDownOpen = false;
  }

}
