import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CourseplannerServiceService {

  public saveChangesPending = new Subject<boolean>();
  
  constructor(private http: HttpClient) { }

  public getCoursePlannerByUid(courseId) {
    return this.http.get<any>(environment.newApiUrl + '/courseBatchPlanner/getCoursePlannersBatchPlanners/' + courseId);
  }

  public addCoursePlanner(syllabusDto: any) {
    return this.http.post<any>(environment.newApiUrl + '/coursePlanner/create', syllabusDto);
  }

  assignBatchList(cpUniqueId: any) {
    return this.http.get<any>(environment.newApiUrl + '/courseBatchPlanner/get/BatchesForAssigning/' + cpUniqueId);
  }

  getCoursePlannerDetails(uniqueId: any) {
    return this.http.get<any>(environment.newApiUrl + '/coursePlanner/get/' + uniqueId);
  }

  getBatchPlannerDetails(uniqueId: any) {
    return this.http.get<any>(environment.newApiUrl + '/batchPlanner/get/' + uniqueId);
  }

  getCoursePlannerUnitsWithFirstCompleteUnit(uniqueId: any) {
    return this.http.get<any>(environment.newApiUrl + '/coursePlanner/get/UnitsWithFirstCompleteUnit/' + uniqueId);
  }

  getBatchPlannerUnitsWithFirstCompleteUnit(uniqueId: any) {
    return this.http.get<any>(environment.newApiUrl + '/batchPlanner/get/UnitsWithFirstCompleteUnit/' + uniqueId);
  }

  public updateCoursePlanner(syllabusDto: any) {
    return this.http.put<any>(environment.newApiUrl + '/coursePlanner/update', syllabusDto);
  }

  public updateBatchPlanner(syllabusDto: any) {
    return this.http.put<any>(environment.newApiUrl + '/batchPlanner/update', syllabusDto);
  }

  getCoursePlannerUnitsWiseDetails(initArray, coursePlannerId) {
    return this.http.put<any>(environment.newApiUrl + '/coursePlanner/get/UnitDetails/' + coursePlannerId, initArray);
  }

  getBatchPlannerUnitsWiseDetails(initArray, coursePlannerId) {
    return this.http.put<any>(environment.newApiUrl + '/batchPlanner/get/UnitDetails/' + coursePlannerId, initArray);
  }
  
public getMasterCourseByUid(uniqueId: any) {
    return this.http.get<any>(environment.newApiUrl + '/linking/get/mastercourse/' + uniqueId);
  }

  public getCommonSubject(uniqueId:any, mCourseUid: any) {
    return this.http.get<any>(environment.newApiUrl + '/linking/get/commonSubject/' + mCourseUid +'/'+ uniqueId);
  }

  public getGradeAndPlanner(mCourseUid: any,subId:any) {
    return this.http.get<any>(environment.newApiUrl + '/linking/get/gradeAndPlanner/' + mCourseUid +'/'+ subId);
  }

  public getMasterCoursePlanner(plannerId: any) {
    return this.http.get<any>(environment.newApiUrl + '/masterPlanner/get/' + plannerId);
  }

  public addBatchPlanner(syllabusDto) {
    return this.http.post<any>(environment.newApiUrl + '/batchPlanner/create', syllabusDto);
  }
  
  public deleteCoursePlannerById(coursePlannerIdList: any) {
    return this.http.delete<any>(environment.newApiUrl + '/coursePlanner/delete/'+ coursePlannerIdList);
  }

  public getTeacher(teacherFilterDto: any) {
    return this.http.post<any>(environment.apiUrl + '/teacher/list/filter', teacherFilterDto);
  }

  public deleteBatchPlannerById(batchPlannerIdList: any) {
    return this.http.delete<any>(environment.newApiUrl + '/batchPlanner/delete', batchPlannerIdList);
  }

  public getCoursePlannermapping(coursePlannerId: any, mappingType: any) {
    return this.http.get<any>(environment.apiUrl + '/coursePlanner/get/chapter/mapping/' + coursePlannerId + '/' + mappingType);
  }

  public saveTaggingLevel(tagLevelDto: any) {
    return this.http.post<any>(environment.apiUrl + '/save/tagging/level', tagLevelDto);
  }

  public getMappingLevel(coursePlannerId: any) {
    return this.http.get<any>(environment.apiUrl + '/get/mapping/level/' + coursePlannerId);
  }

  public saveMapping(coursePlannerMappingDto: any) {
    return this.http.post<any>(environment.apiUrl + '/coursePlanner/post/chapter/mapping', coursePlannerMappingDto);
  }

  public getCategoryList(orgId: string) {
    return this.http.get<any>(environment.apiUrl + '/get/list/category/' + orgId);
  }
  
}
