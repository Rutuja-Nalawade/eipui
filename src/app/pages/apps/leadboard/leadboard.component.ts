import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-leadboard',
  templateUrl: './leadboard.component.html',
  styleUrls: ['./leadboard.component.scss']
})
export class LeadboardComponent implements OnInit {
  examId = 'c9e5e86e-9bc4-4350-8687-d60794ca56c5';
  studentLis: any;
  constructor( private toastService: ToastsupportService) { }

  ngOnInit() {
    this.getLeadBoardDetails(this.examId);
  }

  getLeadBoardDetails(examId){
    // this.userStudentService.getLeadBoardDetails(examId).subscribe(
    //   (res) => {
    //     if (res) {
    //         this.studentLis = res;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }

}
