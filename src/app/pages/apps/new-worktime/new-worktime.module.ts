import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewWorktimeRoutingModule } from './new-worktime-routing.module';
import { CommonTopBarComponent } from './common-top-bar/common-top-bar.component';
import { ShiftListComponent } from './shift-list/shift-list.component';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { PeriodListComponent } from './period-list/period-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CreateWorktimeComponent } from './create-worktime/create-worktime.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { CreatePeriodComponent } from './create-period/create-period.component';
import { BulkAssignComponent } from './bulk-assign/bulk-assign.component';
// import { AmazingTimePickerModule } from 'amazing-time-picker'; // this line 
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

// https://www.npmjs.com/package/amazing-time-picker
@NgModule({
  declarations: [CommonTopBarComponent, ShiftListComponent, PeriodListComponent, CreateWorktimeComponent, CreatePeriodComponent, BulkAssignComponent],
  imports: [
    CommonModule,
    UIModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgbDropdownModule,
    BsDatepickerModule.forRoot(),
    NgSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NewWorktimeRoutingModule,
    NgxMaterialTimepickerModule
  ]
})
export class NewWorktimeModule { }
