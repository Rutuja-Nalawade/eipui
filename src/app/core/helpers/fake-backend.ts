// import { Injectable } from '@angular/core';
// import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { Observable, of, throwError } from 'rxjs';
// import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

// import { User } from '../models/auth.models';
// import { Generalinfo } from '../models/generalInfo';

// @Injectable()
// export class FakeBackendInterceptor implements HttpInterceptor {
//     generalInfo = new Generalinfo();
  
//     constructor(){
//         this.generalInfo.firstName = 'vikram';
//         this.generalInfo.lastName = 'vikram';
//         this.generalInfo.fullName = 'vikram';
//         this.generalInfo.bloodGroup = 'vikram';
//         this.generalInfo.emailId = 'vikram';
//         this.generalInfo.birthDate = 'vikram';
//         this.generalInfo.phoneNumber = 'vikram';
//         this.generalInfo.adharNumber = 'vikram';
//         this.generalInfo.loginId = 'vikram';
          
//     }
//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       
//         const users: User[] = [
//             {
//                 userId:1,
//                 firstName:'vikram',
//                 lastName:'sasane',
//                 loginId:'vikram',
//                 emailId:'sasane.vikram@gmail.com',
//                 password:'password',
//                 phoneNumber:'9552996098',
//                 status:'A',
//                 portalUsersId:'',
//                 organizationId:1,
//                 organizationName: 'Paptronics',
//                 parentOrgUniqueId : 'uId',
//                 parentOrgId : 1,
//                 userRoles:{},
//                 isSetupDone:true,
//                 step:5,
//                 generalInfo: this.generalInfo,
//                 tokenId: 'tokenId',
//                 uniqueId : 'uniqueId',
//                 orgUniqueId : 'orgUniqueId',
//                 profileImageId : '',
//                 isParent : true,
//                 orgType: 1
//             }

//             // { userId: 1, firstName: 'test', lastName : 'user' ,loginId : 'vik',emailId: 'ubold@coderthemes.com', password: 'test' }
//         ];

//         const authHeader = request.headers.get('Authorization');
//         const isLoggedIn = authHeader && authHeader.startsWith('Bearer fake-jwt-token');

//         // wrap in delayed observable to simulate server api call
//         return of(null).pipe(mergeMap(() => {

//             // authenticate - public
//             if (request.url.endsWith('/api/login') && request.method === 'POST') {
//                 const user = users.find(x => x.emailId === request.body.emailId && x.password === request.body.password);
//                 if (!user) { return error('Email or password is incorrect'); }
//                 return ok({
//                     id: user.userId,
//                     loginId: user.loginId,
//                     firstName: user.firstName,
//                     lastName: user.lastName,
//                     emailId: user.emailId,
//                     token: `fake-jwt-token`
//                 });
//             }

//             // get all users
//             if (request.url.endsWith('/api/users') && request.method === 'GET') {
//                 if (!isLoggedIn) { return unauthorised(); }
//                 return ok(users);
//             }

//             // pass through any requests not handled above
//             return next.handle(request);
//         }))
//             .pipe(materialize())
//             .pipe(delay(500))
//             .pipe(dematerialize());

//         // private helper functions

//         function ok(body) {
//             return of(new HttpResponse({ status: 200, body }));
//         }

//         function unauthorised() {
//             return throwError({ status: 401, error: { message: 'Unauthorised' } });
//         }

//         function error(message) {
//             return throwError({ status: 400, error: { message } });
//         }
//     }
// }

// export let FakeBackendProvider = {
//     // use fake backend in place of Http service for backend-less development
//     provide: HTTP_INTERCEPTORS,
//     useClass: FakeBackendInterceptor,
//     multi: true
// };
