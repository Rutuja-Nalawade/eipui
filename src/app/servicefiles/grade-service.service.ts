import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GradeServiceService {

  constructor(private http: HttpClient) { }

  public getAcademicBoardList() {
    return this.http.get<any>(environment.apiUrl + '/academic-board/get/list');
  }

  public getAcademicGradeList() {
    return this.http.get<any>(environment.apiUrl + '/academic-grade/get/list');
  }

  public getMasterGradeList() {

    return this.http.get<any>(environment.apiUrl + '/master/get/grade');
  }

  public getAcademicGradeListByBoardUid(boardUniqueId: any) {
    return this.http.get<any>(environment.apiUrl + '/academic-board/get/by/course/' + boardUniqueId);
  }

  public updateAcademicGradeBoard(academicGrade: any) {
    return this.http.put<any>(environment.apiUrl + '/academic-grade/update/grade/board', academicGrade);
  }

  public deleteAcademicgrade(gradeUniqueId: any) {
    console.log('delete id recieved' + gradeUniqueId)
    return this.http.delete<any>(environment.apiUrl + '/academic-grade/delete/', gradeUniqueId);
  }

  public getAcademicGradeListByBoard(uniqueIds) {
    return this.http.post<any>(environment.apiUrl + '/academic-grade/get/by-board-list', uniqueIds);
  }
}
