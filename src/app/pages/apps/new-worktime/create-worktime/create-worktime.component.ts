import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
// import { AmazingTimePickerService } from 'amazing-time-picker';

@Component({
  selector: 'app-create-worktime',
  templateUrl: './create-worktime.component.html',
  styleUrls: ['./create-worktime.component.scss']
})
export class CreateWorktimeComponent implements OnInit {
  submitted = false;
  isDaysSelected = false;
  shiftForm: FormGroup;
  shiftNewForm: FormGroup;
  shiftsList: FormArray;
  workingDaysList: FormArray;
  workDayList: any;
  workingDayList = [];
  teacherList = [];
  startTime: any = '';
  endTime: any = '';
  pageIndex = 0;
  filterData = {};
  selectedTeacher: any = [];
  selectedWorkingDays = [];
  selectedWorkingDaysValue: any = [];
  isEditShift = false;
  shiftTypeList = [];
  @Input() shiftList: any;
  @Input() shiftData: any;
  @Output() close = new EventEmitter<string>();
  // private atp: AmazingTimePickerService,
  constructor(private commonService: NewCommonService, private _shiftService: ShiftServiceService, private _userService: UserServiceService, private fb: FormBuilder, private toastService: ToastsupportService, private datepipe: DatePipe, private spinner: NgxSpinnerService) {
    this.shiftForm = this.fb.group({
      shiftsList: this.fb.array([]),
    });
  }

  ngOnInit() {

  }
  //   open() {
  //   const amazingTimePicker = this.atp.open();
  //   amazingTimePicker.afterClose().subscribe(time => {
  //     console.log(time);
  //   });
  // }
  async ngOnChanges(changes: SimpleChanges) {
    console.log("object", changes);
    if (changes['shiftData'] && changes['shiftData'].currentValue && changes['shiftList'].currentValue) {
      console.log("object");
      this.getshiftTypeList();
      this.getshiftworkdayList();
      this.workDayList = { shiftsList: changes['shiftList'].currentValue };
      this.selectedShiftAssignToForm(this.shiftData);
      // this.assignOrgShiftsToForm();
      this.isEditShift = true;
      this.isDaysSelected = true;
    } else {
      // console.log('Add new worktime called')
      this.getshiftTypeList();
      // this.getOrgWorkDays();
      this.addShifts();
      this.getshiftworkdayList();
    }
  }
  get f() { return this.shiftNewForm.controls; }

  getshiftTypeList() {
    return this._shiftService.getAllShiftTypes().subscribe(
      res => {
        if (res) {
          res.forEach(element => {
            let dto = { id: element.key, name: element.value }
          });
          this.shiftTypeList = res;
          // console.log('shift type => ' + JSON.stringify(res))
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        // }
      });
  }

  getshiftworkdayList() {
    return this._shiftService.getShiftWorkingDays().subscribe(
      res => {
        if (res) {
          // console.log(' shift working days => ' + JSON.stringify(res))
          let temp = res;
          let temp2 = []
          if (this.isEditShift) {
            for (let i = 0; i < temp.length; i++) {
              for (let j = 0; j < this.selectedWorkingDaysValue.length; j++) {
                if (temp[i].dayValue == this.selectedWorkingDaysValue[j]) {
                  temp2.push(temp[i].uniqueId)
                }
              }
            }
          }
          this.selectedWorkingDays = temp2;
          this.workingDayList = temp.sort(function (a, b) {
            return a.dayValue - b.dayValue;
          })
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        // }
      });
  }
  selectedShiftAssignToForm(shift) {
    // console.log('edit shift details' + JSON.stringify(shift));
    let shiftStartTime = this.setTimeToDate(shift.startTime);
    this.startTime = moment(shiftStartTime).format('hh:mm A');
    let shiftEndTime = this.setTimeToDate(shift.endTime);
    this.endTime = moment(shiftEndTime).format('hh:mm A');
    console.log(shiftStartTime, shiftEndTime);
    this.selectedWorkingDaysValue = [];
    for (let index = 0; index < shift.days.length; index++) {

      let dayValue = shift.days[index].dayValue;
      this.selectedWorkingDaysValue.push(dayValue);
    }
    this.getTeachers({ id: shift.type });
    this.shiftNewForm = this.fb.group({
      id: [shift.id, Validators.nullValidator],
      shiftUid: [shift.unique_id, Validators.nullValidator],
      shiftName: [shift.name, Validators.required],
      startTime: [this.startTime, Validators.required],
      endTime: [this.endTime, Validators.required],
      isStartTimeGreater: [false, Validators.nullValidator],
      type: [shift.type, Validators.required],
      shiftbreaksList: this.fb.array(this.assignBreaks(this.startTime, this.endTime, shift.breaks)),
      workingDaysList: this.fb.array(this.assignShiftWorkingDays(shift.days))
    });
    this.selectedTeacher = [];
    if (shift.users && shift.users.length) {
      for (let index = 0; index < shift.users.length; index++) {
        let userId = shift.users[index].uniqueId;
        this.selectedTeacher.push(userId);
      }
    }
  }
  assignShiftWorkingDays(workingDays: Array<any>) {

    let workingDaysList = [];
    if (workingDays != null && workingDays.length > 0) {
      for (let i = 0; i < workingDays.length; i++) {
        let group = this.fb.group({
          uniqueId: [workingDays[i].uniqueId, Validators.required],
          day: [workingDays[i].day, Validators.nullValidator],
          dayValue: [workingDays[i].dayValue, Validators.required],
          isLinked: [workingDays[i].isLinked, Validators.nullValidator],
        });
        workingDaysList.push(group);
      }
    }
    return workingDaysList;
  }
  assignBreaks(shiftStartTime: any, shiftEndTime: any, shiftsBreaks: Array<any>) {

    let breaksArray = [];
    if (shiftsBreaks != null && shiftsBreaks.length > 0) {
      for (let i = 0; i < shiftsBreaks.length; i++) {
        let breakStartTime = this.setTimeToDate(shiftsBreaks[i].startTime);

        let breakEndTime = this.setTimeToDate(shiftsBreaks[i].endTime);
        let group = this.fb.group({
          breakUid: [shiftsBreaks[i].breakUid, Validators.nullValidator],
          startTime: [moment(breakStartTime).format('hh:mm A'), Validators.nullValidator],
          endTime: [moment(breakEndTime).format('hh:mm A'), Validators.nullValidator],
          isStartTimeGreater: [false, Validators.nullValidator],
          min: [shiftStartTime],
          max: [shiftEndTime]
        });
        breaksArray.push(group);
      }
    }
    return breaksArray;
  }
  addShifts() {

    this.shiftNewForm = this.fb.group({
      id: ['', Validators.nullValidator],
      shiftUid: ['', Validators.nullValidator],
      shiftName: ['', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      type: [null, Validators.required],
      isStartTimeGreater: [false, Validators.nullValidator],
      shiftbreaksList: this.fb.array(this.addBreaksUnderShift()),
      workingDaysList: this.fb.array([]),
    });
  }
  assignWorkingDayList() {
    let workingDaysList = [];
    if (this.workingDayList != null && this.workingDayList.length > 0) {
      for (let i = 0; i < this.workingDayList.length; i++) {
        let group = this.fb.group({
          uniqueId: [this.workingDayList[i].uniqueId, Validators.required],
          day: [this.workingDayList[i].day, Validators.required],
          dayValue: [this.workingDayList[i].dayValue, Validators.nullValidator],
          isLinked: [this.workingDayList[i].isLinked, Validators.required],
        });
        workingDaysList.push(group);
      }
    }
    return workingDaysList;
  }
  addBreaksUnderShift() {
    let breaksArray = [];
    let breakData = this.fb.group({
      id: ['', Validators.nullValidator],
      breakUid: ['', Validators.nullValidator],
      name: ['', Validators.nullValidator],
      startTime: ['', Validators.nullValidator],
      endTime: ['', Validators.nullValidator],
      isStartTimeGreater: [false, Validators.nullValidator],
      min: [this.startTime],
      max: [this.endTime]
    });
    breaksArray.push(breakData);
    return breaksArray;
  }
  onStartDate(startTime: any, isForUpdate: number) {

    console.log('start time: ', this.convertTime(startTime))
    this.startTime = this.convertTime(startTime);
    this.endTime = this.shiftNewForm.controls['endTime'].value;
    let breaklist = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    for (let index = 0; index < breaklist.value.length; index++) {
      let breakData = <FormGroup>breaklist.controls[index];
      breakData.controls['min'].setValue(this.startTime);
      breakData.controls['max'].setValue(this.endTime);
    }
    if (this.startTime > this.endTime) {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(false);
    } else {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(true);
    }
  }

  onEndDate(endtime: any, isForUpdate: number) {
    console.log('end time: ', endtime, this.convertTime(endtime))
    this.endTime = this.convertTime(endtime)
    this.startTime = this.convertTime(this.shiftNewForm.controls['startTime'].value);
    console.log(this.startTime, this.endTime);
    let breaklist = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    for (let index = 0; index < breaklist.value.length; index++) {
      let breakData = <FormGroup>breaklist.controls[index];
      breakData.controls['min'].setValue(this.startTime);
      breakData.controls['max'].setValue(this.endTime);
    }
    if (this.endTime < this.startTime) {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(true);
    } else {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(false);
    }
  }
  onBreakStartDate(breakStartTime: any, index: number) {
    console.log('break start time' + this.convertTime(breakStartTime))
    let startTime = this.convertTime(breakStartTime);
    let shift = <FormGroup>this.shiftNewForm;
    let breakList = <FormArray>shift.controls['shiftbreaksList'];
    let shiftBreak = <FormGroup>breakList.controls[index];
    let endTime = this.convertTime(shiftBreak.controls['endTime'].value);
    if (startTime < endTime) {
      console.log('start time is less 1')
      shiftBreak.controls['isStartTimeGreater'].setValue(false);
    } else {
      console.log('start time is greater 1')
      shiftBreak.controls['isStartTimeGreater'].setValue(true);
    }
  }

  onBreakEndDate(breakEndTime: any, index: number) {
    console.log('break end time' + this.convertTime(breakEndTime))
    let endTime = this.convertTime(breakEndTime);
    let shift = <FormGroup>this.shiftNewForm;
    let breakList = <FormArray>shift.controls['shiftbreaksList'];
    let shiftBreak = <FormGroup>breakList.controls[index];
    let startTime = this.convertTime(shiftBreak.controls['startTime'].value)
    console.log('on end start time' + startTime)
    if (startTime < endTime) {
      console.log('start time is less 2')
      shiftBreak.controls['isStartTimeGreater'].setValue(false);
    } else {
      console.log('start time is greater 2')
      shiftBreak.controls['isStartTimeGreater'].setValue(true);
    }
  }

  convertTime(time) {
    return moment(time, 'hh:mm A').format('HH:mm');
  }
  addBreak() {
    let breakArray = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    let breakData = this.fb.group({
      id: ['', Validators.nullValidator],
      breakUid: ['', Validators.nullValidator],
      startTime: ['', Validators.nullValidator],
      endTime: ['', Validators.nullValidator],
      isStartTimeGreater: [false, Validators.nullValidator],
      min: [this.startTime],
      max: [this.endTime]
    });
    breakArray.push(breakData);
  }
  removeBreak(index) {
    let breakList = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    breakList.removeAt(index);
  }
  cancel() {
    this.submitted = false;
    this.close.emit("")
    this.isEditShift = false;
  }
  change(daysList) {
    if (daysList.length > 0) {
      this.isDaysSelected = true;
    } else {
      this.isDaysSelected = false;
    }
    for (let i = 0; i < this.workingDayList.length; i++) {
      const element = this.workingDayList[i];
      if (daysList.length > 0) {
        for (let j = 0; j < daysList.length; j++) {
          if (daysList[j].dayValue == element.dayValue) {
            element.isLinked = true;
            break;
          } else {
            element.isLinked = false;
          }
        }
      } else {
        element.isLinked = false;
      }
    }
  }
  save() {
    this.submitted = true;
    if (this.shiftNewForm.invalid) {
      return;
    }
    if (!this.isDaysSelected) {
      return;
    }

    console.log(this.shiftForm);
    let formValue = this.shiftNewForm.value;
    this.spinner.show();
    if (!this.isEditShift) {
      formValue.workingDaysList = [];
      for (let i = 0; i < this.workingDayList.length; i++) {
        let group;
        if (this.workingDayList[i].isLinked == true) {
          group = {
            uniqueId: this.workingDayList[i].uniqueId,
          };
          formValue.workingDaysList.push(group);
        }
      }
    }
    // let dto = this.shiftForm.value;
    // dto.shiftsList.push(formValue);
    let processData = formValue;
    // console.log('submit data' + JSON.stringify(submitDataDto))
    let days = [];
    processData.workingDaysList.forEach(element => {
      days.push(element.uniqueId);
    });
    let breaks = []
    processData.shiftbreaksList.forEach(elem => {
      let dto = {
        startTime: this.convertTime(elem.startTime),
        endTime: this.convertTime(elem.endTime),
      }
      breaks.push(dto)
    });

    let submitDataDto = {
      name: processData.shiftName,
      type: processData.type,
      startTime: this.convertTime(processData.startTime),
      endTime: this.convertTime(processData.endTime),
      unique_id: "",
      days: days,
      breaks: breaks,
      userIds: this.selectedTeacher,
    }

    console.log('submit data', submitDataDto);
    this._shiftService.addShiftWorkingDays(submitDataDto).subscribe(
      (res) => {
        if (res) {
          this.toastService.showSuccess("Shift Added Succesfully");
          this.shiftNewForm.reset()
          this.spinner.hide();
          this.commonService.workTimeProcess.next(1);
          this.cancel();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  update() {
    this.submitted = true;
    if (this.shiftNewForm.invalid) {
      return;
    }
    if (!this.isDaysSelected) {
      return;
    }

    let formValue = this.shiftNewForm.value;
    // this.spinner.show();
    if (!this.isEditShift) {
      for (let i = 0; i < this.workingDayList.length; i++) {
        let group;
        if (this.workingDayList[i].isLinked == true) {
          group = {
            uniqueId: this.workingDayList[i].uniqueId,
          };
          formValue.workingDaysList.push(group);
        }
      }
    }
    let dto = this.shiftForm.value;
    dto.shiftsList.push(formValue);
    let processData = dto.shiftsList[0];
    // console.log('processData data' + JSON.stringify(processData))
    let breaks = []
    processData.shiftbreaksList.forEach(elem => {
      let dto = {
        startTime: this.convertTime(elem.startTime),
        endTime: this.convertTime(elem.endTime),
      }
      breaks.push(dto)
    });

    let submitDataDto = {
      name: processData.shiftName,
      type: processData.type,
      startTime: this.convertTime(processData.startTime),
      endTime: this.convertTime(processData.endTime),
      unique_id: processData.shiftUid,
      days: this.selectedWorkingDays,
      breaks: breaks,
      userIds: this.selectedTeacher,
    }
    console.log('udate data selected working days', JSON.stringify(this.selectedWorkingDays));
    console.log('udate data', JSON.stringify(submitDataDto));
    this._shiftService.updateShiftWorkingDays(submitDataDto).subscribe(
      (res) => {
        if (res) {
          this.toastService.showSuccess("Shift updated Succesfully");
          this.shiftNewForm.reset()
          this.spinner.hide();
          this.commonService.workTimeProcess.next(1);
          this.cancel();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  getTimeOnly(time) {
    return moment(time).format('HH:MM:SS');
  }
  getTeachers(value) {
    this.selectedTeacher = [];
    // console.log('get users by type on change' + JSON.stringify(value))
    // let serachDto = null;
    // this.spinner.show();
    // if (isLoadMoreData) {
    //   this.pageIndex = this.pageIndex + 1;
    // } else {
    //   this.spinner.show();
    //   // this.setStaffListDivCollapsed = false;
    this.pageIndex = 0;
    //   this.teacherList = [];
    //   this.filterData = null;
    // }

    // if (this.filterData == null) {
    //   serachDto = {
    //     "pageIndex": this.pageIndex,
    //     "subjectList": []
    //   };
    // } else {
    //   serachDto = this.filterData;
    // }
    this._userService.getUserByPageIndexAndType(this.pageIndex, value.id, 100).subscribe(
      response => {
        // console.log('user list =>', JSON.stringify(response))
        if (response.responseMessage == "Success") {
          this.teacherList = response.users
        } else {
          this.toastService.ShowInfo("Users not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  selectDay(index, days) {
    let workDaysArray = <FormArray>this.shiftNewForm.controls['workingDaysList']
    let day = <FormGroup>workDaysArray.controls[index];
    if (days.value.isLinked) {
      day.controls.isLinked.setValue(false);
    } else {
      day.controls.isLinked.setValue(true);
    }
    let daysArray = workDaysArray.value;
    let selectedDays = daysArray.filter(x => x.isLinked == true);
    this.isDaysSelected = (selectedDays && selectedDays.length > 0) ? true : false;
  }
  customSearchFn(term: string, item: any) {
    term = term.toLocaleLowerCase();
    return item.code.toLocaleLowerCase().indexOf(term) > -1 || item.countryName.toLocaleLowerCase().indexOf(term) > -1;
  }
  setTimeToDate(time) {

    let shiftStartTime = new Date('1/1/2021 ' + time);
    // let splitTimeArry = time.split(":");
    // let hr = splitTimeArry[0];
    // let min = splitTimeArry[1];
    // shiftStartTime.setHours(+hr);
    // shiftStartTime.setMinutes(+min);
    return shiftStartTime;
  }
}
