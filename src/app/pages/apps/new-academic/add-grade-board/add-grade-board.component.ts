import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { SetupComponent } from 'src/app/account/new-authentication/setup/setup.component';

@Component({
  selector: 'app-add-grade-board',
  templateUrl: './add-grade-board.component.html',
  styleUrls: ['./add-grade-board.component.scss']
})
export class AddGradeBoardComponent implements OnInit {
  masterBoardList: any;
  masterGradeList: any;
  @Input() isEditValue: any;
  @Input() boardList: any;
  @Input() gradeList: any;
  @Input() boards = [];
  @Input() grades = []
  mainBoardList = [];
  mainGradeList = [];
  @Output() newItemEvent = new EventEmitter<string>();
  @Input() isFromSetup: any;
  constructor(private setup: SetupComponent, private spinner: NgxSpinnerService, private _gradeServiceService: GradeServiceService, private toastService: ToastsupportService, private _boardService: BoardServiceService) { }

  ngOnInit() {

    console.log("isEditValue", this.isEditValue)
    console.log("boardList", this.boardList)
    console.log("gradeList", this.gradeList)
    console.log("boards", this.boards)
    console.log("grades", this.grades)
    this.getMasterBoardList();
    this.getMasterGradeList();
  }

  gradeSelected(event) {
    console.log("selected grade event", event);
    this.gradeList = [];
    event.forEach(element => {
      this.gradeList.push(element);

    });
  }

  bardSelected(event) {
    console.log("event", event);
    this.boardList = [];
    event.forEach(element => {
      this.boardList.push(element);

    });
  }


  cancelBtn(value) {
    this.newItemEvent.emit(value);
  }

  onSubmit() {

    this.spinner.show()
    //first deselcct all selections
    this.masterGradeList.forEach(mastergrade => {
      mastergrade.doesExist = false;
    });
    // set true for selected elements
    this.masterGradeList.forEach(mastergrade => {
      this.grades.forEach(grade => {
        if (mastergrade.uniqueId == grade) {
          mastergrade.doesExist = true;
        }
      });
    });
    //first deselcct all selections
    this.masterBoardList.forEach(masterboard => {
      masterboard.doesExist = false;
    });
    // set true for selected elements
    this.masterBoardList.forEach(masterboard => {
      this.boards.forEach(board => {
        if (masterboard.uniqueId == board) {
          masterboard.doesExist = true;
        }
      });
    });
    let dto = {
      gradeList: this.masterGradeList,
      boardList: this.masterBoardList
    }
    // console.log("master grade after selection =>" ,JSON.stringify(dto));
    this._gradeServiceService.updateAcademicGradeBoard(dto).subscribe(res => {
      //console.log('res data===>>>', res);
      if (res) {
        this.toastService.showSuccess("Board & Grade saved successfully");
        if (!this.isFromSetup) {
          this.cancelBtn(false);
        } else if (this.isFromSetup) {
          this.setup.selectedStep(2);
        }
      }

      this.spinner.hide()
      // this.loading = false;
    }, (error: HttpErrorResponse) => {
      // if (error instanceof HttpErrorResponse) {

      // } else {
      //   // this.loading = false;
      //   this.toastService.ShowError(error);
      // }

      this.spinner.hide()
    })

  }

  addEditBoard(board, i) {
    // console.log("board",board,i)
    var ele = document.getElementById(i);
    // console.log("ele",ele)
    // console.log("this.boards",this.boards )

    if (ele.classList.contains('gradeBox')) {
      ele.classList.remove("gradeBox");
      ele.classList.add("deactivategradeBox");

      const index = this.mainBoardList.indexOf(board);
      //  console.log("index",index)
      if (index > -1) {
        this.mainBoardList.splice(index, 1);
      }

    } else if (ele.classList.contains('deactivategradeBox')) {
      ele.classList.remove("deactivategradeBox");
      ele.classList.add("gradeBox");
      this.mainBoardList.push(board)
    }

    // console.log("this.mainBoardList",this.mainBoardList)  
  }

  addEditGrade(grade, j) {
    var ele = document.getElementById(j);
    console.log("ele", ele)

    if (ele.classList.contains('gradeBox')) {
      ele.classList.remove("gradeBox");
      ele.classList.add("deactivategradeBox");

      const index = this.mainGradeList.indexOf(grade);
      console.log("index", index)
      if (index > -1) {
        this.mainGradeList.splice(index, 1);
      }

    } else if (ele.classList.contains('deactivategradeBox')) {
      ele.classList.remove("deactivategradeBox");
      ele.classList.add("gradeBox");
      this.mainGradeList.push(grade)
    }

    // console.log("this.mainGradeList",this.mainGradeList)  
  }

  getMasterBoardList() {

    this._boardService.getMasterBoardList().subscribe(
      res => {
        if (res) {
          // console.log("board res",res)
          let temp = res;
          let tempBoards = [];
          if (this.boardList.length > 0) {
            for (let i = 0; i < this.boardList.length; i++) {
              for (let j = 0; j < temp.length; j++) {
                if (this.boardList[i].name == temp[j].name) {
                  tempBoards.push(temp[j].uniqueId)
                }
              }
            }
          }
          this.boards = tempBoards;
          this.masterBoardList = temp;
          // console.log("this.boards",this.boards)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  getMasterGradeList() {

    this._gradeServiceService.getMasterGradeList().subscribe(
      res => {
        if (res) {
          // console.log("grade res",res)
          let temp = res;
          let tempGrades = [];
          if (this.gradeList.length > 0) {
            for (let i = 0; i < this.gradeList.length; i++) {
              for (let j = 0; j < temp.length; j++) {
                if (this.gradeList[i].name == temp[j].name) {
                  tempGrades.push(temp[j].uniqueId)
                }
              }
            }
          }
          this.grades = tempGrades;
          this.masterGradeList = temp;
          // console.log('this.grades', this.grades)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });
  }
}
