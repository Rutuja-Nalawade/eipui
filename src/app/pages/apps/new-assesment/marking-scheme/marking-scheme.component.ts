import { Component, OnInit, Input, ViewChild, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { AssesmentService } from '../../../../servicefiles/assesment.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
@Component({
    selector: 'app-marking-scheme',
    templateUrl: './marking-scheme.component.html',
    styleUrls: ['./marking-scheme.component.scss']
})
export class MarkingSchemeComponent implements OnInit {
    @ViewChild('modelwizardForm', { static: false }) modelwizard: BaseWizardComponent;
    @Input() markingSchemeForm: FormGroup;
    @Input() isImporedScheme:boolean;
    spinner: boolean = false;
    markingSchemeList = [];
    markingScheme: any;
    isAddSchemeSubmit = false;
    constructor(private fb: FormBuilder, private modalService: NgbModal, private toastService: ToastsupportService, private examPatternService: AssesmentService) { }

    ngOnInit() {
        this.examPatternService.spinner.subscribe(
            (value: any) => {
                this.spinner = value;
            });
        this.getSchemeList();

        console.log("this.markingSchemeForm",this.markingSchemeForm.controls)
    }
    ngOnChanges(changes: SimpleChanges) {
        console.log("markingSchemeForm",this.markingSchemeForm.value);
        
        if (changes['isImporedScheme'].currentValue== true) {
          this.getSelectedScheme(this.markingSchemeForm.controls['uniqueId'].value);
        }
      }
    getRightMarks(value: number) {
        this.examPatternService.rightMarks.next(value);
    }

    initSubMarkingScheme() {
        return this.fb.group({
            correctOption: ['', Validators.required],
            rightOption: ['', Validators.required],
            wrongOption: [0, Validators.required],
            marks: ['', Validators.required],
        });
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }



    wizardFirstNextStep() {
    }


    formSubmit() {
        // if (this.examPatternForm.valid) {
        this.modelwizard.navigation.goToNextStep();
        // }

    }

    getMarks(value: number, index: number) {
        let schemeArray = <FormArray>this.markingSchemeForm.controls['markingSchemeCriteriaList'];
        let currentScheme = <FormGroup>schemeArray.controls[index];
        if (value > parseInt(this.markingSchemeForm.controls['highestMarks'].value)) {
            currentScheme.controls['marks'].setValue('');
            this.toastService.ShowWarning("Marks should be less then Highest entered marks..");
            return false;
        }
    }

    getValue(value: number, index: number, optionType: number) {
        let schemeArray = <FormArray>this.markingSchemeForm.controls['markingSchemeCriteriaList'];
        let currentScheme = <FormGroup>schemeArray.controls[index];
        for (let i = 0; i < schemeArray.length; i++) {
            let scheme = <FormGroup>schemeArray.controls[i];
            if (i != index && currentScheme.controls['correctOption'].value == scheme.controls['correctOption'].value && currentScheme.controls['rightOption'].value == scheme.controls['rightOption'].value) {
                currentScheme.controls['rightOption'].setValue('');
                this.toastService.ShowWarning("Duplicate scheme found.Please check..");
                return false;
            }
        }

        let optionCount: number = this.markingSchemeForm.controls['optionCount'].value;
        let schemeGroup = <FormGroup>schemeArray.controls[index];

        if (optionType == 2 && parseInt(schemeGroup.controls['rightOption'].value) > parseInt(schemeGroup.controls['correctOption'].value)) {
            this.toastService.ShowWarning("Right Option should be less then or equal to Correct Option..");
            schemeGroup.controls['rightOption'].setValue(0);
        }
        if (value > optionCount) {
            this.toastService.ShowWarning("Option count should be less than or equal to " + optionCount + " .");
            switch (optionType) {
                case 1:
                    schemeGroup.controls['correctOption'].setValue(0);
                    break;
                case 2:
                    schemeGroup.controls['rightOption'].setValue(0);
                    break;
                case 3:
                    schemeGroup.controls['wrongOption'].setValue(0);
                    break;
            }
        }
    }

    addNewSubScheme() {
        let formArrayOfSubMarkingScheme = <FormArray>this.markingSchemeForm.controls['markingSchemeCriteriaList'];
        formArrayOfSubMarkingScheme.push(this.initSubMarkingScheme());
    }

    removeNewSubScheme(index: number) {
        let formArrayOfSubMarkingScheme = <FormArray>this.markingSchemeForm.controls['markingSchemeCriteriaList'];
        formArrayOfSubMarkingScheme.removeAt(index);
    }

    changeOptionScemeSelect(event) {

        switch (event) {
            case "1":

                break;
            case "2":
                this.markingSchemeForm.controls['name'].setValue("");
                this.markingSchemeForm.controls['highestMarks'].setValue("");
                this.markingSchemeForm.controls['leaveMarks'].setValue("");
                this.markingSchemeForm.controls['defaultMarks'].setValue("");
                break;
        }

    }

    checkExists(value: string) {
        var sceme = this.markingSchemeList.find(x => x.name == value);

        this.markingSchemeList.forEach(element => {
            var valueName = element.name;
            if (valueName.toUpperCase() == value.toUpperCase()) {
                this.markingSchemeForm.controls['name'].setValue("");
                this.toastService.ShowWarning("Scheme Name already exist");
            }
        });
    }


    //vinayak added for new scheme

    getSchemeList() {
        this.markingSchemeList = [];
        return this.examPatternService.getMarkingSchemeList().subscribe(
            res => {
                if (res.success == true) {
                    this.markingSchemeList = res.list;
                   
                }

            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }

    changeSchemeType(event){
        console.log("event.target.value == 2",event.target.value)
        if(event.target.value == 1){
            this.markingSchemeForm.controls['uniqueId'].setValue(this.markingSchemeList[0].uniqueId);
            this.getSelectedScheme(this.markingSchemeList[0].uniqueId);  
        }else if(event.target.value == 2){
            console.log("test")
            this.markingSchemeForm.controls['name'].setValue(null);
            this.markingSchemeForm.controls['highestMarks'].setValue(null);
            this.markingSchemeForm.controls['leaveMarks'].setValue(null);
            this.markingSchemeForm.controls['defaultMarks'].setValue(null);
            this.markingSchemeForm.controls['uniqueId'].setValue(null);
            let formArray=<FormArray>this.markingSchemeForm.controls['markingSchemeCriteriaList'];
            formArray.controls=[];
            formArray.push(this.initSubMarkingScheme())
        }
    }

    getSelectedScheme(value: string) {
       
        return this.examPatternService.getMarkingSchemeDetailedInfo(value).subscribe(
            res => {
                this.markingScheme = '';
                console.log("res", res)
                this.markingScheme = res;
                console.log("markingScheme", this.markingScheme);

                this.examPatternService.rightMarks.next(this.markingScheme.highestMarks);

                this.markingSchemeForm.controls['name'].setValue(this.markingScheme.name);
                this.markingSchemeForm.controls['highestMarks'].setValue(this.markingScheme.highestMarks);
                this.markingSchemeForm.controls['leaveMarks'].setValue(this.markingScheme.leaveMarks);
                this.markingSchemeForm.controls['defaultMarks'].setValue(this.markingScheme.defaultMarks);
                this.markingSchemeForm.controls['uniqueId'].setValue(this.markingScheme.uniqueId);
                let schemeArray=<FormArray>this.markingSchemeForm.controls['markingSchemeCriteriaList'];
                schemeArray.controls=[];
                
                for(let i=0;i<this.markingScheme.markingSchemeCriteriaList.length;i++){
                    schemeArray.push(this.initmarkingSchemeCriteriaList(this.markingScheme.markingSchemeCriteriaList[i]))
                }
                console.log("this.markingSchemeForm",this.markingSchemeForm.value,this.markingSchemeForm.invalid,this.markingSchemeForm.valid)
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
        }

        initmarkingSchemeCriteriaList(value) {
            return this.fb.group({
                correctOption: [value.correctOption, Validators.required],
                rightOption: [value.rightOption, Validators.required],
                wrongOption: [value.wrongOption, Validators.required],
                marks: [value.marks, Validators.required],
            });
        }   

}
