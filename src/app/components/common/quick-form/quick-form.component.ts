import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, SimpleChanges } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';

@Component({
  selector: 'app-quick-form',
  templateUrl: './quick-form.component.html',
  styleUrls: ['./quick-form.component.scss']
})
export class QuickFormComponent implements OnInit {

  basicInfoForm: FormGroup;
  prefixForm: FormGroup;
  submitted: boolean = false;
  genders: any = [];
  userData: any;
  roleInfo = [];
  isSaved = false;
  selectedRole = [];
  userType = "student";
  perfixName = "Student";
  validMessage = "";
  errMessage = "";
  modalReference: NgbModalRef;
  @Input() quickFormType: any;
  @Input() roleData: any;
  @Output() getList = new EventEmitter<string>();
  ngbSmallModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false,
    size: 'sm'
  };
  @ViewChild('addPrefixConfirmationModal', { static: true }) addPrefixConfirmationModal: ElementRef;
  constructor(private spinner: NgxSpinnerService, private _commonService: NewCommonService, private toastService: ToastsupportService, private formBuilder: FormBuilder, private modalService: NgbModal, private router: Router, private _userService: UserServiceService) {
    this.userData = this._commonService.getUserData();
    this.genders = [{ id: "M", name: "Male" }, { id: "F", name: "Female" }, { id: "O", name: "Other" }]
  }

  ngOnInit() {
    console.log(this.selectedRole);
    this.initPerfix();

  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(this.selectedRole);
    if (changes['quickFormType']) {
      console.log(this.selectedRole);
      this.initForm();
      this.selectedRole = [];
      this.selectedRole.push(this.roleData.role.uniqueId)
      this.roleInfo = (this.roleData.roleCombinations) ? this.roleData.roleCombinations : "";
    }
  }
  initForm() {
    switch (this.quickFormType) {
      case 1:
        this.userType = "student";
        this.initStudentbasicInfoForm();
        break;
      case 2:
        this.userType = "staff";
        this.initStaffInfoForm();
        break;
      case 3:
        this.userType = "teacher";
        this.initTeacherInfoForm()
        break;
      case 4:
        this.userType = "guardian";
        this.initParentBasicInfo();
        break;
      default:
        break;
    }
  }
  initPerfix() {
    this.prefixForm = this.formBuilder.group({
      name: ['', Validators.required]
    })
  }
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  saveUser(saveType, prefixModal) {
    this.submitted = true;

    // console.log("this.basicInfoForm",this.basicInfoForm);
    if (this.basicInfoForm.invalid) {

      this.scrollToError();
      return;
    }
    // this.loading = true;
    this.spinner.show();
    this.selectedRole = [];
    this.selectedRole.push(this.roleData.role.uniqueId)
    let generalInfo = this.basicInfoForm.value.generalInfo;
    if (this.quickFormType == 2 || this.quickFormType == 3) {
      this.selectedRole = this.selectedRole.concat(this.basicInfoForm.value.roleId);
    }
    this.selectedRole = this.selectedRole.filter(this.onlyUnique);
    generalInfo['roleIds'] = this.selectedRole;
    this._userService.addUser(this.userType, generalInfo)
      .subscribe(data => {
        console.log(data);
        let type = this.capitalize(this.userType);
        if (data) {
          this.toastService.showSuccess(type + " created successfully.");
          this.isSaved = true;
          setTimeout(() => {
            this.getList.emit(saveType.toString())
            this.initForm();
            this.submitted = false;
            this.isSaved = false;
          }, 2000);
        } else {
          this.toastService.ShowError(type + " not created.");

        }
        this.spinner.hide();
        this.selectedRole.push(this.roleData.role.uniqueId)
      },
        (error: HttpErrorResponse) => {
          this.spinner.hide();
          this.submitted = false;
          if (error.status == 422) {
            if (this.quickFormType != 4) {
              this.modalService.open(prefixModal, { backdrop: 'static', windowClass: 'prefixForm' });
            }
          }
          let errmsg = error.error || (error.error ? error.error.errorMessage : error) || error.statusText || error;
          if (error instanceof HttpErrorResponse) {


          }

          this.selectedRole.push(this.roleData.role.uniqueId)
        }
      );
  }
  get gf() {
    let generalInfo = <FormGroup>this.basicInfoForm.controls['generalInfo'];
    return generalInfo.controls;
  }
  get f() {
    return this.basicInfoForm.controls;
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    // console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }
  initStudentbasicInfoForm() {
    this.basicInfoForm = this.formBuilder.group({
      userUniqueId: ['', Validators.nullValidator],
      // organizationUniqueId: [this.userData.orgUniqueId, Validators.required],
      // enrollmentNumber: ['', Validators.required],
      enrollmentDate: [null],
      // createdOrUpdatedByUniqueId: [this.userData.uniqueId, Validators.nullValidator],
      profileImage: ['', Validators.nullValidator],
      generalInfo: this.getGeneralInfo(),
      userId: [null],
      createdOrUpdatedById: [1]
    })
  }

  getGeneralInfo() {
    let generalInfo = {
      firstName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      lastName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      // birthDate: [null, Validators.nullValidator],
      // gender: ['', Validators.nullValidator],
      // bloodGroup: ['', Validators.nullValidator],
      // adharNumber: ['', Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      mobile: ['', Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      emailId: ['', Validators.compose([Validators.required, Validators.email])],
      // loginId: ['', Validators.required]
      // dateOfJoining: [null, Validators.nullValidator] //staff
    }
    return this.formBuilder.group(generalInfo)
  }
  initStaffInfoForm() {
    this.basicInfoForm = this.formBuilder.group({
      userUniqueId: ['', Validators.nullValidator],
      // organizationUniqueId: [this.userData.orgUniqueId, Validators.required],
      // createdOrUpdatedByUniqueId: [this.userData.uniqueId, Validators.nullValidator],
      profileImage: ['', Validators.nullValidator],
      roleId: [null, Validators.required],
      generalInfo: this.getGeneralInfo(),
      userId: [null],
      // createdOrUpdatedById: [1],
      // organizationId: [this.userData.organizationId]
    });
  }
  initParentBasicInfo() {
    this.basicInfoForm = this.formBuilder.group({
      parentUniqueId: ['', Validators.nullValidator],
      // organizationUniqueId: this.userData.orgUniqueId,
      // createdOrUpdatedByUniqueId: this.userData.uniqueId,
      profileImage: ['', Validators.nullValidator],
      generalInfo: this.parentsGeneralInfo(),
      parentId: [null],
      // organizationId: [this.userData.organizationId]
    })
  }
  parentsGeneralInfo() {
    return this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      lastName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      fullName: ['', Validators.nullValidator],
      gender: ['', Validators.nullValidator],
      bloodGroup: ['', Validators.nullValidator],
      // emailId: ['', [Validators.required, Validators.email]],
      emailId: ['', Validators.required],
      // birthDate:  ['',Validators.nullValidator],
      // phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      mobile: ['', Validators.required],
      adharNumber: ['', Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      // loginId: ['', Validators.required],
      userId: ['', Validators.nullValidator],
      schoolName: ['', Validators.nullValidator],

    });
  }
  initTeacherInfoForm() {

    this.basicInfoForm = this.formBuilder.group({
      userUniqueId: ['', Validators.nullValidator],
      // organizationUniqueId: [this.userData.orgUniqueId, Validators.required],
      // createdOrUpdatedByUniqueId: [this.userData.uniqueId, Validators.nullValidator],
      profileImage: ['', Validators.nullValidator],
      generalInfo: this.getGeneralInfo(),
      roleId: [null, Validators.required],
      userId: [null],
      // createdOrUpdatedById: [1],
      // organizationId: [this.userData.organizationId]
    });
  }
  addPrefixConfirmation() {
    this.modalReference = this.modalService.open(this.addPrefixConfirmationModal, this.ngbSmallModalOptions);
  }

  navigateToMyOrg() {
    this.router.navigate(['/organization/organization/1/myorganization']);
    setTimeout(function () {
      window.scrollTo(0, document.body.scrollHeight);
    }, 1000);
  }
  capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }
  savePerfix() {
    this.submitted = true;
    if (this.prefixForm.invalid) {
      return;
    }
    let perfix = this.prefixForm.controls.name.value;
    this.spinner.show();
    let type = (this.quickFormType == 1) ? 1 : 2;
    this._userService.addPerfix(perfix, type)
      .subscribe(res => {
        if (res && res.success) {
          this.toastService.showSuccess(res.responseMessage);
          this.modalService.dismissAll();
          this.saveUser(2, "abc");
          // this.getList.emit("1")
          // this.initForm();
          // this.submitted = false;
        } else {
          this.submitted = false;
          this.toastService.showSuccess(res.responseMessage);
        }
        this.spinner.hide();
      },
        (error: HttpErrorResponse) => {
          this.spinner.hide();
          // if (error instanceof HttpErrorResponse) {
          //   this.spinner.hide();
          // } else {
          //   this.toastService.ShowError(error);
          //   this.spinner.hide();
          // }
        }
      );
  }
  inputPerfix(event) {
    console.log(event);
    this.validMessage = "";
    this.errMessage = "";
    let perfix = this.prefixForm.controls.name.value;
    this.prefixForm.controls.name.setValue(perfix.toUpperCase());
    perfix = this.prefixForm.controls.name.value;
    if (perfix.length == 4) {
      this.spinner.show();
      this._userService.checkPerfix(perfix)
        .subscribe(res => {
          if (res) {
            if (res.success) {
              this.validMessage = "Perfix Valid";
            } else {
              this.errMessage = "Perfix Invalid";
              this.toastService.ShowWarning(res.responseMessage);
            }
          }
          this.spinner.hide();
        },
          (error: HttpErrorResponse) => {
            this.spinner.hide();
            // if (error instanceof HttpErrorResponse) {
            //   this.spinner.hide();
            // } else {
            //   this.toastService.ShowError(error);
            //   this.spinner.hide();
            // }
          }
        );
    }
    console.log(perfix, this.prefixForm);
  }
  changePerfix(event) {
    console.log(event);
  }
}
