import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import Swal from 'sweetalert2';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shift-list',
  templateUrl: './shift-list.component.html',
  styleUrls: ['./shift-list.component.scss']
})
export class ShiftListComponent implements OnInit {

  userData: any = {};
  organizationShifts = [];
  shiftLists = [];
  shiftTypeList = ["Teaching", "Non-Teaching", "Both"];
  isViewShift = false;
  isEditShift = false;
  viewIndex: any = "";
  selectedShift: any;
  colorClassArray = ['warning', 'primary', 'success', 'info', 'danger'];
  workingDayList: any;
  subscription: Subscription;
  @Output() addShift = new EventEmitter<string>();
  @Output() isViewDetails = new EventEmitter<string>();
  constructor(private spinner: NgxSpinnerService, private _shiftService: ShiftServiceService, private toastService: ToastsupportService, private _commonService: NewCommonService) {
    this.getshiftworkdayList()
    this.getOrganizationShifts();
    this.subscription = this._commonService.workTimeProcess.subscribe(type => {
      if (type == 1) {
        this.getOrganizationShifts();
      }
    })
  }

  ngOnInit() {
  }
  getOrganizationShifts() {
    this.spinner.show();
    return this._shiftService.getAllShifts().subscribe(
      res => {
        if (res) {
          // console.log('get all shifts => ' + JSON.stringify(res))
          if (res.success == true) {
            this.organizationShifts = res.list;
            for (let i = 0; i < this.organizationShifts.length; i++) {
              if (this.organizationShifts[i].days.length != null) {
                for (let j = 0; j < this.organizationShifts[i].days.length; j++) {
                  if (this.workingDayList && this.workingDayList.length != null) {
                    for (let l = 0; l < this.workingDayList.length; l++) {
                      if (this.workingDayList[l].dayValue == this.organizationShifts[i].days[j].dayValue) {
                        this.organizationShifts[i].days[j].name = this.workingDayList[l].name
                      }
                    }
                  } else {
                    this.toastService.ShowError("Organization working days not found")
                  }

                }

              }
            }
            if (this.viewIndex || this.viewIndex == 0) {
              for (let index = 0; index < this.organizationShifts.length; index++) {
                const element = this.organizationShifts[index];
                if (element.id == this.viewIndex) {
                  this.selectedShift = this.organizationShifts[index];
                  break;
                }
              }
            }
          } else {
            this.toastService.ShowWarning('shifts not found')
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  getshiftworkdayList() {
    return this._shiftService.getShiftWorkingDays().subscribe(
      res => {
        if (res) {
          // console.log(' shift working days => ' + JSON.stringify(res))
          this.workingDayList = res;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        // }
      });
  }

  createShift() {
    this.addShift.emit("");
  }
  viewShift(shift, index) {
    this.isViewShift = true;
    this.selectedShift = shift;
    this.viewIndex = shift.id;
    this.isViewDetails.emit("true");
  }
  closeViewShift() {
    this.isViewShift = false;
    this.viewIndex = "";
    this.isViewDetails.emit("false");
  }
  editShift(shift) {
    this.closeViewShift();
    this.isEditShift = true;
    this.viewIndex = shift.id;
    this.shiftLists = this.organizationShifts.filter(shift => shift.shiftUid != this.selectedShift.shiftUid);
  }
  closeEditNewForm($event) {
    this.isEditShift = false;
  }
  deleteShift(shift) {
    Swal.fire({
      position: 'center',
      title: '',
      text: 'Are you sure want to delete shift?',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
    }).then((result) => {
      // console.log('delete shift ' + JSON.stringify(shift))
      this._shiftService.deleteShift(shift.unique_id).subscribe(
        response => {
          this.toastService.showSuccess("Shift deleted successfully");
          this.spinner.hide();
          this.closeViewShift();
          this.getOrganizationShifts();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error)
          // }
          this.spinner.hide();
        });
    })
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
