import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})

export class SetupComponent implements OnInit {
  isFromSetup: boolean = true;
  subscriptionDto: any;
  gradeList = [];
  boardList = [];
  isEditAdd: boolean = false;
  state = [];
  central = [];
  secondary = [];
  primary = [];
  highSchool = [];
  masterBoardList = [];
  masterGradeList = [];
  boards = [];
  grades = [];
  subjectList = [];
  subjectLinkingSubjectForm: FormGroup;
  isAdd: boolean = false;
  data: any;
  step: number = 1;
  tab: string = "tab1";
  courseList: any[];
  courseAndRefForm: FormGroup;
  editData: any;
  selectedIndex = 0;
  isAddNew = false;
  tabOne: boolean = false;
  tabTwo: boolean = false;
  tabThree: boolean = false;
  setupDone: boolean = true;
  constructor(private _courseServiceService: CourseServiceService,
    private toastService: ToastsupportService, private router: Router,
    private _boardService: BoardServiceService, private _gradeServiceService: GradeServiceService,
    private formBuilder: FormBuilder) {
    const navigation = this.router.getCurrentNavigation();
    if (navigation) {
      const state = navigation.extras.state;
      console.log("state", state);

      if (!state) {
        this.router.navigate(['/account/signup']);
      } else {
        this.subscriptionDto = state;
        if (this.subscriptionDto.orgState) {
          this.selectedStep(this.subscriptionDto.orgState - 1)
        } else {
          this.selectedStep(1)
        }
      }
    }
  }

  ngOnInit() { }

  selectedStep(step: number) {
    this.showCompletedTabs(step);
    this.step = step;
    if (step == 1) {
      this.isEditAdd = false;
      this.addeditGrade();
      this.tab = 'tab1';
    } else if (step == 2) {
      this.initCourseAndRefForm();
      this.addCourse();
      this.tab = 'tab2';
    } else {
      this.addSubject();
      this.tab = 'tab3';
    }
  }

  getCourseList() {
    this.courseList = []
    return this._courseServiceService.getCourseDetailsedInfoList(environment.BOTH_COURSES).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            // console.log("get courseList => ",res)
            this.courseList = res.list;
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  initCourseAndRefForm() {
    this.courseAndRefForm = this.formBuilder.group({
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      courseList: this.formBuilder.array([this.initCourseList()])
    });
  }
  initCourseList() {
    return this.formBuilder.group({
      uniqueId: ['', Validators.nullValidator],
      id: [1, Validators.required],
      name: ['', Validators.required],
      isNew: [true, Validators.required],
      year: ['', Validators.required],
      month: ['', Validators.required],
      day: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      endDateValue: ['', Validators.required],
      masterCourseUniqueId: ['', Validators.required],
      academicSessionDto: ['', Validators.nullValidator],
      boardGradeList: this.formBuilder.array([this.initBoardGradeList()], Validators.required),
      alreadySelected: [[]],
      organizationId: ['', Validators.nullValidator],
      masterCourseDtoList: ['', Validators.nullValidator],
      duration: null,
      editMode: true,
      isShowDatePicker: false
    });
  }
  initBoardGradeList() {
    return this.formBuilder.group({
      uniqueId: ['', Validators.nullValidator],
      boardId: ['', Validators.nullValidator],
      gradeId: ['', Validators.nullValidator],
      boardUniqueId: ['', Validators.required],
      gradeUniqueId: ['', Validators.required],
      boardName: ['', Validators.nullValidator],
      gradeName: ['', Validators.nullValidator],
      courseUniqueId: [null, Validators.nullValidator],
      courseName: [null, Validators.nullValidator],
      boardGradeHasCourseId: ['', Validators.nullValidator]
    });
  }

  addCourse() {
    if (this.courseList == null && this.courseList == undefined) {
      this.isAddNew = true
      console.log("test")
      this.selectedIndex = 3
      this.isEditAdd = true;
      this.editData = '';
    } else {
      this.isAddNew = false
      console.log("test")
      this.selectedIndex = 6;
      this.isEditAdd = true;
      this.editData = this.courseList;
    }
  }



  addEditSubject(subject) {
    console.log('subject', subject)
    this.data = subject;
    this.isAdd = false;
    this.initLinkSubjectForm();
    this.isEditAdd = true;

  }
  addSubject() {
    this.isAdd = true;
    this.data = '';
    this.initLinkSubjectForm();
    this.isEditAdd = true;

  }
  // backPage(value){
  //   console.log("value",value)
  //   this.isEditAdd=value;
  //   this.getSujectList()
  // }

  initLinkSubjectForm() {
    this.subjectLinkingSubjectForm = this.formBuilder.group({
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      orgId: [sessionStorage.getItem('organizationId'), Validators.required],
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      linkedSubjectList: this.formBuilder.array([])
    });
  }

  getGradeList() {
    return this._gradeServiceService.getAcademicGradeList().subscribe(
      res => {
        if (res.success == true) {
          this.gradeList = [];
          console.log("getGradeBoardListres", res)
          this.gradeList = res.list;
          // this.getMasterGradeList()
          this.sortGradeList(this.gradeList)
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  getBoardList() {
    return this._gradeServiceService.getAcademicBoardList().subscribe(
      res => {
        if (res) {
          this.boardList = [];
          console.log("getGradeBoardListres", res)
          this.boardList = res;
          this.sortBoardList(this.boardList)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  getMasterBoardList() {

    this._boardService.getMasterBoardList().subscribe(
      res => {
        if (res) {
          console.log("oard res", res)
          this.masterBoardList = res;
          if (this.boardList.length > 0) {
            for (let i = 0; i < this.boardList.length; i++) {
              for (let j = 0; j < this.masterBoardList.length; j++) {
                if (this.boardList[i].name == this.masterBoardList[j].name) {
                  this.boards.push(this.masterBoardList[j].uniqueId)
                }
              }
            }
          }
          console.log("this.boards", this.boards)

        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  getMasterGradeList() {

    this._gradeServiceService.getMasterGradeList().subscribe(
      res => {
        if (res) {
          console.log("grade res", res)
          this.masterGradeList = res;
          if (this.gradeList.length > 0) {
            for (let i = 0; i < this.gradeList.length; i++) {
              for (let j = 0; j < this.masterGradeList.length; j++) {
                if (this.gradeList[i].name == this.masterGradeList[j].name) {
                  this.grades.push(this.masterGradeList[j].uniqueId)
                }
              }
            }
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  addeditGrade() {
    this.isEditAdd = true;
  }

  sortGradeList(list: any) {
    list.forEach(element => {
      if (element.name == '12th' || element.name == '11th') {
        this.secondary.push(element);
      } else if (element.name == '10th' || element.name == '9th' || element.name == '6' || element.name == '7' || element.name == '8') {
        this.highSchool.push(element);
      } else if (element.name == '1' || element.name == '2' || element.name == '3' || element.name == '4' || element.name == '5') {
        this.primary.push(element);
      }
    });

    console.log("secondary", this.secondary)
    console.log("highSchool", this.highSchool)
    console.log("primary", this.primary)
  }

  sortBoardList(list: any) {
    list.forEach(element => {
      if (element.name == 'CBSE' || element.name == 'ICSE' || element.name == 'IB') {
        this.central.push(element);
      } else {
        this.state.push(element);
      }
    });

    console.log("state", this.state)
    console.log("central", this.central)
  }

  backPage(value) {
    console.log("value", value)
    this.isEditAdd = value;
  }

  showCompletedTabs(step: number) {
    if (step == 1) {
      this.tabOne = false;
      this.tabTwo = false;
      this.tabThree = false;
    } else if (step == 2) {
      this.getBoardList();
      this.getGradeList();
      this.tabOne = true;
      this.tabTwo = false;
      this.tabThree = false;
    } else if (step == 3) {
      this.getCourseList();
      this.tabOne = true;
      this.tabTwo = true;
      this.tabThree = false;
    } else if (step == 4) {
      this.tabOne = true;
      this.tabTwo = true;
      this.tabThree = true;
    }
  }

  setupComplete() {
    //4 means setup done
    this.showCompletedTabs(4);
    this.setupDone = true;
  }

}
