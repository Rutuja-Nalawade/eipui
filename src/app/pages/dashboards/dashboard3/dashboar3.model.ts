export interface Role {
    id: number;
    uniqueId: string;
    name: string;
    createdOrUpdatedBy: string;
    description: string;
    isCustomized: string;
    orgUniqueId: string;
}

export interface Department {
    id: number;
    uniqueId: string;
    name: string;
    createdOrUpdatedBy: string;
    orgUniqueId: string;
    status: string;
}

export interface Widget {
    title: string;
    value: number;
    text: string;
    revenue: string;
}

export interface Inbox {
    image: string;
    name: string;
    message: string;
}

export interface Chat {
    image: string;
    name: string;
    message: string;
    time: string;
}

export interface Todo {
    id: number;
    text: string;
    done: boolean;
}

// Chart data
export interface ChartType {
    chart?: any;
    plotOptions?: any;
    colors?: any;
    series?: any;
    stroke?: any;
    fill?: any;
    labels?: any;
    markers?: any;
    legend?: any;
    xaxis?: any;
    yaxis?: any;
    tooltip?: any;
    grid?: any;
    datasets?: any;
    options?: any;
}

export interface DepartmentHead {
    name?: string;
    designation?: string;
    departments?: Array<string>;
    email?: string;
    image?: string;
}


export interface TreeNode {
    name?: string;
    head?: Array<DepartmentHead>;
    icon?: any;
    expandedIcon?: any;
    collapsedIcon?: any;
    children?: TreeNode[];
    leaf?: boolean;
    expanded?: boolean;
    type?: string;
    parent?: TreeNode;
    partialSelected?: boolean;
    styleClass?: string;
    draggable?: boolean;
    droppable?: boolean;
    selectable?: boolean;
    typeId?: number;
    key?: string;

}

export interface AutoCompleteModel {
    value: number;
    display: string;
    uniqueId: string;
}

export interface Dropdown {
    color: string;
}

export interface Batches {
    orgUniqueId: string;
    createdOrUpdatedBy: string;
    batchList: Array<CourseDto>;
    orgId:number;
}

export interface CourseDto {
    courseId: string;
    courseName: string;
    batchList: Array<CommonDto>;
}

export interface CourseVo {
    uniqueId: string;
    name: string;
    boardGradeListDto: Array<BoardGradeDto>;
}

export interface BoardGradeDto {
    uniqueId: string;
    boardUniqueId: string;
    gradeUniqueId: string;
    gradeName: string;
    boardName: string;
    courseUniqueId: string;
    courseName: string;
}

export interface CommonDto {
    id: number;
    uniqueId: string;
    name: string;
    isNew: boolean;
    courseId: string;
    isLinked: boolean;
    subjectId: string;
    createdOrUpdatedBy: string;
}

export interface Subjects {
    orgUniqueId: string;
    orgId : number;
    createdOrUpdatedBy: string;
    courseId: string;
    courseName: string;
    uniqueId: string;
    isFromEip: boolean;
    subjectList: Array<CommonDto>;
    compulsorySubjectList: Array<CommonDto>;
    optionalSubjectList: Array<CommonDto>;
    electiveSubjectList: Array<CommonDto>;
    remainingSubjectList: Array<CommonDto>;
}

export interface LinkedSubjects {
    orgUniqueId: string;
    createdOrUpdatedBy: string;
    linkedSubjectList: Array<Subjects>;
}

export interface FilterDto {
    orgList: Array<any>;
    courseList: Array<any>;
    boardList: Array<any>;
    gradeList: Array<any>;
    batchList: Array<any>;
    roleList: Array<any>;
    pageIndex: any;
    subjectList: Array<any>;
    genderList: Array<any>;
    isForAll: Boolean;
    searchType: number;
    organizationUid: string;
}

export interface TTEntityDto {
    id: string;
    name: string;
    shortName: string;
}

export interface PeriodDto {
    timetableId: any;
    id: any;
    period: any;
    name: any;
    dayWiseCards: Array<DayWiseCardDto>;
}

export interface DayWiseCardDto {
    id: any;
    period: any;
    cards: Array<Card>;
}

export interface Card {
    id: any;
    period: any;
}

export interface TimetableDto {
    id: any;
    name: any;
    periodList: Array<PeriodDto>;
}