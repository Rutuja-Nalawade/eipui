import { CUSTOM_ELEMENTS_SCHEMA,NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewexamRoutingModule } from './newexam-routing.module';
import { TruncatePipePipe } from './truncate-pipe.pipe';
import { DragDropDirective } from './drag-drop.directive';
import { AddPatternComponent } from './add-pattern/add-pattern.component';
import { AddTracksComponent } from './add-tracks/add-tracks.component';
import { BatchdetailsComponent } from './batchdetails/batchdetails.component';
import { CommonExamHeaderComponent } from './common-exam-header/common-exam-header.component';
import { CreatePatternedExamComponent } from './create-patterned-exam/create-patterned-exam.component';
import { CreateQuickExamComponent } from './create-quick-exam/create-quick-exam.component';
import { CreatepatternexamviewComponent } from './createpatternexamview/createpatternexamview.component';
import { ExamListComponent } from './exam-list/exam-list.component';
import { ExamSummaryComponent } from './exam-summary/exam-summary.component';
import { ExamsyllabusComponent } from './examsyllabus/examsyllabus.component';
import { ExamviewComponent } from './examview/examview.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { SchemeComponent } from './scheme/scheme.component';
import { PatternviewComponent } from './patternview/patternview.component';
import { QuestionBankComponent } from './question-bank/question-bank.component';
import { TimedetailsComponent } from './timedetails/timedetails.component';
import { TrackInfoComponent } from './track-info/track-info.component';
import { TrackSyllabusComponent } from './track-syllabus/track-syllabus.component';
import { ViewPatternComponent } from './view-pattern/view-pattern.component';
import { ViewPatternSummaryComponent } from './view-pattern-summary/view-pattern-summary.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgbAccordionModule, NgbCollapseModule, NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { ArchwizardModule } from 'angular-archwizard';
import { NgxEditorModule } from 'ngx-editor';
import { NgxSpinnerModule } from "ngx-spinner";
import { BsDatepickerModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AlertModule, TooltipModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChartsModule } from 'ng2-charts';
import {NgbProgressbarModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { PublishExamComponent } from './publish-exam/publish-exam.component';

@NgModule({
  declarations: [TruncatePipePipe, DragDropDirective, AddPatternComponent, AddTracksComponent, BatchdetailsComponent, CommonExamHeaderComponent, CreatePatternedExamComponent, CreateQuickExamComponent, CreatepatternexamviewComponent, ExamListComponent, ExamSummaryComponent, ExamsyllabusComponent, ExamviewComponent, FileuploadComponent, SchemeComponent, PatternviewComponent, QuestionBankComponent, TimedetailsComponent, TrackInfoComponent, TrackSyllabusComponent, ViewPatternComponent, ViewPatternSummaryComponent, PublishExamComponent],
  imports: [
    CommonModule,
    NewexamRoutingModule,
    TooltipModule.forRoot(),
    AlertModule.forRoot(),
    UiSwitchModule,
    NgbTabsetModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    SharedModule,
    NgbDropdownModule,
    NgbProgressbarModule,
    NgbModule,
    NgbTypeaheadModule,
    ChartsModule,
    NgbCollapseModule,
    NgbAccordionModule,
    ArchwizardModule,
    NgxEditorModule,
    NgxSpinnerModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [ SchemeComponent, TimedetailsComponent],
  bootstrap: [TimedetailsComponent]
})
export class NewexamModule { }
