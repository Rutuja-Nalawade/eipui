import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { NewSetupService } from 'src/app/servicefiles/new-setup.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-board-and-grade',
  templateUrl: './board-and-grade.component.html',
  styleUrls: ['./board-and-grade.component.scss']
})
export class BoardAndGradeComponent implements OnInit {

  userData:any;
  // boardAndGradeForm: FormGroup;
  loading:boolean= false;
  isEdit:boolean= false;
  submitted:boolean= false;
  masterBoardData:any=[];
  masterGradeData:any=[];
  selectedBoards:any=[];
  selectedGrades:any=[];
  alreadySelectedGrades:any=[];
  alreadySelectedBords:any=[];
  oldSelectedGrades:any=[];
  oldSelectedBoards:any=[];
  settings:any;
  isNew:boolean= false;
  @Input() selectedIndex:any;
  @Input() boardAndGradeForm:FormGroup;
  constructor(private _commonService:NewCommonService, private _setupService:NewSetupService, private formBuilder:FormBuilder, private toastService:ToastsupportService) {
    this.userData = this._commonService.getUserData();
  }

  ngOnInit() {
    // this.initBoardGradeForm();
    this.settings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: false,
      enableCheckAll: false
    };
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedIndex'] && changes['selectedIndex'].currentValue== 0) {
      this.getBoardGradeData(); 
    }
  }

  initBoardGradeForm(){
    this.boardAndGradeForm = this.formBuilder.group({
      organizationUid: [this.userData.orgUniqueId, Validators.required],
      boardsList: [[],Validators.required],
      gradesList: [[],Validators.required],
    });
  }
  initDto(){
    return this.formBuilder.group({
      id: [null, Validators.nullValidator],
      name: [null, Validators.required],
      organizationId: [null, Validators.nullValidator],
      isLinked: [null, Validators.nullValidator],
      isNew: [true, Validators.required],
      universalId: [null, Validators.nullValidator],
    });
  }
  getBoardGradeData(){
    this.loading = true;
    this._setupService.getBoardGradeList(this.userData.organizationId).subscribe(result => {
      if (!result || result.errorCode) {

      } else {
        if (result != null && result.boardList != null && result.boardList.length > 0) {  
          this.alreadySelectedBords = result.boardList;
          this.patchBoardList(result.boardList);
          this.getBoardListFromMasterOrg();
        }else{
          this.isNew=true;
          this.isEdit=true;
          this.getBoardListFromMasterOrg();
          //this.boardAndGradeForm.setControl('boardsList', this.formBuilder.array([]));
        }
        if (result != null && result.gradeList != null && result.gradeList.length > 0) { 
          this.alreadySelectedGrades = result.gradeList;
          this.patchGradeList(result.gradeList); 
          this.getGradeListFromMasterOrg();  
        }else{
          this.isNew=true;
          this.isEdit=true;
          this.getGradeListFromMasterOrg();
          //this.boardAndGradeForm.setControl('boardsList', this.formBuilder.array([]));
        }
      }
      this.loading = false;
    },
    (error: HttpErrorResponse) => {
      // if (error instanceof HttpErrorResponse) {

      // } else {
      //   this.loading = false;
      //   this.toastService.ShowWarning(error);
      // }
      this.loading = false;
    });
  }
  patchBoardList(listData){
    let boardsList1 = [];
    let boardsList= this.boardAndGradeForm.get("boardsList") as FormControl
    for (let i = 0; i < listData.length; i++) {
      let data = listData[i];
      let listGroup = {
        id:data.id,
        name: data.name,
        // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId')]
      }
      
      boardsList1.push(listGroup);
    }
    //console.log("set",boardsList1);
    boardsList.setValue(boardsList1);
    //console.log("boardAndGradeForm",this.boardAndGradeForm);
  }
  patchGradeList(listData){
    let gradesList = [];
    let list= this.boardAndGradeForm.get("gradesList") as FormControl
    for (let i = 0; i < listData.length; i++) {
      let data = listData[i];
      let listGroup =  {
        id:data.id,
        name: data.name,
        // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId')]
      }
      gradesList.push(listGroup);
    }
    //console.log("set",gradesList);
    list.setValue(gradesList);
    //console.log("boardAndGradeForm",this.boardAndGradeForm);
    
  }
  getBoardListFromMasterOrg(){
    this.loading = true;    
    this._setupService.getBoardListFromMasterOrg().subscribe(
    async  res => {
        if (res) {
          // this.masterBoardData = res.boardsList;
          for (let i = 0; i < res.boardsList.length; i++) {
            let gData = res.boardsList[i];
            if (this.alreadySelectedBords.length > 0) {
              for (let j = 0; j < this.alreadySelectedBords.length; j++) {
                let data = this.alreadySelectedBords[j];
                if (data.name==gData.name) {
                    gData.isLinked =true
                    gData.alreadySelected=true
                    gData.id=data.id;
                    res.boardsList[i]= gData;
                  this.selectedBoards.push({id:data.id,name:gData.name,isLinked :true,alreadySelected :true});
                  break;
                }else{
                  gData.isLinked=false
                  gData.alreadySelected=false
                }
              }
            }else{
              gData.isLinked=false
              gData.alreadySelected=false
            }
                             
          }
          // this.Grades = res.gradeDtoList;
          this.masterBoardData = res.boardsList;
          this.loading = false;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });
  }
  getGradeListFromMasterOrg(){
    this.loading = true;    
    this._setupService.getGradeListFromMasterOrg().subscribe(
      res => {
        if (res) {
          // this.masterGradeData = res.gradeDtoList;
            for (let i = 0; i < res.gradeDtoList.length; i++) {
              let gData = res.gradeDtoList[i];
              if (this.alreadySelectedGrades.length > 0) {
                for (let j = 0; j < this.alreadySelectedGrades.length; j++) {
                  let data = this.alreadySelectedGrades[j];
                  if (data.name==gData.name) {
                    gData.isLinked =true
                    gData.alreadySelected=true
                    gData.id=data.id;
                    res.gradeDtoList[i]= gData;
                    this.selectedGrades.push({id:data.id,name:gData.name,isLinked :true,alreadySelected :true});
                    break;
                  }else{
                    gData.isLinked=false
                    gData.alreadySelected=false
                  }
                }
              }else{
                gData.isLinked=false
                gData.alreadySelected=false
              }
                               
            }
            // this.Grades = res.gradeDtoList;
            this.masterGradeData = res.gradeDtoList;
            //console.log(this.selectedGrades);
            
          this.loading = false;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });

  }

  onSubmit(){
    this.submitted=true
    if (this.boardAndGradeForm.invalid) {
      this.toastService.ShowWarning("Atleast select one board and grade.")
      return;
    }
    this.loading = true;
    let boardAndGradeData={
      organizationId: this.userData.organizationId,
      boardList:this.selectedBoards,
      gradeList:this.selectedGrades,
    }
    //console.log(boardAndGradeData);
    this._setupService.saveBoardGradeData(boardAndGradeData).subscribe(res => {
      //console.log('res data===>>>', res);
      if (res) {
        this.toastService.showSuccess("Board & Garde saved successfully");
        
        var button1 = document.getElementById("nextBtn1");
        button1.click();
        this.submitted = false;
      }
      this.loading = false;
    },(error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowError(error);
        // }
        this.loading = false;
    })
    
  }
  addNewRow(){
    let boardList= this.boardAndGradeForm.get("boardsList") as FormControl
    this.oldSelectedBoards= boardList.value
    let gradeList= this.boardAndGradeForm.get("gradesList") as FormControl
    this.oldSelectedGrades= gradeList.value
    this.isEdit=true;
  }
  editRow(){
    let boardList= this.boardAndGradeForm.get("boardsList") as FormControl
    this.oldSelectedBoards= boardList.value
    let gradeList= this.boardAndGradeForm.get("gradesList") as FormControl
    this.oldSelectedGrades= gradeList.value
    this.isEdit=true;
    // this.isNew=true;
  }
  deleteRow(){

  }
  cancelRow(){
    this.isEdit=false;
    // this.isNew=false;
    //console.log("this.oldSelectedGrades",this.oldSelectedGrades);
    
    if (this.oldSelectedGrades.length>0) {
      let gradeList= this.boardAndGradeForm.get("gradesList") as FormControl;
      gradeList.setValue(this.oldSelectedGrades);
      this.oldSelectedGrades=[];
    }
    if (this.oldSelectedBoards.length>0) {
      let boardsList= this.boardAndGradeForm.get("boardsList") as FormControl;
      boardsList.setValue(this.oldSelectedBoards)
      this.oldSelectedBoards=[];
    }
  }
  updateRow(){
    this.submitted=true
    //console.log(this.boardAndGradeForm);
    
    if (this.boardAndGradeForm.invalid) {
      // if (this.boardAndGradeForm.controls.boardList.invalid) {
      //   this.toastService.ShowWarning("Atleast select one board.")
      // }
      // if (this.boardAndGradeForm.controls.gradeList.invalid) {
      //   this.toastService.ShowWarning("Atleast select one grade.")
      // }
      return;
    }
    this.submitted=false
    this.isEdit=false;
    // this.isNew=false;
  }
  OnItemSelect(event, type){
    // console.log(event);
    // let formcontrol= this.boardAndGradeForm.get("boardsList") as FormControl
    // console.log("formcontrol",formcontrol);
    this.isNew=true;
    this.isEdit=true;
    let isSelectMetch= false;
    if (type==1) {
      if (this.selectedBoards.length > 0) {
        for (let index = 0; index < this.selectedBoards.length; index++) {
          const element = this.selectedBoards[index];
          if (element.name == event.name) {
            isSelectMetch= true;
            this.selectedBoards[index].isLinked= element.isLinked ? false : true;
            break;
          }
        }
      }else{
        delete event.id;
        event.isLinked= true;
        this.selectedBoards.push(event);
      }
    } else {
      if (this.selectedGrades.length > 0) {
        for (let index = 0; index < this.selectedGrades.length; index++) {
          const element = this.selectedGrades[index];
          if (element.name == event.name) {
            isSelectMetch= true;
            this.selectedGrades[index].isLinked= element.isLinked ? false : true;
            break;
          }
        }
      }else{
        delete event.id;
        event.isLinked= true;
        this.selectedGrades.push(event);
      }
    }    
    if(!isSelectMetch){
      delete event.id;
      event.isLinked= true;
      if (type==1) {
        this.selectedBoards.push(event);
      } else {
        this.selectedGrades.push(event);        
      }
    }
  }
  OnItemDeSelect(event, type){
    let isSelectMetch= false;
    let matchIndex= null;
    if (type==1) {
      for (let index = 0; index < this.selectedBoards.length; index++) {
        const element = this.selectedBoards[index];
        if (element.name == event.name) {
          matchIndex= index;
          if (element.alreadySelected) {
            this.selectedBoards[index].isLinked= element.isLinked ? false : true;
          }else{
            isSelectMetch= true;
          }
          break;
        }
      }
    } else {
      for (let index = 0; index < this.selectedGrades.length; index++) {
        const element = this.selectedGrades[index];
        if (element.name == event.name) {
          isSelectMetch= true;
          matchIndex= index;
          if (element.alreadySelected) {
            this.selectedGrades[index].isLinked= element.isLinked ? false : true;
          }
          break;
        }
      }
    }
    if(isSelectMetch){
      if (type==1) {
        this.selectedBoards.splice(matchIndex,1)
      } else {
        this.selectedGrades.splice(matchIndex,1)        
      }
    }
  }
  get f() { return this.boardAndGradeForm.controls; }
}
