import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { CourseplannerServiceService } from 'src/app/servicefiles/courseplanner-service.service';
declare var $: any;

@Component({
  selector: 'app-course-planner-details',
  templateUrl: './course-planner-details.component.html',
  styleUrls: ['./course-planner-details.component.scss']
})
export class CoursePlannerDetailsComponent implements OnInit {


  unitList = [];
  batchDataList = [];
  selectedUnit: any;
  selectedChapter: any = 0;
  selectedUnitData: any;
  selectedTopicData: any;
  selectedLectureData: any = [{ lectureName: "Lecture 1", duration: "2hr 10min", topicName: "Introduction to Vectors", status: "2", startDate: "07/04/2021", completionStatus: "On time", subtopics: "3", remaingSubtopics: "", percentage: "" }, { lectureName: "Lecture 2", duration: "1hr 10min", topicName: "Scalar Product, Vector Product", status: "1", startDate: "07/04/2021", completionStatus: "", subtopics: "4", remaingSubtopics: "2", percentage: "50" }, { lectureName: "Lecture 3", duration: "1hr 20min", topicName: "Vectors and Three Dimensional Geometry", status: "0", startDate: "07/04/2021", completionStatus: "", subtopics: "4", remaingSubtopics: "", percentage: "" }, { lectureName: "Lecture 4", duration: "2hr 30min", topicName: "Scalar Product, Vector Product", status: "0", startDate: "07/04/2021", completionStatus: "", subtopics: "3", remaingSubtopics: "", percentage: "" }, { lectureName: "Lecture 5", duration: "1hr 30min", topicName: "Introduction to Vectors", status: "0", startDate: "07/04/2021", completionStatus: "", subtopics: "3", remaingSubtopics: "", percentage: "" }, { lectureName: "Lecture 6", duration: "2hr 40min", topicName: "Vectors and Three Dimensional Geometry", status: "0", startDate: "07/04/2021", completionStatus: "", subtopics: "2", remaingSubtopics: "", percentage: "" }];
  totalDuration = 0;
  totalTopicDuration = "0 hr 0 mins";
  isTopicTab = true;
  isSubTopicTab = false;
  isLectureSubTopicTab = false;
  subTopicsData: any = [];
  lectSubTopicsData: any = [];
  subTopicsDuration: any;
  lectSubTopicsDuration: any;
  iseditPlanner = false;
  batchPlannerDetails = [];
  batchPlannerData = [];
  topicData: any;
  @Input() public coursePlannerId: any;
  @Input() public batchPlannerId: any;
  @Input() public plannerType: any;
  @Input() public coursePlannerForm: FormGroup;
  @Input() public coursePlannerName: any;
  @Input() public coursePlannerSubject: any;
  @Input() public totalCPDuration: any;
  @Input() public batchUniqueId: any;
  @Input() public importSubjectId: any;
  @Output() closePlannerDetails = new EventEmitter<string>();
  constructor(private _batchService: BatchesServiceService, private toastService: ToastsupportService, private spinner: NgxSpinnerService, private _cpService: CourseplannerServiceService) { }
  allData: any;
  isEditedFormData:boolean = false;
  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {

    if (changes['coursePlannerId'] && changes['plannerType']) {
      switch(changes['plannerType'].currentValue){
        case 1:
             console.log("unique id=>",this.coursePlannerId)
          this.getCoursePlannerUnitsWithFirstCompleteUnit(this.coursePlannerId);
          this.getBatchList(this.coursePlannerId);
          break;
        case 2:
          console.log("unique id=>",this.coursePlannerId)
          this.getBatchPlanner(this.batchPlannerId)
          this.coursePlannerForm.controls['syllabus'].setValue(this.coursePlannerName);
          this.coursePlannerForm.controls['subjectName'].setValue(this.coursePlannerSubject);
          break;
        case 3:
          console.log("unique id=>",this.coursePlannerId)
          this.getMasterCoursePlanner(this.coursePlannerId);
          break;
        case 4:
            console.log("condition 4 unique id=>",this.coursePlannerId)
            console.log("batch 4 unique id=>",this.batchUniqueId)
            this.getCoursePlannerToAssignBatch(this.coursePlannerId);
            break;
      }
    }
    window.scroll(0, 0);
    // this.progrssProcess()
  }
  getCoursePlannerUnitsWithFirstCompleteUnit(id: any) {
    this.spinner.show();
    this._cpService.getCoursePlannerUnitsWithFirstCompleteUnit(id).subscribe(syllabusData => {
      this.allData = syllabusData;
      // console.log('syllabusData', JSON.stringify( this.allData))
      if (syllabusData.units && syllabusData.units.length > 0) {
        let temp = [];
        syllabusData.units.forEach(element => {
          temp.push({ text: element.unitDetails.name, id: element.id })
        });
        this.unitList = temp;

        this.selectedUnit = syllabusData.units[0].id
        this.selectedUnitData = syllabusData.units[0]
        // this.selectedUnitData = syllabusData.items[0]
        let temp2 = [];
        let totaldration = 0
        if (syllabusData.units[0].chapters && syllabusData.units[0].chapters.length > 0) {
          syllabusData.units[0].chapters.forEach(element => {
            if(element.topics != undefined){
            element.topics.forEach(element => {
              temp2.push({
                subtopics: element.subtopics,
                topicDetails: element.topicDetails,
                isSelected: false,
              });
              totaldration = totaldration + element.topicDetails.duration
            });
          }
          });
        }
        this.selectedTopicData = temp2;
        console.log('subtopic', JSON.stringify(this.selectedTopicData[0]))
        if(this.selectedTopicData[0] != undefined){
          if(this.selectedTopicData[0].subtopics != undefined){
            this.subTopicsData = this.selectedTopicData[0].subtopics
            console.log('calculate subtopic duration on first time');
          }
          this.selectedTopicData[0].isSelected = true
        }
       
        this.totalTopicDuration = this.format(totaldration)
      }
      this.spinner.hide()
    }, (error) => {
    //  this.toastService.ShowError(error);
      this.spinner.hide()
    })
  }
  getMasterCoursePlanner(id: any) {
    this.spinner.show();
    this._cpService.getMasterCoursePlanner(id).subscribe(syllabusData => {
      this.allData = syllabusData;
      // console.log('master course planner', JSON.stringify( this.allData))
      if (syllabusData.units && syllabusData.units.length > 0) {
        let temp = [];
        syllabusData.units.forEach(element => {
          temp.push({ text: element.unitDetails.name, id: element.id, data: element })
        });
        this.unitList = temp;

        this.selectedUnit = syllabusData.units[0].id
        this.selectedUnitData = syllabusData.units[0]
        // this.selectedUnitData = syllabusData.items[0]
        let temp2 = [];
        let totaldration = 0
        if (syllabusData.units[0].chapters && syllabusData.units[0].chapters.length > 0) {
          syllabusData.units[0].chapters.forEach(element => {
            if(element.topics != undefined ){
              element.topics.forEach(element => {
                temp2.push({
                  subtopics: element.subtopics,
                  topicDetails: element.topicDetails,
                  isSelected: false,
                });
                totaldration = totaldration + element.topicDetails.duration
              });
            }
          });
        }
        this.selectedTopicData = temp2;
        this.subTopicsData = this.selectedTopicData[0].subtopics
        this.selectedTopicData[0].isSelected = true
        this.totalTopicDuration = this.format(totaldration)
      }
      this.spinner.hide()
    }, (error) => {
    //  this.toastService.ShowError(error);
      this.spinner.hide()
    })
  }
  getCoursePlannerToAssignBatch(id: any) {
    //call this if data is not return from edit section
    if(this.isEditedFormData == false){
      this.spinner.show();
      this._cpService.getCoursePlannerDetails(id).subscribe(syllabusData => {
        this.allData = syllabusData;
        // console.log('master course planner', JSON.stringify( this.allData))
        if (syllabusData.units && syllabusData.units.length > 0) {
          let temp = [];
          syllabusData.units.forEach(element => {
            temp.push({ text: element.unitDetails.name, id: element.id, data: element })
          });
          this.unitList = temp;
  
          this.selectedUnit = syllabusData.units[0].id
          this.selectedUnitData = syllabusData.units[0]
          // this.selectedUnitData = syllabusData.items[0]
          let temp2 = [];
          let totaldration = 0
          if (syllabusData.units[0].chapters && syllabusData.units[0].chapters.length > 0) {
            syllabusData.units[0].chapters.forEach(element => {
              if(element.topics != undefined ){
                element.topics.forEach(element => {
                  temp2.push({
                    subtopics: element.subtopics,
                    topicDetails: element.topicDetails,
                    isSelected: false,
                  });
                  totaldration = totaldration + element.topicDetails.duration
                });
              }
            });
          }
          this.selectedTopicData = temp2;
          if(this.selectedTopicData[0].subtopics != undefined){
            this.subTopicsData = this.selectedTopicData[0].subtopics
          }
          this.selectedTopicData[0].isSelected = true
          this.totalTopicDuration = this.format(totaldration)
        }
        this.spinner.hide()
      }, (error) => {
     //   this.toastService.ShowError(error);
        this.spinner.hide()
      })
    }
  }
  getBatchPlanner(batchId: any) {
    console.log('this.batchPlannerId' + this.batchPlannerId)
    this.spinner.show();
    this._cpService.getBatchPlannerUnitsWithFirstCompleteUnit(batchId).subscribe(syllabusData => {
      this.allData = syllabusData;
      // console.log('syllabusData', JSON.stringify( this.allData))
      if (syllabusData.units && syllabusData.units.length > 0) {
        let temp = [];
        syllabusData.units.forEach(element => {
          temp.push({ text: element.unitDetails.name, id: element.id })
        });
        this.unitList = temp;

        this.selectedUnit = syllabusData.units[0].id
        this.selectedUnitData = syllabusData.units[0]
        // this.selectedUnitData = syllabusData.items[0]
        let temp2 = [];
        let totaldration = 0
        if (syllabusData.units[0].chapters && syllabusData.units[0].chapters.length > 0) {
          syllabusData.units[0].chapters.forEach(element => {
            if(element.topics != undefined){
              element.topics.forEach(element => {
                temp2.push({
                  subtopics: element.subtopics,
                  topicDetails: element.topicDetails,
                  isSelected: false,
                });
                totaldration = totaldration + element.topicDetails.duration
              });
            }
            
          });
        }
        this.selectedTopicData = temp2;
        console.log('selected topic data anand =>' + JSON.stringify(this.selectedTopicData))
        this.subTopicsData = this.selectedTopicData[0].subtopics
        this.selectedTopicData[0].isSelected = true
        this.totalTopicDuration = this.format(totaldration)
      }
      this.spinner.hide()
    }, (error) => {
      //this.toastService.ShowError(error);
      this.spinner.hide()
    })
  }
  close(type) {
    if (type == "2") {
      this.closePlannerDetails.emit("")
    } else {
      this.iseditPlanner = false;
      if (this.plannerType == 1) {
        this.getCoursePlannerUnitsWithFirstCompleteUnit(this.coursePlannerId);
        this.getBatchList(this.coursePlannerId);
      } else if(this.plannerType == 2){
        this.getBatchPlanner(this.batchPlannerId)
        this.coursePlannerForm.controls['syllabus'].setValue(this.coursePlannerName);
        this.coursePlannerForm.controls['subjectName'].setValue(this.coursePlannerSubject);
      }
    }
    window.scroll(0, 0);
  }
  editPlanner() {
    this.iseditPlanner = true
    this.coursePlannerForm.controls['name'].setValue(this.coursePlannerName);
    this.coursePlannerForm.controls['subjectName'].setValue(this.coursePlannerSubject);
  }
  changeUnit(event) {
    console.log('on unit change data', JSON.stringify(event))
    this.selectedUnitData = [];
    this.selectedChapter = [];
    this.selectedTopicData = [];
    let initArray = [];
    initArray.push(event.id)

    switch (this.plannerType) {
      case 1:
        console.log('this is course planner')
        this.spinner.show();
        this._cpService.getCoursePlannerUnitsWiseDetails(initArray, this.coursePlannerId).subscribe(res => {
          console.log('course planner unit dteails', JSON.stringify(res))
          if (res.success == true) {
            if (res.list && res.list.length > 0) {
              this.selectedUnit = res.list[0].id
              this.selectedUnitData = res.list[0]
              // this.selectedUnitData = syllabusData.items[0]
              let temp2 = [];
              let totaldration = 0
              if (res.list[0].chapters && res.list[0].chapters.length > 0) {
                res.list[0].chapters.forEach(element => {
                  if(element.topics != undefined){
                    element.topics.forEach(element => {
                    
                      temp2.push({
                        subtopics: element.subtopics,
                        topicDetails: element.topicDetails,
                        isSelected: false,
                      });
                      totaldration = totaldration + element.topicDetails.duration
                    });
                  }
                 
                });
                
              }
              this.selectedTopicData = temp2;
              this.subTopicsData = this.selectedTopicData[0].subtopics
              // calculate subtopic duration
           
              this.selectedTopicData[0].isSelected = true
              this.totalTopicDuration = this.format(totaldration)
            }

          } else {
            this.toastService.ShowWarning('No data found related to this unit')
          }
          this.spinner.hide()
        }, (error) => {
        //  this.toastService.ShowError(error);
          this.spinner.hide()
        })
        break
      case 2:
        console.log('this is batch planner' + this.batchPlannerId)
        this.spinner.show();
        this._cpService.getBatchPlannerUnitsWiseDetails(initArray, this.batchPlannerId).subscribe(res => {
          console.log('batch planner unit dteails', JSON.stringify(res))
          if (res.success == true) {
            if (res.list && res.list.length > 0) {

              this.selectedUnit = res.list[0].id
              this.selectedUnitData = res.list[0]
              // this.selectedUnitData = syllabusData.items[0]
              let temp2 = [];
              let totaldration = 0
              if (res.list[0].chapters && res.list[0].chapters.length > 0) {
                res.list[0].chapters.forEach(element => {
                  element.topics.forEach(element => {
                    temp2.push({
                      subtopics: element.subtopics,
                      topicDetails: element.topicDetails,
                      isSelected: false,
                    });
                    totaldration = totaldration + element.topicDetails.duration
                  });
                });
              }
              this.selectedTopicData = temp2;
              this.subTopicsData = this.selectedTopicData[0].subtopics
              this.selectedTopicData[0].isSelected = true
              this.totalTopicDuration = this.format(totaldration)
            }

          } else {
            this.toastService.ShowWarning('No data found related to this unit')
          }
          this.spinner.hide()
        }, (error) => {
       //   this.toastService.ShowError(error);
          this.spinner.hide()
        })
        break
      case 3:
        this.selectedUnit = event.data.id
        this.selectedUnitData = event.data
        let temp2 = [];
        let totaldration = 0
        if (event.data.chapters != null && event.data.chapters.length > 0) {
          event.data.chapters.forEach(element => {
            if(element.topics != undefined ){
              element.topics.forEach(element => {
                temp2.push({
                  subtopics: element.subtopics,
                  topicDetails: element.topicDetails,
                  isSelected: false,
                });
                totaldration = totaldration + element.topicDetails.duration
              });
            }
          });
        }
        this.selectedTopicData = temp2;
        this.subTopicsData = this.selectedTopicData[0].subtopics
        this.selectedTopicData[0].isSelected = true
        this.totalTopicDuration = this.format(totaldration)
      break;
      case 4:
        this.selectedUnit = event.data.id
        this.selectedUnitData = event.data
        let temp23= [];
        let totaldration4 = 0
        if (event.data.chapters != null && event.data.chapters.length > 0) {
          event.data.chapters.forEach(element => {
            if(element.topics != undefined ){
              element.topics.forEach(element => {
                temp23.push({
                  subtopics: element.subtopics,
                  topicDetails: element.topicDetails,
                  isSelected: false,
                });
                totaldration4 = totaldration4 + element.topicDetails.duration
              });
            }
          });
        }
        this.selectedTopicData = temp23;
        this.subTopicsData = this.selectedTopicData[0].subtopics
        this.selectedTopicData[0].isSelected = true
        this.totalTopicDuration = this.format(totaldration4)
      break;
    }
  }
  selectTopic(chapter) {
    console.log('chapter' + JSON.stringify(chapter))
    this.subTopicsData = [];
    this.selectedTopicData = [];
    this.selectedTopicData = chapter.topics;
  }
  getBatchList(courseplannerId: number) {

    return this._batchService.getCoursePlannerAssignedBatchList(courseplannerId).subscribe(
      res => {
        console.log('batches recived' + JSON.stringify(res))
        if (res.success == true) {
          this.batchDataList = res.list;
        } else {
          this.toastService.ShowWarning('No batches found related to this courseplanner')
        }
        // if (res && res.batches.length > 0) {
        //  
        // }
      },
      (error: HttpErrorResponse) => {
        this.batchDataList = [];
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.batchDataList = [];
        //   this.toastService.ShowError(error);
        // }
      });


  }
  openTopicLecture(type) {
    this.isTopicTab = !this.isTopicTab
  }
  openSubModal(data, i) {
    this.selectedTopicData.forEach(element => {
      console.log('topic details' + element.isSelected)
      if(element.isSelected){
        element.isSelected = false
      }
    });
    data.isSelected = true
    console.log('subtopic details data', JSON.stringify(data));
    this.subTopicsData = []
    if (data.subtopics.length > 0) {
      this.subTopicsData = data.subtopics
      this.topicData = data
      this.topicData['status'] = 1
    }
  }

  format(time) {
    // Hours, minutes and seconds
    var hrs = ~~(time / 60);
    var mins = ~~((time % 60));

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";
    if (hrs > 0) {
      ret += "" + hrs + "hrs " + (mins < 10 ? "0" : "");
    }
    ret += "" + mins + "min"
    return ret;
  }
  progrssProcess() {
    $(function () {

      $(".progress").each(function () {

        var value = $(this).attr('data-value');
        var left = $(this).find('.progress-left .progress-bar');
        var right = $(this).find('.progress-right .progress-bar');

        if (value > 0) {
          if (value <= 50) {
            right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
          } else {
            right.css('transform', 'rotate(180deg)')
            left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
          }
        }

      })
      $('.progressbar').each(function () {
        var elementPos = $(this).offset().top;
        var topOfWindow = $(window).scrollTop();
        var percent = $(this).find('.circle').attr('data-percent');
        var animate = $(this).data('animate');
        if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
          $(this).data('animate', true);
          $(this).find('.circle').circleProgress({
            // startAngle: -Math.PI / 2,
            value: percent / 100,
            size: 400,
            thickness: 15,
            fill: {
              color: '#663399'
            }
          }).on('circle-animation-progress', function (event, progress, stepValue) {
            $(this).find('strong').text((stepValue * 100).toFixed(0) + "%");
          }).stop();
        }
      });

      function percentageToDegrees(percentage) {

        return percentage / 100 * 360

      }

    });
  }

  AssignBatch(){
   
    let dto = {
      divisionUniqueId: this.batchUniqueId,
      name: this.coursePlannerName,
      uniqueId: this.coursePlannerId,
      units: this.allData.units
    }
    console.log('assign batch all data' + JSON.stringify(dto))
    this.spinner.show();
    this._cpService.addBatchPlanner(dto).subscribe(res => {
      console.log('create batch planner response' + JSON.stringify(res))
      if(res.statusCode == 200){
        this.toastService.showSuccess(res.responseMessage)
      }else{
        this.toastService.ShowWarning(res.responseMessage)
      }
      this.spinner.hide()
    }, (error) => {
   //   this.toastService.ShowError(error);
      this.spinner.hide()
    })
  }

  returnData(data){
    if(data.isEdit){
      this.isEditedFormData = true
    }
    this.allData = [];
    console.log(data.isEdit);
    console.log(JSON.stringify(data.formData.value));
    console.log(data.batchUniqueId);
    console.log(data.coursePlannerId);
    console.log(data.coursePlannerName);
    this.batchUniqueId = data.batchUniqueId;
    this.coursePlannerName = this.coursePlannerName;
    this.coursePlannerId = data.coursePlannerId
    this.allData = data.formData.value;
    this.selectedUnitData = [];
    this.selectedChapter = [];
    this.selectedTopicData = [];
    this.subTopicsData = [];
    this.unitList = [];
    this.plannerType = 4
    if (this.allData.units && this.allData.units.length > 0) {
      let temp = [];
      this.allData.units.forEach(element => {
        temp.push({ text: element.unitDetails.name, id: element.id, data: element })
      });
      this.unitList = temp;

      this.selectedUnit = this.allData.units[0].id
      this.selectedUnitData = this.allData.units[0]
      // this.selectedUnitData = syllabusData.items[0]
      let temp2 = [];
      let totaldration = 0
      if (this.allData.units[0].chapters && this.allData.units[0].chapters.length > 0) {
        this.allData.units[0].chapters.forEach(element => {
          if(element.topics != undefined ){
            element.topics.forEach(element => {
              temp2.push({
                subtopics: element.subtopics,
                topicDetails: element.topicDetails,
                isSelected: false,
              });
              totaldration = totaldration + element.topicDetails.duration
            });
          }
        });
      }
      this.selectedTopicData = temp2;
      if(this.selectedTopicData[0].subtopics != undefined){
        this.subTopicsData = this.selectedTopicData[0].subtopics
      }
      this.selectedTopicData[0].isSelected = true
      this.totalTopicDuration = this.format(totaldration)
    }

  }
}
