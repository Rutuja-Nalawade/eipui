import { Component, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-educator',
  templateUrl: './educator.component.html',
  styleUrls: ['./educator.component.scss']
})
export class EducatorComponent implements OnInit {
  tabNumber: any = 1;
  isUserDetailsShow = false;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params) {
        switch (params['id']) {
          case 'staff':
            this.tabNumber = 1;
            break;
          case 'teacher':
            this.tabNumber = 2;
            break;
          case 'parent':
            this.tabNumber = 3;
            break;
        }
      }
    });
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['activeUserId']) {
      this.tabNumber = 1;

    }
  }
  tabClick(tabNumber) {
    switch (tabNumber) {
      case 1:
        this.router.navigate(['/newModule/educator/lists/staff']);
        break;
      case 2:
        this.router.navigate(['/newModule/educator/lists/teacher']);
        break;
      case 3:
        this.router.navigate(['/newModule/educator/lists/parent']);
        break;

      default:
        break;
    }
  }

  detailsOpen(event) {
    if (event == 'open') {
      this.isUserDetailsShow = true;
    } else {
      this.isUserDetailsShow = false;
    }
  }
}
