import { Subjects } from "./subjects.model";

export class PaperSubjects {
    updatedBy: any;
    examUid:any;
    subjects: Subjects[] = [];
}
