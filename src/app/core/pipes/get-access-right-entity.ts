import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getMemberShipLevelEntity'
})
export class GetAccessRightForEntityPipe implements PipeTransform {

  transform(parentValue: number, childValue : number): Boolean {
    return this.getMemberShipLevelEntity(parentValue,childValue);
  }
  getMemberShipLevelEntity(parentValue: number,childValue : number): Boolean{
    let accessRights : any;
    accessRights = JSON.parse(sessionStorage.getItem('entityAccessRights'));

    let entity = accessRights.find(x => x.id === parentValue);

    if (entity == null || entity == undefined || entity == 'undefined') { 
        console.log("ENtityFound");
        return false;
    } else {
       let subEntity = entity.entityList.find(y=> y.id === childValue);

       if (subEntity != null && subEntity != undefined && subEntity != 'undefined') { 
            return subEntity.isAccess;
       }  
           return false;
    }
 }
}