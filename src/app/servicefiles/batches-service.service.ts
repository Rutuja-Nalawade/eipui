import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { List } from '../shared/ui/emaillist/email-list.model';

@Injectable({
  providedIn: 'root'
})
export class BatchesServiceService {

  constructor(private http: HttpClient) { }


  public getBatchList() {
    return this.http.get<any>(environment.apiUrl + '/academic-batch/get/by-organization');
  }

  public saveBatch(acaedmicBatch: any) {
    return this.http.post<any>(environment.apiUrl + '/academic-batch/add', acaedmicBatch);
  }

  public updateBatch(acaedmicBatch: any) {
    return this.http.put<any>(environment.apiUrl + '/academic-batch/update', acaedmicBatch);
  }
  public deleteBatch(batchUniqueId: any) {
    console.log('delete id recieved' + batchUniqueId)
    return this.http.delete<any>(environment.apiUrl + '/academic-batch/delete/' + batchUniqueId);
  }

  public getBatchByCourse(uniqueId) {
    return this.http.get<any>(environment.apiUrl + '/academic-batch/get/by-course/' + uniqueId);
  }
  public getBatchByCourseList() {
    return this.http.get<any>(environment.apiUrl + '/academic-batch/get/by-course-list');
  }
  public getBatchGroupList(uniqueId) {
    return this.http.get<any>(environment.apiUrl + '/batchGroup/get/batch/groupcategorywise/' + uniqueId);
  }
  public getGroupCategoryList() {
    return this.http.get<any>(environment.newApiUrl + '/groupcategory/get');
  }

  public getCoursePlannerAssignedBatchList(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/coursePlanner/get/assignedBatches/' + uniqueId);
  }
  public getBatchCategoryByBatch(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/groupcategory/get/forbatch/' + uniqueId);
  }
  public getBatchGroupByBatchCategory(dto) {
    return this.http.post<any>(environment.apiUrl + '/batchGroup/get/batch/groupcategorywise/', dto);
  }
  public getCtegoryGroupByBatchUid(dto) {
    return this.http.post<any>(environment.apiUrl + '/batchGroup/get/categorywise', dto);
  }

  public getBatchGroupCategoryAndBacthGroupList(List) {
    return this.http.post<any>(environment.apiUrl + '/batchGroup/get/categorywise' , List);
  }
}
