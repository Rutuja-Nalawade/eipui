import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupNewComponent } from './signup-new/signup-new.component';
import { MobileVerificationNewComponent } from './mobile-verification-new/mobile-verification-new.component';
import { OrganizationTypeNewComponent } from './organization-type-new/organization-type-new.component';
import { SetupComponent } from './setup/setup.component';


const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'signup',
        component: SignupNewComponent
    },
    {
        path: 'mobile-verify',
        component: MobileVerificationNewComponent
    },
    {
        path: 'organization-type',
        component: OrganizationTypeNewComponent
    },
    {
        path: 'setup',
        component: SetupComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NewAuthRoutingModule { }
