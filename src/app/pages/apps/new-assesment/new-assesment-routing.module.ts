import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonHeaderComponent } from './common-header/common-header.component';
import { AddTracksComponent } from './add-tracks/add-tracks.component';


const routes: Routes = [
  {path:'assesmentDetails',component:CommonHeaderComponent},
  { path: 'track/create/:examId', component: AddTracksComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewAssesmentRoutingModule { }
