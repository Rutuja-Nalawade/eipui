import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewCommonService {

  private messageSource = new BehaviorSubject<string>("");
  filterData = new Subject();
  currentSource = this.messageSource.asObservable();
  constructor() {

  }
  isotherClicked = new Subject<any>();
  iscloseClicked = new Subject<any>();
  iscancelClicked = new Subject<any>();
  batchFromParent = new Subject<any>();
  workTimeProcess = new Subject<any>();

  nextCount() {

  }
  changeSource(data: any) {
    this.messageSource.next(data);
  }
  getUserData() {
    return JSON.parse(sessionStorage.getItem('currentUser'))
  }
}
