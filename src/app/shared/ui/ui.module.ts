import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgbCollapseModule, NgbDatepickerModule, NgbTimepickerModule, NgbDropdownModule, NgbToastModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { ClickOutsideModule } from 'ng-click-outside';
import { SlimscrollDirective } from './slimscroll.directive';
import { CountToDirective } from './count-to.directive';
import { ModalModule } from 'ngx-bootstrap';
import { PreloaderComponent } from './preloader/preloader.component';
import { PagetitleComponent } from './pagetitle/pagetitle.component';
import { PortletComponent } from './portlet/portlet.component';
import { EmaillistComponent } from './emaillist/emaillist.component';
import { WidgetComponent } from './widget/widget.component';
import { ToastComponent } from '../../core/toast/toast.component';
import { ToastrModule } from 'ngx-toastr';
import { TypeaheadModule } from 'ngx-bootstrap';
import { PopoverModule } from 'ngx-bootstrap';
import { OnlynumberDirective } from '../validators/onlynumber.directive';
import { AlphabetOnlyDirective } from '../validators/onlyapbhabets.directive';
import { CommonloaderComponent } from './preloader/commonloader.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { InfiniteScrollerDirective } from './scroller/infinite-scroller.directive';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgSelectModule } from '@ng-select/ng-select';
import { CheckallDirective } from '../validators/checkall.directive';
import { TruncateTextPipe } from 'src/app/truncate-text.pipe';


@NgModule({
  // tslint:disable-next-line: max-line-length

  declarations: [SlimscrollDirective, PreloaderComponent, PagetitleComponent, PortletComponent, WidgetComponent, EmaillistComponent, CountToDirective, ToastComponent, OnlynumberDirective, AlphabetOnlyDirective, CommonloaderComponent, InfiniteScrollerDirective, CheckallDirective, TruncateTextPipe],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    NgbCollapseModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbDropdownModule,
    NgbToastModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    InfiniteScrollModule,
    NgbTabsetModule,
    AngularMultiSelectModule,
    TypeaheadModule.forRoot(),
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    DragDropModule,
    NgSelectModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ],
  // tslint:disable-next-line: max-line-length
  exports: [SlimscrollDirective, PreloaderComponent, PagetitleComponent, PortletComponent, WidgetComponent, EmaillistComponent, CountToDirective, ToastComponent, OnlynumberDirective, AlphabetOnlyDirective, CommonloaderComponent, InfiniteScrollerDirective, InfiniteScrollModule, CheckallDirective, TruncateTextPipe]
})
export class UIModule { }
