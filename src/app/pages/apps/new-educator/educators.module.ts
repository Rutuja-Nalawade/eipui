import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EducatorsRoutingModule } from './educators-routing.module';
import { EducatorComponent } from './educator/educator.component';
import { StaffModule } from '../staff/staff.module';
import { TeacherModule } from '../teacher/teacher.module';
import { ParentsModule } from '../parents/parents.module';


@NgModule({
  declarations: [EducatorComponent],
  imports: [
    CommonModule,
    EducatorsRoutingModule,
    StaffModule,
    TeacherModule,
    ParentsModule
  ]
})
export class EducatorsModule { }
