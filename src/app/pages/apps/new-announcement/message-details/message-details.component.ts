import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.scss']
})
export class MessageDetailsComponent implements OnInit {
  @Input() selectedMsgId: Number;
  parentList=[];
  constructor() { }

  ngOnInit() {
    console.log(this.selectedMsgId)
  }

}
