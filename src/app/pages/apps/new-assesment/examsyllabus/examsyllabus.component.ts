import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
declare var $: any;
@Component({
  selector: 'app-examsyllabus',
  templateUrl: './examsyllabus.component.html',
  styleUrls: ['./examsyllabus.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExamsyllabusComponent implements OnInit {
  actualCoursePlanner: FormGroup;
  @Input() timedetails: FormGroup;
  @Input() examSyllabus: FormGroup;
  @Input() selectedSubjects = [];
  @Input() courseId: number;
  @Input() type: any;
  @Input() examType: any;
  @Input() step: any;
  @Input() assignmentTypeId: any;
  @Input() isFirstNext: any;
  rem: boolean = false;
  subjectsList: any = []
  isUnit1: any;
  isUnit2: any;
  isUnit3: any;
  isUnit4: any;
  typeName = "Unit";

  constructor(private fb: FormBuilder, private examService: ExamServiceService, private spinner: NgxSpinnerService, private toastService: ToastsupportService) {
  }
  ngOnInit() {
    // this.actualCoursePlanner = this.fb.group({
    //   subjectsList: this.fb.array([], Validators.required),
    // })
    // $('[data-toggle="tooltip"]').tooltip()

  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['step'] && changes['step'].currentValue == 3) {
      console.log(this.timedetails);
      // this.actualCoursePlanner.controls.subjectsList.setValue(this.examSyllabus.controls.subjectList);
      switch (this.type) {
        case 1:
          this.typeName = "Unit";
          break;
        case 2:
          this.typeName = "Chapter";
          break;
        case 4:
          this.typeName = "SubTopic";
          break;
        default:
          this.typeName = "Topic";
          break;
      }
    }
  }

  comparesyllabus = (item: any, selected: any) => {
    if (selected.chapterName && item.chapterName) {
      return item.chapterName === selected.chapterName;
    }
    if (item.name && selected.name) {
      return item.name === selected.name;
    }
    return false;
  };


  isCollaps(id, headId, head) {

    var ele = document.getElementById(id);
    console.log("ele", ele)
    if (ele.classList.contains('isUnhide')) {
      ele.classList.remove("isUnhide");
      ele.classList.add("isHide");

    } else if (ele.classList.contains('isHide')) {
      ele.classList.remove("isHide");
      ele.classList.add("isUnhide");

    }

    var element = document.getElementById(headId);
    if (element.classList.contains('fa-angle-right')) {
      element.classList.remove("fa-angle-right");
      element.classList.add("fa-angle-up");

    } else if (element.classList.contains('fa-angle-up')) {
      element.classList.remove("fa-angle-up");
      element.classList.add("fa-angle-right");

    }

    var element1 = document.getElementById(head);
    if (element1.classList.contains('dactiveSubject')) {
      element1.classList.remove("dactiveSubject");
      element1.classList.add("activeSubject");

    } else if (element1.classList.contains('activeSubject')) {
      element1.classList.remove("activeSubject");
      element1.classList.add("dactiveSubject");

    }
  }

  onClickCheckbox(event, cp, subIndex, index) {
    console.log(cp);
    if (event.target.checked) {
      console.log("object");
      this.spinner.show();
      if (cp.controls.syllabusCoursewise.value == null || cp.controls.syllabusCoursewise.value.length == 0) {
        switch (this.type) {
          case 1:
            this.examService.getUnitListByCoursePlanner(cp.controls.uniqueId.value).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchList(res.list, 1));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Unit not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
          case 2:
            this.examService.getChapterTopicListByCoursePlanner(cp.controls.uniqueId.value, 1).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchList(res.list, 2));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Chapter not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
          case 4:
            this.examService.getSubtopicListByCoursePlanner(cp.controls.uniqueId.value).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchList(res.list, 4));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Subtopic not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
          default:
            this.examService.getChapterTopicListByCoursePlanner(cp.controls.uniqueId.value, 2).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchList(res.list, 3));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Topic not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
        }
      } else {
        this.spinner.hide();
        cp.controls.isSelected.setValue(true);
      }
    } else {
      let subjectList = <FormArray>this.examSyllabus.controls.subjectList;
      let subjectGroup = <FormGroup>subjectList.controls[subIndex];
      let syllabus = subjectGroup.controls.syllabus.value;
      let actualSyllabus = subjectGroup.controls.actualSyllabus.value;
      let syllabusList = cp.controls.syllabusCoursewise.value;
      let syllabusIds = [];
      let syllabusCpDatas = [];
      if (syllabus.length > 0 && syllabusList.length > 0) {
        syllabus.forEach((element, i) => {
          let isMatch = false;
          syllabusList.forEach(elem => {
            if (element == elem.id) {
              isMatch = true;
              return true;
            }
          });
          if (!isMatch) {
            syllabusIds.push(element);
            syllabusCpDatas.push(actualSyllabus[i]);
          }

        });
        subjectGroup.controls.actualSyllabus.setValue(syllabusCpDatas);
        subjectGroup.controls.syllabus.setValue(syllabusIds);
        cp.controls.selectedSyllabus.setValue([]);
      }
      cp.controls.isSelected.setValue(false);
    }
  }
  patchList(data, type) {
    let list = [];
    data.forEach((x, index) => {
      let dto = { id: x.id };
      switch (type) {
        case 1:
          dto['unitDetails'] = x.unitDetails;
          dto['name'] = x.unitDetails.name;
          dto['duration'] = x.unitDetails.duration;
          break;
        case 2:
          dto['chapterDetails'] = x.chapterDetails;
          dto['name'] = x.chapterDetails.name;
          dto['duration'] = x.chapterDetails.duration;
          break;
        case 4:
          if (x.subtopics) {
            x.subtopics.forEach(element => {
              dto['subTopicDetails'] = element.subTopicDetails;
              dto.id = element.id;
              dto['name'] = element.subTopicDetails.name;
              dto['duration'] = element.subTopicDetails.duration;
            });
          }
          break;
        default:
          if (x.topics) {
            x.topics.forEach(element => {
              dto['topicDetails'] = element.topicDetails;
              dto.id = element.id;
              dto['name'] = element.topicDetails.name;
              dto['duration'] = element.topicDetails.duration;
            });
          }
          break;
      }
      list.push(dto);
    });
    return list;
  }
  onSelectionChange(index: number, currentIndex: number, $event: any, type: any) {
    console.log("type", type)
    console.log("event", event)
    console.log("index", index, "currentIndex", currentIndex)
    var ele = document.getElementById(type);
    console.log("ele", ele)
    if ($event.target.checked) {
      ele.classList.remove("isHideSelect");
      ele.classList.add("isUnhideSelect");

    } else {
      ele.classList.remove("isUnhideSelect");
      ele.classList.add("isHideSelect");
    }


    let subjectArray = <FormArray>this.examSyllabus.controls['subjects'];
    let subjectGroup = <FormGroup>subjectArray.controls[index];
    let actualSyllabusArray = <FormArray>subjectGroup.controls['actualSyllabus'];
    let syllabusArray = <FormArray>subjectGroup.controls['syllabus'];
    let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
    let actualCPGroup = <FormGroup>actualCPArray.controls[currentIndex];
    let syllabusCoursewise = <FormArray>actualCPGroup.controls['syllabusCoursewise'];
    let selectedSyllArray = <FormArray>actualCPGroup.controls['selectedsyllabusCoursewise'];
    console.log("selectedSyllArray", selectedSyllArray);

    if ($event.target.checked == true) {
      actualCPGroup.controls['isSelected'].setValue(true);

      // this.getSyllabusList(actualCPGroup.cogetSyllabusListntrols['id'].value, actualSyllabusArray);
      this.getSyllabusList(actualCPGroup.controls['id'].value, syllabusCoursewise);
    } else {
      // let actualSyllabusList = actualSyllabusArray.value.filter(x => x.coursePlannerId != actualCPGroup.controls['id'].value);
      syllabusCoursewise.controls = [];
      // selectedSyllArray.controls=[]
      actualCPGroup.setControl('selectedsyllabusCoursewise', this.fb.array([]));
      // console.log("selectedSyllArray",selectedSyllArray);
    }
  }

  onSelectionChangeRemark($event: any, type: any) {
    console.log("type", type)
    console.log("event", event)

    var ele = document.getElementById(type);
    console.log("ele", ele)
    console.log("$event.target.checked", $event.target.checked)
    if ($event.target.checked) {
      ele.classList.remove("isHideOptionalRem");
      ele.classList.add("isUnhideOptionalRem");

    } else {
      ele.classList.remove("isUnhideOptionalRem");
      ele.classList.add("isHideOptionalRem");
    }

  }


  public getSyllabusList(coursePlannerId: any, syllabusArray: FormArray) {
    // return this.examService.getSyllabus(coursePlannerId, this.type).subscribe(
    //   res => {
    //     if (res) {
    //       // syllabusArray.controls = [];
    //       switch (parseInt(this.type)) {
    //         case 1:
    //           for (let syllabus1 of res) {
    //             syllabusArray.push(this.addUnitSyllabus(syllabus1, coursePlannerId));
    //           }
    //           break;
    //         case 2:
    //           for (let syllabus1 of res) {
    //             syllabusArray.push(this.addChapterSyllabus(syllabus1, coursePlannerId));
    //           }
    //           break;
    //         case 4:
    //           for (let syllabus1 of res) {
    //             syllabusArray.push(this.addSubTopicSyllabus(syllabus1, coursePlannerId));
    //           }
    //           break;
    //         default:
    //           for (let syllabus1 of res) {
    //             syllabusArray.push(this.addTopicSyllabus(syllabus1, coursePlannerId));
    //           }
    //       }
    //     }
    //     console.log("exam syllabus1", this.examSyllabus)
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error.error instanceof Error) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       // this.toasterService.ShowError(error.error.errorMessage);
    //     }
    //   });
  }

  addUnitSyllabus(data: any, coursePlannerId: any) {
    return this.fb.group({
      id: data.unitId,
      name: data.unitName,
      coursePlannerId: coursePlannerId
    })
  }

  addChapterSyllabus(data: any, coursePlannerId: any) {
    return this.fb.group({
      id: data.chapterId,
      name: data.chapterName,
      coursePlannerId: coursePlannerId
    })
  }

  addTopicSyllabus(data: any, coursePlannerId: any) {
    return this.fb.group({
      chapterId: data.chapterId,
      chapterName: data.chapterName,
      id: data.topicId,
      name: data.topicName,
      coursePlannerId: coursePlannerId
    })
  }

  addSubTopicSyllabus(data: any, coursePlannerId: any) {
    return this.fb.group({
      chapterId: data.chapterId,
      chapterName: data.chapterName,
      id: data.subtopicId,
      name: data.subtopicName,
      coursePlannerId: coursePlannerId
    })
  }



  addSyllabusControl(value: any, index: number, currentIndex: number) {
    var stringArry = []
    value.forEach(element => {
      stringArry.push(element.name);
    });
    console.log("stringArry", stringArry)
    console.log("index", index, "currentIndex", currentIndex)
    let subjectArray = <FormArray>this.examSyllabus.controls['subjects'];
    let formGroup = <FormGroup>subjectArray.controls[index];
    let syllabusArray = <FormArray>formGroup.controls['syllabus'];
    let cpArray = <FormArray>formGroup.controls['actualCoursePlanners'];
    let cpformGroup = <FormGroup>cpArray.controls[currentIndex];
    let selectedSyllArray = <FormArray>cpformGroup.controls['selectedsyllabusCoursewise'];
    console.log("value", value)

    for (let i = selectedSyllArray.length - 1; i >= 0; i--) {

      selectedSyllArray.removeAt(i);
    }
    if (value != null && value.length > 0) {

      for (let i = 0; i < value.length; i++) {
        selectedSyllArray.push(new FormControl(value[i]));
      }
    }

    console.log("subjectArray", subjectArray.value)
  }


  findCommonElements3(arr1, arr2) {
    debugger
    return arr1.some(item => arr2.includes(item))
  }


  onRemarkChange($event) {
    if ($event.target.checked == true) {
      this.rem = true;
    } else {
      this.rem = false;
    }
  }
  changeRemarks(event, index) {
    console.log(event);
    let subjectList = this.examSyllabus.value.subjectList
    let subjectGroup = subjectList[index];
    subjectGroup.remarks = event.target.value;
  }
  changeCpSyllabus(event, item, index) {
    let subjectList = <FormArray>this.examSyllabus.controls.subjectList
    let subjectGroup = <FormGroup>subjectList.controls[index];
    console.log(item);
    let syllabusIds: any = (subjectGroup.controls.syllabus.value) ? subjectGroup.controls.syllabus.value : [];
    let syllabus: any = (subjectGroup.controls.actualSyllabus.value) ? subjectGroup.controls.actualSyllabus.value : [];
    event.forEach(x => {
      syllabusIds.push(x.id);
      syllabus.push(x);
    });
    let unique = [...new Set(syllabusIds)];
    let uniqueSyllabus = [...new Set(syllabus)];
    console.log(syllabusIds, event);
    subjectGroup.controls.actualSyllabus.setValue(uniqueSyllabus);
    let subjectList1 = this.examSyllabus.value.subjectList
    let subjectGroup1 = subjectList1[index];
    subjectGroup1.actualSyllabus = uniqueSyllabus;
    subjectGroup1.syllabus = unique;
    subjectGroup.controls.syllabus.setValue(unique);
    console.log(this.examSyllabus);
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  getPassingMarks(value: number) {
    if (value > parseInt(this.examSyllabus.controls['maximumMarks'].value)) {
      this.examSyllabus.controls['passingMarks'].setValue('');
      // this._commonService.ShowWarning("Passing marks should be less than exam marks.");
    }
  }
  doItLater(index) {
    let subjectList = <FormArray>this.examSyllabus.controls.subjectList
    let subjectGroup = <FormGroup>subjectList.controls[index];
    if (subjectGroup.controls.isSyllabusSelected.value) {
      subjectGroup.controls.syllabus.setValue([]);
      subjectGroup.controls.actualSyllabus.setValue([]);
      subjectGroup.controls.isSyllabusSelected.setValue(false);
    } else {
      subjectGroup.controls.isSyllabusSelected.setValue(true);
    }
  }
}
