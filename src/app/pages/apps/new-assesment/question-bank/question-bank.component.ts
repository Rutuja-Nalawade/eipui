import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AssesmentService } from '../../../../servicefiles/assesment.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-question-bank',
  templateUrl: './question-bank.component.html',
  styleUrls: ['./question-bank.component.scss']
})
export class QuestionBankComponent implements OnInit {
  units = ['JEE Advanced Pattern', 'Custom Pattern', 'Pattern', 'Custom', 'Advanced Pattern']
  syllabus = ['JEE Advanced Pattern', 'Custom Pattern', 'Pattern', 'Custom', 'Advanced Pattern']
  topics = ['JEE Advanced Pattern', 'Custom Pattern', 'Pattern', 'Custom', 'Advanced Pattern']
  chapters = ['JEE Advanced Pattern', 'Custom Pattern', 'Pattern', 'Custom', 'Advanced Pattern']
  subtopics = ['JEE Advanced Pattern', 'Custom Pattern', 'Pattern', 'Custom', 'Advanced Pattern']
  unitarr = [];
  unitRemArr = []
  chapterarr = []
  topicarr = []
  subtopicarr = []

  //api
  message: string;
  trackSyllabus: any;
  trackUid: string;
  questionCount: number = 25;
  noOfQuestionsEntered: number=0;
  currentEnteredQuestions: number = 0;
  pageIndex: number = 0;
  dropdownSettings: any;
  questions = [];
  preQuestions = [];
  trackedQuestions = [];
  trackApprovedQuetion: any;
  hiddenAddButtons = [];
  hiddenRemoveButtons = [];
  hiddenSolutions = [];
  visibleSolutions = [];
  rowValue = 25;
  syllabusForm: FormGroup;
  rowList = [{ id: 25, value: '25 Questions' }, { id: 50, value: '50 Questions' }, { id: 100, value: '100 Questions' }, { id: 150, value: '150 Questions' }, { id: 200, value: '200 Questions' }]

  isPick: boolean = true;
  isAppr: boolean = false;
  isTrack: boolean = false;
  isexamList: boolean = false;
  examData: any;
  @Input() questionData: any;
  @Input() selectedTrack: any;
  errorType: any;
  reportErrForm: FormGroup;
  errorList = [];
  isTrackList: boolean = false;
  solutiosres: any;
  solutionobj: any;
  totTrackQuestionCount=0;
  @Output() closeQuestionBank = new EventEmitter<string>();
  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    // private qbsUrlService: QbsUrlService, 
    private examSetterService: AssesmentService,
    private toastService: ToastsupportService,
    private modalService: NgbModal,
    private location: Location) {
    this.loadMathjaxConfig();

    console.log("questionData", this.questionData)
  }


  ngOnInit() {

    // this.examSetterService.currentMessage.subscribe(message => this.message = message)
    //     this.route.params.subscribe(params => {
    //         this.trackUid = params['trackUid'];
    //     });

    // this.initSyllabusForm();
    // this.dropDownSetting();

    // this.getTrackSyllabus();
    console.log("questionData", this.questionData)
    this.spinner.show()
    this.getExamTrackQuestionBySyllabus()
  }

  initReoprtForm() {
    this.reportErrForm = this.formBuilder.group({
      error: [null, Validators.nullValidator],
      errorMsg: [null, Validators.nullValidator]
    })
  }
  openSolution(demo, demoIcon) {
    let ele = document.getElementById(demo)
    let element = document.getElementById(demoIcon)
    if (ele.classList.contains('isHide')) {
      ele.classList.remove('isHide')
      ele.classList.add('isUnHide')
    } else if (ele.classList.contains('isUnHide')) {
      ele.classList.remove('isUnHide')
      ele.classList.add('isHide')
    }

    if (element.classList.contains('fa-angle-up')) {
      element.classList.remove('fa-angle-up')
      element.classList.add('fa-angle-down')
    } else if (element.classList.contains('fa-angle-down')) {
      element.classList.remove('fa-angle-down')
      element.classList.add('fa-angle-up')
    }
  }

  openSolution1(demo, demoIcon) {
    let ele = document.getElementById(demo)
    let element = document.getElementById(demoIcon)
    if (ele.classList.contains('isHide')) {
      ele.classList.remove('isHide')
      ele.classList.add('isUnHide')
    } else if (ele.classList.contains('isUnHide')) {
      ele.classList.remove('isUnHide')
      ele.classList.add('isHide')
    }

    if (element.classList.contains('fa-plus')) {
      element.classList.remove('fa-plus')
      element.classList.add('fa-minus')
    } else if (element.classList.contains('fa-minus')) {
      element.classList.remove('fa-minus')
      element.classList.add('fa-plus')
    }
  }

  selectUnits(value) {
    this.unitarr = []
    for (let i = 0; i < value.length; i++) {
      this.unitarr.push(value[i]);
    }
  }

  deleteSelectedUnit(user, i) {
    this.unitarr.splice(i, 1);
  }

  clearUnit() {
    this.unitarr = [];
  }

  selectChapters(value) {
    this.chapterarr = []
    for (let i = 0; i < value.length; i++) {
      this.chapterarr.push(value[i]);
    }
  }

  deleteSelectedChapters(user, i) {
    this.chapterarr.splice(i, 1);
  }

  clearChapters() {
    this.chapterarr = [];
  }

  selectTopics(value) {
    this.topicarr = []
    for (let i = 0; i < value.length; i++) {
      this.topicarr.push(value[i]);
    }
  }

  deleteSelectedTopics(user, i) {
    this.topicarr.splice(i, 1);
  }

  clearTopics() {
    this.topicarr = [];
  }

  selectSubTopics(value) {
    this.subtopicarr = []
    for (let i = 0; i < value.length; i++) {
      this.subtopicarr.push(value[i]);
    }
  }

  deleteSelectedSubTopics(user, i) {
    this.subtopicarr.splice(i, 1);
  }

  clearSubTopics() {
    this.subtopicarr = [];
  }

  deleteSelectedAll() {
    this.subtopicarr = [];
    this.topicarr = [];
    this.unitarr = [];
    this.chapterarr = [];
  }

  //api 

  initSyllabusForm() {
    this.syllabusForm = this.formBuilder.group({
      units: [[], Validators.required],
      chapters: [[], Validators.required],
      topics: [[], Validators.nullValidator],
      subtopics: [[], Validators.nullValidator],
    });
  }

  dropDownSetting() {
    this.dropdownSettings = {
      singleSelection: false,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      primaryKey: 'uniqueId',
      labelKey: 'name'
    };
  }



  pageChanged(page: number) {
    this.pageIndex = page - 1;
    this.getExamTrackQuestionBySyllabus();
  }

  OnChangeElement(event) {
    console.log("event", event)
    this.questionCount = event.id;
    this.pageIndex = this.pageIndex + 1;
    this.getExamTrackQuestionBySyllabus();
  }



  loadTrackQuestions() {
    const dto = {
      assessmentUid: this.questionData.assessmentUid,
      subjectUid: this.questionData.subjectUid,
      sectionUid: this.questionData.sectionUid,
      trackUid: this.questionData.trackUid
    }
    this.spinner.show()
    this.examSetterService.getTrackQuestions(dto).subscribe(
      res => {
        if (res.success == true) {
          this.trackedQuestions = res.list;
          this.totTrackQuestionCount=res.list.length;
          this.trackApprovedQuetion = res;
          console.log("2")
          this.spinner.hide();
          this.renderMathjax();
        } else {
          this.spinner.hide();

        }

      },
      (error: HttpErrorResponse) => {
        // console.log("error", error);
        // if (error instanceof HttpErrorResponse) {
        //   console.log("error", error);
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }

  appendData(questions: Array<any>) {
    this.questions = []
    for (let i = 0; i < questions.length; ++i) {
      this.questions.push(questions[i]);

    }

    console.log("this.questions", this.questions)
  }

  tabClick(activeId, deactiveId1, deactiveId2) {
    let ele = document.getElementById(activeId);
    let element1 = document.getElementById(deactiveId1)
    let element2 = document.getElementById(deactiveId2)
    ele.classList.add('isHeadActive')
    element1.classList.remove('isHeadActive')
    element2.classList.remove('isHeadActive')

    if (activeId == 'pick') {
      this.isAppr = false
      this.isTrack = false
      this.isPick = true

    } else if (activeId == 'appr') {
      this.isPick = false
      this.isTrack = false
      this.isAppr = true
    } else if (activeId == 'track') {
      this.isAppr = false
      this.isPick = false
      this.isTrack = true
    }
  }


  renderMathjax() {

    setTimeout(() => {
      MathJax.Hub.Queue(['Typeset', MathJax.Hub, 'questions-panel']);
    }, 1000);
  }

  loadMathjaxConfig() {

    MathJax.Hub.Config({
      showMathMenu: false,
      tex2jax: { inlineMath: [["$", "$"]], displayMath: [["$$", "$$"]] },
      menuSettings: { zoom: "Double-Click", zscale: "150%" },
      CommonHTML: { linebreaks: { automatic: true } },
      "HTML-CSS": { linebreaks: { automatic: true } },
      SVG: { linebreaks: { automatic: true } },
      displayAlign: "left"
    });
  }

  onClickAddQuestion(selectedQuestion: any, itemIndex: number) {
    let question = this.trackedQuestions.find(x => x.uniqueId == selectedQuestion.uniqueId);
    if (question != undefined && question != '') {
      this.toastService.ShowError("This question is exists in tracked questions.");
    } else {
      this.hiddenAddButtons[itemIndex] = false;
      this.hiddenRemoveButtons[itemIndex] = true;
      this.currentEnteredQuestions = this.currentEnteredQuestions + 1;
      this.preQuestions.push(selectedQuestion);
    }
  }

  onClickRemoveQuestion(question: any, itemIndex: number) {

    const index: number = this.preQuestions.indexOf(question);
    if (index !== -1)
      this.preQuestions.splice(index, 1);
    this.currentEnteredQuestions = this.currentEnteredQuestions - 1;
    this.hiddenAddButtons[itemIndex] = true;
    this.hiddenRemoveButtons[itemIndex] = false;
  }

  onClickRemovePrevQuestion(question: any) {

    const index: number = this.preQuestions.indexOf(question);
    if (index !== -1) {
      this.preQuestions.splice(index, 1);
    }

    const itemIndex = this.questions.indexOf(question);
    this.currentEnteredQuestions = this.currentEnteredQuestions - 1;
    
    this.hiddenAddButtons[itemIndex] = true;
    this.hiddenRemoveButtons[itemIndex] = false;
  }

  onClickApproveQuestions() {

    let totalQuestionHaveToAprovedForDB = this.selectedTrack.totalQuestions;
    let alredyEnterQuestion=0
    alredyEnterQuestion = this.noOfQuestionsEntered;
    let selectedQuestion = this.preQuestions.length;
    let finalQuestionsDto = [];
    this.preQuestions.forEach(element => {
      finalQuestionsDto.push(element);
    });
    this.trackedQuestions.forEach(element => {
      finalQuestionsDto.push(element);
    });
    console.log("finalQuestionsDto",finalQuestionsDto)
    let remaingQuestionToEnterDB = totalQuestionHaveToAprovedForDB - alredyEnterQuestion;
    let diffrenceQuestion = remaingQuestionToEnterDB
    if (diffrenceQuestion >= selectedQuestion) {
      this.spinner.show();
      let questionUniqueIds = this.pluck(finalQuestionsDto, "uniqueId");
      let postData = {
        assessmentUid: this.questionData.assessmentUid,
        subjectUid: this.questionData.subjectUid,
        sectionUid: this.questionData.sectionUid,
        trackUid: this.questionData.trackUid,
        questionUids: questionUniqueIds,

      };

      this.examSetterService.saveExamQuestions(postData).subscribe(
        (res: any) => {
          if (res.statusCode == 200) {

            this.toastService.showSuccess(res.responseMessage)
            this.preQuestions = [];
            this.hiddenAddButtons = [];
            this.hiddenRemoveButtons = [];
            this.noOfQuestionsEntered = this.currentEnteredQuestions;
            this.loadTrackQuestions();
          }

          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide()
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          //   this.toastService.ShowError(error.error);
          //   this.spinner.hide();
          // } else {
          //   this.spinner.hide();
          //   this.toastService.ShowError(error);
          // }
        });
    } else {
        //console.log("test")
        this.toastService.ShowWarning("You can add only " + diffrenceQuestion + " " + "now");
     
     
    }


  }

  onClickRemoveTrackedQuestion(questionUniqueId: any) {



    let index = this.trackedQuestions.indexOf(questionUniqueId);
    this.trackedQuestions.splice(index, 1);
    this.noOfQuestionsEntered=this.noOfQuestionsEntered-1;
    this.currentEnteredQuestions = this.currentEnteredQuestions - 1;
    

  }
  pluck(objs, name) {
    var sol = [];
    for (var i in objs) {
      if (objs[i].hasOwnProperty(name)) {
        sol.push(objs[i][name]);
      }
    }
    return sol;
  }

  //vinayak neww design or api added

  getExamTrackQuestionBySyllabus() {
    this.spinner.show()
    const dto = {
      assessmentUid: this.questionData.assessmentUid,
      subjectUid: this.questionData.subjectUid,
      sectionUid: this.questionData.sectionUid,
      trackUid: this.questionData.trackUid,
      pageIndex: this.pageIndex,
      questionCount: this.questionCount
    }
    this.examSetterService.getExamTrackQuestionBySyllabus(dto).subscribe(
      res => {
        if (res.statusCode == 200) {


          console.log("question res", res);
          this.loadTrackQuestions();
          this.noOfQuestionsEntered = this.selectedTrack.questionsAdded;
          this.currentEnteredQuestions = this.selectedTrack.questionsAdded;
          this.trackSyllabus = res;
          if (this.questions != null && this.questions.length > 0) {
            this.appendData(res.questions);
          } else {
            this.questions = res.questions;
          }
          this.renderMathjax();
          console.log("1");
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");this.toastService.ShowError(error.error);
        // } else {
        //   this.toastService.ShowError(error);
        // }
      //  console.log("3");
        this.spinner.hide()
      });

  }
  openReportErr(content) {
    this.initReoprtForm();
    this.modalService.open(content, { centered: true, backdrop: 'static', windowClass: 'reportEr' });
  }

  close() {
    this.reportErrForm.reset();
  }

  gotoTrack(type) {
    if (type == 2) {
      // this.isexamList=true;
      this.closeQuestionBank.emit("2");
    } else {
      this.closeQuestionBank.emit("1");
    }
  }

  getSolutuion(solution, content) {
    this.spinner.show()
    this.solutionobj = solution;
    this.examSetterService.getSolution(solution.uniqueId).subscribe(
      res => {
        if (res) {
          this.solutiosres = res;
          this.renderMathjax();
          this.modalService.open(content, { centered: true });
         
          
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.toastService.ShowError('Something went wrong');
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
       // console.log("3");
        this.spinner.hide()
      });
  }

}
