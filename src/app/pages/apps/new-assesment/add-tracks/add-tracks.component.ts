import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { AssesmentSetterServiceService } from '../../../../servicefiles/assesment-setter-service.service';
import { AssesmentService } from '../../../../servicefiles/assesment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
    selector: 'app-add-tracks',
    templateUrl: './add-tracks.component.html',
    styleUrls: ['./add-tracks.component.scss']
})
export class AddTracksComponent implements OnInit {
    isOptional: boolean = false;
    parentIndex: number;
    childIndex: number = 0;
    previousMarks: number;
    @Input() examUid: any;
    @Input() isEditTrack: any;
    @Output() closeFromTrackInfo = new EventEmitter<any>();
    @Input() trackData: any;
    imageSrc: string;
    exam: any;
    examTrackForm: FormGroup;
    markingSchemeForm: FormGroup;
    instructionForm: FormGroup;
    teachers = [];
    rightMarks: number;
    isFromEvent: boolean = false;
    customObjectiveExamQuestionType = []
    jeeAdvanceSubjectiveQuestionType = []
    files: any = [];
    minDate = new Date();
    // maxDate: Date;



    questionTypes = [
        { "id": 1, "name": "SCQ" },
        { "id": 2, "name": "MCQ" },
        { "id": 3, "name": "Numeric" },
        { "id": 4, "name": "SA" },
        { "id": 5, "name": "VSA" },
        { "id": 6, "name": "FIB" },
        { "id": 7, "name": "TF" }
    ];
    editorConfig = {
        "editable": true,
        "spellcheck": true,
        "height": "auto",
        "minHeight": "100%;",
        "width": "auto",
        "minWidth": "0",
        "translate": "yes",
        "enableToolbar": true,
        "showToolbar": true,
        "placeholder": "Enter text here...",
        "imageEndPoint": "",
        "toolbar": [
            ["bold", "italic", "underline"],
            ["fontName", "fontSize", "color"],
            ["link"]
        ]
    };

    //vinayak new track
    @Input() examId: any;
    simpleExamTrackForm: FormGroup;
    patternExamTrackForm: FormGroup;

    examType = 1;//for testing
    @Output() close = new EventEmitter<string>();
    isImporedScheme: boolean;
    constructor(private spinner: NgxSpinnerService, private router: Router, private fb: FormBuilder, private modalService: NgbModal, private toastService: ToastsupportService, private examSetterService: AssesmentSetterServiceService, private examService: AssesmentService, private _userService : UserServiceService) {

    }

    ngOnInit() {

        this.initExamTrackForm();
        this.initSimpleExamTrackForm()
        this.initPatternExamTrackForm();
        if (this.examUid != null) {

            // this.getTeacherList();
        }

        this.initMarkingSchemeForm();
        this.initInstructionForm();
        this.getsubjectiveQuestionTypeList();
        this.getObjectiveQuestionTypeList();

        this.examSetterService.rightMarks.subscribe(
            (value: any) => {
                this.rightMarks = value;
                this.isFromEvent = true;
                this.calculateMarks(this.parentIndex, this.childIndex);
            });
        if (this.examId != "") {
            // this.examUid = this.examId;
            // this.getExam(this.examId);
            // this.getTeacherList();
            // this.initExamTrackForm();
        }


    }
    ngOnChanges(changes: SimpleChanges) {
        if (changes['examId'] != null && changes['isEditTrack']) {
            this.getExam(this.examUid);


        } else {
            this.getExam(this.examUid);

        }


    }

    getObjectiveQuestionTypeList() {
        console.log("object");
        this.spinner.show();
        this.examService.getObjectiveQuestionTypeList2().subscribe(
            res => {
                if (res.success == true) {
                    this.spinner.hide();
                    console.log("Objective", res);
                    this.customObjectiveExamQuestionType = res.list;
                }
            },
            (error: HttpErrorResponse) => {
                this.spinner.hide();
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured."); this.spinner.hide();
                // } else {
                //     this.spinner.hide();
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }

    getsubjectiveQuestionTypeList() {
        this.spinner.show();
        this.examService.getsubjectiveQuestionTypeList2().subscribe(
            res => {


                if (res.success == true) {
                    this.spinner.hide();
                    console.log("Objective", res);
                    this.jeeAdvanceSubjectiveQuestionType = res.list;
                }
            },
            (error: HttpErrorResponse) => {
                this.spinner.hide();
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                //     this.spinner.hide();
                // } else {
                //     this.spinner.hide();
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    initTrackList(Qtype: number) {
        return this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            teacherId: [null, Validators.required],
            noOfQuestions: ['', Validators.required],
            optionalQuestions: [0, Validators.required],
            questionType: [Qtype, Validators.nullValidator],
            endDate: [new Date(), Validators.required],
            rightMarks: [1, Validators.required],
            wrongMarks: [0, Validators.nullValidator],
            leaveMarks: [0, Validators.nullValidator],
            instructionDto: this.fb.array([]),
            markingScheme: this.fb.array([]),
            defaultWrongMarks: [0, Validators.nullValidator]
        });
    }


    onSelectFile(e: any) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        let max_size: number = 2085184;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            this.toastService.ShowWarning("invalid format");
            return;
        }
        console.log("imageSrc", this.imageSrc);
        if (e.target.files[0].size > max_size) {
            this.toastService.ShowWarning("Maximum size of 2 MB is allowed..");
            this.instructionForm.controls['instructionTextOrImage'].setValue('');
            return false;
        } else {
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsDataURL(file);
        }
    }
    _handleReaderLoaded(e: any) {
        this.imageSrc = e.target.result;
        this.instructionForm.controls['instructionTextOrImage'].setValue(this.imageSrc);

    }

    getInstructionType() {
        this.instructionForm.controls['instructionTextOrImage'].setValue('');
    }



    getTeacherList() {
        this.teachers = [];
        let serachDto = {
            "orgList": [(sessionStorage.getItem('orgUniqueId'))],
            "roleList": [6, 7],
            "isForAll": false
        };
        return this.examService.getSearchedByUserData(serachDto).subscribe(
            response => {
                this.teachers = response;
            },
            (error: HttpErrorResponse) => {
                // if (error instanceof HttpErrorResponse) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowError(error)
                // }
            });
    }

    pushSubjects(subjects: any) {
        const controlArray = <FormArray>this.examTrackForm.controls['subjects'];
        controlArray.controls = [];
        if (subjects != null && subjects.length > 0) {
            for (let i = 0; i < subjects.length; i++) {
                controlArray.push(this.initSubjectsWithValue(subjects[i]));
            }
        }
    }
    initSubjectsWithValue(subject: any) {
        return this.fb.group({
            uniqueId: [subject.uniqueId, Validators.nullValidator],
            subjectId: [subject.uniqueId, Validators.nullValidator],
            name: [subject.name, Validators.nullValidator],
            totalMarks: [0, Validators.nullValidator],
            trackList: this.fb.array([this.initTrackList(1)])
        });
    }
    initSecSubject(subjects: any) {
        let sectionSubject = [];
        if (subjects != null && subjects.length > 0) {
            for (let i = 0; i < subjects.length; i++) {
                sectionSubject.push(this.fb.group({
                    uniqueId: ['', Validators.nullValidator],
                    subjectId: [subjects[i].subjectId, Validators.nullValidator],
                    examAssignmentSubjectId: [subjects[i].examAssignmentSubjectId, Validators.nullValidator],
                    examAssignmentPatternSubjectId: [subjects[i].examAssignmentPatternSubjectId, Validators.nullValidator],
                    name: [subjects[i].name, Validators.nullValidator],
                    totalQuestions: [subjects[i].totalQuestions, Validators.nullValidator],
                    addedQuestions: [0, Validators.nullValidator],
                    sections: this.fb.array(this.initSections(subjects[i].sectionList))
                }));
            }
        }
        return sectionSubject;
    }
    initSections(sections: any) {
        let sectionArray = [];
        if (sections != null && sections.length > 0) {
            for (let i = 0; i < sections.length; i++) {
                sectionArray.push(this.fb.group({
                    id: [sections[i].uniqueId, Validators.nullValidator],
                    name: [sections[i].name, Validators.nullValidator],
                    noOfQuestions: [sections[i].totalQuestions, Validators.nullValidator],
                    questionType: [sections[i].questionType, Validators.nullValidator],
                    addedQuestions: [0, Validators.nullValidator],
                    trackList: this.fb.array([this.initTrackList(sections[i].questionType)])
                }));
            }
            return sectionArray;
        }
    }
    initExamTrackForm() {
        this.examTrackForm = this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            examUid: [this.examUid, Validators.required],
            totalMarks: [0, Validators.nullValidator],
            examAssignmentType: ['', Validators.required],
            patternType: ['', Validators.nullValidator],
            subjects: this.fb.array([], Validators.nullValidator),
            patternDto: this.examPattern(),
            createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.required],
            organizationId: [sessionStorage.getItem('organizationId'), Validators.nullValidator],
        });
    }




    examPattern() {
        return this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            patternId: ['', Validators.nullValidator],
            patternName: ['', Validators.nullValidator],
            actualQuestions: ['', Validators.nullValidator],
            addedQuestions: [0, Validators.required],
            subjects: this.fb.array([], Validators.nullValidator),
        });
    }
    isCollaps(id, headId) {

        var ele = document.getElementById(id);

        if (ele.classList.contains('isUnhide')) {
            ele.classList.remove("isUnhide");
            ele.classList.add("isHide");

        } else if (ele.classList.contains('isHide')) {
            ele.classList.remove("isHide");
            ele.classList.add("isUnhide");

        }

        var element = document.getElementById(headId);
        if (element.classList.contains('fa-angle-right')) {
            element.classList.remove("fa-angle-right");
            element.classList.add("fa-angle-up");

        } else if (element.classList.contains('fa-angle-up')) {
            element.classList.remove("fa-angle-up");
            element.classList.add("fa-angle-right");

        }

    }
    onOptionalCheck2(id, id2, index1, index2) {


        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[index2];
        // trackGroup.controls['noOfQuestions'].setValue(0);
        if (sectionGroup.controls['totalQuestions'].value == null || sectionGroup.controls['totalQuestions'].value == '') {
            this.toastService.ShowWarning('Please fill first total Question');
            return false;
        }
        let totValue = sectionGroup.controls['totalQuestions'].value;
        let optValue = sectionGroup.controls['optionalQuestions'].value;
        if (sectionGroup.controls['totalQuestions'].value != null) {
            sectionGroup.controls['optionalQuestions'].setValue(totValue - optValue);
        }

        sectionGroup.controls['optionalQuestions'].setValue(0);
        var ele = document.getElementById(id);

        if (ele.classList.contains('col-12')) {
            ele.classList.remove("col-12");
            ele.classList.add("col-6");
            ele.style.paddingRight = '0'

        } else if (ele.classList.contains('col-6')) {
            ele.classList.remove("col-6");
            ele.classList.add("col-12");
            ele.style.paddingRight = '12px'
        }

        var element = document.getElementById(id2);
        if (element.classList.contains('isHideOptional')) {
            element.classList.remove("isHideOptional");
            element.classList.add("isUnhideOptional");
            element.classList.add("col-6");

        } else if (element.classList.contains('col-6')) {
            element.classList.remove("col-6");
            element.classList.remove("isUnhideOptional");
            element.classList.add("isHideOptional");
        }
    }

    checkValidate(index1, index2) {
        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[index2];
        // trackGroup.controls['noOfQuestions'].setValue(0);
        let totValue = sectionGroup.controls['totalQuestions'].value;
        let optValue = sectionGroup.controls['optionalQuestions'].value;



        if (totValue == null || totValue == '') {

            this.toastService.ShowWarning('Please fill total Question');
            sectionGroup.controls['totalQuestions'].setValue(null);
            sectionGroup.controls['optionalQuestions'].setValue(null);
        } else if (totValue <= 0) {
            this.toastService.ShowWarning('total Questions should be more than 0');
            sectionGroup.controls['totalQuestions'].setValue(null);
        } else if (parseInt(optValue) >= parseInt(totValue)) {
            this.toastService.ShowWarning('Optinal questions should be less than total Question');
            sectionGroup.controls['optionalQuestions'].setValue(0);
        }

    }
    instructiionAvailable = true;


    noRadioSelected = true;
    uploadFile(event) {

        var file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
        let max_size: number = 2085184;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            this.toastService.ShowWarning("invalid format");
            return;
        }
        if (event.target.files[0].size > max_size) {
            this.toastService.ShowWarning("Maximum size of 2 MB is allowed..");
            this.instructionForm.controls['instructionTextOrImage'].setValue('');
            return false;
        } else {
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsDataURL(file);
        }


        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let trackGroup = <FormGroup>sectionArray.controls[this.childIndex];
        let instructionGroup = <FormGroup>trackGroup.controls['instructionsDto'];

        var files = event.target.files
        this.files = [];
        for (let index = 0; index < files.length; index++) {
            const element = files[index];
            this.files.push(element.name)

            instructionGroup.controls['files'].setValue(element.name);
            console.log("test", instructionGroup.controls['files'].value)
        }

        console.log("instructionGroup", this.simpleExamTrackForm.value)
    }
    deleteAttachment(index) {
        this.files.splice(index, 1)
        if (index == 0 && this.files.length == 0) {

            let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
            let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
            let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
            let trackGroup = <FormGroup>sectionArray.controls[this.childIndex];
            let instructionGroup = <FormGroup>trackGroup.controls['instructionsDto'];
            instructionGroup.controls['files'].setValue(null);
            this.instructionForm.controls['instructionTextOrImage'].setValue('');
        }
    }
    changeQuestionType() {
        console.log("examTrackForm", this.examTrackForm);

    }
    patchSubMarkingScheme(subMarkingScheme: Array<any>) {
        let subArray = [];
        if (subMarkingScheme != null && subMarkingScheme.length > 0) {
            for (let j = 0; j < subMarkingScheme.length; j++) {
                subArray.push(this.fb.group({
                    correctOption: subMarkingScheme[j].correctOption,
                    rightOption: subMarkingScheme[j].rightOption,
                    wrongOption: subMarkingScheme[j].wrongOption,
                    marks: subMarkingScheme[j].marks,
                }));
            }
        }
        return subArray;
    }

    //vinayak added for new track
    initSimpleExamTrackForm() {
        this.simpleExamTrackForm = this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            trackSubjectList: this.fb.array([], Validators.nullValidator),
            totalMarks: [0, Validators.nullValidator]
        });
    }

    initPatternExamTrackForm() {
        this.patternExamTrackForm = this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            trackSubjectList: this.fb.array([], Validators.nullValidator),
            totalMarks: [0, Validators.nullValidator]
        });
    }


    getExam(examUid: string) {
        this.spinner.show();
        this.examSetterService.getExam(examUid).subscribe(
            res => {
                this.exam = res;
                console.log("this.exam", this.exam)
                this.examTrackForm.controls['examAssignmentType'].setValue(res.isPatternedAssessment == false ? 1 : 2);
                this.simpleExamTrackForm.controls['uniqueId'].setValue(res.uniqueId);
                if (res.isPatternedAssessment == false) {
                    // this.pushSubjects(res.trackSubjectList);
                    //new exam
                    this.pushTrackSimpleExamSubjects(res.trackSubjectList)
                } else {
                    // this.examTrackForm.controls['patternType'].setValue(res.patternType);
                    // let patternDtoGroup = <FormGroup>this.examTrackForm.controls['patternDto'];
                    // patternDtoGroup.controls['patternId'].setValue(res.uniqueId);
                    // patternDtoGroup.controls['patternName'].setValue(res.name);
                    // patternDtoGroup.controls['actualQuestions'].setValue(res.totalMarks);
                    // patternDtoGroup.setControl('subjects', this.fb.array(this.initSecSubject(res.trackSubjectList)));
                    //new exam
                    this.patternExamTrackForm.controls['uniqueId'].setValue(res.uniqueId);
                    this.pushTrackPatternExamSubjects(res.trackSubjectList);
                }
                this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.spinner.hide();
                //     this.toastService.ShowError(error.error.errorMessage);
                // }
                this.spinner.hide();
            });

    }
    pushTrackSimpleExamSubjects(subjects: any) {
        const controlArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        controlArray.controls = [];
        if (subjects != null && subjects.length > 0) {
            for (let i = 0; i < subjects.length; i++) {
                controlArray.push(this.initSimpleExamSubjectTrack(subjects[i]));
                this.getTeachers(subjects[i].uniqueId, controlArray, i)
            }
        }

    }

    initSimpleExamSubjectTrack(subject: any) {
        console.log("test 2")

        return this.fb.group({
            uniqueId: [subject.uniqueId, Validators.nullValidator],
            name: [subject.name, Validators.nullValidator],
            totalMarks: [0, Validators.nullValidator],
            sectionList: this.fb.array([this.initSimpleExamSection()]),
            teacherList: this.fb.array([]),
        });
    }

    initSimpleExamSection() {
        return this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            name: ['', Validators.nullValidator],
            questionType: [null, Validators.nullValidator],
            totalQuestions: ['', Validators.nullValidator],
            optionalQuestions: [0, Validators.nullValidator],
            positiveMarks: [1, Validators.nullValidator],
            negativeMarks: [0, Validators.nullValidator],
            leaveMarks: [0, Validators.nullValidator],
            instructionsDto: this.initInstructionsDto(),
            isInstruction: [false, Validators.nullValidator],
            trackList: this.fb.array([this.initSimplePatternExamSubjectTrackList()]),
            isMarkingScheme: [false, Validators.nullValidator],
            markingSchemeUid: ['', Validators.nullValidator],
        });
    }

    initInstructionsDto() {
        return this.fb.group({
            uniqueId: [null, Validators.nullValidator],
            text: [null, Validators.nullValidator],
            instructionType: [null, Validators.nullValidator],
            name: [null, Validators.nullValidator],
            files: [null, Validators.nullValidator],
        });
    }

    initSimplePatternExamSubjectTrackList() {
        return this.fb.group({
            deadLine: ['', Validators.nullValidator],
            teacherDto: this.initTeacherDto(),
            totalQuestions: ['', Validators.nullValidator],
        });
    }

    initTeacherDto() {
        return this.fb.group({
            uniqueId: [null, Validators.nullValidator],
        });
    }

    getTeachers(uniqueId: any, controlArray, index) {
        console.log(controlArray);
        // for (let i = 0; i < controlArray.length; i++) {
        let controlGroup = <FormGroup>controlArray.controls[index];
        const teachersArray = <FormArray>controlGroup.controls['teacherList'];
        console.log(uniqueId);
        this.getTeachersListBySubject(uniqueId, teachersArray)
        // }

    }
    getTeachersListBySubject(uniqueId, tArray) {

        this._userService.getTeacherListBySubjectList(uniqueId).subscribe(
            res => {
                if (res.succeess) {

                    for (let teachers of res.list) {
                        tArray.push(this.addTeachers(teachers));

                    }

                } else {
                    this.toastService.ShowError(res.responseMessage)
                }
                this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
                // if (error instanceof HttpErrorResponse) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowError(error)
                // }
                this.spinner.hide();
            });
    }

    addTeachers(data: any) {
        console.log("couse group")
        return this.fb.group({
            uniqueId: data.uniqueId,
            name: data.name,

        });
    }

    addSimpleFormControl(subjectIndex: number, trackIndex: number) {
        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[subjectIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[trackIndex];
        if ((sectionGroup.controls['totalQuestions'].value == '' && sectionGroup.controls['totalQuestions'].value == 0) || (sectionGroup.controls['positiveMarks'].value == '' && sectionGroup.controls['positiveMarks'].value == 0)) {
            this.toastService.ShowWarning("Please fill all track values.");
            return false;
        }
        sectionArray.push(this.initSimpleExamSection());
    }

    removeSimpleFormControl(parentIndex: number, childIndex: number) {
        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[childIndex];
        let questions: number = sectionGroup.controls['totalQuestions'].value;
        let marks: number = sectionGroup.controls['positiveMarks'].value;
        subjectGroup.controls['totalMarks'].setValue(subjectGroup.controls['totalMarks'].value - (questions * marks));
        sectionArray.removeAt(childIndex);
    }

    calculateMarks(parentIndex: number, currentIndex: number) {

        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];

        subjectGroup.controls['totalMarks'].setValue(0);
        let trackMarks: number = 0;
        for (let i = 0; i < sectionArray.length; i++) {

            let totalQuestion = 0;
            let trackFormGroup = <FormGroup>sectionArray.controls[i];
            if (trackFormGroup.controls['totalQuestions'].value == 0 && trackFormGroup.controls['totalQuestions'].value != '') {
                this.toastService.ShowWarning("Exam marks should not be greater than 0");
                trackFormGroup.controls['totalQuestions'].setValue('');
                return false;
            }
            if (trackFormGroup.controls['positiveMarks'].value == 0) {
                trackFormGroup.controls['positiveMarks'].setValue('');
                return false;
            }
            if (this.isFromEvent && currentIndex == i) {

                trackFormGroup.controls['positiveMarks'].setValue(this.rightMarks);
            }
            totalQuestion = trackFormGroup.controls['totalQuestions'].value - trackFormGroup.controls['optionalQuestions'].value;

            trackMarks = trackMarks + totalQuestion * trackFormGroup.controls['positiveMarks'].value;
            subjectGroup.controls['totalMarks'].setValue(trackMarks);

        }

        let totalMarks: number = 0;
        for (let i = 0; i < subjectArray.length; i++) {

            let subjectFormGroup = <FormGroup>subjectArray.controls[i];
            totalMarks = totalMarks + subjectFormGroup.controls['totalMarks'].value;
            console.log("totalMarks", totalMarks)

        }
        this.simpleExamTrackForm.controls['totalMarks'].setValue(totalMarks);

        if (totalMarks > parseInt(this.exam.totalMarks)) {

            let trackFormGroup = <FormGroup>sectionArray.controls[currentIndex];

            let lastIndexValue: number = trackFormGroup.controls['totalQuestions'].value;
            console.log("lastIndexValue", lastIndexValue)
            let diffNumber: number = totalMarks - lastIndexValue;
            console.log("diffNumber", diffNumber)
            let actualNumber: number = this.exam.totalMarks - diffNumber;
            console.log("actualNumber", actualNumber)
            totalMarks = diffNumber + actualNumber;
            console.log("totalMarks", totalMarks)
            this.toastService.ShowWarning("You can add only " + actualNumber + " questions");
            trackFormGroup.controls['totalQuestions'].setValue(actualNumber);
            this.simpleExamTrackForm.controls['totalMarks'].setValue(totalMarks);
            let subjectFormGroup = <FormGroup>subjectArray.controls[parentIndex];
            console.log("subjectFormGroup", subjectFormGroup.value)
            subjectFormGroup.controls['totalMarks'].setValue(0);
            let subSectionArray = <FormArray>subjectFormGroup.controls['sectionList'];
            let totalSubMarks: number = 0;
            for (let i = 0; i < subSectionArray.length; i++) {
                let trackFormGroupSubject = <FormGroup>sectionArray.controls[i];
                totalSubMarks = totalSubMarks + parseInt(trackFormGroupSubject.controls['totalQuestions'].value);
            }
            console.log("totalSubMarks", totalSubMarks)

            subjectFormGroup.controls['totalMarks'].setValue(totalSubMarks);
            console.log("subjectFormGroup", subjectFormGroup)
        }


        this.isFromEvent = false;


    }

    setTotal(event, i, j) {
        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[i];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[j];
        let trackArray = <FormArray>sectionGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[0];
        trackGroup.controls['totalQuestions'].setValue(sectionGroup.controls['totalQuestions'].value)
    }

    submit() {
        let formValue;

        this.spinner.show();
        if (this.exam.isPatternedAssessment == false) {
            formValue = this.simpleExamTrackForm.value
        } else if (this.exam.isPatternedAssessment == true) {
            formValue = this.patternExamTrackForm.value
        }
        if (this.exam.isPatternedAssessment == false && formValue.totalMarks != this.exam.totalMarks) {
            this.toastService.ShowWarning("Total track marks must be same as exam marks..");
            this.spinner.hide();
            return false;
        }
        console.log("form Value", formValue)
        // if (this.exam.isPatternedAssessment == true && formValue.patternDto.actualQuestions != formValue.patternDto.addedQuestions) {
        //     this.toastService.ShowWarning("Please complete all subject questions..");
        //     return false;
        // }
        this.examSetterService.addExamTrack(formValue).subscribe(
            res => {
                this.toastService.showSuccess("Tracks added successfully..");
                this.spinner.hide();
                this.close.emit("");
                // this.router.navigate(['/newModule/assesment/track/info', this.examUid]);
            },
            (error: any) => {
                this.spinner.hide();
                // if (error instanceof HttpErrorResponse) {
                //     console.log("Client-side error occured.");

                //     this.spinner.hide();
                // } else {
                //     console.log("error",error);
                //     this.toastService.ShowError(error.message);
                //     this.spinner.hide();
                // }
            });
    }

    getMarkingScheme(scrollDataModal, parentIndex: number, childIndex: number) {
        this.parentIndex = parentIndex;
        this.childIndex = childIndex;

        // let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        // let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        // let trackArray = <FormArray>subjectGroup.controls['trackList'];
        // let trackGroup = <FormGroup>trackArray.controls[childIndex];
        // let markingSchemeArray = <FormArray>trackGroup.controls['markingScheme'];

        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[this.childIndex];
        this.modalService.open(scrollDataModal);


        if (sectionGroup.controls['isMarkingScheme'].value == true) {

            this.isImporedScheme = true;
        } else {
            this.initMarkingSchemeForm();
        }
    }


    initmarkingSchemeCriteriaList() {
        return this.fb.group({
            correctOption: [0, Validators.required],
            rightOption: [0, Validators.required],
            wrongOption: [0, Validators.required],
            marks: [0, Validators.required],
        });
    }

    initMarkingSchemeForm() {
        this.markingSchemeForm = this.fb.group({
            schemeType: ['', Validators.required],
            optionCount: [1, Validators.required],
            name: ['', Validators.required],
            uniqueId: [null, Validators.nullValidator],
            highestMarks: ['', Validators.required],
            leaveMarks: [0, Validators.required],
            defaultMarks: [0, Validators.required],
            isTemplate: [false, Validators.required],
            isImported: [false, Validators.required],

            markingSchemeCriteriaList: this.fb.array([this.initmarkingSchemeCriteriaList()])
        });
    }
    saveimportedScheme() {

        console.log("this.markingSchemeForm add track", this.markingSchemeForm.value)
        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[this.childIndex];
        sectionGroup.controls['markingSchemeUid'].setValue(this.markingSchemeForm.controls['uniqueId'].value);
        sectionGroup.controls['isMarkingScheme'].setValue(true);
        this.markingSchemeForm.controls['isImported'].setValue(true);
        console.log("simpleExamTrackForm", this.simpleExamTrackForm.value)
        this.rightMarks = this.markingSchemeForm.controls['highestMarks'].value;
        this.isFromEvent = true;
        this.calculateMarks(this.parentIndex, this.childIndex)
    }

    saveAndImportScheme(type: any) {
        this.markingSchemeForm.controls['isTemplate'].setValue(type);
        return this.examSetterService.saveMarkingScheme(this.markingSchemeForm.value).subscribe(
            res => {
                if (res) {
                    this.markingSchemeForm.controls['schemeType'].setValue(1),
                        this.markingSchemeForm.controls['uniqueId'].setValue(res.uniqueId);
                    let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
                    let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
                    let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
                    let sectionGroup = <FormGroup>sectionArray.controls[this.childIndex];
                    sectionGroup.controls['isMarkingScheme'].setValue(true);
                    this.markingSchemeForm.controls['isImported'].setValue(true);
                    this.saveimportedScheme();

                    this.modalService.dismissAll();
                }
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }


    checkQuestionType(parentIndex, currentIndex) {
        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup1 = <FormGroup>sectionArray.controls[currentIndex];
        let questionType1 = sectionGroup1.controls['questionType'].value;
        console.log("questionType1", questionType1)
        if (sectionArray.length > 1 && questionType1 != null) {
            for (let j = 0; j < sectionArray.length - 1; j++) {
                if (j != currentIndex) {
                    let sectionGroup2 = <FormGroup>sectionArray.controls[j];
                    let questionType2 = sectionGroup2.controls['questionType'].value;
                    console.log("questionType2", questionType2)
                    if (parseInt(questionType1) == parseInt(questionType2)) {
                        this.toastService.ShowWarning("Question Category can't repeat.");
                        sectionGroup1.controls['questionType'].setValue(null)
                    }
                }

            }
        }

    }

    getInstruction(parentIndex: number, childIndex: number, content: string) {
        this.files = [];
        this.parentIndex = parentIndex;
        this.childIndex = childIndex;

        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let trackGroup = <FormGroup>sectionArray.controls[childIndex];
        let instructionGroup = <FormGroup>trackGroup.controls['instructionsDto'];
        this.modalService.open(content);
        console.log("instructionGroup", instructionGroup)
        if (instructionGroup.controls['text'].value != null) {
            console.log("instructionGroup inside", instructionGroup)
            this.instructionForm.controls['instructionUid'].setValue(instructionGroup.controls['uniqueId'].value);
            this.instructionForm.controls['instructionType'].setValue(instructionGroup.controls['instructionType'].value);
            this.instructionForm.controls['instructionTextOrImage'].setValue(instructionGroup.controls['text'].value);
            this.files.push(instructionGroup.controls['files'].value)
        } else {
            this.initInstructionForm();
        }

    }

    initInstructionForm() {
        this.instructionForm = this.fb.group({
            instructionUid: ['', Validators.nullValidator],
            instructionType: ['', Validators.required],
            instructionTextOrImage: ['', Validators.required],
        });
    }

    resetInstructionValue() {

        this.instructionForm.controls['instructionTextOrImage'].setValue(null)
    }

    saveInstruction() {
        // let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        // let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
        // let trackArray = <FormArray>subjectGroup.controls['trackList'];
        // let trackGroup = <FormGroup>trackArray.controls[this.childIndex];
        // let instructionArray = <FormArray>trackGroup.controls['instructionDto'];

        let subjectArray = <FormArray>this.simpleExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let trackGroup = <FormGroup>sectionArray.controls[this.childIndex];
        let instructionGroup = <FormGroup>trackGroup.controls['instructionsDto'];

        // for (let i = instructionArray.length - 1; i >= 0; i--) {
        //     instructionArray.removeAt(i);
        // }



        instructionGroup.controls['instructionType'].setValue(this.instructionForm.controls['instructionType'].value);
        instructionGroup.controls['uniqueId'].setValue(this.instructionForm.controls['instructionUid'].value)
        instructionGroup.controls['text'].setValue(this.instructionForm.controls['instructionTextOrImage'].value);
        // instructionArray.push(this.fb.group({
        //     instructionUid: this.instructionForm.controls['instructionUid'].value,
        //     instructionType: this.instructionForm.controls['instructionType'].value,
        //     instructionTextOrImage: this.instructionForm.controls['instructionTextOrImage'].value
        // }));
        trackGroup.controls['isInstruction'].setValue(true);
        console.log("this.simpleExamTrackForm", this.simpleExamTrackForm.value)
    }

    //Pattern exam track
    pushTrackPatternExamSubjects(subjects: any) {

        const controlArray = <FormArray>this.patternExamTrackForm.controls['trackSubjectList'];
        controlArray.controls = [];
        if (subjects != null && subjects.length > 0) {
            for (let i = 0; i < subjects.length; i++) {
                controlArray.push(this.initPatternExamSubjectTrack(subjects[i], i));
                this.getTeachers(subjects[i].uniqueId, controlArray, i);
                this.initPatternExamSection(subjects[i].sectionList, controlArray)
            }
        }

    }

    initPatternExamSubjectTrack(subject: any, index) {

        return this.fb.group({
            uniqueId: [subject.uniqueId, Validators.nullValidator],
            name: [subject.name, Validators.nullValidator],
            totalQuestionsCount: [subject.totalQuestions, Validators.nullValidator],
            totalAddedQuestions: [0, Validators.nullValidator],
            sectionList: this.fb.array([]),
            teacherList: this.fb.array([]),
        });
    }

    initPatternExamSection(sections, controlArray) {

        for (let i = 0; i < controlArray.length; i++) {
            let controlGroup = <FormGroup>controlArray.controls[i];
            const sectionArray = <FormArray>controlGroup.controls['sectionList'];
            sectionArray.controls = [];

            if (sections != null && sections.length > 0) {
                for (let i = 0; i < sections.length; i++) {
                    sectionArray.push(this.initPatternSectionList(sections[i]));
                }
            }

        }
        console.log("this.patternExamTrackForm", this.patternExamTrackForm.value)

    }

    initPatternSectionList(section) {
        return this.fb.group({
            uniqueId: [section.uniqueId, Validators.nullValidator],
            name: [section.name, Validators.nullValidator],
            // totalFixedQuestions: [section.totalQuestions, Validators.nullValidator],
            totalQuestions: [section.totalQuestions, Validators.nullValidator],
            trackList: this.fb.array([this.initSimplePatternExamSubjectTrackList()]),
        });
    }

    removePatternControl(index1: number, index2: number, index3: number) {

        let subjectArray = <FormArray>this.patternExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[index2];
        let trackArray = <FormArray>sectionGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[index3];
        let value: number = trackGroup.controls['totalQuestions'].value;
        sectionGroup.controls['totalQuestions'].setValue(sectionGroup.controls['totalQuestions'].value - value);
        subjectGroup.controls['totalAddedQuestions'].setValue(subjectGroup.controls['totalAddedQuestions'].value - value);
        trackGroup.controls['totalQuestions'].value
        trackArray.removeAt(index3);
        console.log('this.patternExamTrackForm.', this.patternExamTrackForm.value)
    }

    addPatternControl(index1: number, index2: number, index3: number) {
        console.log("addPaternControl");
        let subjectArray = <FormArray>this.patternExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];
        let sectionGroup = <FormGroup>sectionArray.controls[index2];
        let trackArray = <FormArray>sectionGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[index3];

        if (trackGroup.controls['totalQuestions'].value == '') {

            this.toastService.ShowWarning("Please fill all track values.");
            return false;
        }
        trackArray.push(this.initSimplePatternExamSubjectTrackList());
    }

    checkQuestionCount(index1: number, index2: number, index3: number, event) {
        //when question subject  add into the api we should validate in track submit method for pattern  
        let subjectArray = <FormArray>this.patternExamTrackForm.controls['trackSubjectList'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];

        let sectionArray = <FormArray>subjectGroup.controls['sectionList'];

        let sectionSecSum: number = 0;
        let trackSum: number = 0;
        let subjectSum: number = 0;
        // let formGroup = <FormGroup>sectionArray.controls[index2];
        // let totalSectionQuestionCount: number = formGroup.controls['totalFixedQuestions'].value;

        // let trackArray = <FormArray>formGroup.controls['trackList'];
        // for (let i = 0; i < sectionArray.length; i++) {debugger


        //     if(i==index2){
        //         for (let j = 0; j < trackArray.length; j++) {
        //             let trackGroup = <FormGroup>trackArray.controls[j];
        //             let questionCount = trackGroup.controls['totalQuestions'].value;
        //             console.log("totalSectionQuestionCount", totalSectionQuestionCount)
        //             console.log("questionCount", questionCount)


        //             if (questionCount == 0 && questionCount != '') {

        //                 this.toastService.ShowWarning("Question count must be greater than zero.");
        //                 trackGroup.controls['totalQuestions'].setValue('');
        //                 // subjectGroup.controls['totalAddedQuestions'].setValue(trackSum);
        //                 return false;
        //             }
        //             if (questionCount == '') {
        //                 trackGroup.controls['totalQuestions'].setValue('');
        //                 // subjectGroup.controls['totalAddedQuestions'].setValue(trackSum);
        //                 return false;
        //             }
        //             trackSum = trackSum + parseInt(questionCount);
        //             console.log("trackSum", trackSum)
        //             if (trackSum > totalSectionQuestionCount) {
        //                 let lastIndexValue: number = trackGroup.controls['totalQuestions'].value;
        //                 console.log("lastIndexValue", lastIndexValue)
        //                 let diffNumber: number = trackSum - lastIndexValue;
        //                 console.log("diffNumber", diffNumber)
        //                 let actualNumber: number = totalSectionQuestionCount - diffNumber;
        //                 console.log("actualNumber", actualNumber)
        //                 trackSum = diffNumber + actualNumber;
        //                 console.log("trackSum", trackSum)
        //                 this.toastService.ShowWarning("You can add only " + actualNumber + " questions");
        //                 trackGroup.controls['totalQuestions'].setValue(actualNumber);
        //             }
        //             console.log("totalQuestions", trackGroup.controls['totalQuestions'].value)
        //         }
        //     }

        //     sectionSum=sectionSum + trackSum;  
        //     console.log("sectionSum", sectionSum)
        // }

        // console.log("sectionSum", sectionSum)  

        // subjectSum = subjectSum + sectionSum
        // subjectGroup.controls['totalAddedQuestions'].setValue(subjectSum);
        // console.log("subjectSum", subjectSum)


        //working code for single section question count 

        for (let i = 0; i < sectionArray.length; i++) {
            let formGroup = <FormGroup>sectionArray.controls[i];
            let totalSectionQuestionCount: number = formGroup.controls['totalQuestions'].value;
            let trackArray = <FormArray>formGroup.controls['trackList'];
            let sectionSum: number = 0;
            for (let j = 0; j < trackArray.length; j++) {
                let trackGroup = <FormGroup>trackArray.controls[j];

                let questionCount = trackGroup.controls['totalQuestions'].value;
                console.log("totalSectionQuestionCount", totalSectionQuestionCount)
                console.log("questionCount", questionCount)


                if (questionCount == 0 && questionCount != '') {
                    console.log("0 sectionSum", sectionSum)
                    this.toastService.ShowWarning("Question count must be greater than zero.");
                    trackGroup.controls['totalQuestions'].setValue('');
                    subjectGroup.controls['totalAddedQuestions'].setValue(sectionSum);
                    return false;
                }
                if (questionCount == '') {
                    trackGroup.controls['totalQuestions'].setValue('');
                    subjectGroup.controls['totalAddedQuestions'].setValue(sectionSum);
                    return false;
                }
                sectionSum = sectionSum + parseInt(questionCount);
                console.log("sectionSum", sectionSum)
                if (sectionSum > totalSectionQuestionCount) {
                    let lastIndexValue: number = trackGroup.controls['totalQuestions'].value;
                    console.log("lastIndexValue", lastIndexValue)
                    let diffNumber: number = sectionSum - lastIndexValue;
                    console.log("diffNumber", diffNumber)
                    let actualNumber: number = totalSectionQuestionCount - diffNumber;
                    console.log("actualNumber", actualNumber)
                    sectionSum = diffNumber + actualNumber;
                    console.log("sectionSum", sectionSum)
                    this.toastService.ShowWarning("You can add only " + actualNumber + " questions");
                    trackGroup.controls['totalQuestions'].setValue(actualNumber);
                }
                console.log("totalQuestions", trackGroup.controls['totalQuestions'].value)


            }

            subjectSum = subjectSum + sectionSum

            console.log("subjectSum", subjectSum)

        }
        subjectGroup.controls['totalAddedQuestions'].setValue(subjectSum);

    }

    cancel() {
        if (this.isEditTrack === true) {
            this.closeFromTrackInfo.emit(false)
        } else {
            this.close.emit("cancel");
        }

    }

}
