import { AdvancedFilterComponent } from './../../../components/common/advanced-filter/advanced-filter.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewLearnerRoutingModule } from './new-learner-routing.module';
import { FilerPipe } from './filer.pipe';
import { StudentsComponent } from './students/students.component';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { NgbDropdownModule, NgbModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule, BsDropdownModule, TabsModule } from 'ngx-bootstrap';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CommonComponentModule } from 'src/app/components/common/common-component.module';
import { BulkAssignFormComponent } from './bulk-assign-form/bulk-assign-form.component';
import { BulkuploadComponent } from '../bulkupload/bulkupload.component';
import { ClickOutsideModule } from 'ng-click-outside';


@NgModule({
  declarations: [FilerPipe, StudentsComponent, BulkAssignFormComponent, BulkuploadComponent],
  imports: [
    CommonModule,
    UIModule,
    NgbTypeaheadModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgbModule,
    NewLearnerRoutingModule,
    NgSelectModule,
    BsDatepickerModule.forRoot(),
    DragDropModule,
    NgxSpinnerModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    CommonComponentModule,
    ClickOutsideModule
  ],
  exports: [],
  providers: []
})
export class NewLearnerModule { }
