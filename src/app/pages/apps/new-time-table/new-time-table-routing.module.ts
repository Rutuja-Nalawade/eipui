import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonTopBarComponent } from './common-top-bar/common-top-bar.component';


const routes: Routes = [{
  path: 'lists/:id',
  component: CommonTopBarComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewTimeTableRoutingModule { }
