import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';

@Component({
  selector: 'app-advanced-filter',
  templateUrl: './advanced-filter.component.html',
  styleUrls: ['./advanced-filter.component.scss']
})
export class AdvancedFilterComponent implements OnInit {

  orgModelList = [];
  boardModelList = [];
  academicYearModelList = [];
  academicCalendarModelList = [];
  gradeModelList = [];
  courseModelList = [];
  subjectModelList = [];
  batchModelList = [];
  yearModelList = [];
  batchLists = [];
  subjectLists = [];
  subjectList = [];
  courseList = [];
  orgCourseList = [];
  selectedBoardList = [];
  selectedGradeList = [];
  selectedCourseList = [];
  selectedBatchList = [];
  selectedSubjectList = [];
  selectedYearList = [];
  selectedCalenderList = [];
  selectedRoleList = [];
  selectedBatchGroups = [];
  selectedBatchGroupCategory = [];
  gradeList = [];
  boardList = [];
  orgBoardList: any;
  orgGradeList: any;
  academicYearList = [];
  academicCalendarList = [];
  selectedOrgList = [];
  userData: any
  organizationList = [];
  orgList = [];
  studentList = [];
  roleInfo = [];
  batchGroupList = [];
  batchCategoryList = [];
  searchType: any;
  @Input() filterType: any;
  @Output() closeDropdown = new EventEmitter<string>();
  @Output() applyDropdown = new EventEmitter<string>();


  constructor(private _commonService: NewCommonService, private toastService: ToastsupportService, private _userService: UserServiceService, private _batchService: BatchesServiceService, private _courseService: CourseServiceService, private _gradeService: GradeServiceService, private _boardService: BoardServiceService, private _subjectService: SubjectServiceService) {
    this.userData = this._commonService.getUserData();
  }

  ngOnInit() {
    // this.getChildOrganizationList(this.userData.orgUniqueId);
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['filterType']) {
      switch (this.filterType) {
        case 1:
          this.getBoardList();
          this.getGradeList();
          this.getCourseList();
          this.getSubjectList();
          this.getBatchList();
          // this.getBatchGroupList();
          // this.getBatchCategoryList();
          this.selectedRoleList.push(8);
          this.searchType = 3;
          break;
        case 2:
          this.searchType = 1;
          break;
        case 3:
          this.selectedRoleList.push(6); this.selectedRoleList.push(7);
          this.searchType = 2;
          break;
        case 4:
          this.getBoardList();
          this.getGradeList();
          this.getCourseList();
          this.selectedRoleList.push(9);
          this.searchType = 4;
          break;

        default:
          break;
      }
    }
  }
  getBoardList() {
    return this._boardService.getAcademicBoardsList().subscribe(
      res => {
        if (res) {
          this.orgBoardList = res;
          if (this.orgBoardList != null && this.orgBoardList.length > 0) {
            let boardList = [];

            boardList = this.orgBoardList;
            this.boardList = boardList
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  getBatchGroupList() {
    // return this._batchService.getAcademicBoardsList().subscribe(
    //   res => {
    //     if (res) {
    //       this.orgBoardList = res;
    //       if (this.orgBoardList != null && this.orgBoardList.length > 0) {
    //         let boardList = [];

    //         boardList = this.orgBoardList;
    //         this.boardList = boardList
    //       }
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {

    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  getBatchCategoryList() {
    return this._batchService.getGroupCategoryList().subscribe(
      res => {
        if (res) {
          let categoryList = res;
          if (categoryList != null && categoryList.length > 0) {
            let list = [];

            list = categoryList;
            this.batchCategoryList = list
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  getBatchGroupListByCategory() {
    this.batchGroupList = [];
    for (let i = 0; i < this.selectedBatchGroupCategory.length; i++) {
      const element = this.selectedBatchGroupCategory[i];
      this._batchService.getBatchGroupList(element).subscribe(
        res => {
          if (res) {
            let categoryList = res;
            if (categoryList != null && categoryList.length > 0) {
              let list = [];

              list = categoryList;
              this.batchGroupList = list
            }
          }
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
        });
    }
  }
  getBoardListFromCourse() {
    this.boardList = [];
    this._boardService.getBoardByCourseUid(this.selectedCourseList).subscribe(
      res => {
        if (res) {
          this.orgBoardList = res;
          if (this.orgBoardList != null && this.orgBoardList.length > 0) {
            let boardList = [];
            for (let i = 0; i < this.orgBoardList.length; i++) {
              const element = this.orgBoardList[i];
              if (element.boards && element.boards.length > 0) {
                boardList = element.boards;
              }
            }
            this.boardList = (this.boardList.length == 0) ? boardList : this.boardList.concat(boardList);
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }

  getGradeList() {
    this.gradeList = [];
    this._gradeService.getAcademicGradeList().subscribe(
      res => {
        if (res.success == true) {
          this.orgGradeList = res.list;
          if (this.orgGradeList != null && this.orgGradeList != null && this.orgGradeList.length > 0) {
            let gradeList = [];

            // for (let i = 0; i < this.orgGradeList.length; i++) {
            //   const element = this.orgGradeList[i];
            //   if (element.boardDto) {
            //     gradeList.push(element.boardDto);
            //   }
            // }
            this.gradeList = res.list;

          }else{
            this.toastService.showSuccess(res.responseMessage);
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  getGradeListFromBoard() {
    this.gradeList = [];

    this._gradeService.getAcademicGradeListByBoard(this.selectedBoardList).subscribe(
      res => {
        if (res.success == true) {
          this.orgGradeList = res.list;
          if (this.orgGradeList != null && this.orgGradeList != null && this.orgGradeList.length > 0) {
            let gradeList = [];
            for (let i = 0; i < res.list[0].courseList.length; i++) {
              const element = res.list[0].courseList[i];

              if (this.selectedCourseList.length > 0) {

                if (this.selectedCourseList.includes(element.courseDto.uniqueId) && element.grades && element.grades.length > 0) {

                  gradeList = (gradeList.length == 0) ? element.grades : gradeList.concat(element.grades);
                }
              } else {
                if (element.grades && element.grades.length > 0) {

                  gradeList = (gradeList.length == 0) ? element.grades : gradeList.concat(element.grades);
                }

              }
            }
            // this.orgGradeList = gradeList;
            this.gradeList = gradeList;
          }
        }else{
          this.toastService.showSuccess(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
  getSubjectList() {
    return this._subjectService.getSubjectList().subscribe(
      res => {
        if (res.success == true) {
          let subjectList = res.list;
          if (subjectList != null && subjectList != null && subjectList.length > 0) {
            this.subjectList = subjectList;
          }
        } else {
          this.toastService.ShowWarning('No subjects found')
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  getBatchList() {
    return this._batchService.getBatchList().subscribe(
      res => {
        if (res.success == true) {
          let batchLists = res.list;
          if (batchLists != null && batchLists != null && batchLists.length > 0) {
            let batchList = [];
            for (let i = 0; i < batchLists.length; i++) {
              if (batchLists[i].batches && batchLists[i].batches.length > 0) {
                batchList = (batchList.length == 0) ? batchLists[i].batches : batchList.concat(batchLists[i].batches);
                // batchList.push(batchLists[i].batches);
              }
            }
            this.batchLists = batchList;
          }
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }

  onModalItemSelect(item, type) {
    let selectedItem = [];
    let id: number;
    for (let index = 0; index < item.length; index++) {
      const element = item[index];
      selectedItem.push(element.uniqueId)
    }
    switch (type) {
      case 1:
        this.selectedOrgList = selectedItem;
        break;
      case 2:
        this.selectedYearList = selectedItem;
        this.selectedCalenderList = [];
        if (selectedItem.length > 0) {
        } else {
          this.academicCalendarList = [];
          this.academicCalendarModelList = [];
          this.courseList = [];
          this.courseModelList = [];
          this.batchLists = [];
          this.batchModelList = [];
          this.subjectList = [];
          this.subjectModelList = [];
        }
        break;
      case 3:
        this.selectedCalenderList = selectedItem;
        break;
      case 4:
        this.selectedBoardList = selectedItem;
        this.selectedGradeList = [];
        this.gradeList = [];
        if (selectedItem.length > 0) {
          this.getGradeListFromBoard();
        } else {
          this.getGradeList();
        }
        break;
      case 5:
        this.selectedGradeList = selectedItem;
        break;
      case 6:
        this.selectedCourseList = selectedItem;
        this.selectedBatchList = [];
        this.selectedSubjectList = [];
        this.selectedBoardList = [];
        this.selectedGradeList = [];
        this.gradeList = [];
        this.boardList = [];
        this.batchLists = [];
        this.subjectLists = [];
        if (selectedItem.length > 0) {
          this.getBacthesFromCourseList(this.selectedCourseList, selectedItem);
          this.getSubjectsFromCourseList();
          this.getBoardListFromCourse();
        } else {
          this.batchModelList = [];
          this.subjectModelList = [];
          this.getBoardList();
          this.getSubjectList();
          this.getGradeList();
          this.getBatchList();
        }
        break;
      case 7:
        this.selectedBatchList = selectedItem;
        break;
      case 8:
        this.selectedSubjectList = selectedItem;
        break;
      case 9:
        this.selectedBatchGroupCategory = selectedItem;
        this.selectedBatchGroups = [];
        if (selectedItem.length > 0) {
          this.getBatchGroupListByCategory();
        }
        break;
      case 10:
        this.selectedBatchGroups = selectedItem;
        break;
    }
  }
  getBacthesFromCourseList(alreadySelected, currentSelected) {
    this.batchLists = [];
    for (let i = 0; i < this.selectedCourseList.length; i++) {
      const element = this.selectedCourseList[i];
      this._batchService.getBatchByCourse(element).subscribe(
        res => {
          if (res.success == true) {
            let batchList = res.list;
            if (batchList != null && batchList.length > 0) {
              this.batchLists = (this.batchLists.length == 0) ? batchList : this.batchLists.concat(batchList);
            }
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }

        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          // this.toastService.ShowError(error);
          // }
        });

    }

  }

  getSubjectsFromCourseList() {
    let filterDto = {
      "courseList": this.selectedCourseList
    };
    this.subjectList = [];
    this._subjectService.getSubjectsByCourses(this.selectedCourseList).subscribe(
      response => {
        if (response) {
          if (response.responseDto.success) {
            let subjectList = response;
            if (subjectList.subjectsDtoList && subjectList.subjectsDtoList != null && subjectList.subjectsDtoList.length > 0) {
              this.subjectList = (this.subjectList.length == 0) ? subjectList.subjectsDtoList : this.subjectList.concat(subjectList.subjectsDtoList);
            }
          } else {
            this.toastService.ShowWarning(response.responseDto.responseMessage);
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
  getCourseList() {
    this._courseService.getCourseDetailsedInfoList(3).subscribe(
      response => {
        if (response.success == true) {
          // console.log("get courseList => ",res)
          this.orgCourseList = response.list;
          if (this.orgCourseList.length > 0) {
            let courseList = [];
            for (let i = 0; i < this.orgCourseList.length; i++) {
              let dto = {
                "id": this.orgCourseList[i].id, "itemName": this.orgCourseList[i].name, "uniqueId": this.orgCourseList[i].uniqueId
              };
              courseList.push(dto);
            }
            this.courseList = courseList;
          }
        } else {
          this.toastService.ShowWarning(response.responseMessage)
        }

      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }

  cancelFilterModal() {
    this.selectedOrgList = [];
    this.orgModelList = [];
    this.boardModelList = [];
    this.academicYearModelList = [];
    this.academicCalendarModelList = [];
    this.gradeModelList = [];
    this.courseModelList = [];
    this.subjectModelList = [];
    this.batchModelList = [];
    this.yearModelList = [];
    this.selectedBoardList = [];
    this.selectedGradeList = [];
    this.selectedCourseList = [];
    this.selectedBatchList = [];
    this.selectedSubjectList = [];
    this.selectedYearList = [];
    this.selectedCalenderList = [];
    this.selectedRoleList = [];
    // this._learnersService.filterData.next(null);
    this.closeDropdown.emit()
  }
  apply() {
    if (this.filterType == 2 && this.selectedRoleList.length == 0) {
      this.toastService.ShowError("Please select atleast one role")
      return;
    } else if (this.filterType == 1 && this.selectedOrgList.length == 0) {
      // this.toastService.ShowError("Please select organization.")
      // return;
    }
    // let searchDto = {
    //   "orgList": this.selectedOrgList,
    //   "roleList": this.selectedRoleList,
    //   "pageIndex": 0,
    //   "boardList": this.selectedBoardList,
    //   "gradeList": this.selectedGradeList,
    //   "courseList": this.selectedCourseList,
    //   "batchList": this.selectedBatchList,
    //   "yearList": this.selectedYearList,
    //   "subjectList": this.selectedSubjectList,
    //   "academicCalendarList": this.selectedCalenderList,
    //   "searchType": this.searchType
    // };
    let searchDto = {
      "courseIds": this.selectedCourseList,
      "groupCategoriesIds": [],
      "batchGroupIds": [],
      "batchIds": this.selectedBatchList,
      "gradeIds": this.selectedGradeList,
      "boardIds": this.selectedBoardList,
      "subjectIds": this.selectedSubjectList,
      "page": 0,
      "size": 10,
      "unassigned": false,
      "states": [1, 2],
      "responseFields": {
        "courseField": true,
        "batchFiled": true,
        "boardField": true,
        "gradeField": true,
        "subjectField": true,
        "groupCategoryField": true,
        "batchGroupFiled": false
      }
    };
    switch (this.filterType) {
      case 1:
        this._commonService.filterData.next(searchDto);
        break;
      case 2:
        // this._staffService.filterData.next(searchDto);
        break;
      case 3:
        // this._teacherService.filterData.next(searchDto);
        break;
      case 4:
        // this._parentService.filterData.next(searchDto);
        break;

      default:
        break;
    }

    this.applyDropdown.emit();
    this.cancelFilterModal();
  }

}
