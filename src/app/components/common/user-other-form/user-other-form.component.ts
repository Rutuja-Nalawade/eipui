import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-user-other-form',
  templateUrl: './user-other-form.component.html',
  styleUrls: ['./user-other-form.component.scss']
})
export class UserOtherFormComponent implements OnInit {

  stateList = [];
  submitted = false;
  userData: any;
  loding = false;
  // otherFormGroup: FormGroup;
  currentUserId: Number = null;
  saveState: number = 0;
  studentAddressInfo: any;
  tabNumber: Number = 0;
  maxDate: Date;
  countryList = [{ id: 1, name: "India" }]
  userType = ["", "student", "staff", "teacher", "guardian"]
  @Input() public userId: Number;
  @Input() public userDetailsType: any;
  @Input() public otherFormGroup: FormGroup;
  @Input() userUniqueId: any;
  @Output() nextTab = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private _userService: UserServiceService, private datePipe: DatePipe) {
    this.userData = this._commonService.getUserData();
    this.currentUserId = 114;
    // this.currentUserId=localStorage.getItem("selectedStudentId");
    this.currentUserId = this.userId;
    this.maxDate = new Date();
    // this.initStudentAddressForm();
    this._commonService.isotherClicked.subscribe(tabNo => {
      if (tabNo == '2' || tabNo == 2) {
        // this.getUserAddress(this.currentUserId)
        this.tabNumber = tabNo
      }

    })
  }

  ngOnInit() {
    this.currentUserId = this.userId;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['userDetailsType']) {
      this.currentUserId = this.userId;
      this.getStateList();
      // this.getUserAddress(this.userId.toString())
    }
  }
  getStateList() {
    this._userService.getStateLiist().subscribe(
      response => {
        this.stateList = response;
        // this.childOrganizationList.push(orgDto);
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  get addressF() {
    return this.otherFormGroup.controls;
  }

  patchStudentAddressInfo(studentAddressInfo: any) {
    let countryId = studentAddressInfo.countryId == null ? "1" : studentAddressInfo.countryId;
    let stateId = studentAddressInfo.stateId == null ? "" : studentAddressInfo.stateId;
    let bloodGroup = studentAddressInfo.bloodGroup == null ? "A+" : studentAddressInfo.bloodGroup;
    // this.otherFormGroup.controls['userUniqueId'].setValue(studentAddressInfo.userUniqueId);
    this.otherFormGroup.controls['userUniqueId'].setValue(this.currentUserId);
    this.otherFormGroup.controls['address'].setValue(studentAddressInfo.address);
    this.otherFormGroup.controls['address2'].setValue("");
    this.otherFormGroup.controls['stateId'].setValue(stateId);
    this.otherFormGroup.controls['countryId'].setValue(countryId);
    this.otherFormGroup.controls['zipcode'].setValue(studentAddressInfo.zipcode);
    this.otherFormGroup.controls['createdOrUpdatedByUniqueId'].setValue(this.userData.uniqueId);
    this.otherFormGroup.controls['city'].setValue(studentAddressInfo.city);
    this.otherFormGroup.controls['birthDate'].setValue(studentAddressInfo.birthDate);
    this.otherFormGroup.controls['bloodGroup'].setValue(bloodGroup);
    this.otherFormGroup.controls['adharNumber'].setValue(studentAddressInfo.adharNumber);
    this.otherFormGroup.controls['setMap'].setValue("");
    this.otherFormGroup.controls['userId'].setValue(studentAddressInfo.userId);
  }
  saveAddressInfo() {

    this.submitted = true;
    console.log(this.otherFormGroup);

    if (this.otherFormGroup.invalid) {
      // this.scrollToError();
      return;
    }
    let otherData = this.otherFormGroup.value;
    let dto = {
      address: {
        countryId: otherData.countryId,
        address: otherData.address,
        stateId: otherData.stateId,
        city: otherData.city,
        zipcode: otherData.zipcode
      },
      bloodGroup: otherData.bloodGroup,
      birthDate: this.datePipe.transform(otherData.birthDate, "yyyy-MM-dd'T'HH:mm:ss"),
      adharNumber: otherData.adharNumber
    };
    this.spinner.show();
    let usertype = this.userType[this.userDetailsType];
    // if (otherData.isAdd) {
    //   this._userService.addUserOtherInfo(usertype, this.userUniqueId, dto).subscribe(
    //     response => {
    //       switch (this.userDetailsType) {
    //         case 1:
    //           this.toastService.showSuccess("Student address info saved successfully.");
    //           this.nextTab.emit("3")
    //           break;
    //         case 2:
    //           this.toastService.showSuccess("Staff others info saved successfully.");
    //           this.nextTab.emit("6");
    //           break;
    //         case 3:
    //           this.toastService.showSuccess("Teacher other info saved successfully.");
    //           this.nextTab.emit("5");
    //           break;
    //       }
    //       this.submitted = false;
    //       this.spinner.hide()
    //     },
    //     (error: HttpErrorResponse) => {
    //       if (error instanceof HttpErrorResponse) {
    //         this.loding = false;
    //       } else {
    //         this.toastService.ShowError(error);
    //         this.loding = false;
    //       }
    //       this.spinner.hide();
    //     }
    //   );
    // } else {
    this._userService.updateUserOtherInfo(usertype, this.userUniqueId, dto).subscribe(
      response => {
        switch (this.userDetailsType) {
          case 1:
            this.toastService.showSuccess("Student address info saved successfully.");
            this.nextTab.emit("3")
            break;
          case 2:
            this.toastService.showSuccess("Staff others info saved successfully.");
            this.nextTab.emit("6");
            break;
          case 3:
            this.toastService.showSuccess("Teacher other info saved successfully.");
            this.nextTab.emit("5");
            break;
        }
        this.submitted = false;
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          this.loding = false;
        } else {
          this.toastService.ShowError(error);
          this.loding = false;
        }
        this.spinner.hide();
      }
    );
    // }
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }
  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    this.scrollTo(firstElementWithError);
  }
  close() {
    this._commonService.iscloseClicked.next(true)
  }

}
