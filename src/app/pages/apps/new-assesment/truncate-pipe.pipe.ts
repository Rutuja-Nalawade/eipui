import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncatePipe'
})
export class TruncatePipePipe implements PipeTransform {

  transform(value: string, limit = 30, completeWords = false, ellipsis = '...') {
    console.log('inside truncate pipe value===>>>',value);
    console.log('inside truncate pipe limit===>>>',limit);
    if (completeWords) {
      limit = value.substr(0, limit).lastIndexOf(' ');
    }
    return value.length > limit ? value.substr(0, limit) + ellipsis : value;
  }

}
