import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewOrganizationRoutingModule } from './new-organization-routing.module';
import { MyOrganizationComponent } from './my-organization/my-organization.component';
import { CommonTopBarComponent } from './common-top-bar/common-top-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { RoleComponent } from './role/role.component';


@NgModule({
  declarations: [MyOrganizationComponent, CommonTopBarComponent, RoleComponent],
  imports: [
    CommonModule,
    UIModule,
    FormsModule,
    ReactiveFormsModule,
    NewOrganizationRoutingModule
  ]
})
export class NewOrganizationModule { }
