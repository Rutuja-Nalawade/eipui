import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
//import { AcademicService } from '../academic.service';

import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-academic-batches',
  templateUrl: './academic-batches.component.html',
  styleUrls: ['./academic-batches.component.scss']
})
export class AcademicBatchesComponent implements OnInit {
  @Input() currentTab: Number;
  batchList: any = [];
  courseList: any = [];
  batchFormEdit: FormGroup;
  // batchDto:any;
  isShowBatchForm: boolean = false;
  CourseName: any;
  isShowBatchClassForm: boolean = false;
  isaddedit: boolean = false;
  constructor(private _batchesServiceService: BatchesServiceService, private _courseServiceService: CourseServiceService,
    private toastService: ToastsupportService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService) { }

  ngOnChanges(changes: SimpleChanges) {
    console.log("currentValue", changes['currentTab']);

    if (changes['currentTab'].currentValue == 6) {
      // console.log("this.curremt tab",this.currentTab)
      // this.isEditAdd=false;
      this.isShowBatchForm = false;
      this.isShowBatchClassForm = false;
      this.getBatchList();

      this.initBatchEditForm();
    }
  }


  ngOnInit() {
  }

  initBatchEditForm() {
    this.batchFormEdit = this.formBuilder.group({
      courseUid: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      uniqueId: new FormControl('', Validators.required),
    });
  }

  getBatchList() {
    this.spinner.show()
    this.batchList = []
    return this._batchesServiceService.getBatchList().subscribe(
      res => {
        if (res.success == true) {
          console.log("get batchlist => ", res)
          this.batchList = res.list;
          // this.batchDto = res;
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide()
      });
  }

  getCourseList() {
    this.courseList = []
    this.spinner.show()
    return this._courseServiceService.getCourseBasicInfoList(environment.COURSE_NOT_EXPIRED).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            // console.log("get courseList => ",res)
            this.courseList = res.list;
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
        }
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide()
      });
  }


  saveBatch() {
    this.spinner.show()
    console.log("get batchlist => ", this.batchFormEdit.value)
    this._batchesServiceService.saveBatch(this.batchFormEdit.value).subscribe(
      response => {
        this.getBatchList();
        this.isShowBatchClassForm = false;
        this.isShowBatchForm = false;
        this.toastService.showSuccess("Batch save successfully");
        this.spinner.hide()
        this.batchFormEdit.reset();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   // this.dataLoading = false;
        // } else {
        //   // this.dataLoading = false;
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide()
      }
    );

  }
  courseSelectSaveBatch() {
    this.spinner.show()
    console.log("get batchlist => ", this.batchFormEdit.value)
    this._batchesServiceService.saveBatch(this.batchFormEdit.value).subscribe(
      response => {
        this.resetBatchEdit()
        this.getBatchList();
        this.isShowBatchClassForm = false;
        this.isShowBatchForm = false;
        this.toastService.showSuccess("Batch save successfully");
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   // this.dataLoading = false;
        // } else {
        //   // this.dataLoading = false;
        //   this.toastService.ShowError(error);
        // }

        this.spinner.hide()
      }
    );

  }

  resetBatchEdit() {
    this.batchFormEdit.patchValue({
      courseUid: null,
      name: null,
    })
  }


  updateBatch() {
    this.spinner.show()
    console.log('update batch=>', this.batchFormEdit.value)
    this._batchesServiceService.updateBatch(this.batchFormEdit.value).subscribe(
      response => {
        this.getBatchList();
        this.isShowBatchClassForm = false;
        this.isShowBatchForm = false;
        this.toastService.showSuccess("Batch update successfully");
        this.spinner.hide()
        this.batchFormEdit.reset();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   // this.dataLoading = false;
        // } else {
        //   // this.dataLoading = false;
        //   this.toastService.ShowError(error);
        // }

        this.spinner.hide()
      }
    );

  }

  addBatch() {
    this.isShowBatchClassForm = true;
  }

  addEditBatch(data, isaddedit, batch) {
    console.log("data", data);
    this.CourseName = data.course.name;
    this.batchFormEdit.patchValue({
      courseUid: data.course.uniqueId
    })
    this.isaddedit = isaddedit;


    if (isaddedit) {

      this.batchFormEdit.patchValue({
        courseUid: data.course.uniqueId,
        name: batch.name,
        uniqueId: batch.uniqueId,
      })

    }

    this.isShowBatchForm = true;
  }



  closeBatchForm() {
    this.isShowBatchForm = false;
    this.isShowBatchClassForm = false;
    this.resetBatchEdit();
  }

  openConfirm(content) {

    this.modalService.open(content, { centered: true });
  }

  removeBatch() {

    this.spinner.show()
    console.log("delete batch=>", this.batchFormEdit.controls['uniqueId'].value);
    this._batchesServiceService.deleteBatch(this.batchFormEdit.controls['uniqueId'].value).subscribe(
      response => {
        this.isShowBatchClassForm = false;
        this.isShowBatchForm = false;
        this.getBatchList();
        this.toastService.showSuccess("Batch removed successfully");
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // console.log('error recived' + error)
        // if (error instanceof HttpErrorResponse) {
        //   // this.dataLoading = false;
        // } else {
        //   // this.dataLoading = false;
        //   this.toastService.ShowError(error);
        // }

        this.spinner.hide()
      }
    );
  }



}
