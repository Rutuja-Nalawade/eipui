import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Widget, UserBalance, RevenueData, ChartType } from './default.model';
import { widgetData, salesMixedChart, revenueRadialChart, userBalanceData, revenueData } from './data';
import { StaffService } from '../staff.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';

@Component({
  selector: 'app-work-time',
  templateUrl: './work-time.component.html',
  styleUrls: ['./work-time.component.scss']
})
export class WorkTimeComponent implements OnInit {
  panelOpenState = false;
  breadCrumbItems: Array<{}>;
  userData:any={}
  currentUserId: Number = null;
  widgetData: Widget[];
  userBalanceData: UserBalance[];
  revenueData: RevenueData[];
  salesMixedChart: ChartType;
  revenueRadialChart: ChartType;
  @Input() userId:Number;
  @Input() currentTab:Number;
  shift:any=[];
  saveList: any;
  constructor(private _educatersService:StaffService,private spinner:NgxSpinnerService, private _commonService:NewCommonService) {
    this.userData = this._commonService.getUserData();
    this.currentUserId=this.userId;
   }

  ngOnInit() {
    console.log("date",moment("16206000").format('LT'));
    this.breadCrumbItems = [{ label: 'UBold', path: '/' }, { label: 'Dashboard', path: '/', active: true }];
    this.currentUserId=this.userId;
    /**
     * fetches data
     */
    this._fetchData();
    // this.getShift();
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log("currentValue",changes['currentTab']);
    
    if (changes['userId'] || changes['currentTab'].currentValue== 3) {
      // this.getRoleInfo();
      this.getShift(this.userId)
    }
  }

  private _fetchData() {

    this.widgetData = widgetData;
    this.salesMixedChart = salesMixedChart;
    this.revenueRadialChart = revenueRadialChart;
    this.userBalanceData = userBalanceData;
    this.revenueData = revenueData;
  }

  getShift(Id){
    // let Id="22";
    this.spinner.show();
        this._educatersService.getShift(Id).subscribe(
            response => {
                this.shift = response.shiftsLists;
                this.saveList = response.shiftsLists;
                console.log("hgdjJ",this.shift);
                // this.shift.forEach(element => {
                //   element.startTime=moment(element.startTime).format('LT');
                //   element.endTime=moment(element.endTime).format('LT');
                //   // if(element.shiftbreaksList){
                //   //   element.shiftbreaksList.forEach(element1 => {
                //   //     console.log("element1",element1);
                //   //     element1.startTime=moment(element1.startTime).format('LT');
                //   //     element1.endTime=moment(element1.endTime).format('LT');
                //   //   });
                //   // }
                 
                // });
                console.log("hgdjJ",this.shift);
                
                this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              
                this.spinner.hide();
            }
        );
    }

    saveShift(){
      const body={
        orgUid:this.userData.orgUniqueId,
        createdOrUpdatedByUid: this.userData.uniqueId,
        userId: this.userId,
        shiftsLists:this.saveList,
        userUid: ""
      }

      
      // const body1= {orgUid:"025db97b-cec3-43e9-9728-6750abf48610",
      // userUid:"",
      // createdOrUpdatedByUid:"1e44aa82-5e42-49fd-b424-148b7ea8d542",
      // userId:203,
      // shiftsLists:[{shiftUid:"34928787-ca63-4531-b166-ce07167b3909",isLinked:false,shiftName:"Morning"}]}

      this.spinner.show();
      this._educatersService.addShift(body).subscribe(
          response => {
             console.log("response",response)
              this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            
              this.spinner.hide();
          }
      );
    }

    isSave(item){
      console.log("item",item);
      this.saveList.forEach(element => {
        if(element.id === item.id){
          if(element.isLinked==false){
            element.isLinked=true;
          }else{
            element.isLinked=false;
          }
         
        }
       
      });
    }
}
