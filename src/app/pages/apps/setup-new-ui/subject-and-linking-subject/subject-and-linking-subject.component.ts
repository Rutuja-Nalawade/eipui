import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges, TemplateRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SetupComponent } from 'src/app/account/new-authentication/setup/setup.component';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { CommonDto, LinkedSubjects, Subjects } from 'src/app/pages/dashboards/dashboard3/dashboar3.model';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-subject-and-linking-subject',
  templateUrl: './subject-and-linking-subject.component.html',
  styleUrls: ['./subject-and-linking-subject.component.scss']
})
export class SubjectAndLinkingSubjectComponent implements OnInit {

  userData: any;
  loading: Boolean = false;
  // subjectLinkingSubjectForm: FormGroup;
  submitted: boolean = false;
  oldRow: any;
  startDate: any;
  endDate: any;
  currentEditRowIndex: any;
  academicCalendars = [];
  academicYearList = [];
  minDate: Date;
  settings = {};
  isNewRowAdd = false;
  duplicateRecords = [];
  linkSubjectDto: LinkedSubjects;
  subjectList: Subjects;
  compulsorySubjectDropList = [];
  optionalSubjectList = [];
  electiveSubjectDropList = [];
  subjectListForCollege = [];
  masterSubjectList = [];
  alreadySelectedSubjectList = [];
  subjectList1 = [];
  subjectForm: FormGroup;
  isSelectAll: boolean = false;
  selectedCount = 0;
  selectedSubject = [];
  @Input() selectedIndex: any;
  @Input() editData: any;
  @ViewChild('subjectList', { static: true }) content: TemplateRef<any>;
  @Input() subjectLinkingSubjectForm: FormGroup;
  @Output() newItemEvent = new EventEmitter<string>();
  @Input() isFromacademic: any;
  @Input() isAdd: any;
  @Input() isFromSetup: any;
  masterCourseList: any = [];
  mastersSubjectUids: any = [];
  selecteedCourseUniqueId: any;
  remainingSubjectCount: any = 0;
  constructor(private setup: SetupComponent, private _subjectServiceService: SubjectServiceService, private _courseServiceService: CourseServiceService, private formBuilder: FormBuilder, private toastService: ToastsupportService, private modalService: NgbModal, private router: Router) {

    this.currentEditRowIndex = null;
  }

  ngOnInit() {
    // this.initLinkSubjectForm();
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("changes['subjectLinkingSubjectForm']", changes['subjectLinkingSubjectForm']);
    this.isAdd = true;
    if (changes['selectedIndex'] && changes['selectedIndex'].currentValue == 2) {
      console.log("value subject", this.subjectLinkingSubjectForm)
      this.isAdd = true;
      this.getAllSubjects();
      console.log('mastercourse called 1')
      this.getCourseListFromMasterOrg();
    } else if (changes['selectedIndex'] && changes['selectedIndex'].currentValue == 3) {
      this.patchSubjectData(this.editData, true, false)
      console.log('mastercourse called 2')
      console.log("this.editData", this.editData)
      if (this.editData.compulsorySubjects == undefined && this.editData.optionalSubjects == undefined && this.editData.electiveGroupsAndSubjects == undefined) {
        this.isAdd = true;
        console.log('mastercourse called 3')
        this.getCourseListFromMasterOrg();
      } else {
        console.log('mastercourse called 4')
        this.isAdd = false;
      }
      this.getAllSubjects();
      // this.getLinkSubjectList(false);
    }
  }
  initLinkSubjectForm() {
    this.subjectLinkingSubjectForm = this.formBuilder.group({
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      orgId: [sessionStorage.getItem('organizationId'), Validators.required],
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      linkedSubjectList: this.formBuilder.array([])
    });
  }



  patchSubjectData(subjectData, isedit, isnew) {

    let courseLinkedGroup = [];
    if (isnew) {
      for (let i = 0; i < subjectData.linkedSubjectList.length; i++) {
        if (this.selectedIndex == 3 && (subjectData.linkedSubjectList[i].courseId == this.editData.courseId)) {
          console.log("test 1")
          let SubjectByCourseFormGroup = this.formBuilder.group({
            courseId: [subjectData.linkedSubjectList[i].uniqueId, Validators.required],
            courseName: [subjectData.linkedSubjectList[i].name, Validators.nullValidator],
            subjectList: this.formBuilder.array(this.patchRemainingSubjects(subjectData.linkedSubjectList[i].remainingSubjectList, [])),
            compulsorySubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.linkedSubjectList[i].compulsorySubjectList)),
            optionalSubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.linkedSubjectList[i].optionalSubjectList)),
            electiveSubjectList: this.formBuilder.array(this.patchElectiveSubjects(subjectData.linkedSubjectList[i].electiveSubjectList)),
            editMode: isedit

          });
          courseLinkedGroup.push(SubjectByCourseFormGroup);
        } else if (this.selectedIndex == 2) {
          console.log("test 2")
          let SubjectByCourseFormGroup = this.formBuilder.group({
            courseId: [subjectData.linkedSubjectList[i].uniqueId, Validators.required],
            courseName: [subjectData.linkedSubjectList[i].name, Validators.nullValidator],
            subjectList: this.formBuilder.array(this.patchRemainingSubjects(subjectData.linkedSubjectList[i].remainingSubjectList, [])),
            compulsorySubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.linkedSubjectList[i].compulsorySubjectList)),
            optionalSubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.linkedSubjectList[i].optionalSubjectList)),
            electiveSubjectList: this.formBuilder.array(this.patchElectiveSubjects(subjectData.linkedSubjectList[i].electiveSubjectList)),
            editMode: isedit
          });
          courseLinkedGroup.push(SubjectByCourseFormGroup);
        }
      }
    } else {
      console.log("test 3")
      if (subjectData) {
        let SubjectByCourseFormGroup = this.formBuilder.group({
          courseId: [subjectData.course.uniqueId, Validators.required],
          courseName: [subjectData.course.name, Validators.nullValidator],
          subjectList: this.formBuilder.array(this.patchRemainingSubjects(subjectData.remainingSubjects, [])),
          compulsorySubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.compulsorySubjects)),
          optionalSubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.optionalSubjects)),
          electiveSubjectList: this.formBuilder.array(this.patchElectiveSubjects(subjectData.electiveGroupsAndSubjects)),
          editMode: isedit
        });
        courseLinkedGroup.push(SubjectByCourseFormGroup);
      } else {
        let SubjectByCourseFormGroup = this.formBuilder.group({
          courseId: [null, Validators.required],
          courseName: [null, Validators.nullValidator],
          subjectList: this.formBuilder.array(this.patchRemainingSubjects(subjectData.remainingSubjects, [])),
          compulsorySubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.compulsorySubjects)),
          optionalSubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData.optionalSubjects)),
          electiveSubjectList: this.formBuilder.array(this.patchElectiveSubjects(subjectData.electiveGroupsAndSubjects)),
          editMode: isedit
        });
        courseLinkedGroup.push(SubjectByCourseFormGroup);

      }

    }
    this.subjectLinkingSubjectForm.setControl('linkedSubjectList', this.formBuilder.array(courseLinkedGroup));

    console.log("value subject", this.subjectLinkingSubjectForm)

    console.log('patchSubjectData' + JSON.stringify(subjectData))
  }

  patchCompulsoryOrOptionalRemainingSubjects(subjectList: Array<any>) {
    let subjectsToPatch = [];
    if (subjectList != null && subjectList.length > 0) {
      for (let i = 0; i < subjectList.length; i++) {
        let subjectGroup = this.formBuilder.group({
          subjectId: subjectList[i].uniqueId,
          name: subjectList[i].name,
          isLinked: subjectList[i].isSelected,
          uniqueId: subjectList[i].uniqueId,
          courseHasSubjectUniqueId: subjectList[i].courseHasSubjectUniqueId
        });
        subjectsToPatch.push(subjectGroup);
      }
    }
    return subjectsToPatch;
  }

  patchRemainingSubjects(subjectList: Array<any>, uIdList) {
    console.log("patchRemainingSubjects", subjectList);
    let subjectsToPatch = [];
    if (subjectList != null && subjectList.length > 0) {
      for (let i = 0; i < subjectList.length; i++) {
        if (!uIdList.includes(subjectList[i].uniqueId)) {
          let subjectGroup = this.formBuilder.group({
            subjectId: subjectList[i].subjectId,
            name: subjectList[i].name,
            isLinked: subjectList[i].isLinked,
            uniqueId: subjectList[i].uniqueId,
            id: subjectList[i].id
          });
          subjectsToPatch.push(subjectGroup);
        }
      }
    }
    return subjectsToPatch;
  }

  getCourseListFromMasterOrg() {
    this._courseServiceService.getCourseUnlinkedList().subscribe(
      res => {
        if (res && res.success) {
          this.masterCourseList = res.list;
          console.log('masterCourseList' + this.masterCourseList)
          this.loading = false;
        } else {
          this.toastService.ShowWarning(res.responseMessage);
          this.masterCourseList = [];
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });
  }

  patchElectiveSubjects(electiveSubjects: Array<any>) {
    let electiveSubjectListToPatch = [];
    if (electiveSubjects != null && electiveSubjects.length > 0) {
      for (let i = 0; i < electiveSubjects.length; i++) {
        let electiveSubjectDto = this.formBuilder.group({
          groupId: electiveSubjects[i].uniqueId,
          groupName: electiveSubjects[i].name,
          uniqueId: electiveSubjects[i].uniqueId,
          electiveSubjectList: this.formBuilder.array(this.patchGroupSubjects(electiveSubjects[i].electiveSubjects))
        });
        electiveSubjectListToPatch.push(electiveSubjectDto);
      }
    }
    return electiveSubjectListToPatch;
  }

  patchGroupSubjects(electiveSubjects: Array<any>) {
    let electiveSubjectList = [];
    if (electiveSubjects != null && electiveSubjects.length > 0) {
      for (let i = 0; i < electiveSubjects.length; i++) {
        let subject = this.formBuilder.group({
          id: electiveSubjects[i].id,
          subjectId: electiveSubjects[i].subjectId,
          name: [electiveSubjects[i].name, Validators.required],
          isLinked: electiveSubjects[i].isLinked,
          uniqueId: electiveSubjects[i].uniqueId,
          courseHasSubjectUniqueId: electiveSubjects[i].courseHasSubjectUniqueId
        });
        electiveSubjectList.push(subject);
      }
    }
    return electiveSubjectList;
  }
  addNewRow() {
    let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
    if (this.masterCourseList.length > courseFormArray.length) {
      let SubjectByCourseFormGroup = this.formBuilder.group({
        courseId: [null, Validators.required],
        courseName: [null, Validators.nullValidator],
        subjectList: this.formBuilder.array(this.patchRemainingSubjects(this.subjectList1, [])),
        compulsorySubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects([])),
        optionalSubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects([])),
        electiveSubjectList: this.formBuilder.array(this.patchElectiveSubjects([])),
        editMode: true
      });
      courseFormArray.push(SubjectByCourseFormGroup);
    } else {
      this.toastService.ShowWarning("No more course to add.");
    }
    console.log(this.subjectLinkingSubjectForm);
  }
  updateRow(row) {
    this.submitted = true;
    console.log(this.subjectLinkingSubjectForm)
    if (this.subjectLinkingSubjectForm.invalid) {
      return;
    }
    this.submitted = false;
    row.controls.editMode.value = false;

  }
  deleteCourseHasSubject(i) {
    let submitData = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
    submitData.removeAt(i);
    // console.log('delete ', JSON.stringify(submitData));
    // this.loading = true
    // this._subjectServiceService.deleteCourseHasSubject(submitData.courseId).subscribe(
    //   response => {
    //     this.loading = false;
    //     this.toastService.showSuccess("Subjects delete dsuccessfully");
    //     this.cancelBtn(false)
    //     // stepper.next();
    //     this.submitted = false;
    //     this.cancelBtn(false);
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       this.loading = false;
    //     } else {
    //       this.loading = false;
    //       this.toastService.ShowError(error);
    //     }
    //   }
    // );

  }
  editRow(row, index) {
    console.log("this.currentEditRowIndex", this.currentEditRowIndex);
    this.submitted = false;
    if (this.currentEditRowIndex || this.currentEditRowIndex == 0) {
      let controlArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
      let rowGroup = <FormGroup>controlArray.controls[this.currentEditRowIndex];
      console.log("rowGroup", rowGroup);
      rowGroup.controls.subjectList = this.formBuilder.array(this.patchRemainingSubjects(this.oldRow.subjectList, []));
      rowGroup.controls.compulsorySubjectList = this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(this.oldRow.compulsorySubjectList));
      rowGroup.controls.optionalSubjectList = this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(this.oldRow.optionalSubjectList));
      rowGroup.controls.electiveSubjectList = this.formBuilder.array(this.patchElectiveSubjects(this.oldRow.electiveSubjectList));
      //rowGroup.setValue(this.oldRow)
      rowGroup.controls['editMode'].setValue(false);
      rowGroup.controls['compulsorySubjectList'].setValue(this.oldRow.compulsorySubjectList);
      rowGroup.controls['optionalSubjectList'].setValue(this.oldRow.optionalSubjectList);
      rowGroup.controls['electiveSubjectList'].setValue(this.oldRow.electiveSubjectList);
      rowGroup.controls['subjectList'].setValue(this.oldRow.subjectList);
    }
    this.currentEditRowIndex = index;
    row.controls.editMode.value = true;
    this.oldRow = row.value;
  }
  deleteRow(row, index) {

  }
  importSubject(centerDataModal: String) {
    this.modalService.open(centerDataModal, { centered: true, backdrop: 'static' });
    this.getSubjectListFromMasterOrg()
  }
  dropSubjectFromList(event: CdkDragDrop<string[]>, link: any, index: number, type: number, electiveIndex: number, course: FormGroup) {

    let item = event.item;
    console.log("item", item)
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log("moveItemInArray", moveItemInArray);
    } else {

      let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
      console.log("courseFormArray", courseFormArray)
      let courseForm = <FormGroup>courseFormArray.controls[index];
      console.log("courseForm", courseForm)
      let array = event.container.data;
      console.log(array);
      let subjectArray = null;

      switch (type) {
        case 1:
          subjectArray = <FormArray>courseForm.controls['compulsorySubjectList'];
          break;
        case 2:
          subjectArray = <FormArray>courseForm.controls['optionalSubjectList'];
          break;
        case 3:
          subjectArray = <FormArray>courseForm.controls['electiveSubjectList'];
          break;
      }

      let subjectFormGroup = this.formBuilder.group({

        id: [item.data.id, Validators.nullValidator],
        name: [item.data.name, Validators.nullValidator],
        uniqueId: [item.data.uniqueId, Validators.nullValidator],
        subjectId: [item.data.uniqueId, Validators.nullValidator],
        isLinked: [item.data.isLinked, Validators.nullValidator]
      });

      console.log("subjectFormGroup", subjectFormGroup)

      if (electiveIndex == null || electiveIndex == undefined) {
        subjectArray.push(subjectFormGroup);
      } else {

        let electiveSubjectArray = <FormGroup>subjectArray.controls[electiveIndex];
        let electiveSubjects = <FormArray>electiveSubjectArray.controls['electiveSubjectList'];
        electiveSubjects.push(subjectFormGroup);
      }

      let subjectMainList = <FormArray>courseForm.controls['subjectList'];
      let subjectList = subjectMainList.value;
      let subjectObject = item.data;
      let compObject: CommonDto = subjectObject;
      subjectList = subjectList.filter(x => x.uniqueId != compObject.uniqueId);
      courseForm.setControl('subjectList', this.formBuilder.array(subjectList));
    }
  }
  removeSubjectFromList(event: CdkDragDrop<string[]>, parentIndex: number, childIndex: number, type: number, item, electiveSubjectIndex: number) {
    let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];

    console.log(item);
    console.log("item to delete");
    console.log(item);
    console.log(courseFormArray.value);
    let courseForm = <FormGroup>courseFormArray.controls[parentIndex];
    // let courseForm = this.newMethod(courseFormArray, parentIndex);

    console.log(courseForm);

    let removeSubjectArray = null;

    switch (type) {
      case 1: removeSubjectArray = <FormArray>courseForm.controls['compulsorySubjectList'];
        break;
      case 2: removeSubjectArray = <FormArray>courseForm.controls['optionalSubjectList'];
        break;
      case 3: removeSubjectArray = <FormArray>courseForm.controls['electiveSubjectList'];
        break;
    }

    if (electiveSubjectIndex == null || electiveSubjectIndex == undefined) {
      removeSubjectArray.removeAt(childIndex);
    } else {
      console.log(removeSubjectArray);
      let electiveSubjectFormGroup = <FormGroup>removeSubjectArray.controls[childIndex];
      console.log("when deleting");
      console.log(electiveSubjectFormGroup.value);
      let electiveSubjectListArray = <FormArray>electiveSubjectFormGroup.controls['electiveSubjectList'];
      electiveSubjectListArray.removeAt(electiveSubjectIndex);
    }

    let subjectArray = <FormArray>courseForm.controls['subjectList'];

    let subjectFormGroup = this.formBuilder.group({
      id: [item.id, Validators.nullValidator],
      name: [item.name, Validators.nullValidator],
      subjectId: [item.subjectId, Validators.nullValidator],
      uniqueId: [item.uniqueId, Validators.nullValidator],
      isLinked: false
    });
    subjectArray.push(subjectFormGroup);
  }


  selectAllSubject(event) {
    console.log("selectAll", event.target.checked);
    if (event.target.checked) {
      for (var i = 0; i < this.masterSubjectList.length; i++) {
        this.masterSubjectList[i].isSelected = true;
        if (this.alreadySelectedSubjectList.includes(this.masterSubjectList[i].universal_id)) {
          this.masterSubjectList[i].isNew = false;
        } else {
          this.masterSubjectList[i].isNew = true;
        }
      }
      this.selectedCount = this.masterSubjectList.length;
    } else {
      for (var i = 0; i < this.masterSubjectList.length; i++) {
        this.masterSubjectList[i].isSelected = false;
      }
      this.selectedCount = 0;
    }
    this.isSelectAll = !this.isSelectAll
    console.log("selectAll", this.masterSubjectList);
  }
  subjectChange(subject) {
    if (!subject.isSelected) {
      subject.isSelected = true;
      this.selectedCount = this.selectedCount + 1;
    } else {
      subject.isSelected = false;
      this.selectedCount = this.selectedCount - 1;
    }
    if (this.alreadySelectedSubjectList.length > 0 && this.alreadySelectedSubjectList.includes(subject.universal_id)) {
      subject.isNew = false;
      console.log("alreadySelected");
    } else {
      console.log("Not alreadySelected");
      subject.isNew = true;
    }
  }
  initSubjectForm() {
    this.subjectForm = this.formBuilder.group({
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      subjectList: this.formBuilder.array([this.initSubjectList()])
    });
  }
  initSubjectList() {
    return this.formBuilder.group({
      id: ['', Validators.nullValidator],
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.required],
      universal_id: ['', Validators.nullValidator],
      isSelected: [false, Validators.nullValidator],
      isLinked: [false, Validators.nullValidator]
    })
  }
  getSubjectListFromMasterOrg() {
    this._subjectServiceService.getMasterSubjects().subscribe(
      res => {
        if (res) {
          this.masterSubjectList = res;
          // this.patchSubjectList(this.masterSubjectList);
          let selectedSubject = res.filter(x => x.isSelected == true);
          this.selectedCount = selectedSubject.length;

          console.log("MASTERsubjectList", this.masterSubjectList);
          this.loading = false;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });
  }
  getAllSubjects() {
    this.loading = true;
    this._subjectServiceService.getSubjectList().subscribe(
      response => {
        if (response && response.success == true) {
          console.log('is edit' + this.isAdd)
          console.log("subjectList", response);
          this.mastersSubjectUids = response.list.map(x => x.uniqueId);
          this.remainingSubjectCount = response.list.length;
          this.patchSubjecttoCourse(response.list);
          this.subjectList1 = response.list;
        } else {
          this.toastService.ShowWarning(response.responseMessage);
        }
        // this.patchSubjectList(this.subjectList);

        // if (this.subjectList1 && this.subjectList1.length > 0) {
        //   this.selectedCount = this.subjectList1.length;
        //   this.subjectList1.map(subject => {
        //     this.alreadySelectedSubjectList.push(subject.universal_id)
        //   })
        // }
        this.loading = false;
        this.getSubjectListFromMasterOrg();
        if (this.subjectList1 && this.subjectList1.length == 0) {
          this.modalService.open(this.content, { backdrop: 'static' });
        }
        // this.childOrganizationList.push(orgDto);
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.loading = false;
        // } else {
        //   this.loading = false;
        //   this.toastService.ShowError(error);
        // }
        this.loading = false;
      }
    );
  }
  patchSubjecttoCourse(listofSubject) {
    if (this.isAdd == true) {
      let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
      let courseFormList = this.subjectLinkingSubjectForm.controls['linkedSubjectList'].value;
      courseFormList.forEach((element, i) => {
        let courseForm = <FormGroup>courseFormArray.controls[i];
        let subjectList = courseForm.value.subjectList;
        let compsubjectList = courseForm.value.compulsorySubjectList;
        let opSubjectList = courseForm.value.optionalSubjectList;
        let elSubjectList = courseForm.value.electiveSubjectList;
        let uIdList = [];
        elSubjectList.forEach(element => {
          if (element.electiveSubjectList.length > 0) {
            element.electiveSubjectList.forEach(elm => {
              uIdList.push(elm.uniqueId);
            });
          }
        });
        opSubjectList.forEach(element => {
          uIdList.push(element.uniqueId);
        });
        compsubjectList.forEach(element => {
          uIdList.push(element.uniqueId);
        });
        let subjects = (subjectList.length > 0) ? this.patchRemSubjectListData(listofSubject, uIdList) : this.patchRemainingSubjects(listofSubject, uIdList);
        let opSubjects = (opSubjectList.length > 0) ? this.patchSubjectListData(opSubjectList) : [];
        let compSubjects = (compsubjectList.length > 0) ? this.patchSubjectListData(compsubjectList) : [];
        let eleSubjects = (elSubjectList.length > 0) ? this.patchEleSubjectListData(elSubjectList) : [];
        courseForm.setControl('subjectList', this.formBuilder.array(subjects));
        courseForm.setControl('compulsorySubjectList', this.formBuilder.array(compSubjects));
        courseForm.setControl('optionalSubjectList', this.formBuilder.array(opSubjects));
        courseForm.setControl('electiveSubjectList', this.formBuilder.array(eleSubjects));
      });
    } else {
      let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
      let courseForm = <FormGroup>courseFormArray.controls[0];
      let compsubjectList = courseForm.value.compulsorySubjectList;
      let opSubjectList = courseForm.value.optionalSubjectList;
      let elSubjectList = courseForm.value.electiveSubjectList;
      let uIdList = [];
      elSubjectList.forEach(element => {
        if (element.electiveSubjectList.length > 0) {
          element.electiveSubjectList.forEach(elm => {
            uIdList.push(elm.uniqueId);
          });
        }
      });
      opSubjectList.forEach(element => {
        uIdList.push(element.uniqueId);
      });
      compsubjectList.forEach(element => {
        uIdList.push(element.uniqueId);
      });
      let subjects = this.patchRemSubjectListData(listofSubject, uIdList);
      courseForm.setControl('subjectList', this.formBuilder.array(subjects));
    }

  }
  patchRemSubjectListData(subjectList, uIdList) {
    let subjectsToPatch = [];
    for (let i = 0; i < subjectList.length; i++) {
      if (this.mastersSubjectUids.includes(subjectList[i].uniqueId) && !uIdList.includes(subjectList[i].uniqueId)) {

        let subjectGroup = this.formBuilder.group({
          subjectId: subjectList[i].subjectId,
          name: subjectList[i].name,
          isLinked: subjectList[i].isLinked,
          uniqueId: subjectList[i].uniqueId,
          id: subjectList[i].id
        });
        subjectsToPatch.push(subjectGroup);
      }
    }
    return subjectsToPatch;
  }
  patchSubjectListData(subjectList) {
    let subjectsToPatch = [];
    for (let i = 0; i < subjectList.length; i++) {
      if (this.mastersSubjectUids.includes(subjectList[i].uniqueId)) {

        let subjectGroup = this.formBuilder.group({
          subjectId: subjectList[i].subjectId,
          name: subjectList[i].name,
          isLinked: subjectList[i].isLinked,
          uniqueId: subjectList[i].uniqueId,
          id: subjectList[i].id
        });
        subjectsToPatch.push(subjectGroup);
      }
    }
    return subjectsToPatch;
  }
  patchEleSubjectListData(electiveSubjects) {
    let electiveSubjectListToPatch = [];
    if (electiveSubjects != null && electiveSubjects.length > 0) {
      for (let i = 0; i < electiveSubjects.length; i++) {
        let electiveSubjectDto = this.formBuilder.group({
          groupId: electiveSubjects[i].uniqueId,
          groupName: electiveSubjects[i].name,
          uniqueId: electiveSubjects[i].uniqueId,
          electiveSubjectList: this.formBuilder.array(this.patchSubjectListData(electiveSubjects[i].electiveSubjects))
        });
        electiveSubjectListToPatch.push(electiveSubjectDto);
      }
    }
    return electiveSubjectListToPatch;
  }
  patchSubjectList(subjects: any) {
    if (subjects != null && subjects.subjectList != null && subjects.subjectList.length > 0) {
      let subjectList = [];

      for (let i = 0; i < subjects.subjectList.length; i++) {
        let subject = subjects.subjectList[i];
        let subjectGroup = this.formBuilder.group({
          id: [subject.id, Validators.nullValidator],
          name: [subject.name, Validators.required],
          uniqueId: [subject.uniqueId, Validators.nullValidator],
          orgUniqueId: [sessionStorage.getItem('orgUniqueId')],
          universal_id: [subject.universal_id, Validators.nullValidator]
          // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId')]
        });
        subjectList.push(subjectGroup);
      }
      this.subjectForm.setControl('subjectList', this.formBuilder.array(subjectList));
    }
  }
  cancelSubject() {
    for (var i = 0; i < this.masterSubjectList.length; i++) {
      if (this.masterSubjectList[i].isNew) {
        this.masterSubjectList[i].isSelected = false;
        this.masterSubjectList[i].isNew = false;
      }
    }
    // if (this.selectedIndex == 3) {
    //   this.isAdd = false;
    // } else {
    //   this.isAdd = true;
    // }
    this.modalService.dismissAll();
  }
  saveSubject() {
    this.loading = true;
    console.log("masterSubjectList", this.masterSubjectList);
    let selectedSubjectList = []
    let selectedSubjectsCount = this.masterSubjectList.filter(x => x.isSelected)
    if (selectedSubjectsCount == null || selectedSubjectsCount.length == 0) {
      this.toastService.ShowWarning("Please select atleast one subject");
      // alert("Please Select atleast one subject")
      this.loading = false;
      return;
    }
    this.masterSubjectList.map(x => {
      if (x.isSelected == true) {
        selectedSubjectList.push({
          name: x.name,
          universal_id: x.uniqueId,
          isSelected: x.isSelected
        })
      }
    });
    console.log('selectedSubjectList' + JSON.stringify(selectedSubjectList));


    this._subjectServiceService.addUpdateMasterSubjects(selectedSubjectList).subscribe(
      response => {
        if (response) {
          if (response.success) {
            this.toastService.showSuccess(response.responseMessage);
            let subjectList = response;
            console.log(subjectList);
            this.getAllSubjects();
            this.getSubjectListFromMasterOrg();
            this.modalService.dismissAll();
          } else {
            this.toastService.ShowWarning(response.responseMessage);
          }
        }
        this.loading = false;
        // this.patchSubjectList(this.subjectList);
        // this.toastService.showSuccess("Subjects added successfully");
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.loading = false;
        // } else {
        //   this.loading = false;
        //   this.toastService.ShowError(error);
        // }
        this.loading = false;
      }
    );

  }
  onChangeCourse(deviceValue, i) {
    this.selecteedCourseUniqueId = deviceValue.uniqueId;
    let courseSubjectList = this.subjectLinkingSubjectForm.controls['linkedSubjectList'].value;
    let couseList = courseSubjectList.filter(x => x.courseId == deviceValue.uniqueId);
    if (couseList.length > 1) {
      this.toastService.ShowWarning("This course already used.");
      let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
      let courseForm = <FormGroup>courseFormArray.controls[i];
      courseForm.controls.courseId.setValue(null);
    }
  }
  onSubmit() {
    let courseSubjectList = this.subjectLinkingSubjectForm.controls['linkedSubjectList'].value;
    let courseList = courseSubjectList.filter(x => (!x.courseId));
    if (courseList.length > 0) {
      this.toastService.ShowWarning("Please select course");
      return;
    }
    let submitArray = [];
    let valid = true;
    courseSubjectList.forEach(element => {
      let submitData = element;
      if (this.remainingSubjectCount == 0 || this.remainingSubjectCount == submitData.subjectList.length) {
        valid = false;
        return;
      }
      let courseDto = {
        course: submitData.courseName,
        uniqueId: submitData.courseId,
      }
      let compulsorySubjects = [];
      submitData.compulsorySubjectList.forEach(com => {
        let dto = {
          uniqueId: com.uniqueId,
          name: com.name,
          isSelected: true,
          courseHasSubjectUniqueId: ""
        }
        compulsorySubjects.push(dto)
      });
      let optionalSubjects = [];
      submitData.optionalSubjectList.forEach(opt => {
        let dto = {
          uniqueId: opt.uniqueId,
          name: opt.name,
          isSelected: true,
          courseHasSubjectUniqueId: ""
        }
        optionalSubjects.push(dto)
      });

      let remainingSubjects = [];
      submitData.subjectList.forEach(rem => {
        let dto = {
          uniqueId: rem.uniqueId,
          name: rem.name,
          isSelected: false,
          courseHasSubjectUniqueId: ""
        }
        remainingSubjects.push(dto)
      });

      let electiveGroupsAndSubjects = [];
      submitData.electiveSubjectList.forEach(ele => {

        let electiveSubjects = [];
        ele.electiveSubjectList.forEach(els => {
          let dto = {
            uniqueId: els.uniqueId,
            name: els.name,
            isSelected: true,
            courseHasSubjectUniqueId: ""
          }
          electiveSubjects.push(dto)
        });

        let dto = {
          name: ele.groupName,
          electiveSubjects: electiveSubjects
        }
        electiveGroupsAndSubjects.push(dto)
      });

      let dto = {
        course: courseDto,
        compulsorySubjects: compulsorySubjects,
        optionalSubjects: optionalSubjects,
        remainingSubjects: remainingSubjects,
        electiveGroupsAndSubjects: electiveGroupsAndSubjects,
      }
      submitArray.push(dto)
    });
    if (!valid) {
      this.toastService.ShowWarning("Please add compulsory or optional or elective subject.");
      return;
    }

    this.loading = true
    this._subjectServiceService.addCourseHasSubject(submitArray).subscribe(
      response => {
        this.loading = false;
        this.toastService.showSuccess("Subjects linked successfully");
        this.cancelBtn(false)
        if (this.isFromSetup) {
          sessionStorage.setItem('isSetupDone', "true");
          this.setup.setupComplete();
          setTimeout(() => {
            this.router.navigate(['/newModule/academic/academicDetails']);
          }, 1000);
        }
        // stepper.next();
        // this.submitted = false;
        // let setupStatus = sessionStorage.getItem('isSetupDone');
        // if (setupStatus == 'false') {
        //   sessionStorage.setItem('isSetupDone', "true");
        //   this.accessRightService.isSetupDone.next(true);
        //   this.toastService.showSuccess("Your setup saved successfully.");
        //   setTimeout(() => {
        //     window.location.reload()
        //   }, 1000);
        // }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.loading = false;
        // } else {
        //   this.loading = false;
        //   this.toastService.ShowError(error);
        // }
        this.loading = false;
      }
    );
  }
  removeElectiveSubjectGroup(parentIndex: number, childIndex: number, electiveSubjectIndex: number) {
    let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];

    let courseForm = <FormGroup>courseFormArray.controls[parentIndex];

    let removeSubjectArray = <FormArray>courseForm.controls['electiveSubjectList']
    console.log("removeSubjectArray", removeSubjectArray);
    let group = <FormGroup>removeSubjectArray.controls[childIndex];

    let groupUniqueId = group.controls['uniqueId'].value;
    let subjectList = group.controls['electiveSubjectList'].value;
    let remainingsubjectlist = courseForm.controls.subjectList.value;
    let subjectArray = remainingsubjectlist.concat(subjectList);
    console.log("groupUniqueId", groupUniqueId)
    if (groupUniqueId != "") {
      this.loading = true
      this._subjectServiceService.deleteElectiveGroup(groupUniqueId).subscribe(
        response => {
          this.loading = false;
          this.toastService.showSuccess("elective group deleted successfully");
          removeSubjectArray.removeAt(childIndex);
          courseForm.setControl('subjectList', this.formBuilder.array(subjectArray));
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   this.loading = false;
          // } else {
          //   this.loading = false;
          //   this.toastService.ShowError(error);
          // }
          this.loading = false;
        }
      );
    } else {
      removeSubjectArray.removeAt(childIndex);
      courseForm.setControl('subjectList', this.formBuilder.array(subjectArray));
    }


  }

  deleteElectiveGroup(groupUniqueId) {

  }
  createElectiveGroup(index: number) {
    let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];

    let courseForm = <FormGroup>courseFormArray.controls[index];

    let electiveSubjectArray = <FormArray>courseForm.controls['electiveSubjectList'];

    let electiveSubjectGroup = this.formBuilder.group({
      groupName: ['', Validators.required],
      uniqueId: ['', Validators.nullValidator],
      electiveSubjectList: this.formBuilder.array([])
    });

    electiveSubjectArray.push(electiveSubjectGroup);
  }
  searchSubject(event) {
    console.log(event, event.target.value);
    let value = event.target.value;
    for (var i = 0; i < this.masterSubjectList.length; i++) {
      if (this.masterSubjectList[i].name.includes(value)) {
        this.masterSubjectList[i].isSearched = true;
      } else {
        this.masterSubjectList[i].isSearched = false;
      }
    }
  }

  onUpdate() {
    let submitData = this.subjectLinkingSubjectForm.controls['linkedSubjectList'].value[0];
    if (this.remainingSubjectCount == 0 || this.remainingSubjectCount == submitData.subjectList.length) {
      this.toastService.ShowWarning("Please add compulsory or optional or elective subject.");
      return;
    }
    console.log('submitData => ', JSON.stringify(submitData))
    let courseDto = {
      course: submitData.courseName,
      uniqueId: submitData.courseId,
    }
    let compulsorySubjects = [];
    submitData.compulsorySubjectList.forEach(com => {
      let dto = {
        uniqueId: com.uniqueId,
        name: com.name,
        isSelected: true,
        courseHasSubjectUniqueId: com.courseHasSubjectUniqueId
      }
      compulsorySubjects.push(dto)
    });
    let optionalSubjects = [];
    submitData.optionalSubjectList.forEach(opt => {
      let dto = {
        uniqueId: opt.uniqueId,
        name: opt.name,
        isSelected: true,
        courseHasSubjectUniqueId: opt.courseHasSubjectUniqueId
      }
      optionalSubjects.push(dto)
    });

    let remainingSubjects = [];
    submitData.subjectList.forEach(rem => {
      let dto = {
        uniqueId: rem.uniqueId,
        name: rem.name,
        isSelected: false,
        courseHasSubjectUniqueId: ""
      }
      remainingSubjects.push(dto)
    });

    let electiveGroupsAndSubjects = [];
    submitData.electiveSubjectList.forEach(ele => {

      let electiveSubjects = [];
      ele.electiveSubjectList.forEach(els => {
        let dto = {
          uniqueId: els.uniqueId,
          name: els.name,
          isSelected: true,
          courseHasSubjectUniqueId: els.courseHasSubjectUniqueId
        }
        electiveSubjects.push(dto)
      });

      let dto = {
        name: ele.groupName,
        uniqueId: ele.uniqueId,
        electiveSubjects: electiveSubjects
      }
      electiveGroupsAndSubjects.push(dto)
    });

    let dto = {
      course: courseDto,
      compulsorySubjects: compulsorySubjects,
      optionalSubjects: optionalSubjects,
      remainingSubjects: remainingSubjects,
      electiveGroupsAndSubjects: electiveGroupsAndSubjects,
    }
    let submitArray = [];
    submitArray.push(dto)
    console.log('submit subject data => ', JSON.stringify(submitArray))

    this.loading = true
    this._subjectServiceService.updateCourseHasSubject(submitArray).subscribe(
      response => {
        this.loading = false;
        this.toastService.showSuccess("Subjects updated successfully");
        this.cancelBtn(false)
        // stepper.next();
        this.submitted = false;
        this.cancelBtn(false);
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.loading = false;
        // } else {
        //   this.loading = false;
        //   this.toastService.ShowError(error);
        // }
        this.loading = false;
      }
    );
  }

  cancelBtn(value) {
    this.newItemEvent.emit(value);
  }

  previousStep() {
    this.setup.selectedStep(2)
  }
}
