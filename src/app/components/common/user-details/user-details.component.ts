import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { ApexAxisChartSeries, ApexChart, ApexDataLabels, ApexStroke, ApexTooltip, ApexXAxis, ChartComponent } from 'ng-apexcharts';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { PeriodServiceService } from 'src/app/servicefiles/period-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { ChartType } from './default.model';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  tooltip: ApexTooltip;
  dataLabels: ApexDataLabels;
};

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  isTabFormShow = true;
  userData: any
  activeUser;
  teacherSubjectList = [];
  currentJustify = 'justified';
  basicFormGroup: FormGroup;
  userAddressInfoForm: FormGroup;
  teacherSubjectForm: FormGroup;
  teacherPeriodForm: FormGroup;
  userBasicInfo: any;
  imageSrc: any;
  imageSrcFile: any;
  fileSelected = false;
  isUserDeatilsShow = true;
  userAddressInfo: any;
  stateList = [];
  activeTab = 'basic';
  teacherActiveTab = 'workTimeForm';
  mainActiveTab = "basicDetails";
  tabTitle = "Basic Details"
  orgList = [];
  revenueRadialChart: ChartType;
  guardianForms = new Array<FormGroup>();
  guardianIndex = 0
  relations = [{ id: 1, name: 'Mother' }, { id: 2, name: 'Father' }, { id: 3, name: 'Guardian' }];
  isGuardianDeatils = false;
  academicFormGroup: FormGroup;
  selectedElectiveSub = [];
  studentAcademicInfo: any = {
    "studentUniqueIds": null,
    "course": {
      "uniqueId": "",
      "name": ""
    },
    "batch": {
      "uniqueId": "",
      "name": ""
    },
    "board": {
      "uniqueId": "",
      "name": ""
    },
    "grade": {
      "uniqueId": "",
      "name": ""
    },
    "batchGroupDetails": [],
    "subjects": {
      "compulsorySubjects": [],
      "optionalSubjects": [],
      "electiveSubjects": []
    }
  };
  orgBatchList: any;
  academicCalendarList: any;
  orgCourseList = [];
  complsorySubjects = [];
  optionalSubject = [];
  electiveSubject = [];
  selectedOptionalSubject = [];
  getbatchHasGroupList: any;
  currentUserId: any;
  isCheckTrue = false
  createProfileFile;
  selectFileForEditUser;
  url;
  selectedFiles: FileList;
  currentFileUpload: File;
  imageLoaded: boolean = false;
  imageError: string;
  isUploadFile = false;
  oldImgSrc = ""
  shiftList: any = [];
  saveList: any;
  staffShiftData: any;
  studentDataList: any = [];
  remainingStudents = 3
  teacherPeriodData: any
  slotList: any
  relationArray = ["Mother", "Father", "Guardian"]
  userType = ["", "student", "staff", "teacher", "guardian"]
  userTypes = ["Student", "Staff", "Teacher", "Parent"];
  userTypesList = ["", "Learners", "Staff", "Teacher", "Parent"];
  isWorkingDetailsForm = false;
  subscription: Subscription;
  form: FormGroup;
  emailId: any = {};
  mobileNo: any = {};
  days = ["", "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  shiftIdList = [];
  shiftListWithPeriod = [];
  shiftListData = [];
  selectedDays = [];
  selectedShiftData: any = {};
  selectedShiftPeriodData: any = {};
  isPeriodListData = false;
  currentIndex = 0;
  isAccountActivate = false;
  isToggelSwitchOn = false;
  userStates: any;
  currentState: any;
  currentStateData: any = {};
  @Input() selectedUserId: Number;
  @Input() roleData: any;
  @Input() selectedUserUniqueId: any;
  @Input() userDetailsType: any;
  @ViewChild('tabset', { static: false }) selectedTab: NgbTabset;
  @ViewChild('mainTabs', { static: false }) mainTabs: NgbTabset;
  isAcademicFormShow = false
  @ViewChild("chart", { static: false }) chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  constructor(private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private _DomSanitizationService: DomSanitizer, private formBuilder: FormBuilder, private _userService: UserServiceService, private _periodService: PeriodServiceService, private modalService: NgbModal) {
    this.userData = this._commonService.getUserData();
    this.subscription = this._commonService.iscloseClicked.subscribe(click => {
      if (click == true) {
        this.isUserDeatilsShow = true;
        this.isTabFormShow = false;
        this.fileSelected = false;
        this.getImage();
        this.getBasicInfo(this.selectedUserUniqueId);
        let usertype = this.userType[this.userDetailsType];
        if (this.userDetailsType == 1) {
          this.getGuardian(this.selectedUserUniqueId);
          this.isAcademicFormShow = false;
          this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "")
        } else if (this.userDetailsType == 2) {
          this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "")
          this.getShift(this.selectedUserUniqueId)
        } else if (this.userDetailsType == 3) {
          this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "")
          this.getTeacherSubjects(this.selectedUserUniqueId)
        } else if (this.userDetailsType == 4) {
          this.getChlidrens(this.selectedUserUniqueId)
        }
      } else if (click == 2) {
        this.isUserDeatilsShow = true;
        this.isAcademicFormShow = false;
        this.isTabFormShow = false;
        this.getUserAcademicInfo(this.activeUser);
        this.mainActiveTab = "academicDetails"
      } else if (click == 3) {
        this.isUserDeatilsShow = true;
        this.isWorkingDetailsForm = false;
        this.isTabFormShow = false;
        this.mainActiveTab = "workDetails"
        this.getShift(this.selectedUserUniqueId)
        this.getUserPeriods(this.activeUser)
      } else if (click == 4) {
        this.teacherActiveTab = "dayAndPeriod"
        this.getUserPeriods(this.activeUser)
      }
      window.scroll(0, 0);

    })
    this.chartOptions = {
      series: [
        {
          name: "series1",
          data: [31, 40, 28, 51, 42, 109, 100]
        },
        {
          name: "series2",
          data: [11, 32, 45, 32, 34, 52, 41]
        }
      ],
      chart: {
        height: 350,
        type: "line",
        toolbar: {
          show: true,
          offsetX: 0,
          offsetY: 0,
          tools: {
            download: false,
            selection: true,
            zoom: true,
            zoomin: true,
            zoomout: true,
            pan: true,
            customIcons: []
          }
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "smooth"
      },
      xaxis: {
        type: "datetime",
        categories: [
          "2018-09-19T00:00:00.000Z",
          "2018-09-19T01:30:00.000Z",
          "2018-09-19T02:30:00.000Z",
          "2018-09-19T03:30:00.000Z",
          "2018-09-19T04:30:00.000Z",
          "2018-09-19T05:30:00.000Z",
          "2018-09-19T06:30:00.000Z"
        ]
      },
      tooltip: {
        x: {
          format: "dd/MM/yy HH:mm"
        }
      }
    };
  }

  ngOnInit() {
    this.activeUser = this.selectedUserId;
    // this.getChildOrganizationList(this.userData.orgUniqueId);
    this.initUserBasicInfoForm();
    this.isTabFormShow = false
    this._fetchData();
    this.form = this.formBuilder.group({
      file: [null]
    })
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['userDetailsType']) {
      this.activeUser = this.selectedUserId;
      let usertype = this.userType[this.userDetailsType];
      this.fileSelected = false;
      this.getImage();
      this.getBasicInfo(this.selectedUserUniqueId);
      this.getUserStates();
      switch (this.userDetailsType) {
        case 1:
          this.getGuardian(this.selectedUserUniqueId);
          this.getStateList();
          this.initUserAddressForm();
          this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "")
          break;
        case 2:
          this.getStateList();
          this.initUserAddressForm();
          this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "")
          this.getShift(this.selectedUserUniqueId)
          break;
        case 3:
          this.tabTitle = "Teacher Details"
          this.getStateList();
          this.initUserAddressForm();
          this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "")
          this.getTeacherSubjects(this.selectedUserUniqueId)
          break;
        case 4:
          this.getChlidrens(this.selectedUserUniqueId)
          break;
      }
    }
  }
  getImage() {
    let usertype = this.userType[this.userDetailsType];
    this._userService.getImage(usertype, this.selectedUserUniqueId, true).subscribe(
      response => {
        if (response != null && response) {
          if (response.image) {
            let image = response.image;
            this.imageSrcFile = image;
          } else {
          }
        }
      },
      (error: HttpErrorResponse) => {
        // console.log(error);
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  generateData(baseval, count, yrange) {
    var i = 0;
    var series = [];
    while (i < count) {
      var x = Math.floor(Math.random() * (750 - 1 + 1)) + 1;
      var y =
        Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
      var z = Math.floor(Math.random() * (75 - 15 + 1)) + 15;
      series.push([x, y, z]);
      baseval += 86400000;
      i++;
    }
    return series;
  }
  // async getChildOrganizationList(parentOrganizationId) {
  //   this._learnersService.getOrganizationList(parentOrganizationId).subscribe(
  //     response => {
  //       let organizationList = response;
  //       if (organizationList != null && organizationList.length > 0) {
  //         let orgList = [];
  //         this.orgList = response;
  //       } else {
  //         this.orgList = [];
  //       }
  //       this.getBasicInfo(this.selectedUserUniqueId);
  //     },
  //     (error: HttpErrorResponse) => {
  //       if (error instanceof HttpErrorResponse) {
  //       } else {
  //         this.toastService.ShowError(error);
  //       }
  //     });
  // }
  initUserBasicInfoForm() {
    let basicFormGrup = {
      usercode: ['', Validators.nullValidator],
      roles: ['', Validators.nullValidator],
      generalInfo: this.getGeneralInfo()
    }
    switch (this.userDetailsType) {
      case 1:
        basicFormGrup['organizationName'] = [''];
        basicFormGrup['roleId'] = [null, Validators.nullValidator];
        basicFormGrup['organizationUniqueId'] = ['', Validators.nullValidator];
        basicFormGrup['enrollmentNumber'] = ['', Validators.nullValidator];
        basicFormGrup['enrollmentDate'] = [null];
        break;
      case 2:
        basicFormGrup['roleId'] = [[], Validators.required];
        basicFormGrup['organizationUniqueId'] = ['', Validators.nullValidator];
        basicFormGrup['organizationName'] = [''];
        basicFormGrup['employeeId'] = [null, Validators.nullValidator];
        basicFormGrup['roleName'] = [null, Validators.nullValidator];
        break;
      case 3:
        basicFormGrup['roleId'] = [[], Validators.required];
        basicFormGrup['organizationUniqueId'] = ['', Validators.nullValidator];
        basicFormGrup['employeeId'] = [null, Validators.nullValidator];
        basicFormGrup['organizationName'] = [''];
        break;
      case 4:
        basicFormGrup['roleId'] = [null, Validators.nullValidator];
        basicFormGrup['parentId'] = [this.activeUser];
        basicFormGrup['parentUniqueId'] = ['', Validators.nullValidator];
        break;
    }
    this.basicFormGroup = this.formBuilder.group(basicFormGrup)
  }

  getGeneralInfo() {
    let generalInfo = {
      firstName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      lastName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      gender: ['', Validators.nullValidator],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      fullName: [''],
      emailId: ['', [Validators.required, Validators.email]]
    }
    switch (this.userDetailsType) {
      case 1:
        break;
      case 2:
        generalInfo['dateOfJoining'] = [null, Validators.nullValidator]
        break;
      case 3:
        generalInfo['dateOfJoining'] = [null, Validators.nullValidator]
        break;
      case 4:
        generalInfo['bloodGroup'] = [null, Validators.nullValidator]
        generalInfo['userId'] = [null, Validators.nullValidator]
        generalInfo['schoolName'] = [null, Validators.nullValidator]
        generalInfo['adharNumber'] = ['', Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])]
        break;
    }
    return this.formBuilder.group(generalInfo)
  }
  getBasicInfo(userId: any) {
    if (userId != null && userId != undefined) {
      this.spinner.show();
      let usertype = this.userType[this.userDetailsType];
      this._userService.getUserBasicInfo(usertype, this.selectedUserUniqueId).subscribe(
        response => {
          this.userBasicInfo = response;
          this.patchBasicInfoToBasicInfoForm(this.userBasicInfo);
          if (this.userDetailsType == 4) {
            this.getUserAddress("guardian", this.selectedUserUniqueId, 3, "");
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
      this.getUserEmail(usertype, this.selectedUserUniqueId, 1, "");
      this.getUserMobile(usertype, this.selectedUserUniqueId, 1, "");
    }
  }
  patchBasicInfoToBasicInfoForm(basicInfo) {
    if (this.userDetailsType != 4) {
      let organizationName = ""
      if (this.orgList.length > 0) {
        organizationName = this.orgList[0].name;
      }
      this.basicFormGroup.controls['organizationName'].setValue(organizationName);
      this.basicFormGroup.controls['roles'].setValue(basicInfo.roles);
      this.basicFormGroup.controls['organizationUniqueId'].setValue(this.userData.orgUniqueId);
    }
    let roleIds = []
    for (let i = 0; i < basicInfo.roles.length; i++) {
      const element = basicInfo.roles[i];
      if (!this.userTypes.includes(element.name)) {
        roleIds.push(element.uniqueId);
      }
    }
    let generalInfoFormGroup = <FormGroup>this.basicFormGroup.controls['generalInfo'];
    generalInfoFormGroup.controls['firstName'].setValue(basicInfo.firstName);
    generalInfoFormGroup.controls['lastName'].setValue(basicInfo.lastName);
    generalInfoFormGroup.controls['gender'].setValue(basicInfo.gender);
    this.basicFormGroup.controls['roleId'].setValue(roleIds);
    this.basicFormGroup.controls['usercode'].setValue(basicInfo.usercode);
    generalInfoFormGroup.controls['fullName'].setValue(basicInfo.firstName + ' ' + basicInfo.lastName, Validators.required);
    switch (this.userDetailsType) {
      case 1:
        this.basicFormGroup.controls['enrollmentNumber'].setValue(basicInfo.usercode);
        this.basicFormGroup.controls['enrollmentDate'].setValue((basicInfo.userDate) ? new Date(basicInfo.userDate) : "");

        break;
      case 2:
        // this.basicFormGroup.controls['roleName'].setValue(basicInfo.roleName);
        this.basicFormGroup.controls['employeeId'].setValue(basicInfo.usercode);
        generalInfoFormGroup.controls['dateOfJoining'].setValue((basicInfo.userDate) ? new Date(basicInfo.userDate) : "");
        break;
      case 3:
        this.basicFormGroup.controls['employeeId'].setValue(basicInfo.usercode);
        generalInfoFormGroup.controls['dateOfJoining'].setValue((basicInfo.userDate) ? new Date(basicInfo.userDate) : "");
        break;
      case 4:
        this.basicFormGroup.controls['parentUniqueId'].setValue(basicInfo.parentUniqueId);
        // generalInfoFormGroup.controls['bloodGroup'].setValue(basicInfo.generalInfo.bloodGroup);
        // generalInfoFormGroup.controls['adharNumber'].setValue(basicInfo.generalInfo.adharNumber);
        break;
    }
    this.fileSelected = (this.imageSrc != null && this.imageSrc != '') ? true : false
  }
  getUserEmail(userType, uniqueId, type, index) {
    this.spinner.show();
    return this._userService.getUserEmailId(userType, uniqueId).subscribe(
      response => {
        this.spinner.hide();
        if (type != 2) {
          let generalInfoFormGroup = <FormGroup>this.basicFormGroup.controls['generalInfo'];
          generalInfoFormGroup.controls['emailId'].setValue(response.emailId, Validators.required);
        } else {
          this.emailId = response;
          let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
          let generalInfo = <FormGroup>gaurdianInfoForm.controls.generalInfo;
          generalInfo.controls.emailId.setValue(response.emailId);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  getUserMobile(userType, uniqueId, type, index) {
    this.spinner.show();
    this._userService.getUserMobile(userType, uniqueId).subscribe(
      response => {
        if (type != 2) {
          let generalInfoFormGroup = <FormGroup>this.basicFormGroup.controls['generalInfo'];
          generalInfoFormGroup.controls['phoneNumber'].setValue(response.mobile, Validators.required);
        } else {
          this.mobileNo = response;
          let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
          let generalInfo = <FormGroup>gaurdianInfoForm.controls.generalInfo;
          generalInfo.controls.phoneNumber.setValue(response.mobile);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  initUserAddressForm() {
    return this.userAddressInfoForm = this.formBuilder.group({
      countryId: ['1', Validators.required],
      country: ['India'],
      address: ['', Validators.required],
      address2: ['', Validators.nullValidator],
      stateId: ['', Validators.required],
      state: [''],
      zipcode: ['', [Validators.required, Validators.maxLength(6), Validators.minLength(6)]],
      city: ['', Validators.required],
      birthDate: [null, Validators.nullValidator],
      bloodGroup: ['A+', Validators.nullValidator],
      adharNumber: ['', [Validators.nullValidator, Validators.maxLength(12), Validators.minLength(12)]],
      setMap: ['', Validators.nullValidator],
      isAdd: [true, Validators.nullValidator],
      userId: [this.activeUser],
      organizationId: [this.userData.organizationId]
    });
  }
  getUserAddress(usertype, uniqueId, type, index) {
    if (uniqueId != null && uniqueId != undefined) {
      this.spinner.show();
      this._userService.getUserOtherInfo(usertype, uniqueId).subscribe(
        response => {
          if (type == 1) {
            this.userAddressInfo = response;
            this.patchUserAddressInfo(this.userAddressInfo);
          } else if (type == 2) {
            let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
            let generalInfo = <FormGroup>gaurdianInfoForm.controls.generalInfo;
            generalInfo.controls.bloodGroup.setValue(response.bloodGroup);
            generalInfo.controls.birthDate.setValue(response.birthDate);
            generalInfo.controls.adharNumber.setValue(response.adharNumber);
          } else {
            let generalInfo = <FormGroup>this.basicFormGroup.controls['generalInfo'];
            generalInfo.controls.bloodGroup.setValue(response.bloodGroup);
            // generalInfo.controls.birthDate.setValue(response.birthDate);
            generalInfo.controls.adharNumber.setValue(response.adharNumber);
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          if (error instanceof HttpErrorResponse) {
            this.userAddressInfoForm.controls['userUniqueId'].setValue(this.selectedUserUniqueId);
          } else {
            this.toastService.ShowError(error);
            this.userAddressInfoForm.controls['userUniqueId'].setValue(this.selectedUserUniqueId);
          }
          this.spinner.hide();
        });
    }
  }

  patchUserAddressInfo(userAddressInfo: any) {
    let countryId = !userAddressInfo.address.countryId ? 1 : (userAddressInfo.address.countryId == 1 ? 1 : 1);
    let country = !userAddressInfo.address.countryId ? "" : (userAddressInfo.address.countryId == 1 ? 'India' : 'India');
    let stateId = !userAddressInfo.address.stateId ? null : userAddressInfo.address.stateId;
    let state = "";
    if (this.stateList.length > 0) {
      if (userAddressInfo.address.stateId) {
        this.stateList.map(item => {
          if (item.id == userAddressInfo.address.stateId) {
            state = item.name
            return;
          }
        })
      }
    } else {
      stateId = !userAddressInfo.address.stateId ? null : userAddressInfo.address.stateId;
    }
    let bloodGroup = !userAddressInfo.bloodGroup ? null : userAddressInfo.bloodGroup;
    // this.userAddressInfoForm.controls['userUniqueId'].setValue(this.selectedUserUniqueId);
    this.userAddressInfoForm.controls['address'].setValue(userAddressInfo.address.address);
    this.userAddressInfoForm.controls['address2'].setValue("");
    this.userAddressInfoForm.controls['state'].setValue(state);
    this.userAddressInfoForm.controls['stateId'].setValue(stateId);
    this.userAddressInfoForm.controls['country'].setValue(country);
    this.userAddressInfoForm.controls['countryId'].setValue(countryId);
    this.userAddressInfoForm.controls['zipcode'].setValue(userAddressInfo.address.zipcode);
    // this.userAddressInfoForm.controls['createdOrUpdatedByUniqueId'].setValue(this.userData.uniqueId);
    this.userAddressInfoForm.controls['city'].setValue(userAddressInfo.address.city);
    this.userAddressInfoForm.controls['birthDate'].setValue(userAddressInfo.birthDate);
    this.userAddressInfoForm.controls['bloodGroup'].setValue(bloodGroup);
    this.userAddressInfoForm.controls['adharNumber'].setValue(userAddressInfo.adharNumber);
    this.userAddressInfoForm.controls['setMap'].setValue("");
    this.userAddressInfoForm.controls['isAdd'].setValue((userAddressInfo.address.address) ? false : true);
    this.userAddressInfoForm.controls['userId'].setValue(this.activeUser);
  }
  getStateList() {
    this._userService.getStateLiist().subscribe(
      response => {
        this.stateList = response;
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  nexttab(event) {
    var type = parseInt(event)
    this.scrollToTop();
    switch (type) {
      case 1:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'basic'
        break;
      case 2:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'other'
        break;
      case 3:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'guardian'
        break;
      case 4:
        this.isUserDeatilsShow = false;
        this.isAcademicFormShow = true;
        break;
      case 5:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'subject'
        break;
      case 6:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'workTiming'
        break;
      case 7:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'assignedLearners'
        break;
      case 8:
        this.isUserDeatilsShow = false;
        this.isWorkingDetailsForm = true;
        break;
    }
  }
  editClick(type) {
    this.scrollToTop();
    switch (type) {
      case 1:
        var id: number = 0;
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'basic'
        break;
      case 2:
        var id: number = 1;
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'other'
        break;
      case 3:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'guardian'
        break;
      case 4:
        this.isUserDeatilsShow = false;
        this.isAcademicFormShow = true;
        break;
      case 5:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'subject'
        break;
      case 6:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'workTiming'
        break;
      case 7:
        this.isUserDeatilsShow = false;
        this.isTabFormShow = true;
        this.activeTab = 'assignedLearners'
        break;
      case 8:
        this.isUserDeatilsShow = false;
        this.isWorkingDetailsForm = true;
        break;
    }
  }
  close() {
    this._commonService.iscancelClicked.next(this.userDetailsType)
  }
  _fetchData() {
    this.revenueRadialChart = {
      chart: {
        height: 200,
        type: 'radialBar',
      },
      plotOptions: {
        radialBar: {
          hollow: {
            size: '65%',
          },
          dataLabels: {
            name: {
              show: false
            },
            value: {
              fontSize: '24px',
              color: 'rgb(0, 136, 255)',
              offsetY: 10,
              formatter: (val) => {
                return val + '%';
              }
            }
          },
          track: {
            background: "#a9c4dc",
          }
        }
      },
      colors: ['rgb(0, 136, 255)'],
      labels: ['2/3 steps'],
      series: [70],
      stroke: {
        lineCap: 'round',
      },
    };
  }
  //guardian
  getGuardian(userId) {
    this.spinner.show();
    this._userService.getGuardians("student", this.selectedUserUniqueId).subscribe(
      response => {
        if (response && response.length > 0) {
          this.patchAllGuardians(response);
        } else {
          this.guardianForms = [];
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }
  patchAllGuardians(allGuardiansList: Array<any>) {
    this.guardianForms = new Array<FormGroup>();
    this.guardianIndex = 0;
    for (let i = 0; i < allGuardiansList.length; i++) {
      this.getUserEmail("guardian", allGuardiansList[i].uniqueId, 2, i);
      this.getUserMobile("guardian", allGuardiansList[i].uniqueId, 2, i);
      this.getUserAddress("guardian", allGuardiansList[i].uniqueId, 2, i);
      let guardianForm = this.formBuilder.group({
        uniqueId: allGuardiansList[i].uniqueId,
        relationUniqueId: allGuardiansList[i].relationUniqueId,
        studentUniqueId: allGuardiansList[i].studentUniqueId != null ? allGuardiansList[i].studentUniqueId : "",
        organizationUniqueId: allGuardiansList[i].organizationUniqueId,
        parentId: allGuardiansList[i].parentId,
        createdOrUpdatedByUniqueId: this.userData.uniqueId,
        generalInfo: this.patchGuardianGeneralInfo(allGuardiansList[i]),
        studentId: [this.activeUser],
        isNew: false,
        isShowVewMore: false,
        organizationId: [this.userData.organizationId],
        profileImage: null
      });
      this.guardianForms.push(guardianForm);
      this._userService.getImage("guardian", allGuardiansList[i].uniqueId, false).subscribe(
        response => {
          if (response != null && response) {
            if (response.image) {
              let gaurdianInfoForm = <FormGroup>this.guardianForms[i];
              gaurdianInfoForm.controls.profileImage.setValue(response.image);
            }
          }
        });
    }
  }
  patchGuardianGeneralInfo(generalInfo: any) {
    let relationName = ""
    this.relations.map(item => {
      if (item.id == generalInfo.relation) {
        relationName = item.name;
        return;
      }
    })
    return this.formBuilder.group({
      firstName: [generalInfo.firstName, Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      lastName: [generalInfo.lastName, Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      birthDate: [generalInfo.birthDate, Validators.nullValidator],
      fullName: [generalInfo.fullName],
      gender: [generalInfo.gender, Validators.nullValidator],
      bloodGroup: [generalInfo.bloodGroup, Validators.nullValidator],
      adharNumber: [generalInfo.adharNumber, Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      phoneNumber: [this.mobileNo.mobile, Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      emailId: [this.emailId.emailId, Validators.required],
      relation: [generalInfo.relation, Validators.required],
      relationName: [relationName],
    });
  }
  showHideViewMore(item) {
    item.value.isShowVewMore = !item.value.isShowVewMore
  }
  nextTab(nextTab: number) {
    switch (nextTab) {
      case 1:
        if (this.userDetailsType == 1) {
          this.mainActiveTab = "academicDetails"
          this.getUserAcademicInfo(this.activeUser)
        } else {
          this.mainActiveTab = "account";
          this.getUserCurrentStates();
        }
        this.scrollToTop();
        break;
      case 2:
        this.mainActiveTab = "account";
        this.getUserCurrentStates()
        this.scrollToTop();
        break;
      case 3:
        this.mainActiveTab = "basicDetails"
        this.getBasicInfo(this.selectedUserUniqueId);
        let usertype = this.userType[this.userDetailsType];
        this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "")
        if (this.userDetailsType == 3) {
          this.getShift(this.selectedUserUniqueId)
        } else if (this.userDetailsType == 1) {
          this.getGuardian(this.selectedUserUniqueId);
        }
        this.scrollToTop();
        break;
      case 4:
        if (this.userDetailsType == 3) {
          this.mainActiveTab = 'workDetails'
          this.getShift(this.selectedUserUniqueId)
          this.getUserPeriods(this.activeUser)
        } else if (this.userDetailsType == 1) {
          this.mainActiveTab = "performance";
        } else {
          this.mainActiveTab = "basicDetails";
          console.log(this.mainActiveTab);
        }
        this.scrollToTop();
        break;
    }
  }
  getUserAcademicInfo(userId: Number) {
    this.currentUserId = userId;
    // if (userId != null && userId != undefined) {
    this.spinner.show();
    this._userService.getAcademicInfo(this.selectedUserUniqueId).subscribe(
      response => {
        if (response && response.success) {
          this.selectedElectiveSub = [];
          this.studentAcademicInfo = response;
          let subjects = response.subjects;
          if (subjects.electiveSubjects.length > 0) {
            for (let index = 0; index < subjects.electiveSubjects.length; index++) {
              const element = subjects.electiveSubjects[index];
              for (let i = 0; i < element.subjects.length; i++) {
                const element2 = element.subjects[i];
                console.log(element2);
                if (element2.studentSubjectUniqueId) {
                  this.selectedElectiveSub.push(element)
                }
              }
            }
          }
        }
        // this.patchStudentAcademicInfo(this.studentAcademicInfo);
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          // this.initAcademicForm();
          // this.academicFormGroup.controls['userUniqueId'].setValue(this.currentUserId);
        } else {
          // this.toastService.ShowError(error);
          // this.initAcademicForm();
          // this.academicFormGroup.controls['userUniqueId'].setValue(this.currentUserId);
        }
        this.spinner.hide();
      });
    // }
  }

  onSelectMainTab(event) {
    let tabId = event.nextId;
    this.mainActiveTab = tabId;
    switch (tabId) {
      case 'basicDetails':
        this.getBasicInfo(this.selectedUserUniqueId);
        let usertype = this.userType[this.userDetailsType];
        this.getUserAddress(usertype, this.selectedUserUniqueId, 1, "");
        if (this.userDetailsType == 1) {
          this.getGuardian(this.selectedUserUniqueId);
        }
        break;
      case 'academicDetails':
        this.getUserAcademicInfo(this.activeUser)
        break;
      case 'account':
        this.getUserCurrentStates();
        break;
      case 'performance':
        break;
      case 'workDetails':
        this.mainActiveTab = 'workDetails'
        this.getShift(this.selectedUserUniqueId)
        this.getUserPeriods(this.activeUser)
        break;
    }
  }
  onSelectTeacherTab(event) {
    let tabId = event.nextId
    switch (tabId) {
      case 'workTimeForm':
        this.getShift(this.selectedUserUniqueId)
        break;
      case 'dayAndPeriod':
        this.getUserPeriods(this.activeUser)
        break;
    }
  }
  scrollToTop() {
    var elmnt = document.getElementById("detailsUser");
    elmnt.scrollIntoView();
    window.scroll(0, 0);
  }
  showAcademicDetails(event) {
    this.isAcademicFormShow = false;
    this.isTabFormShow = false;
    this.isUserDeatilsShow = true;
    this.getUserAcademicInfo(this.activeUser);
    this.mainActiveTab = "academicDetails"
  }
  openFile() {
    if (this.imageSrcFile && !this.oldImgSrc) {
      this.oldImgSrc = this.imageSrcFile
    }
    (document.getElementById("imgupload") as HTMLInputElement).value = "";
    document.getElementById("imgupload").click();
  }

  onSelectFile(event) {
    this.selectedFiles = event.target.files;
    this.createProfileFile = event.target.files[0];
    this.fileSelected = true;
    this.isUploadFile = true;
    this.selectFileForEditUser = true;
    var file = event.target.files[0];
    if (!file) return;
    const max_size = 149715200;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastService.ShowWarning('invalid format');
      return;
    }
    if (event.target.files[0].size > max_size) {
      this.imageError =
        'Maximum size allowed is ' + (max_size / 1000) / 1000 + 'Mb';
      return false;
    } else {
      const file = (event.target as HTMLInputElement).files[0];
      this.form.patchValue({
        file: file
      });
      this.form.get('file').updateValueAndValidity();
      this._handleReaderLoaded(event);
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }
  _handleReaderLoaded(e) {
    var reader = e.target;
    this.imageSrcFile = reader.result;
    this.currentFileUpload = this.selectedFiles[0];
    var formData: any = new FormData();
    formData.append("file", this.form.get('file').value);
    // return this._learnersService.fileUpload(formData, this.selectedUserUniqueId).subscribe(
    //   res => {
    //     if (res) {
    //       // console.log(res);
    //       this.toastService.showSuccess("Image upload successfully!");
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof Error) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       this.toastService.ShowError(error.toString());
    //     }
    //   });
  }
  uploadImage() {
    this.spinner.show();
    // this.basicFormGroup.controls['profileImage'].setValue(this.imageSrc);
    var formData: any = new FormData();
    let type = this.userType[this.userDetailsType];
    formData.append("file", this.form.get('file').value);
    this._userService.fileUpload(type, formData, this.selectedUserUniqueId).subscribe(
      response => {
        if (response) {
          this.oldImgSrc = "";
          this.isUploadFile = false
          // this.getBasicInfo(this.selectedUserUniqueId)
        }
        this.toastService.showSuccess("Image upload successfully.");
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  cancelImage() {
    this.imageSrcFile = (this.oldImgSrc) ? this.oldImgSrc : "";
    this.imageSrc = (this.oldImgSrc) ? this.oldImgSrc : "";
    this.oldImgSrc = "";
    this.isUploadFile = false
    this.fileSelected = false
  }
  getTeacherSubjects(userId: string) {
    this.spinner.show();
    this._userService.getTeacherSubjects(userId).subscribe(
      response => {
        if (response && response != null && response.subjects != null) {
          this.teacherSubjectList = response.subjects;
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        this.spinner.hide();
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
      });
  }
  getShift(Id) {
    this.spinner.show();
    let usertype = this.userType[this.userDetailsType];
    this._userService.getAssignedShift(usertype, this.selectedUserUniqueId).subscribe(
      response => {
        this.shiftList = []
        let ShiftsList = response;
        for (let i = 0; i < ShiftsList.length; i++) {
          ShiftsList[i].days.sort((a, b) => {
            return a.dayValue - b.dayValue;
          });
          this.shiftList.push(ShiftsList[i])
        }
        this.staffShiftData = response;
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }
  getChlidrens(userId) {
    this.spinner.show();
    let type = this.userType[this.userDetailsType];
    this._userService.getGuardianChildrens(type, this.selectedUserUniqueId).subscribe(
      response => {
        this.studentDataList = [];
        if (response && response.length > 0) {
          this.studentDataList = response;
          for (let index = 0; index < response.length; index++) {
            const element = response[index];
            this._userService.getImage("student", element.uniqueId, false).subscribe(
              response => {
                if (response != null && response) {
                  if (response.image) {
                    this.studentDataList[index]['profileImageId'] = response.image;
                  }
                }
              });
          }
          if (response.length <= 3) {
            this.remainingStudents = (3 - response.length)
          } else {
            this.remainingStudents = response.length
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  getUserPeriods(id: any) {
    this.spinner.show()
    this.shiftListWithPeriod = [];
    this._periodService.getAllPeriod().subscribe(
      response => {
        if (response && response.length > 0) {
          let shiftListWithPeriod = [];
          for (let i = 0; i < response.length; i++) {
            const element = response[i];
            this.shiftIdList.push(element.shift.uniqueId)
            shiftListWithPeriod.push({
              name: element.shift.name,
              uniqueId: element.shift.uniqueId,
              periods: element.periods,
              isAssignPeriodDays: false
            })
          }
          this.shiftListWithPeriod = shiftListWithPeriod
          this.getShiftData();
        }
        this.spinner.hide()
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide()
      });
  }
  getShiftData() {
    this.spinner.show();
    this._userService.getAssignedShift("teacher", this.selectedUserUniqueId).subscribe(
      response => {
        this.shiftListData = []
        let shiftList = []
        let ShiftsList = response;
        if (ShiftsList.length > 0) {
          this.shiftListData = response;
          let shift = ShiftsList[0];
          // let daysArray = [{ day: "MON", dayValue: 2, id: 2, isSelected: false,uniqueId:"" }, { day: "TUE", dayValue: 3, id: 3, isSelected: false, uniqueId:""}, { day: "WED", dayValue: 4, id: 4, isSelected: false, uniqueId:"" }, { day: "THU", dayValue: 5, id: 5, isSelected: false, uniqueId:"" }, { day: "FRI", dayValue: 6, id: 6, isSelected: false, uniqueId:"" }, { day: "SAT", dayValue: 7, id: 7, isSelected: false, uniqueId:"" }, { day: "SUN", dayValue: 1, id: 1, isSelected: false, uniqueId:"" }]
          let index = this.shiftIdList.findIndex(x => x == shift.uniqueId);
          if (index != -1) {
            let period = this.shiftListWithPeriod[index];
            this.currentIndex = index;
            this._userService.getAssignedPeriod(this.selectedUserUniqueId, period.uniqueId).subscribe(
              res => {
                if (res.teacherPeriods && res.teacherPeriods.length > 0) {
                  this.assignPeriod(period, shift, index, res.teacherPeriods);

                } else {
                  this.assignPeriod(period, shift, index, []);
                }
              },
              (error: HttpErrorResponse) => {

                this.assignPeriod(period, shift, index, []);
              });

          } else {
            this.isPeriodListData = false;
            this.currentIndex = index;
          }
        }

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }
  assignPeriod(period, shift, index, alreadySelected) {
    let mon = false;
    let tue = false;
    let wed = false;
    let thu = false;
    let fri = false;
    let sat = false;
    let sun = false;
    for (let i = 0; i < period.periods.length; i++) {
      const element = period.periods[i];
      if (alreadySelected.length > 0) {
        for (let j = 0; j < alreadySelected.length; j++) {
          const element1 = alreadySelected[j];
          period.periods[i]['tue'] = tue;
          period.periods[i]['wed'] = wed;
          period.periods[i]['thu'] = thu;
          period.periods[i]['fri'] = fri;
          period.periods[i]['sat'] = sat;
          period.periods[i]['sun'] = sun;
          period.periods[i]['mon'] = mon;
          if (element1.period.uniqueId == element.uniqueId) {
            for (let k = 0; k < element1.shiftDays.length; k++) {
              const element3 = element1.shiftDays[k];
              switch (element3.dayValue) {
                case 1:
                  period.periods[i]['sun'] = true;
                  break;
                case 2:
                  period.periods[i]['mon'] = true;
                  break;
                case 3:
                  period.periods[i]['tue'] = true;
                  break;
                case 4:
                  period.periods[i]['wed'] = true;
                  break;
                case 5:
                  period.periods[i]['thu'] = true;
                  break;
                case 6:
                  period.periods[i]['fri'] = true;
                  break;
                case 7:
                  period.periods[i]['sat'] = true;
                  break;
              }
            }
            break;
          }
        }
      } else {
        period.periods[i]['tue'] = tue;
        period.periods[i]['wed'] = wed;
        period.periods[i]['thu'] = thu;
        period.periods[i]['fri'] = fri;
        period.periods[i]['sat'] = sat;
        period.periods[i]['sun'] = sun;
        period.periods[i]['mon'] = mon;
      }
    }
    this.selectedShiftData = period;
    this.shiftListWithPeriod[index] = period;
    this.shiftListWithPeriod[index].isAssignPeriodDays = true;
    this.isPeriodListData = true;
  }
  onSelectTab(event, i) {
    this.spinner.show();
    let shiftdata = this.shiftList[i];
    let index = this.shiftIdList.findIndex(x => x == shiftdata.uniqueId);
    if (index != -1) {
      if (this.currentIndex >= 0) {
        this.shiftListWithPeriod[this.currentIndex] = this.selectedShiftData;
      }
      let period = this.shiftListWithPeriod[index];
      this.currentIndex = index;
      if (!period.isAssignPeriodDays) {
        this._userService.getAssignedPeriod(this.selectedUserUniqueId, period.uniqueId).subscribe(
          res => {
            if (res.teacherPeriods && res.teacherPeriods.length > 0) {
              this.assignPeriod(period, shiftdata, index, res.teacherPeriods);

            } else {
              this.assignPeriod(period, shiftdata, index, []);
            }
          },
          (error: HttpErrorResponse) => {

            this.assignPeriod(period, shiftdata, index, []);
          });
      } else {
        this.selectedShiftData = period;
      }

      this.isPeriodListData = true;
      this.spinner.hide();
    } else {
      this.isPeriodListData = false;
      this.currentIndex = index;
      this.spinner.hide();
    }
  }
  activeDectiveModal(activeModal) {
    console.log(this.isAccountActivate);
    this.modalService.open(activeModal, { backdrop: 'static', windowClass: 'activeDeactiveModal' })
  }
  changeToggel(event, activeModal) {
    console.log(this.isAccountActivate);
    this.modalService.open(activeModal, { backdrop: 'static', windowClass: 'activeDeactiveModal' })
  }
  getUserStates() {
    this.spinner.show()
    let type = this.userType[this.userDetailsType];
    this._userService.getUserStates(type).subscribe(
      response => {
        if (response && response.success) {
          this.userStates = response.list;
        } else {
          this.toastService.ShowWarning(response.responseMessage);
        }
        this.spinner.hide()
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide()
      });
  }
  getUserCurrentStates() {
    this.spinner.show()
    let type = this.userType[this.userDetailsType];
    this._userService.getUserCurrentStates(type, this.selectedUserUniqueId).subscribe(
      response => {
        if (response) {
          this.currentStateData = response;
          this.isToggelSwitchOn = (response.id == 1) ? true : false;
          this.isAccountActivate = (response.id == 1) ? true : false;
        } else {
          // this.toastService.ShowWarning("");
        }
        this.spinner.hide()
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide()
      });
  }
  updateState() {
    // this.spinner.show();
    let type = this.userType[this.userDetailsType];
    let newState = "";
    let currentState = "";
    this.userStates.forEach(element => {
      if (this.isAccountActivate && element.id == 3) {
        newState = element.id;
        currentState = element.name;
      } else if (!this.isAccountActivate && element.id == 1) {
        newState = element.id;
        currentState = element.name;
      }
    });
    let dto = {
      users: [this.selectedUserUniqueId],
      newState: newState
    };
    this._userService.updateUserState(type, dto).subscribe(
      response => {
        if (response && response.success) {
          this.isAccountActivate = (this.isAccountActivate) ? true : false;
          this.isToggelSwitchOn = (this.isAccountActivate) ? true : false;
          this.getUserCurrentStates();
          this.modalService.dismissAll();
        } else {
          this.toastService.ShowWarning(response.responseMessage);
        }
        this.spinner.hide()
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide()
      });
  }
  closePopup() {
    this.isToggelSwitchOn = (this.isAccountActivate) ? true : false;
    console.log(this.isToggelSwitchOn);
  }
}