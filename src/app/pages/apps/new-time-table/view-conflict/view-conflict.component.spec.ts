import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewConflictComponent } from './view-conflict.component';

describe('ViewConflictComponent', () => {
  let component: ViewConflictComponent;
  let fixture: ComponentFixture<ViewConflictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewConflictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewConflictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
