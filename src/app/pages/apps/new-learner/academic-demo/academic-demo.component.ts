import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-academic-demo',
  templateUrl: './academic-demo.component.html',
  styleUrls: ['./academic-demo.component.scss']
})
export class AcademicDemoComponent implements OnInit {

  courseData: any = ['NEET 22', 'MCA', 'BCA'];
  boardData: any = ['CBSE', 'MAH', 'DEL'];
  subjectList: any = [{ id: '1', name: 'Physics' }, { id: '2', name: 'Maths' }, { id: '3', name: 'Chemistry' }, { id: '4', name: 'Architecture' }, { id: '5', name: 'English' }, { id: '6', name: 'Science' }, { id: '7', name: 'Hindi' }, { id: '8', name: 'Marathi' }, { id: '9', name: 'CPP' }, { id: '10', name: 'Angular' }];
  opsubjectList: any = [];

  elsubjectList: any = [{ id: '1', name: 'Sanskrit' }, { id: '2', name: 'French' }, { id: '3', name: 'Tamil' }]
  evsubjectList: any = [{ id: '1', name: 'Coding' }, { id: '2', name: 'Design' }, { id: '3', name: 'Networking' }]

  batchGroupPerformanceList: any = [{ id: '1', name: 'Performer' }, { id: '2', name: 'Archive' }, { id: '3', name: 'Fighter' }]
  batchGroupGenderList: any = [{ name: 'Male', selected: false }, { name: 'Female', selected: false }, { name: 'Others', selected: false }]
  relations: any = ['Father', 'Mother', 'Brother', 'Sister', 'Uncle', 'Aunty'];


  currentUserId: Number = null;
  loaderBasicInfo = false
  loading = false;
  userData: any;
  academicFormGroup: FormGroup;
  Gender: string = '';
  Performance: any = [];
  Language: any = [];
  Vocational: any = [];
  studentAcademicInfo: any;
  orgBatchList: any;
  academicCalendarList: any;
  orgCourseList = [];
  complsorySubjects = [];
  optionalSubject = [];
  electiveSubject = [];
  selectedOptionalSubject = [];
  getbatchHasGroupList: any;
  isCheckTrue = false;
  orgBoardList = [];
  orgGradeList = [];
  orgBatchGroupList = [];
  orgGroupCategoreyList = [];
  academicYearList = [];
  selectedGroupCategory = [];
  selectedBatchGroup = [];
  submitted = false;
  selectedCourseUniqueId = "";
  isSelectElectiveSub = true;
  selectedElectiveSub = [];
  selectedOptioanlSub = [];
  @Input() public userId: Number;
  @Input() public currentTab: Number;
  @Input() public userUniqueId: any;
  @Output() showAcademicDetails = new EventEmitter<string>();
  // @Input() public academicFormGroup:FormGroup;
  constructor(private formBuilder: FormBuilder, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private datePipe: DatePipe, private _courseService: CourseServiceService, private _batchService: BatchesServiceService, private _boardService: BoardServiceService, private _gradeService: GradeServiceService, private _userService: UserServiceService, private _subjectService: SubjectServiceService) {
    // this.currentUserId = localStorage.getItem("currentUserId");
    this.currentUserId = 114;
    this.userData = this._commonService.getUserData();
    this.initStudentBasicInfoForm();

  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['currentTab'] && changes['currentTab'].currentValue == 4) {
      // this.getAcademicYearList();
      // this.getBatchGroupList("");
      //this.getGroupCategoryList();
      this.getCourseListByAcademicCalendar("");
      // this.getBatch(this.currentUserId)
      this.getUserAcademicInfo(this.userId)
      window.scroll(0, 0);
    }
  }



  initStudentBasicInfoForm() {
    this.academicFormGroup = this.formBuilder.group({
      academicYearUniqueId: ['', Validators.nullValidator],
      academicCalendarUniqueId: ['', Validators.nullValidator],
      courseUniqueId: [null, Validators.required],
      boardUniqueId: [null, Validators.required],
      // batch: ['', Validators.required],
      divisionUniqueId: [null, Validators.required],
      // grade: ['', Validators.required],
      semesterUniqueId: [null, Validators.required],
      batchHasGroupList: this.formBuilder.array([]),
      electiveSubjectList: this.formBuilder.array([this.initElectiveSubjectGroup()]),
      selectedComplsorySubject: ['', Validators.required],
      selectedOptionalSubject: ['', Validators.nullValidator],
      userUniqueId: ['', Validators.nullValidator],
      year: ['', Validators.nullValidator],
      batchGroup: [null, Validators.required],
      gruopCategory: [null, Validators.required],
      createdOrUpdatedByUniqueId: [this.userData.uniqueId, Validators.nullValidator]
    })
  }

  getUserAcademicInfo(userId: Number) {

    this.currentUserId = userId;

    // if (userId != null && userId != undefined) {
    this.spinner.show();
    // this.saveState = 2;
    this._userService.getAcademicInfo(this.userUniqueId).subscribe(
      response => {
        this.initStudentBasicInfoForm();
        if (response && response.success) {
          this.studentAcademicInfo = response;
          this.patchStudentAcademicInfo(this.studentAcademicInfo);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          this.initStudentBasicInfoForm();
          this.academicFormGroup.controls['userUniqueId'].setValue(this.currentUserId);
        } else {
        //  this.toastService.ShowError(error);
          this.initStudentBasicInfoForm();
          this.academicFormGroup.controls['userUniqueId'].setValue(this.currentUserId);
        }
        this.spinner.hide();
      }
    );
    // }
  }
  patchStudentAcademicInfo(StudentAcademicInfo: any) {
    // this.academicFormGroup.controls['userUniqueId'].setValue(this.currentUserId);
    let subjects = StudentAcademicInfo.subjects;
    if (subjects.optionalSubjects.length > 0) {
      for (let index = 0; index < subjects.optionalSubjects.length; index++) {
        const element = subjects.optionalSubjects[index];
        if (element.studentSubjectUniqueId) {
          this.selectedOptioanlSub.push(element.csUniqueId)
        }
      }
    }
    if (subjects.electiveSubjects.length > 0) {
      for (let index = 0; index < subjects.electiveSubjects.length; index++) {
        const element = subjects.electiveSubjects[index];
        for (let i = 0; i < element.subjects.length; i++) {
          const element2 = element.subjects[i];
          if (element2.studentSubjectUniqueId) {
            this.selectedElectiveSub.push(element2.csUniqueId)
          }
        }
      }
    }


    if (StudentAcademicInfo.course != null && StudentAcademicInfo.course != undefined) {
      this.academicFormGroup.controls['courseUniqueId'].setValue(StudentAcademicInfo.course.uniqueId);
      this.getCourseSubjects(StudentAcademicInfo.course.uniqueId);
      this.selectedCourseUniqueId = StudentAcademicInfo.course.uniqueId
      this.getBatchList(StudentAcademicInfo.course.uniqueId);
      this.getBoardList(StudentAcademicInfo.course.uniqueId);
    }
    let batchGroupDetails = StudentAcademicInfo.batchGroupDetails[0];

    if (StudentAcademicInfo.batch != null && StudentAcademicInfo.batch != undefined) {
      // this.getCustomizedBatch(StudentAcademicInfo.divisionUniqueId);
      this.getGroupCategoryList(StudentAcademicInfo.batch.uniqueId, batchGroupDetails.groupCategory.uniqueId)
      this.academicFormGroup.controls['divisionUniqueId'].setValue(StudentAcademicInfo.batch.uniqueId);
    }
    if (StudentAcademicInfo.board != null && StudentAcademicInfo.board != undefined) {
      this.getGradeList(StudentAcademicInfo.board.uniqueId)
      this.academicFormGroup.controls['boardUniqueId'].setValue(StudentAcademicInfo.board.uniqueId);
    }
    if (StudentAcademicInfo.grade != null && StudentAcademicInfo.grade != undefined) {
      this.academicFormGroup.controls['semesterUniqueId'].setValue(StudentAcademicInfo.grade.uniqueId);
    }
    if (StudentAcademicInfo.batchGroupDetails != null && StudentAcademicInfo.batchGroupDetails != undefined && StudentAcademicInfo.batchGroupDetails.length > 0) {

      this.academicFormGroup.controls['batchGroup'].setValue(batchGroupDetails.batchGroups[0].uniqueId);
      this.academicFormGroup.controls['gruopCategory'].setValue(batchGroupDetails.groupCategory.uniqueId);
    }
    console.log(this.academicFormGroup.value, this.academicFormGroup);
  }
  getBatchList(courseId: string) {

    if (courseId != null && courseId != undefined && courseId != "") {
      this.spinner.show();
      this._batchService.getBatchByCourse(courseId).subscribe(
        res => {
          if (res.success == true) {
            this.orgBatchList = res.list;
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        }
      );
    }
  }

  getCourseListByAcademicCalendar(academicCalendarUniqueId: string) {
    this._courseService.getCourseDetailsedInfoList(3).subscribe(
      res => {
        if (res.success == true) {
          // console.log("get courseList => ",res)
          this.orgCourseList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      }
    );
  }
  getCourseSubjects(CoursId: string) {

    this.selectedOptionalSubject = []
    this.optionalSubject = [];

    if (CoursId != null && CoursId != undefined && CoursId != "") {
      this.spinner.show();
      this._subjectService.getCourseSubjects(CoursId).subscribe(
        (res) => {
          if (res && res.success) {

            let data = (res.list.length > 0) ? res.list[0] : [];
            this.complsorySubjects = (data.compulsorySubjects && data.compulsorySubjects.length > 0) ? this.patchSubjectList(data.compulsorySubjects) : [];
            let electiveSubject = data.electiveGroupsAndSubjects;

            this.academicFormGroup.controls['selectedComplsorySubject'].setValue(this.complsorySubjects);

            let electiveGroupList = <FormArray>this.academicFormGroup.controls['electiveSubjectList'];
            electiveGroupList.controls = [];
            if (electiveSubject != null && electiveSubject.length > 0) {
              let electiveSubjectGroupList = [];
              for (let i = 0; i < electiveSubject.length; i++) {
                let formGroup = {
                  name: electiveSubject[i].name,
                  uniqueId: electiveSubject[i].uniqueId,
                  electiveSubjects: this.patchSubjects(electiveSubject[i].electiveSubjects)
                };
                electiveSubjectGroupList.push(formGroup);
              }
              this.electiveSubject = electiveSubjectGroupList;
              // this.academicFormGroup.setControl('electiveSubjectList', this.formBuilder.array(electiveSubjectGroupList));
            }

            data.optionalSubjects.forEach(element => {
              let elemnt = {
                uniqueId: element.uniqueId,
                subjectName: element.name,
                csUniqueId: element.courseHasSubjectUniqueId,
                studentSubjectUniqueId: ""
              };
              if (this.selectedOptioanlSub.includes(element.courseHasSubjectUniqueId)) {
                this.selectedOptionalSubject.push(elemnt);
              } else {
                this.optionalSubject.push(elemnt);
              }
            });
          } else {
            this.toastService.ShowWarning(res.responseMessage);
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   // console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }
  }
  patchSubjects(subjectList: Array<any>) {
    let electiveSubjectList = [];

    if (subjectList != null && subjectList.length > 0) {
      for (let j = 0; j < subjectList.length; j++) {
        let subject = {
          uniqueId: subjectList[j].uniqueId,
          subjectName: subjectList[j].name,
          csUniqueId: subjectList[j].courseHasSubjectUniqueId,
          studentSubjectUniqueId: "",
          selected: (this.selectedElectiveSub.includes(subjectList[j].courseHasSubjectUniqueId)) ? true : false
        };
        electiveSubjectList.push(subject);
      }
    }
    return electiveSubjectList;
  }
  patchSubjectList(subjectList: Array<any>) {
    let subjectListData = [];

    if (subjectList != null && subjectList.length > 0) {
      for (let j = 0; j < subjectList.length; j++) {
        let subject = {
          uniqueId: subjectList[j].uniqueId,
          subjectName: subjectList[j].name,
          csUniqueId: subjectList[j].courseHasSubjectUniqueId,
          studentSubjectUniqueId: ""
        };
        subjectListData.push(subject);
      }
    }
    return subjectListData;
  }
  initElectiveSubjectGroup(): FormGroup {
    return this.formBuilder.group({
      groupTitle: ['', Validators.nullValidator],
      electiveSubject: this.formBuilder.array([this.initElectiveSubjectList()])
    });
  }
  initElectiveSubjectList(): FormGroup {
    return this.formBuilder.group({
      name: ['', Validators.nullValidator],
      uniqueId: ['', Validators.nullValidator],
      courseHasSubjectId: ['', Validators.nullValidator],
      selected: [false, Validators.nullValidator]
    });
  }
  // -----------------------------Start Get category List By groupCategoryList---------------------------
  getcategorylist(groupCategoryList) {

    let isLinkedArray = this.formBuilder.array([]);
    for (let j = 0; j < groupCategoryList.length; j++) {
      let subject = this.formBuilder.group({
        isLinked: groupCategoryList[j].isLinked,
        uniqueId: groupCategoryList[j].uniqueId,
        name: groupCategoryList[j].name,
      });
      isLinkedArray.push(subject);
    }
    return isLinkedArray;
  }
  getBoardList(courseUniqueId) {
    this.spinner.show();
    let courseArray = [courseUniqueId]
    return this._boardService.getBoardByCourseUid(courseArray).subscribe(
      res => {
        if (res) {
          this.orgBoardList = res[0].boards;
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
         
        // }
        this.orgBoardList = [];
        this.spinner.hide();
      }
    );
  }

  getGradeList(uniqueId) {
    this.spinner.show();
    let uniqueIds = [uniqueId];
    return this._gradeService.getAcademicGradeListByBoard(uniqueIds).subscribe(
      res => {
        if (res.success == true) {
          // this.orgGradeList = res[0].grades;
          let gradeList = []
          for (let i = 0; i < res.list[0].courseList.length; i++) {
            const element = res.list[0].courseList[i];
            if (this.selectedCourseUniqueId == element.courseDto.uniqueId) {
              // gradeList=element.grades
              this.orgGradeList = element.grades;
            }
          }
          // this.orgGradeList = gradeList;
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        
        // }
        this.spinner.hide();
        this.orgGradeList = [];
      }
    );
  }
  get af() {
    return this.academicFormGroup.controls;
  }
  selectGroupOfBatch(batchData, group) {

    for (let i = 0; i < this.academicFormGroup.value.batchHasGroupList.length; i++) {
      if (this.academicFormGroup.value.batchHasGroupList[i].uniqueId == batchData.value.uniqueId) {
        for (let j = 0; j < this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList.length; j++) {
          if (this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList[j].uniqueId == group.value.uniqueId) {
            this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList[j].isLinked = true
          }
          else {
            this.academicFormGroup.value.batchHasGroupList[i].groupCategoryList[j].isLinked = false
          }
        }
      }
    }
  }
  saveStudentAcademicInfo() {
    this.submitted = true;

    if (this.academicFormGroup.invalid) {
      this.scrollToError();
      return false;
    }

    this.spinner.show();
    let electiveSubjectGroup = [];
    if (this.electiveSubject.length > 0) {
      for (let i = 0; i < this.electiveSubject.length; i++) {
        const element = this.electiveSubject[i];
        let electiveDto = {
          electiveGroup: {
            name: element.name,
            uniqueId: element.uniqueId
          },
          subjects: []
        }
        for (let j = 0; j < element.electiveSubjects.length; j++) {
          const element2 = element.electiveSubjects[j];
          if (element2.selected) {
            electiveDto.subjects.push({
              subjectName: element2.subjectName,
              csUniqueId: element2.csUniqueId,
              studentSubjectUniqueId: ""
            })
            electiveSubjectGroup.push(electiveDto)
          }
        }
      }
    }
    let academicData = this.academicFormGroup.value
    let dto = {
      studentUniqueIds: [this.userUniqueId],
      course: {
        uniqueId: academicData.courseUniqueId
      },
      batch: {
        uniqueId: academicData.divisionUniqueId
      },
      board: {
        uniqueId: academicData.boardUniqueId
      },
      grade: {
        uniqueId: academicData.semesterUniqueId
      },
      batchGroupDetails: [
        {
          groupCategory: this.selectedGroupCategory[0],
          batchGroups: this.selectedBatchGroup
        }
      ],
      subjects: {
        compulsorySubjects: this.complsorySubjects,
        optionalSubjects: this.selectedOptionalSubject,
        electiveSubjects: electiveSubjectGroup
      }

    }
    // this.academicFormGroup.controls['selectedOptionalSubject'].setValue(JSON.stringify(this.selectedOptionalSubject));
    // this.academicFormGroup.controls['selectedComplsorySubject'].setValue(JSON.stringify(this.complsorySubjects));

    this._userService.assignAcademicInfo(dto).subscribe(
      response => {
        this.toastService.showSuccess("Student academic info saved successfully.");
        this.spinner.hide();
        this.submitted = false;
        this.showAcademicDetails.emit("");
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');

    this.scrollTo(firstElementWithError);
  }
  close() {
    this._commonService.iscloseClicked.next(2)
  }
  //drop event here 
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex)
    } else {
      moveItemInArray(this.selectedOptionalSubject, event.previousIndex, event.currentIndex);
    }
  }
  onSelectionChange(index: number, subjectIndex: number, event: any) {
    // let electiveSubjectListArray = <FormArray>this.academicFormGroup.controls['electiveSubjectList'];

    let electiveSubjectGroup = this.electiveSubject[index];

    let electiveSubject = electiveSubjectGroup.electiveSubjects;
    if (event == true) {
      for (let i = 0; i < electiveSubject.length; i++) {
        let formGroup = electiveSubject[i];
        console.log(formGroup);
        if (subjectIndex == i) {
          formGroup.selected = !formGroup.selected;
        } else {
          formGroup.selected = false;
        }
      }
    }
  }
  getDatafromCrourseList(event) {
    this.selectedElectiveSub = [];
    this.selectedOptioanlSub = [];
    this.orgBatchGroupList = [];
    this.orgGradeList = [];
    this.academicFormGroup.controls['divisionUniqueId'].setValue(null);
    this.academicFormGroup.controls['boardUniqueId'].setValue(null);
    this.academicFormGroup.controls['semesterUniqueId'].setValue(null);
    this.academicFormGroup.controls['batchGroup'].setValue(null);
    this.getCourseSubjects(event.uniqueId)
    this.getBatchList(event.uniqueId)
    this.getBoardList(event.uniqueId)
    this.selectedCourseUniqueId = event.uniqueId
  }
  getCustomBatch(event) {
    if (event != null && event != undefined && event != '') {
      // this.getCustomizedBatch(event.uniqueId)
      this.getGroupCategoryList(event.uniqueId, "")
    }
  }
  removeSubjectFromList(event, index, number, item) {
    this.selectedOptionalSubject.splice(index, 1);
    this.optionalSubject.push(item);
  }
  changeBoard(event) {
    this.orgGradeList = [];
    this.academicFormGroup.controls['semesterUniqueId'].setValue(null);
    this.getGradeList(event.uniqueId)
  }
  getBatchGroupList(uniqueId) {
    this.spinner.show();
    return this._batchService.getBatchGroupList(uniqueId).subscribe(
      res => {
        if (res) {
          this.orgBatchGroupList = res[0].batchGroup;
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
         
        // }
        this.spinner.hide();
        this.orgBatchGroupList = [];
      }
    );
  }
  getGroupCategoryList(uId, categoryuid) {
    this.spinner.show();
    return this._batchService.getCtegoryGroupByBatchUid([uId]).subscribe(
      res => {
        if (res && res.success == true) {
          this.orgGroupCategoreyList = res.batchCategoryWiseGroups[0].batchGroupList;
          if (categoryuid) {
            res.batchCategoryWiseGroups[0].batchGroupList.forEach(element => {
              if (element.categoryUniqueId == categoryuid) {
                this.orgBatchGroupList = element.batchGroups;
              }
            });
          }

          console.log("group Category", this.orgGroupCategoreyList)
        }
        else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
       
        // }
        this.spinner.hide();
        this.orgGroupCategoreyList = [];
      }
    );
  }
  changeGroupCategory(event) {
    this.selectedGroupCategory = [];
    this.selectedBatchGroup = [];
    this.orgBatchGroupList = (event.batchGroups) ? event.batchGroups : [];
    this.academicFormGroup.controls['batchGroup'].setValue(null);
    this.selectedGroupCategory.push(event)
  }
  changeBatchGroup(event) {
    this.selectedBatchGroup = [];
    this.selectedBatchGroup.push(event)
  }
}
