let url = 'https://eip-prod.byteachers.com/';
let apiContextPath = 'eicoreservices/api';
let contextPath = 'eicoreservices';
let eaasContextPath = 'eaaservices/api';
let newEaasContextPath = 'eaaservices';
let qbsContextPath = 'qbsservices/api';
let stlContextPath = 'stlservices/api';
let imageContextPath = 'eicoreservices/api/image';
let dmsContextPath = 'dmsservices/doc/file/upload';
let dmsDownloadContextPath = 'dmsservices/doc/documentstore';
let fileSaveContextPath = 'dmsservices/doc/save';
let dmsActualContextPath = 'dmsservices/doc/';
let smsGatewayContextPath = 'smsservices/api';
let websocketContextPath = 'stlservices/timeline';
let fileContextPath = 'eicoreservices/api';
let apiNodeContextPath = 'evaluation/';
let authContextPath = 'authentication-services/api'
let newauthContextPath = 'authentication-services';


export const environment = {
  production: true,
  url: url,
  apiUrl: url + apiContextPath,
  newApiUrl: url + contextPath,
  newAuthApiUrl: url + newauthContextPath,
  eaasUrl: url + eaasContextPath,
  newEaasUrl: url + newEaasContextPath,
  qbsUrl: "https://qbs.byteachers.com/" + qbsContextPath,
  
  stlUrl: url + stlContextPath,
  imageUrl: url + imageContextPath,
  dmsUrl: url + dmsContextPath,
  dmsDownloadUrl: url + dmsDownloadContextPath,
  fileSaveUrl: url + fileSaveContextPath,
  dmsActualUrl: url + dmsActualContextPath,
  smsGatewayUrl: url + smsGatewayContextPath,
  websocketUrl: url + websocketContextPath,
  fileUrl: url + fileContextPath,
  apiNodeUrl: url + apiNodeContextPath,
  authUrl: url + authContextPath,
  COURSE_NOT_EXPIRED : 1,
  COURSE_EXPIRED: 2,
  BOTH_COURSES: 3
};
