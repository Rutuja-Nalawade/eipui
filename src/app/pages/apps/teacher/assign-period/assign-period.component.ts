import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { TeacherService } from '../teacher.service';
import * as moment from 'moment';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { PeriodServiceService } from 'src/app/servicefiles/period-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-assign-period',
  templateUrl: './assign-period.component.html',
  styleUrls: ['./assign-period.component.scss']
})
export class AssignPeriodComponent implements OnInit {

  teacherPeriodForm: FormGroup;
  selectedSubjectList = [];
  workingDaysLists: any;
  saveState: number = 0;
  userData: any;
  slotList = [];
  slotLists = [];
  slotListss = [];
  PeriodSetting: {};
  // teacherPeriodData = [];
  settings = {};
  currentUserId;
  shiftIdList = [];
  shiftListWithPeriod = [];
  shiftList = [];
  selectedDays = [];
  selectedShiftData: any = {};
  selectedShiftPeriodData: any = {};
  isPeriodListData = false;
  currentIndex = 0;
  @Input() userId: any;
  @Input() userUniqueId: any;
  @Input() currentTab: Number;
  @Input() teacherPeriodData: any;
  @Input() public userDetailsType: any;
  @Output() nextTab = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private _teacherService: TeacherService, private toastService: ToastsupportService, private _periodService: PeriodServiceService, private _userService: UserServiceService) {
    this.userData = this._commonService.getUserData();
  }

  ngOnInit() {

    this.settings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: false,
      enableCheckAll: false
    };

  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['currentTab'] && changes['currentTab'].currentValue == 5) {
      this.currentUserId = this.userId;
      this.getPeriods();
      // this.getChlidrens(this.userId)
      // this.getUserPeriods(this.userId)
    }
    if (changes['teacherPeriodData'].currentValue != undefined) {
    }
  }
  getPeriods() {
    this.spinner.show()
    this.shiftListWithPeriod = [];
    this._periodService.getAllPeriod().subscribe(
      response => {
        if (response && response.length > 0) {
          let shiftListWithPeriod = [];
          for (let i = 0; i < response.length; i++) {
            const element = response[i];
            this.shiftIdList.push(element.shift.uniqueId)
            shiftListWithPeriod.push({
              name: element.shift.name,
              uniqueId: element.shift.uniqueId,
              periods: element.periods,
              isAssignPeriodDays: false
            })
          }
          this.shiftListWithPeriod = shiftListWithPeriod
          this.getShift();
        }
        this.spinner.hide()
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide()
      });
  }
  getShift() {
    this.spinner.show();
    this.shiftList = []
    this._userService.getAssignedShift("teacher", this.userUniqueId).subscribe(
      response => {
        let shiftList = []
        let ShiftsList = response;
        if (ShiftsList.length > 0) {
          this.shiftList = response;
          let shift = ShiftsList[0];
          // let daysArray = [{ day: "MON", dayValue: 2, id: 2, isSelected: false,uniqueId:"" }, { day: "TUE", dayValue: 3, id: 3, isSelected: false, uniqueId:""}, { day: "WED", dayValue: 4, id: 4, isSelected: false, uniqueId:"" }, { day: "THU", dayValue: 5, id: 5, isSelected: false, uniqueId:"" }, { day: "FRI", dayValue: 6, id: 6, isSelected: false, uniqueId:"" }, { day: "SAT", dayValue: 7, id: 7, isSelected: false, uniqueId:"" }, { day: "SUN", dayValue: 1, id: 1, isSelected: false, uniqueId:"" }]
          let index = this.shiftIdList.findIndex(x => x == shift.uniqueId);
          if (index != -1) {
            let period = this.shiftListWithPeriod[index];
            this.currentIndex = index;
            this._userService.getAssignedPeriod(this.userUniqueId, period.uniqueId).subscribe(
              res => {
                if (res.teacherPeriods && res.teacherPeriods.length > 0) {
                  this.assignPeriod(period, shift, index, res.teacherPeriods);

                } else {
                  this.assignPeriod(period, shift, index, []);
                }
              },
              (error: HttpErrorResponse) => {

                this.assignPeriod(period, shift, index, []);
              });

          } else {
            this.isPeriodListData = false;
            this.currentIndex = index;
          }
        }

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }
  assignPeriod(period, shift, index, alreadySelected) {
    let mon = { isSelected: false, isDisabled: true, workingdayIndex: 0, uniqueId: "" };
    let tue = { isSelected: false, isDisabled: true, workingdayIndex: 0, uniqueId: "" };
    let wed = { isSelected: false, isDisabled: true, workingdayIndex: 0, uniqueId: "" };
    let thu = { isSelected: false, isDisabled: true, workingdayIndex: 0, uniqueId: "" };
    let fri = { isSelected: false, isDisabled: true, workingdayIndex: 0, uniqueId: "" };
    let sat = { isSelected: false, isDisabled: true, workingdayIndex: 0, uniqueId: "" };
    let sun = { isSelected: false, isDisabled: true, workingdayIndex: 0, uniqueId: "" };
    for (let i = 0; i < shift.days.length; i++) {
      const element = shift.days[i];
      switch (element.dayValue) {
        case 1:
          sun = { isSelected: false, isDisabled: false, workingdayIndex: 1, uniqueId: element.uniqueId }
          break;
        case 2:
          mon = { isSelected: false, isDisabled: false, workingdayIndex: 2, uniqueId: element.uniqueId }
          break;
        case 3:
          tue = { isSelected: false, isDisabled: false, workingdayIndex: 3, uniqueId: element.uniqueId }
          break;
        case 4:
          wed = { isSelected: false, isDisabled: false, workingdayIndex: 4, uniqueId: element.uniqueId }
          break;
        case 5:
          thu = { isSelected: false, isDisabled: false, workingdayIndex: 5, uniqueId: element.uniqueId }
          break;
        case 6:
          fri = { isSelected: false, isDisabled: false, workingdayIndex: 6, uniqueId: element.uniqueId }
          break;
        case 7:
          sat = { isSelected: false, isDisabled: false, workingdayIndex: 7, uniqueId: element.uniqueId }
          break;
      }
    }
    for (let i = 0; i < period.periods.length; i++) {
      const element = period.periods[i];
      if (alreadySelected.length > 0) {
        for (let j = 0; j < alreadySelected.length; j++) {
          const element1 = alreadySelected[j];
          period.periods[i]['tue'] = tue;
          period.periods[i]['wed'] = wed;
          period.periods[i]['thu'] = thu;
          period.periods[i]['fri'] = fri;
          period.periods[i]['sat'] = sat;
          period.periods[i]['sun'] = sun;
          period.periods[i]['mon'] = mon;
          if (element1.period.uniqueId == element.uniqueId) {
            for (let k = 0; k < element1.shiftDays.length; k++) {
              const element3 = element1.shiftDays[k];
              switch (element3.dayValue) {
                case 1:
                  period.periods[i]['sun'] = { isSelected: true, isDisabled: false, workingdayIndex: 1, uniqueId: sun.uniqueId };
                  break;
                case 2:
                  period.periods[i]['mon'] = { isSelected: true, isDisabled: false, workingdayIndex: 2, uniqueId: mon.uniqueId };
                  break;
                case 3:
                  period.periods[i]['tue'] = { isSelected: true, isDisabled: false, workingdayIndex: 3, uniqueId: tue.uniqueId };
                  break;
                case 4:
                  period.periods[i]['wed'] = { isSelected: true, isDisabled: false, workingdayIndex: 4, uniqueId: wed.uniqueId };
                  break;
                case 5:
                  period.periods[i]['thu'] = { isSelected: true, isDisabled: false, workingdayIndex: 5, uniqueId: thu.uniqueId };
                  break;
                case 6:
                  period.periods[i]['fri'] = { isSelected: true, isDisabled: false, workingdayIndex: 6, uniqueId: fri.uniqueId };
                  break;
                case 7:
                  period.periods[i]['sat'] = { isSelected: true, isDisabled: false, workingdayIndex: 7, uniqueId: sat.uniqueId };
                  break;
              }
            }
            break;
          }
        }
      } else {
        period.periods[i]['tue'] = tue;
        period.periods[i]['wed'] = wed;
        period.periods[i]['thu'] = thu;
        period.periods[i]['fri'] = fri;
        period.periods[i]['sat'] = sat;
        period.periods[i]['sun'] = sun;
        period.periods[i]['mon'] = mon;
      }
    }
    this.selectedShiftData = period;
    this.shiftListWithPeriod[index] = period;
    this.shiftListWithPeriod[index].isAssignPeriodDays = true;
    this.selectedShiftPeriodData = period.periods;
    this.isPeriodListData = true;
  }

  // -----------------------------START get Data For Selection in multiselect---------------------
  getTeacherPeriod(userId: string) {

    this.spinner.show();
    this.slotListss = [];
    this._teacherService.getPeriodLists(userId).subscribe(
      response => {
        this.slotListss = response.shiftList;
        for (let index = 0; index < this.slotListss.length; index++) {
          let slotList = this.slotListss[index].periodList;
          if (slotList != null && slotList.length > 0) {
            for (let i = 0; i < slotList.length; i++) {
              let periodsDto = {
                "uniqueId": slotList[i].uniqueId,
                "name": slotList[i].name,
                "isLinked": false,
                "id": i,
                "period": slotList[i].period,
              }
              this.slotLists.push(periodsDto);
            }
          }
        }

        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      }
    );
  }
  // -----------------------------END get Data For Selection in multiselect---------------------
  getTeacherPeriodList(periodListArray: Array<any>) {
    let periodListArrayList = [];
    if (periodListArray != null && periodListArray.length > 0) {
      for (let i = 0; i < periodListArray.length; i++) {
        let period = {
          "id": periodListArray[i].id,
          "name": periodListArray[i].name + " (" + moment(periodListArray[i].startTime).format('h:mm a') + "-" + moment(periodListArray[i].endTime).format('h:mm a') + ")",
          "startTime": periodListArray[i].startTime,
          "endTime": periodListArray[i].endTime,
          "uniqueId": periodListArray[i].uniqueId,
          "isLinked": periodListArray[i].isLinked
        };
        periodListArrayList.push(period);
      }
    }
    return periodListArrayList;
  }
  getTeacherShiftWorkinDays(workingdaysArray: Array<any>) {

    let workingdayList = [];
    if (workingdaysArray != null && workingdaysArray.length > 0) {
      for (let i = 0; i < workingdaysArray.length; i++) {
        let workingdays = this.formBuilder.group({
          day: workingdaysArray[i].day,
          dayValue: workingdaysArray[i].dayValue,
          orgUid: workingdaysArray[i].orgUid,
          uniqueId: workingdaysArray[i].uniqueId,
          periodList: [this.getTeacherPeriodList(workingdaysArray[i].periodList)],
        });
        workingdayList.push(workingdays);
      }
    }
    return workingdayList;
  }
  submitTeacherPeriodInfo() {
    if (this.isPeriodListData) {
      this.spinner.show();
      let selectedData = [];
      for (let index = 0; index < this.shiftListWithPeriod.length; index++) {
        const element = this.shiftListWithPeriod[index];
        for (let i = 0; i < element.periods.length; i++) {
          const element2 = element.periods[i];
          let dto = {
            period: {
              uniqueId: element2.uniqueId
            }, shiftDays: []
          }
          if (element2.mon && element2.mon.isSelected) {
            dto.shiftDays.push({ uniqueId: element2.mon.uniqueId, dayValue: 2 })
          }
          if (element2.tue && element2.tue.isSelected) {
            dto.shiftDays.push({ uniqueId: element2.tue.uniqueId, dayValue: 3 })
          }
          if (element2.wed && element2.wed.isSelected) {
            dto.shiftDays.push({ uniqueId: element2.wed.uniqueId, dayValue: 4 })
          }
          if (element2.thu && element2.thu.isSelected) {
            dto.shiftDays.push({ uniqueId: element2.thu.uniqueId, dayValue: 5 })
          }
          if (element2.fri && element2.fri.isSelected) {
            dto.shiftDays.push({ uniqueId: element2.fri.uniqueId, dayValue: 6 })
            selectedData.push(dto);
          }
          if (element2.sat && element2.sat.isSelected) {
            dto.shiftDays.push({ uniqueId: element2.sat.uniqueId, dayValue: 7 })
          }
          if (element2.sun && element2.sun.isSelected) {
            dto.shiftDays.push({ uniqueId: element2.sun.uniqueId, dayValue: 1 })
          }
          if (dto.shiftDays.length > 0) {
            selectedData.push(dto);
          }
        }
      }
      if (selectedData.length > 0) {
        this._userService.assignPeriod(selectedData, this.userUniqueId)
          .subscribe(data => {
            if (data) {
              this.toastService.showSuccess("Period Information saved successfully.");
              // this.searchTeachers(false);
              setTimeout(() => {
                this._commonService.iscloseClicked.next(3)
              }, 800);
            }
            this.spinner.hide();
          }, (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            //   this.spinner.hide();
            // } else {
            //   this.toastService.ShowError(error);
            //   this.spinner.hide();
            // }
            this.spinner.hide();
          }
          );
      } else {
        this.toastService.ShowWarning("Please select period days.");
        this.spinner.hide();
      }
    }
  }

  selectPeriod(isDisabled, period1, j, k, dayindex, alreadySelected) {
    if (!isDisabled) {

      var element = this.selectedShiftData.periods[j];
      if (element.uniqueId == period1.uniqueId) {
        switch (dayindex) {
          case 1:
            element.mon = { isSelected: (alreadySelected) ? false : true, isDisabled: false, workingdayIndex: 2, uniqueId: element.mon.uniqueId }
            break;
          case 2:
            element.tue = { isSelected: (alreadySelected) ? false : true, isDisabled: false, workingdayIndex: 3, uniqueId: element.tue.uniqueId }
            break;
          case 3:
            element.wed = { isSelected: (alreadySelected) ? false : true, isDisabled: false, workingdayIndex: 4, uniqueId: element.wed.uniqueId }
            break;
          case 4:
            element.thu = { isSelected: (alreadySelected) ? false : true, isDisabled: false, workingdayIndex: 5, uniqueId: element.thu.uniqueId }
            break;
          case 5:
            element.fri = { isSelected: (alreadySelected) ? false : true, isDisabled: false, workingdayIndex: 6, uniqueId: element.fri.uniqueId }
            break;
          case 6:
            element.sat = { isSelected: (alreadySelected) ? false : true, isDisabled: false, workingdayIndex: 7, uniqueId: element.sat.uniqueId };
            break;
          case 7:
            element.sun = { isSelected: (alreadySelected) ? false : true, isDisabled: false, workingdayIndex: 1, uniqueId: element.sun.uniqueId };
            break;
        }
      }
    }
  }
  close() {
    this._commonService.iscloseClicked.next(3)
  }
  onSelectTab(event, i) {
    this.spinner.show();
    let shiftdata = this.shiftList[i];
    let index = this.shiftIdList.findIndex(x => x == shiftdata.uniqueId);
    if (index != -1) {
      if (this.currentIndex >= 0) {
        this.shiftListWithPeriod[this.currentIndex] = this.selectedShiftData;
      }
      let period = this.shiftListWithPeriod[index];
      this.currentIndex = index;
      if (!period.isAssignPeriodDays) {
        this._userService.getAssignedPeriod(this.userUniqueId, period.uniqueId).subscribe(
          res => {
            if (res.teacherPeriods && res.teacherPeriods.length > 0) {
              this.assignPeriod(period, shiftdata, index, res.teacherPeriods);

            } else {
              this.assignPeriod(period, shiftdata, index, []);
            }
          },
          (error: HttpErrorResponse) => {

            this.assignPeriod(period, shiftdata, index, []);
          });
      } else {
        this.selectedShiftData = period;
        this.isPeriodListData = (period.periods.length > 0) ? true : false;
      }

      this.spinner.hide();
    } else {
      this.isPeriodListData = false;
      this.currentIndex = index;
      this.spinner.hide();
    }
  }
}
