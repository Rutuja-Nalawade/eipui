import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ExamSetterServiceService } from '../exam-setter-service.service';
import { ExamService } from '../exam.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-add-tracks',
    templateUrl: './add-tracks.component.html',
    styleUrls: ['./add-tracks.component.scss']
})
export class AddTracksComponent implements OnInit {
    isOptional: boolean = false;
    userData: any
    parentIndex: number;
    childIndex: number = 0;
    previousMarks: number;
    examUid: string;
    imageSrc: string;
    exam: any;
    examTrackForm: FormGroup;
    markingSchemeForm: FormGroup;
    instructionForm: FormGroup;
    teachers = [];
    spinner: boolean = false;
    rightMarks: number;
    isFromEvent: boolean = false;
    customObjectiveExamQuestionType=[]
    jeeAdvanceSubjectiveQuestionType=[]

    minDate = new Date(new Date().setDate(new Date().getDate() - 1));
    // maxDate: Date;

    questionTypes = [
        { "id": 1, "name": "SCQ" },
        { "id": 2, "name": "MCQ" },
        { "id": 3, "name": "Numeric" },
        { "id": 4, "name": "SA" },
        { "id": 5, "name": "VSA" },
        { "id": 6, "name": "FIB" },
        { "id": 7, "name": "TF" }
    ];
    editorConfig = {
        "editable": true,
        "spellcheck": true,
        "height": "auto",
        "minHeight": "100%;",
        "width": "auto",
        "minWidth": "0",
        "translate": "yes",
        "enableToolbar": true,
        "showToolbar": true,
        "placeholder": "Enter text here...",
        "imageEndPoint": "",
        "toolbar": [
            ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
            ["fontName", "fontSize", "color"],
        ]
    };
    @Input() examId: any;

    constructor(private route: ActivatedRoute,private spinner1: NgxSpinnerService, private router: Router, private fb: FormBuilder, private modalService: NgbModal, private toastService: ToastsupportService, private examSetterService: ExamSetterServiceService, private examService: ExamService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.examUid = params['examId'];
            if (this.examUid != null) {
                this.getExam(this.examUid);
                this.getTeacherList();
            }
        });
        this.initExamTrackForm();
        this.initMarkingSchemeForm();
        this.initInstructionForm();
        this.getsubjectiveQuestionTypeList();
        this.getObjectiveQuestionTypeList();

        this.examSetterService.rightMarks.subscribe(
            (value: any) => {
                this.rightMarks = value;
                this.isFromEvent = true;
                this.calculateMarks(this.parentIndex, this.childIndex);
            });
        if (this.examId != "") {
            // this.examUid = this.examId;
            // this.getExam(this.examId);
            // this.getTeacherList();
            // this.initExamTrackForm();
        }
    }
    ngOnChanges(changes: SimpleChanges) {
        if (changes['examId'] != null) {
            // this.examUid = this.examId;
            // this.getExam(this.examId);
            // this.initExamTrackForm();
            // this.getTeacherList();
        }
    }

    getObjectiveQuestionTypeList(){
        this.spinner1.show();
        this.examService.getObjectiveQuestionTypeList().subscribe(
          res => {
            if (res) {
              this.spinner1.hide();
              console.log("Objective",res);
              this.customObjectiveExamQuestionType = res;
            }
          },
          (error: HttpErrorResponse) => {
            // if (error.error instanceof Error) {
            //   console.log("Client-side error occured.");this.spinner1.hide();
            // } else {
            //   this.spinner1.hide();
            //   this.toastService.ShowWarning(error.error.errorMessage);
            // }
            this.spinner1.hide();
          });
      }
    
      getsubjectiveQuestionTypeList(){
        this.spinner1.show();
        this.examService.getsubjectiveQuestionTypeList().subscribe(
          res => {
            if (res) {
              this.spinner1.hide();
              console.log("Subjective",res);
              this.jeeAdvanceSubjectiveQuestionType = res;
            }
          },
          (error: HttpErrorResponse) => {
            // if (error.error instanceof Error) {
            //   console.log("Client-side error occured.");
            //   this.spinner1.hide();
            // } else {
            //   this.spinner1.hide();
            //   this.toastService.ShowWarning(error.error.errorMessage);
            // }
            this.spinner1.hide();
          });
      }
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    calculateMarks(parentIndex: number, currentIndex: number) {
        console.log("caleed calculated")
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];

        subjectGroup.controls['totalMarks'].setValue(0);
        let trackMarks: number = 0;
        for (let i = 0; i < trackArray.length; i++) {
            
            let totalQuestion = 0;
            let trackFormGroup = <FormGroup>trackArray.controls[i];
            if (trackFormGroup.controls['noOfQuestions'].value == 0) {
                trackFormGroup.controls['noOfQuestions'].setValue('');
                return false;
            }
            if (trackFormGroup.controls['rightMarks'].value == 0) {
                trackFormGroup.controls['rightMarks'].setValue('');
                return false;
            }
            if (this.isFromEvent && currentIndex == i) {
                console.log("true")
                trackFormGroup.controls['rightMarks'].setValue(this.rightMarks);
            }
            totalQuestion = trackFormGroup.controls['noOfQuestions'].value - trackFormGroup.controls['optionalQuestions'].value;
     
            trackMarks = trackMarks + totalQuestion * trackFormGroup.controls['rightMarks'].value;
        }
        subjectGroup.controls['totalMarks'].setValue(trackMarks);
        let totalMarks: number = 0;
        for (let i = 0; i < subjectArray.length; i++) {
            
            let subjectFormGroup = <FormGroup>subjectArray.controls[i];
            totalMarks = totalMarks + subjectFormGroup.controls['totalMarks'].value;
        }
        this.examTrackForm.controls['totalMarks'].setValue(totalMarks);
        console.log('totalMarks',totalMarks)
        if (totalMarks > parseInt(this.exam.marks)) {
            this.toastService.ShowWarning("Exam marks should not be greater than " + this.exam.marks);
        }
        this.isFromEvent = false;


    }
    getMarkingScheme(scrollDataModal, parentIndex: number, childIndex: number) {
        this.parentIndex = parentIndex;
        this.childIndex = childIndex;
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[childIndex];
        let markingSchemeArray = <FormArray>trackGroup.controls['markingScheme'];

        this.modalService.open(scrollDataModal);

        if (markingSchemeArray != null && markingSchemeArray.length > 0) {
            let markingScheme = <FormGroup>markingSchemeArray.controls[0];

            this.markingSchemeForm.patchValue({
                schemeType: markingScheme.controls['schemeType'].value,
                optionCount: markingScheme.controls['optionCount'].value,
                name: markingScheme.controls['name'].value,
                uniqueId: markingScheme.controls['uniqueId'].value,
                highestMarks: markingScheme.controls['highestMarks'].value,
                leaveMarks: markingScheme.controls['leaveMarks'].value,
                defaultMarks: markingScheme.controls['defaultMarks'].value,
                subMarkingScheme: this.fb.array(this.patchSubMarkingScheme(this.markingSchemeForm.controls['subMarkingScheme'].value))
            });
            //  this.markingSchemeForm.setControl('subMarkingScheme', this.fb.array(this.patchSubMarkingScheme(markingScheme.controls['subMarkingScheme'].value)));
        } else {
            this.initMarkingSchemeForm();
        }
    }
    initMarkingSchemeForm() {
        this.markingSchemeForm = this.fb.group({
            schemeType: ['', Validators.required],
            optionCount: [1, Validators.required],
            name: ['', Validators.required],
            uniqueId: ['', Validators.nullValidator],
            highestMarks: ['', Validators.required],
            leaveMarks: [0, Validators.required],
            defaultMarks: ['', Validators.required],
            organizationUid: ['', Validators.nullValidator],
            orgId: ['', Validators.nullValidator],
            createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.nullValidator],
            subMarkingScheme: this.fb.array([this.initSubMarkingScheme()])
        });
    }

    initSubMarkingScheme() {
        return this.fb.group({
            correctOption: [0, Validators.required],
            rightOption: [0, Validators.required],
            wrongOption: [0, Validators.required],
            marks: [0, Validators.required],
        });
    }
    patchSubMarkingScheme(subMarkingScheme: Array<any>) {
        let subArray = [];
        if (subMarkingScheme != null && subMarkingScheme.length > 0) {
            for (let j = 0; j < subMarkingScheme.length; j++) {
                subArray.push(this.fb.group({
                    correctOption: subMarkingScheme[j].correctOption,
                    rightOption: subMarkingScheme[j].rightOption,
                    wrongOption: subMarkingScheme[j].wrongOption,
                    marks: subMarkingScheme[j].marks,
                }));
            }
        }
        return subArray;
    }
    getInstruction(parentIndex: number, childIndex: number, content: string) {

        this.parentIndex = parentIndex;
        this.childIndex = childIndex;
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[childIndex];
        let instructionArray = <FormArray>trackGroup.controls['instructionDto'];

        this.modalService.open(content);

        if (instructionArray != null && instructionArray.length > 0) {
            let instruction = <FormGroup>instructionArray.controls[0];
            this.instructionForm.controls['instructionUid'].setValue(instruction.controls['instructionUid'].value);
            this.instructionForm.controls['instructionType'].setValue(instruction.controls['instructionType'].value);
            this.instructionForm.controls['instructionTextOrImage'].setValue(instruction.controls['instructionTextOrImage'].value);
        } else {
            this.initInstructionForm();
        }

    }
    initInstructionForm() {
        this.instructionForm = this.fb.group({
            instructionUid: ['', Validators.nullValidator],
            instructionType: ['', Validators.required],
            instructionTextOrImage: ['', Validators.required],
        });
    }
    addFormControl(subjectIndex: number, trackIndex: number) {
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[subjectIndex];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[trackIndex];
        if ((trackGroup.controls['noOfQuestions'].value == '' && trackGroup.controls['noOfQuestions'].value == 0) || (trackGroup.controls['rightMarks'].value == '' && trackGroup.controls['rightMarks'].value == 0)) {
            this.toastService.ShowWarning("Please fill all track values.");
            return false;
        }
        trackArray.push(this.initTrackList(1));
    }
    initTrackList(Qtype:number) {
        return this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            teacherId: [null, Validators.required],
            noOfQuestions: ['', Validators.required],
            optionalQuestions: [0, Validators.required],
            questionType: [Qtype, Validators.nullValidator],
            endDate: [new Date(), Validators.required],
            rightMarks: [1, Validators.required],
            wrongMarks: [0, Validators.nullValidator],
            leaveMarks: [0, Validators.nullValidator],
            instructionDto: this.fb.array([]),
            markingScheme: this.fb.array([]),
            defaultWrongMarks: [0, Validators.nullValidator]
        });
    }
    removeFormControl(parentIndex: number, childIndex: number) {
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[parentIndex];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[childIndex];
        let questions: number = trackGroup.controls['noOfQuestions'].value;
        let marks: number = trackGroup.controls['rightMarks'].value;
        subjectGroup.controls['totalMarks'].setValue(subjectGroup.controls['totalMarks'].value - (questions * marks));
        trackArray.removeAt(childIndex);
    }
    checkQuestionCount(index1: number, index2: number, index3: number) {
        let patternDtoGroup = <FormGroup>this.examTrackForm.controls['patternDto'];
        let subjectArray = <FormArray>patternDtoGroup.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let sectionArray = <FormArray>subjectGroup.controls['sections'];
        let sectionGroup = <FormGroup>sectionArray.controls[index2];
        let trackArray = <FormArray>sectionGroup.controls['trackList'];
        let totalQuestionCount: number = sectionGroup.controls['noOfQuestions'].value;
        let sum: number = 0;

        for (let i = 0; i < trackArray.length; i++) {
            let formGroup = <FormGroup>trackArray.controls[i];
            let questionCount = formGroup.controls['noOfQuestions'].value;
            if (questionCount == 0) {
                this.toastService.ShowWarning("Question count must be greater than zero.");
                formGroup.controls['noOfQuestions'].setValue('');
                return false;
            }
            sum = sum + parseInt(questionCount);
        }
        let formGroup = <FormGroup>trackArray.controls[index3];
        if (sum > totalQuestionCount) {
            let lastIndexValue: number = formGroup.controls['noOfQuestions'].value;
            let diffNumber: number = sum - lastIndexValue;
            let actualNumber: number = totalQuestionCount - diffNumber;
            sum = diffNumber + actualNumber;
            this.toastService.ShowWarning("You can add only " + actualNumber + " questions");
            formGroup.controls['noOfQuestions'].setValue(actualNumber);
        }
        sectionGroup.controls['addedQuestions'].setValue(sum);
        let subjectQuestionCount: number = 0;
        for (let i = 0; i < sectionArray.length; i++) {
            let sectionFormGroup = <FormGroup>sectionArray.controls[i];
            subjectQuestionCount = subjectQuestionCount + parseInt(sectionFormGroup.controls['addedQuestions'].value);
        }
        subjectGroup.controls['addedQuestions'].setValue(subjectQuestionCount);
        let patternQuestionCount: number = 0;
        for (let i = 0; i < subjectArray.length; i++) {
            let subjectFormGroup = <FormGroup>subjectArray.controls[i];
            patternQuestionCount = patternQuestionCount + parseInt(subjectFormGroup.controls['addedQuestions'].value);
        }
        patternDtoGroup.controls['addedQuestions'].setValue(patternQuestionCount);

    }
    addPatternControl(index1: number, index2: number, index3: number) {
        console.log("addPaternControl");

        let patternDtoGroup = <FormGroup>this.examTrackForm.controls['patternDto'];
        let subjectArray = <FormArray>patternDtoGroup.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let sectionArray = <FormArray>subjectGroup.controls['sections'];
        let sectionGroup = <FormGroup>sectionArray.controls[index2];
        let trackArray = <FormArray>sectionGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[index3];
        if (trackGroup.controls['noOfQuestions'].value == '') {
            console.log("addPaternControl noOfQuestions");
            this.toastService.ShowWarning("Please fill all track values.");
            return false;
        }
        trackArray.push(this.initTrackList(sectionGroup.controls['questionType'].value));
    }
    removePatternControl(index1: number, index2: number, index3: number) {
        let patternDtoGroup = <FormGroup>this.examTrackForm.controls['patternDto'];
        let subjectArray = <FormArray>patternDtoGroup.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let sectionArray = <FormArray>subjectGroup.controls['sections'];
        let sectionGroup = <FormGroup>sectionArray.controls[index2];
        let trackArray = <FormArray>sectionGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[index3];
        let value: number = trackGroup.controls['noOfQuestions'].value;
        sectionGroup.controls['addedQuestions'].setValue(sectionGroup.controls['addedQuestions'].value - value);
        subjectGroup.controls['addedQuestions'].setValue(subjectGroup.controls['addedQuestions'].value - value);
        patternDtoGroup.controls['addedQuestions'].setValue(patternDtoGroup.controls['addedQuestions'].value - value);
        trackArray.removeAt(index3);
    }
    onSelectFile(e: any) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        let max_size: number = 2085184;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            this.toastService.ShowWarning("invalid format");
            return;
        }
        console.log("imageSrc", this.imageSrc);
        if (e.target.files[0].size > max_size) {
            this.toastService.ShowWarning("Maximum size of 2 MB is allowed..");
            this.instructionForm.controls['instructionTextOrImage'].setValue('');
            return false;
        } else {
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsDataURL(file);
        }
    }
    _handleReaderLoaded(e: any) {
        this.imageSrc = e.target.result;
        console.log("imageSrc", this.imageSrc);

        this.instructionForm.controls['instructionTextOrImage'].setValue(this.imageSrc);

    }
    saveInstruction() {
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[this.childIndex];
        let instructionArray = <FormArray>trackGroup.controls['instructionDto'];

        for (let i = instructionArray.length - 1; i >= 0; i--) {
            instructionArray.removeAt(i);
        }

        instructionArray.push(this.fb.group({
            instructionUid: this.instructionForm.controls['instructionUid'].value,
            instructionType: this.instructionForm.controls['instructionType'].value,
            instructionTextOrImage: this.instructionForm.controls['instructionTextOrImage'].value
        }));
    }
    getInstructionType() {
        this.instructionForm.controls['instructionTextOrImage'].setValue('');
    }
    saveScheme() {

        console.log("called")
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[this.parentIndex];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[this.childIndex];
        let markingSchemeArray = <FormArray>trackGroup.controls['markingScheme'];

        for (let i = markingSchemeArray.length - 1; i >= 0; i--) {
            markingSchemeArray.removeAt(i);
        }

        markingSchemeArray.push(this.fb.group({
            schemeType: this.markingSchemeForm.controls['schemeType'].value,
            optionCount: this.markingSchemeForm.controls['optionCount'].value,
            name: this.markingSchemeForm.controls['name'].value,
            uniqueId: this.markingSchemeForm.controls['uniqueId'].value,
            highestMarks: this.markingSchemeForm.controls['highestMarks'].value,
            leaveMarks: this.markingSchemeForm.controls['leaveMarks'].value,
            defaultMarks: this.markingSchemeForm.controls['defaultMarks'].value,
            subMarkingScheme: this.fb.array(this.patchSubMarkingScheme(this.markingSchemeForm.controls['subMarkingScheme'].value))
        }));

        this.rightMarks = this.markingSchemeForm.controls['highestMarks'].value;
        this.isFromEvent = true;
        this.calculateMarks(this.parentIndex,this.childIndex)
    }
    saveAndImportScheme() {
        // this.examPatternService.spinner.next(true);
        this.markingSchemeForm.controls['organizationUid'].setValue(sessionStorage.getItem('orgUniqueId'));
        this.markingSchemeForm.controls['orgId'].setValue(sessionStorage.getItem('organizationId'));
        return this.examSetterService.saveMarkingScheme(this.markingSchemeForm.value).subscribe(
            res => {
                if (res) {
                    this.markingSchemeForm.controls['schemeType'].setValue(1),
                        this.markingSchemeForm.controls['uniqueId'].setValue(res.uniqueId),
                        this.saveScheme();
                    this.modalService.dismissAll();
                    // this.examPatternService.spinner.next(false);
                }
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     // this.examPatternService.spinner.next(false);
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }
    submit(formValue: any) {

        if (this.exam.examType == 1 && formValue.totalMarks != this.exam.marks) {
            this.toastService.ShowWarning("Total track marks must be same as exam marks..");
            return false;
        }
        if (this.exam.examType == 2 && formValue.patternDto.actualQuestions != formValue.patternDto.addedQuestions) {
            this.toastService.ShowWarning("Please complete all subject questions..");
            return false;
        }
        this.spinner = true;
        this.examSetterService.addExamTrack(formValue).subscribe(
            res => {
                this.spinner = false;
                this.toastService.showSuccess("Tracks added successfully..");
                this.router.navigate(['/newModule/exam/track/info', this.examUid]);
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.spinner = false;
                //     this.toastService.ShowError(error.error.errorMessage);
                // }
                this.spinner = false;
            });
    }
    getTeacherList() {
        this.teachers = [];
        let serachDto = {
            "orgList": [(sessionStorage.getItem('orgUniqueId'))],
            "roleList": [6, 7],
            "isForAll": false
        };
        return this.examService.getSearchedByUserData(serachDto).subscribe(
            response => {
                this.teachers = response;
            },
            (error: HttpErrorResponse) => {
                // if (error instanceof HttpErrorResponse) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowError(error)
                // }
            });
    }
    getExam(examUid: string) {
        this.spinner = true;
        this.examSetterService.getExam(examUid).subscribe(
            res => {
                this.exam = res;
                // this.maxDate = new Date(new Date().setDate(new Date(res.examDate).getDate() - 1));
                this.examTrackForm.controls['examAssignmentType'].setValue(res.examType);
                if (res.examType == 1) {
                    this.pushSubjects(res.subjects);
                } else {
                    this.examTrackForm.controls['patternType'].setValue(res.patternType);
                    let patternDtoGroup = <FormGroup>this.examTrackForm.controls['patternDto'];
                    patternDtoGroup.controls['patternId'].setValue(res.pattern1Uid);
                    patternDtoGroup.controls['patternName'].setValue(res.pattern1Name);
                    patternDtoGroup.controls['actualQuestions'].setValue(res.totalQuestions);
                    patternDtoGroup.setControl('subjects', this.fb.array(this.initSecSubject(res.subjects)));
                }
                this.spinner = false;
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.spinner = false;
                //     this.toastService.ShowError(error.error.errorMessage);
                // }
                this.spinner = false;
            });
    }
    pushSubjects(subjects: any) {
        const controlArray = <FormArray>this.examTrackForm.controls['subjects'];
        controlArray.controls = [];
        if (subjects != null && subjects.length > 0) {
            for (let i = 0; i < subjects.length; i++) {
                controlArray.push(this.initSubjectsWithValue(subjects[i]));
            }
        }
    }
    initSubjectsWithValue(subject: any) {
        return this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            subjectId: [subject.id, Validators.nullValidator],
            name: [subject.name, Validators.nullValidator],
            totalMarks: [0, Validators.nullValidator],
            trackList: this.fb.array([this.initTrackList(1)])
        });
    }
    initSecSubject(subjects: any) {
        let sectionSubject = [];
        if (subjects != null && subjects.length > 0) {
            for (let i = 0; i < subjects.length; i++) {
                sectionSubject.push(this.fb.group({
                    uniqueId: ['', Validators.nullValidator],
                    subjectId: [subjects[i].subjectId, Validators.nullValidator],
                    examAssignmentSubjectId: [subjects[i].examAssignmentSubjectId, Validators.nullValidator],
                    examAssignmentPatternSubjectId: [subjects[i].examAssignmentPatternSubjectId, Validators.nullValidator],
                    name: [subjects[i].name, Validators.nullValidator],
                    totalQuestions: [subjects[i].totalQuestions, Validators.nullValidator],
                    addedQuestions: [0, Validators.nullValidator],
                    sections: this.fb.array(this.initSections(subjects[i].sections))
                }));
            }
        }
        return sectionSubject;
    }
    initSections(sections: any) {
        let sectionArray = [];
        if (sections != null && sections.length > 0) {
            for (let i = 0; i < sections.length; i++) {
                sectionArray.push(this.fb.group({
                    id: [sections[i].id, Validators.nullValidator],
                    name: [sections[i].name, Validators.nullValidator],
                    noOfQuestions: [sections[i].noOfQuestions, Validators.nullValidator],
                    questionType: [sections[i].questionType, Validators.nullValidator],
                    addedQuestions: [0, Validators.nullValidator],
                    trackList: this.fb.array([this.initTrackList(sections[i].questionType)])
                }));
            }
            return sectionArray;
        }
    }
    initExamTrackForm() {
        this.examTrackForm = this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            examUid: [this.examUid, Validators.required],
            totalMarks: [0, Validators.nullValidator],
            examAssignmentType: ['', Validators.required],
            patternType: ['', Validators.nullValidator],
            subjects: this.fb.array([], Validators.nullValidator),
            patternDto: this.examPattern(),
            createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.required],
            organizationId: [sessionStorage.getItem('organizationId'), Validators.nullValidator],
        });
    }

    examPattern() {
        return this.fb.group({
            uniqueId: ['', Validators.nullValidator],
            patternId: ['', Validators.nullValidator],
            patternName: ['', Validators.nullValidator],
            actualQuestions: ['', Validators.nullValidator],
            addedQuestions: [0, Validators.required],
            subjects: this.fb.array([], Validators.nullValidator),
        });
    }
    isCollaps(id, headId) {

        var ele = document.getElementById(id);

        if (ele.classList.contains('isUnhide')) {
            ele.classList.remove("isUnhide");
            ele.classList.add("isHide");

        } else if (ele.classList.contains('isHide')) {
            ele.classList.remove("isHide");
            ele.classList.add("isUnhide");

        }

        var element = document.getElementById(headId);
        if (element.classList.contains('fa-minus')) {
            element.classList.remove("fa-minus");
            element.classList.add("fa-plus");

        } else if (element.classList.contains('fa-plus')) {
            element.classList.remove("fa-plus");
            element.classList.add("fa-minus");

        }

    }
    onOptionalCheck2(id, id2, index1, index2) {
        console.log("id", id)
        let subjectArray = <FormArray>this.examTrackForm.controls['subjects'];
        let subjectGroup = <FormGroup>subjectArray.controls[index1];
        let trackArray = <FormArray>subjectGroup.controls['trackList'];
        let trackGroup = <FormGroup>trackArray.controls[index2];
        // trackGroup.controls['noOfQuestions'].setValue(0);
        trackGroup.controls['optionalQuestions'].setValue(0);
        var ele = document.getElementById(id);
        console.log("ele", ele)
        if (ele.classList.contains('col-12')) {
            ele.classList.remove("col-12");
            ele.classList.add("col-6");
            ele.style.paddingRight = '0'

        } else if (ele.classList.contains('col-6')) {
            ele.classList.remove("col-6");
            ele.classList.add("col-12");
            ele.style.paddingRight = '12px'
        }

        var element = document.getElementById(id2);
        if (element.classList.contains('isHideOptional')) {
            element.classList.remove("isHideOptional");
            element.classList.add("isUnhideOptional");
            element.classList.add("col-6");

        } else if (element.classList.contains('col-6')) {
            element.classList.remove("col-6");
            element.classList.remove("isUnhideOptional");
            element.classList.add("isHideOptional");
        }
    }

    onOptionalCheck1() {
        if (this.isOptional == false) {
            this.isOptional = true;
        } else {
            this.isOptional = false;
        }
    }
    instructiionAvailable = true;

    files: any = [];
    noRadioSelected = true;
    uploadFile(event) {

        var file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
        let max_size: number = 2085184;
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            this.toastService.ShowWarning("invalid format");
            return;
        }
        if (event.target.files[0].size > max_size) {
            this.toastService.ShowWarning("Maximum size of 2 MB is allowed..");
            this.instructionForm.controls['instructionTextOrImage'].setValue('');
            return false;
        } else {
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsDataURL(file);
        }
        var files = event.target.files
        for (let index = 0; index < files.length; index++) {
            const element = files[index];
            this.files.push(element.name)
            console.log("event", files);
        }
    }
    deleteAttachment(index) {
        this.files.splice(index, 1)
        if (index == 0 && this.files.length == 0) {
            this.instructionForm.controls['instructionTextOrImage'].setValue('');
        }
    }
    changeQuestionType() {
        console.log("examTrackForm", this.examTrackForm);

    }
}
