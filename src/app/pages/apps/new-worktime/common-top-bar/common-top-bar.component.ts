import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-common-top-bar',
  templateUrl: './common-top-bar.component.html',
  styleUrls: ['./common-top-bar.component.scss']
})
export class CommonTopBarComponent implements OnInit {

  tabNumber = 1;
  isShowfilter = false;
  submitted = false;
  shiftTypeList = [];
  selectedShiftTypes = [];
  selectedTeachers = [];
  selectedDays = [];
  workingDayList = [];
  userData: any = {};
  pageIndex = 0;
  teacherList = [];
  filterData = {};
  isAddNew = false;
  isViewShift = false;
  isBulkAssign = false;
  constructor(private spinner: NgxSpinnerService, private _commonService: NewCommonService, private toastService: ToastsupportService, private _shiftService: ShiftServiceService, private _userService: UserServiceService, private router: Router, private route: ActivatedRoute) {
    this.userData = this._commonService.getUserData();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params) {
        switch (params['id']) {
          case 'shift':
            this.tabNumber = 1;
            break;
          case 'period':
            this.tabNumber = 2;
            break;
        }
      }
    });
  }
  tabClick(index) {
    this.tabNumber = index;
    switch (index) {
      case 1:
        this.router.navigate(['newModule/workTime/lists/shift']);
        break;
      case 2:
        this.router.navigate(['newModule/workTime/lists/period']);
        break;
    }
  }
  addShift() {
    this.isAddNew = true;
  }
  findRecords(value) {

  }
  openFilter() {
    this.isShowfilter = true;
    this.getOrgWorkDays();
    this.getshiftTypeList();
    // this.getTeachers(false);
  }
  OpenBulkAssignForm() {
    this.isBulkAssign = true;
  }
  closeFilter() {
    this.isShowfilter = false;
  }
  onChange(type) {
    switch (type) {
      case 1:
        this.selectedShiftTypes = [];
        this.selectedShiftTypes.push(type.id);
        break;
      case 2:
        this.selectedDays = [];
        this.selectedDays.push(type.dayValue);
        break;
      case 3:
        this.selectedTeachers = [];
        this.selectedTeachers.push(type.id);
        break;
    }
  }
  getOrgWorkDays() {
    this.spinner.show();
    this._shiftService.getShiftWorkingDays().subscribe(
      res => {
        if (res) {
          this.workingDayList = res;
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        //   //this.sessionService.checkUnauthorizedRequest(error.error);
        // }
        this.spinner.hide();
      });
  }
  getTeachers(value) {
    this.pageIndex = 0;
    this._userService.getUserByPageIndexAndType(this.pageIndex, value.id, 100).subscribe(
      response => {
        // console.log('user list =>', JSON.stringify(response))
        if (response.message == "Success") {
          this.teacherList = response.users
        } else {
          this.toastService.ShowInfo("Users not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  applyFilter() {
    if (this.selectedShiftTypes.length == 0 && this.selectedDays.length == 0 && this.selectedTeachers.length == 0) {
      this.toastService.ShowWarning("Please select atleast one option!")
    }
  }
  closeAddNewForm(event) {
    this.isAddNew = false;
  }
  isViewDetails(event) {
    if (event == "true") {
      this.isViewShift = true;
    } else {
      this.isViewShift = false;
    }
  }
  closeBulkAssign(event) {
    this.isBulkAssign = false;
  }
  getshiftTypeList() {
    return this._shiftService.getAllShiftTypes().subscribe(
      res => {
        if (res) {
          res.forEach(element => {
            let dto = { id: element.key, name: element.value }
          });
          this.shiftTypeList = res;
          // console.log('shift type => ' + JSON.stringify(res))
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        // }
      });
  }

}
