import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicBatchesComponent } from './academic-batches.component';

describe('AcademicBatchesComponent', () => {
  let component: AcademicBatchesComponent;
  let fixture: ComponentFixture<AcademicBatchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcademicBatchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicBatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
