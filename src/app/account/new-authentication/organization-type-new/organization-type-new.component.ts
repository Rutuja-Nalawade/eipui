import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';
import { CookieService } from 'src/app/servicefiles/cookie.service';

@Component({
  selector: 'app-organization-type-new',
  templateUrl: './organization-type-new.component.html',
  styleUrls: ['./organization-type-new.component.scss']
})
export class OrganizationTypeNewComponent implements OnInit {
  basicRoleSelected: boolean = false;
  moreRoleSelected: boolean = false;
  instituteName: string = '';
  basicRole: any;
  moreRole: any;

  orgnizationTypeForm: FormGroup;
  organizationNameForm: FormGroup;
  submitted = false;
  nameSubmitted = false;
  error: any;
  loading = false;
  showModalBox: boolean = false;
  organizationType = "1";
  orgType = "institute"
  orgTypeImgUrl = "assets/images/auth/instituteImage.png";
  imgSrcType1 = "assets/images/new-auth/Institute.png";
  imgSrcType2 = "assets/images/new-auth/College.png";
  imgSrcType3 = "assets/images/new-auth/School.png";
  subscriptionDto: any;
  success = ""
  showOrganizationNameForm: Boolean = false;
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private modalService: NgbModal, private _authService: AuthServiceService, private cookieService: CookieService, private toastService: ToastsupportService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state;
    console.log("state", state);

    if (!state) {
      this.router.navigate(['/account/signup']);
    } else {
      //this.subscriptionDto=state;
    }
    //   this.subscriptionDto = {
    //     "firstName": "Ranjit",
    //     "lastName": "Barve",
    //     "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIzMDY1IiwiYXV0aCI6WyIyIl0sIm9yZ0lkIjoiMzQiLCJpYXQiOjE2MjM5Mjg4OTUsImV4cCI6MTYyMzk1ODg5NX0.Hq5iFph7h8XpzD7R5nleaAXc7CJeNHTqeTeBaBG-R3k",
    //     "orgCode": 3714206,
    //     "basicRoles": [
    //         {
    //             "uniqueId": "32d5d3c5-bd4c-4e20-a0ed-b5940e309fb9",
    //             "name": "Teacher"
    //         },
    //         {
    //             "uniqueId": "c32887a5-8b0e-4ab6-b7d0-c4da34e0bf77",
    //             "name": "Staff"
    //         }
    //     ],
    //     "moreRoles": [
    //         {
    //             "uniqueId": "820a4a52-c6af-489d-93f2-d52a3205df95",
    //             "name": "Director"
    //         },
    //         {
    //             "uniqueId": "1841ac0b-b20e-473b-a1a6-bccf5db96571",
    //             "name": "Center Head"
    //         }
    //     ]
    // }

    console.log(this.subscriptionDto)
  }

  ngOnInit() {
    this.getRoles();
    this.orgnizationTypeForm = this.formBuilder.group({
      type: ['', Validators.required]
    });
    // if (this.showModalBox) {
    this.organizationNameForm = this.formBuilder.group({
      organizationName: ['', [Validators.required]]
    });
    // }

  }
  get f() { return this.orgnizationTypeForm.controls; }
  get fa() { return this.organizationNameForm.controls; }
  /**
   * On submit form
   */
  onSubmit(responsiveData: string) {
    this.submitted = true;
    console.log("submited");
    this.error = ''
    // stop here if form is invalid
    if (this.orgnizationTypeForm.invalid) {
      console.log(this.orgnizationTypeForm);

      return;
    }

    this.loading = true;

    console.log(this.orgnizationTypeForm.value);
    setTimeout(() => {
      this.loading = false;
      // alert("Selected- "+ this.orgnizationTypeForm.value.type)
      this.organizationType = this.orgnizationTypeForm.value.type
      var type = "institute"
      if (this.organizationType == "1") {
        type = "institute"
        this.orgType = "institute"
      } else if (this.organizationType == "2") {
        type = "school";
        this.orgType = "school"
      } else {
        type = "college"
        this.orgType = "college"
      }
      this.orgTypeImgUrl = "assets/images/auth/" + type + "Img.png";
      // jQuery("#myModal").modal();
      // this.showModalBox = true;
      this.modalService.open(responsiveData, { size: 'lg', windowClass: 'modal-full' });

      // this.router.navigate(['/account/confirm']);
    }, 1000);
  }

  onNameSubmit() {
    this.nameSubmitted = true;
    console.log("submited");
    this.error = ''
    this.success = ''

    // stop here if form is invalid
    if (this.organizationNameForm.invalid) {
      console.log(this.organizationNameForm);

      return;
    }
    let currentUser = null;
    this.loading = true;
    let organizationName = this.organizationNameForm.controls['organizationName'].value;
    this.subscriptionDto.organizationType = this.organizationType;
    this.subscriptionDto.organizationName = organizationName;
    this._authService.organizationRegistration(this.subscriptionDto).subscribe(
      (res) => {
        if (res) {
          // this.success="Organization name added successfully!"
          currentUser = res.userDto;
          sessionStorage.setItem('tokenId', currentUser.tokenId);
          sessionStorage.setItem('currentUser', JSON.stringify(currentUser));
          sessionStorage.setItem('uniqueId', currentUser.uniqueId);
          sessionStorage.setItem('orgUniqueId', currentUser.orgUniqueId);//null
          sessionStorage.setItem('organizationId', currentUser.organizationId);
          sessionStorage.setItem('organizationName', res.organizationName);
          sessionStorage.setItem('parentOrgUniqueId', currentUser.parentOrgUniqueId);
          sessionStorage.setItem('parentOrgId', currentUser.parentOrgId);
          sessionStorage.setItem('isParent', currentUser.isParent);
          sessionStorage.setItem('orgType', currentUser.orgType);
          sessionStorage.setItem('isSetupDone', JSON.parse("false"));
          sessionStorage.setItem('step', "0");
          sessionStorage.setItem('organizationType', res.organizationType)
          sessionStorage.setItem('boardId', currentUser.boardId)
          sessionStorage.setItem('stateId', currentUser.stateId)
          sessionStorage.setItem('userId', currentUser.userId);
          sessionStorage.setItem('fullName', currentUser.fullName);
          if (currentUser && currentUser.tokenId) {

            // store user details and jwt in cookie
            this.cookieService.setCookie('currentUser', JSON.stringify(currentUser), 1);
          }
          let currentTime: number = Date.now();
          sessionStorage.setItem('lastLoginTime', currentTime.toString());
          setTimeout(() => {
            this.loading = false;
            // this.modalService.dismissAll();
            this.router.navigate(['/newModule/newSetup1/educationData/']);
          }, 1000);
          this.toastService.showSuccess("Organization name added successfully!");

        } else {
          // setTimeout(() => {
          //   this.router.navigate(['/account/mobile-verify/'],{state:this.signupForm.value});
          // }, 1000);
          // this.error="Organization name not added."
          this.toastService.ShowError("Organization name not added.");
          this.loading = false;
        }
      },
      (err: HttpErrorResponse) => {
        if (err instanceof HttpErrorResponse) {
          // console.log("Client-side error occured.");
          var error1 = err
          // this.toastService.ShowError(error1);
        } else {
          this.error = err
          //this.toastService.ShowError(err);
          //this.sessionService.checkUnauthorizedRequest(error.error);
        }
        this.loading = false;
      });
  }

  largeModal(largeDataModal: string) {
    console.log(largeDataModal);

    this.modalService.open(largeDataModal, { size: 'lg' });
  }
  mouseOver(index) {
    if (index == 1) {
      this.imgSrcType1 = "assets/images/new-auth/Institute-Hover.png";
    } else if (index == 2) {
      this.imgSrcType2 = "assets/images/new-auth/College-Hover.png";
    } else if (index == 3) {
      this.imgSrcType3 = "assets/images/new-auth/School-hover.png";
    }
  }
  mouseOut(index) {
    if (index == 1) {
      this.imgSrcType1 = "assets/images/new-auth/Institute.png";
    } else if (index == 2) {
      this.imgSrcType2 = "assets/images/new-auth/College.png";
    } else if (index == 3) {
      this.imgSrcType3 = "assets/images/new-auth/School.png";
    }
  }
  typeClick(index) {
    if (index == 1) {
      this.orgType = "institute"
    } else if (index == 2) {
      this.orgType = "college"
    } else if (index == 3) {
      this.orgType = "school"
    }
    this.showOrganizationNameForm = true;
    this.organizationNameForm.reset()
  }

  // selectFS(event, selected) {
  //   this.roleSelected = false;
  //   if (selected == 1) {
  //     var element = document.getElementById("staff");
  //     element.classList.remove("fs-selected");
  //     this.staffSelected = true;
  //     this.facultySelected = false;
  //   } else {
  //     var element = document.getElementById("faculty");
  //     element.classList.remove("fs-selected");
  //     this.facultySelected = true;
  //     this.staffSelected = false;
  //   }
  //   event.srcElement.classList.add("fs-selected");
  // }

  // selectRole(event, selected) {
  //   if (selected == 1) {
  //     var element = document.getElementById("head");
  //     element.classList.remove("fs-selected");
  //   } else {
  //     var element = document.getElementById("owner");
  //     element.classList.remove("fs-selected");
  //   }
  //   event.srcElement.classList.add("fs-selected");
  //   this.roleSelected = true;
  // }

  selectBasicRole(details) {
    this.basicRoleSelected = true;
    this.basicRole = details;
    let roles = this.subscriptionDto.basicRoles;
    for (let i = 0; i < roles.length; i++) {
      if (roles[i].name == details.name) {
        var element = document.getElementById(roles[i].name);
        element.classList.add("fs-selected");
      } else {
        var element = document.getElementById(roles[i].name);
        element.classList.remove("fs-selected");
      }
    }
    console.log(details)
  }

  selectMoreRole(details) {
    this.moreRoleSelected = true;
    this.moreRole = details;
    let roles = this.subscriptionDto.moreRoles;
    for (let i = 0; i < roles.length; i++) {
      if (roles[i].name == details.name) {
        var element = document.getElementById(roles[i].name);
        element.classList.add("fs-selected");
      } else {
        var element = document.getElementById(roles[i].name);
        element.classList.remove("fs-selected");
      }
    }
    console.log(details)
  }

  createOrganization() {
    console.log(this.basicRole);
    console.log(this.moreRole);
    console.log(this.instituteName);
    let orgData = {
      "orgName": this.instituteName,
      "roleUniqueIds": [
        this.basicRole.uniqueId,
        this.moreRole.uniqueId
      ]
    }
    this.loading = true;
    this._authService.createOrganization(orgData).subscribe(
      (res) => {
        if (res) {
          setTimeout(() => {
            this.router.navigate(['/account/setup/'], { state: res });
          }, 1000);

        }
        this.loading = false;
      },
      (err: HttpErrorResponse) => {
        if (err instanceof HttpErrorResponse) {
          // console.log("Client-side error occured.");
          this.error = err
        } else {
          this.error = err
          // this.toastService.ShowError(error);
          //this.sessionService.checkUnauthorizedRequest(error.error);
        }
        this.loading = false;
      });
  }

  getRoles() {
    this.loading = true;
    this._authService.getRoles().subscribe(data => {
      this.subscriptionDto = data;
      this.loading = false;
    }, (error: HttpErrorResponse) => {
      if (error instanceof HttpErrorResponse) {
       // console.log("Client side error occured");
      } else {
        //this.toastService.ShowError(error);
        this.loading = false;
      }
    });
  }
  copyCode() {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.subscriptionDto.orgCode;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
}
