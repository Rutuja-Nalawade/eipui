import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import {Color, SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
@Component({
  selector: 'app-studentrank',
  templateUrl: './studentrank.component.html',
  styleUrls: ['./studentrank.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class StudentrankComponent implements OnInit {
 // Pie
 colSize: any = 'col-sm-12';
 // tslint:disable-next-line: no-inferrable-types
 examId: string ;
 type: string;
 subjectStudentDetails: any;
 unitStudentDetails: any;
 chapterStudentDetails: any;
 topicStudentDetails: any;
  masterListShow = true;
  subjectListShow = false;
  unitListShow = false;
  chapterListShow = false;
  topicListShow = false;
  showTopicTab = false;
  public loadingAllChart = false;
  public loadingStudentList = false;
  public loadingCheckReport = false;
  public isReportAvailable = false;
  public loadingSingleStudentReport = true;

  public tabSubject = false;
  public tabUnit = false;
  public tabChapter = false;

  public reportMessage = 'Please Wait...!';
 subjectList = [];
 chartStudents = [];
 chartTotalMarks = [];
 chartpositiveMarks = [];
 chartNegativeMarks = [];
 subjecthigestmarks = [];
 subjectlowestmarks = [];
 subjectaveragemarks = [];
 subjectDetails: {
  subjectName: string,
  uniqueId: string,
  highestmarks: number,
  lowestmarks: number,
  averagemarks: number,
  color: string
}[] = [];
 unitList = [];
 unitDetails: {
   unitName: string,
   uniqueId: string,
   noofquestions: number,
   nooftimequestionattempted: number,
   nooftimequestionisright: number,
   attemptedaccuracy: string,
   overallaccuracy: string,
   color: string
 }[] = [];
 unitOverallAccuracy = [];

 chapterList = [];
 chapterDetails: {
  chapterName: string,
   uniqueId: string,
   noofquestions: number,
   nooftimequestionattempted: number,
   nooftimequestionisright: number,
   attemptedaccuracy: string,
   overallaccuracy: string,
   color: string
 }[] = [];
 chapterOverallAccuracy = [];

 topicList = [];
 topicDetails: {
  topicName: string,
   uniqueId: string,
   noofquestions: number,
   nooftimequestionattempted: number,
   nooftimequestionisright: number,
   attemptedaccuracy: string,
   overallaccuracy: string,
   color: string
 }[] = [];
 topicOverallAccuracy = [];
constructor(config: NgbModalConfig, private modalService: NgbModal,
            private toastService: ToastsupportService, private route: ActivatedRoute, private router: Router ) {
              config.backdrop = 'static';
              config.keyboard = false;
              monkeyPatchChartJsTooltip();
              monkeyPatchChartJsLegend();
              this.route.params.subscribe(params => {
                this.examId = params['examId'];
                this.type = params['type'];
            });
}
absentStudentList: {
  studentId: number
  Name: string;
  uniqueId: string,
  rank: string;
  totalmarks: string ;
  positivemarks: string ;
  negativemarks: string ;
}[] = [];
  studentList: {
    studentId: number
    Name: string;
    uniqueId: string,
    rank: string;
    totalmarks: string ;
    positivemarks: string ;
    negativemarks: string ;
  }[] = [];

  StudentName: string;
  examHighestMarks: string;
  examAverageMarks: string;
  examlowestMarks: string;
  studentMarksScored: string;
  positiveMarks: string;
  nigativeMarks: string;
  numberofrightquestion: string;
  numberofwrongquestion: string;
  numberofunattemptedquestion: string;
  attemptedaccuracy: string;
  overallaccuracy: string;
  totaltime: string;
  subjectreport: any;
  unitreport: any;
  chapterreport: any;
  selectedStudentId: any;

  showAllReport = false;
  examslist: {
    examuid: string
    examName: string,
    examtype: number,
    selected: boolean,
    color: string ,
  }[] = [];

  ngOnInit() {
    this.checkReportAvailable(this.examId);
    // this.modalService.open('content');
    const element: HTMLElement = document.getElementById('launchmodal') as HTMLElement;
    element.click();
    this.loadingCheckReport = true;
  }

  open(content) {
    this.modalService.open(content);
  }
  openStudentAllReport(content, studentId, studentName) {
    this.clearData();
    this.showAllReport = false;
    this.StudentName = studentName;
    this.selectedStudentId = studentId;
    this.modalService.open(content, { size: 'xl' });
    this.getStudentReport(this.examId, this.type, studentId);
  }
  clearData() {
    this.examHighestMarks = '0';
    this.examAverageMarks = '0';
    this.examlowestMarks = '0';
    this.studentMarksScored = '0';
    this.positiveMarks = '0';
    this.nigativeMarks = '0';
    this.numberofrightquestion = '0';
    this.numberofwrongquestion = '0';
    this.numberofunattemptedquestion = '0';
    this.attemptedaccuracy = '0';
    this.overallaccuracy = '0';
    this.totaltime = '0';
    this.subjectreport = null;
    this.unitreport = null;
    this.chapterreport = null;
  }
  getStudentReport(examId, type , studentId){
    console.log('stud id ' + studentId);
    this.loadingSingleStudentReport = true;
    // this.userStudentService.getSingleStudentReport(examId, type, studentId).subscribe(
    //   (res) => {
    //     console.log('getSingleStudentReport' + JSON.stringify(res));
    //     if (res) {
    //         this.loadingSingleStudentReport = false;
    //         this.examHighestMarks = res.examstatistics.highestmarks;
    //         this.examAverageMarks = res.examstatistics.averagemarks;
    //         this.examlowestMarks = res.examstatistics.lowestmarks;
    //         if(res.examreport != null){
    //           this.studentMarksScored = res.examreport.marksscored;
    //           this.positiveMarks = res.examreport.positivemarks;
    //           this.nigativeMarks = res.examreport.negativemarks;
    //           this.numberofrightquestion = res.examreport.numberofrightquestion;
    //           this.numberofwrongquestion = res.examreport.numberofwrongquestion;
    //           this.numberofunattemptedquestion = res.examreport.numberofunattemptedquestion;
    //           this.attemptedaccuracy = res.examreport.attemptedaccuracy;
    //           this.overallaccuracy = res.examreport.overallaccuracy;
    //           this.totaltime = res.examreport.totaltime;
    //         }
    //         this.subjectreport = res.subjectreport;
    //         this.unitreport = res.unitreport;
    //         this.chapterreport = res.chapterreport;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  selectExam(examId, examType, selected){
    if (!selected) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < this.examslist.length; i++) {

        if (examId === this.examslist[i].examuid) {
          this.examslist[i].selected = true;
          this.examslist[i].color = '#d3d3d3';
        } else {
          this.examslist[i].selected = false;
          this.examslist[i].color = '#ffffff';
        }
        this.getStudentReport(examId, examType, this.selectedStudentId);
      }
    }
  }
  checkReportAvailable(examId) {
    // this.userStudentService.checkReportAvailable(examId).subscribe(
    //   (res) => {
    //     if (res) {
    //         switch (res) {
    //                 case 1:
    //                   this.reportMessage = 'Sorry! report is not available for this exam, you can gerate result for this exam or modify or correct answer in question paper onces result genrated you cant modify answers or result';
    //                   this.isReportAvailable = true;
    //                   break;
    //                 case 2:
    //                   this.tabSubject = false;
    //                   this.tabUnit = false;
    //                   this.tabChapter = false;
    //                   const element: HTMLElement = document.getElementById('closemodal') as HTMLElement;
    //                   element.click();
    //                   this.loadingAllChart = true;
    //                   this.loadingStudentList = true;
    //                   this.getStudentsFromEipCore(this.examId, this.type);
    //                   break;
    //                 case 3:
    //                   this.tabSubject = true;
    //                   this.tabUnit = true;
    //                   this.tabChapter = true;
    //                   this.loadingAllChart = true;
    //                   this.loadingStudentList = true;
    //                   const element1: HTMLElement = document.getElementById('closemodal') as HTMLElement;
    //                   element1.click();
    //                   this.getStudentsFromEipCore(this.examId, this.type);
    //                   this.getExamReport(this.examId, this.type);
    //                   break;
    //             }
    //         this.loadingCheckReport = false;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  getAllstudentExams() {

    this.loadingSingleStudentReport = true;
    this.clearData();

    // this.userStudentService.getStudentExamList(this.selectedStudentId).subscribe(
    //   (res) => {
    //     if (res) {
    //       this.showAllReport = true;
    //       let count = 0;
    //       res.forEach(element => {
    //         if (count === 0){
    //           this.examslist.push({
    //             examuid: element.exam_uid,
    //             examName: element.exam_name,
    //             examtype: element.attempt_type,
    //             selected: true,
    //             color: '#d3d3d3' ,
    //           });
    //           this.getStudentReport(element.examuid, element.examtype, this.selectedStudentId);
    //         } else {
    //           this.examslist.push({
    //             examuid: res.exam_uid,
    //             examName: res.exam_name,
    //             examtype: element.attempt_type,
    //             selected: false,
    //             color: '#ffffff' ,
    //           });
    //         }
    //         count = count + 1;
    //       });
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  getStudentsFromEipCore(examId, type) {
    // this.userStudentService.getStudentsFromEipCorebyexam(examId, type).subscribe(
    //   (res) => {
    //     if (res) {
    //         res.forEach(element => {
    //           let dto;
    //           if (element.isAttended) {
    //               this.chartStudents.push(element.studentName);
    //               dto = {
    //               studentId: element.studentUniqueId,
    //               Name: element.studentName,
    //               uniqueId: element.studentUniqueId,
    //               rank: 'Loading',
    //               totalmarks: 'Loading',
    //               positivemarks: 'Loading',
    //               negativemarks: 'Loading',
    //              };
    //               this.studentList.push(dto);
    //           } else {
    //             dto = {
    //               studentId: element.studentUniqueId,
    //               Name: element.studentName,
    //               uniqueId: element.studentUniqueId,
    //               rank: 'Absent',
    //               totalmarks: 'Absent',
    //               positivemarks: 'Absent',
    //               negativemarks: 'Absent',
    //              };
    //             this.absentStudentList.push(dto);
    //           }
    //       });

    //         this.getstudentdatafromnodejs(examId, this.type);
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  getstudentdatafromnodejs(examId, type) {
    // this.userStudentService.getStudentsFromNestjsbyexam(examId, type).subscribe(
    //   (res) => {
    //     if (res) {
    //      res.studentdetails.forEach(element => {
    //           this.studentList.forEach(students => {
    //             if (students.studentId === element.studentid) {
    //               if (students.rank === 'Loading') {
    //                   this.chartTotalMarks.push(element.marksscored);
    //                   this.chartNegativeMarks.push(element.negativemarks);
    //                   this.chartpositiveMarks.push(element.positivemarks);
    //               }
    //               students.rank = element.rank,
    //               students.totalmarks = element.marksscored;
    //               students.positivemarks = element.positivemarks;
    //               students.negativemarks = element.negativemarks;
    //           }
    //           });
    //       });
    //      // tslint:disable-next-line: only-arrow-functions
    //      this.studentList.sort( function(a, b) {
    //         // Compare the 2 dates
    //         if (b.totalmarks > a.totalmarks ) { return 1; }
    //         if (b.totalmarks < a.totalmarks) { return -1; }
    //         return 0;
    //       });
    //      this.studentList = this.studentList.concat(this.absentStudentList);
    //      this.loadingAllChart = false;
    //      this.loadingStudentList = false;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
// add chart data here
// --------------line chart data--------------------
// tslint:disable-next-line: member-ordering
public barChartOptions: ChartOptions = {
  responsive: true,
  // We use these empty structures as placeholders for dynamic theming.
  scales: { xAxes: [{}], yAxes: [{}] },
};
// tslint:disable-next-line: member-ordering
public examoneChartLabels: Label[] = this.chartStudents;
// tslint:disable-next-line: member-ordering
public examoneChartType: ChartType = 'bar';
// tslint:disable-next-line: member-ordering
public examoneChartLegend = true;
// tslint:disable-next-line: member-ordering
public examoneChartData: ChartDataSets[] = [
  { data: this.chartTotalMarks, label: 'Marks Scored' },
  { data: this.chartpositiveMarks, label: 'Positive marks' },
  { data: this.chartNegativeMarks, label: 'Negative marks' }
];

 // --------------line chart data--------------------

  getExamReport(examid, type) {
    // this.userStudentService.getExamReport(examid, type).subscribe(
    //   (res) => {
    //     let subjectcount = 0;
    //     res.subjectDetails.forEach(element => {
    //        this.subjectList.push(element.subjectname);
    //        this.subjecthigestmarks.push(element.highestmarks);
    //        this.subjectlowestmarks.push(element.lowestmarks);
    //        this.subjectaveragemarks.push(element.averagemarks);
    //        let dto;
    //        if ( subjectcount === 0) {
    //          dto = {
    //           subjectName: element.subjectname,
    //           highestmarks: element.highestmarks,
    //           lowestmarks: element.lowestmarks,
    //           averagemarks: element.averagemarks,
    //           uniqueId: element.subjectid,
    //           color: '#DCDCDC'
    //          };
    //          this.getsubjectStudents(this.examId, element.subjectid);
    //        } else {
    //          dto = {
    //           subjectName: element.subjectname,
    //           highestmarks: element.highestmarks,
    //           lowestmarks: element.lowestmarks,
    //           averagemarks: element.averagemarks,
    //           uniqueId: element.subjectid,
    //           color: '#ffffff'
    //          };
    //        }
    //        subjectcount = subjectcount + 1;
    //        this.subjectDetails.push(dto);
    //      });

    //     let unitcount = 0;
    //     res.unitDetails.forEach(element => {
    //        this.unitList.push(element.unitname);
    //        this.unitOverallAccuracy.push(element.overallaccuracy);
    //        let  dto;
    //        if (unitcount === 0) {
    //         dto = {
    //           unitName: element.unitname,
    //           uniqueId: element.unitsuniqueid,
    //           noofquestions: element.noofquestions,
    //           nooftimequestionattempted: element.nooftimesquestionisattempted,
    //           nooftimequestionisright: element.nooftimesattemptedquestionisright,
    //           attemptedaccuracy: element.attemptedaccuracy,
    //           overallaccuracy: element.overallaccuracy,
    //           color: '#DCDCDC'
    //         };
    //         this.getUnitStudents(this.examId, element.unitsuniqueid);
    //        } else {
    //         dto = {
    //           unitName: element.unitname,
    //           uniqueId: element.unitsuniqueid,
    //           noofquestions: element.noofquestions,
    //           nooftimequestionattempted: element.nooftimesquestionisattempted,
    //           nooftimequestionisright: element.nooftimesattemptedquestionisright,
    //           attemptedaccuracy: element.attemptedaccuracy,
    //           overallaccuracy: element.overallaccuracy,
    //           color: '#fffff'
    //         };
    //        }
    //        unitcount = unitcount + 1;
    //        this.unitDetails.push(dto);
    //      });
    //     if (unitcount === 0){
    //       this.tabUnit = false;
    //      }
    //     let chaptercount = 0;
    //     res.chapterDetails.forEach(element => {
    //       this.chapterList.push(element.chaptername);
    //       this.chapterOverallAccuracy.push(element.overallaccuracy);
    //       let  dto;
    //       if (chaptercount === 0) {
    //         dto = {
    //           chapterName: element.chaptername,
    //           uniqueId: element.chaptersuniqueid,
    //           noofquestions: element.noofquestions,
    //           nooftimequestionattempted: element.nooftimesquestionisattempted,
    //           nooftimequestionisright: element.nooftimesattemptedquestionisright,
    //           attemptedaccuracy: element.attemptedaccuracy,
    //           overallaccuracy: element.overallaccuracy,
    //           color: '#DCDCDC'
    //         };
    //         this.getChapterStudents(this.examId, element.chaptersuniqueid);
    //       } else {
    //         dto = {
    //           chapterName: element.chaptername,
    //           uniqueId: element.chaptersuniqueid,
    //           noofquestions: element.noofquestions,
    //           nooftimequestionattempted: element.nooftimesquestionisattempted,
    //           nooftimequestionisright: element.nooftimesattemptedquestionisright,
    //           attemptedaccuracy: element.attemptedaccuracy,
    //           overallaccuracy: element.overallaccuracy,
    //           color: '#ffffff'
    //         };
    //       }
    //       chaptercount = chaptercount + 1;
    //       this.chapterDetails.push(dto);
    //     });
    //     if (chaptercount === 0){
    //       this.tabChapter = false;
    //      }

    //     let topicount = 0;
    //     if (res.topicDetails.lenth > 0) {
    //       this.showTopicTab = true;
    //     }
    //     res.topicDetails.forEach(element => {
    //       this.topicList.push(element.topicname);
    //       this.topicOverallAccuracy.push(element.overallaccuracy);
    //       let  dto;
    //       if (topicount === 0) {
    //         dto = {
    //           topicName: element.topicname,
    //           uniqueId: element.topicuniqueid,
    //           noofquestions: element.noofquestions,
    //           nooftimequestionattempted: element.nooftimesquestionisattempted,
    //           nooftimequestionisright: element.nooftimesattemptedquestionisright,
    //           attemptedaccuracy: element.attemptedaccuracy,
    //           overallaccuracy: element.overallaccuracy,
    //           color: '#DCDCDC'
    //         };
    //         this.getTopicStudents(this.examId, element.topicuniqueid);
    //       } else {
    //         dto = {
    //           topicName: element.topicname,
    //           uniqueId: element.topicuniqueid,
    //           noofquestions: element.noofquestions,
    //           nooftimequestionattempted: element.nooftimesquestionisattempted,
    //           nooftimequestionisright: element.nooftimesattemptedquestionisright,
    //           attemptedaccuracy: element.attemptedaccuracy,
    //           overallaccuracy: element.overallaccuracy,
    //           color: '#ffffff'
    //         };
    //       }
    //       topicount = topicount + 1;
    //       this.topicDetails.push(dto);
    //     });
    //     if (topicount === 0){
    //       this.showTopicTab = false;
    //      }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  getsubjectStudents(examId, subjectId) {
    // this.userStudentService.getsubjectStudents(examId, subjectId).subscribe(
    //   (res) => {
    //     if (res) {
    //       this.subjectStudentDetails = res;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  getUnitStudents(examId, subjectId) {
    // this.userStudentService.getUnitStudents(examId, subjectId).subscribe(
    //   (res) => {
    //     if (res) {
    //       this.unitStudentDetails = res;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }

  getChapterStudents(examId, chapterid) {
    // this.userStudentService.getChapterStudents(examId, chapterid).subscribe(
    //   (res) => {
    //     if (res) {
    //       this.chapterStudentDetails = res;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  getTopicStudents(examId, topicid) {
    // this.userStudentService.getTopicStudents(examId, topicid).subscribe(
    //   (res) => {
    //     if (res) {
    //       this.topicStudentDetails = res;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }

  genrateCompleteReportForcefully() {
    this.reportMessage = 'Please wait genrating report forcefully....!';
    this.loadingCheckReport = true;
    // this.userStudentService.genrateCompleteReportForcefully(this.examId).subscribe(
    //   (res) => {
    //     if (res) {
    //       this.loadingCheckReport = false;
    //       this.loadingAllChart = true;
    //       this.loadingStudentList = true;
    //       this.getStudentsFromEipCore(this.examId, 1);
    //       const element: HTMLElement = document.getElementById('closemodal') as HTMLElement;
    //       element.click();
    //       this.genrateQuestionReportForcefully(this.examId);
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });

  }

  genrateQuestionReportForcefully(examId) {
    // this.userStudentService.genrateQuestionReportForcefully(examId).subscribe(
    //   (res) => {
    //     if (res) {
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   });

  }
  // ------------------unit chart--------------------
  // tslint:disable-next-line: member-ordering
  public unitPieChartOptions: ChartOptions = {
    responsive: true,
  };
   // tslint:disable-next-line: member-ordering
  public unitPieChartLabels: Label[] = this.unitList;
   // tslint:disable-next-line: member-ordering
  public unitPieChartData: SingleDataSet = this.unitOverallAccuracy;
   // tslint:disable-next-line: member-ordering
  public unitPieChartType: ChartType = 'pie';
   // tslint:disable-next-line: member-ordering
  public unitPieChartLegend = true;
   // tslint:disable-next-line: member-ordering
  public unitPieChartPlugins = [];
  // tslint:disable-next-line: member-ordering
  public pieChartColors = [
    {
      backgroundColor: ['rgba(87, 147, 243)', 'rgba(84, 120, 194)', 'rgba(211, 74, 116)', 'rgba(186, 62, 73)', 'rgba(221, 69, 68)',
       'rgba(253, 156, 53)', 'rgba(253, 196, 44)', 'rgba(212, 224, 90)', 'rgba(84, 120, 194)'],
    },
  ];
  // ------------------unit chart--------------------

    // ------------------Chapter chart--------------------
    // tslint:disable-next-line: member-ordering
    public chapterPieChartOptions: ChartOptions = {
      responsive: true,
    };
    // tslint:disable-next-line: member-ordering
    public chapterPieChartLabels: Label[] = this.chapterList;
     // tslint:disable-next-line: member-ordering
    public chapterPieChartData: SingleDataSet = this.chapterOverallAccuracy;
     // tslint:disable-next-line: member-ordering
    public chapterPieChartType: ChartType = 'pie';
     // tslint:disable-next-line: member-ordering
    public chapterPieChartLegend = true;
     // tslint:disable-next-line: member-ordering
    public chapterPieChartPlugins = [];
      // ------------------Chapter chart--------------------
      // tslint:disable-next-line: member-ordering
      public topicPieChartOptions: ChartOptions = {
        responsive: true,
      };
       // tslint:disable-next-line: member-ordering
      public topicPieChartLabels: Label[] = this.topicList;
       // tslint:disable-next-line: member-ordering
      public topicPieChartData: SingleDataSet = this.topicOverallAccuracy;
       // tslint:disable-next-line: member-ordering
      public topicPieChartType: ChartType = 'pie';
       // tslint:disable-next-line: member-ordering
      public topicPieChartLegend = true;
       // tslint:disable-next-line: member-ordering
      public topicPieChartPlugins = [];
      // ------------------Chapter chart--------------------

 // subject data-------------------------------
  // tslint:disable-next-line: member-ordering
 public subjectChartOptions: ChartOptions = {
  responsive: true,
};
 // tslint:disable-next-line: member-ordering
public barChartType: ChartType = 'bar';
 // tslint:disable-next-line: member-ordering
public barChartLegend = true;
 // tslint:disable-next-line: member-ordering
public barChartPlugins = [];
 // tslint:disable-next-line: member-ordering
public subjectbarChartLabels: Label[] = this.subjectList;
 // tslint:disable-next-line: member-ordering
public subjectbarChartData: ChartDataSets[] = [
  { data: this.subjecthigestmarks, label: 'Highest Marks' },
  { data: this.subjectlowestmarks, label: 'Lowest Marks' },
  { data: this.subjectaveragemarks, label: 'Average Marks' }
];
 // subject data-------------------------------
  changeTab(event) {
    switch (event.nextId) {
      case 0:
        this.colSize = 'col-sm-12';
        this.masterListShow = true;
        this.subjectListShow = false;
        this.unitListShow = false;
        this.chapterListShow = false;
        this.topicListShow = false;
        break;
      case 1:
        this.colSize = 'col-sm-6';
        this.masterListShow = false;
        this.subjectListShow = true;
        this.unitListShow = false;
        this.chapterListShow = false;
        this.topicListShow = false;
        break;
      case 2:
        this.colSize = 'col-sm-6';
        this.masterListShow = false;
        this.subjectListShow = false;
        this.unitListShow = true;
        this.chapterListShow = false;
        this.topicListShow = false;
        break;
      case 3:
        this.colSize = 'col-sm-6';
        this.masterListShow = false;
        this.subjectListShow = false;
        this.unitListShow = false;
        this.chapterListShow = false;
        this.topicListShow = false;
        break;
      case 4:
        this.colSize = 'col-sm-6';
        this.masterListShow = false;
        this.subjectListShow = false;
        this.unitListShow = false;
        this.chapterListShow = true;
        this.topicListShow = false;
        break;
      case 5:
        this.colSize = 'col-sm-6';
        this.masterListShow = false;
        this.subjectListShow = false;
        this.unitListShow = false;
        this.chapterListShow = false;
        this.topicListShow = true;
        break;
      case 6:
          this.colSize = 'col-sm-6';
          this.masterListShow = false;
          this.subjectListShow = false;
          this.unitListShow = false;
          this.chapterListShow = false;
          this.topicListShow = false;
          break;
    }
}

// bar and line chart processing-----------------------
 // events
 public exam_one_chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  // console.log(event, active);
}

public exam_one_chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  // console.log(event, active);
}

public exam_one_randomize(): void {
  this.examoneChartType = this.examoneChartType === 'bar' ? 'line' : 'bar';
}
// bar and line chart processing-----------------------

onClickSubject(subjectId) {
  this.subjectDetails.forEach(res => {
    if (res.uniqueId === subjectId) {
      res.color = '#DCDCDC';
    } else {
      res.color = '#FFFFFF';
    }
  });
  this.subjectStudentDetails = null;
  this.getsubjectStudents(this.examId, subjectId);
}
onClickUnit(unitId) {
  this.unitDetails.forEach(res => {
    if (res.uniqueId === unitId) {
      res.color = '#DCDCDC';
    } else {
      res.color = '#FFFFFF';
    }
  });
  this.unitStudentDetails = null;
  this.getUnitStudents(this.examId, unitId);
}
onClickChapter(chapterid) {
  this.chapterDetails.forEach(res => {
    if (res.uniqueId === chapterid) {
      res.color = '#DCDCDC';
    } else {
      res.color = '#FFFFFF';
    }
  });
  this.chapterStudentDetails = null;
  this.getChapterStudents(this.examId, chapterid);
}
onClickTopic(topicid) {
  this.topicDetails.forEach(res => {
    if (res.uniqueId === topicid) {
      res.color = '#DCDCDC';
    } else {
      res.color = '#FFFFFF';
    }
  });
  this.topicStudentDetails = null;
  this.getTopicStudents(this.examId, topicid);
}

  openPaperModification(){
    this.router.navigate(['/hr/2/modifypaper', this.examId]);
  }

  ViewStudentSubmissionInfo(){

  }
}
