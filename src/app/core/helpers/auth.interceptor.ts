import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';


@Injectable({providedIn: 'root'})
export class AuthInterceptor implements HttpInterceptor {

    constructor(private router: Router) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const tokenId = sessionStorage.getItem('tokenId');
        // let previousTime = sessionStorage.getItem("lastLoginTime");
        // let currentTime: number = (new Date()).getTime();

        if (tokenId!=null && tokenId!=undefined) {
            const cloned = request.clone({
                headers: request.headers.set("Authorization", "Bearer "+tokenId)
            });
            return next.handle(cloned);
        } else {
            return next.handle(request);
        }
    }
}