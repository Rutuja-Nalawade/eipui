import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-timedetails',
  templateUrl: './timedetails.component.html',
  styleUrls: ['./timedetails.component.scss']
})
export class TimedetailsComponent implements OnInit {
  @Input() timingInfo: FormGroup;
  @Input() examType: any;
  @Input() typeborder:any;
  mytime: Date;
  showTimePicker=false
  time: NgbTimeStruct = {hour: 13, minute: 30, second: 0};
  minDate = new Date(new Date().setDate(new Date().getDate()-1));
  meridian = true;
  
  constructor() { }

  ngOnInit() {
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  startDatechanged(){

  }
  timerShow() {
    document.getElementById("timer").click(); // Click on the checkbox
  }
  selectTime(){
    this.showTimePicker=true
  }
  timeChange(event){
    console.log("event",event)
  }
}
