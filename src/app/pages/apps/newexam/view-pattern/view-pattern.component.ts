import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ExamService } from '../exam.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
declare var $:any
@Component({
  selector: 'app-view-pattern',
  templateUrl: './view-pattern.component.html',
  styleUrls: ['./view-pattern.component.scss']
})
export class ViewPatternComponent implements OnInit {

  tabNumber:any=1;
 currentUserId:Number
 isButtonDisabled=true;
 isSelectPatternShow=false;
 isPatternTabShow=false;
 isRadioActivate:any="1";
 examPatternList: [];
 examPatternType:any;
 examPattern: any;
 pattern1: any;
 pattern2: any;

 type=[{id:'1',name:'NEET Pattern'},{id:'2',name:'JEE Mains Pattern'},{id:'3',name:'JEE Advanced Pattern'},{id:'4',name:'Custom Pattern'}]
 @Input() activeUserId:Number
  constructor(private _commonService: NewCommonService,
              private _examService:ExamService,
              private toastService: ToastsupportService,
              private spinner:NgxSpinnerService ) {
  
    
    this._commonService.isotherClicked.subscribe(tab=>{
      console.log('selected tab is1==>>',tab);
      this.tabNumber=tab
      if(tab=='1' || tab==1){
        this.tabNumber=1
        console.log('selected tab is2==>>',this.tabNumber)
      }
      
    })
    console.log("uerid4---",this.activeUserId);
  }
  
  ngOnInit() {
    this.currentUserId= this.activeUserId
    console.log("userId2--------",this.activeUserId);
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['activeUserId']) {
        // Do your logic here
        this.tabNumber=1;
        console.log("tab changed");
        
    }
  }

  ngAfterViewInit(){
    $(document).ready(function() {
      "use strict";
    
      $('ul > li').click(function(e) {
        e.preventDefault();
        $('ul > li').removeClass('active');
        $(this).addClass('active');
      });
    });
  }
  tabClick(tabNumber){
    // console.log('selected tab===>>',tabNumber)
    localStorage.setItem('selectedTab',tabNumber)//basic=1,other=2
    this.tabNumber= tabNumber;
    this._commonService.isotherClicked.next(tabNumber)
    this._commonService.batchFromParent.next(0)
    // if(tabNumber==1){
    //   this._commonService.batchFromParent.next(true)
    // }else{
    //   this._commonService.batchFromParent.next(false)
    // }
  }

  close(){
    this._commonService.iscloseClicked.next(true)
    localStorage.removeItem('selectedTab')
  }
  getExamPatternList(event){
    console.log("event",event)

    // if (event=="") {
    //   this.isButtonDisabled=true;event
    //   this.isSelectPatternShow=false;
    //   this.isPatternTabShow=false;
    // } else {
    //   this.isButtonDisabled=false;
    //   this.isSelectPatternShow=true;
    //   this.isPatternTabShow=true;
    // }



    if (event != null && event != "") {
      this.spinner.show()
      this.examPatternList = [];
      this.examPatternType = event.id;
      //let dto = { "patternType": value, "organizationUid": sessionStorage.getItem('orgUniqueId') }
      return this._examService.getExamPatternList(event.id,sessionStorage.getItem('organizationId')).subscribe(
        res => {
          if (res) {
            this.examPatternList = res;
            console.log("this.examPatternList",this.examPatternList);
            this.spinner.hide()
          }
        },
        (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowWarning(error.error.errorMessage);
          // }
          this.spinner.hide()
        });
    }
  }

  getExamPatternDetails(examPatternUid: string) {
    this.examPattern=null
    console.log("examPatternUid",examPatternUid)
   
    if (examPatternUid != null) {
      this.spinner.show()
      // let examPattern = this.examPatternList.find(x => x.uniqueId == examPatternUid);
      return this._examService.getExamPatternFromEip(examPatternUid, this.examPatternType).subscribe(
        res => {
          console.log("res",res)
          if (res) {
            this.examPattern = res;
            this.pattern1 = this.examPattern.examPatterns[0];
            this.pattern2 = this.examPattern.examPatterns[1];
            console.log("this.pattern1",this.pattern1)
            console.log("this.pattern2",this.pattern2)
          }
          this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
            
          // } else {
          //   this.toastService.ShowError(error.error.errorMessage);
          // }

          this.spinner.hide()
        }
      );
    }

  }

}
