import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  listClicked = false;
  staffList = [];
  userData: any;
  pageIndex: number = 0;
  filterData: any = null;
  textSearch: boolean = false;
  selectedstaffId: any;
  staffBasicForm: FormGroup;
  submitted: boolean = false;
  isShowQuickForm: boolean = false;
  genders: any = [];
  currentUserId = null;
  isDropDownOpen = false;
  roleList = []
  selectedRoleList = []
  selectedShiftList = []
  selectedDaysList = []
  selectedOrgList = [];
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  public totalCount = 100;
  isShowPagination = false;
  pageSizeArray = [{ id: 0, name: "1-10", value: 10 }, { id: 1, name: "1-20", value: 20 }, { id: 2, name: "1-50", value: 50 }, { id: 3, name: "1-100", value: 100 }];
  pageSize = 10;
  selectedPageSize = 0;
  totalPages = 0;
  isNextPageValid = true;
  isSelectAll = false;
  shiftsList = [];
  orgList = [];
  selectedStaff: any = {};
  roleData: any = [];
  days = [{ day: "SUN", dayValue: 1, id: 1 }, { day: "MON", dayValue: 2, id: 2 }, { day: "TUE", dayValue: 3, id: 3 }, { day: "WED", dayValue: 4, id: 4 }, { day: "THU", dayValue: 5, id: 5 }, { day: "FRI", dayValue: 6, id: 6 }, { day: "SAT", dayValue: 7, id: 7 }]
  message = "";
  maxLength = 50;
  stringType = 3;
  searchUserList = [];
  searchableUserList = [];
  staffUniqueIds = [];
  searchText = ""
  @Input() tabNumber: Number;
  @Output() detailsOpen = new EventEmitter<string>();
  @ViewChild('myDrop', { static: true }) myDrop: NgbDropdown;

  constructor(private _userServiceService: UserServiceService, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private _DomSanitizationService: DomSanitizer, private formBuilder: FormBuilder, private toastService: ToastsupportService, private _userService: UserServiceService) {
    this.userData = this._commonService.getUserData();
    this.selectedOrgList = [parseInt(this.userData.organizationId)]
    //   this._commonService.iscloseClicked.subscribe(click => {
    //     if (click) {
    //         this.listClicked = false;
    //     }
    // })
    this._commonService.iscancelClicked.subscribe(click => {
      if (this.tabNumber == 1 && click == 2) {
        this.listClicked = false;
        this.findStaffRecords([])
        this.detailsOpen.emit("close")
        document.querySelector('body').classList.remove('hideScroll');
      }
    })
  }

  ngOnInit() {
    // this.findStaffRecords([])
    // this.initStaffInfoForm();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['tabNumber'] && changes['tabNumber'].currentValue == 1) {
      this.listClicked = false;
      this.isDropDownOpen = false;
      this.isShowQuickForm = false;
      this.getRoleInfo();
      this.findStaffRecords([]);
      // this.getstaffShiftInfo();
      // this.getChildOrganizationList(this.userData.orgUniqueId);
      // this.initStaffInfoForm();
    }
  }

  findStaffRecords(value: any) {
    console.log(value);
    if (value != null && value != undefined && value.length > 2) {
      this.spinner.show();
      this.textSearch = true;
      // this.isShowPagination = false;
      let searchDto = {
        'orgList': [this.userData.organizationId],
        'searchKey': value,
        'searchType': 1,
      };
      // this.loading = true;
      // this._staffService.searchUsersBySearchKey(searchDto)
      //   .subscribe(data => {
      //     if (data != null && data.length > 0) {
      //       this.staffList = data;
      //       this.isShowPagination = true;
      //     } else {
      //       this.staffList = [];
      //     }
      //     this.spinner.hide();
      //     this.textSearch = false;
      //   }, (error: HttpErrorResponse) => {
      //     if (error instanceof HttpErrorResponse) {
      //       this.spinner.hide();
      //       this.textSearch = false;
      //     } else {
      //       this.toastService.ShowError(error);
      //       this.spinner.hide();
      //       this.textSearch = false;
      //     }
      //   });
    } else {
      if (value.length == 0 || value == undefined) {
        //  alert();
        this.textSearch = false;
        this.searchStaff(false);
      }
    }
  }
  searchable(ele) {
    if (ele.keyCode == 13) {
      this.stringType = this.checkInputValue(this.searchText);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.searchText) == false) {
          this.toastService.ShowWarning("Email not valid!");
          this.message = "You search as email. Please complete email and enter";
          this.maxLength = 50;
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else if (this.stringType == 2) {
        this.maxLength = 10;
        if (this.searchText.length != 10) {
          this.message = "You search as mobile. Please complete mobile and enter";
          this.toastService.ShowWarning("Mobile not valid!");
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else {

      }
    }
  }
  searchStaff(isLoadMoreData: boolean) {

    let SerachNEwDto = null;

    if (isLoadMoreData) {
      // this.pageIndex = this.pageIndex + 1;
    } else {
      this.spinner.show();
      // this.setStaffListDivCollapsed = false;
      this.pageIndex = 0;
      this.staffList = [];
      this.filterData = (this.filterData != null && this.isDropDownOpen) ? this.filterData : null;
    }

    if (this.filterData == null) {
      SerachNEwDto = {
        staffUniqueIds: this.staffUniqueIds,
        page: this.pageIndex,
        size: 30,
        states: [
          1,
          2
        ],
        roleInformation: true
      }
    } else {
      SerachNEwDto = this.filterData;
    }
    this._userServiceService.getAllStaff(SerachNEwDto).subscribe(
      response => {
        let temp = response.staffs;
        // this.isShowPagination = false;
        let tempList = [];
        this.isShowPagination = false;
        this.totalCount = response.totalRecords;
        this.totalPages = response.totalPages;
        this.isNextPageValid = (this.totalPages == (this.pageIndex + 1)) ? false : true;
        if (this.staffList == null || this.staffList.length == 0) {

          if (temp != null && temp.length > 0) {
            temp.forEach((element, key) => {

              let dto = {
                name: element.staff.fullName,
                uniqueId: element.staff.uniqueId,
                roles: element.roles.toString()
              }
              this.staffList.push(dto);
              this.getImage(element.staff.uniqueId, key)
            });
            // this.staffList = tempList;
            this.isShowPagination = true;
            console.log("object");
          }
        } else {
          if (temp != null && temp.length > 0) {
            for (let i = 0; i < temp.length; i++) {
              const element = temp[i];
              let dto = {
                name: element.staff.fullName,
                uniqueId: element.staff.uniqueId,
                roles: element.roles
              }
              this.staffList.push(dto);
              this.getImage(element.staff.uniqueId, i)

            }
            console.log("object1");
            this.isShowPagination = true;
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  findRecords(value: any) {
    this.searchText = value;
    let stringLength = value.length;
    if (stringLength == 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      }
      this.message = "";
      this.maxLength = 50;

      this.searchUsers(value);
    } else if (stringLength > 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) == false) {
        }
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        if (stringLength != 10) {
        }
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      } else {
        let searchedUsers = [];
        if (this.searchableUserList.length > 0) {
          for (let i = 0; i < this.searchableUserList.length; i++) {
            const element = this.searchableUserList[i];
            if (element.fullName.toLowerCase().includes(value.toLowerCase())) {
              searchedUsers.push(element)
            }
          }
          this.searchUserList = searchedUsers;
          this.myDrop.open();
        }
        this.message = "";
        this.maxLength = 50;
      }
    } else if (stringLength == 0) {
      if (this.staffUniqueIds.length > 0) {
        this.staffUniqueIds = [];
        this.searchStaff(false)
      }
      this.staffUniqueIds = [];
      this.message = "";
      this.searchUserList = [];
    }
  }
  checkInputValue(string) {
    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(string)) {//if (/^[a-zA-Z0-9]*$/.test(value) == false) {
      return 1;
    } else if (/^[0-9]*$/.test(string)) {
      return 2;
    } else {
      return 3;
    }
  }
  searchUsers(value) {
    this._userService.searchUsers("staff", value)
      .subscribe(data => {
        if (data != null && data.length > 0) {
          // this.totalCount=data.length;
          this.searchableUserList = data;
          this.searchUserList = data;
          this.isShowPagination = true;
          this.myDrop.open();
        } else {
          this.searchUserList = [];
          this.toastService.ShowWarning("User not found!");
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.textSearch = false;
        // } else {
        //   this.toastService.ShowError(error);
        //   this.textSearch = false;
        // }
        this.spinner.hide();
      });
  }
  clickDropdownItem(item) {
    this.staffUniqueIds.push(item.uniqueId);
    this.searchStaff(false);
    this.myDrop.close();
  }
  rowClick(user) {
    this.selectedstaffId = user.uniqueId;
    this.selectedStaff = user;
    this.listClicked = true;
    this.isShowQuickForm = false;
    window.scroll(0, 0);
    this.detailsOpen.emit("open")
    var body = document.body;
    body.classList.add("hideScroll");
    // this.selectedStudentId=studentInfo.id
    // localStorage.setItem('selectedStudentId', currentId);
  }
  closeQuickForm() {
    this.isShowQuickForm = false;
    // this.staffBasicForm.reset();
    // this.initStaffInfoForm();
    this.submitted = false;
  }
  OpenQuickForm() {
    this.isShowQuickForm = true
  }
  onScroll() {
    this.searchStaff(true);
  }
  cancelFilterModal() {
    this.isDropDownOpen = false;
    this.myDrop.close();
  }
  apply() {
    this.searchStaff(false);
    this.myDrop.close();
  }
  openDropdown() {
    this.isDropDownOpen = true;
  }
  getList(event) {
    if (event == "1") {
      this.isShowQuickForm = false;
    }
    this.findStaffRecords([])
  }
  getRoleInfo() {
    this._userService.getUserRolesData("staff")
      .subscribe(data => {
        if (data) {
          this.roleData = data;
          let roleInfo = (data.roleCombinations) ? data.roleCombinations : "";
          let roleList = []
          for (let i = 0; i < roleInfo.length; i++) {
            let roles = {
              "id": roleInfo[i].uniqueId,
              "itemName": roleInfo[i].name
            }
            roleList.push(roles)
          }
          this.roleList = roleList;
        }
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
  onItemChange(item, type) {
    switch (type) {
      case 0:
        this.selectedRoleList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedRoleList.push(element.id);
          }
        }
        this.searchStaff(false);
        break;
      case 1:
        this.selectedShiftList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedShiftList.push(element.id);
          }
        }
        this.searchStaff(false);
        break;
      case 2:
        this.selectedDaysList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedDaysList.push(element.id);
          }
        }
        this.searchStaff(false);
        break;
      case 3:
        this.selectedOrgList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedDaysList.push(element.id);
          }
        } else {
          this.selectedOrgList = [parseInt(this.userData.organizationId)]
        }
        this.searchStaff(false);
        break;

      default:
        break;
    }
  }
  changePageSize(item) {
    this.pageSize = item.value;
    this.selectedPageSize = item.id;
  }
  goToPreviousPage() {
    if (this.pageIndex) {
      this.pageIndex = this.pageIndex - 1;
      this.isNextPageValid = true;
      this.searchStaff(true);
    }
  }
  goToNextPage() {
    if (this.isNextPageValid) {
      this.pageIndex = this.pageIndex + 1;
      this.searchStaff(true);
    }
  }
  checkUncheck(event) {
    this.isSelectAll = !this.isSelectAll
  }
  rowCheckboxChange(event) {
    if (!event.target.checked) {
      this.isSelectAll = false;
    }

  }
  // getstaffShiftInfo() {
  //   this._staffService.getstaffShiftInfo(this.userData.orgUniqueId).subscribe(
  //     response => {
  //       // console.log(response)
  //       if (response != null && response.shiftsList != null && response.shiftsList.length > 0) {
  //         let shiftlist = []
  //         let shiftData = response.shiftsList;
  //         // shiftlist.push(shiftData)
  //         for (let i = 0; i < shiftData.length; i++) {
  //           let dto = { id: shiftData[i].id, itemName: shiftData[i].shiftName, shiftUid: shiftData[i].shiftUid };
  //           shiftlist.push(dto)
  //         }
  //         this.shiftsList = shiftlist;
  //       }

  //     }, (error: HttpErrorResponse) => {
  //       if (error instanceof HttpErrorResponse) {
  //         // this.dataLoading = false;
  //       } else {
  //         this.toastService.ShowError(error);
  //         // this.dataLoading = false;
  //       }
  //     });
  // }
  // getChildOrganizationList(parentOrganizationId) {
  //   // let orgDto = { "uniqueId": this.orgUniqueId, "name": sessionStorage.getItem('organizationName') + " (Own)" };
  //   this._staffService.getOrganizationList(parentOrganizationId).subscribe(
  //     response => {
  //       let organizationList = response;
  //       // this.childOrganizationList.push(orgDto);
  //       if (organizationList != null && organizationList.length > 0) {
  //         let orgList = []
  //         for (let i = 0; i < organizationList.length; i++) {
  //           orgList.push({
  //             "id": organizationList[i].id, "itemName": organizationList[i].name
  //           });
  //         }
  //         this.orgList = orgList;
  //       }
  //     },
  //     (error: HttpErrorResponse) => {
  //       if (error instanceof HttpErrorResponse) {

  //       } else {
  //         this.toastService.ShowError(error);
  //       }
  //     }
  //   );
  // }
  getImage(uniqueId, index) {
    this._userService.getImage("staff", uniqueId, false).subscribe(
      response => {
        if (response != null && response) {
          if (response.image) {
            this.staffList[index]['imageCode'] = response.image;
          }
        }
      });
  }
}
