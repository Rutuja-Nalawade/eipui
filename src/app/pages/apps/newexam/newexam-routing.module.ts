import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExamListComponent } from './exam-list/exam-list.component';
import { AddPatternComponent } from './add-pattern/add-pattern.component';
import { AddTracksComponent } from './add-tracks/add-tracks.component';
import { CreateQuickExamComponent } from './create-quick-exam/create-quick-exam.component';
import { TrackInfoComponent } from './track-info/track-info.component';
import { TrackSyllabusComponent } from './track-syllabus/track-syllabus.component';
import { QuestionBankComponent } from './question-bank/question-bank.component';
import { ViewPatternComponent } from './view-pattern/view-pattern.component';
import { CreatePatternedExamComponent } from './create-patterned-exam/create-patterned-exam.component';
import { CanExitGuard } from 'src/app/can-exit.guard';
import { PublishExamComponent } from './publish-exam/publish-exam.component';


const routes: Routes = [
  {  path: 'examList', component: ExamListComponent},
  {  path: 'create-quick-exam', component: CreateQuickExamComponent},
  {  path: 'create-pattern', component: AddPatternComponent}, 
  {  path: 'create-pattern-exam', component: CreatePatternedExamComponent},
  {  path: 'view-pattern', component: ViewPatternComponent},
    { path: 'addPattern', component: AddPatternComponent },
    { path: 'track/create/:examId', component: AddTracksComponent },
    { path: 'track/info/:examId', component: TrackInfoComponent },
    { path: 'track/syllabus/:trackUid/:subjectName/:isEditable/:syllabusState/:testType/:examAssignmentSubjectId', component: TrackSyllabusComponent },
    { path: 'track/questionBank/:trackUid', component: QuestionBankComponent},
    { path: 'publish/:examId/:isAssessment/:examDate/:iseditable',component: PublishExamComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewexamRoutingModule { }
