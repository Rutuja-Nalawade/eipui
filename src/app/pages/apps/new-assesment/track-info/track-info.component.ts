import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { AssesmentService } from '../../../../servicefiles/assesment.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { CourseplannerServiceService } from 'src/app/servicefiles/courseplanner-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-track-info',
  templateUrl: './track-info.component.html',
  styleUrls: ['./track-info.component.scss']
})
export class TrackInfoComponent implements OnInit {
  @Input() examUid: any;
  examTracks: any;
  subjectName: any;
  selectedTrack: any;
  trackName: any;
  trackSyllabusPopup = false;
  isSelectGrade = false;
  syllabusId = 1;
  trackSyllabusForm: FormGroup;
  syllabusList = [];
  syllabusData: any;
  teachers = [];
  gradeList = [
  ];
  selectedGrade: any;
  subjectUniqueId: void;
  trackUniqueId: any;
  toQuestion: boolean;
  questionData: any;
  sectionDetails: any;
  editTrack = false;
  plannerData: any;
  unitList = [];
  chapterList = [];
  topicList = [];
  subTopicList = [];
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  submitted = false;
  isSelectPlanner = false;
  subjectCpFormGroup: FormGroup;
  coursePlanner: any = [];
  typeName = "Unit";
  @Output() close = new EventEmitter<string>();
  constructor(private modalService: NgbModal,
    private route: ActivatedRoute,
    private _examService: ExamServiceService,
    private _cpService: CourseplannerServiceService,
    private assesmentService: AssesmentService,
    private toastService: ToastsupportService,
    private router: Router,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private _userService: UserServiceService,
    private _DomSanitizationService: DomSanitizer) { }

  ngOnInit() {
    this.initTrackSyllabus(this.syllabusId, 1);

    if (this.examUid != null) {
      this.getExamTracks(this.examUid);
    }
  }
  initTrackSyllabus(type, syllabusType) {
    let dto: any = {};
    switch (type) {
      case 1:
        dto = {
          syllabusIds: [[], Validators.required]
        }
        break;
      default:
        if (type == 3) {
          dto['syllabusIds'] = [[], Validators.required]
        }
        dto['grade'] = ["", Validators.required]
        switch (syllabusType) {
          case 1:
            dto['unit'] = [null, Validators.required]
            break;
          case 2:
            dto['chapter'] = [null, Validators.required]
            break;
          case 4:
            dto['subTopic'] = [null, Validators.required]
            break;

          default:
            dto['topic'] = [null, Validators.required]
            break;
        }
        break;
    }
    this.trackSyllabusForm = this.fb.group(dto);
  }

  getExamTracks(examUid: string) {
    this.spinner.show();
    this.assesmentService.getExamTrack(examUid).subscribe(
      res => {
        if (res) {

          this.examTracks = res;
          this.examTracks.trackSubjectList.forEach((x, i) => {
            x.sectionList.forEach((y, j) => {
              y.trackList.forEach((z, k) => {
                if (z.teacherDto && z.teacherDto.uniqueId) {
                  this.getImage(z.teacherDto.uniqueId, i, j, k);
                } else {
                  z.teacherDto['imageCode'] = "";
                }
              });
            });
          });
          this.spinner.hide();
          console.log('this.examTracks', this.examTracks)

        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.toastService.ShowError(error.error)
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   // this.spinner = false;
        //   this.toastService.ShowError(error)
        // }
      }
    );
  }

  goToSyllabus(trackSubject, section, track, index) {
    this.syllabusData = {};
    this.subjectUniqueId = trackSubject.uniqueId;
    this.sectionDetails = section;
    console.log("track", track)
    this.selectedTrack = track;
    this.trackUniqueId = track.uniqueId;
    this.subjectName = trackSubject.name;
    this.trackName = "Track-" + (index + 1);
    if (trackSubject.isSyllabusSelected) {
      this.spinner.show();
      this.assesmentService.getSubjectSyllabusTrack(this.examUid, trackSubject.uniqueId).subscribe(
        res => {
          if (res) {
            this.syllabusData = res;
            if (res.importSyllabusDto && !res.customSyllabusDto) {
              this.syllabusId = 1;
            } else if (!res.importSyllabusDto && res.customSyllabusDto) {
              this.syllabusId = 2;
              this.getGradeList(this.examTracks.courseDto.uniqueId, trackSubject.uniqueId);
            } else {
              this.syllabusId = 3;
              this.getGradeList(this.examTracks.courseDto.uniqueId, trackSubject.uniqueId);
            }
            this.initTrackSyllabus(this.syllabusId, res.syllabusType);
            if (res.importSyllabusDto || res.customSyllabusDto) {
              let listData = (res.importSyllabusDto) ? res.importSyllabusDto : res.customSyllabusDto
              switch (res.syllabusType) {
                case 1:
                  this.syllabusList = this.patchList(listData.unitList, res.syllabusType);
                  break;
                case 2:
                  this.syllabusList = this.patchList(listData.chapterList, res.syllabusType);
                  break;
                case 4:
                  this.syllabusList = this.patchList(listData.subTopicsList, res.syllabusType);
                  break;

                default:
                  this.syllabusList = this.patchList(listData.topicList, res.syllabusType);
                  break;
              }
            }
            this.trackSyllabusPopup = true;
            this.spinner.hide();
            console.log('this.examTracks', this.examTracks)

          }
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide()
        //   if (error instanceof HttpErrorResponse) {
        //     console.log("Client-side error occured.");
        //     this.spinner.hide();
        //   } else {
        //     this.spinner.hide();
        //     this.toastService.ShowError(error)
        //   }
         }
      );
    } else {
      this.subjectCpFormGroup = this.fb.group({
        uniqueId: [trackSubject.uniqueId, Validators.required],
        subjectName: [trackSubject.name, Validators.required],
        actualCoursePlanners: this.fb.array([], Validators.nullValidator),
        remarks: ['', Validators.nullValidator],
        syllabus: [[], Validators.nullValidator],
        examAssignmentPatternSubjectId: ['', Validators.nullValidator]
      });
      this.coursePlanner = [];
      this.spinner.show();
      let courseId = this.examTracks.courseDto.uniqueId;
      this._examService.getCoursePlannerList(trackSubject.uniqueId, courseId).subscribe(
        res => {
          if (res && res.success) {
            this.coursePlanner = res.list;
            let cpList = [];
            let actualCPArray = <FormArray>this.subjectCpFormGroup.controls['actualCoursePlanners'];
            actualCPArray.controls = [];
            for (let cp of res.list) {
              actualCPArray.push(this.addCoursePlanner(cp, false))
            }
            // this.subjectCpFormGroup.controls.actualCoursePlanners.setValue(cpList);
            // cpArray.controls = [];
            this.isSelectPlanner = true;
          } else {
            this.toastService.ShowWarning(res.responseMessage);
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide()
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          //   this.spinner.hide();
          // } else {
          //   this.spinner.hide();
          //   this.toastService.ShowError(error)
          // }
        });
    }
  }
  addCoursePlanner(data: any, isSelected: boolean) {
    return this.fb.group({
      uniqueId: data.uniqueId,
      name: data.name,
      isSelected: isSelected,
      syllabusCoursewise: [],
      selectedSyllabus: [],
      selectedsyllabusCoursewise: this.fb.array([], Validators.nullValidator),
    });
  }
  onClickCheckbox(event, cp, index) {
    console.log(cp);
    if (event.target.checked) {
      console.log("object");
      this.spinner.show();
      if (cp.controls.syllabusCoursewise.value == null || cp.controls.syllabusCoursewise.value.length == 0) {
        switch (this.examTracks.assessentCategory) {
          case 1:
            this._examService.getUnitListByCoursePlanner(cp.controls.uniqueId.value).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchSyllabusList(res.list, 1));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Unit not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
          case 2:
            this._examService.getChapterTopicListByCoursePlanner(cp.controls.uniqueId.value, 1).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchSyllabusList(res.list, 2));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Chapter not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
          case 4:
            this._examService.getSubtopicListByCoursePlanner(cp.controls.uniqueId.value).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchSyllabusList(res.list, 4));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Subtopic not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
          default:
            this._examService.getChapterTopicListByCoursePlanner(cp.controls.uniqueId.value, 2).subscribe(
              res => {
                if (res && res.success) {
                  cp.controls.syllabusCoursewise.setValue(this.patchSyllabusList(res.list, 3));
                  cp.controls.isSelected.setValue(true);
                } else {
                  this.toastService.ShowWarning("Topic not found");
                }
                console.log(res);
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                this.spinner.hide()
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.spinner.hide();
                //   this.toastService.ShowError(error);
                //   // this._commonService.ShowError(error.error.errorMessage);
                // }
              })
            break;
        }
      } else {
        this.spinner.hide();
        cp.controls.isSelected.setValue(true);
      }
    } else {
      cp.controls.isSelected.setValue(false);
      let subjectGroup = <FormGroup>this.subjectCpFormGroup;
      let syllabus = subjectGroup.controls.syllabus.value;
      let syllabusList = cp.controls.syllabusCoursewise.value;
      let syllabusIds = [];
      if (syllabus.length > 0 && syllabusList.length > 0) {
        syllabus.forEach((element, i) => {
          let isMatch = false;
          syllabusList.forEach(elem => {
            if (element == elem.id) {
              isMatch = true;
              return true;
            }
          });
          if (!isMatch) {
            syllabusIds.push(element);
          }

        });
        console.log(cp);
        subjectGroup.controls.syllabus.setValue(syllabusIds);
        cp.controls.selectedSyllabus.setValue([]);
      }
      console.log(cp);
      console.log(this.subjectCpFormGroup);
    }
  }
  patchSyllabusList(data, type) {
    let list = [];
    data.forEach((x, index) => {
      let dto = { id: x.id };
      switch (type) {
        case 1:
          dto['unitDetails'] = x.unitDetails;
          dto['name'] = x.unitDetails.name;
          dto['duration'] = x.unitDetails.duration;
          break;
        case 2:
          dto['chapterDetails'] = x.chapterDetails;
          dto['name'] = x.chapterDetails.name;
          dto['duration'] = x.chapterDetails.duration;
          break;
        case 4:
          if (x.subtopics) {
            x.subtopics.forEach(element => {
              dto['subTopicDetails'] = element.subTopicDetails;
              dto.id = element.id;
              dto['name'] = element.subTopicDetails.name;
              dto['duration'] = element.subTopicDetails.duration;
            });
          }
          break;
        default:
          if (x.topics) {
            x.topics.forEach(element => {
              dto['topicDetails'] = element.topicDetails;
              dto.id = element.id;
              dto['name'] = element.topicDetails.name;
              dto['duration'] = element.topicDetails.duration;
            });
          }
          break;
      }
      list.push(dto);
    });
    return list;
  }


  onSelectionChangeRemark($event: any, type: any) {
    console.log("type", type)
    console.log("event", event)

    var ele = document.getElementById(type);
    console.log("ele", ele)
    console.log("$event.target.checked", $event.target.checked)
    if ($event.target.checked) {
      ele.classList.remove("isHideOptionalRem");
      ele.classList.add("isUnhideOptionalRem");

    } else {
      ele.classList.remove("isUnhideOptionalRem");
      ele.classList.add("isHideOptionalRem");
    }

  }
  changeRemarks(event) {
    console.log(event);
    let subjectGroup = this.subjectCpFormGroup.value;
    subjectGroup.remarks = event.target.value;
  }
  changeCpSyllabus(event) {
    let subjectGroup = <FormGroup>this.subjectCpFormGroup;
    let syllabusIds: any = (subjectGroup.controls.syllabus.value) ? subjectGroup.controls.syllabus.value : [];

    event.forEach(x => {
      syllabusIds.push(x.id);
    });
    let unique = [...new Set(syllabusIds)];
    console.log(syllabusIds, event);
    let subjectGroup1 = this.subjectCpFormGroup.value;
    subjectGroup1.syllabus = unique;
    subjectGroup.controls.syllabus.setValue(unique);
  }
  patchList(data, type) {
    let list = [];
    data.forEach((x, index) => {

      let dto = { id: x.id };
      switch (type) {
        case 1:
          dto['unitDetails'] = x.unitDetails;
          dto['name'] = x.unitDetails.name;
          dto['duration'] = x.unitDetails.duration;
          break;
        case 2:
          dto['chapterDetails'] = x.chapterDetails;
          dto['name'] = x.chapterDetails.name;
          dto['duration'] = x.chapterDetails.duration;
          break;
        case 4:
          x.subtopics.forEach(element => {
            dto['subTopicDetails'] = element.subTopicDetails;
            dto.id = element.id;
            dto['name'] = element.subTopicDetails.name;
            dto['duration'] = element.subTopicDetails.duration;
          });
          break;
        default:
          x.topics.forEach(element => {
            dto['topicDetails'] = element.topicDetails;
            dto.id = element.id;
            dto['name'] = element.topicDetails.name;
            dto['duration'] = element.topicDetails.duration;
          });
          break;
      }
      list.push(dto);
    });
    return list;
  }
  getGradeList(courseUid, subjectUid) {
    this.spinner.show();
    this._examService.getGradePlanner(courseUid, subjectUid).subscribe(
      res => {
        if (res && res.success) {
          res.list.forEach(x => {
            x['isSelect'] = false;
          });
          this.gradeList = res.list;
          console.log(res.list);
        } else {
          this.toastService.ShowWarning(res.responseMessage);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error)
        // }
      }
    );

  }
  editTrackSyllbus(teacherTrackUid: string, subjectName: string, syllabusState: number, testType: number, examAssignmentSubjectId: number) {
    this.editTrack = true;
    // this.router.navigate(['/newModule/exam/track/syllabus', teacherTrackUid, subjectName, "YES", syllabusState, testType, examAssignmentSubjectId]);
  }
  goToQuestions(teacherTrackUid: string) {
    // this.examSetterService.changeMessage("sylabus ready")
    // this.router.navigate(['/exam/searchExam/9/track/pick/questions', teacherTrackUid]);
    this.router.navigate(['/newModule/exam/track/questionBank', teacherTrackUid]);
  }

  openModal(standardModal: string) {
    this.modalService.open(standardModal);
  }

  lockTrack() {
    this.spinner.show();
    let isValid = true;
    this.examTracks.trackSubjectList.forEach(x => {
      x.sectionList.forEach(x2 => {
        x2.trackList.forEach(element => {
          if (element.questionsAdded != element.totalQuestions) {
            isValid = false;
            console.log(isValid);
            return;
          }
        });
        if (!isValid) {
          return;
        }
      });
      if (!isValid) {
        return;
      }
    });
    if (!isValid) {
      this.toastService.ShowWarning("Please all question to all track, then save.");
      this.spinner.hide();
      return;
    }
    console.log(isValid);
    this._examService.lockExamTrack(this.examUid).subscribe(
      res => {
        if (res) {
          this.spinner.hide();
          this.toastService.showSuccess("Exam Track Locked Successfully");
          this.close.emit("");
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        //   this.spinner.hide();
        // }
      });
  }
  next() {
    // this.isSelectGrade = true;
  }
  cancel() {
    this.isSelectGrade = false;
    this.trackSyllabusPopup = false;
    this.isSelectPlanner = false;
    // this.close.emit("");
  }
  checkgrade(event, index) {
    console.log(event, this.gradeList);
    this.selectedGrade = [];
    this.trackSyllabusForm.controls.grade.setValue("");
    this.gradeList.forEach((x, i) => {
      if (i != index) {
        this.gradeList[i].isSelect = false;
      } else {
        this.gradeList[i].isSelect = (event.target.checked) ? true : false;
        if (event.target.checked) {
          this.trackSyllabusForm.controls.grade.setValue(x.name);
          this.selectedGrade = x.planners;
        }
      }
    })
    if (this.gradeList.length > 0) {
      this.getCoursePlannerData();
    }
  }
  getCoursePlannerData() {
    this.spinner.show();
    let cpUid = this.selectedGrade[0].uniqueId;
    this._cpService.getMasterCoursePlanner(cpUid).subscribe(
      res => {
        if (res) {

          this.plannerData = res;
          this.patchUnits(this.plannerData.units);
          this.spinner.hide();
          console.log(res);
        } else {
          this.toastService.ShowWarning("course planner data not found.");
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide()
      }
    );
  }
  patchUnits(data) {
    let list = [];
    data.forEach(x => {
      x['name'] = x.unitDetails.name
      list.push(x);
    });
    this.unitList = list;
    console.log(this.unitList);
  }
  changeUnits(event) {
    this.spinner.show();
    console.log(event);
    if (this.trackSyllabusForm.controls.chapter) {
      let list = [];
      event.forEach(x => {
        x.chapters.forEach(element => {
          if (element.chapterDetails) {
            element['name'] = element.chapterDetails.name
            list.push(element);
          }
        });
      });

      this.trackSyllabusForm.controls.chapter.setValue(null);
      if (this.trackSyllabusForm.controls.topic) {
        this.trackSyllabusForm.controls.topic.setValue(null);
      }
      if (this.trackSyllabusForm.controls.subTopic) {
        this.trackSyllabusForm.controls.subTopic.setValue(null);
      }
      this.chapterList = list;
    }
    this.spinner.hide();
  }
  changeChapters(event) {
    this.spinner.show();
    console.log(event);
    if (this.trackSyllabusForm.controls.topic) {
      let list = [];
      event.forEach(x => {
        x.topics.forEach(element => {
          if (element.topicDetails) {
            element['name'] = element.topicDetails.name
            list.push(element);
          }
        });
      });
      this.trackSyllabusForm.controls.topic.setValue(null);
      if (this.trackSyllabusForm.controls.subTopic) {
        this.trackSyllabusForm.controls.subTopic.setValue(null);
      }
      this.topicList = list;
    }
    this.spinner.hide();
  }
  changeTopics(event) {
    this.spinner.show();
    console.log(event);
    if (this.trackSyllabusForm.controls.subTopic) {
      let list = [];
      event.forEach(x => {
        x.subtopics.forEach(element => {
          if (element.subTopicDetails) {
            element['name'] = element.subTopicDetails.name
            list.push(element);
          }
        });
      });
      this.trackSyllabusForm.controls.subTopic.setValue(null);
      this.subTopicList = list;
    }
    this.spinner.hide();
  }
  goToQuetionBank() {
    console.log("syllabus", this.trackSyllabusForm);
    this.submitted = false;
    if (this.trackSyllabusForm.invalid) {
      return;
    }
    this.spinner.show();
    console.log("syllabus", this.trackSyllabusForm.value)
    let syllabusIds = [];
    if (this.syllabusId == 2 || this.syllabusId == 3) {
      switch (this.syllabusData.syllabusType) {
        case 1:
          syllabusIds = this.trackSyllabusForm.controls['unit'].value;
          break;
        case 2:
          syllabusIds = this.trackSyllabusForm.controls['chapter'].value;
          break;
        case 4:
          syllabusIds = this.trackSyllabusForm.controls['subTopic'].value;
          break;

        default:
          syllabusIds = this.trackSyllabusForm.controls['topic'].value;
          break;
      }
      if (this.syllabusId == 3) {
        let ids = this.trackSyllabusForm.controls['syllabusIds'].value;
        syllabusIds = syllabusIds.concat(ids);
      }

    } else {
      syllabusIds = this.trackSyllabusForm.controls['syllabusIds'].value;
    }
    const obj = {
      assessmentUid: this.examUid,
      subjectUid: this.subjectUniqueId,
      sectionUid: this.sectionDetails.uniqueId,
      trackUid: this.trackUniqueId,
      syllabusIds: syllabusIds
    }

    this.questionData = obj;
    this.assesmentService.addyllabusTrack(obj).subscribe(
      res => {
        if (res.success == true) {

          this.spinner.hide();
          this.trackSyllabusPopup = false;
          this.toQuestion = true;
          console.log('res', res)

        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide()
      }
    );
  }
  closeQuestion(event) {
    console.log(event);
    this.toQuestion = false;
    if (event == "2") {
      this.close.emit("");
    }
    this.getExamTracks(this.examUid);
  }
  closeTrack() {
    this.toQuestion = false;
    console.log("event");
    this.close.emit("cancel");
  }
  getImage(uniqueId, i, j, k) {
    this._userService.getImage("teacher", uniqueId, false).subscribe(
      response => {
        if (response != null && response) {
          if (response.image) {
            let trackSubjectList = this.examTracks.trackSubjectList[i];
            let section = trackSubjectList.sectionList[j];
            let track = section.trackList[k];
            track.teacherDto['imageCode'] = response.image;
          }
        }
      });
  }
  saveSyllabus() {
    let subjectSyllabus = this.subjectCpFormGroup.value;
    if (subjectSyllabus.syllabus && subjectSyllabus.syllabus.length == 0) {
      this.toastService.ShowWarning("Please select syllabus");
      return;
    }

    this.spinner.hide();
    let obj = {
      assessmentUid: this.examUid,
      subjectUid: subjectSyllabus.uniqueId,
      remarks: subjectSyllabus.remarks,
      syllabusIds: subjectSyllabus.syllabus
    }
    console.log(obj);
    this._examService.updateSubjectSyllyabus(obj).subscribe(
      res => {
        if (res && res.success == true) {

          this.spinner.hide();
          this.isSelectPlanner = false;
          console.log('res', res)
          this.toastService.showSuccess(res.responseMessage);
          this.getExamTracks(this.examUid);
        } else {
          this.toastService.ShowWarning(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide()
      }
    );
  }
  closeEditTrack(event) {
    this.editTrack = false;
  }
}
