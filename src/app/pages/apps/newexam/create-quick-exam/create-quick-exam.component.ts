import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ExamService } from '../exam.service';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import { HttpErrorResponse } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-quick-exam',
  templateUrl: './create-quick-exam.component.html',
  styleUrls: ['./create-quick-exam.component.scss']
})
export class CreateQuickExamComponent implements OnInit {

  isAction: boolean = true;
  isCreateQuickExam: boolean = false;
  quickExamForm: FormGroup;
  testTypeList: any[];
  minDate = new Date(new Date().setDate(new Date().getDate() - 1));
  userList = [];
  invigilatorForm: FormGroup;
  courseList: any[];
  batchList: any[];
  studentList: any[];
  subjectsList: any[];
  isCreatePatternExam: boolean = false;

  students = [];
  isSubmit: boolean = false;
  isChangeActionValue: any = "1";
  examPatternForm: FormGroup;
  examTypeList = [];
  jeeAdvanceQuestionType=[];
  customExamQuestionType=[];
  rightMarks: number;
  isFromEvent: boolean = false;
  parentIndex: number;
  childIndex: number;
  currentIndex: number;
  totalSubjects: number;
  selectedSubjectIds: any;
  imageSrc: string;
  isCreatePattern: boolean = false;
  isPatternCreate: boolean = false;
  subjects = [];
  examPatternType:any;
  examPatternList: [];
  isFirstNext: boolean = false;
  testTypeForm: FormGroup;
  toggle: Boolean = false;
  testTypeSubmitted: Boolean= false;
  coursePlanner = [];
  courseId: number;
  @ViewChild('wizardForm', { static: false }) wizard: BaseWizardComponent;
  type: any;
  
  constructor(private examService: ExamService,
              private spinner: NgxSpinnerService, 
              private fb: FormBuilder, 
              private toastService:ToastsupportService, 
              private modalService:NgbModal,
              private router:Router) {
  }

  ngOnInit() {
    this.initExamForm();
    this.initTestTypeForm();
    this.getTestTypeList();
    this.examService.subjectList.subscribe(
      (value: any) => {
        this.subjectsList = value;
      });

    this.examService.courses.subscribe(
      (value: any) => {
        this.courseList = value;
      });

    this.examService.batches.subscribe(
      (value: any) => {
        this.batchList = value;
      });

    this.examService.studentList.subscribe(
      (value: any) => {
        this.students = value;
      });

    console.log("this.quickExamForm",this.quickExamForm.controls,this.quickExamForm.value)
  }
  initExamForm() {
    this.quickExamForm = this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.required],
      examType: [1, Validators.required],
      examAssignmentTypeId: ['', Validators.required],
      pattern1TimingInfo: this.initTimeInfo(),
      batchInfo: this.initBatchInfo(),
      examSyllabus: this.initExamSubject(),
      marks: ['', Validators.required],
      passingMarks: ['', Validators.required],
      studentType: ['', Validators.required],
      studentList: this.fb.array([], Validators.required),
      organizationId: [sessionStorage.getItem('organizationId'), Validators.nullValidator],
      createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.nullValidator],
      isFromMobile: [false, Validators.required],
      // isObjective: [false, Validators.required],
    });
  }
  initTestTypeForm() {
    this.testTypeForm = this.fb.group({
      organizationId: [sessionStorage.getItem('organizationId'), Validators.nullValidator],
      createdUpdatedBy: [sessionStorage.getItem('userId'), Validators.nullValidator],
      isAssignment: [false, Validators.required],
      examAssignmentTypeList: this.fb.array([this.initTestType()], Validators.nullValidator)
    });
  }
  initTestType() {
    return this.fb.group({
      id: ['', Validators.nullValidator],
      name: ['', Validators.required]
    });
  }
  initBatchInfo() {
    return this.fb.group({
      courseId: [null, Validators.required],
      batchIds: [[], Validators.nullValidator],
    })
  }

  initTimeInfo() {
    return this.fb.group({
      testDate: ["", Validators.required],
      testTime: ["", Validators.required],
      extraTime: ['', Validators.nullValidator],
      testDuration: ['', Validators.required],
    })
  }
  initExamSubject() {
    return this.fb.group({
      subjects: this.fb.array([], Validators.required)
    })
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  getPassingMarks(value: number) {
    if (value > parseInt(this.quickExamForm.controls['marks'].value)) {
      this.quickExamForm.controls['passingMarks'].setValue('');
      // this._commonService.ShowWarning("Passing marks should be less than exam marks.");
    }
  }

  formSubmit() {
    this.wizard.navigation.goToNextStep();
  }

  omit_special_char(event: any) {
    let k = event.charCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45); // (k >= 48 && k <= 57) : allow numbers
  }
  radioButtonEvent(value: number) {
    const controlArray = <FormArray>this.quickExamForm.controls['studentList'];
    for (let i = controlArray.length - 1; i >= 0; i--) {
      controlArray.removeAt(i);
    }
    if (value == 1) {
      this.quickExamForm.setControl('studentList', this.fb.array(this.students));
    }
  }
  addDeleteStudentUid(values: any, user: any) {
    const controlArray = <FormArray>this.quickExamForm.controls['studentList'];
    if (values.currentTarget.checked) {
      controlArray.push(new FormControl(user));
    } else {
      let index = controlArray.controls.findIndex(x => x.value == user.id);
      controlArray.removeAt(index);
    }
  }
  selectedSubjects = [];
  addSubject(value: any) {

    console.log("value",value)
    let examSyllabus = <FormGroup>this.quickExamForm.controls['examSyllabus'];
    const controlArray = <FormArray>examSyllabus.controls['subjects'];
    controlArray.controls = [];
    this.selectedSubjects = [];
    if (value != null && value.length > 0) {
      this.selectedSubjects = value;
      for (let i = 0; i < value.length; i++) {
        controlArray.push(this.initSubjects(value[i]));
      }
    }

 
    // this.addActualSubjects(value, controlArray);
  }

  initSubjects(subject: any) {
    return this.fb.group({
      uniqueId: [subject.subjectId, Validators.nullValidator],
      id: [subject.id, Validators.required],
      subjectName: [subject.name, Validators.required],
      actualCoursePlanners: this.fb.array([], Validators.required),
      remarks: ['', Validators.nullValidator],
      syllabus: this.fb.array([], Validators.required),
      examAssignmentPatternSubjectId: ['', Validators.nullValidator],
      actualSyllabus: this.fb.array([], Validators.nullValidator),
    });
  }

  // addActualSubjects(subjectList: any, subjectArray: FormArray) {

  
  //   // console.log("subjectArray",subjectArray);

  //   // for (let i = 0; i < subjectArray.length; i++) {
  //   //   console.log("subjectArray.controls[i]",subjectArray.controls[i])
  //   //   console.log("subjectArray.controls[i]",subjectArray.controls[i])
      
  //   //   console.log("subjectArray.controls[i]",subjectArray.controls[i])
  //   //   let subjectGroup = <FormGroup>subjectArray.controls[i];
  //   //   let actualSubjectArray = <FormControl>subjectGroup.controls['subjectName'];
  //   //   console.log("subjectGroup",subjectGroup);
  //   //   for (let subject of subjectList) {
  //   //     subjectGroup.controls['subjectName']=subject.name;
  //   //     subjectGroup.controls['id']=subject.id
  //   //     subjectGroup.controls['uniqueId']=subject.subjectId
  //   //     // console.log("subject",subject);
  //   //     // actualSubjectArray.push(this.addSubjectToActualSubjects(subject));
  //   //   }
  //   // }
   
  // }


  // addSubjectToActualSubjects(data: any) {
  //   return this.fb.group({
  //     name: data.name,
  //     uniqueId: data.subjectId,
  //     id: data.id
  //   });
  // }

  public getCoursePlannerBySubject(subjectId: number, cpArray: FormArray) {
    console.log("couse api")
    this.coursePlanner = [];
    return this.examService.getCoursePlannerList(subjectId,this.courseId).subscribe(
      res => {
        if (res) {
          this.coursePlanner = res;
          cpArray.controls = [];
          
            for (let cp of res) {
              cpArray.push(this.addCoursePlanner(cp, false));
              console.log("cpArray",cpArray.length)
              
            }

            for(let j=0; j<cpArray.length; j++){
              
                let coursePlannerGroup = <FormGroup>cpArray.controls[j];
                console.log("coursePlannerGroup",coursePlannerGroup)
                console.log("subjectGroup.value['id']",coursePlannerGroup.value['id'])
              }
              console.log("value",this.quickExamForm.value)
        }


      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   // this.toasterService.ShowError(error.error.errorMessage);
        // }
      });
  }

  addCoursePlanner(data: any, isSelected: boolean) {
    console.log("couse group")
    return this.fb.group({
      id: data.id,
      name: data.name,
      isSelected: isSelected,
      syllabusCoursewise:this.fb.array([], Validators.nullValidator),
      selectedsyllabusCoursewise:this.fb.array([], Validators.nullValidator),
    });
  }

  openConfirm(content){
    this.modalService.open(content, { centered: true });
  }
 
   saveQuickExam() {
    this.quickExamForm.controls['examAssignmentTypeId'].setValue(this.quickExamForm.value.examAssignmentTypeId.toString());
    console.log("test exam 1",this.quickExamForm.value)
    return this.examService.addExam(this.quickExamForm.value).subscribe(
      res => {
        // this.spinner = false;
        // this.toasterService.showSuccess("Exam Added Successfully..");
     
        this.quickExamForm.reset();
        this.isChangeActionValue = "1"
        this.isAction = true;
        this.isCreateQuickExam = false;
        this.isCreatePatternExam = false;
           this.router.navigate(['/newModule/exam/examList']);
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   // this.spinner = false;
        //   // this.toasterService.ShowError(error.error.errorMessage);
        // }
      });

  }

  
  wizardFirstNext() {
    console.log("test exam 2", this.quickExamForm.value.examAssignmentTypeId.toString())
    this.isFirstNext=true;
    let batchDetails = <FormGroup>this.quickExamForm.controls['batchInfo'];
    this.courseId = batchDetails.controls['courseId'].value;
    let examSyllabus = <FormGroup>this.quickExamForm.controls['examSyllabus'];
    let subjectArray = <FormArray>examSyllabus.controls['subjects'];
    console.log("subjectArray",subjectArray," subjectArray.length",subjectArray.length)
    for (let i=0; i<subjectArray.length; i++) {
     
     
      let subjectGroup = <FormGroup>subjectArray.controls[i];
     
      let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
        this.getCoursePlannerBySubject(subjectGroup.value['id'],actualCPArray);
     
    }

    console.log("this.students",this.students)
    let studentListArry = <FormArray>this.quickExamForm.controls['studentList'];
    studentListArry.controls=[]
    for(let k=0;k < this.students.length;k++){
      studentListArry.push(new FormControl(this.students[k]));
    }

 
    
    let exam = this.quickExamForm.value;
    // if (exam.name != '' && exam.marks != '' && exam.passingMarks != '' && exam.examAssignmentTypeId != '' && exam.studentType != '' && exam.pattern1TimingInfo.testDuration != '' && exam.batchInfo.courseId != '') {

    // } else {
    //   // this._commonService.ShowWarning("Please fill all values..");
    //   console.log("show warning")
    //   return false;
    // }

    
    // if (subjectArray.length == 0) {
    //   // this._commonService.ShowWarning("Please select atleast one subject");
    //   console.log("show warning")
    //   return false;
    // }

    // let studentArray = <FormArray>this.quickExamForm.controls['studentList'];
    // if (studentArray.length == 0) {
    //   // this._commonService.ShowWarning("Please select atleast one student");
    //   console.log("show warning")
    //   return false;
    // }
  }

  wizardSecondNextStep() {
    let syllabusGroup = <FormGroup>this.quickExamForm.controls['examSyllabus'];
    let subjectArray = <FormArray>syllabusGroup.controls['subjects'];
    
      
      for (let l=0; l<subjectArray.length; l++) {
        
        let subjectGroup = <FormGroup>subjectArray.controls[l];
        let syllabusArray = <FormArray>subjectGroup.controls['syllabus'];
        let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
        for (let j=0; j<actualCPArray.length; j++) {
       
          let selectedsyllabusCoursewise = <FormGroup>actualCPArray.controls[j];
          let selectedsyllabus = <FormArray>selectedsyllabusCoursewise.controls['selectedsyllabusCoursewise'];
          for (let k=0; k<selectedsyllabus.length; k++) {
          
            console.log("selectedsyllabus",selectedsyllabus.controls[k].value);
            
            syllabusArray.push(new FormControl(selectedsyllabus.controls[k].value));
          }
        }
       
      }
      console.log("this.quickExamForm.",this.quickExamForm.value);
      // if (syllabusArray.length == 0) {
      //   // this.toasterService.ShowWarning("Please select syllabus");
      //   return false;
      // }
      // if (syllabusGroup.controls['boardUid'].value != 0 && syllabusGroup.controls['boardUid'].value != null && syllabusGroup.controls['boardUid'].value != '') {
      // } else {
      //   // this.toasterService.ShowWarning("Please select Board");
      //   return false;
      // }
      // if (syllabusGroup.controls['gradeUid'].value != 0 && syllabusGroup.controls['gradeUid'].value != null && syllabusGroup.controls['gradeUid'].value != '') {
      // } else {
      //   // this.toasterService.ShowWarning("Please select Grade");
      //   return false;
      // }
    
    this.lastNext();
  }
  exam: any;
  courseName: string;
  testType: string;
  batchName = [];
  lastNext() {
    this.exam = this.quickExamForm.value;
    console.log("exam",this.exam);
    let course = this.courseList.find(x => x.id == this.exam.batchInfo.courseId);
    this.courseName = course.name;
    console.log("this.testTypeList",this.testTypeList)
    let testType = this.testTypeList.find(x => x.id == this.exam.examAssignmentTypeId);
    this.testType = testType.name;

    if (this.exam.batchInfo.batchIds != '') {
      this.batchName = [];
      for (let i = 0; i < this.exam.batchInfo.batchIds.length; i++) {
        let batch = this.batchList.find(x => x.id == this.exam.batchInfo.batchIds[i]);
        this.batchName.push(batch.name);
      }
    }

    console.log("this.batchName",this.batchName)
    console.log("this.batchName",this.batchName.length)
    console.log("this.batchName",this.batchName[0])
  }
  checkExists(value: string, index: number) {
    if (this.testTypeList != null && this.testTypeList.length > 0) {
      let controlArray = <FormArray>this.testTypeForm.controls['examAssignmentTypeList'];
      for (let type of this.testTypeList) {
        if (type.name.toUpperCase() == value.toUpperCase()) {
          this.toastService.ShowWarning("Name already added");
          let formGroup = <FormGroup>controlArray.controls[index];
          formGroup.controls['name'].setValue('');
          return false;
        }
      }
    }
  }

  addNewTestType(TestTypeList: any) {
    const control = <FormArray>this.testTypeForm.controls['examAssignmentTypeList'];
    control.push(this.initTestType());
  }
  togleType() {
    this.toggle = !this.toggle;
  }
  addtestType() {
    this.testTypeSubmitted=true;
    if(this.testTypeForm.invalid){
      return;
    }
    let examAssignmentType = this.testTypeForm.value;
    if (examAssignmentType.examAssignmentTypeList != null && examAssignmentType.examAssignmentTypeList.length > 0) {
      for (let i = 0; i < examAssignmentType.examAssignmentTypeList.length; i++) {
        if (this.is_special_char(examAssignmentType.examAssignmentTypeList[i].name)) {
          this.toastService.ShowWarning("Special charachters are not allowed..");
          return false;
        }
      }
      // this.spinner = true;
      this.spinner.show()
      return this.examService.addTestType(examAssignmentType).subscribe(
        res => {
          this.testTypeList = [];
          this.testTypeList = res;
          if (res.length>0) {
            res.map(type=>{
              if (type.name==examAssignmentType.examAssignmentTypeList[0].name) {
                this.selectedTestType=type.id
              }
            })
          }
          this.resetTestType();
          this.toggle = false;
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
          // } else {
          //   // this.spinner = false;
          //   this.spinner.hide();
          //   this.toastService.ShowError(error.error.errorMessage);
          // }
          this.spinner.hide();
        });
    }
  }
  resetTestType() {
    const controlArray = <FormArray>this.testTypeForm.controls['examAssignmentTypeList'];
    for (let i = controlArray.length - 1; i >= 0; i--) {
      controlArray.removeAt(i);
    }
    controlArray.push(this.initTestType());
  }
  is_special_char(value: string) {
    let pattern = new RegExp(/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/);
    return pattern.test(value);
  }
  getTestTypeList() {
    this.spinner.show();
    return this.examService.getTestTypeList(sessionStorage.getItem('organizationId'), false).subscribe(
      res => {
        console.log("res", res)
        if (res) {
          this.testTypeList = [];
          this.testTypeList = res;
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error.error.errorMessage);
        //   // this._commonService.ShowError(error.error.errorMessage);
        // }
        this.spinner.hide();
      })
  }
  selectedTestType="";
  testTypeChange(event){
    this.selectedTestType=event.id;
    console.log("event",event);
    
  }

  

}
