import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-user-basic-form',
  templateUrl: './user-basic-form.component.html',
  styleUrls: ['./user-basic-form.component.scss']
})
export class UserBasicFormComponent implements OnInit {

  // basicFormGroup: FormGroup;
  currentUserId: Number = null;
  userData: any;
  fileSelected = false
  loaderBasicInfo = false
  loding = false;
  studentBasicInfo: any;
  setStudListDivCollapsed = false;
  imageSrc: any;
  studentProfileImageForm: FormGroup;
  submitted = false;
  tabNumber: any = 1;
  selectedFiles: FileList;
  currentFileUpload: File;
  imageLoaded: boolean = false;
  imageError: string;
  organizationList = [];
  orgList = [];
  roleInfo = [];
  userType = ["", "student", "staff", "teacher", "guardian"]
  userEmail = "";
  userMobile = "";
  selectedRole: any = [];
  isBasicUpdate = false;
  @Input() userId: Number;
  @Input() userDetailsType: any;
  @Input() roleData: any;
  @Input() basicFormGroup: FormGroup;
  @Input() userUniqueId: any;
  @Output() nextTab = new EventEmitter<string>();

  constructor(private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private _userService: UserServiceService, private datePipe: DatePipe) {
    this.userData = this._commonService.getUserData();
    this.currentUserId = 114;
    this.currentUserId = this.userId;

    var tabNo = localStorage.getItem("selectedTab");
    this.tabNumber = (tabNo) ? tabNo : 1;
  }

  ngOnInit() {
    this.currentUserId = this.userId;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['userId'] && changes['userDetailsType'].currentValue) {
      this.currentUserId = this.userId;
      this.selectedRole = [];
      let basicData = this.basicFormGroup.value;
      this.userEmail = basicData.generalInfo.emailId;
      this.userMobile = basicData.generalInfo.phoneNumber;
      if (this.roleData) {
        this.selectedRole.push(this.roleData.role.uniqueId)
        this.roleInfo = (this.roleData.roleCombinations) ? this.roleData.roleCombinations : "";
      }
      switch (this.userDetailsType) {
        case 1:
          // this.getChildOrganizationList(this.userData.orgUniqueId);
          break;
        case 2:
          break;
        case 3:
          // this.getChildOrganizationList(this.userData.orgUniqueId);
          break;
        case 4:
          break;
      }
    }
  }
  get f() { return this.basicFormGroup.controls; }

  get gf() {
    let generalInfo = <FormGroup>this.basicFormGroup.controls['generalInfo'];
    return generalInfo.controls;
  }


  saveStudentBasicInfo() {
    this.submitted = true;
    if (this.basicFormGroup.invalid) {
      // this.scrollToError();
      return;
    }
    this.spinner.show();
    let basicData = this.basicFormGroup.value;
    let usertype = this.userType[this.userDetailsType];
    let basicdto = {
      firstName: basicData.generalInfo.firstName,
      lastName: basicData.generalInfo.lastName,
      gender: basicData.generalInfo.gender,
      usercode: (basicData.usercode) ? basicData.usercode : "",
      userDate: "",
      roles: []
    }
    let roles = [];
    roles.push(this.roleData.role);

    if (this.userDetailsType == 2 || this.userDetailsType == 3) {
      basicdto.userDate = this.datePipe.transform(basicData.generalInfo.dateOfJoining, "yyyy-MM-dd'T'HH:mm:ss");
      for (let i = 0; i < this.roleData.roleCombinations.length; i++) {
        const element = this.roleData.roleCombinations[i];
        if (basicData.roleId.includes(element.uniqueId)) {
          roles.push(element);
        }
      }
    } else if (this.userDetailsType == 1) {
      basicdto.userDate = this.datePipe.transform(basicData.enrollmentDate, "yyyy-MM-dd'T'HH:mm:ss")
    }
    basicdto.roles = roles;
    let emaildto = { emailId: basicData.generalInfo.emailId };
    let mobiledto = { mobile: basicData.generalInfo.phoneNumber };
    if (this.userEmail != emaildto.emailId) {
      this._userService.updateUserEmailId(usertype, this.userUniqueId, emaildto).subscribe(
        response1 => {
          this.toastService.showSuccess("Email update successfully.");
          this.spinner.hide();
          if (this.userMobile != mobiledto.mobile) {
            this.mobileDataUpdate(usertype, basicdto, basicData, mobiledto)
          } else {
            this.basicDataUpdate(usertype, basicdto, basicData)
          }
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          // } else {
          //   this.spinner.hide();
          //   this.toastService.ShowWarning(error);
            
          // }
          this.isBasicUpdate = false
        });
    } else {
      if (this.userMobile != mobiledto.mobile) {
        this.mobileDataUpdate(usertype, basicdto, basicData, mobiledto)
      } else {
        this.basicDataUpdate(usertype, basicdto, basicData)
      }
    }


  }
  mobileDataUpdate(usertype, basicdto, basicData, mobiledto) {
    this._userService.updateUserMobile(usertype, this.userUniqueId, mobiledto).subscribe(
      response2 => {
        this.toastService.showSuccess("Mobile update successfully.");
        this.spinner.hide();
        this.basicDataUpdate(usertype, basicdto, basicData)
        this.isBasicUpdate = true
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide();
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        
        // }
        this.isBasicUpdate = false
      });
  }
  basicDataUpdate(usertype, basicdto, basicData) {
    this.spinner.show();
    this._userService.updateUserBasicInfo(usertype, this.userUniqueId, basicdto).subscribe(
      response => {
        let type = this.capitalize(usertype);
        this.toastService.showSuccess(type + " Basic info saved successfully.");
        if (this.userDetailsType != 4) {
          this.nextTab.emit("2")
        } else {
          this.nextTab.emit("7")
        }
        this.submitted = false;
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.submitted = false;
        // } else {
        //   this.toastService.ShowError(error);
        //   this.submitted = false;
        // }
        this.submitted = false;
        this.spinner.hide();
      });
    if (this.userDetailsType == 4) {
      let dto = {
        address: {
          countryId: 0,
          address: "",
          stateId: 0,
          city: null,
          zipcode: ""
        },
        bloodGroup: basicData.generalInfo.bloodGroup,
        birthDate: null,
        adharNumber: basicData.generalInfo.adharNumber
      };
      this._userService.updateUserOtherInfo(usertype, this.userUniqueId, dto).subscribe(
        response => {

        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   this.loding = false;
          // } else {
          //   this.toastService.ShowError(error);
          //   this.loding = false;
          // }
          this.loding = false;
          this.spinner.hide();
        }
      );
    }
  }

  close() {
    this._commonService.iscloseClicked.next(true)
  }
  capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }
}
