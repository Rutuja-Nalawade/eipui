import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';

@Component({
  selector: 'app-assign-period',
  templateUrl: './assign-period.component.html',
  styleUrls: ['./assign-period.component.scss']
})
export class AssignPeriodComponent implements OnInit {
  teacherPeriodForm: FormGroup;
  selectedSubjectList = [];
  workingDaysLists: any;
  saveState: number = 0;
  userData: any;
  slotData = {};
  slotListss = [];
  PeriodSetting: {};
  // teacherPeriodData = [];
  settings = {};
  currentUserId;
  isAddLecture = false;
  isEditLecture = false;
  type = 2;
  shiftPeriodData: any = {};
  selectedShiftPeriodData = [];
  selectedIndexOfPeriod = ""
  selectedDayIndex: any = ""
  currentShitUid: any = ""
  shiftList = [];
  currentIndex = 0;
  assignPeriodData = [];
  isAssignPeriod = true;
  selectedLectureId = "";
  days = ["", "Sunday", "Monday", "Tuesday", "Wedesday", "Thursady", "Friday", "Saturday"];
  @Input() timetableUniqueId: any;
  @Input() batchData: any;
  @Output() closeForm = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private timeTableService: TimeTableServiceService, private toastService: ToastsupportService) {
  }

  ngOnInit() {
    // this.getUserPeriods(56)
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['batchData'] && changes['batchData'].currentValue) {
      this.currentShitUid = this.batchData.shiftLists[0].unique_id;
      this.shiftList = this.batchData.shiftLists;
      this.getPeriodData();
    }
  }
  getPeriodData() {
    let dto =
    {
      "batchId": this.batchData.uniqueId,
      "shiftId": this.currentShitUid,
      "timetableId": this.timetableUniqueId
    }
    this.spinner.show();
    this.timeTableService.getPeriodList(dto).subscribe(
      res => {
        if (res) {
          this.shiftPeriodData = res;

          this.selectedShiftPeriodData = res.periodsList;
          for (let i = 0; i < res.periodsList.length; i++) {
            let mon = { isSelected: false, isAssigned: false, isDisabled: (res.shiftHasDaysList.includes(2)) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 2, periodLectures: [] };
            let tue = { isSelected: false, isAssigned: false, isDisabled: (res.shiftHasDaysList.includes(3)) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 3, periodLectures: [] };
            let wed = { isSelected: false, isAssigned: false, isDisabled: (res.shiftHasDaysList.includes(4)) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 4, periodLectures: [] };
            let thu = { isSelected: false, isAssigned: false, isDisabled: (res.shiftHasDaysList.includes(5)) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 5, periodLectures: [] };
            let fri = { isSelected: false, isAssigned: false, isDisabled: (res.shiftHasDaysList.includes(6)) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 6, periodLectures: [] };
            let sat = { isSelected: false, isAssigned: false, isDisabled: (res.shiftHasDaysList.includes(7)) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 7, periodLectures: [] };
            let sun = { isSelected: false, isAssigned: false, isDisabled: (res.shiftHasDaysList.includes(1)) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 1, periodLectures: [] };
            const element2 = res.periodsList[i];
            if (element2.dayWiseSlots.length > 0) {
              for (let j = 0; j < element2.dayWiseSlots.length; j++) {
                const element = element2.dayWiseSlots[j];
                let periodLectures = (element.periodLectures) ? element.periodLectures : [];
                switch (element.dayValue) {
                  case 1:
                    sun = { isSelected: (element.isSelected || (element.periodLectures && element.periodLectures.length > 0)) ? true : false, isAssigned: (element.periodLectures && element.periodLectures.length > 0) ? true : false, isDisabled: (element.orgWorkingDayUniqueId) ? sun.isDisabled : true, uniqueId: element.uniqueId, orgWorkingDayUniqueId: element.orgWorkingDayUniqueId, dayValue: 1, periodLectures: periodLectures }
                    break;
                  case 2:
                    mon = { isSelected: (element.isSelected || (element.periodLectures && element.periodLectures.length > 0)) ? true : false, isAssigned: (element.periodLectures && element.periodLectures.length > 0) ? true : false, isDisabled: (element.orgWorkingDayUniqueId) ? mon.isDisabled : true, uniqueId: element.uniqueId, orgWorkingDayUniqueId: element.orgWorkingDayUniqueId, dayValue: 2, periodLectures: periodLectures }
                    break;
                  case 3:
                    tue = { isSelected: (element.isSelected || (element.periodLectures && element.periodLectures.length > 0)) ? true : false, isAssigned: (element.periodLectures && element.periodLectures.length > 0) ? true : false, isDisabled: (element.orgWorkingDayUniqueId) ? tue.isDisabled : true, uniqueId: element.uniqueId, orgWorkingDayUniqueId: element.orgWorkingDayUniqueId, dayValue: 3, periodLectures: periodLectures }
                    break;
                  case 4:
                    wed = { isSelected: (element.isSelected || (element.periodLectures && element.periodLectures.length > 0)) ? true : false, isAssigned: (element.periodLectures && element.periodLectures.length > 0) ? true : false, isDisabled: (element.orgWorkingDayUniqueId) ? wed.isDisabled : true, uniqueId: element.uniqueId, orgWorkingDayUniqueId: element.orgWorkingDayUniqueId, dayValue: 4, periodLectures: periodLectures }
                    break;
                  case 5:
                    thu = { isSelected: (element.isSelected || (element.periodLectures && element.periodLectures.length > 0)) ? true : false, isAssigned: (element.periodLectures && element.periodLectures.length > 0) ? true : false, isDisabled: (element.orgWorkingDayUniqueId) ? thu.isDisabled : true, uniqueId: element.uniqueId, orgWorkingDayUniqueId: element.orgWorkingDayUniqueId, dayValue: 5, periodLectures: periodLectures }
                    break;
                  case 6:
                    fri = { isSelected: (element.isSelected || (element.periodLectures && element.periodLectures.length > 0)) ? true : false, isAssigned: (element.periodLectures && element.periodLectures.length > 0) ? true : false, isDisabled: (element.orgWorkingDayUniqueId) ? fri.isDisabled : true, uniqueId: element.uniqueId, orgWorkingDayUniqueId: element.orgWorkingDayUniqueId, dayValue: 6, periodLectures: periodLectures }
                    break;
                  case 7:
                    sat = { isSelected: (element.isSelected || (element.periodLectures && element.periodLectures.length > 0)) ? true : false, isAssigned: (element.periodLectures && element.periodLectures.length > 0) ? true : false, isDisabled: (element.orgWorkingDayUniqueId) ? sat.isDisabled : true, uniqueId: element.uniqueId, orgWorkingDayUniqueId: element.orgWorkingDayUniqueId, dayValue: 7, periodLectures: periodLectures }
                    break;

                }
              }
            } else {
              mon.isDisabled = true;
              tue.isDisabled = true;
              wed.isDisabled = true;
              thu.isDisabled = true;
              fri.isDisabled = true;
              sat.isDisabled = true;
              sun.isDisabled = true;
            }
            this.selectedShiftPeriodData[i]['mon'] = mon
            this.selectedShiftPeriodData[i]['tue'] = tue
            this.selectedShiftPeriodData[i]['wed'] = wed
            this.selectedShiftPeriodData[i]['thu'] = thu
            this.selectedShiftPeriodData[i]['fri'] = fri
            this.selectedShiftPeriodData[i]['sat'] = sat
            this.selectedShiftPeriodData[i]['sun'] = sun
          }
          this.batchData.shiftLists[this.currentIndex].isPeriodAssigned = true;
          this.batchData.shiftLists[this.currentIndex]['periodData'] = res.periodsList;
          this.spinner.hide();
        } else {
          this.spinner.hide();
          this.selectedShiftPeriodData = [];
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
      });
  }
  lectureClick(item) {
    console.log(item);
    this.selectedLectureId = item.lectureId;
  }
  selectPeriod(isDisabled, period, j, dayindex, day, lectureId) {
    if (!isDisabled) {

      let selectedPeriod = this.selectedShiftPeriodData[j]
      this.slotData = {
        periodId: period.uniqueId,
        periodName: period.name,
        day: day.orgWorkingDayUniqueId,
        shiftId: this.currentShitUid,
        dayValue: day.dayValue,
        dayWiseSlots: period.dayWiseSlots,
        dayName: this.days[day.dayValue]
      }
      this.selectedDayIndex = dayindex;
      this.selectedIndexOfPeriod = j;
      console.log(day.isAssigned);
      if (day.isAssigned) {

        this.selectedLectureId = lectureId;
        this.type = 4;
        this.isEditLecture = true;

      } else if (day.isSelected) {
        this.type = 2;
        this.isAddLecture = true;

      } else {
        switch (dayindex) {
          case 1:
            selectedPeriod.mon = { isDisabled: false, isAssigned: false, isSelected: (day.selected) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 2, periodLectures: [] };
            break;
          case 2:
            selectedPeriod.tue = { isDisabled: false, isAssigned: false, isSelected: (day.selected) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 3, periodLectures: [] };
            break;
          case 3:
            selectedPeriod.wed = { isDisabled: false, isAssigned: false, isSelected: (day.selected) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 4, periodLectures: [] };
            break;
          case 4:
            selectedPeriod.thu = { isDisabled: false, isAssigned: false, isSelected: (day.selected) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 5, periodLectures: [] };
            break;
          case 5:
            selectedPeriod.fri = { isDisabled: false, isAssigned: false, isSelected: (day.selected) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 6, periodLectures: [] };
            break;
          case 6:
            selectedPeriod.sat = { isDisabled: false, isAssigned: false, isSelected: (day.selected) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 7, periodLectures: [] };
            break;
          case 7:
            selectedPeriod.sun = { isDisabled: false, isAssigned: false, isSelected: (day.selected) ? false : true, uniqueId: "", orgWorkingDayUniqueId: "", dayValue: 1, periodLectures: [] };
            break;
        }
        this.assignPeriod(period, day);

      }
    }
  }
  assignPeriod(period, day) {
    this.spinner.show();
    let dto = {
      "batchId": this.batchData.uniqueId,
      "shiftId": this.currentShitUid,
      "shiftHasDaysList": this.shiftPeriodData.shiftHasDaysList,
      "periodsList": [{
        name: period.name,
        uniqueId: period.uniqueId,
        dayWiseSlots: [{
          dayValue: day.dayValue,
          isSelected: true
        }]
      }]
    }
    this.timeTableService.assignBatchPeriod(dto).subscribe(
      (res) => {
        if (res) {
          this.toastService.showSuccess("Periods assigned to batch succesfully");
          this.spinner.hide();
          this.type = 2;
          this.isAddLecture = true;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  close() {
    this.closeForm.emit("")
  }
  submitTeacherPeriodInfo() {
    console.log(this.assignPeriodData);
    this.isAssignPeriod = false;
    return;
    this.spinner.show();
    // if (this.assignPeriodData[0].periodList) {

    // }
    this.timeTableService.assignBatchPeriod(this.assignPeriodData[0]).subscribe(
      (res) => {
        if (res) {
          this.toastService.showSuccess("Assign Periods to batch save succesfully");
          this.isAssignPeriod = false;
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
    // this.closeForm.emit("")
  }

  closeAddLectureForm(event) {
    console.log(event, event.subjectName);
    let resDto = JSON.parse(event)
    console.log(typeof resDto);
    if (this.isAddLecture && resDto != "" && resDto != 2) {
      let selectedPeriod = this.selectedShiftPeriodData[this.selectedIndexOfPeriod];
      switch (this.selectedDayIndex) {
        case 1:
          selectedPeriod.mon.periodLectures = [{ subjectName: resDto.subjectName, lectureId: resDto.lectureId }];
          selectedPeriod.mon.isAssigned = true;
          break;
        case 2:
          selectedPeriod.tue.periodLectures = [{ subjectName: resDto.subjectName, lectureId: resDto.lectureId }];
          selectedPeriod.tue.isAssigned = true;
          break;
        case 3:
          selectedPeriod.wed.periodLectures = [{ subjectName: resDto.subjectName, lectureId: resDto.lectureId }];
          selectedPeriod.wed.isAssigned = true;
          break;
        case 4:
          selectedPeriod.thu.periodLectures = [{ subjectName: resDto.subjectName, lectureId: resDto.lectureId }];
          selectedPeriod.thu.isAssigned = true;
          break;
        case 5:
          selectedPeriod.fri.periodLectures = [{ subjectName: resDto.subjectName, lectureId: resDto.lectureId }];
          selectedPeriod.fri.isAssigned = true;
          break;
        case 6:
          selectedPeriod.sat.periodLectures = [{ subjectName: resDto.subjectName, lectureId: resDto.lectureId }];
          selectedPeriod.sat.isAssigned = true;
          break;
        case 7:
          selectedPeriod.sun.periodLectures = [{ subjectName: resDto.subjectName, lectureId: resDto.lectureId }];
          selectedPeriod.sun.isAssigned = true;
          break;
      }
    }
    if (this.isEditLecture && resDto == 3) {
      let selectedPeriod = this.selectedShiftPeriodData[this.selectedIndexOfPeriod];
      switch (this.selectedDayIndex) {
        case 1:
          selectedPeriod.mon.periodLectures = this.getperiodLecturesList(selectedPeriod.mon.periodLectures);
          selectedPeriod.mon.isAssigned = false;
          break;
        case 2:
          selectedPeriod.tue.periodLectures = this.getperiodLecturesList(selectedPeriod.tue.periodLectures);
          selectedPeriod.tue.isAssigned = false;
          break;
        case 3:
          selectedPeriod.wed.periodLectures = this.getperiodLecturesList(selectedPeriod.wed.periodLectures);
          selectedPeriod.wed.isAssigned = false;
          break;
        case 4:
          selectedPeriod.thu.periodLectures = this.getperiodLecturesList(selectedPeriod.thu.periodLectures);
          selectedPeriod.thu.isAssigned = false;
          break;
        case 5:
          selectedPeriod.fri.periodLectures = this.getperiodLecturesList(selectedPeriod.fri.periodLectures);
          selectedPeriod.fri.isAssigned = false;
          break;
        case 6:
          selectedPeriod.sat.periodLectures = this.getperiodLecturesList(selectedPeriod.sat.periodLectures);
          selectedPeriod.sat.isAssigned = false;
          break;
        case 7:
          selectedPeriod.sun.periodLectures = this.getperiodLecturesList(selectedPeriod.sun.periodLectures);
          selectedPeriod.sun.isAssigned = false;
          break;
      }
    }
    if (this.isEditLecture && event == "4") {
      this.getPeriodData();
    }
    this.isAddLecture = false;
    this.isEditLecture = false;
  }
  getperiodLecturesList(data) {
    let lectures = []
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      if (element.lectureId != this.selectedLectureId) {
        lectures.push(element)
      }
    }
    return lectures;
  }
  onSelectTab(event, index) {
    // index = index - 1;
    console.log(index, event, this.assignPeriodData, this.batchData);
    this.currentIndex = index;
    this.currentShitUid = this.batchData.shiftLists[index].unique_id;
    if (this.batchData.shiftLists[index].isPeriodAssigned) {
      this.selectedShiftPeriodData = this.batchData.shiftLists[index].periodData
    } else {
      this.getPeriodData()
    }
  }
}
