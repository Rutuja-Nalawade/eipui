import { Component, OnInit } from '@angular/core';
import SockJS from 'sockjs-client';
import { Stomp } from '@stomp/stompjs';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthServiceService } from './servicefiles/auth-service.service';

@Component({
  selector: 'app-ubold',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  stompClient: any;
  constructor(private router: Router, private translate: TranslateService, private authService: AuthServiceService) {
    translate.setDefaultLang('en');
    translate.use('en');
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    //this.translate.use('en');
    this.setThemeOnStart();
  }

  ngOnInit() {

    this.authService.isDomainChanged.subscribe(
      (value: any) => {

      }
    );
    //this.connect(this.router);
  }

  connect(router: Router) {
    const socket = new SockJS(environment.websocketUrl);
    this.stompClient = Stomp.over(socket);

    const _this = this;
    this.stompClient.connect({}, function (frame) {
      console.log('Connected: ' + frame);

      _this.stompClient.subscribe('/notifyToSubscribe', function (hello) {
        // console.log(hello.body);
        let responseJson: any = JSON.parse(hello.body);
        if (sessionStorage.getItem("orgUniqueId") == responseJson.orgUniqueId) {
          sessionStorage.removeItem('currentUser');
          sessionStorage.removeItem('tokenId');
          sessionStorage.removeItem('currentUser');
          sessionStorage.removeItem('uniqueId');
          sessionStorage.removeItem('orgUniqueId');
          sessionStorage.removeItem('organizationId');
          sessionStorage.removeItem('organizationName');
          sessionStorage.removeItem('parentOrgUniqueId');
          sessionStorage.removeItem('parentOrgId');
          sessionStorage.removeItem('lastLoginTime');
          sessionStorage.clear();
          router.navigate(['/account/login']);
        }
      });
    });
  }
  setThemeOnStart() {
    document.body.classList.add('theme1');
  }
}
