import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { AssesmentService } from 'src/app/servicefiles/assesment.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { DomSanitizer } from '@angular/platform-browser';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-examview',
  templateUrl: './examview.component.html',
  styleUrls: ['./examview.component.scss']
})
export class ExamviewComponent implements OnInit {

  // @Input() exam: any;
  @Input() quickExamForm: FormGroup;
  @Input() courseName: any;
  //    @Input() gradeName : any; 
  examData: any;
  @Input() batchName: any;
  @Input() assignmentTypeId: any;
  //    @Input() boardName : any; 
  @Input() testType: string;
  @Output() changeStep = new EventEmitter<string>();
  @Output() close = new EventEmitter<any>();

  isTenStud = true;
  examTime = '';
  examName: any = '';
  exam: any = null;
  @Input() isExamViewFromList: any;
  @Input() examUid: any;
  isEditData: boolean;
  isEditStudent: boolean = false;
  studentList = [];
  isExamSummaryList: any;
  studInfo = {};

  constructor(private modalService: NgbModal, private datePipe: DatePipe,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private _examService: AssesmentService,
    private toastService: ToastsupportService,
    private _DomSanitizationService: DomSanitizer, private _userService: UserServiceService) { }
  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes, this.isExamViewFromList);

    if (this.isExamViewFromList == false) {

      this.exam = this.quickExamForm.value;

      console.log('exam', this.exam);
      this.examTime = '';

      let date = new Date();
      let dateformat = moment(date).format("yyyy/mm/dd");
      let timeformat = this.convertTime(this.exam.timingDetailsDto.assessmentTime);
      let time = this.exam.timingDetailsDto.assessmentTime.split(" ");
      console.log(dateformat, this.exam.timingDetailsDto.assessmentTime, time, new Date('' + this.exam.timingDetailsDto.assessmentTime));
      let splitTimeArry = timeformat.split(":");
      let hr = splitTimeArry[0];
      let min = splitTimeArry[1];
      date.setHours(+hr);
      date.setMinutes(+min);
      let newdatetime = date;
      let duration = (this.assignmentTypeId == 2) ? this.exam.duration : this.exam.timingDetailsDto.duration;
      console.log(newdatetime, duration, typeof this.exam.timingDetailsDto.duration);
      var timelater = new Date(newdatetime.getTime() + (parseInt(duration) * 60 * 1000));
      // console.log(this.getTimeOnly(timelater));
      this.examTime = this.getTimeOnly(newdatetime) + '-' + this.getTimeOnly(timelater);
      console.log(this.examTime);
    } else {
      this.getExamBasicInfo()
      this.getExamSyllabus()

    }

  }
  showAll() {
    this.isTenStud = false;
  }

  editExamName(content) {
    this.examName = this.exam.name;
    this.modalService.open(content, { windowClass: "modalEditName", backdrop: 'static' });
  }
  getTimeOnly(time) {
    return this.datePipe.transform(time, ' h:mm a');
  }
  edit(type) {
    this.changeStep.emit(type.toString());
  }

  closePopup() {
    this.quickExamForm.value.name = this.examName;

  }

  getExamBasicInfo() {


    this.spinner.show();
    this._examService.getExamBasicInfo(this.examUid).subscribe(
      (res: any) => {
        if (res) {


          console.log('test', res);

          this.getExamSyllabus()
          this.examData = res;
          this.testType = res.assessmentCategoryDto.name;
          this.courseName = res.course.name;
          console.log('test', res);
          let timeData = moment(this.examData.timingDetailsDto.assessmentTime).format('HH:mm');
          console.log(timeData, moment(this.examData.timingDetailsDto.assessmentTime).format('HH:mm'));
          let date = new Date();
          let time = timeData;
          let splitTimeArry = time.split(":");
          let hr = splitTimeArry[0];
          let min = splitTimeArry[1];
          date.setHours(+hr);
          date.setMinutes(+min);
          let newdatetime = date;
          let duration = (this.examData.timingDetailsDto.duration) ? this.examData.timingDetailsDto.duration : 0;
          console.log(newdatetime, duration, typeof this.examData.timingDetailsDto.duration);
          var timelater = new Date(newdatetime.getTime() + (parseInt(duration) * 60 * 1000));
          this.examTime = this.getTimeOnly(newdatetime) + '-' + this.getTimeOnly(timelater);
          console.log('examTime', this.examTime);
          this.spinner.hide();
          this.getExamStudent();




        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.toastService.ShowWarning(error.error);
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }

      });
  }

  getExamSyllabus() {
    let subjectList = [];
    this.spinner.show();
    this._examService.getExamSyllabuisInfo(this.examUid).subscribe(
      (res: any) => {
        if (res) {

          console.log('res', res);
          res.list.forEach(element => {
            let obj = { subjectName: '', remarks: '', actualSyllabus: [] }
            obj.subjectName = element.subjectDto.name;
            if (element.isSyllabusSelected) {
              switch (element.syllabusType) {
                case 1:
                  element.unitPlannerList.forEach(element1 => {
                    element1.units.forEach(element3 => {
                      let obj1 = { name: '' };
                      obj1.name = element3.unitDetails.name;
                      obj.actualSyllabus.push(obj1);
                    });
                  });
                  break;
                case 2:
                  element.chapterPlannerList.forEach(element1 => {
                    element1.chapters.forEach(element3 => {
                      let obj1 = { name: '' };
                      obj1.name = element3.chapterDetails.name;
                      obj.actualSyllabus.push(obj1);
                    });
                  });
                  break;
                case 3:
                  element.unitList.forEach(element1 => {
                    let obj1 = { name: '' };
                    obj1.name = element1.unitDetails.name;
                    obj.actualSyllabus.push(obj1);
                  });
                  break;
                case 4:
                  element.unitList.forEach(element1 => {
                    let obj1 = { name: '' };
                    obj1.name = element1.unitDetails.name;
                    obj.actualSyllabus.push(obj1);
                  });
                  break;

                default:
                  element.unitList.forEach(element1 => {
                    let obj1 = { name: '' };
                    obj1.name = element1.unitDetails.name;
                    obj.actualSyllabus.push(obj1);
                  });
                  break;
              }
            }

            subjectList.push(obj);
          });
          this.examData.subjectList = subjectList;
          this.getExamStudent();
          console.log('subjectList', subjectList);
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.toastService.ShowWarning(error.error);

        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }

      });
  }
  getExamStudent() {
    //getAssesmentStudentInfo
    this.spinner.show();
    this._examService.getAssesmentStudentInfo(this.examUid).subscribe(
      (res: any) => {
        if (res) {
          let students = [];
          let batchUids = [];
          this.batchName = [];
          console.log('test', res);
          this.studentList = res.list;
          res.list.forEach(element => {
            this.batchName.push(element.batchDto.name);
            batchUids.push(element.batchDto.uniqueId);
            element.studentList.forEach(element => {
              let obj = { student: { name: element.fullName, uniqueId: element.uniqueId }, imageCode: this.getImage(element.uniqueId) }
              students.push(obj);
            });
          });

          this.examData.studentList = students;
          this.examData.batchInfo = { batchIds: batchUids };
          console.log('examData', this.examData);
          this.exam = this.examData;
          console.log('batchName', this.batchName);
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error.error);
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }

      });
  }

  updateExam() {
    this.exam = this.quickExamForm.value;
  }
  updateExamBasicInfo() {
    const dto = {
      uniqueId: this.examUid,
      name: this.examName,
      passingMarks: this.exam.passingMarks,
      maximumMarks: this.exam.maximumMarks
    }
    this.spinner.show();
    this._examService.updateBasicInfo(dto).subscribe(
      (res: any) => {
        console.log('res', res);

        if (res.success == true) {

          this.getExamBasicInfo()

        }

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.toastService.ShowWarning(error.error);
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }

      });
  }

  editTimingInfo() {
    this.isEditData = true;
  }

  editStudInfo() {
    if (this.isExamViewFromList == false) {
      this.studInfo['courseId'] = this.exam.batchInfo.courseId;
      this.studInfo['batchIds'] = this.exam.batchInfo.batchIds;
      this.studInfo['studentList'] = this.exam.studentList;
      this.isExamSummaryList = true;
    } else {
      this.quickExamForm = this.fb.group({
        batchInfo: this.fb.group({
          courseId: [this.exam.batchInfo.courseId, Validators.required],
          courseName: ['', Validators.nullValidator],
          batchIds: [this.exam.batchInfo.batchIds, Validators.required],
        })
      })
      this.studInfo['courseId'] = this.exam.course.uniqueId;
      this.studInfo['batchIds'] = this.exam.batchInfo.batchIds;
      this.studInfo['studentList'] = this.exam.studentList;
      this.isExamSummaryList = false;
    }
    this.isEditStudent = true;
  }

  closeTimingInfo(event) {
    this.isEditData = false;

    if (this.isExamViewFromList) {
      this.getExamBasicInfo();
    } else {
      let date = new Date();
      let dateformat = moment(date).format("yyyy/mm/dd");
      let timeformat = this.convertTime(event.assessmentTime);
      let time = event.assessmentTime.split(" ");
      console.log(dateformat, event.assessmentTime, time, new Date('' + event.assessmentTime));
      let splitTimeArry = timeformat.split(":");
      let hr = splitTimeArry[0];
      let min = splitTimeArry[1];
      date.setHours(+hr);
      date.setMinutes(+min);
      let newdatetime = date;
      let duration = (this.assignmentTypeId == 2) ? this.exam.duration : event.duration;
      var timelater = new Date(newdatetime.getTime() + (parseInt(duration) * 60 * 1000));
      // console.log(this.getTimeOnly(timelater));
      this.examTime = this.getTimeOnly(newdatetime) + '-' + this.getTimeOnly(timelater);
      this.exam.timingDetailsDto.duration = event.duration;
      this.exam.timingDetailsDto.assessmentTime = event.assessmentTime;
      this.exam.timingDetailsDto.extraTime = event.extraTime;
      this.exam.timingDetailsDto.assessmentDate = event.assessmentDate;
    }
  }

  closeStudInfo(event) {

    if (event != false) {
      this.exam = this.quickExamForm.value;

      if (this.exam.batchInfo.batchIds != '') {
        this.batchName = [];
        for (let i = 0; i < this.exam.batchInfo.batchIds.length; i++) {
          event.map(x => {
            if (x.uniqueId == this.exam.batchInfo.batchIds[i]) {
              this.batchName.push(x.name);
              return;
            }
          });
        }
      }
    }
    this.isEditStudent = false;

  }
  cancel() {
    this.close.emit('')
  }
  getImage(uniqueId) {
    this._userService.getImage("student", uniqueId, false).subscribe(
      response => {
        if (response != null && response) {
          if (response.image) {
            return response.image;
          }
        }
        return "";
      });
  }
  convertTime(time) {
    return moment(time, 'hh:mm A').format('HH:mm');
  }
}
