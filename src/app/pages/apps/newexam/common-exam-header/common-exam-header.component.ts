import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-common-exam-header',
  templateUrl: './common-exam-header.component.html',
  styleUrls: ['./common-exam-header.component.scss']
})
export class CommonExamHeaderComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  changeAction(value) {
    console.log("value",value)
    // var value=main.id
    if (value == 2) {
      this.router.navigate(['/newModule/exam/create-quick-exam'])
     
    }
    else if (value == 3) {
      this.router.navigate(['/newModule/exam/create-pattern-exam'])
    }
    else if (value == 4) {
      this.router.navigate(['/newModule/exam/create-pattern'])
     
    }else if(value == 5){
      this.router.navigate(['/newModule/exam/view-pattern'])  
    }
    // this.isTrackCreate=false;
  }

}
