import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AssesmentService {

  api: any;
  other_api: any;
  qbs_api: any
  studentList = new Subject();
  subjectList = new Subject();
  courses = new Subject();
  batches = new Subject();
  rightMarks = new Subject();
  spinner = new Subject();
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  constructor(private http: HttpClient) { }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  public addExamPattern(examPattern: any) {
    return this.http.post<any>(environment.eaasUrl + '/exam/pattern/add', examPattern);
  }

  getPaginationExamList(obj) {
    return this.http.post<any>(environment.eaasUrl + '/assessment/filter/get', obj);
  }

  public getTestTypeList(orgUid: string, isAssignment: boolean) {
    return this.http.get<any>(environment.eaasUrl + '/exam-assignment/type/get/' + orgUid + '/' + isAssignment);
    // eaaservices/api/exam-assignment/type/get/2/false
  }

  public getUserByRoleAndOrg(searchDto: any) {
    return this.http.post<any>(environment.apiUrl + '/user/list/byrole', searchDto);
  }
 
  public assignInvigilator(dto) {
    return this.http.post<any>(environment.newEaasUrl + '/exam/add/invigilator', dto);
  }

  public checkInvigilatorAvailability(examId,userId) {
    return this.http.get<any>(environment.newEaasUrl + '/is/invigilator/free/'+ examId+'/'+userId);
  }
  public getCourses(organizationId: any) {
    return this.http.get<any>(environment.apiUrl + '/course/list/' + organizationId);
  }

  getByCourseList(courseList: any) {
    return this.http.post<any>(environment.apiUrl + '/get/by/course/list', courseList);
  }

  public getSubjectByCourseList(courseList: any) {
    return this.http.post<any>(environment.apiUrl + '/get/by/course/ids', courseList);
  }

  public getfilteredStudent(filterDto) {
    return this.http.post<any>(environment.apiUrl + '/students/list/filter', filterDto)
  }

  public getsubjectSyllabus(filterDto) {
    return this.http.post<any>(environment.apiUrl + '/get/syllabus', filterDto);
  }

  public addExam(exam: any) {
    // https://eip-dev.byteachers.com/exam/add
    return this.http.post<any>(environment.eaasUrl + '/exam/add', exam);
  }

  public getAllSubjectByOrg(orgId: any) {
    return this.http.get<any>(environment.apiUrl + '/subject/list/get/' + orgId);
  }
  getExamType() {
    return this.http.get<any>(environment.eaasUrl + '/get/question/type/list');
  }
  public getMarkingSchemeList() {
    return this.http.get<any>(environment.newEaasUrl + '/markingscheme/getall/byorg');
  }

  public getMarkingSchemeDetailedInfo(uniqueId) {
    return this.http.get<any>(environment.newEaasUrl + '/markingscheme/detailed/info/' + uniqueId);
  }

  public saveMarkingScheme(schemeData: any) {
    return this.http.post<any>(environment.eaasUrl + '/save/mcq/scheme', schemeData);
  }

  public getCoursePlannerList(subjectId: any, courseId): Observable<any> {


    return this.http.get<any>(environment.apiUrl + '/get/course-planner/list/' + courseId + '/' + subjectId);
  }


  public getExamPatternList(patternType: any, orgId: any) {
    return this.http.get<any>(environment.eaasUrl + '/exam/pattern/list/' + patternType + "/" + orgId);
  }

  public getExamPatternFromEip(examPatternUid: string, patternType: any) {
    return this.http.get<any>(environment.eaasUrl + '/get/exam/pattern/info/' + examPatternUid + "/" + patternType);
  }

  public getExamPatternFromQbs(examPatternUid: string) {
    return this.http.post<any>(environment.apiUrl + '/get/exam/pattern/info', examPatternUid);
  }
  getsubjectiveQuestionTypeList() {
    return this.http.get<any>(environment.newEaasUrl + '/question/get/subjective-type');
  }
  getsubjectiveQuestionTypeList2() {
    return this.http.get<any>(environment.newEaasUrl + '/question/get/subjective-type');
  }

  getObjectiveQuestionTypeList() {
    return this.http.get<any>(environment.newEaasUrl + '/question/get/objective-type');
  }
  getObjectiveQuestionTypeList2() {
    return this.http.get<any>(environment.newEaasUrl + '/question/get/objective-type');
  }

  public getJeeAdvanceSubjects(orgUid: string) {
    return this.http.get<any>(environment.apiUrl + '/jee/advance/subject/list/' + orgUid);
  }
  addTestType(dto: any) {
    return this.http.post<any>(environment.eaasUrl + '/exam-assignment/type/add-update', dto);
  }
  getSearchedByUserData(serachDto: any) {
    return this.http.post<any>(environment.apiUrl + '/get/users/byRoles', serachDto);
  }


  getSyllabus(cpId: any, type: number) {
    console.log("type", type, "cpId", cpId);
    return this.http.get<any>(environment.apiUrl + '/get/syllabus/by/cp/type/' + cpId + '/' + type);
  }

  getExamTrack(examUid: string) {
    return this.http.get<any>(environment.newEaasUrl + '/exam-track/get/' + examUid);
  }

  getGradeListByExamSubjectId(examSubjectId: number) {
    return this.http.get<any>(environment.eaasUrl + '/exam/grade/list/' + examSubjectId);
  }

  lockExamTrack(examUid: string) {
    return this.http.get<any>(environment.eaasUrl + "/exam/track/lock/" + examUid);
  }

  public getExamSubjectSyllabus(examSubjectId: number, testType: number, syllabusState: number) {
    return this.http.get<any>(environment.eaasUrl + '/exam/subject/syllabus/' + examSubjectId + '/' + testType + '/' + syllabusState);
  }


  public getAddUpdateTrackSyllabus(trackData: any) {
    return this.http.post<any>(environment.eaasUrl + '/exam/track/syllabus/add-update', trackData);
  }

  public getTopicsByChaptersIds(syllabusData: any) {
    return this.http.post<any>(environment.eaasUrl + '/exam/get/topic', syllabusData);
  }

  public getSubTopicsByChaptersIds(syllabusData: any) {
    return this.http.post<any>(environment.eaasUrl + '/exam/get/subtopic', syllabusData);
  }

  getExamTrackSyllabus(trackUid: String) {
    return this.http.get<any>(environment.eaasUrl + '/exam/track/syllabus/get/' + trackUid);
  }

  public getPatternList(organizationId: any) {
    return this.http.get<any>(environment.eaasUrl + '/get/pattern/list/' + organizationId);
  }

  public getPatternInfoFromEip(patternUid: string) {
    return this.http.post<any>(environment.eaasUrl + '/get/pattern/info', patternUid);
  }

  getQbsSyllabusUnits(qbsCourseId: number, qbsStandardId: number, subjectName: string) {
    return this.http.get<any>(environment.qbsUrl + '/syllabus/get/exam/units?qbsCourseId=' + qbsCourseId + '&qbsStandardId=' + qbsStandardId + '&subjectName=' + subjectName);
  }

  getChapters(unitIds: any) {
    return this.http.post<any>(environment.qbsUrl + '/syllabus/get/chapter', unitIds)
  }

  getTopics(chapterIds: any) {
    return this.http.post<any>(environment.qbsUrl + '/syllabus/get/topic', chapterIds);
  }

  getSubTopics(topicIds: any) {
    return this.http.post<any>(environment.qbsUrl + '/syllabus/get/subtopic', topicIds);
  }

  getSyllabusQuestions(data: any) {
    return this.http.post<any>(environment.qbsUrl + '/pagination/getQuestions/bySyllabus', data)
  }

 


  deleteExamQuestions(questionData: any) {
    return this.http.post(environment.eaasUrl + "/exam/track/questions/delete", questionData);
  }

  cancelExam(examId: string) {
    return this.http.get<any>(environment.apiUrl + '/cancel/exam/' + examId);
  }


  getSubjectSyllabusTrack(examUid, subjecId) {
    return this.http.get<any>(environment.newEaasUrl + '/exam/get/subjectsyllabus/' + examUid + '/' + subjecId);
  }

  addyllabusTrack(dto) {
    return this.http.post<any>(environment.newEaasUrl + '/exam-track-syllabus/add',dto);
  }


  //new api added vinayak

  getExamTrackQuestionBySyllabus(dto) {
    return this.http.put<any>(environment.newEaasUrl + '/exam-track-question/get/bysyllabus',dto);
  }

  getTrackQuestions(dto) {
    return this.http.put<any>(environment.newEaasUrl + "/exam-track-question/get", dto);
  }

  saveExamQuestions(questionData: any) {
    return this.http.post(environment.newEaasUrl + "/exam-track-question/save", questionData);
  }

  getSolution(id: any) {

    console.log("test sol")
    return this.http.get(environment.newEaasUrl + "/question/get/solution/byuid/"+ id);
  }

  getExamSetting(id:any){
    return this.http.get(environment.newEaasUrl + "/get/exam-settings/"+ id);
  }
  getTestSetting(id:any){
    return this.http.get(environment.newEaasUrl + "/get/exam-settings/"+ id);
  }

  getAssignmentSetting(id:any){
    return this.http.get(environment.newEaasUrl + "/get/exam-settings/"+ id);
  }

  savePublishExam(publishData: any) {
    return this.http.post(environment.newEaasUrl + "/exam/publish", publishData);
  }
  getAssesmentStudentInfo(id){
    return this.http.get(environment.newEaasUrl + "/exam/get/students/"+ id);
  }

  getAllStudent(filterDto) {
    return this.http.put<any>(environment.newApiUrl + '/students/get/list', filterDto)
  }

  getExamBasicInfo(uniqueId) {
    return this.http.get<any>(environment.newEaasUrl + '/exam/get/basicinfo/'+ uniqueId)
  }

  getExamSyllabuisInfo(uniqueId) {
    return this.http.get<any>(environment.newEaasUrl + '/exam/get/syllabus-details/'+ uniqueId)
  }

  updateStudentsInfo(dto) {
    return this.http.put<any>(environment.newEaasUrl + '/exam/update/participants',dto)
  }

  updateBasicInfo(dto){
    return this.http.put<any>(environment.newEaasUrl + '/exam/update/basic-info',dto)
  }

  updateTimingInfo(dto){
    return this.http.put<any>(environment.newEaasUrl + '/exam/update/timing-info',dto)
  }

}
