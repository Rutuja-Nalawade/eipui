import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { HttpErrorResponse } from '@angular/common/http';

import { FormGroup, FormBuilder, FormArray, Validators, FormControl, FormGroupName } from '@angular/forms';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Allocation } from './allocation';
import { from } from 'rxjs';
import { Bulkbearch } from './bulkbearch';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
// "./node_modules/jquery/dist/jquery.min.js"
@Component({
  selector: 'app-bulkasign',
  templateUrl: './bulkasign.component.html',
  styleUrls: ['./bulkasign.component.scss']
})
export class BulkasignComponent implements OnInit {
  loading = false;
  closeResult: string;
  academicYear = [];
  academicCalender = [];
  courseList = [];
  batchList = [];
  boardList = [];
  gradeList = [];
  groupList = [];

  AlwacademicYear = [];
  AlwacademicCalender = [];
  AlwcourseList = [];
  AlwbatchList = [];
  AlwboardList = [];
  AlwgradeList = [];
  AlwgroupList = [];

  studentList: {
    studentId: number
    selectedName: string;
    selected: boolean;
    color: string ;
  }[] = [];

  filterstudent: any;
  orgId = parseInt(sessionStorage.getItem("organizationId"));
  DataShow: any = false;
  SelectedAcademicYearId: any = 0;
  SelectedAcademicCalenderId: any = 0;
  SelectedcourseId: any = 0;
  SelectedBatchId: any = 0;
  SelectedBoardId: any = 0;
  SelectedGradeId: any = 0;
  SelectedGroupId: any = 0;

  AllowcatedAcademicYearId: any = 0;
  AllowcatedCalenderId: any = 0;
  AllowcatedcourseId: any = 0;
  AllowcatedBatchId: any = 0;
  AllowcatedBoardId: any = 0;
  AllowcatedGradeId: any = 0;
  AllowcatedGroupId: any = 0;

  // check mouseover container
  mouseOverContainer: any;
  // Transfer Items Between Lists
  // tslint:disable-next-line: no-inferrable-types
  multiselectionEnable: boolean = false;
  spinner: boolean = false;

  // to remove elements use this array
  remove:{
    studentId: number
    selectedName: string;
    selected: boolean;
    color: string;
  } [] = [];

  // to store selected studentlist
  selectedStudents: {
    studentId: number
    selectedName: string;
    selected: boolean;
    color : string ;
  }[] = [];

  tempStudents:{
    studentId: number
    selectedName: string;
  }[] = [];
  model = new Allocation();
  bulksearch = new Bulkbearch();
  pageIndex: number = 0;
  pageIndexError: number = 0;
  constructor(private formBuilder: FormBuilder,private toastService: ToastsupportService, private modalService: NgbModal,) {


   }

  ngOnInit() {
    // this.getAcademicYear(this.orgId);
    // this.getUnAsignedStudents()
  }
  getUnAsignedStudents(){
    this.spinner = true;
    this.loading = true;
    // this.userStudentService.getUnAsignedStudents(this.orgId, this.pageIndex).subscribe(
    //   (res) => {
    //     if (res) {
    //       // this.studentList = res;
    //       if(res.length>0){
    //         // this.studentList = [];
    //         res.forEach(element => {
    //           let dto = {
    //             studentId: element.userId,
    //              selectedName: element.fullName,
    //             selected: false,
    //             color : '#ffffff',
    //           };
    //           this.studentList.push(dto);
    //         });
    //         this.loading = false;
    //         this.spinner = false;
    //       }
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       this.loading = false;
    //       this.spinner = false;
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }

  getAcademicYear(orgId) {
    this.loading = true;
    // this.academicCalenderService.getAcademicYearList(orgId).subscribe(
    //   (res) => {
    //     if (res) {
    //       this.academicYear = res.academicYearsDtoList;
    //       this.AlwacademicYear = res.academicYearsDtoList;
    //       // console.log("Course List", this.courseList);
    //       this.loading = false;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.loading = false;
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }

  onStudentsScroll() {
    this.loading = true;
    this.spinner = true;
    // console.log('Student scroll end and called');
    this.pageIndex = this.pageIndex + 1;
    this.getUnAsignedStudents();
  }
  //============================================================================================================
 
 
  onCourseChange(courseid) {
    // console.log("courseid"+courseid);
    this.loading = true;
    //to call board by cours

    // this.boardService.getBoardsByCourse(courseid).subscribe(
    //   (res) => {
    //     if (res) {

    //       this.boardList = res;
    //       // console.log("Board list", this.boardList);

    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       this.loading = false;
    //       this.toastService.ShowError(error);
    //     }
    //   });


    // to call batch by course
    // this.batchService.getCourseBatchList(courseid).subscribe(
    //   (res) => {
    //     if (res) {

    //       this.batchList = res;
    //       this.loading = false;
    //       // console.log("Batch list", this.batchList);

    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       this.loading = false;
    //       this.toastService.ShowError(error);
    //     }
    //   });

}

onBatchChange(BatchId){

  // console.log("BatchId"+BatchId);
  //to get group by batch
  this.loading = true;
  // this.batchService.getGroupListFromBatch(BatchId).subscribe(
  //   (res) => {
  //     if (res) {

  //       this.groupList = res;
  //       this.loading = false;
  //       // console.log("group list", this.groupList);

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log('Client-side error occured.');
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}

onBoardChange(BoardId){
  // call function to get grades
  this.loading = true;
  // this.gradeService.getGradesByBoard(BoardId).subscribe(
  //   (res) => {
  //     if (res) {

  //       this.gradeList = res;
  //       this.loading = false;
  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log("Client-side error occured.");
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}

serachStudent(){
    this.spinner = true;
    this.bulksearch.selectedcourseId = this.SelectedcourseId;
    this.bulksearch.selectedBoardId = this.SelectedBoardId;
    this.bulksearch.selectedBatchId = this.SelectedBatchId;
    this.bulksearch.selectedGradeId = this.SelectedGradeId;
    this.bulksearch.selectedGroupId = this.SelectedGroupId;
    this.bulksearch.academicYearId = this.SelectedAcademicYearId;
    this.bulksearch.academicCalenderId = this.SelectedAcademicCalenderId;
    this.bulksearch.orgId = sessionStorage.getItem('orgUniqueId');
    this.bulksearch.pageIndex = this.pageIndex;
    this.loading = true;
    this.spinner = true;
    // this.userStudentService.getBulkAsignedStudentInfo(this.bulksearch).subscribe(
    // (res) => {
    //   if (res) {
    //     // this.studentList = res;
    //     if(res.length>0){
    //       this.studentList = [];
    //       res.forEach(element => {
    //         let dto = {
    //           studentId: element.studentId,
    //            selectedName: element.selectedName,
    //           selected: false,
    //           color : '#ffffff',
    //         };
    //         this.studentList.push(dto);
    //       });
    //       this.loading = false;
    //       this.spinner = false;
    //     } else {
    //       if( this.pageIndexError !== 3) {
    //           this.pageIndex = this.pageIndex + 1;
    //           this.serachStudent();
    //           this.pageIndexError = this.pageIndexError + 1;
    //       } else {
    //         this.spinner = false;
    //       }
    //     }
    //   }
    // },
    // (error: HttpErrorResponse) => {
    //   if (error instanceof HttpErrorResponse) {
    //     console.log("Client-side error occured.");
    //   } else {
    //     this.loading = false;
    //     this.spinner = false;
    //     this.toastService.ShowError(error);
    //   }
    // });

}

// allocation process starts here
onAcademicYearChange(academicYear) {
  // get academic calender her by academic year id
  // this.academicCalenderService.getAcademicCalendarListByAcademicYearAndOrg(sessionStorage.getItem('orgUniqueId'), academicYear).subscribe(
  //   (res) => {
  //     if (res) {
  //         this.academicCalender = res.academicCalendarsDtoList;

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log('Client-side error occured.');
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}
// allocation process starts here
onAlwAcademicYearChange(academicYear) {
  // get academic calender her by academic year id
  // this.academicCalenderService.getAcademicCalendarListByAcademicYearAndOrg(sessionStorage.getItem('orgUniqueId'), academicYear).subscribe(
  //   (res) => {
  //     if (res) {
  //         this.AlwacademicCalender = res.academicCalendarsDtoList;

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log('Client-side error occured.');
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}
onAlwAcademicCalenderChange(calenderId){
  this.loading = true;
  // this.courseService.getCourseListByAcademicCalendar(calenderId).subscribe(
  //   (res) => {
  //     if (res) {
  //       this.AlwcourseList = res;
  //       // console.log("Course List", this.courseList);
  //       this.loading = false;
  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log('Client-side error occured.');
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}
onAcademicCalenderChange(calenderId){
  this.loading = true;
  // this.courseService.getCourseListByAcademicCalendar(calenderId).subscribe(
  //   (res) => {
  //     if (res) {
  //       this.courseList = res;
  //       // console.log("Course List", this.courseList);
  //       this.loading = false;
  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log('Client-side error occured.');
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}
onAlwCourseChange(courseid) {
  // console.log("courseid"+courseid);
  this.loading = true;
  //to call board by cours

  // this.boardService.getBoardsByCourse(courseid).subscribe(
  //   (res) => {
  //     if (res) {

  //       this.AlwboardList = res;
  //       // console.log("Board list", this.boardList);

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log("Client-side error occured.");
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });


  // to call batch by course
  // this.batchService.getCourseBatchList(courseid).subscribe(
  //   (res) => {
  //     if (res) {

  //       this.AlwbatchList = res;
  //       this.loading = false;
  //       // console.log("Batch list", this.batchList);

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log("Client-side error occured.");
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}

onAlwBoardChange(BoardId){

  console.log("BoardId"+BoardId);

  //call function to get grades
  this.loading = true;
  // this.gradeService.getGradesByBoard(BoardId).subscribe(
  //   (res) => {
  //     if (res) {

  //       this.AlwgradeList = res;
  //       this.loading = false;
  //       console.log("grade list", this.gradeList);

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log("Client-side error occured.");
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}

onAlwBatchChange(BatchId){

  // console.log("BatchId"+BatchId);
  //to get group by batch
  this.loading = true;
  // this.batchService.getGroupListFromBatch(BatchId).subscribe(
  //   (res) => {
  //     if (res) {

  //       this.AlwgroupList = res;
  //       this.loading = false;
  //       // console.log("group list", this.groupList);

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log("Client-side error occured.");
  //     } else {
  //       this.loading = false;
  //       this.toastService.ShowError(error);
  //     }
  //   });

}

allocatedetails(){

  this.selectedStudents.forEach(element => {
    let dto ={
      studentId : element.studentId,
      selectedName:element.selectedName
    }

    this.tempStudents.push(dto);
    console.log('studentList' + JSON.stringify(this.tempStudents))
  });
  // console.log("Selected students string json"+ JSON.stringify(this.selectedStudents))
  this.loading = true;
  this.spinner = true;
  this.model.selectedAcademicYearId = this.AllowcatedAcademicYearId;
  this.model.selectedAcademicCalenderId = this.AllowcatedCalenderId;
  this.model.selectedcourseId = this.AllowcatedcourseId;
  this.model.selectedBoardId = this.AllowcatedBoardId;
  this.model.selectedBatchId = this.AllowcatedBatchId;
  this.model.selectedGradeId = this.AllowcatedGradeId;
  this.model.selectedGroupId = this.AllowcatedGroupId;
  this.model.studentList = this.tempStudents;
  // this.userStudentService.allocateDetails(this.model).subscribe(
  //   (res) => {
  //     if (res) {
  //       this.toastService.showSuccess("Allocation details saved successfully");
  //     console.log("Student Save Response", res);

  //       // if(res =="201 status ok"){
  //         this.selectedStudents = [];
  //         this.tempStudents = [];
  //         this.spinner = false ;
  //       //   this.selectedStudents = null
  //       // }

  //     }
  //   },
  //   (error: HttpErrorResponse) => {
  //     if (error instanceof HttpErrorResponse) {
  //       console.log("Client-side error occured."+error);
  //     } else {
  //       this.spinner = false;
  //       this.loading = false;
  //       this.toastService.showSuccess(error);
  //       this.selectedStudents = [];
  //       this.tempStudents = [];
  //     }
  //   });

}

open(content) {

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {

      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });

  }


  opensearchmodal(content) {

    this.modalService.open(content, { size: 'xl',  ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {

      this.closeResult = `Closed with: ${result}`;

    }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;

    });

  }

  

  private getDismissReason(reason: any): string {

    if (reason === ModalDismissReasons.ESC) {

      return 'by pressing ESC';

    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {

      return 'by clicking on a backdrop';

    } else {

      return  `with:  ${reason}`;

    }

  }

  reloadPage(){


  }


  //select moveseover container
  setMouseover(string){
    this.mouseOverContainer = string;
    // console.log("Mouse over container"+string);
   }
   //to select item from studentlist
   SelectDeselectMainList(index){

    //check if multi select is enabled or not
    if(this.multiselectionEnable){
      if(this.studentList[index].selected==true){
        this.studentList[index].selected=false;
        this.studentList[index].color = "#ffffff"
      }else{
        this.studentList[index].selected=true;
        this.studentList[index].color = "#d3d3d3"
      }
    }

  }
   //to select item from Selectedstudentlist
  SelectDeselectsubList(index){
    //check if multi select is enabled or not
    if(this.multiselectionEnable){
      if(this.selectedStudents[index].selected==true){
        this.selectedStudents[index].selected=false;
        this.selectedStudents[index].color = "#ffffff"
      }else{
        this.selectedStudents[index].selected=true;
        this.selectedStudents[index].color = "#d3d3d3"
      }

    }
  }

  //check uncheck multiselection
  onMultiCheckChange(){

    if(this.multiselectionEnable == false) {
      for(let i = 0; i<this.studentList.length; i++){
          this.studentList[i].selected = false
         this.studentList[i].color ="#ffffff"
      }
      for(let i = 0; i<this.selectedStudents.length; i++){

        this.selectedStudents[i].selected = false;
        this.selectedStudents[i].color = '#ffffff';
      }
    }

  }

  drop(event: CdkDragDrop<string[]>) {
    if(this.multiselectionEnable == false){
      // if multi select is not checked execute as normal
      if (event.previousContainer !== event.container) {
        transferArrayItem(event.previousContainer.data,event.container.data, 
          event.previousIndex, event.currentIndex)
      } else {
        moveItemInArray(this.studentList, event.previousIndex, event.currentIndex);
      }

    }else{

      switch(this.mouseOverContainer){

        case "Main container":
              // move sub container to main container
               // tslint:disable-next-line: prefer-for-of
               for(let i =0; i<this.selectedStudents.length; i++){
                 if(this.selectedStudents[i].selected==true){
                   let dto = {
                    studentId: this.selectedStudents[i].studentId,
                    selectedName: this.selectedStudents[i].selectedName,
                     selected: false,
                     color : '#ffffff',
                   }
                   this.studentList.push(dto)
                   this.remove.push(dto);
                 }
             }
               for (let i = 0; i<this.remove.length; i++){
               for(let l =0; l<this.selectedStudents.length; l++){
                 if(this.remove[i].studentId == this.selectedStudents[l].studentId){
                        this.selectedStudents.splice(l, 1);
                 }
               }
             }
               this.remove = [];
               break;
        case 'sub container':
         // move main container to sub container
           // tslint:disable-next-line: prefer-for-of
           for (let i = 0; i < this.studentList.length; i++) {
               if (this.studentList[i].selected == true){
                 let dto = {
                  studentId: this.studentList[i].studentId,
                  selectedName: this.studentList[i].selectedName,
                   selected: false,
                   color : '#ffffff',
                 };
                 this.selectedStudents.push(dto);
                 this.remove.push(dto);
               }
           }
           // tslint:disable-next-line: prefer-for-of
           for (let i = 0; i < this.remove.length; i++) {
             for (let l = 0; l < this.studentList.length; l++){
               if (this.remove[i].studentId == this.studentList[l].studentId){
                      this.studentList.splice(l, 1);
               }
             }
           }
           this.remove = [];
           break;
      }
    }
  }


  clearSearch(){
    this.SelectedAcademicYearId = 0;
    this.SelectedAcademicCalenderId = 0;
    this.SelectedGroupId  = 0;
    this.SelectedcourseId  =0;
    this.SelectedBatchId  =0;
    this.SelectedBoardId  = 0;
    this.SelectedGradeId  = 0;
  }
}
