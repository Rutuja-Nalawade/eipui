import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectComponent } from '@ng-select/ng-select';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ExamService } from '../exam.service';

interface FilterDto {
    orgList: Array<any>;
    courseList: Array<any>;
    boardList: Array<any>;
    gradeList: Array<any>;
    batchList: Array<any>;
    roleList: Array<any>;
    pageIndex: any;
    subjectList: Array<any>;
    genderList: Array<any>;
    isForAll: Boolean;
    searchType: number;
    organizationUid: string;
}
@Component({
  selector: 'app-batchdetails',
  templateUrl: './batchdetails.component.html',
  styleUrls: ['./batchdetails.component.scss']
})
export class BatchdetailsComponent implements OnInit {
  @Input() batchInfo: FormGroup;

  selectedStudentsList = [];
  viewSelectedStudentsList = [];
  remainingSelectedStudents=0;
  courseList = [];
  batchList = [];
  studentList = [];
  students = [];
  boardId: string;
  checkedList:any;
  masterSelected:Boolean;
  @ViewChild('ngSelectComponent', { static: false }) ngSelectComponent: NgSelectComponent;
  constructor(private examService:ExamService, private modalService: NgbModal, private _DomSanitizationService: DomSanitizer, private toastService:ToastsupportService) {
    this.masterSelected = false;
   }
 
  ngOnInit() {
    this.getCourseList();
}

public getCourseList() {
    this.courseList = [];
    return this.examService.getCourses(sessionStorage.getItem('organizationId')).subscribe(
        res => {
            if (res) {
                this.courseList = res;
                console.log("this.courseList",this.courseList);
                this.examService.courses.next(res);
            }
        },
        (error: HttpErrorResponse) => {
            // if (error.error instanceof Error) {
            //     console.log("Client-side error occured.");
            // } else {
            //     // this._commonService.ShowError(error.error.errorMessage);
            // }
        });
}

  getBatchAndSubjectList() {
    let courseList = [];
    this.batchList = [];
    this.studentList = [];
    this.examService.subjectList.next('');
    this.examService.studentList.next('');
    if (this.batchInfo.controls['courseId'].value != null && this.batchInfo.controls['courseId'].value != '') {
      this.ngSelectComponent.clearModel();
        let courseId: any = this.batchInfo.controls['courseId'].value
        console.log("courseId",courseId)
        courseList.push(courseId);
        console.log("courseList",courseList)
        if (courseList != null && courseList.length > 0) {
            this.getBatchList(courseList);
            this.getSubjects(courseList);
        }
    }
}

getSubjects(courseList: any) {
  console.log("subjects");
  
  this.examService.getSubjectByCourseList(courseList).subscribe(
      res => {
          if (res) {
            console.log("res");
              this.examService.subjectList.next(res);
          }
      },
      (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //     console.log("Client-side error occured.");
          // } else {
          //   //   this._commonService.ShowError(error.error.errorMessage);
          // }
      });
}

getBatchList(courseList: any) {
  this.batchList = [];
  return this.examService.getByCourseList(courseList).subscribe(
      res => {
          if (res) {
              this.batchList = res;
              console.log("this.batchList",this.batchList);
              this.examService.batches.next(res);
          }
      },
      (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //     console.log("Client-side error occured.");
          // } else {
          //     this.toastService.ShowError("No Batches found for this course.");
          //   //   this._commonService.ShowError(error.error.errorMessage);
          // }
      });
}

getStudents(searchStudent:String) {
  this.studentList = [];
 
  let filterDto: FilterDto = {} as any;

  if (this.batchInfo.controls['courseId'].value != null && this.batchInfo.controls['courseId'].value != '') {
      let courseList = [];
      let courseId: number = this.batchInfo.controls['courseId'].value
      courseList.push(courseId);
      // let courseList: [] = this.batchInfo.controls['courseId'].value;
      filterDto.courseList = courseList;
  }

  if (this.batchInfo.controls['batchIds'].value != null && this.batchInfo.controls['batchIds'].value != '') {
      // let batch = this.batchList.find(x => x.uniqueId == this.batchInfo.controls['batchIds'].value);
      let batchList: [] = this.batchInfo.controls['batchIds'].value;
      filterDto.batchList = batchList;
  }
  if (this.batchInfo.controls['courseId'].value != null) {
      let orgList = [];
      let roleList = [];
      orgList.push(parseInt(sessionStorage.getItem('organizationId')));
      roleList.push(8);
      filterDto.orgList = orgList;
      filterDto.pageIndex = null;
      filterDto.searchType = 3;
      filterDto.roleList = roleList;
      
      console.log("filterDto",filterDto);
      return this.examService.getfilteredStudent(filterDto).subscribe(
          res => {
              if (res) {
                  if (res.length > 0) {
                    this.examService.studentList.subscribe(
                      (value: any) => {
                        this.students = value;
                      });
                      
                      console.log("students",this.students.length)
                      console.log("students",this.students)
                      if(this.students.length != 0){
                        if(this.students.length == res.length){
                          this.masterSelected = true;
                        }
                        res.map(user=>{
                        
                          this.students.forEach(element => {
                            if(user.id == element.id){
                              user.isSelected= true
                            }
                          });
                            
                          })
                      }

                      if(this.students.length == 0){
                        res.map(user=>{
                         
                            user.isSelected= false
                          })
                      }
                     
                     
                        this.modalService.open(searchStudent,{backdrop: 'static'});
                        
                        this.studentList = res;
                    }else{
                      this.toastService.ShowError("Student data not found!")
                    }
                    this.studentList = res;
              }
          },
          (error: HttpErrorResponse) => {
              // if (error.error instanceof Error) {
              //     console.log("Client-side error occured.");
              // } else {
              //     this.toastService.ShowError(error.error.errorMessage);
              // }
          });
  }

}
radioButtonEvent(value: number) {
    // const controlArray = <FormArray>this.quickExamForm.controls['studentList'];
    // for (let i = controlArray.length - 1; i >= 0; i--) {
    //   controlArray.removeAt(i);
    // }
    // if (value == 1) {
    //   this.quickExamForm.setControl('studentList', this.fb.array(this.students));
    // }
  }
  addDeleteStudentUid(values: any, user: any) {
    // const controlArray = <FormArray>this.quickExamForm.controls['studentList'];
    // if (values.currentTarget.checked) {
    //   controlArray.push(new FormControl(user));
    // } else {
    //   let index = controlArray.controls.findIndex(x => x.value == user.id);
    //   controlArray.removeAt(index);
    // }
  }
  save(){
    this.selectedStudentsList=[]
    this.selectedStudentsList=this.checkedList;
    this.masterSelected = false;
    this.viewSelectedStudentsList= this.checkedList.slice(0, 6);
    if (this.checkedList.length>5) {
      this.remainingSelectedStudents= this.checkedList.length-6;
    }else{
      this.remainingSelectedStudents=0
    }
    this.examService.studentList.next(this.selectedStudentsList);
    console.log("checkedList",this.checkedList);
    
  }
  checkUncheckAll() {
    console.log("")
    for (var i = 0; i < this.studentList.length; i++) {
      this.studentList[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.studentList.every(function(item:any) {
        return item.isSelected == true;
      })
    this.getCheckedItemList();
  }

  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.studentList.length; i++) {
      if(this.studentList[i].isSelected)
      this.checkedList.push(this.studentList[i]);
    }
    this.checkedList = this.checkedList;
  }
  deleteSelectedStudent(id,index){
    console.log("id",id);
    console.log("index",index);
    
    console.log("selectedStudentsList",this.selectedStudentsList);
    var studentList= this.selectedStudentsList.splice(index, 1);
    console.log("studentList",this.selectedStudentsList);
    this.viewSelectedStudentsList= this.selectedStudentsList.slice(0, 5);
    console.log("viewSelectedStudentsList",this.viewSelectedStudentsList);
    if (this.selectedStudentsList.length>5) {
      this.remainingSelectedStudents= this.selectedStudentsList.length-5;
    }else{
      this.remainingSelectedStudents=0
    }
    this.examService.studentList.next(this.selectedStudentsList);
  }
}
