import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParentsRoutingModule } from './parents-routing.module';
import { ParentsListComponent } from './parents-list/parents-list.component';
import { NgbDropdownModule, NgbModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { BrowserModule } from '@angular/platform-browser';
import { CommonComponentModule } from 'src/app/components/common/common-component.module';
import { BsDropdownModule, TabsModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [ParentsListComponent],
  imports: [
    CommonModule,
    ParentsRoutingModule,
    UIModule,
    NgbTabsetModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    SharedModule,
    UIModule,
    NgbModule,
    NgbDropdownModule,
    // BrowserModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    CommonComponentModule
  ],
  exports: [ParentsListComponent],
  bootstrap: []
})
export class ParentsModule { }
