import { Component, OnInit, Input, Output,EventEmitter, SimpleChanges } from '@angular/core';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AssesmentService } from 'src/app/servicefiles/assesment.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-add-students',
  templateUrl: './add-students.component.html',
  styleUrls: ['./add-students.component.scss']
})
export class AddStudentsComponent implements OnInit {
  masterSelected: Boolean;
  studentList = [];
  checkedList: any;
  batchList = [];
  courseid: any;
  searchText;
  adeddbatchList = [];
  selectedstudents=[];
  @Input() examUid:any;
  @Output() close=new EventEmitter<any>(); 
  @Input() isExamSummaryList: any;
  @Input() studInfo: any;
  @Input() batchInfo:FormGroup;
  
  constructor(private batchService: BatchesServiceService,
              private _examService: AssesmentService,
              private spinner: NgxSpinnerService,
              private toastService: ToastsupportService,
              private examService: ExamServiceService,) { }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['isExamSummaryList'].currentValue== true){
      console.log("studInfo",this.studInfo);
      this.getBatchList(this.studInfo.courseId);
    }else{
      this.getBasicInfo();
    }
  }
  checkUncheckAll() {
    console.log("")
    for (var i = 0; i < this.studentList.length; i++) {
      this.studentList[i].student.isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.masterSelected = this.studentList.every(function (item: any) {
      return item.student.isSelected == true;
    })
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.studentList.length; i++) {
      if (this.studentList[i].student.isSelected)
        this.checkedList.push(this.studentList[i]);
    }
    this.checkedList = this.checkedList;
    console.log("checkedList",this.checkedList)
  }

  isAddMoreStudents() {
    let batchDto = [];
    let addedBatchUidList=[]
    this.spinner.show();
    this._examService.getAssesmentStudentInfo(this.examUid).subscribe(
      (res: any) => {
        if (res) {

          this.checkedList=[]
          console.log('test', res);
          res.list.forEach(element => {
            batchDto.push(element.batchDto);
            this.checkedList=element.studentList;

          });

          batchDto.forEach(element => {
            this.batchList.forEach(element1 => {
              if (element.uniqueId == element1.uniqueId) {
                addedBatchUidList.push(element.uniqueId);
              }
            });
          });
          this.adeddbatchList=addedBatchUidList;
          console.log('selectedstudents', this.selectedstudents);
          console.log('batchDto', batchDto);
          console.log('adeddbatchList', this.adeddbatchList);
          this.getStudents(batchDto,false);
         
         
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide();
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }

      });
  }
  examAddedStudents(){
    this.checkedList=[];
    
    this.studInfo.studentList.forEach(element => {
      this.checkedList.push(element.student);
    });
    
    console.log("check list",this.checkedList);
    this.adeddbatchList=this.studInfo.batchIds;
    let batchesId=[];
    this.studInfo.batchIds.forEach(element => {
      let obj={uniqueId:element}
      batchesId.push(obj);
    });
    this.getStudents(batchesId,false);
  }
  getBasicInfo() {
    let batchDto = [];
    this.spinner.show();
    this._examService.getExamBasicInfo(this.examUid).subscribe(
      (res: any) => {
        if (res) {

          this.courseid = res.course.uniqueId
          console.log('test', res);
          this.getBatchList(this.courseid);
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide();
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }

      });
  }
  getStudents(searchBatch,status) {
    let bacthes=[]
    this.studentList = [];
    let filterDto = {
      "batchIds": [],
      "courseIds": [],
      "groupCategoriesIds": [],
      "batchGroupIds": [],
      "gradeIds": [],
      "boardIds": [],
      "subjectIds": [],
      "page": 0,
      "size": 50,
      "unassigned": false,
      "states": [
        1,
        2
      ],
      "studentUniqueIds": [],
      "responseFields": {
        "courseField": false,
        "batchFiled": true,
        "boardField": false,
        "gradeField": false,
        "subjectField": false,
        "groupCategoryField": false,
        "batchGroupFiled": false
      }
    }
    this.spinner.show();
    // if(this.isExamSummaryList){
    //   searchBatch.forEach(element => {
    //     bacthes.push(element);
    //   });
    // }else{
      searchBatch.forEach(element => {
        bacthes.push(element.uniqueId)
      });
   
    filterDto.batchIds = bacthes;
    if (searchBatch.length > 0) {
      console.log("filterDto", filterDto);
      return this._examService.getAllStudent(filterDto).subscribe(
        res => {
          if (res) {
            console.log("res", res);

            if (res.students && res.students.length > 0) {
             
               if (this.checkedList.length != 0) {
                if (this.checkedList.length == res.students.length) {
                  this.masterSelected = true;
                }
                res.students.map(user => {
                  this.checkedList.forEach(element => {
                    if (user.student.uniqueId == element.uniqueId) {
                      user.student.isSelected = true;
                    }
                  });
                })
              }
              if (this.checkedList.length == 0) {
                res.students.map(user => {

                  user.student.isSelected = false
                })
              }

            } else {
              this.toastService.ShowError("Student data not found!")
            }
            this.studentList = res.students;
            this.spinner.hide();
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }

  }

  getBatchList(courseid: any) {
    this.batchList = [];
    return this.batchService.getBatchByCourse(courseid).subscribe(
      res => {
        if (res.success == true) {
          console.log("res", res)
          this.batchList = res.list;
          if(this.isExamSummaryList){
            this.examAddedStudents();
          }else{
            this.isAddMoreStudents();
          }
         
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError("No Batches found for this course.");
        //   //   this._commonService.ShowError(error.error.errorMessage);
        // }
      });
  }
  saveMoreStudent(){
    let students=[];
    this.checkedList.forEach(element => {
      students.push(element.uniqueId)
    });
    
   if(this.isExamSummaryList){
    this.masterSelected = false;
    console.log("this.checkedList",this.checkedList)
    this.examService.studentList.next(this.checkedList);
    this.close.emit(this.batchList)
    }else{

      const obj={
        assessmentUid:this.examUid,
        studentUids: students
      }
  
      this.spinner.show();
  
      return this._examService.updateStudentsInfo(obj).subscribe(
        res => {
          if (res) {
            console.log("res", res)
            if(res.success == true){
              this.toastService.showSuccess('Student update successfully');
              this.close.emit(false)
            }
           
            this.spinner.hide()
          }
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide();
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
         
        });
     }
    
  }

  changeBatch(event){
    console.log(event);
    this.getStudents(event,true);
  }

  subCancel(){
    this.close.emit(false)
  }

}
