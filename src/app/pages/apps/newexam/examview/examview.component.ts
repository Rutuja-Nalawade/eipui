import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-examview',
  templateUrl: './examview.component.html',
  styleUrls: ['./examview.component.scss']
})
export class ExamviewComponent implements OnInit {

  @Input() exam : any; 
  @Input() courseName : any; 
//    @Input() gradeName : any; 
  @Input() batchName : any; 
//    @Input() boardName : any; 
  @Input() testType : string;
  isTenStud = true;
   constructor() { }
   ngOnInit() {
   }

   showAll(){
    this.isTenStud=false;
  }

}
