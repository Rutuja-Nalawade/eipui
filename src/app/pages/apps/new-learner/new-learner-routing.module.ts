import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentsComponent } from './students/students.component';


const routes: Routes = [
  {
    path: 'students',
    component: StudentsComponent
  },
  {
    path: 'lists',
    component: StudentsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewLearnerRoutingModule { }
