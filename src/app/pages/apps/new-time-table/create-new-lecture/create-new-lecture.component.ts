import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, Input, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ClassroomServiceService } from 'src/app/servicefiles/classroom-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { HttpErrorResponse } from '@angular/common/http';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { PeriodServiceService } from 'src/app/servicefiles/period-service.service';
import { pipe } from 'rxjs';

@Component({
  selector: 'app-create-new-lecture',
  templateUrl: './create-new-lecture.component.html',
  styleUrls: ['./create-new-lecture.component.scss']
})
export class CreateNewLectureComponent implements OnInit {
  createNewLectureForm: FormGroup;
  userData: any = {};
  submitted = false;
  subjectList = [];
  courseList = [];
  batchList = [];
  periodList = [];
  batchGroupCategoryAllData = [];
  batchGroupCategoryList = [];
  batchGroupList = [];
  teacherList = [];
  classroomList = [];
  typeList = [{ id: 1, name: "Manual" }, { id: 2, name: "Import" }, { id: 3, name: "Automatic" }];
  shiftUniqueId: any = null;
  periodUniqueId: any = null;
  @Output() close = new EventEmitter<any>();
  @Input() timetableId: any;
  @Input() createNewDate: any;
  @Input() lectureType: any;
  @Input() editLectureData: any;

  constructor(private _commonService: NewCommonService,
    private formBuilder: FormBuilder,
    private toastService: ToastsupportService,
    private datePipe: DatePipe,
    private spinner: NgxSpinnerService,
    private courseService: CourseServiceService,
    private servece: TimeTableServiceService,
    private classRoomService: ClassroomServiceService,
    private userService: UserServiceService,
    private batchesService: BatchesServiceService
    , private subjectService: SubjectServiceService,
    private timetableService: TimeTableServiceService,
    private periodService: PeriodServiceService) {
    this.userData = this._commonService.getUserData();

  }

  ngOnInit() {
    this.initNewLectureForm();
    if (this.lectureType == 2 || this.lectureType == 3) {
      console.log("editLectureData", this.editLectureData)
      this.patchEditLectureData()
    }

    console.log("createNewDate", this.createNewDate);
    console.log('form', this.createNewLectureForm.value)
    this.getSubjectList();
    this.getClassRooms();
    this.getCourseList();
    this.getPeriods()
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initNewLectureForm();
    console.log(changes['editLectureData']);
    this.shiftUniqueId = null;
    if (changes['lectureType'].currentValue != 1) {

      console.log("editLectureData", this.editLectureData)
      let obj = { name: this.editLectureData.courseName, uniqueId: this.editLectureData.courseId }
      let obj1 = { name: this.editLectureData.subjectName, uniqueId: this.editLectureData.subjectId }
      this.shiftUniqueId = this.editLectureData.shiftUid;
      this.periodUniqueId = this.editLectureData.periodId;
      this.getTeachersListBySubject(obj1);
      this.getBatchList(obj);
      this.getBatchGroupCategoryAndBacthGroupList(this.editLectureData.batchIds[0])

    } else {
      this.shiftUniqueId = this.editLectureData.shiftUid;
      this.periodUniqueId = this.editLectureData.periodId;
    }
  }

  patchEditLectureData() {


    this.createNewLectureForm.controls['course'].setValue(this.editLectureData.courseId)
    this.createNewLectureForm.controls['batch'].setValue(this.editLectureData.batchIds[0].uniqueId)
    this.createNewLectureForm.controls['batchgroup'].setValue(null)
    this.createNewLectureForm.controls['period'].setValue(this.editLectureData.periodId);
    this.createNewLectureForm.controls['subject'].setValue(this.editLectureData.subjectId)
    this.createNewLectureForm.controls['teacherId'].setValue(this.editLectureData.teacherIds[0].uniqueId)
    this.createNewLectureForm.controls['classroomId'].setValue(this.editLectureData.classRoomId);


    console.log("this.createNewLectureForm", this.createNewLectureForm.getRawValue())
  }
  initNewLectureForm() {
    console.log("createNewDate", this.createNewDate);
    this.createNewLectureForm = this.formBuilder.group({
      date: [this.createNewDate, Validators.required],
      course: [null, Validators.required],
      batch: [null, Validators.required],
      batchgroup: [null, Validators.required],
      period: [null, Validators.required],
      subject: [null, Validators.required],
      teacherId: [null, Validators.required],
      batchgroupcategoryId: [null, Validators.required],
      classroomId: [null, Validators.required],

    });
  }
  get f() { return this.createNewLectureForm.controls; }
  cancel() {
    this.close.emit("");
    this.submitted = false;
  }
  save() {
    this.submitted = true;
    if (this.createNewLectureForm.invalid) {
      return;
    }
  }



  getSubjectList() {
    this.spinner.show();
    return this.subjectService.getSubjectList().subscribe(
      res => {
        if (res.success == true) {

          this.subjectList = res.list;
        } else {
          this.toastService.ShowWarning('No subjects found')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getClassRooms() {
    this.spinner.show();
    return this.classRoomService.getAcademicClassRoom().subscribe(
      res => {
        console.log("class room", res)
        if (res.success == true) {
          this.classroomList = res.list;
        } else {
          this.toastService.ShowError(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getPeriods() {
    this.spinner.show();
    return this.periodService.getAllPeriodTimeTableWise(this.timetableId).subscribe(
      res => {
        console.log("period", res)
        this.periodList = res;
        // if (res.success==true) {
        //  this.classroomList=res.classroomList;
        // }else{
        //   this.toastService.ShowWarning('Class room not found')
        // }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getCourseList() {
    this.spinner.show();
    return this.courseService.getCourseBasicInfoList(1).subscribe(
      res => {

        if (res.success == true) {
          // console.log("get courseList => ",res)
          this.courseList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getBatchList(event) {
    this.createNewLectureForm.controls.batch.setValue(null);
    this.createNewLectureForm.controls.batchgroupcategoryId.setValue(null);
    this.createNewLectureForm.controls.batchgroup.setValue(null);

    this.spinner.show();
    return this.batchesService.getBatchByCourse(event.uniqueId).subscribe(
      res => {
        if (res.success == true) {
          this.batchList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getBatchGroupCategoryAndBacthGroupList(event) {
    this.createNewLectureForm.controls.batchgroupcategoryId.setValue(null)
    this.createNewLectureForm.controls.batchgroup.setValue(null)
    this.batchGroupCategoryList = [];
    this.batchGroupCategoryAllData = [];
    let batchListTest = [];
    batchListTest.push(event.uniqueId)
    this.spinner.show();
    return this.batchesService.getBatchGroupCategoryAndBacthGroupList(batchListTest).subscribe(
      res => {
        console.log("batchGroupCategory", JSON.stringify(res))

        if (res.success == true) {
          this.batchGroupCategoryAllData = res.batchCategoryWiseGroups;
          let tempList = [];
          this.batchGroupCategoryAllData[0].batchGroupList.forEach(element => {
            tempList.push({
              name: element.categoryName,
              uniqueId: element.categoryUniqueId
            })
          });
          this.batchGroupCategoryList = tempList;

          if (this.lectureType == 2) {
            this.getBatchGroupList(this.batchGroupCategoryList[0].uniqueId);
          }
        } else {
          this.toastService.ShowWarning('Class room not found')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getBatchGroupList(event) {
    // this.batchGroupList=res;
    this.createNewLectureForm.controls.batchgroup.setValue(null)
    this.batchGroupList = [];
    let tempList = [];
    this.batchGroupCategoryAllData[0].batchGroupList.forEach(element => {
      if (event.uniqueId == element.categoryUniqueId) {
        element.batchGroups.forEach(groups => {
          tempList.push({
            name: groups.name,
            uniqueId: groups.uniqueId
          })
        });
      }
    });
    this.batchGroupList = tempList;
  }
  saveLecture() {

    this.submitted = true;
    if (this.createNewLectureForm.invalid) {
      console.log('test')
      console.log('this.createNewLectureForm', this.createNewLectureForm)
      return;
    }
    if (this.lectureType == 1) {
      this.addExtraLecture()
    } else if (this.lectureType == 2) {
      this.changeLecture()
    } else if (this.lectureType == 3) {
      this.changeLecture()
    }
  }

  addExtraLecture() {
    let batchGroup = [];
    batchGroup.push(this.createNewLectureForm.value.batchgroup);
    let batches = [];
    batches.push(this.createNewLectureForm.value.batch);
    let teachers = [];
    teachers.push(this.createNewLectureForm.value.teacherId)
    let obj = {
      batchGroupUniqueIdList: batchGroup,
      batchUniqueIdList: batches,
      classroomUniqueId: this.createNewLectureForm.value.classroomId,
      courseUniqueId: this.createNewLectureForm.value.course,
      lectureDate: this.datePipe.transform(this.createNewDate, 'yyyy-MM-dd'),
      periodUniqueId: this.createNewLectureForm.value.period,
      subjectUniqueId: this.createNewLectureForm.value.subject,
      teacherUniqueIdList: teachers,
      timetableUniqueId: this.timetableId,
      substitutionType: 1
    }
    console.log("obj", obj)

    this.addExtraNdChangeLecture(obj);



  }

  changeLecture() {
    let formValue = this.createNewLectureForm.getRawValue()
    let teachers = [];

    teachers.push(formValue.teacherId);
    let batchGroupUniqueIdList = [];
    let batchUniqueIdList = [];
    batchGroupUniqueIdList.push(formValue.batchgroup);
    batchUniqueIdList.push(formValue.batch);
    const obj = {
      classroomUniqueId: formValue.classroomId,
      courseUniqueId: formValue.course,
      lectureDate: this.datePipe.transform(this.createNewDate, 'yyyy-MM-dd'),
      subjectUniqueId: formValue.subject,
      teacherUniqueIdList: teachers,
      timetableUniqueId: this.timetableId,
      substitutionType: this.lectureType == 2 ? 3 : 5,
      cardHasLessonUniqueId: this.editLectureData.uniqueId,
      periodUniqueId: formValue.period,
      batchGroupUniqueIdList: batchGroupUniqueIdList,
      batchUniqueIdList: batchUniqueIdList,
      substitutionUniqueId: this.editLectureData.substitutionId ? this.editLectureData.substitutionId : null
    }

    this.addExtraNdChangeLecture(obj);

    console.log("obj method", obj, this.createNewLectureForm.getRawValue());
  }
  addExtraNdChangeLecture(obj) {
    this.spinner.show();
    this.timetableService.addExtraLeture(obj).subscribe(
      res => {
        console.log("save extra lecture", res)
        this.close.emit(res.success);

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getTeachersListBySubject(event) {
    let dto = {
      shiftId: this.shiftUniqueId,
      periodId: this.periodUniqueId,
      orgDayId: this.editLectureData.orgWorkDayId,
      subjectId: event.uniqueId
    }
    this.timetableService.getTeacherListBySubjectDto(dto).subscribe(
      res => {
        if (res && res.success) {

          this.teacherList = res.list;
        } else {
          this.toastService.ShowError(res.responseMessage);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  changePeriod(event) {
    console.log(event);
    this.createNewLectureForm.controls.teacherId.setValue(null);
    if (this.createNewLectureForm.controls.subject.value) {
      let obj1 = { name: this.editLectureData.subjectName, uniqueId: this.createNewLectureForm.controls.subject.value }
      this.shiftUniqueId = event.shiftUniqueId;
      this.periodUniqueId = event.uniqueId;
      this.getTeachersListBySubject(obj1);
    }
  }
}
