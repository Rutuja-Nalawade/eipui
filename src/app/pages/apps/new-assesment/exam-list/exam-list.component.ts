import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AssesmentService } from '../../../../servicefiles/assesment.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { DatePipe } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-exam-list',
  templateUrl: './exam-list.component.html',
  styleUrls: ['./exam-list.component.scss']
})
export class ExamListComponent implements OnInit {
  pageIndex: number = 0;
  page = 0;
  examData: any;
  examList = [];
  teachers = [];
  defaultRows: any = '10';
  pageSize = 10;
  rowList: any = [{ id: '10', name: '10/Row' }, { id: '20', name: '20/Row' }, { id: '30', name: '30/Row' }]

  userListInvigilator = [];
  invigilatorForm: FormGroup;
  date = new Date();
  isExamListFound: boolean = false;
  totalExams: any;
  deleteExamId: string;
  isCreateTrack: boolean = false;
  examId: any;
  isViewTrack: boolean = false;
  @Input() currentTab: any;
  @Output() hideHeader = new EventEmitter<string>();
  isPublished: boolean;
  examDataPublish: any;
  isExamView: boolean = false;
  invigilatorName: any = null;
  examUniqueId: any[];
  isStatusTrue: boolean = false;
  checkInvigilatorStatus: boolean = false;
  isFirstTime = true;
  constructor(private examService: AssesmentService,
    private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private toastService: ToastsupportService,
    private modalService: NgbModal,
    private router: Router,
    private _examService: ExamServiceService,
    private userService: UserServiceService, private datePipe: DatePipe) {
  }

  ngOnInit() {
    // console.log("date",this.datePipe.transform('Wed Aug 18 11:58:32 UTC 2021','yyyy-MM-dd hh:mm:ss'))
    this.getExamList();

    this.initInvigilatorForm();


  }
  pageChanged(page: number) {
    this.pageIndex = page - 1;
    this.page = page;
    if (!this.isFirstTime) {
      this.getExamList();
    }
    this.isFirstTime = false;
  }

  public getExamList() {
    this.spinner.show()
    this.examList = [];
    let obj = {
      courseIds: [],
      batchIds: [],
      subjectIds: [],
      states: [],
      assessmentTypeIds: [],
      startDateTime: "",
      endDateTime: "",
      createdBy: "",
      page: this.pageIndex,
      size: this.pageSize
    }
    // return this.examService.getPaginationExamList(this.userData.orgUniqueId, this.pageIndex).subscribe(
    return this.examService.getPaginationExamList(obj).subscribe(
      res => {
        console.log("res", res);
        if (res) {

          this.examData = res;
          this.examList = res.assessments;
          console.log(this.examData);
          // this.totalExams=res['totalElements']
          this.isExamListFound = (this.examList && this.examList.length > 0) ? true : false;
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });

  }

  assignInvigilator(content: StringMap, examId) {
    this.examUniqueId = examId;
    this.userListInvigilator = [];

    // return this.examService.getSearchedByUserData(serachDto).subscribe(
    //   res => {
    //     if (res) {
    //       this.userListInvigilator = res;
    //       this.modalService.open(content, { centered: true });
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error.error instanceof Error) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       this.toastService.ShowError(error.error.errorMessage);
    //     }
    //   });

    this.spinner.show();
    this.userService.getUserByPageIndexAndType(0, 3, 100).subscribe(
      response => {
        console.log('getTeacherList', response.users)

        if (response.users && response.users.length > 0) {
          this.userListInvigilator = response.users
          this.modalService.open(content, { centered: true });
        } else {
          this.toastService.ShowWarning("Users not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
      });
  }

  invigilatorChange(event) {
    console.log("test invigilator", event)
    this.examService.checkInvigilatorAvailability(this.examUniqueId, event).subscribe(
      response => {
        console.log('getTeacherList', response)
        this.checkInvigilatorStatus = response;

        this.isStatusTrue = true;


        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
      });
  }


  saveInvigilator() {
    let staffId = [this.invigilatorName]
    const dto = {
      assessmentUid: this.examUniqueId,
      staffUids: staffId
    }
    console.log("body", dto)
    if (this.checkInvigilatorStatus == false) {
      this.toastService.ShowWarning("Please select anather invigilator");
      return;
    }
    this.spinner.show();
    return this.examService.assignInvigilator(dto).subscribe(
      res => {
        if (res.success == true) {
          console.log("test", res);
          this.checkInvigilatorStatus = false;
          this.isStatusTrue = false;
          this.invigilatorName = null;
          this.toastService.showSuccess("Invigilator assigned successfully..");
          this.modalService.dismissAll();
          this.getExamList();
        }


      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error.error.errorMessage);
        // }
      });
  }

  closeInvigilator() {
    this.invigilatorName = null;
    this.checkInvigilatorStatus = false;
    this.isStatusTrue = false;
    this.isCreateTrack = false;
    this.isViewTrack = false;
    this.isExamView = false;
    this.isPublished = false;
  }



  initInvigilatorForm() {
    this.invigilatorForm = this.fb.group({
      id: ['', Validators.nullValidator],
      examAssignmentUid: ['', Validators.required],
      userId: [null, Validators.required],
      createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.required],
    });
  }

  cancel() {
    $("#assignInvigilatorModal").modal("hide")
    this.invigilatorForm.reset();
  }

  routeToExamSetter(examId: string) {
    this.examId = examId;
    this.isCreateTrack = true;
    this.hideHeader.emit("1");
  }

  routeToViewTracks(examId: string) {
    this.examId = examId;
    this.isViewTrack = true;
    this.hideHeader.emit("1");
  }

  openCancelExam(centerDataModal: string, examId: string) {
    this.deleteExamId = examId;
    this.modalService.open(centerDataModal, { centered: true });
  }

  examCancel() {
    this.spinner.show();
    this._examService.cancelExam(this.deleteExamId).subscribe(
      res => {
        if (res) {

          this.pageIndex = 0;
          this.getExamList();
          this.toastService.showSuccess("Exam cancel successfully")
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   // this.toaterService.ShowWarning(error.error.errorMessage);
        // }
        // this.toastService.ShowError("Exam cancel unsuccessfully")
      });

  }

  routeToPublishExam(exam, testTrue, startDate, testFalse) { }
  routeToPaperSets(exam) { }
  routeToViewResult(exam, isAssessment) { }

  openPublish(exam) {
    this.examDataPublish = exam;
    this.isPublished = true;
  }

  closePublish(event) {
    this.getExamList()
    this.isPublished = false;
    this.isCreateTrack = false;
    this.isViewTrack = false;
    this.isExamView = false;
  }
  closeAddTrack(event) {
    this.isCreateTrack = false;
    this.isViewTrack = false;
    this.isExamView = false;
    console.log(event);
    if (event != 'cancel') {
      this.getExamList();
    }

    this.hideHeader.emit("2");

  }

  viewExam(uniqueId) {
    this.examId = uniqueId;
    this.isExamView = true;
    this.hideHeader.emit("1");
  }
  closeExamView(event) {
    this.isExamView = false;
    this.isCreateTrack = false;
    this.isViewTrack = false;
    this.hideHeader.emit("2");
    this.getExamList()
  }
}
