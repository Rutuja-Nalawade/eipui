import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { Router } from '@angular/router';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-academic-grade',
  templateUrl: './academic-grade.component.html',
  styleUrls: ['./academic-grade.component.scss']
})
export class AcademicGradeComponent implements OnInit {
  isFromSetup:boolean = false;
  @Input() currentTab:Number;
  gradeList=[];
  boardList=[];
  isEditAdd:boolean=false;
  state=[];
  central=[];
  secondary=[];
  primary=[];
  highSchool=[];
  masterBoardList=[];
  masterGradeList=[];
  boards=[];
  grades=[];
  constructor(private _boardService: BoardServiceService, private _gradeServiceService:GradeServiceService,
              private toastService: ToastsupportService,
              private router:Router,private spinner: NgxSpinnerService) { }

  ngOnInit() { 

  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("currentValue",changes['currentTab']);
    
    if (changes['currentTab'].currentValue== 3) {
      console.log("this.curremt tab",this.currentTab);
      this.secondary=[]
      this.state=[]
      this.primary=[]
      this.highSchool=[]
      this.central=[]
      this.gradeList=[];
      this.boardList=[];
      this.masterBoardList=[];
      this.masterGradeList=[];
      this.boards=[];
      this.grades=[];
      this.isEditAdd=false;
      this.getBoardList();
      this.getGradeList();
    }
  }

  getGradeList(){
this.spinner.show()
    return this._gradeServiceService.getAcademicGradeList().subscribe(
         res => {
             if (res.success == true) {
               this.gradeList = [];
               console.log("getGradeBoardListres",res)
              this.gradeList=res.list;
              // this.getMasterGradeList()
              this.sortGradeList(this.gradeList)
             }else{
              this.toastService.ShowWarning(res.responseMessage)
            }
             this.spinner.hide()
         },
         (error: HttpErrorResponse) => {
            //  if (error instanceof HttpErrorResponse) {
            //  } else {
            //      this.toastService.ShowWarning(error);
            //  }
         });
 }

  getBoardList(){
    this.spinner.show()
   return this._gradeServiceService.getAcademicBoardList().subscribe(
        res => {
            if (res) {
              this.boardList = [];
              console.log("getGradeBoardListres",res)
              this.boardList=res;
              this.sortBoardList(this.boardList)
            }

            this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            // } else {
            //     this.toastService.ShowWarning(error);
            // }

            this.spinner.hide()
        });
}

  getMasterBoardList() {
    this.spinner.show()
    this._boardService.getMasterBoardList().subscribe(
        res => {
            if (res) {
              console.log("oard res",res)
              this.masterBoardList=res;
              if(this.boardList.length > 0){
                for(let i=0;i<this.boardList.length;i++){
                  for(let j=0;j<this.masterBoardList.length;j++){
                    if(this.boardList[i].name == this.masterBoardList[j].name){
                      this.boards.push(this.masterBoardList[j].uniqueId)
                    }
                  }
                }
              }
              console.log("this.boards",this.boards)
             
            }

            this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            // } else {
            //     this.toastService.ShowWarning(error);
            // }

            this.spinner.hide()
        });

  }

  getMasterGradeList() {
    this.spinner.show()
    this._gradeServiceService.getMasterGradeList().subscribe(
        res => {
            if (res) {
              console.log("grade res",res)
              this.masterGradeList=res;
              if(this.gradeList.length > 0){
                for(let i=0;i<this.gradeList.length;i++){
                  for(let j=0;j<this.masterGradeList.length;j++){
                    if(this.gradeList[i].name == this.masterGradeList[j].name){
                      this.grades.push(this.masterGradeList[j].uniqueId)
                    }
                  }
                }
              }
            }

            this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            // } else {
            //     this.toastService.ShowWarning(error);
            // }

            this.spinner.hide()
        });
  }

  addeditGrade(){
    this.isEditAdd = true;
   }

  sortGradeList(list:any){
    this.secondary = [];
    this.primary = [];
    this.highSchool = [];
    list.forEach(element => {
      if(element.name == '12th' || element.name == '11th'){
        this.secondary.push(element);
      }else if(element.name == '10th' || element.name == '9th' || element.name == '6' || element.name == '7' || element.name == '8'){
        this.highSchool.push(element);
      }else if(element.name == '1' || element.name == '2' || element.name == '3' || element.name == '4' || element.name == '5'){
        this.primary.push(element);
      }
    });

    // console.log("secondary",this.secondary)
    // console.log("highSchool",this.highSchool)
    // console.log("primary",this.primary)
  }

  sortBoardList(list:any){
    this.state = [];
    this.central = [];
    list.forEach(element => {
    if(element.name == 'CBSE' || element.name == 'ICSE' || element.name == 'IB'){
      this.central.push(element);
    }else {
      this.state.push(element);
    }
  });

  // console.log("state",this.state)
  //   console.log("central",this.central)
  }

  backPage(value){
    console.log("value",value)
    this.isEditAdd=value;

    this.getBoardList();
    this.getGradeList();
  }
}

