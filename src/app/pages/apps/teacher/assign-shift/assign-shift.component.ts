import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Widget, UserBalance, RevenueData, ChartType } from './../../staff/work-time/default.model';
import { widgetData, salesMixedChart, revenueRadialChart, userBalanceData, revenueData } from './../../staff/work-time/data';
import { TeacherService } from '../teacher.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
@Component({
  selector: 'app-assign-shift',
  templateUrl: './assign-shift.component.html',
  styleUrls: ['./assign-shift.component.scss']
})
export class AssignShiftComponent implements OnInit {
  panelOpenState = false;
  breadCrumbItems: Array<{}>;
  userData:any={}
  currentUserId: Number = null;
  widgetData: Widget[];
  userBalanceData: UserBalance[];
  revenueData: RevenueData[];
  salesMixedChart: ChartType;
  revenueRadialChart: ChartType;
  @Input() userId:Number;
  @Input() currentTab:Number;
  shift:any=[];
  saveList: any;
  constructor(private _educatersService:TeacherService,private spinner:NgxSpinnerService, private _commonService:NewCommonService, private toastService : ToastsupportService) {
    this.userData = this._commonService.getUserData();
   }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'UBold', path: '/' }, { label: 'Dashboard', path: '/', active: true }];
    this.currentUserId=this.userId;
    /**
     * fetches data
     */
    this._fetchData();
    // this.getShift();
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log("currentValue",changes['currentTab']);
    
    if (changes['userId'] || changes['currentTab'].currentValue== 4) {
      // this.getRoleInfo();
      this.getShift(this.userId)
    }
  }
  private _fetchData() {

    this.widgetData = widgetData;
    this.salesMixedChart = salesMixedChart;
    this.revenueRadialChart = revenueRadialChart;
    this.userBalanceData = userBalanceData;
    this.revenueData = revenueData;
  }

  getShift(Id){
    // let Id="203";
    this.spinner.show();
        this._educatersService.getShift(Id).subscribe(
            response => {
              this.shift = response.shiftsLists;
              this.saveList = response.shiftsLists;
              console.log("hgdjJ",this.shift);
              // this.shift.forEach(element => {
              //   element.startTime=moment(element.startTime).format('LT');
              //   element.endTime=moment(element.endTime).format('LT');
              //   // if(element.shiftbreaksList){
              //   //   element.shiftbreaksList.forEach(element1 => {
              //   //     console.log("element1",element1);
              //   //     element1.startTime=moment(element1.startTime).format('LT');
              //   //     element1.endTime=moment(element1.endTime).format('LT');
              //   //   });
              //   // }
               
              // });
              console.log("hgdjJ",this.shift);
                this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              
                this.spinner.hide();
            }
        );
    }

    saveShift(){
      const body={
        orgUid:this.userData.orgUniqueId,
        createdOrUpdatedByUid: this.userData.uniqueId,
        userId: this.userId,
        shiftsLists:this.saveList,
        userUid: ""
      }
      this.spinner.show();
      this._educatersService.addShift(body).subscribe(
          response => {
            console.log("response",response)
            this.shift=response.shiftsLists;
              this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            
              this.spinner.hide();
          }
      );
    }

    isSave(item){
      console.log("item",item);
      this.saveList.forEach(element => {
        if(element.id === item.id){
          if(element.isLinked==false){
            element.isLinked=true;
          }else{
            element.isLinked=false;
          }
         
        }
       
      });
    }

}
