import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';

@Component({
  selector: 'app-mobile-verification-new',
  templateUrl: './mobile-verification-new.component.html',
  styleUrls: ['./mobile-verification-new.component.scss']
})
export class MobileVerificationNewComponent implements OnInit {

  mobileVerifyForm: FormGroup;
  otpVerifyForm: FormGroup;
  submitted = false;
  otpSubmitted = false;
  error = '';
  success = '';
  loading = false;
  isMobileVerify = false;
  isOtpValid = false;
  isMobileVerifySuccesss = false;
  subscriptionDto:any;
  isMobileDisabled=false;
  timeLeft: number = 0;
  show: boolean = false;
  otpValid: boolean = false;
  otpResponse: boolean = false;
  interval;
  timesToMobileVerify=1;
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private _authService: AuthServiceService, private router: Router) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state;
    
    if(!state){
      this.router.navigate(['/account/signup']);
    }else{
      this.subscriptionDto=state;
    }
   // sessionStorage.setItem('timesToMobileVerify', "1");
   }

  ngOnInit() {
    this.mobileVerifyForm = this.formBuilder.group({
      mobile: ['', [Validators.required, Validators.pattern("^[0-9]{10}$")]]
    });
    this.otpVerifyForm = this.formBuilder.group({
      // otpNumber: ['', [Validators.required, Validators.pattern("[0-9]*")]]
      digit1: ['', [Validators.required]],
      digit2: ['', [Validators.required]],
      digit3: ['', [Validators.required]],
      digit4: ['', [Validators.required]],
      digit5: ['', [Validators.required]],
      digit6: ['', [Validators.required]],
    });
  }

  get f() { return this.mobileVerifyForm.controls; }
  get fa() { return this.otpVerifyForm.controls; }
  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;
    this.error=''

    // stop here if form is invalid
    if (this.mobileVerifyForm.invalid) {
      return;
    }

    if (this.interval) {
      clearInterval(this.interval);
    }

    this.loading = true;
    let mobile = this.mobileVerifyForm.controls['mobile'].value;
    this.subscriptionDto.mobile= mobile;
    this._authService.generateOtpNew(this.subscriptionDto).subscribe((res: any) => {
        console.log('response of otp verification====>>>', res);
        if(res){
          console.log('true response');
          if(res.isOtpVerified){
            this.router.navigate(['/account/organization-type/'],{state:this.subscriptionDto});
          }else{
            setTimeout(() => {
              this.loading = false;
              // this.isOtpValid = true
              // this.router.navigate(['/account/confirm']);
              this.isMobileVerify = true;
              this.isMobileDisabled=true;
              this.mobileVerifyForm.disable()
              this.timeLeft=120
              this.startTimer();
            }, 1000);
          }
        }else{
          console.log('false response');
          this.error='Mobile no already exists'
        }
      },error=>{
          this.loading = false;
          this.error='Mobile no already exists'
      })

    console.log(this.mobileVerifyForm.value);
   
  }
  onOtpSubmit() {
    this.otpSubmitted = true;
    this.error=''
    // stop here if form is invalid
    if (this.otpVerifyForm.invalid) {
      this.isOtpValid=false;
      return;
    } else {
      this.isOtpValid=true;
      this.loading = true;
      let mobile = this.mobileVerifyForm.controls['mobile'].value;
      let otp = this.otpVerifyForm.controls['digit1'].value+this.otpVerifyForm.controls['digit2'].value+this.otpVerifyForm.controls['digit3'].value+this.otpVerifyForm.controls['digit4'].value+this.otpVerifyForm.controls['digit5'].value+this.otpVerifyForm.controls['digit6'].value;
      this.subscriptionDto.mobile= mobile;
      this.subscriptionDto.otp= otp;
      // console.log('data before otp verification==>>', data);
      this._authService.otpVerificationNew(this.subscriptionDto).subscribe((res: any) => {
        console.log('response of otp verification====>>>', otp);
      // var res=false
        if(res){
          this.otpResponse=true
          this.otpValid=true
          sessionStorage.setItem('tokenId', res.token);
          sessionStorage.setItem('currentUser', JSON.stringify(res));
        //   console.log('true response');
          setTimeout(() => {
          this.router.navigate(['/account/organization-type/'],{state:res});
        }, 1000);
        this.loading = false;

        }else{
          // console.log('false response');
          this.otpResponse=true
          this.otpValid=false
          // this.error='Otp expired!'
          this.loading = false;
        }
      },(err)=>{
        //this.error=err;
        this.otpResponse=true
        this.otpValid=false
        this.loading = false;
      })
    }
  }
  clickToLogin() {
    this.router.navigate(['/account/login']);
  }
  resendOtp(){
    // let userOtpDetailsDto = new UserOtpDetailsDto();
    // userOtpDetailsDto.userId = this.userId;
    // userOtpDetailsDto.phoneNumber = this.phoneNumber;
    this.show = true;
    this.loading = true;

    this._authService.generateOtpNew(this.subscriptionDto).subscribe(
      (res) => {
          this.loading = false;
          this.show = false;
          this.timeLeft = 120;
      },
      (err: HttpErrorResponse) => {
        if (err instanceof HttpErrorResponse) {
         // console.log("Client-side error occured.");
        } else {
          this.loading = false;
          this.timeLeft = 0;
          // this.toastService.ShowError(error);
          this.error=err
        }
      });
  }
  startTimer() {
    this.timeLeft=120
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      }else{
        clearInterval(this.interval);
      }
    },1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }
  onChangeEvent(event){
    // this.isMobileVerify= false
    this.error=''
    if (!this.mobileVerifyForm.invalid) {
      console.log("mobileVerifyForm",this.mobileVerifyForm);
      // this.isMobileDisabled= true
    }
    
  }
  mobileChange(){
    this.isMobileDisabled=false;
    this.isMobileVerify=false;
    this.submitted=false;
    this.otpSubmitted=false;
    this.otpValid=false;
    this.otpResponse=false;
    this.mobileVerifyForm.enable();
    this.mobileVerifyForm.controls['mobile'].setValue('');
    this.otpVerifyForm.reset();
  }
  getCodeBoxElement(index) {
    return document.getElementById('otpDigit' + index);
  }
  onKeyUpEvent(index, event) {
    const eventCode = event.which || event.keyCode;
    if ((this.getCodeBoxElement(index) as HTMLInputElement).value.length === 1) {
      if (index !== 6) {
        (this.getCodeBoxElement(index+ 1) as HTMLInputElement).focus();
      } else {
        (this.getCodeBoxElement(index) as HTMLInputElement).blur();
        // Submit code
        console.log('submit code ');
      }
    }
    if (eventCode === 8 && index !== 1) {
      (this.getCodeBoxElement(index - 1) as HTMLInputElement).focus();
    }
    if (!this.otpVerifyForm.invalid) {
      this.isOtpValid=true;
    }else{
      this.otpResponse=false
    }
  }
  onFocusEvent(index) {
    for (var item = 1; item < index; item++) {
      const currentElement = this.getCodeBoxElement(item);
      
      if (!(currentElement as HTMLInputElement).value) {
          (currentElement as HTMLInputElement).focus();
          break;
      }
    }
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  
}
