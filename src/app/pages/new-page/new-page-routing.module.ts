import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'newSetup1', loadChildren: () => import('../apps/setup-new-ui/setup-new-ui.module').then(m => m.SetupNewUIModule) },
  { path: 'learners', loadChildren: () => import('../apps/new-learner/new-learner.module').then(m => m.NewLearnerModule) },
  { path: 'educator', loadChildren: () => import('../apps/new-educator/educators.module').then(m => m.EducatorsModule) },
  { path: 'curriculumn', loadChildren: () => import('../apps/new-courseplanner/new-courseplanner.module').then(m => m.NewCourseplannerModule) },
  { path: 'exam', loadChildren: () => import('../apps/newexam/newexam.module').then(m => m.NewexamModule) },
  { path: 'academic', loadChildren: () => import('../apps/new-academic/new-academic.module').then(m => m.NewAcademicModule) },
  { path: 'curriculumnNew', loadChildren: () => import('../apps/new-courseplanner/new-courseplanner.module').then(m => m.NewCourseplannerModule) },
  { path: 'workTime', loadChildren: () => import('../apps/new-worktime/new-worktime.module').then(m => m.NewWorktimeModule) },
  { path: 'timeTable', loadChildren: () => import('../apps/new-time-table/new-time-table.module').then(m => m.NewTimeTableModule) },
  { path: 'assesment', loadChildren: () => import('../apps/new-assesment/new-assesment.module').then(m => m.NewAssesmentModule) },
  { path: 'announcement', loadChildren: () => import('../apps/new-announcement/new-announcement.module').then(m => m.NewAnnouncementModule) },
  { path: 'organization', loadChildren: () => import('../apps/new-organization/new-organization.module').then(m => m.NewOrganizationModule) },
  { path: 'attendance', loadChildren: () => import('../apps/new-attendance/new-attendance.module').then(m => m.NewAttendanceModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewPageRoutingModule { }
