import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { PaperSubjects } from "./paper-subjects.model";
import { Subjects } from './subjects.model';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
@Component({
  selector: 'app-modifypaper',
  templateUrl: './modifypaper.component.html',
  styleUrls: ['./modifypaper.component.scss']
})
export class ModifypaperComponent implements OnInit {
  value:any;
  examUid: any;
  public spinner= false;
  examDetails: any;
  paperSubjects = new PaperSubjects();
  constructor(config: NgbModalConfig,private route: ActivatedRoute, private toastService: ToastsupportService) { 
    config.backdrop = 'static';
    config.keyboard = false;
    this.route.params.subscribe(params => {
      this.examUid = params['examId'];
  });
  console.log('examuid recieved' + this.examUid)
  this.paperSubjects.examUid = this.examUid;
  this.paperSubjects.updatedBy = sessionStorage.getItem('uniqueId')
  this.loadMathjaxConfig();
  }

  ngOnInit() {

    this.examPaper(this.examUid);
  }


  renderMathjax() {

    setTimeout(() => {
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, 'questions-panel']);
    }, 1000);
  }
  
  loadMathjaxConfig() {
  
      MathJax.Hub.Config({
          showMathMenu: false,
          tex2jax: { inlineMath: [["$", "$"]], displayMath: [["$$", "$$"]] },
          menuSettings: { zoom: "Double-Click", zscale: "150%" },
          CommonHTML: { linebreaks: { automatic: true } },
          "HTML-CSS": { linebreaks: { automatic: true } },
          SVG: { linebreaks: { automatic: true } },
          displayAlign: "left"
      });
  }
  examPaper(examUid: any) {
    // this.spinner = true;
    // this.userStudentService.getExamPaper(examUid).subscribe(
    //   (res) => {
    //     this.spinner = false;
        
    //     if (res) {
    //       this.examDetails = res
    //       let examPaper = res;
    //       if(examPaper.isAlreadyModified == true){
    //         this.paperSubjects.subjects = examPaper.examDetails.subjects;
    //       }else{

    //         examPaper.examDetails.examquestionjson.forEach(subjects => {
    //           let sectionsArray = [];
    //           subjects.sectionQuestionDtos.forEach(sections => {
    //             let questionsArray = [];
    //               sections.questionDtos.forEach(questions => {
    //                 let question = {
    //                   isModified: false,
    //                   isAnswerCorrected: false,
    //                   isQuestionBonus: false,
    //                   bonusMarksFor: 0,
    //                   id: questions.id,
    //                   question:  questions.question,
    //                   questionType:  questions.questionType,
    //                   uniqueId:  questions.uniqueId,
    //                   options:  questions.options,
    //                   answers:  questions.answers,
    //                 }
    //                 questionsArray.push(question)
    //               });       
    //               let section = {
    //                 id: sections.id,
    //                 sectionName: sections.sectionName,
    //                 questionType: sections.questionType,
    //                 questionMarkingDto: sections.questionMarkingDto,
    //                 optionalQuestions: sections.optionalQuestions,
    //                 questionDtos: questionsArray,
    //                 markingSchemeDtos: sections.markingSchemeDtos
    //               }
    //               sectionsArray.push(section)    
    //           });
    //           let dto = {
    //             subjectName: subjects.subjectName,
    //             subjectUid: subjects.subjectUid,
    //             sectionQuestionDtos: sectionsArray,
    //           }
    //           this.paperSubjects.subjects.push(dto);
    //         });
    //       }
         
    //       // console.log(JSON.stringify(examPaper));
    //       this.renderMathjax();
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       this.spinner = false;
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.spinner = false;
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }

  submitPaperChanges(){
    // this.spinner = true;
    // this.userStudentService.submitPaperChanges(this.paperSubjects).subscribe(
    //   (res) => {
    //     this.spinner = false;
        
    //     if (res) {

    //         console.log('res paper subnit ' + res)
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       this.spinner = false;
    //       console.log('Client-side error occured.');
    //     } else {
    //       this.spinner = false;
    //       this.toastService.ShowError(error);
    //     }
    //   });
  }
  onChangeAnswer(event, subjectUid, sectionunId, questionuniqueId, optionuniqueId){
    console.log(event.currentTarget.checked+ subjectUid+ sectionunId+ questionuniqueId+ optionuniqueId)
   
      this.paperSubjects.subjects.forEach(subjects => {
        if(subjects.subjectUid == subjectUid){
          subjects.sectionQuestionDtos.forEach( sections =>{
            if(sections.id == sectionunId){
                sections.questionDtos.forEach( questions =>{
                  if(questions.uniqueId == questionuniqueId){
                    questions.options.forEach(options => {
                        if(options.uniqueId == optionuniqueId){
                          options.correct = true; 
                        }else{
                          options.correct = false;
                        }
                    });
                  }
                })
            }
          })
        }
      });
  }

  onChangeAnswerCorrection($event, subjectUid, sectionunId, questionuniqueId){

    console.log(event+ subjectUid+ sectionunId+ questionuniqueId)
    this.paperSubjects.subjects.forEach(subjects => {
      if(subjects.subjectUid == subjectUid){
        subjects.sectionQuestionDtos.forEach( sections =>{
          if(sections.id == sectionunId){
              sections.questionDtos.forEach( questions =>{
                if(questions.uniqueId == questionuniqueId){
                  questions.isAnswerCorrected = true;
                  $event.srcElement.checked = true;
                  questions.isQuestionBonus = false;
                }
              })
          }
        })
      }
    
    });
  }

  onChangeQuestionBonus($event, subjectUid, sectionunId, questionuniqueId, questionType){

    console.log(event+ subjectUid+ sectionunId+ questionuniqueId)
    this.paperSubjects.subjects.forEach(subjects => {
      if(subjects.subjectUid == subjectUid){
        subjects.sectionQuestionDtos.forEach( sections =>{
          if(sections.id == sectionunId){
              sections.questionDtos.forEach( questions =>{
                if(questions.uniqueId == questionuniqueId){
                  questions.isAnswerCorrected = false;
                  $event.srcElement.checked = true;
                  questions.isQuestionBonus = true;

                  if(questionType == 2){
                    questions.bonusMarksFor = 3
                  }
                }
              })
          }
        })
      }
    
    });
  }

  onChangeAnswerModified($event, subjectUid, sectionunId, questionuniqueId){

    console.log(event+ subjectUid+ sectionunId+ questionuniqueId)
    this.paperSubjects.subjects.forEach(subjects => {
      if(subjects.subjectUid == subjectUid){
        subjects.sectionQuestionDtos.forEach( sections =>{
          if(sections.id == sectionunId){
              sections.questionDtos.forEach( questions =>{
                if(questions.uniqueId == questionuniqueId){
                  questions.isAnswerCorrected = false;
                  questions.isQuestionBonus = false;
                }
              })
          }
        })
      }
    
    });
  }

  
}
