import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SubjectServiceService {

  constructor(private http: HttpClient) { }

  public getCourseHasSubject(type) {

    return this.http.get<any>(environment.apiUrl + '/academic-coursehassubject/get/all/' + type);
  }


  public getMasterSubjects() {

    return this.http.get<any>(environment.apiUrl + '/master/get/subject');

  }

  getSubjectList() {
    return this.http.get<any>(environment.apiUrl + '/academic-subject/getall');
  }

  addUpdateMasterSubjects(selectedSubjectList) {
    return this.http.post<any>(environment.apiUrl + '/academic-subject/addorupdate', selectedSubjectList);
  }
  updateMasterSubjects(selectedSubjectList) {
    return this.http.post<any>(environment.apiUrl + '/academic-subject/add', selectedSubjectList);
  }

  addCourseHasSubject(selectedSubjectList) {
    return this.http.post<any>(environment.apiUrl + '/academic-coursehassubject/add', selectedSubjectList);
  }

  updateCourseHasSubject(selectedSubjectList) {
    return this.http.put<any>(environment.apiUrl + '/academic-coursehassubject/update', selectedSubjectList);
  }

  deleteCourseHasSubject(uniqueId) {
    return this.http.delete<any>(environment.apiUrl + '/academic-coursehassubject/delete/' + uniqueId);
  }

  deleteElectiveGroup(uniqueId) {
    return this.http.delete<any>(environment.apiUrl + '/elective-subject-group/delete/' + uniqueId);
  }
  getCourseSubjects(courseUniqueId) {
    return this.http.get<any>(environment.apiUrl + '/academic-coursehassubject/get/by/course/' + courseUniqueId);
  }
  public getCoursHasSubjectByUid(uniqueId) {

    return this.http.get<any>(environment.apiUrl + '/academic-coursehassubject/get/' + uniqueId);

  }
  public getSubjectByBatchUid(uniqueId) {

    return this.http.get<any>(environment.apiUrl + '/academic-subject/get/bybatch/' + uniqueId);

  }
  getSubjectsByCourse(courseUniqueId) {
    return this.http.get<any>(environment.apiUrl + '/academic-coursehassubject/get/subjects/' + courseUniqueId);
  }
  getSubjectsByCourses(courseUIds) {
    return this.http.post<any>(environment.apiUrl + '/academic-coursehassubject/get/subject', courseUIds);
  }

  getMasterCommonSUbjects(courseUniqueId) {
    return this.http.get<any>(environment.apiUrl + '/master/get/commonSubjects/' + courseUniqueId);
  }

  getpatternCategorySubjects(uniqueId) {
    return this.http.get<any>(environment.newEaasUrl + '/patternCategory/get/subject/' + uniqueId);
  }
}
