import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { ClassroomServiceService } from 'src/app/servicefiles/classroom-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';

@Component({
  selector: 'app-subtitute-leture',
  templateUrl: './subtitute-leture.component.html',
  styleUrls: ['./subtitute-leture.component.scss']
})
export class SubtituteLetureComponent implements OnInit {
  @Input() showData: any;
  @Input() type: any;
  @Input() timetableId: any;
  @Output() closeSubtituteLecture = new EventEmitter<object>();
  dataForSubstitution: any;
  childType: any;
  substituteForm: FormGroup;
  submitted = false;
  subjectList = [];
  teacherList = [];
  classroomList = [];
  isConflictsTeacher: boolean;
  isConflictsClassroom: boolean;
  isRemoveLecture: boolean;
  isViewConflict: boolean = false;
  viewConflictObject = { typeName: "Classroom" };
  conflictData: any;
  selectedClassroomId: string;
  viewConflictType: number;
  constructor(private toastService: ToastsupportService,
    private timeTableService: TimeTableServiceService,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private userService: UserServiceService,
    private classRoomService: ClassroomServiceService,
    private subjectService: SubjectServiceService,) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['type'] && changes['type'].currentValue == 1) {
      this.childType = 1;
      console.log("showData", this.showData)
      this.initAssignLectureForm();
      this.getSubjectList();
      this.getClassRooms();
    }
    this.dataForSubstitution = this.showData;
    console.log("this.dataForSubstitution", this.dataForSubstitution)
  }



  checkConflict() {
    let teacherIds = [];
    teacherIds.push(this.substituteForm.controls['teacherId'].value)
    const obj = {
      period: this.dataForSubstitution.periodId,
      day: this.dataForSubstitution.orgWorkDayId,
      timetableId: this.timetableId,
      teacherIds: teacherIds
    }
    this.timeTableService.getConflicts(obj).subscribe(
      res => {
        if (res) {

          console.log("res", res)
          if (res.success) {
            this.conflictData = res;
            if (res.conflicts[0].isBusy == true) {
              this.isConflictsTeacher = true;
            } else {
              this.isConflictsTeacher = false;
            }

            if (res.conflictClassRoomIds.length > 0) {
              res.conflictClassRoomIds.forEach(element => {
                if (this.selectedClassroomId == element) {
                  this.isConflictsClassroom = true;
                }
              });

            } else {
              this.isConflictsClassroom = false;
            }


          } else {
            this.isConflictsTeacher = false;
            this.isConflictsClassroom = false;
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  viewConflict(conflictType) {
    let conflictData = []
    this.viewConflictObject['conflictType'] = conflictType;
    if (conflictType == 1) {
      this.viewConflictObject.typeName = "Teacher";
      conflictData = this.conflictData.conflicts;
      this.viewConflictObject['conflictData'] = conflictData;
    } else {
      for (let i = 0; i < this.conflictData.conflicts.length; i++) {
        const element = this.conflictData.conflicts[i];
        if (element.isBusy) {
          let lectures = [];
          let details = element;
          for (let j = 0; j < element.lectureList.length; j++) {
            const element2 = element.lectureList[j];
            if (element2.classRoomId == this.selectedClassroomId) {
              lectures.push(element2)
            }
          }
          details.lectureList = lectures;
          conflictData.push(details);
        }
      }
      this.viewConflictObject.typeName = "Classroom";
      this.viewConflictObject['conflictData'] = conflictData;
    }
    switch (this.type) {
      case 2:
        this.viewConflictType = 3;
        this.viewConflictObject['conflictData'] = conflictData;
        break;
      case 4:
        this.viewConflictType = 3;
        break;
    }
    this.isViewConflict = true;
  }

  changeClassRoom(event) {
    this.selectedClassroomId = ""
    this.isConflictsClassroom = false;
    this.selectedClassroomId = event.uniqueId
    console.log('selected teacher', this.substituteForm.controls['teacherId'].value)
    if (this.substituteForm.controls['teacherId'].value != null) {
      this.checkConflict();
    }
  }

  initAssignLectureForm() {
    this.substituteForm = this.formBuilder.group({
      subjectId: [null, Validators.required],
      classroomId: [null, Validators.required],
      teacherId: [null, Validators.required],

    });
  }
  get f() { return this.substituteForm.controls; }



  getTeachersListBySubject(event) {
    let dto = {
      shiftId: this.showData.shiftUid,
      periodId: this.showData.periodId,
      orgDayId: this.showData.orgWorkDayId,
      subjectId: event.uniqueId
    }
    this.timeTableService.getTeacherListBySubjectDto(dto).subscribe(
      res => {
        if (res && res.success) {

          this.teacherList = res.list;
        } else {
          this.toastService.ShowError(res.responseMessage);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }

  getSubjectList() {
    this.spinner.show();
    return this.subjectService.getSubjectList().subscribe(
      res => {
        if (res.success == true) {
          console.log("res sub", res)
          this.subjectList = res.list;
        } else {
          this.toastService.ShowWarning('No Subjects found')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  getClassRooms() {
    this.spinner.show();
    return this.classRoomService.getAcademicClassRoom().subscribe(
      res => {
        console.log("class room", res)
        if (res.success == true) {
          this.classroomList = res.list;
        } else {
          this.toastService.ShowWarning('Class room not found')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  saveSubstituteForm(type) {
    this.submitted = true;

    if (this.substituteForm.invalid) {
      return false;
    }
    let teachers = [];
    teachers.push(this.substituteForm.value.teacherId);
    let batchgroups = [];
    this.dataForSubstitution.groupIds.forEach(element => {
      batchgroups.push(element.uniqueId)
    });
    const obj = {
      classroomUniqueId: this.substituteForm.value.classroomId,
      courseUniqueId: this.dataForSubstitution.courseId,
      lectureDate: this.datePipe.transform(this.dataForSubstitution.substituteDate, 'yyyy-MM-dd'),
      subjectUniqueId: this.substituteForm.value.subjectId,
      teacherUniqueIdList: teachers,
      timetableUniqueId: this.timetableId,
      substitutionType: 2,
      cardHasLessonUniqueId: this.dataForSubstitution.uniqueId,
      substitutionUniqueId: this.dataForSubstitution.substitutionId,
      isReplace: (type == 3) ? true : false,
      batchGroupUniqueIdList: batchgroups
    }

    if (type == 3) {
      let replaceLessons = []
      for (let i = 0; i < this.conflictData.conflicts.length; i++) {
        const element = this.conflictData.conflicts[i];
        if (element.isBusy) {
          for (let j = 0; j < element.lectureList.length; j++) {
            const element2 = element.lectureList[j];
            if (element2.substitutionId) {
              replaceLessons.push(element2.substitutionId);
            } else {
              replaceLessons.push(element2.uniqueId)
            }

          }
        }
      }
      obj['replaceLessons'] = replaceLessons
    }
    this.cancelNdSubstituteLecture(obj)
    console.log("obj", obj);



  }

  cancelSubstitute() {
    this.closeSubtituteLecture.emit({ param1: 'false', param2: '' });
  }

  cancelNdSubstituteLecture(obj) {
    this.spinner.show();
    return this.timeTableService.addExtraLeture(obj).subscribe(
      res => {
        if (res.success == true) {

          this.closeSubtituteLecture.emit({ param1: 'cancellecture', param2: '' });
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  removeLecture(type) {
    this.isRemoveLecture = true;
  }

  removeCancel() {
    this.isRemoveLecture = false;
  }

  removeAction() {
    this.isRemoveLecture = false;
    this.closeSubtituteLecture.emit({ param1: 'false', param2: '' });

  }


  closeConflict(event) {
    this.isViewConflict = false;
  }
}
