import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatepatternexamviewComponent } from './createpatternexamview.component';

describe('CreatepatternexamviewComponent', () => {
  let component: CreatepatternexamviewComponent;
  let fixture: ComponentFixture<CreatepatternexamviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatepatternexamviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatepatternexamviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
