import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../core/models/auth.models';
import { CookieService } from './cookie.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http: HttpClient, private cookieService: CookieService) { }
  user: User;
  isDomainChanged = new Subject<any>();
    /**
     * Returns the current user
     */
     public currentUser(): User {
      if (!this.user) {
          //this.user = JSON.parse(this.cookieService.getCookie('currentUser'));
          this.user = JSON.parse(sessionStorage.getItem('currentUser'));
      }
      return this.user;
  }

  /**
   * Logout the user
   */
  logout() {
      // remove user from local storage to log user out
      this.cookieService.deleteCookie('currentUser');
      sessionStorage.removeItem('currentUser');
      sessionStorage.removeItem('entityAccessRights');
      sessionStorage.clear();
      this.user = null;
  }


  getLoginOtp(data: any) {
    return this.http.post<any>(environment.authUrl + '/login/web/admin/get/otp', data);
  }

  verifyLoginOtp(data: any) {
    return this.http.post<any>(environment.authUrl + '/login/web/admin/verify/otp', data);
  }

  generateOtpNew(userData: any) {
    return this.http.post<any>(environment.authUrl + '/signup/generate/otp', userData);
  }

  otpVerificationNew(userData: any) {
    return this.http.post<any>(environment.authUrl + '/signup/verify/otp', userData);
  }

  getRoles() {
    return this.http.get<any>(environment.authUrl + '/signup/get/roles');
  }

  createOrganization(orgData: any) {
    return this.http.put<any>(environment.authUrl + '/setup/update/organization/name/role', orgData);
  }
  organizationRegistration(userData: any) {
    return this.http.post<any>(environment.apiUrl + '/user/organization/registration', userData);
  }

  signUpNew(userData: any) {
    return this.http.post<any>(environment.authUrl + '/signup/user/details/registration', userData);
  }
}
