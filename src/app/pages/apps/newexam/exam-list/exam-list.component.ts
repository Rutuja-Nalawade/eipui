import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ExamService } from '../exam.service';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import Swal from 'sweetalert2';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-exam-list',
  templateUrl: './exam-list.component.html',
  styleUrls: ['./exam-list.component.scss']
})
export class ExamListComponent implements OnInit {
  pageIndex: number = 0;
  examData: any;
  examList = [];
  defaultRows:any='10';
  rowList:any=[{id:'10',name:'10/Row'},{id:'20',name:'20/Row'},{id:'30',name:'30/Row'}]
 
  userListInvigilator = [];
  invigilatorForm: FormGroup;
 
  isExamListFound: boolean = false;
  totalExams:any;
  deleteExamId: string;
  
  constructor(private examService: ExamService,private spinner1: NgxSpinnerService, private fb: FormBuilder, private toastService:ToastsupportService, private modalService:NgbModal,private router: Router) {
  }

  ngOnInit() {
    this.getExamList();
  
    this.initInvigilatorForm();

    
  }
  pageChanged(page: number) {
    this.pageIndex = page - 1;
    this.getExamList();
  }

  public getExamList() {
  this.spinner1.show()
    this.examList = [];
    // return this.examService.getPaginationExamList(this.userData.orgUniqueId, this.pageIndex).subscribe(
    return this.examService.getPaginationExamList(sessionStorage.getItem('organizationId'), this.pageIndex).subscribe(
      res => {
        console.log("res", res);
        if (res) {

          this.examData = res;
          this.examList = res['content'];
          this.totalExams=res['totalElements']
          this.isExamListFound = true;
          console.log("Exam" + JSON.stringify(this.examData));
          this.spinner1.hide()
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   this.spinner1.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner1.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
        this.spinner1.hide()
      });
  }

  assignInvigilator(content: string, examAssignmentUid: string) {
    console.log("examAssignmentUid",examAssignmentUid)
    if (examAssignmentUid != null && examAssignmentUid != '') {
      this.invigilatorForm.controls['examAssignmentUid'].setValue(examAssignmentUid);
    }
    this.userListInvigilator = [];
    let serachDto = {
      "orgList": [(sessionStorage.getItem('orgUniqueId'))],
      "roleList": [3, 4, 5, 6, 7],
      "isForAll": false
    };
    return this.examService.getSearchedByUserData(serachDto).subscribe(
      res => {
        if (res) {
          this.userListInvigilator = res;
          this.modalService.open(content, { centered: true });
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error.error.errorMessage);
        // }
      });
  }

  saveInvigilator() {
    return this.examService.assignInvigilator(this.invigilatorForm.value).subscribe(
      res => {
        this.toastService.showSuccess("Invigilator assigned successfully..");
        this.invigilatorForm.reset()
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error.error.errorMessage);
        // }
      });
  }

  close(){
    this.invigilatorForm.reset()
  }
  courseId: any;
 

  initInvigilatorForm() {
    this.invigilatorForm = this.fb.group({
      id: ['', Validators.nullValidator],
      examAssignmentUid: ['', Validators.required],
      userId: [null, Validators.required],
      createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.required],
    });
  }

  cancel() {
    $("#assignInvigilatorModal").modal("hide")
    this.invigilatorForm.reset();
  }

 routeToExamSetter(examId: string) {
    this.router.navigate(['/newModule/exam/track/create', examId]);
   
  }

  routeToViewTracks(examId: string) {
    this.router.navigate(['/newModule/exam/track/info', examId]);
  }

  openCancelExam(centerDataModal: string, examId: string) {
    this.deleteExamId = examId;
    this.modalService.open(centerDataModal, { centered: true });
  }

  cancelExam() {
    this.spinner1.show();
    this.examService.cancelExam(this.deleteExamId).subscribe(
      res => {
        if (res) {

          this.pageIndex = 0;
          this.getExamList();
          this.toastService.showSuccess("Exam cancel successfully")
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner1.hide();
        // } else {
        //   this.spinner1.hide();
        //   // this.toaterService.ShowWarning(error.error.errorMessage);
        // }
        // this.toastService.ShowError("Exam cancel unsuccessfully")
        this.spinner1.hide()
      });

  }
}
