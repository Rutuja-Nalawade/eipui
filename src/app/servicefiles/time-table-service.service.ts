import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimeTableServiceService {

  constructor(private http: HttpClient) { }

  getTimetableList(type, pageindex) {
    return this.http.get<any>(environment.newApiUrl + '/academic-timetable/get/list/' + pageindex + '/' + type);
  }
  getPeriodList(dto: any) {
    return this.http.post<any>(environment.newApiUrl + '/academic-timetable/get/batch-periods', dto);
  }
  assignBatchPeriod(dto) {
    return this.http.post<any>(environment.newApiUrl + '/academic-timetable/assign/batchperiods', dto);
  }
  getTeacherListBySubjectDto(dto) {
    return this.http.post<any>(environment.apiUrl + '/subject-teacher/by/subjectperiodandshiftday', dto);
  }
  assignLecture(dto) {
    return this.http.post<any>(environment.newApiUrl + '/academic-timetable/add/lecture', dto);
  }
  getConflicts(dto) {
    return this.http.post<any>(environment.newApiUrl + '/timetable-conflict/get/conflicts', dto);
  }
  saveBatchShifts(dto: any) {
    return this.http.post<any>(environment.url + 'eicoreservices/batch-shift/add', dto);
  }
  updateBatchShifts(dto: any) {
    return this.http.put<any>(environment.url + 'eicoreservices/batch-shift/update', dto);
  }
  saveTimetable(dto: any) {
    return this.http.post<any>(environment.url + 'eicoreservices/academic-timetable/add', dto);
  }
  getBatchList(pageIndex: any) {
    return this.http.get<any>(environment.url + 'eicoreservices/batch-shift/get/coursewithassignedbatchshifts/' + pageIndex);
  }
  applyExicution(dto) {
    return this.http.post<any>(environment.newApiUrl + '/academic-timetable/publish', dto);
  }
  getPreApplyExicution(dto) {
    return this.http.post<any>(environment.newApiUrl + '/academic-timetable/get/previous/execution', dto);
  }
  getExicutionHistory(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/academic-timetable/get/execution/history/' + uniqueId);
  }
  getTimeTableData(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/academic-timetable/get/' + uniqueId);
  }

  getSubstitutionDayData(body) {
    console.log("date test", body)
    return this.http.post<any>(environment.newApiUrl + '/substitution/slotwise/get', body);
  }

  getSubstitutionWeekData(body) {
    console.log("date test", body)
    return this.http.post<any>(environment.newApiUrl + '/substitution/datewise/get', body);
  }

  getInitialSubstitutionLecture(body) {
    console.log("date test", body)
    return this.http.post<any>(environment.newApiUrl + '/substitution/get/initial/lecture', body);
  }

  addExtraLeture(body) {

    console.log("obj service", body);
    return this.http.post<any>(environment.newApiUrl + '/substitution/add', body);
  }
  getLectureData(uid) {
    return this.http.get<any>(environment.newApiUrl + '/academic-timetable/get/lecture/' + uid);
  }
  removeLecture(uid) {
    return this.http.put<any>(environment.newApiUrl + '/substitution/remove/lecture/' + uid, []);
  }
  updateLecture(dto) {
    return this.http.put<any>(environment.newApiUrl + '/academic-timetable/update/lecture', dto);
  }
  deleteLecture(dto) {
    return this.http.delete<any>(environment.newApiUrl + '/substitution/delete/' + dto);
  }

  getViewAllChanges(obj) {
    return this.http.post<any>(environment.newApiUrl + '/substitution/list/get', obj);
  }
  publishedChanges(obj) {
    return this.http.put<any>(environment.newApiUrl + '/substitution/publish', obj);
  }
  rescheduleLecture(obj) {
    return this.http.post<any>(environment.newApiUrl + '/substitution/reschedule/lecture', obj);
  }
  reActiveLecture(obj) {
    return this.http.post<any>(environment.newApiUrl + '/substitution/reactivate/lecture', obj);
  }
  //Imports
  //period
  checkPeriods(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-period/check', dto);
  }
  linkPeriods(dto) {
    return this.http.put<any>(environment.newApiUrl + '/import-period/link', dto);
  }
  //subject
  getSubjectLink(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-subject/get', dto);
  }
  linkUnlinkSubjects(dto, url) {
    return this.http.post<any>(environment.newApiUrl + url, dto);
  }
  unlinkSubject(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-subject/unlink', dto);
  }
  //unlinkSubject
  getLinkSubject(type) {
    return this.http.get<any>(environment.apiUrl + '/academic-coursehassubject/get/all/' + type);
  }
  addLinkSubject(dto) {
    return this.http.post<any>(environment.apiUrl + '/academic-coursehassubject/add', dto);
  }
  updateLinkSubject(dto) {
    return this.http.put<any>(environment.apiUrl + '/academic-coursehassubject/update', dto);
  }
  //Batch
  getBatchLink(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-batch/get', dto);
  }
  linkBatch(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-batch/link', dto);
  }
  unlinkBatch(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-batch/unlink', dto);
  }
  addAndLinkBatch(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-batch/add/and/link', dto);
  }
  //Classroom
  getClassroomLink(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-classroom/get', dto);
  }
  addLinkClassroom(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-classroom/add/and/link', dto);
  }
  linkClassroom(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-classroom/link', dto);
  }
  unlinkClassroom(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-classroom/unlink', dto);
  }
  //Teacher
  getTeacherLink(dto) {
    return this.http.put<any>(environment.newApiUrl + '/import-teacher/check', dto);
  }
  linkTeacher(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-teacher/link', dto);
  }
  unlinkTeacher(dto) {
    return this.http.put<any>(environment.newApiUrl + '/import-teacher/unlink', dto);
  }

  addAndLinkTeachers(dto) {
    return this.http.put<any>(environment.newApiUrl + '/import-teacher/addandlink', dto);
  }

  getTeacherList(dto) {
    return this.http.post<any>(environment.apiUrl + '/teacher/list/filter', dto);
  }

  assignShiftToBatch(dto) {
    return this.http.put<any>(environment.newApiUrl + '/timetable-shift/assignto/batchandteacher', dto);
  }
  checkAssignedCheck() {
    return this.http.get<any>(environment.newApiUrl + '/timetable-shift/check');
  }

  getAssignedShifts() {
    return this.http.get<any>(environment.newApiUrl + '/timetable-shift/check');
  }

  //BatchGroup
  getBatchGroupLink(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-batchgroup/get', dto);
  }
  checkBatchGroupLink(dto) {
    return this.http.put<any>(environment.newApiUrl + '/import-batchgroup/check', dto);
  }
  linkBatchGroup(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-batchgroup/link', dto);
  }
  addLinkBatchGroup(dto) {
    return this.http.put<any>(environment.newApiUrl + '/import-batchgroup/addandlink', dto);
  }
  unlinkBatchGroup(dto) {
    return this.http.post<any>(environment.newApiUrl + '/import-batchgroup/unlink', dto);
  }
  importxmlToTimetable(dto) {
    return this.http.put<any>(environment.newApiUrl + '/import-timetable/xml', dto);
  }
}