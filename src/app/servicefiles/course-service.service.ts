import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { env } from 'process';

@Injectable({
  providedIn: 'root'
})
export class CourseServiceService {

  constructor(private http: HttpClient) { }

  public getCourseBasicInfoList(type) {
    return this.http.get<any>(environment.apiUrl + '/academic-course/get/basic/info/' + type);
  }
  getMasterCourseList() {
    return this.http.get(environment.apiUrl + "/master/get/course")
  }
  getCourseUnlinkedList() {
    return this.http.get<any>(environment.apiUrl + "/academic-coursehassubject/unlinked");
  }

  getCourseDetailsedInfoList(type) {
    return this.http.get<any>(environment.apiUrl + '/academic-course/get/detailed/info/' + type);
  }

  addCourse(courseDetails) {
    return this.http.post<any>(environment.apiUrl + '/academic-course/add', courseDetails);
  }

  updateCourse(courseDetails) {
    return this.http.put<any>(environment.apiUrl + '/academic-course/update', courseDetails);
  }

  deleteCours(courseUniqueId) {
    return this.http.delete<any>(environment.apiUrl + '/academic-course/update/' + courseUniqueId);
  }
}
