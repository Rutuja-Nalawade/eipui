import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CourseplannerListComponent } from './courseplanner-list/courseplanner-list.component';
import { DeactivateGuard } from './deactivate.gaurd';


const routes: Routes = [
  {
    path: 'lists',
    component: CourseplannerListComponent
  },
  {
    path: 'list',
    component: CourseplannerListComponent,
    canDeactivate: [DeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewCourseplannerRoutingModule { }
