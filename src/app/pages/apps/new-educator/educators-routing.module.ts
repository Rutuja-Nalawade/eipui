import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EducatorComponent } from './educator/educator.component';


const routes: Routes = [
  {
    path: 'lists',
    component: EducatorComponent
  },
  {
    path: 'lists/:id',
    component: EducatorComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EducatorsRoutingModule { }
