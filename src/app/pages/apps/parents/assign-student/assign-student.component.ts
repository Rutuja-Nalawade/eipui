import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-assign-student',
  templateUrl: './assign-student.component.html',
  styleUrls: ['./assign-student.component.scss']
})
export class AssignStudentComponent implements OnInit {

  phone: any = ['123456789', '234567891', '345678912'];
  bloodGroup: any = ['O+ (O Positive)', 'A+ (A Positive)', 'O- (O Negative)', 'A- (A Negative)'];
  relations: any = [{ id: 1, name: 'Mother' }, { id: 2, name: 'Father' }, { id: 3, name: 'Guardian' }];
  guardianList: any = [];
  isGuardian = true;
  isAssignStudent = false;
  dataLoading = false;
  isSearchResult = false;
  searchText: string;
  guardianForm: FormGroup;
  // studentForms: FormGroup;
  studentForms = new Array<FormGroup>();
  userData: any;
  // studentDataList: any = [];
  currentUserId;
  createdOrUpdatedBy: any;
  alreadyExist = false;
  submitted = false;
  assignSubmitted = false;
  studentIndex: Number;
  activeTab = 0;
  isEditForm = false
  studentList: any = [];
  saveLinkedStudentForm: FormGroup
  remainingStudents = 3
  relationArray = ["Mother", "Father", "Guardian"]
  modalReference: NgbModalRef;
  ngbSmallModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false,
    size: 'sm',
    centered: true,
    windowClass: 'assignstd'
  };
  assignStudentData: any;
  editStudentIndex: any
  @ViewChild('assignStudent', { static: true }) assignStudent: ElementRef;
  @Input() userId: Number;
  @Input() currentTab: Number;
  @Input() public userUniqueId: Number;
  @Input() public userDetailsType: any;
  @Input() public studentDataList: any;
  constructor(private formBuilder: FormBuilder, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private modalService: NgbModal, private _DomSanitizationService: DomSanitizer, private _userService: UserServiceService) {
    this.userData = this._commonService.getUserData();

  }

  ngOnInit() {
    // this.getGuardian();
    // this.initStudentsForms()
    this.createdOrUpdatedBy = this.userData.uniqueId;
  }
  ngOnChanges(changes: SimpleChanges) {

    if (changes['userDetailsType']) {
      this.currentUserId = this.userId;
      this.initSaveLinkedStudentForms();
      // this.studentList = <FormArray>this.saveLinkedStudentForm.controls['studentList'];
      // this.getRoleInfo();
      // this.getChlidrens(this.userId)
      if (this.studentDataList && this.studentDataList.length > 0) {
        let studentListData = this.studentDataList;

        let studentDtoList = <FormArray>this.saveLinkedStudentForm.controls['studentList'];
        if (studentListData != null && studentListData.length > 0) {
          for (let i = 0; i < studentListData.length; i++) {
            let studentsDto = this.formBuilder.group({
              "uniqueId": studentListData[i].uniqueId,
              "name": studentListData[i].name,
              "relation": studentListData[i].relation.toString(),
              "studentId": studentListData[i].studentId,
              "profileImageId": studentListData[i].profileImageId,
              "isLinked": true
            })
            studentDtoList.push(studentsDto);
          }
          // this.studentList = this.studentDataList
          if (this.studentDataList.length <= 3) {
            this.remainingStudents = (3 - this.studentDataList.length)
          } else {
            this.remainingStudents = this.studentDataList.length
          }
        }
      } else {
        this.studentDataList = []
      }
    }
  }
  initSaveLinkedStudentForms() {
    this.saveLinkedStudentForm = this.formBuilder.group({
      "parentUniqueId": [''],
      "parentId": [this.currentUserId],
      "createdOrUpdatedBy": this.userData.uniqueId,
      "studentList": this.formBuilder.array([])

    })

  }
  onAssignStudent(item) {
    if (item == 'guardian') {
      this.isGuardian = true;
      this.isAssignStudent = false;
    } else if (item == 'parent') {
      this.isGuardian = false;
      this.isAssignStudent = true;
    }
  }


  addGuardian(data) {
    console.log(this.studentDataList, data);
    if (this.studentDataList && this.studentDataList.length != 0) {
      const found = this.studentDataList.some(el => el.uniqueId === data.uniqueId);
      if (found) {
        this.toastService.ShowWarning("This student already assigned!")
        return;
      } else {
        this.assignStudentData = data
        this.assignStudentData['relation'] = null;
        this.assignStudentData['isLinked'] = false;
        this.isSearchResult = false
        this.modalReference = this.modalService.open(this.assignStudent, this.ngbSmallModalOptions);
      }

    } else if (this.studentDataList && this.studentDataList.length == 0) {
      this.assignStudentData = data
      this.assignStudentData['relation'] = null;
      this.assignStudentData['isLinked'] = false;
      this.isSearchResult = false
      this.modalReference = this.modalService.open(this.assignStudent, this.ngbSmallModalOptions);
    }
  }

  getChlidrens(userId) {
    this.spinner.show();
    this._userService.getGuardianChildrens("guardian", this.userUniqueId).subscribe(
      response => {
        if (response && response.length > 0) {
          this.studentDataList = response;
          if (response.length <= 3) {
            this.remainingStudents = (3 - response.length)
          } else {
            this.remainingStudents = response.length
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }



  findRecords(value: any) {
    let data = {
      val: 'searchBar',
      searchDetails: value
    }
    // this.loading = true;
    if (value != null && value != undefined && value.length > 3) {

      // this.getGuardianRecords(data.searchDetails);
      this.searchUsers(value);
    }
  }

  searchUsers(value) {
    this._userService.searchUsers("student", value)
      .subscribe(data => {
        if (data != null && data.length > 0) {
          // this.totalCount=data.length;
          this.guardianList = data;
          for (let i = 0; i < data.length; i++) {
            const element = data[i];
            this._userService.getImage("guardian", element.uniqueId, false).subscribe(
              response => {
                if (response != null && response) {
                  if (response.image) {
                    this.guardianList[i]['profileImageId'] = response.image;
                  }
                }
              });
          }
          this.isSearchResult = true;
        } else {
          this.guardianList = [];
          this.toastService.ShowWarning("User not found!");
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  generateRandomKey() {
    let randomkey = Math.floor((Math.random() * 10000) + 1);
    return randomkey;
  }
  initStudentGeneralInfoForm() {
    return this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{2,45}')])],
      lastName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{2,45}')])],
      birthDate: [null, Validators.nullValidator],
      gender: ['', Validators.nullValidator],
      bloodGroup: ['', Validators.nullValidator],
      adharNumber: ['', Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      phoneNumber: ['', Validators.compose([Validators.required])],
      emailId: ['', Validators.required, Validators.email],
      // loginId: ['', Validators.required],
      relation: ['', Validators.required]
    })
  }
  initStudentInfoForm() {
    return this.formBuilder.group({
      uniqueId: [this.generateRandomKey()],
      studentUniqueId: ['', Validators.required],
      organizationUniqueId: ['', Validators.nullValidator],
      parentUniqueId: ['', Validators.nullValidator],
      createdOrUpdatedByUniqueId: [this.userData.uniqueId, Validators.nullValidator],
      generalInfo: this.initStudentGeneralInfoForm(),
      studentId: [this.currentUserId],
      organizationId: [this.userData.organizationId],
      parentId: [null]
    });
  }
  addMoreStudent() {
    if (this.studentForms.length <= 2) {
      this.studentForms.push(this.initStudentInfoForm());
      var tabNumber = this.studentForms.length
      this.activeTab = tabNumber + 1
    }
  }
  initStudentsForms() {
    this.studentForms.push(this.initStudentInfoForm());
  }
  getParentFormIndex(index: number) {
    this.studentIndex = index;
  }
  removeStudent(index: number) {
    let uniqueId = this.studentForms[index].controls['uniqueId'].value;
    this.studentForms = this.studentForms.filter(x => x.controls['uniqueId'].value != uniqueId);
    this.activeTab = 0
  }
  editStudent(item, i) {
    this.assignStudentData = item
    this.editStudentIndex = i
    this.modalReference = this.modalService.open(this.assignStudent, this.ngbSmallModalOptions);
  }
  deleteStudent(item: any, index) {
    Swal.fire({
      text: 'Are you sure want to delete Learner?',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        let studentDtoList = <FormArray>this.saveLinkedStudentForm.controls['studentList'];
        let studentList = studentDtoList.value;
        let stdentData = studentList[index]
        this.spinner.show()
        // if (stdentData.isLinked) {
        // let student = {
        //   "parentId": this.currentUserId,
        //   "createdOrUpdatedBy": this.createdOrUpdatedBy,
        //   "studentList": [studentList[index]]
        // }
        this._userService.deleteGuardian("student", item.relationUniqueId).subscribe(
          response => {
            if (response) {
              this.toastService.showSuccess("Student delete successfully");
              studentDtoList.removeAt(index)
              this.studentDataList.splice(index, 1)
              if (this.studentDataList.length <= 3) {
                this.remainingStudents = (3 - this.studentDataList.length)
              } else {
                this.remainingStudents = this.studentDataList.length
              }
            }
            this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            // } else {
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide()
          }
        );
        // } else {
        //   studentDtoList.removeAt(index)
        //   this.studentDataList.splice(index, 1)
        //   this.spinner.hide()
        //   if (this.studentDataList.length <= 3) {
        //     this.remainingStudents = (3 - this.studentDataList.length)
        //   } else {
        //     this.remainingStudents = this.studentDataList.length
        //   }
        // }
      }
    })
  }
  onAddStudent() {

  }
  close() {
    this._commonService.iscloseClicked.next(true)
  }
  saveStudents() {
    this._commonService.iscloseClicked.next(true);
    // this.spinner.show()
    // this._parentService.saveRelation(this.saveLinkedStudentForm.value).
    //   subscribe(response => {
    //     this.toastService.showSuccess("All Relation saved successfully");
    //     this.spinner.show();
    //     setTimeout(() => {
    //       this._commonService.iscloseClicked.next(true)
    //     }, 600);
    //     (error: HttpErrorResponse) => {
    //       if (error instanceof HttpErrorResponse) {
    //       } else {
    //         this.toastService.ShowError(error);
    //       }
    //       this.spinner.show()
    //     }
    //   })
  }
  assignStd() {
    this.assignSubmitted = true;
    console.log(this.assignStudentData);
    if (this.assignStudentData.relation) {
      this.spinner.show()
      let studentListData = this.assignStudentData;
      let studentDtoList = <FormArray>this.saveLinkedStudentForm.controls['studentList'];

      let dto = {
        "fullName": this.assignStudentData.fullName,
        "uniqueId": this.userUniqueId,
        "relation": this.assignStudentData.relation
      }
      this._userService.saveGuardian("student", dto, this.assignStudentData.uniqueId).subscribe(
        response => {
          this.toastService.showSuccess("Relation saved successfully");
          this.getChlidrens(12);
          this.spinner.hide()
          this.modalService.dismissAll()
          this.assignSubmitted = false;
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.assignSubmitted = false;
          this.spinner.hide();
        }
      );


    }
  }
}
