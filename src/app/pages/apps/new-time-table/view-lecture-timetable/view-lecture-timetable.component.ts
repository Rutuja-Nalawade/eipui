import { Component, EventEmitter, OnInit, Output, Input, ViewChild, SimpleChanges } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-lecture-timetable',
  templateUrl: './view-lecture-timetable.component.html',
  styleUrls: ['./view-lecture-timetable.component.scss']
})
export class ViewLectureTimetableComponent implements OnInit {
  isJeeAdd: boolean = true;
  isJeeMains: boolean = false;
  isJeeNeet: boolean = false;
  isJeeBoard: boolean = false;
  itemList = [{ id: 0, name: 'All' }, { id: 1, name: 'Classroom' }, { id: 2, name: 'Batch' }, { id: 3, name: 'Teacher' }, { id: 4, name: 'Subject' }];
  shiftList = [{ id: 0, name: 'Shift M1' }, { id: 1, name: 'Shift M2' }, { id: 2, name: 'Shift M3' }]
  typeList1 = [{ id: 0, name: 'All' }];
  typeListDef = [0];
  itemListDef = [0];
  shiftListDef = [0]
  isNewLecture = false;
  childtype = 2;
  type = 4;
  isactiveTab = 1;
  isCancelLecture: boolean;
  initialDetails: any;
  courseList = [{ id: 1, name: "JEE Advanced", isActive: true }, { id: 2, name: "JEE Mains", isActive: false }, { id: 3, name: "NEET", isActive: false }, { id: 4, name: "Boards", isActive: false }];
  @ViewChild('myselect1', { static: true }) myselect1;
  @ViewChild('myselect2', { static: true }) myselect2;
  @ViewChild('myselect3', { static: true }) myselect3;

  @Input() mainType: any;
  typeList = [{ id: 1, name: "Classroom", isActive: true }, { id: 2, name: "Batch", isActive: false }, { id: 3, name: "Subject", isActive: false }, { id: 4, name: "Teacher", isActive: false }];
  dateList = [];

  isRightSidePopup = false;
  popupType = 1;
  isPopHover = false;
  viewData = {};

  @Output() newItemEvent = new EventEmitter<string>();
  isAddNew: boolean = false;

  //vinayak
  @Input() periodData: any;
  @Input() weekDates: any;
  @Output() close = new EventEmitter<string>();
  @Input() timetableId: any;
  @Input() selectedDate: any;
  @Input() dateObj: any;
  @Input() timetableName: any;
  isSubtitutePopup: boolean = false;
  createNewLectureDate: any;
  lectureType: any;
  editLectureData: any;
  dataForShow: any;
  isBlurBack: boolean = false;
  objDate: any;
  isRescheduleLecture = false;
  isReschedule: boolean;
  isActivteLecture: boolean;
  constructor(private spinner: NgxSpinnerService,
    private toastService: ToastsupportService,
    private timetableService: TimeTableServiceService,
    private router: Router) {

  }


  ngOnInit() {


  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['periodData']) {
      console.log("periodData", this.periodData)
    }
  }

  addMore() {
    this.type = 4;
    this.childtype = 2;
    this.isNewLecture = true;
  }

  cancel() {
    this.close.emit("");
    this.router.navigate(['/newModule/timeTable/lists/timeTable']);
  }

  close1(value) {
    this.newItemEvent.emit(value);
  }
  closeAddLectureForm(event) {
    this.isNewLecture = false;
    this.isRightSidePopup = false;
    this.isCancelLecture = false;
    if (this.popupType == 6 && this.childtype == 2 && event == "1") {
      this.type = 5;
      this.isNewLecture = true;
    }
    if (event == "2") {
      this.popupType = 6;
      this.childtype = 2;
      this.isRightSidePopup = true;
    }
  }
  closeViewConflict() {

  }
  activeTab(index) {
    this.isactiveTab = index;
  }
  editLecture() {
    this.type = 4;
    this.childtype = 3;
    this.isNewLecture = true;
  }
  deleteLecture() {

  }
  itemHover() {
    this.isPopHover = true;
  }
  actionPopupView(event) {
    switch (event) {
      case "1":
        this.type = 4;
        this.childtype = 3;
        this.isNewLecture = true;
        break;
      case "2":




        break;
      case "3":
        this.isRightSidePopup = true;
        this.popupType = 1;
        break;
      case "4":
        this.type = 5;
        this.isNewLecture = true;
        break;
      case "5":
        this.isRightSidePopup = true;
        this.popupType = 4;
        break;
    }
  }
  openPopup(type) {
    if (type == 1) {
      this.popupType = 5;
    } else if (type == 2) {
      this.popupType = 6;
      this.childtype = 1;
    } else {
      this.popupType = 6;
      this.childtype = 2;
    }
    this.isRightSidePopup = true;
  }

  changeType(event) {
    console.log(event)

    if (event.id == 0) {
      this.typeList1 = [{ id: 0, name: 'All' }]

    } else if (event.id == 1) {
      this.typeList1 = [{ id: 0, name: 'Classroom 1' }, { id: 1, name: 'Classroom 2' }, { id: 2, name: 'Classroom 3' }]
    } else if (event.id == 2) {
      this.typeList1 = [{ id: 0, name: 'Batch 1' }, { id: 1, name: 'Batch 2' }, { id: 2, name: 'Batch 3' }]
    } else if (event.id == 3) {
      this.typeList1 = [{ id: 0, name: 'Teacher 1' }, { id: 1, name: 'Teacher 2' }, { id: 2, name: 'Teacher 3' }]
    } else if (event.id == 4) {
      this.typeList1 = [{ id: 0, name: 'Subject 1' }, { id: 1, name: 'Subject 2' }, { id: 2, name: 'Subject 3' }]
    }

  }

  //vinayak
  actionView(type, data, date, selectedDate, orgWorkDayId, shiftUid) {
    data.orgWorkDayId = orgWorkDayId;
    data.substituteDate = date;
    data.selectedDate = selectedDate;
    data.shiftUid = shiftUid;
    this.dataForShow = data;
    console.log(this.dataForShow);
    this.type = type;
    if (type == 1) {
      this.isSubtitutePopup = true;
    } else if (type == 8) {

      this.isCancelLecture = true;
    } else if (type == 10) {

      this.isRescheduleLecture = true;
    } else if (type == 11) {
      this.isBlurBack = true;
      this.isActivteLecture = true;
    } else {
      this.isRightSidePopup = true;
    }
  }

  closeSubtituteLecture(event1, event2) {
    this.isRightSidePopup = false;
    this.isCancelLecture = false;
    this.isActivteLecture = false;
    this.isSubtitutePopup = false;
    this.isRescheduleLecture = false;
    this.isBlurBack = false;
    if (event1 == 'reschedulelecture') {
      this.isReschedule = true;
      this.initialDetails = event2;
    }

    if (event1 == 'cancellecture') {
      this.close.emit("refreshData");
    } else if (event1 == 'subtitutelecture') {
      this.isSubtitutePopup = true;
      this.dataForShow = event2;
    }
    if (event1 == 'reactivateLecture') {
      this.close.emit("refreshData");
    }
  }

  addNewLecture(date, type, dataEdit, data) {
    console.log(type);
    console.log(data);
    if (type == 1) {
      dataEdit = {
        shiftUid: data.shiftuniqueId,
        orgWorkDayId: data.orgWorkDayId,
        periodId: data.uniqueId,
      }
      this.editLectureData = dataEdit;
    } else {
      dataEdit.shiftUid = data.shiftuniqueId;
      dataEdit.orgWorkDayId = data.orgWorkDayId;
      this.editLectureData = dataEdit;
    }

    this.lectureType = type
    this.createNewLectureDate = date
    this.isAddNew = true;
  }

  closeAddNewForm(event) {
    console.log(event);
    this.isAddNew = false;
  }

  isBlurBackGround(event) {
    this.isBlurBack = event;
  }

  viewAllChanges(type) {
    if (type == 3) {
      this.type = 7;
      this.isRightSidePopup = true;
      this.popupType = 7;
      this.objDate = this.dateObj;

    } else {
      this.isRightSidePopup = false;
      this.close.emit("0");
    }
  }
  cancelReset(type) {
    if (type == 1) {
      let obj = {
        initialSubstitutionUniqueId: (this.initialDetails.substitutionId) ? this.initialDetails.substitutionId : null,
        newSubstitutionUniqueId: this.dataForShow.substitutionId
      }
      this.spinner.show();
      return this.timetableService.rescheduleLecture(obj).subscribe(
        res => {
          if (res && res.success) {
            this.toastService.showSuccess(res.responseMessage)
            this.isReschedule = false;
            this.close.emit("refreshData");
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
          console.log("res", res)
          //  this.initialDetails=res;
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error)
          // }
          this.spinner.hide();
        });
    } else {
      this.isReschedule = false;
    }
  }

}
