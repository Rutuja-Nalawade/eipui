import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import IterResult from 'rrule/dist/esm/src/iterresult';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-preview-xml-timetable',
  templateUrl: './preview-xml-timetable.component.html',
  styleUrls: ['./preview-xml-timetable.component.scss']
})
export class PreviewXmlTimetableComponent implements OnInit {
  @Input() xmlData: any;
  itemList = [{ id: 0, name: 'Classroom' }, { id: 1, name: 'Batch' }, { id: 2, name: 'Teacher' }];
  filterList = [];
  selectedSubjects = []
  selectedSubjectList = [];
  selectedFilterDataValue: any;
  selectedfilterDataUId: any;
  classroomList: any[];
  batchList: any[];
  teacherList: any[];
  selectedFilterType: any;
  isChecked = false;

  @Output() savePreview = new EventEmitter<string>()
  @Output() closePreiew = new EventEmitter<string>()
  constructor(private _toastService: ToastsupportService) { }
  subjectList = [];
  title: any;
  colorClassArray = ['color1', 'color2', 'color3', 'color4'];
  filteredLEcture: any;
  lecturesAllData: any;
  indexValue = 0;
  ngOnInit() {
    // console.log('xmlData' + JSON.stringify(this.xmlData))
    this.setTimeTableData(this.xmlData);
    this.setSubjects(this.xmlData['subjects']);
    this.setTeachers(this.xmlData['teachers'])
    this.setBatches(this.xmlData['classes'])
    this.setClassRoom(this.xmlData['classrooms'])

  }
  setTimeTableData(xmlData: any) {
    let lectures = [];
    xmlData['cards'].forEach(card => {
      let classroomName = "";
      let classroomUniqueId = "";
      let periodName = "";
      let periodId = "";
      let periodStartTime = "";
      let periodEndTime = "";
      let letureId = "";
      let teacherName = "";
      let teacherUniqueId = "";
      let subjectName = "";
      let subjectUniqueId = "";
      let batchName = "";
      let batchId = "";
      let batchGroupName = "";
      let batchGroupId = "";
      if (card.classroomids != "") {
        xmlData['classrooms'].forEach(classroom => {
          classroomName = classroom.xmlName;
          classroomUniqueId = classroom.xmlId
        });
      }
      //get days informations
      let daysObject = this.getDays(card.days);

      //get period information
      xmlData['periods'].forEach(period => {
        if (period.period == card.period) {
          periodName = period.name;
          periodId = period.period;
          periodStartTime = period.starttime;
          periodEndTime = period.endtime
          return
        }
      });

      xmlData['lessons'].forEach(lesson => {
        if (card.lessonid == lesson.id) {
          letureId = lesson.id;
          xmlData['subjects'].forEach(subject => {
            if (lesson.subjectid == subject.id) {
              subjectName = subject.name;
              subjectUniqueId = subject.id;
              return
            }
          });

          xmlData['teachers'].forEach(teacher => {
            if (lesson.teacherids == teacher.id) {
              teacherName = teacher.name;
              teacherUniqueId = teacher.id;
              return
            }
          });
          xmlData['classes'].forEach(batc => {
            if (batc.id == lesson.classids) {
              batchName = batc.xmlName;
              batchId = batc.id;
              return
            }
          });

          xmlData['batchgroups'].forEach(batchgroup => {
            if (batchgroup.id == lesson.groupids) {
              batchGroupName = batchgroup.xmlName;
              batchGroupId = batchgroup.id;
              return
            }
          });

        }
      });

      lectures.push({
        classroomName: classroomName,
        classroomUniqueId: classroomUniqueId,
        periodName: periodName,
        periodId: periodId,
        periodStartTime: periodStartTime,
        periodEndTime: periodEndTime,
        letureId: letureId,
        teacherName: teacherName,
        teacherUniqueId: teacherUniqueId,
        subjectName: subjectName,
        subjectUniqueId: subjectUniqueId,
        weekDay: daysObject.day,
        weekDayValue: daysObject.dayValue,
        batchName: batchName,
        batchId: batchId,
        batchGroupName: batchGroupName,
        batchGroupId: batchGroupId
      })
    });

    this.lecturesAllData = lectures

  }
  getDays(days: any) {
    let result;
    switch (days) {
      case '1000000':
        result = { dayValue: 1, day: 'Monday' }
        break;
      case '0100000':
        result = { dayValue: 2, day: 'Tuesday' }
        break;
      case '0010000':
        result = { dayValue: 3, day: 'Wednesday' }
        break;
      case '0001000':
        result = { dayValue: 4, day: 'ThursDay' }
        break;
      case '0000100':
        result = { dayValue: 5, day: 'Friday' }
        break;
      case '0000010':
        result = { dayValue: 6, day: 'Saturday' }
        break;
      case '0000001':
        result = { dayValue: 7, day: 'Sunday' }
        break;
    }
    return result;
  }

  sortDataByDay(data) {
    let sortedArray = data.sort((n1, n2) => {
      if (n1.dayValue > n2.dayValue) {
        return 1;
      }

      if (n1.dayValue < n2.dayValue) {
        return -1;
      }

      return 0;
    });

    var groupBy = function (xs, key) {
      return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    };
    let groupbyperiodName = groupBy(sortedArray, 'weekDayValue');
    console.log('data sorted to show', JSON.stringify(groupbyperiodName))
    return groupbyperiodName;
  }
  setTeachers(teachers) {
    this.teacherList = teachers.reduce((unique, o) => {
      if (!unique.some(obj => obj.id === o.id && obj.name === o.name)) {
        unique.push(o);
      }
      return unique;
    }, []);
  }

  setBatches(classes) {
    let tempList = classes.reduce((unique, o) => {
      if (!unique.some(obj => obj.id === o.id && obj.name === o.name)) {
        unique.push(o);
      }
      return unique;
    }, []);
    let temp2 = [];
    tempList.forEach(element => {
      temp2.push({
        id: element.id,
        name: element.xmlName
      })
    });
    this.batchList = temp2
    this.selectedFilterType = 1;
    this.changeFilterMaster({ id: 1, name: 'batch' });
  }

  setClassRoom(classrooms) {
    let tempList = classrooms.reduce((unique, o) => {
      if (!unique.some(obj => obj.id === o.id && obj.name === o.name)) {
        unique.push(o);
      }
      return unique;
    }, []);
    let temp2 = [];
    tempList.forEach(element => {
      temp2.push({
        id: element.id,
        name: element.xmlName
      })
    });
    this.classroomList = temp2
  }

  setSubjects(subjects) {
    // console.log('setSubjects' + JSON.stringify(subjects))
    this.subjectList = subjects.reduce((unique, o) => {
      if (!unique.some(obj => obj.id === o.id && obj.name === o.name)) {
        unique.push(o);
      }
      return unique;
    }, []);
    let tempSelectedSubjectsList = [];
    // // for ui chips show;
    let tempSelectedSubjects = [];
    this.subjectList.forEach(subject => {
      tempSelectedSubjectsList.push(subject.id);
      tempSelectedSubjects.push(subject)
    });
    this.selectedSubjectList = tempSelectedSubjectsList;
    this.selectedSubjects = tempSelectedSubjects;
    // console.log('this.selectedSubjects ' + JSON.stringify(this.selectedSubjects))
  }

  removeSubject(item, index) {
    // console.log(item);
    // console.log(this.selectedSubjectList);
    if (this.isChecked == false) {
      this.selectedSubjects.splice(index, 1)
      this.selectedSubjectList = this.selectedSubjectList.filter(t => t !== item.id);
    } else {
      this._toastService.ShowWarning('Please uncheck show all timetable ')
    }

    // this.shiftChange({ uniqueId: this.selectedShiftUniqueId });
  }
  changeSubjects(event) {
    // console.log(event, this.selectedSubjectList);
    this.selectedSubjects = event;
    // this.shiftChange({ uniqueId: this.selectedShiftUniqueId })
  }
  checkShowAll() {
    if (this.isChecked == true) {
      this._toastService.ShowWarning('Please uncheck show all timetable ')
    }
  }

  changeFilterMaster(evt) {
    this.selectedFilterDataValue = null;
    // console.log(JSON.stringify(evt))
    switch (evt.id) {
      case 0:
        // this is for classroom list
        this.filterList = this.classroomList
        //set filter data first position
        this.selectedFilterDataValue = this.classroomList[0].id
        //get shift by first data element
        this.filterData({ id: this.classroomList[0].id, name: this.classroomList[0].name })
        break;
      case 1:
        // this is for batch list
        this.filterList = this.batchList
        //set filter data first position
        this.selectedFilterDataValue = this.batchList[0].id
        //get shift by first data element
        this.filterData({ id: this.batchList[0].id, name: this.batchList[0].name })
        break;

      case 2:
        // this is for Teacher list
        this.filterList = this.teacherList
        //set filter data first position
        this.selectedFilterDataValue = this.teacherList[0].id
        //get shift by first data element
        this.filterData({ id: this.teacherList[0].id, name: this.teacherList[0].name })
        break;
    }
  }

  filterData(evt) {
    this.title = evt.name
    console.log('filtertype' + this.selectedFilterType)
    this.selectedfilterDataUId = evt.id;
    let newProcessedData = [];
    let processJSON = JSON.stringify(this.lecturesAllData)
    let processJSON2 = []
    JSON.parse(processJSON).forEach(lecture => {
      if (this.selectedSubjectList.includes(lecture.subjectUniqueId)) {
        processJSON2.push(lecture)
      }
    });
    switch (this.selectedFilterType) {
      case 0:
        // this is for classroom list
        processJSON2.forEach(data => {
          if (data.classroomUniqueId == evt.id) {
            console.log('data found')
            newProcessedData.push(data)
          }
        });
        break;
      case 1:
        // this is for batch list
        // console.log('original data' + JSON.stringify(processJSON2));
        console.log('filterte batch uniqueId' + evt.id)
        processJSON2.forEach(data => {
          if (data.batchId == evt.id) {
            console.log('data found')
            newProcessedData.push(data)
          }
        });
        break;

      case 2:
        // this is for Teacher list
        processJSON2.forEach(data => {
          if (data.teacherUniqueId == evt.id) {
            console.log('data found')
            newProcessedData.push(data)
          }
        });
        break;
      default:

        break;
    }

    var groupBy = function (xs, key) {
      return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    };
    let groupbyperiodName = groupBy(newProcessedData, 'periodName');
    Object.keys(groupbyperiodName).forEach((key) => {
      let sortDataByDay = groupBy(groupbyperiodName[key], 'weekDayValue');
      const map = new Map()
      for (let i = 1; i < 8; i++) {
        let tempData = sortDataByDay[i]
        if (tempData == undefined) {
          sortDataByDay[i] = []
        }
      }
      groupbyperiodName[key] = sortDataByDay;
    });
    // console.log('final data ' + JSON.stringify(groupbyperiodName))
    this.filteredLEcture = groupbyperiodName;


  }

  ShowAllData(event) {
    if (this.isChecked) {

      var groupBy = function (xs, key) {
        return xs.reduce(function (rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
      };
      let groupbyperiodName = groupBy(this.lecturesAllData, 'periodName');
      Object.keys(groupbyperiodName).forEach((key) => {
        let sortDataByDay = groupBy(groupbyperiodName[key], 'weekDayValue');
        const map = new Map()
        for (let i = 1; i < 8; i++) {
          let tempData = sortDataByDay[i]
          if (tempData == undefined) {
            sortDataByDay[i] = []
          }
        }
        groupbyperiodName[key] = sortDataByDay;
      });
      // console.log('final data ' + JSON.stringify(groupbyperiodName))
      this.filteredLEcture = groupbyperiodName;

    } else {

      this.changeFilterMaster({ id: 1, name: 'batch' });

    }
  }
  cancel() {
    this.closePreiew.emit("");
  }
  viewAllChanges() {
    console.log("save");
    this.savePreview.emit("");
  }

  prevDetails(data) {
    console.log("prev", data)
    console.log("this.indexValue prev", this.indexValue)
    this.indexValue = this.indexValue - 1;
    console.log("this.indexValue next", this.indexValue)
  }

  nextDetails(data) {
    console.log("next", data)
    console.log("this.indexValue prev", this.indexValue)
    this.indexValue = this.indexValue + 1;
    console.log("this.indexValue next", this.indexValue)
  }
}
