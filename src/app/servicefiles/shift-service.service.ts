import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShiftServiceService {

  constructor(private http: HttpClient) { }

  getAllShifts() {
    return this.http.get<any>(environment.newApiUrl + '/shifts/get/all')
  }

  getAllShiftTypes() {
    return this.http.get<any>(environment.newApiUrl + '/shifts/get/shifttypes')
  }

  getShiftWorkingDays() {
    return this.http.get<any>(environment.newApiUrl + '/workingdays/get/true')
  }

  getShiftbasicDetails(typesArray, type) {
    return this.http.put<any>(environment.newApiUrl + '/shifts/get/basicdetails/' + type, typesArray)
  }

  addShiftWorkingDays(data) {
    return this.http.post<any>(environment.newApiUrl + '/shifts/add', data)
  }

  deleteShift(uniqueId) {
    return this.http.delete<any>(environment.newApiUrl + '/shifts/delete/' + uniqueId)
  }

  updateShiftWorkingDays(data) {
    return this.http.put<any>(environment.newApiUrl + '/shifts/update', data)
  }

  getShiftByBatch(uniqueId){
    return this.http.get<any>(environment.newApiUrl + '/batch-shift/get/' + uniqueId)
  }

  getShiftByTeacher(uniqueId){
    return this.http.get<any>(environment.newApiUrl + '/teacher/get/shift/' + uniqueId)
  }
  getShiftByClassroomAndTimeTable(classroomuniqueId, timeTableId){
    return this.http.get<any>(environment.newApiUrl + '/timetable-shift/get/byclassroomandtimetable/' + classroomuniqueId + '/' + timeTableId)
  }
//bulk assign
  public getSelectedUserBasicInfo(orgId: any) {
    return this.http.get<any>(environment.authUrl + '/get/shift/roles/by/org/' + orgId);
  }
  getSearchedByUserData(serachDto: any) {
    return this.http.post<any>(environment.apiUrl + '/get/users/byRoles', serachDto);
  }
  addBulkUserstoShift(UserHasShiftDto) {
    return this.http.post<any>(environment.apiUrl + '/link/users/shifts', UserHasShiftDto);
  }
}
