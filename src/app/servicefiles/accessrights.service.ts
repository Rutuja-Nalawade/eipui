import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from './cookie.service';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class AccessrightService {

    constants : Observable<any>;
    isLeftLoaded = new Subject<any>();
    isSetupDone = new Subject<boolean>();
    accessRightList : string;

    constructor(private http: HttpClient, private cookieService: CookieService) {
        this.getJSON().subscribe(data => {
            console.log(data);
            this.constants = data;
        });
    }


    public getJSON(): Observable<any> {
        return this.http.get("../../../assets/json/constants.json");
    }


    checkUserHasAuthorityToAccess(id: number) : Boolean {

        let accessRights : Array<any>;
        accessRights = JSON.parse(sessionStorage.getItem('entityAccessRights'));
    
        let entity = accessRights.find(entity => entity.id === id);
    
        if(entity.isAccess==true){
           console.log(true);
            return true;
        } else {
            console.log(false);
            return false;
        }
        // if (entity == null || entity == undefined || entity == 'undefined') { 
        //     console.log("ENtityFound");
        //     return false;
        // } else {
        //     return true;
        // }
        
    }


    checkUserHasAuthorityToAccessEntity(parentValue: number,childValue : number) : Boolean {

        let accessRights : any;
        accessRights = JSON.parse(sessionStorage.getItem('entityAccessRights'));
    
        let entity = accessRights.find(entity => entity.id === parentValue);
    
        if (entity == null || entity == undefined || entity == 'undefined') { 
            console.log("ENtityFound");
            return false;
        } else {
           let subEntity = entity.entityList.find(subEntity=> subEntity.id === childValue);
    
           if (subEntity == null || subEntity == undefined || subEntity == 'undefined') { 
               return false;
           }  else {
               return true;
           }  
        }
        
    }

    getCssClassToCrud(point: number): string {
        let cssClass : string = "";
        switch (point) {
            case 1: cssClass = "fas fa-plus-square pull-right";
                break;
            case 2: cssClass = "fab fa-readme pull-right";
                break;
            case 3: cssClass = "fas fa-edit pull-right";
                break;
            case 4:  cssClass = "fas fa-trash-alt";
                break;
            case 5:  cssClass = "fas fa-upload";
                break;
            case 6:  cssClass = "fas fa-download";
                break;
            case 7:  cssClass = "fas fa-skull-crossbones";
                break;
            case 8:  cssClass = "fas fa-allergies";
                break;

        }
        return cssClass;
    }


    getEntityListAccess(){
        return this.http.get<any>(environment.apiUrl + '/accessrights/get');
    }
} 