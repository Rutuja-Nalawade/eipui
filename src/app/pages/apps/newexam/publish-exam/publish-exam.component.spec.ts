import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishExamComponent } from './publish-exam.component';

describe('PublishExamComponent', () => {
  let component: PublishExamComponent;
  let fixture: ComponentFixture<PublishExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
