import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArchwizardModule } from 'angular-archwizard';
import { NewAssesmentRoutingModule } from './new-assesment-routing.module';
import { CommonHeaderComponent } from './common-header/common-header.component';
import { AssesmentExamComponent } from './assesment-exam/assesment-exam.component';
import { BatchdetailsComponent } from './batchdetails/batchdetails.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { ExamsyllabusComponent } from './examsyllabus/examsyllabus.component';
import { TimedetailsComponent } from './timedetails/timedetails.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ExamviewComponent } from './examview/examview.component';
import { PatternviewComponent } from './patternview/patternview.component';
import { ExamListComponent } from './exam-list/exam-list.component';
import { AddTracksComponent } from './add-tracks/add-tracks.component';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TrackInfoComponent } from './track-info/track-info.component';
import { MarkingSchemeComponent } from './marking-scheme/marking-scheme.component';
import { NgxEditorModule } from 'ngx-editor';
import { TruncatePipePipe } from './truncate-pipe.pipe';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { CreatePatternComponent } from './create-pattern/create-pattern.component';
import { QuestionBankComponent } from './question-bank/question-bank.component';
import { PublishAssesmentComponent } from './publish-assesment/publish-assesment.component';
import { NgxCollapseModule } from 'ngx-collapse';
import { AddStudentsComponent } from './add-students/add-students.component';
import { EditExamDataComponent } from './edit-exam-data/edit-exam-data.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
@NgModule({
  declarations: [CommonHeaderComponent, AssesmentExamComponent,BatchdetailsComponent,BasicInfoComponent, ExamsyllabusComponent, TimedetailsComponent, ExamviewComponent, PatternviewComponent, ExamListComponent, AddTracksComponent, TrackInfoComponent, MarkingSchemeComponent, TruncatePipePipe, CreatePatternComponent, QuestionBankComponent, PublishAssesmentComponent,AddStudentsComponent, EditExamDataComponent],
  imports: [
    CommonModule,
    NewAssesmentRoutingModule,
    ArchwizardModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgbModule,
    ToastrModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule, NgxEditorModule,
    NgxMaterialTimepickerModule,
    BsDatepickerModule.forRoot(),
    NgxCollapseModule,
    Ng2SearchPipeModule
  ]
})
export class NewAssesmentModule { }
