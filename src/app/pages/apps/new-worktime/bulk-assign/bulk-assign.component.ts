import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-bulk-assign',
  templateUrl: './bulk-assign.component.html',
  styleUrls: ['./bulk-assign.component.scss']
})
export class BulkAssignComponent implements OnInit {

  userData: any = {};
  organizationShifts = [];
  teacherList = [];
  currentUserId: string;
  searchByUserRoles: any;
  isSelectAll = false;
  selectedCount = 0;
  colorClassArray = ['primary', 'success', 'warning', 'info', 'danger'];
  shiftTypeList = ["Teaching", "Non-Teaching", "Both"];
  selectedRoleList = [];
  selectedRoles = [];
  selectedTeacherList = [];
  selectedShiftList = [];
  @Input() type: any;
  @Output() close = new EventEmitter<string>();
  constructor(private spinner: NgxSpinnerService, private _commonService: NewCommonService, private toastService: ToastsupportService, private workTimeServices: ShiftServiceService, private _DomSanitizationService: DomSanitizer) {
    this.userData = this._commonService.getUserData();
    this.getOrganizationShifts();
    this.getUserBasicInfo(this.userData.organizationId);
  }

  ngOnInit() {
    // this.getUserShiftInfo();
  }
  getOrganizationShifts() {
    this.spinner.show();
    return this.workTimeServices.getAllShifts().subscribe(
      res => {
        if (res) {
          if (res.list.length > 0) {
            this.organizationShifts = res.list;
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  getUserShiftInfo(teacherId: any) {
    this.currentUserId = teacherId;
    // this.getUserBasicInfo(teacherId);
  }
  getUserBasicInfo(userId: string) {
    this.workTimeServices.getSelectedUserBasicInfo(userId).subscribe(
      response => {
        this.searchByUserRoles = response;
        let rolList = [];
        if (response && response.length > 0) {
          rolList.push(response[0].id);
          this.selectedRoleList = rolList;
          this.getSearchedUserInfo();
        }
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);

        // }
      }
    );
  }
  onChange(event) {
    if (event && event.length > 0) {
      this.selectedRoleList = [];
      for (let index = 0; index < event.length; index++) {
        const element = event[index];
        this.selectedRoleList.push(element.id);
      }
      this.getSearchedUserInfo();
    }
  }
  shiftSlect(shift, i) {
    shift.isLinked = !shift.isLinked;
  }
  selectAllTeacher(event) {
    let teacherList = this.teacherList;
    if (event.target.checked) {
      let count = 0
      for (var i = 0; i < teacherList.length; i++) {
        teacherList[i].isLinked = true;
        count = count + 1
      }
      this.selectedCount = count;
    } else {
      let count = 0
      for (var i = 0; i < teacherList.length; i++) {
        teacherList[i].isLinked = false;
        count = count + 1
      }
      this.selectedCount = count;
    }
    this.isSelectAll = !this.isSelectAll
  }
  teacherChange(teacher) {
    if (!teacher.isLinked) {
      teacher.isLinked = true;
      this.selectedCount = this.selectedCount + 1;
    } else {
      teacher.isLinked = false;
      this.selectedCount = this.selectedCount - 1;
      this.isSelectAll = false
    }
  }
  assign() {
    let teacherList = this.teacherList.filter(x => x.isLinked == true);
    let shiftList = this.organizationShifts.filter(x => x.isLinked == true);
    if (teacherList.length == 0) {
      this.toastService.ShowWarning("Select atleast one user!");
    } else if (shiftList.length == 0) {
      this.toastService.ShowWarning("Select atleast one shift!");
    } else {
      for (let index = 0; index < teacherList.length; index++) {
        this.selectedTeacherList.push(teacherList[index].id);

      }
      let dto = {
        orgUid: this.userData.orgUniqueId,
        // userUid: this.userData.uniqueId'),
        createdOrUpdatedByUid: this.userData.uniqueId,
        shiftsLists: this.organizationShifts,
        userIdList: this.selectedTeacherList
      }
      this.spinner.show();
      this.workTimeServices.addBulkUserstoShift(dto)
        .subscribe(data => {
          this.toastService.showSuccess("Shift assign successfully.");
          this.cancel();
          this.spinner.hide();
          this.teacherList = [];
          this.selectedTeacherList = [];
          this.selectedShiftList = [];
          this.searchByUserRoles = [];
        },
          (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            //   console.log('client-side-error-occured')
            // } else {
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide();
          });
    }
  }
  cancel() {
    this.close.emit("");
  }
  getSearchedUserInfo() {
    if (this.selectedRoleList.length != 0 && this.selectedRoleList != null && this.selectedRoleList != undefined) {
      this.spinner.show();
      let searchDto = {
        "orgList": [this.userData.orgUniqueId],
        "roleList": this.selectedRoleList,
        "pageIndex": 0,
        'isForAll': false
      };
      this.teacherList = [];
      this.workTimeServices.getSearchedByUserData(searchDto).subscribe(
        response => {
          if (response != null && response != undefined) {
            this.teacherList = response;
            if (response.length > 0) {
              for (let index = 0; index < response.length; index++) {
                this.teacherList[index]['isLinked'] = false;
              }
              this.spinner.hide();
            }
            this.spinner.hide();
          } else {
            this.toastService.ShowWarning("Users Not found");
            this.spinner.hide();
          }
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }
    else {
      this.toastService.ShowError("please Select At least Single Role");
      this.teacherList = [];
    }

  }
}
