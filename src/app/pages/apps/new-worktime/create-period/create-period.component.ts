import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { PeriodServiceService } from 'src/app/servicefiles/period-service.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';

@Component({
  selector: 'app-create-period',
  templateUrl: './create-period.component.html',
  styleUrls: ['./create-period.component.scss']
})
export class CreatePeriodComponent implements OnInit {
  submitted = false;
  userData: any = {};
  slotLists = [];
  organizationShifts = [];
  selectedSlotId: string = null;
  slotForm: FormGroup;
  slotNewAddForm: FormGroup;
  slotAddForm: FormGroup;
  slotListForm: any;
  min = new Date();
  max = new Date();
  shiftUniqueId: any;
  isEditSlot = false;
  @Input() periodList: any;
  @Input() slotData: any;
  @Output() close = new EventEmitter<string>();
  constructor(private _commonService: NewCommonService, private formBuilder: FormBuilder, private toastService: ToastsupportService, private _periodService: PeriodServiceService, private _shiftService: ShiftServiceService, private datePipe: DatePipe, private spinner: NgxSpinnerService) {

  }

  ngOnInit() {
    this.initSlotAddForm();
  }
  ngOnChanges(changes: SimpleChanges) {

    if (changes['slotData'] && changes['slotData'].currentValue) {
      this.selectedSlotAssignToForm(this.slotData);
      this.isEditSlot = true;
    } else {
      this.getOrganizationShifts();
      this.addSlotToArray();
    }
  }
  initSlotAddForm() {
    this.slotAddForm = this.formBuilder.group({
      slotList: this.formBuilder.array([])
    });
  }
  initSlotForm() {
    return this.slotForm = this.formBuilder.group({
      name: ['', Validators.required],
      uniqueId: [null, Validators.nullValidator],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      duration: ['', Validators.nullValidator],
      shiftName: ['', Validators.nullValidator],
      shiftUniqueId: [null, Validators.required],
      min: [null, Validators.nullValidator],
      max: [null, Validators.nullValidator],
      isEditMode: [false, Validators.nullValidator],
      isStartTimeGreater: [false, Validators.nullValidator]
    });
  }
  selectedSlotAssignToForm(slot) {
    console.log('selected slots data =>' + JSON.stringify(slot))
    this.slotNewAddForm = this.formBuilder.group({
      name: [slot.name, Validators.required],
      uniqueId: [slot.uniqueId, Validators.nullValidator],
      endTime: [moment(slot.endTime, 'HH:mm').format("hh:mm A"), Validators.required],
      startTime: [moment(slot.startTime, 'HH:mm').format("hh:mm A"), Validators.required],
      duration: [slot.duration, Validators.nullValidator],
      shiftName: [slot.shiftName, Validators.nullValidator],
      shiftUniqueId: [slot.shiftUniqueId, Validators.required],
      // createdOrUpdatedBy: [this.userData.uniqueId, Validators.nullValidator],
      // orgUniqueId: [this.userData.orgUniqueId, Validators.required],
      min: [slot.min, Validators.nullValidator],
      max: [slot.max, Validators.nullValidator],
      isEditMode: [true, Validators.nullValidator],
      isStartTimeGreater: [false, Validators.nullValidator]
    });
    this.shiftUniqueId = slot.shiftUniqueId;
    this.getOrganizationShifts();
  }

  getOrganizationShifts() {
    let intarray = [1, 2, 3]
    return this._shiftService.getShiftbasicDetails(intarray, false).subscribe(
      res => {
        console.log("shift data =>", res)
        if (res) {
          this.organizationShifts = res;
          if (res.length > 0) {
            let shiftData = {};
            for (let index = 0; index < this.organizationShifts.length; index++) {
              const element = this.organizationShifts[index];
              console.log(this.shiftUniqueId, element.uniqueId);
              if (this.isEditSlot && this.shiftUniqueId == element.uniqueId) {
                shiftData = element;
                this.slotNewAddForm.controls.min.setValue(element.startTime);
                this.slotNewAddForm.controls.max.setValue(element.endTime);
                console.log(this.slotAddForm);
              }
              this.organizationShifts[index].shiftName = element.shiftName + " (" + element.startTime + "-" + element.endTime + ")";
            }
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }

  addSlotToArray() {
    this.submitted = false;
    // slotList.push(this.initSlotForm());
    this.slotNewAddForm = this.initSlotForm()
  }
  get f() { return this.slotNewAddForm.controls; }
  setShiftStartAndEndTimeForAddSlot($event: any) {
    if ($event && $event != undefined) {
      // let shift = this.organizationShifts.find(x => x.shiftUid == $event.shiftUid);
      console.log($event);
      let min = $event.startTime;
      let max = $event.endTime;

      let Slot = this.slotNewAddForm;
      Slot.controls['min'].setValue(min);
      Slot.controls['max'].setValue(max);
      // Slot.controls['startTime'].setValue(min);
      // Slot.controls['endTime'].setValue(max);
      console.log(this.slotNewAddForm);
    }
  }
  onChangeStartTime(startDateTime: any, isForUpdate: number) {
    let startTime = this.convertTime(startDateTime);
    let formGroup = this.slotNewAddForm;
    let endTime = this.convertTime(formGroup.controls['endTime'].value);
    if (startTime == endTime) {
      // console.log("equal");
    }

    if (startTime > endTime) {
      formGroup.controls['isStartTimeGreater'].setValue(true);
    } else {
      formGroup.controls['isStartTimeGreater'].setValue(false);
    }

    // alert(this.compareTimes(startTime,endTime));
  }

  onChangeEndTime(endDateTime: any, isForUpdate: number) {
    let endTime = this.convertTime(endDateTime);

    let formGroup = this.slotNewAddForm;
    let startTime = this.convertTime(formGroup.controls['startTime'].value);
    if (startTime == endTime) {
      // console.log("equal");
    }
    if (startTime > endTime) {
      formGroup.controls['isStartTimeGreater'].setValue(true);
    } else {
      formGroup.controls['isStartTimeGreater'].setValue(false);
    }
  }

  cancel() {
    this.submitted = false;
    this.close.emit("")
    this.isEditSlot = false;
  }
  saveSlots() {
    this.submitted = true;
    console.log(this.slotNewAddForm);
    if (this.slotNewAddForm.invalid) {
      return;
    }
    let slot = this.slotNewAddForm.value;

    let submitDto = {
      name: slot.name,
      startTime: this.convertTime(slot.startTime),
      endTime: this.convertTime(slot.endTime),
      shiftUid: slot.shiftUniqueId
    }
    console.log("period data =>", JSON.stringify(submitDto))
    if (slot.isStartTimeGreater) {
      this.toastService.ShowWarning("Start time is not greater than end time. Plase add start time greater than end time.")
      return false;
    }
    var slots = this.slotAddForm.value;
    slots.slotList.push(submitDto);
    this.spinner.show();
    this._periodService.savePeriod(submitDto).subscribe(
      res => {
        if (res) {
          this.toastService.showSuccess("Period added successfully.");
          this.spinner.hide();
          this._commonService.workTimeProcess.next(2);
          this.cancel();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError("Period updating failed.");
        // }
        this.spinner.hide();
      });
  }

  updateSlots() {
    this.submitted = true;
    console.log(this.slotNewAddForm);
    if (this.slotNewAddForm.invalid) {
      return;
    }
    let slot = this.slotNewAddForm.value;
    let submitDto = {
      name: slot.name,
      uniqueId: slot.uniqueId,
      startTime: this.convertTime(slot.startTime),
      endTime: this.convertTime(slot.endTime),
      shiftUid: slot.shiftUniqueId
    }
    console.log("period data =>", JSON.stringify(submitDto))
    if (slot.isStartTimeGreater) {
      this.toastService.ShowWarning("Start time is not greater than end time. Plase add start time greater than end time.")
      return false;
    }
    var slots = this.slotAddForm.value;
    slots.slotList.push(submitDto);
    this.spinner.show();
    this._periodService.updatePeriod(submitDto).subscribe(
      res => {
        if (res) {
          this.toastService.showSuccess("Period update successfully.");
          this.spinner.hide();
          this._commonService.workTimeProcess.next(2);
          this.cancel();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError("Period updating failed.");
        // }
        this.spinner.hide();
      });
  }
  convertTime(time) {
    return moment(time, 'hh:mm A').format('HH:mm');
  }
}
