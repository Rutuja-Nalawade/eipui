import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewPageRoutingModule } from './new-page-routing.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardsModule } from '../dashboards/dashboards.module';
import { AppsModule } from '../apps/apps.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgbDropdownModule,
    DashboardsModule,
    AppsModule,
    NewPageRoutingModule
  ]
})
export class NewPageModule { }
