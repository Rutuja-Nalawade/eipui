import { HttpErrorResponse, JsonpClientBackend, } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { AssesmentSetterServiceService } from 'src/app/servicefiles/assesment-setter-service.service';
import { AssesmentService } from 'src/app/servicefiles/assesment.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-create-pattern',
  templateUrl: './create-pattern.component.html',
  styleUrls: ['./create-pattern.component.scss']
})
export class CreatePatternComponent implements OnInit {
  @Input() quickExamForm: FormGroup;
  @Input() assignmentType: any;
  @Output() hideButtons = new EventEmitter<string>();
  selectedCategory: number;
  createCategoryForm: FormGroup;
  createPatternForm: FormGroup;
  createPatternTwoForm: FormGroup;
  patternForm: FormGroup;
  category = [];
  patterns: any = [];
  masterCourseList: any;
  subjectList = [];
  isPatternSetpOne = false;
  isPatternSetpTwo = false;
  questionTypes = [];
  questionCategories = [];
  isJEAdvancePaper = false;
  isMasterListIsAdavance = false;
  isJEAdvancePaperSchemeDuplicate = false;
  isSelectSubjectDisabled = false;
  isNew = true;
  addPaper_two_step_one = false
  addPaper_two_step_two = false;
  paperTwoCard = false;
  enableButtons = false;
  showPaper1 = false
  showPaper2 = false
  instructionForm: FormGroup;
  patternData: any;
  isOwned = true;
  isCustom = false;
  //vinayak added
  childIndex: number;
  parentIndex: number;
  subChildIndex: number;
  addedQuestionForm_one: number = 0;
  addedQuestionForm_two = 0;
  editorConfig = {
    "editable": true,
    "spellcheck": true,
    "height": "auto",
    "minHeight": "100%;",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "Enter text here...",
    "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline"],
      ["fontName", "fontSize", "color"],
      ["link"]
    ]
  };

  files: any = [];
  noRadioSelected = true;
  imageSrc: string;
  markingSchemeForm: FormGroup;
  rightMarks: any;
  isFromEvent: boolean;
  isImporedScheme: boolean;
  isShowSubjectList = false;
  activePaper = 1;
  patternPapers = 1;
  selectedAdvPattern: any;
  patternSelect: any = null;
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  constructor(private _examService: ExamServiceService,
    private spinner: NgxSpinnerService,
    private _toastService: ToastsupportService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private _courseService: CourseServiceService,
    private _subjectService: SubjectServiceService,
    private _assesmentService: AssesmentService,
    private examSetterService: AssesmentSetterServiceService,) { }

  ngOnInit() {
    this.initCategoryForm();
    this.initCreatePatternForm();
    this.getExamCategory();
    this.initInstructionForm();
    this.intitPatternForm();
  }
  initCategoryForm() {
    this.createCategoryForm = this.fb.group({
      name: ['', Validators.nullValidator],
      subjects: ['', Validators.nullValidator],
      masterCourse: ['', Validators.nullValidator],
    })
  }
  initCreatePatternForm() {
    this.createPatternForm = this.fb.group({
      name: ['', Validators.required],
      category: ['', Validators.required],
      subjects: ['', Validators.required],
      sctions: ['', Validators.required],
      totalQuestions: ['', Validators.required],
      questionCategory: ['', Validators.required],
      duration: ['', Validators.required]
    })
  }
  initCreatePatternTwoForm() {
    this.createPatternTwoForm = this.fb.group({
      name: ['', Validators.nullValidator],
      category: ['', Validators.nullValidator],
      subjects: ['', Validators.nullValidator],
      sctions: ['', Validators.nullValidator],
      totalQuestions: ['', Validators.nullValidator],
      questionCategory: ['', Validators.nullValidator],
      duration: ['', Validators.nullValidator]
    })
  }
  intitPatternForm() {
    this.patternForm = this.fb.group({
      name: ['', Validators.required],
      duplicatePattern: [false, Validators.nullValidator],
      patternCategory: this.fb.group({
        uniqueId: ['', Validators.required]
      }),
      patterns: this.fb.array([this.patternsArray()]),
    })
  }

  patternsArray() {
    return this.fb.group({
      duration: ['', Validators.required],
      questionCategory: ['', Validators.required],
      subjectSectionDtoList: this.fb.array([]),
    })
  }

  subjectSectionDtoListArray(subject) {
    return this.fb.group({
      subject: this.fb.group({
        name: [subject.name, Validators.nullValidator],
        uniqueId: [subject.uniqueId, Validators.required]
      }),
      sectionDtoList: this.fb.array([this.sectionDtoListArray()]),
    })
  }

  sectionDtoListArray() {
    return this.fb.group({
      name: ['', Validators.required],
      questionType: ['', Validators.required],
      totalQuestions: [0, Validators.required],
      optionalQuestions: ['', Validators.nullValidator],
      positiveMarks: ['', Validators.nullValidator],
      negativeMarks: ['', Validators.nullValidator],
      leaveMarks: ['', Validators.nullValidator],
      markingSchemeUid: ['', Validators.nullValidator],
      isMarkingScheme: [false, Validators.nullValidator],
      instructionsDto: this.fb.group({
        uniqueId: [null, Validators.nullValidator],
        text: [null, Validators.required],
        instructionType: [null, Validators.required],
        name: [null, Validators.nullValidator],
        instructiionAvailable: [false, Validators.nullValidator],
        files: [null, Validators.nullValidator]
      }),
    })
  }

  changePattern(event) {
    this.spinner.show();
    let uniqueId = "";
    if (this.isMasterListIsAdavance) {
      uniqueId = event.patterns[0].uniqueId;
      this.patternPapers = event.patterns.length;
      this.selectedAdvPattern = event;
      this.activePaper = 1;
    } else {
      uniqueId = event.uniqueId;
    }
    this._examService.getPattern(uniqueId).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            this.patternData = res;
            if (this.assignmentType != undefined && this.assignmentType == 2) {
              this.quickExamForm.controls.duration.setValue(res.duration);
              this.patchSubject(res);
            }
            this.isShowSubjectList = true;
          } else {
            this._toastService.ShowWarning('Pattern data not found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   this._toastService.ShowError(error);
        // }
      });
  }
  patchSubject(data) {
    this.quickExamForm.controls.patternUid.setValue(data.uniqueId);
    console.log(this.quickExamForm);
    let examSyllabus = <FormArray>this.quickExamForm.controls['subjectList'];
    console.log(examSyllabus);
    examSyllabus.controls = [];
    if (data != null && data.subjectSectionDtoList.length > 0) {
      let selectedSubjects = data.subjectSectionDtoList;
      for (let i = 0; i < selectedSubjects.length; i++) {
        examSyllabus.push(this.initSubjects(selectedSubjects[i]));
      }
    }
  }
  initSubjects(subject: any) {
    return this.fb.group({
      uniqueId: [subject.subject.uniqueId, Validators.required],
      subjectName: [subject.subject.name, Validators.required],
      actualCoursePlanners: this.fb.array([], Validators.nullValidator),
      remarks: ['', Validators.nullValidator],
      syllabus: [[], Validators.nullValidator],
      isSyllabusSelected: [true, Validators.nullValidator],
      examAssignmentPatternSubjectId: ['', Validators.nullValidator],
      actualSyllabus: [[], Validators.nullValidator],
    });
  }
  getExamCategory() {
    this.spinner.show()
    this.category = [];
    return this._examService.getPatternCategory().subscribe(
      res => {
        console.log("pattern category list res", res);
        if (res) {
          if (res.success == true) {
            if (res.list != undefined) {
              this.category = res.list
            } else {
              this._toastService.ShowError('Something error in getting category please try again')
            }
          } else {
            this._toastService.ShowWarning('No Catgories Found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }


  getMasterCourse() {
    console.log('get master course panel called')
    this.spinner.show()
    this.masterCourseList = [];
    return this._courseService.getMasterCourseList().subscribe(
      res => {
        console.log("master course list res", JSON.stringify(res));
        if (res) {
          this.masterCourseList = res;
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //    this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }

  getAllSubjects() {
    this.spinner.show()
    this.subjectList = [];
    return this._subjectService.getSubjectList().subscribe(
      res => {
        console.log("master subjectList list res", JSON.stringify(res));
        if (res) {
          if (res.success == true) {
            if (res.list != undefined) {
              this.subjectList = res.list
            } else {
              this._toastService.ShowError('Something error in getting subjects please try again')
            }
          } else {
            this._toastService.ShowWarning('No subjects Found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }

  getCourseSubjects(evt) {
    console.log('course changes', JSON.stringify(evt))
    this.spinner.show()
    this.subjectList = [];
    // return this.examService1.getPaginationExamList(this.userData.orgUniqueId, this.pageIndex).subscribe(
    return this._subjectService.getMasterCommonSUbjects(evt.uniqueId).subscribe(
      res => {
        console.log("master subject list res", JSON.stringify(res));
        if (res) {

          if (res.success == true) {
            if (res.list != undefined) {
              this.subjectList = res.list
            } else {
              this._toastService.ShowError('Something error in getting subjects please try again')
            }
          } else {
            this._toastService.ShowWarning('No subjects Found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }

  getpatternCategorySubjects(uniqueId) {
    console.log('pattern step 1 get subjects', JSON.stringify(uniqueId))
    this.spinner.show()
    this.subjectList = [];
    this.isSelectSubjectDisabled = false;
    // return this.examService1.getPaginationExamList(this.userData.orgUniqueId, this.pageIndex).subscribe(
    return this._subjectService.getpatternCategorySubjects(uniqueId).subscribe(
      res => {
        console.log("getpatternCategorySubjects subject list res", JSON.stringify(res));
        if (res) {

          if (res.success == true) {
            if (res.list != undefined) {
              this.subjectList = res.list
              let tempSelectedSubjects = [];
              if (this.isJEAdvancePaper == true) {
                this.subjectList.forEach(element => {
                  element.disabled = true;
                  tempSelectedSubjects.push(element.uniqueId)
                });
                this.initCreatePatternTwoForm();
                this.createPatternForm.controls['subjects'].setValue(tempSelectedSubjects)
                this.createPatternTwoForm.controls['subjects'].setValue(tempSelectedSubjects);
                this.isSelectSubjectDisabled = true;
                // this.createPatternForm.controls['subjects'].disable();
                // this.createPatternTwoForm.controls['subjects'].disable();
              }
              if (this.isOwned == false && this.isCustom == false && this.isJEAdvancePaper == false) {
                this.subjectList.forEach(element => {
                  element.disabled = true;
                  tempSelectedSubjects.push(element.uniqueId)
                });
                this.createPatternForm.controls['subjects'].setValue(tempSelectedSubjects);
                this.isSelectSubjectDisabled = true;
                // this.createPatternForm.controls['subjects'].disable();
              }
            } else {
              this._toastService.ShowError('Something error in getting subjects please try again')
            }
          } else {
            this._toastService.ShowWarning('No subjects Found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //    this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }

  changeCategory(evt) {
    console.log('evt changeCategory', evt)
    this.isShowSubjectList = false;
    this.activePaper = 0;
    this.patternSelect = null;
    if (this.assignmentType != undefined && this.assignmentType == 2) {
      this.quickExamForm.removeControl('patternUid');
      this.quickExamForm.removeControl('duration');
      this.quickExamForm.removeControl('subjectList');
      this.quickExamForm.addControl('subjectList', this.fb.array([], Validators.required));
      this.quickExamForm.addControl('patternUid', new FormControl('', Validators.required));
      this.quickExamForm.addControl('duration', new FormControl('', Validators.required));
    }
    if (evt.owned == false && evt.advanced != undefined && evt.advanced == true) {
      this.isMasterListIsAdavance = true;
      this.activePaper = 1;
      this.spinner.show()
      this.patterns = [];
      return this._examService.getAdvancePatternByCategory().subscribe(
        res => {
          console.log("pattern list res", JSON.stringify(res));
          if (res) {
            if (res.success == true) {
              if (res.list != undefined) {
                let patterns = [];
                res.list.forEach((element, key) => {
                  patterns.push({
                    name: element.patternGroup.name,
                    uniqueId: key,
                    patterns: element.patterns
                  })
                });
                this.patterns = patterns;
              } else {
                this._toastService.ShowError('Something error in getting patterns please try again')
              }
            } else {
              this._toastService.ShowWarning('No patterns found')
            }
            this.spinner.hide()
          }
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide()
          // if (error instanceof HttpErrorResponse) {
          //   this.spinner.hide()
          //   console.log("Client-side error occured.");
          // } else {
          //   this.spinner.hide()
          //   this._toastService.ShowError(error);
          // }
        });
    } else {
      this.isMasterListIsAdavance = false;
      this.spinner.show()
      this.patterns = [];
      return this._examService.getPatternByCategory(evt.uniqueId).subscribe(
        res => {
          console.log("pattern list res", JSON.stringify(res));
          if (res) {
            if (res.success == true) {
              if (res.list != undefined) {
                this.patterns = res.list
              } else {
                this._toastService.ShowError('Something error in getting patterns please try again')
              }
            } else {
              this._toastService.ShowWarning('No patterns found')
            }
            this.spinner.hide()
          }
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide()
          // if (error instanceof HttpErrorResponse) {
          //   this.spinner.hide()
          //   console.log("Client-side error occured.");
          // } else {
          //   this.spinner.hide()
          //   this._toastService.ShowError(error);
          // }
        });
    }

  }

  addCategory() {
    this.spinner.show();
    console.log('add category called')
    let subjects = this.createCategoryForm.get('subjects').value
    let tempSubjectList = [];
    subjects.forEach(element => {
      tempSubjectList.push({ uniqueId: element })
    });
    let Dto = {
      name: this.createCategoryForm.get('name').value,
      uniqueId: "",
      masterCourse: {
        uniqueId: this.createCategoryForm.get('masterCourse').value
      },
      subjects: tempSubjectList
    }
    return this._examService.addPatternCategory(Dto).subscribe(
      res => {
        if (res) {
          console.log("category Created SuccessFully", JSON.stringify(res));
          if (res.success == true) {
            this._toastService.showSuccess('Pattern Category Created SuccessFully')
            let dtos = this.category;
            // // this.category = [];
            // this.category.push({
            //   uniqueId: res.uniqueId,
            //   name: res.name
            // })
            // this.category = this.category;
            // console.log(this.category);
            this.getExamCategory();
          } else {
            this._toastService.ShowWarning('Error creating pattern category')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  changeQuestionCategory(id) {
    this.createPatternForm.controls['questionCategory'].setValue(id);
    switch (id) {
      case 1:
        this.getObjectiveQuestionTypeList();
        break;
      case 2:
        this.getsubjectiveQuestionTypeList();
        break;
      default:
        this._toastService.ShowError('Something wrong with question category please contact to support')
        break
    }
  }
  changeQuestionCategory2(id) {
    this.createPatternTwoForm.controls['questionCategory'].setValue(id);
  }
  removeSection(parentIndex: number, childIndex: number, currentIndex: number) {
    let examPatternArray = <FormArray>this.patternForm.controls['patterns'];
    let examPatternGroup = <FormGroup>examPatternArray.controls[parentIndex];
    let subjectArray = <FormArray>examPatternGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    sectionArray.removeAt(currentIndex);
  }
  changeCreatePatternCategory(evt) {
    console.log('Category Details' + JSON.stringify(evt))
    if (evt.owned == false) {
      this.isOwned = false
      if (evt.advanced != undefined && evt.advanced == true) {
        this.isJEAdvancePaper = true;
      } else {
        this.isJEAdvancePaper = false;
      }
      if (evt.custom != undefined && evt.custom == true) {
        this.isCustom = true;
      }
    } else {
      this.isJEAdvancePaper = false;
    }
  }
  getsubjectiveQuestionTypeList() {
    this.questionTypes = []
    return this._assesmentService.getsubjectiveQuestionTypeList().subscribe(
      res => {
        if (res) {
          console.log(" this.questionTypes =>", JSON.stringify(res));
          if (res.success == true) {
            this.questionTypes = res.list
          } else {
            this._toastService.ShowWarning('Question types not found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  getObjectiveQuestionTypeList() {
    this.questionTypes = []
    return this._assesmentService.getObjectiveQuestionTypeList().subscribe(
      res => {
        if (res) {
          console.log(" this.questionTypes =>", JSON.stringify(res));
          if (res.success == true) {
            this.questionTypes = res.list
          } else {
            this._toastService.ShowWarning('Question types not found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  getQuestionCategories() {
    this.questionCategories = []
    return this._examService.getQuestionCategories().subscribe(
      res => {
        if (res) {
          console.log(" this.questionCategories =>", JSON.stringify(res));
          if (res.success == true) {
            let tempList = [];
            res.list.forEach(element => {
              if (this.isJEAdvancePaper == true && element.id == 1) {
                tempList.push({ name: element.name, id: element.id, selected: 'checked' });
                this.createPatternForm.controls['questionCategory'].setValue(1);
                this.getObjectiveQuestionTypeList();
              } else {
                tempList.push({ name: element.name, id: element.id, selected: 'unchecked' });
              }

            });
            this.questionCategories = tempList;
            console.log(" this.questionCategories =>", JSON.stringify(this.questionCategories));
          } else {
            this._toastService.ShowWarning('Question Categories not found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  startPattern() {
    console.log('pattern form =>', JSON.stringify(this.createPatternForm.value))
    this.getpatternCategorySubjects(this.createPatternForm.get('category').value)
    this.getQuestionCategories();
    this.isPatternSetpOne = true
    this.isNew = false;
    this.isShowSubjectList = false;
    this.hideButtons.emit("1");

  }
  addPaperTwo() {
    this.addPaper_two_step_one = false;
    this.addPaper_two_step_two = true;
    this.createPatternTwoForm.controls['questionCategory'].setValue(1);
    this.getObjectiveQuestionTypeList();
    this.enableButtons = true;
  }
  CreateDuplicatePaper() {
    this.isJEAdvancePaperSchemeDuplicate = true
    this.patternForm.controls['duplicatePattern'].setValue(true);
    this.addPaper_two_step_one = true
    this.isPatternSetpTwo = false;
    this.isPatternSetpOne = false;
    this.isNew = false;
    this.paperTwoCard = true
    this.initCreatePatternTwoForm();
    //copy createpatternform in createpatternformtwo  
    this.createPatternTwoForm.controls['name'].setValue(this.createPatternForm.get('name').value);
    this.createPatternTwoForm.controls['sctions'].setValue(this.createPatternForm.get('sctions').value);
    this.createPatternTwoForm.controls['totalQuestions'].setValue(this.createPatternForm.get('totalQuestions').value);
    this.createPatternTwoForm.controls['duration'].setValue(this.createPatternForm.get('duration').value);
    this.createPatternTwoForm.controls['questionCategory'].setValue(1);
    this.getObjectiveQuestionTypeList();
    this.enableButtons = true;
  }
  cancelPaper2() {
    this.addPaper_two_step_one = true;
    this.addPaper_two_step_two = false;
  }
  addPaper() {
    this.addPaper_two_step_one = true;
    this.addPaper_two_step_two = false;
    this.paperTwoCard = true;
  }
  nextToStepTwo() {
    if (this.createPatternForm.invalid) {
      this._toastService.ShowWarning('Please select all data');
      return
    }
    console.log('started for step one')
    if (this.isJEAdvancePaper == true) {
      console.log('started for step one -two')
      this.addPaper_two_step_one = true
    } else {
      this.isPatternSetpTwo = true;
      this.processForSectionDetails();
    }
    this.isPatternSetpOne = false;
    this.isNew = false;
  }

  showPaper1Form() {
    this.showPaper1 = true
    this.showPaper2 = false
  }
  showPaper2Form() {
    this.showPaper1 = false
    this.showPaper2 = true
  }
  processForSectionDetails() {
    this.addPaper_two_step_one = false
    this.isPatternSetpTwo = true;
    this.showPaper1 = true

    // //set name to pattern form 
    this.patternForm.controls['name'].setValue(this.createPatternForm.get('name').value)
    this.patternForm['controls'].patternCategory['controls'].uniqueId.setValue(this.createPatternForm.get('category').value)

    let patternArray = <FormArray>this.patternForm.controls['patterns'];
    let patternGroup = <FormGroup>patternArray.controls[0];
    console.log('duration' + this.createPatternForm.get('duration').value)
    console.log('questionCategory' + this.createPatternForm.get('questionCategory').value)
    console.log('patterngroup' + JSON.stringify(patternGroup.value))
    patternGroup.controls['duration'].setValue(this.createPatternForm.get('duration').value)
    patternGroup.controls['questionCategory'].setValue(this.createPatternForm.get('questionCategory').value)
    console.log('patterngroup' + JSON.stringify(patternGroup.value))
    let subSecArray = <FormArray>patternGroup.controls['subjectSectionDtoList'];
    subSecArray.controls = []

    let selectedSubjects = this.createPatternForm.get('subjects').value;
    for (let i = 0; i < this.subjectList.length; i++) {
      if (selectedSubjects.includes(this.subjectList[i].uniqueId)) {
        subSecArray.push(this.subjectSectionDtoListArray({ name: this.subjectList[i].name, uniqueId: this.subjectList[i].uniqueId }))
        let totalSections = this.createPatternForm.get('sctions').value
        let sectionsGroup = <FormGroup>subSecArray.controls[i];
        let sections = <FormArray>sectionsGroup.controls['sectionDtoList'];
        sections.controls = []
        for (let l = 0; l < totalSections; l++) {
          sections.push(this.sectionDtoListArray())
        }
      }

    }

    //for secondPaperProcess
    if (this.isJEAdvancePaper == true && this.isJEAdvancePaperSchemeDuplicate == false) {
      patternArray.push(this.patternsArray())
      let patternGroup2 = <FormGroup>patternArray.controls[1];
      console.log('duration 2' + this.createPatternForm.get('duration').value)
      console.log('questionCategory 2' + this.createPatternForm.get('questionCategory').value)
      patternGroup2.controls['duration'].setValue(this.createPatternTwoForm.get('duration').value)
      patternGroup2.controls['questionCategory'].setValue(this.createPatternTwoForm.get('questionCategory').value)
      let subSecArray = <FormArray>patternGroup2.controls['subjectSectionDtoList'];
      subSecArray.controls = []

      let selectedSubjects = this.createPatternTwoForm.get('subjects').value;
      for (let i = 0; i < this.subjectList.length; i++) {
        if (selectedSubjects.includes(this.subjectList[i].uniqueId)) {
          subSecArray.push(this.subjectSectionDtoListArray({ name: this.subjectList[i].name, uniqueId: this.subjectList[i].uniqueId }))
          let totalSections = this.createPatternTwoForm.get('sctions').value
          let sectionsGroup = <FormGroup>subSecArray.controls[i];
          let sections = <FormArray>sectionsGroup.controls['sectionDtoList'];
          sections.controls = []
          for (let l = 0; l < totalSections; l++) {
            sections.push(this.sectionDtoListArray())
          }
        }

      }

    }

    console.log('patternForm final details' + JSON.stringify(this.patternForm.value))

  }

  onOptionalCheck2(id, id2, i, j, k) {
    console.log("id", id)

    let control = <FormArray>this.patternForm.controls['patterns'];
    let examformGroup = <FormGroup>control.controls[i];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjectSectionDtoList'];
    let subjectFormGroup = <FormGroup>Subjectcontrol.controls[j];
    let sectionControl = <FormArray>subjectFormGroup.controls['sectionDtoList'];
    let sectionFormGroup = <FormGroup>sectionControl.controls[k];
    sectionFormGroup.controls['optionalQuestions'].setValue(0)
    var ele = document.getElementById(id);
    console.log("ele", ele)


    if (ele.classList.contains('col-12')) {
      ele.classList.remove("col-12");
      ele.classList.add("col-6");
      ele.style.paddingRight = '0'

    } else if (ele.classList.contains('col-6')) {
      ele.classList.remove("col-6");
      ele.classList.add("col-12");
      ele.style.paddingRight = '12px'
    }

    var element = document.getElementById(id2);


    if (element.classList.contains('isHideOptional')) {
      element.classList.remove("isHideOptional");
      element.classList.add("isUnhideOptional");
      element.classList.add("col-6");

    } else if (element.classList.contains('col-6')) {
      element.classList.remove("col-6");
      element.classList.remove("isUnhideOptional");
      element.classList.add("isHideOptional");
    }
  }

  togleCategory(customType) {
    this.modalService.open(customType, { backdrop: 'static', windowClass: 'addTestType' });
    this.getMasterCourse();
    this.getAllSubjects();
  }

  toglePattern(customType) {
    this.initCreatePatternForm();
    this.modalService.open(customType, { backdrop: 'static', windowClass: 'addTestType' });
  }

  showMainContainer() {
    this.addPaper_two_step_one = false;
    this.isPatternSetpOne = false;
    this.isPatternSetpTwo = false;
    this.isNew = true;
    this.isMasterListIsAdavance = false;
    this.isShowSubjectList = false;
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  checkQuestionCountPaper1(value) {
    console.log('value' + value)
    if (value == 0) {
      this._toastService.ShowWarning("Question count must be greater than zero.");
      return false;
    }
    //when question subject  add into the api we should validate in track submit method for pattern
    let subjectArray = <FormArray>this.patternForm.controls['patterns'];
    let subjectGroup = <FormGroup>subjectArray.controls[0];
    let subjectsection = <FormArray>subjectGroup.controls['subjectSectionDtoList'];
    let totalQuestions = this.createPatternForm.get('totalQuestions').value;
    console.log('totalQuestions' + totalQuestions)
    this.addedQuestionForm_one = 0;
    console.log('total added question before', this.addedQuestionForm_one)
    for (let i = 0; i < subjectsection.length; i++) {
      let sectionGroup = <FormGroup>subjectsection.controls[i];
      let sectionArrays = <FormArray>sectionGroup.controls['sectionDtoList'];
      for (let j = 0; j < sectionArrays.length; j++) {
        let section = <FormGroup>sectionArrays.controls[j];
        let questionCount: number = section.get('totalQuestions').value;
        this.addedQuestionForm_one = +this.addedQuestionForm_one + (+questionCount);
      }
    }
    console.log('total added question after', this.addedQuestionForm_one)
    if (totalQuestions > this.addedQuestionForm_one) {
      return true;
    } else if (this.addedQuestionForm_one > totalQuestions) {
      this._toastService.ShowWarning("You can add only " + totalQuestions + " questions");
      return false;
    }

  }

  checkQuestionCountPaper2(value) {
    console.log('value' + value)
    if (value == 0) {
      this._toastService.ShowWarning("Question count must be greater than zero.");
      return false;
    }
    //when question subject  add into the api we should validate in track submit method for pattern
    let subjectArray = <FormArray>this.patternForm.controls['patterns'];
    let subjectGroup = <FormGroup>subjectArray.controls[1];
    let subjectsection = <FormArray>subjectGroup.controls['subjectSectionDtoList'];
    let totalQuestions = this.createPatternTwoForm.get('totalQuestions').value;
    console.log('totalQuestions' + totalQuestions)
    this.addedQuestionForm_one = 0;
    console.log('total added question before', this.addedQuestionForm_one)
    for (let i = 0; i < subjectsection.length; i++) {
      let sectionGroup = <FormGroup>subjectsection.controls[i];
      let sectionArrays = <FormArray>sectionGroup.controls['sectionDtoList'];
      for (let j = 0; j < sectionArrays.length; j++) {
        let section = <FormGroup>sectionArrays.controls[j];
        let questionCount: number = section.get('totalQuestions').value;
        this.addedQuestionForm_one = +this.addedQuestionForm_one + (+questionCount);
      }
    }
    console.log('total added question after', this.addedQuestionForm_one)
    if (totalQuestions > this.addedQuestionForm_one) {
      return true;
    } else if (this.addedQuestionForm_one > totalQuestions) {
      this._toastService.ShowWarning("You can add only " + totalQuestions + " questions");
      return false;
    }
  }

  addSection(parentIndex: number, childIndex: number) {
    let examPatternArray = <FormArray>this.patternForm.controls['patterns'];
    let examPatternGroup = <FormGroup>examPatternArray.controls[parentIndex];
    let subjectArray = <FormArray>examPatternGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    sectionArray.push(this.sectionDtoListArray());
  }

  //vinayak added
  getInstruction(parentIndex: number, childIndex: number, subChildIndex: number, content: string) {
    this.files = [];
    this.parentIndex = parentIndex;
    this.childIndex = childIndex;
    this.subChildIndex = subChildIndex;
    let patternsArray = <FormArray>this.patternForm.controls['patterns'];
    let patternsGroup = <FormGroup>patternsArray.controls[parentIndex];
    let subjectArray = <FormArray>patternsGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    let sectionGroup = <FormGroup>sectionArray.controls[subChildIndex];
    let instructionGroup = <FormGroup>sectionGroup.controls['instructionsDto'];

    this.modalService.open(content);

    if (instructionGroup.controls['text'].value != null) {
      console.log("instructionGroup inside", instructionGroup)
      this.instructionForm.controls['instructionUid'].setValue(instructionGroup.controls['uniqueId'].value);
      this.instructionForm.controls['instructionType'].setValue(instructionGroup.controls['instructionType'].value);
      this.instructionForm.controls['instructionTextOrImage'].setValue(instructionGroup.controls['text'].value);
      this.files.push(instructionGroup.controls['files'].value)
    } else {
      this.initInstructionForm();
    }

  }

  initInstructionForm() {
    this.instructionForm = this.fb.group({
      instructionUid: ['', Validators.nullValidator],
      instructionType: ['', Validators.required],
      instructionTextOrImage: ['', Validators.required],
    });
  }

  _handleReaderLoaded(e: any) {
    this.imageSrc = e.target.result;


    this.instructionForm.controls['instructionTextOrImage'].setValue(this.imageSrc);

  }


  uploadFile(event) {

    var file = event.dataTransfer ? event.dataTransfer.files[0] : event.target.files[0];
    let max_size: number = 2085184;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this._toastService.ShowWarning("invalid format");
      return;
    }
    if (event.target.files[0].size > max_size) {
      this._toastService.ShowWarning("Maximum size of 2 MB is allowed..");
      this.instructionForm.controls['instructionTextOrImage'].setValue('');
      return false;
    } else {
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
    var files = event.target.files
    this.files = [];

    let patternsArray = <FormArray>this.patternForm.controls['patterns'];
    let patternsGroup = <FormGroup>patternsArray.controls[this.parentIndex];
    let subjectArray = <FormArray>patternsGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[this.childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    let sectionGroup = <FormGroup>sectionArray.controls[this.subChildIndex];
    let instructionGroup = <FormGroup>sectionGroup.controls['instructionsDto'];



    for (let index = 0; index < files.length; index++) {

      const element = files[index];
      this.files.push(element.name);
      instructionGroup.controls['files'].setValue(element.name);
      console.log("test", instructionGroup.controls['files'].value)
    }

  }


  deleteAttachment(index) {

    this.files.splice(index, 1)
    if (index == 0 && this.files.length == 0) {
      this.instructionForm.controls['instructionTextOrImage'].setValue('');
      let patternsArray = <FormArray>this.patternForm.controls['patterns'];
      let patternsGroup = <FormGroup>patternsArray.controls[this.parentIndex];
      let subjectArray = <FormArray>patternsGroup.controls['subjectSectionDtoList'];
      let subjectGroup = <FormGroup>subjectArray.controls[this.childIndex];
      let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
      let sectionGroup = <FormGroup>sectionArray.controls[this.subChildIndex];
      let instructionGroup = <FormGroup>sectionGroup.controls['instructionsDto'];
      instructionGroup.controls['files'].setValue(null);
    }
  }

  onSelectFile(e: any) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    let max_size: number = 2085184;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this._toastService.ShowWarning("invalid format");
      return;
    }
    console.log("imageSrc", this.imageSrc);
    if (e.target.files[0].size > max_size) {
      this._toastService.ShowWarning("Maximum size of 2 MB is allowed..");
      this.instructionForm.controls['instructionTextOrImage'].setValue('');
      return false;
    } else {
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }

  resetInstructionValue() {
    this.instructionForm.controls['instructionTextOrImage'].setValue(null)
  }

  saveInstruction() {

    let patternsArray = <FormArray>this.patternForm.controls['patterns'];
    let patternsGroup = <FormGroup>patternsArray.controls[this.parentIndex];
    let subjectArray = <FormArray>patternsGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[this.childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    let sectionGroup = <FormGroup>sectionArray.controls[this.subChildIndex];
    let instructionGroup = <FormGroup>sectionGroup.controls['instructionsDto'];






    instructionGroup.controls['instructionType'].setValue(this.instructionForm.controls['instructionType'].value);
    instructionGroup.controls['uniqueId'].setValue(this.instructionForm.controls['instructionUid'].value)
    instructionGroup.controls['text'].setValue(this.instructionForm.controls['instructionTextOrImage'].value);

    instructionGroup.controls['instructiionAvailable'].setValue(true);
    console.log("this.simpleExamTrackForm", this.patternForm.value)
    this.instructionForm.reset();
    this.files = [];
  }

  getMarkingScheme(scrollDataModal, parentIndex: number, childIndex: number, subChildIndex: number) {
    this.parentIndex = parentIndex;
    this.childIndex = childIndex;
    this.subChildIndex = subChildIndex;
    let patternsArray = <FormArray>this.patternForm.controls['patterns'];
    let patternsGroup = <FormGroup>patternsArray.controls[parentIndex];
    let subjectArray = <FormArray>patternsGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    let sectionGroup = <FormGroup>sectionArray.controls[subChildIndex];
    this.modalService.open(scrollDataModal);


    if (sectionGroup.controls['isMarkingScheme'].value == true) {

      this.isImporedScheme = true;
    } else {
      this.initMarkingSchemeForm();
    }
  }
  initMarkingSchemeForm() {
    this.markingSchemeForm = this.fb.group({
      schemeType: ['', Validators.required],
      optionCount: [1, Validators.required],
      name: ['', Validators.required],
      uniqueId: [null, Validators.nullValidator],
      highestMarks: ['', Validators.required],
      leaveMarks: [0, Validators.required],
      defaultMarks: [0, Validators.required],
      isTemplate: [false, Validators.required],
      isImported: [false, Validators.required],

      markingSchemeCriteriaList: this.fb.array([this.initmarkingSchemeCriteriaList()])
    });
  }

  initmarkingSchemeCriteriaList() {
    return this.fb.group({
      correctOption: [0, Validators.required],
      rightOption: [0, Validators.required],
      wrongOption: [0, Validators.required],
      marks: [0, Validators.required],
    });
  }

  saveimportedScheme() {

    console.log("this.markingSchemeForm add track", this.markingSchemeForm.value)


    let patternsArray = <FormArray>this.patternForm.controls['patterns'];
    let patternsGroup = <FormGroup>patternsArray.controls[this.parentIndex];
    let subjectArray = <FormArray>patternsGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[this.childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    let sectionGroup = <FormGroup>sectionArray.controls[this.subChildIndex];


    sectionGroup.controls['markingSchemeUid'].setValue(this.markingSchemeForm.controls['uniqueId'].value);
    sectionGroup.controls['isMarkingScheme'].setValue(true);
    this.markingSchemeForm.controls['isImported'].setValue(true);
    console.log("simpleExamTrackForm", this.patternForm.value)
    this.rightMarks = this.markingSchemeForm.controls['highestMarks'].value;
    this.isFromEvent = true;
    this.markingSchemeForm.reset()
    // this.calculateMarks(this.parentIndex, this.childIndex)
  }

  saveAndImportScheme(type: any) {
    this.markingSchemeForm.controls['isTemplate'].setValue(type);
    return this.examSetterService.saveMarkingScheme(this.markingSchemeForm.value).subscribe(
      res => {
        if (res) {
          this.markingSchemeForm.controls['schemeType'].setValue(1),
            this.markingSchemeForm.controls['uniqueId'].setValue(res.uniqueId);
          let patternsArray = <FormArray>this.patternForm.controls['patterns'];
          let patternsGroup = <FormGroup>patternsArray.controls[this.parentIndex];
          let subjectArray = <FormArray>patternsGroup.controls['subjectSectionDtoList'];
          let subjectGroup = <FormGroup>subjectArray.controls[this.childIndex];
          let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
          let sectionGroup = <FormGroup>sectionArray.controls[this.subChildIndex];
          sectionGroup.controls['isMarkingScheme'].setValue(true);
          this.markingSchemeForm.controls['isImported'].setValue(true);
          this.saveimportedScheme();

          this.modalService.dismissAll();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this._toastService.ShowWarning(error.error.errorMessage);
        // }
      });
  }


  checkExists(value: string, parentIndex: number, childIndex: number, currentIndex: number) {

    let examPatternArray = <FormArray>this.patternForm.controls['patterns'];
    let examPatternGroup = <FormGroup>examPatternArray.controls[parentIndex];
    let subjectArray = <FormArray>examPatternGroup.controls['subjectSectionDtoList'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sectionDtoList'];
    for (let k = 0; k < sectionArray.length; k++) {
      let sectionGroup = <FormGroup>sectionArray.controls[k];
      let name: string = sectionGroup.controls['name'].value;
      if (k != currentIndex && name != "" && name.toUpperCase() == value.toUpperCase()) {
        this._toastService.ShowWarning("Section Name already added");
        let currentSecGroup = <FormGroup>sectionArray.controls[currentIndex];
        currentSecGroup.controls['name'].setValue('');
        return false;
      }
    }
  }


  submitPatternForm() {
    if (this.patternForm.invalid) {
      this._toastService.ShowWarning('please fill all details')
      return
    }
    let totalQuestions = this.createPatternForm.get('totalQuestions').value;
    console.log(this.addedQuestionForm_one, totalQuestions);
    if (this.addedQuestionForm_one < totalQuestions) {
      this._toastService.ShowWarning('added questions are more than total questions')
      return
    }
    if (this.isJEAdvancePaper == true) {
      let totalQuestions2 = this.createPatternTwoForm.get('totalQuestions').value;
      if (this.addedQuestionForm_one < totalQuestions2) {
        this._toastService.ShowWarning('added questions are more than total questions in paper 2')
        return
      }
    }
    this.spinner.show();
    console.log('submit pattern data', JSON.stringify(this.patternForm.value));
    return this._examService.addPattern(this.patternForm.value).subscribe(
      res => {
        if (res) {
          this._toastService.showSuccess('Pattern created succesfully');
          this.showMainContainer();
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this._toastService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  getPaperData(type) {
    let index = (type == 1) ? 0 : 1;
    if (type != this.activePaper) {
      this.activePaper = type;
      this.spinner.show();
      let uniqueId = "";
      uniqueId = this.selectedAdvPattern.patterns[index].uniqueId;
      this.patternPapers = this.selectedAdvPattern.patterns.length;

      this._examService.getPattern(uniqueId).subscribe(
        res => {
          if (res) {
            if (res.success == true) {
              this.patternData = res;
              if (this.assignmentType != undefined && this.assignmentType == 2) {
                this.quickExamForm.controls.duration.setValue(res.duration);
                this.patchSubject(res);
              }
              this.isShowSubjectList = true;
            } else {
              this._toastService.ShowWarning('Pattern data not found')
            }
            this.spinner.hide()
          }
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide()
          // if (error instanceof HttpErrorResponse) {
          //   this.spinner.hide()
          //   console.log("Client-side error occured.");
          // } else {
          //   this.spinner.hide()
          //   this._toastService.ShowError(error);
          // }
        });
    }
  }
}