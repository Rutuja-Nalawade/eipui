import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ExamService } from '../exam.service';
@Component({
  selector: 'app-track-info',
  templateUrl: './track-info.component.html',
  styleUrls: ['./track-info.component.scss']
})
export class TrackInfoComponent implements OnInit {
  examUid: string;
  examTracks: any;
  constructor(private modalService: NgbModal, 
              private route: ActivatedRoute, 
              private examService: ExamService, 
              private toastService: ToastsupportService, 
              private router: Router) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.examUid = params['examId'];
      if (this.examUid != null) {
          this.getExamTracks(this.examUid);
      }
  });
  }

  getExamTracks(examUid: string) {
   
    this.examService.getExamTrack(examUid).subscribe(
        res => {
            if (res) {
                this.examTracks = res;
                // this.spinner = false;
              console.log('this.examTracks',this.examTracks)
                
            }
        },
        (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            //     console.log("Client-side error occured.");
            // } else {
            //     // this.spinner = false;
            //     this.toastService.ShowError(error)
            // }
        }
    );
}0

goToSyllabus(teacherTrackUid: string, subjectName: string, syllabusState: number, testType: number, examAssignmentSubjectId: number) {
  // this.examSetterService.changeMessage("sylabus ready")
  
  this.router.navigate(['/newModule/exam/track/syllabus', teacherTrackUid, subjectName, "NO", syllabusState, testType, examAssignmentSubjectId]);
  // this.router.navigate(['/exam/track/syllabus', teacherTrackUid, subjectName, "NO", syllabusState, testType, examAssignmentSubjectId]);
}

editTrackSyllbus(teacherTrackUid: string, subjectName: string, syllabusState: number, testType: number, examAssignmentSubjectId: number) {
  this.router.navigate(['/newModule/exam/track/syllabus', teacherTrackUid, subjectName, "YES", syllabusState, testType, examAssignmentSubjectId]);
}
goToQuestions(teacherTrackUid: string) {
  // this.examSetterService.changeMessage("sylabus ready")
  // this.router.navigate(['/exam/searchExam/9/track/pick/questions', teacherTrackUid]);
this.router.navigate(['/newModule/exam/track/questionBank', teacherTrackUid]);
}

openModal(standardModal: string) {
  this.modalService.open(standardModal);
}

lockTrack() {
  // this.spinner = true;
  this.examService.lockExamTrack(this.examUid).subscribe(
      res => {
          if (res) {
              // this.spinner = false;
              this.toastService.showSuccess("Exam Track Locked Successfully");
              this.router.navigate(['/exam/examList']);
          }
      },
      (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //     console.log("Client-side error occured.");
          // } else {
          //     // this.spinner = false;
          // }
      });
}
}
