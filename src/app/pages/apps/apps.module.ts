import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// import { HttpClientModule } from '@angular/common/http';
import {
  NgbDropdownModule, NgbModalModule, NgbTypeaheadModule,
  NgbPaginationModule, NgbProgressbarModule, NgbTooltipModule
} from '@ng-bootstrap/ng-bootstrap';
import { DndModule } from 'ngx-drag-drop';
import { NgApexchartsModule } from 'ng-apexcharts';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { AppRoutingModule } from './apps-routing.module';

import { UIModule } from '../../shared/ui/ui.module';

import { NgSelectModule } from '@ng-select/ng-select';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TagInputModule } from 'ngx-chips';
import { NgxEditorModule } from 'ngx-editor';
import { ColorPickerModule } from '../../../../node_modules/ngx-color-picker';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BulkasignComponent } from './bulkasign/bulkasign.component';
import { StudentrankComponent } from './studentrank/studentrank.component';
///import { CreatecourseplannerComponent } from './courseplanner/createcourseplanner/createcourseplanner.component';
import { ChartsModule } from 'ng2-charts';// used from https://valor-software.com/ng2-charts/#/GeneralInfo
import { NgxLoadingModule } from 'ngx-loading';
import { LeadboardComponent } from './leadboard/leadboard.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ModifypaperComponent } from './modifypaper/modifypaper.component';

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [BulkasignComponent, StudentrankComponent, LeadboardComponent, ModifypaperComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    NgbDropdownModule,
    UIModule,
    FormsModule,
    DndModule,
    NgbModalModule,
    NgbPaginationModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    Ng2SearchPipeModule,
    // HttpClientModule,
    NgbTypeaheadModule,
    NgbProgressbarModule,
    NgbTooltipModule,
    NgSelectModule,
    NgOptionHighlightModule,
    DragDropModule,
    NgbModule,
    AngularMultiSelectModule,
    TagInputModule,
    NgxEditorModule,
    ColorPickerModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgxQRCodeModule,
    NgxSpinnerModule,
    ChartsModule,
    NgxLoadingModule.forRoot({}),
    InfiniteScrollModule

  ],
  providers: [DatePipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppsModule { }
