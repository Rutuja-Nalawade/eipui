import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  api;
  filterData = new Subject();
  constructor( private http: HttpClient) {
    // this.api = this._commonService.api.base_url
  }
  getstaffShiftInfo(orgaid: string) {
    return this.http.get<any>(environment.apiUrl + '/get/shift/' + orgaid);
  }
  getShift(id) {
    return this.http.get<any>(environment.apiUrl + '/get/user/shifts/' + id);
  }

  addShift(body) {
    return this.http.post<any>(environment.apiUrl + '/add/user/shifts', body);
  }

  searchUsersBySearchKey(filterDto: any) {
    return this.http.post<any>(environment.apiUrl + '/entity/search', filterDto);
  }
  getOrgTeacherListWithPagination(filterDto: any) {
    return this.http.post<any>(environment.apiUrl + '/teacher/list/filter', filterDto);
  }
  public createTeacherBasicInfo(teacherBasicData: any) {
    return this.http.post<any>(environment.authUrl + '/add/teacher/basicInfo', teacherBasicData);
  }

  //Basic info
  getRoleData(orgId: string) {
    return this.http.get<any>(environment.apiUrl + '/staff/role/list/' + orgId);
  }
  getOrganizationList(orgId: string) {
    return this.http.post<any>(environment.apiUrl + '/organization/list', orgId);
  }
  getTeacherBasicInfo(userId: any) {
    return this.http.get<any>(environment.authUrl + '/get/teacher/basicInfo/' + userId);
  }
  updateTeacherBasicInfo(teacherBasicData: any) {
    return this.http.post<any>(environment.authUrl + '/update/teacher/basicInfo', teacherBasicData);
  }

  // Otehers
  getStateLiist() {
    return this.http.get<any>(environment.apiUrl + '/state/list');
  }
  getTeacherAddressInfo(userId) {
    return this.http.get<any>(environment.authUrl + '/get/teacher/addressInfo/' + userId);
  }
  updateTeacherAddressInfo(teacherBasicData: any) {
    return this.http.post<any>(environment.authUrl + '/update/teacher/addressInfo', teacherBasicData);
  }

  //subjects
  getAllSubjectList(orgId: string) {
    return this.http.get<any>(environment.apiUrl + '/subject/list/get/' + orgId);
  }
  getTeacherSubjectInfo(userId) {
    return this.http.get<any>(environment.apiUrl + '/get/teacher/subjectsInfo/' + userId);
  }
  createTeacherSubjectInfo(teacherSubjectInfo: any) {
    return this.http.post<any>(environment.apiUrl + '/add/teacher/subjectsInfo', teacherSubjectInfo);
  }

  //assign period
  getPeriodLists(orgUniqueId: string) {
    return this.http.get<any>(environment.apiUrl + '/get/teacher/periods/' + orgUniqueId);
  }
  addUserAssignedPeriodInfo(periodDto) {
    return this.http.post<any>(environment.apiUrl + '/assign/periods', periodDto);
  }

  // New API
  getRolesData() {
    return this.http.get<any>(environment.authUrl + '/send/teacher');
  }
  public addTeacherBasicInfo(teacherBasicData: any) {
    return this.http.post<any>(environment.url + 'authentication-services/teacher/add/basicInfo', teacherBasicData);
  }
  fileUpload(formdata, id) {
    return this.http.post<any>(environment.url + 'authentication-services/teacher/upload/' + id, formdata);
  }
}
