import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClassroomServiceService {

  constructor(private http: HttpClient) { }


  public getAcademicClassRoom() {
    return this.http.get<any>(environment.apiUrl + '/academic-classroom/get');
  }
  getClassroomTypes() {
    return this.http.get<any>(environment.apiUrl + '/academic-classroom/get-types');
  }
  getClassroomCategories() {
    return this.http.get<any>(environment.apiUrl + '/academic-classroom/get-categories');
  }

  saveAcademicClassRoom(acaedmicClassRoom: any) {
    return this.http.post<any>(environment.apiUrl + '/academic-classroom/add', acaedmicClassRoom);
  }

  updateAcademicClassRoom(acaedmicClassRoom: any) {
    return this.http.put<any>(environment.apiUrl + '/academic-classroom/update', acaedmicClassRoom);
  }
  deleteAcademicClassRoom(uniqueId) {
    console.log('delet classroom 2=> ', uniqueId)
    return this.http.delete<any>(environment.apiUrl + '/academic-classroom/delete/' + uniqueId);
  }


}
