import { Component, OnInit, ElementRef } from '@angular/core';

import { Widget, UserBalance, RevenueData, ChartType } from './default.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-default-dashboard',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss'],
})

/**
 * Dashboard-1 component: handling the dashboard-1 with sidebar and content
 */
export class DefaultDashboardComponent implements OnInit {
  loading = false;
  academicCalender: any;
  courses: any;
  organizationId = sessionStorage.getItem('orgUniqueId');
  recentExams: any;
  labels = [];
  highestMarks = [];
  lowestMarks = [];
  averageMarks = [];
  examBatches = [];
  examAnalyticcsMessage: any;

  options: ChartType = {
    chart: {
        height: 370,
        type: 'line',
        padding: {
            right: 0,
            left: 0
        },
        stacked: false,
        toolbar: {
            show: false
        },
        events: {
          click: (event, chartContext, config) => {
            // console.log('clicked'  + JSON.stringify(event) + config.dataPointIndex);
            this.chartClick(config.dataPointIndex);
          }
        }
    },
    stroke: {
        width: [0, 2, 4],
        curve: 'straight'
    },
    plotOptions: {
        bar: {
            columnWidth: '50%'
        }
    },
    colors: ['#1abc9c', '#e3eaef', '#4a81d4'],
    series: [{
        name: 'Highest Score',
        type: 'column',
        data: []
    }, {
        name: 'Average Score',
        type: 'area',
        data: []
    }, {
        name: 'Lowest Score',
        type: 'line',
        data: []
    }],
    fill: {
        opacity: [0.85, 0.25, 1],
        gradient: {
            inverseColors: false,
            shade: 'light',
            type: 'vertical',
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
        }
    },
    // tslint:disable-next-line: max-line-length
    labels: [],
    markers: {
        size: 0
    },
    legend: {
        show: false
    },
    xaxis: {
        type: 'text',
        axisBorder: {
            show: false
        },
        axisTicks: {
            show: false
        }
    },
    yaxis: {
        title: {
            text: '',
        },
    },
    tooltip: {
        shared: true,
        intersect: false,
        y: {
            formatter(y) {
                if (typeof y !== 'undefined') {
                    return y.toFixed(0) + ' points';
                }
                return y;

            }
        }
    },
    grid: {
        borderColor: '#f1f3fa'
    }
};

  // bread crumb items
  breadCrumbItems: Array<{}>;

  currentDate = new Date();
  constructor(private eref: ElementRef,  private toastService: ToastsupportService,private router: Router) {}
  ngOnInit() {
    this.breadCrumbItems = [{ label: 'UBold', path: '/' }, { label: 'Dashboard', path: '/', active: true }];

    /**
     * fetches data
     */
    this._fetchRecentExams(this.organizationId)
    // this._fetchAcademicCalenders(this.organizationId);
  }

  // _fetchAcademicCalenders(orgId){
  //   this.loading = true;
  //   this.academiccalender.getAcademicCalendarList(orgId).subscribe(
  //     response => {
  //       this.academicCalender =  response.academicCalendarsDtoList;
  //       this.loading = false;
  //     },
  //     (error: HttpErrorResponse) => {
  //       if (error instanceof HttpErrorResponse) {
  //         this.loading = false;
  //       } else {
  //         this.loading = false;
  //         this.toastService.ShowError(error);
  //       }
  //     });
  // }
  // onAcademicCalenderChange(calenderId){
  //   console.log('calenderId' + calenderId);
  //   this.loading = true;
  //   this.courseService.getCourseListByAcademicCalendar(calenderId).subscribe(
  //     (res) => {
  //       if (res) {
  //         this.courses = res;
  //         // console.log("Course List", this.courseList);
  //         this.loading = false;
  //       }
  //     },
  //     (error: HttpErrorResponse) => {
  //       if (error instanceof HttpErrorResponse) {
  //         console.log('Client-side error occured.');
  //       } else {
  //         this.loading = false;
  //         this.toastService.ShowError(error);
  //       }
  //     });
  // }
  chartClick(index){
    // console.log('clicked index' + index + this.recentExams[index].examUid + this.recentExams[index].examType);
    this.router.navigate(['/hr/2/studentrank', this.recentExams[index].examUid, this.recentExams[index].examType]);
  }
  onCourseChange(courseId){

      this.labels = [];
      this.highestMarks = [];
      this.lowestMarks = [];
      this.averageMarks = [];
      this.examBatches = [];

      this.examAnalyticcsMessage = '';
      this.loading = true;
      // console.log('courseId ' + courseId)
      // this.studentService.getCourseExams(courseId).subscribe(
      // (res) => {
      //   // console.log('getCourseExams' + res);
      //   if (res) {
      //     this.recentExams = res;
      //     this.recentExams.forEach(element => {
      //       this.highestMarks.push(element.highestMarks);
      //       this.lowestMarks.push(element.lowestmarks);
      //       this.averageMarks.push(element.averageMarks);
      //       const temparray = [];
      //       element.batches.forEach(batche => {
      //         temparray.push(batche.batchname);
      //       });
      //       this.labels.push(element.examName + '(' + temparray.join() + ')');
      //       this.examBatches.push(temparray);
      //     });
      //     this.options.series = [{
      //       name: 'Lowest Score',
      //       type: 'column',
      //       data: this.lowestMarks
      //   }, {
      //       name: 'Highest Score',
      //       type: 'area',
      //       data: this.highestMarks
      //   }, {
      //       name: 'Average Score',
      //       type: 'line',
      //       data: this.averageMarks
      //   }]
      //     this.options.labels = this.labels;
      //     this.examAnalyticcsMessage = 'Exam analytics for specific course';
      //     this.loading = false;
      //     // console.log('lables array' + JSON.stringify(this.examBatches));
      //   }
      // },
      // (error: HttpErrorResponse) => {
      //   if (error instanceof HttpErrorResponse) {
      //     console.log('Client-side error occured.');
      //     this.loading = false;
      //   } else {
      //     this.toastService.ShowError(error);
      //     this.loading = false;
      //   }
      // });
  }
  _fetchRecentExams(orgId){
    this.loading = true;
    // console.log('orgId ' + orgId)
    // this.studentService.getRecenteExams(orgId).subscribe(
    //   (res) => {
    //     // console.log('getSingleStudentReport' + JSON.stringify(res));
    //     if (res) {
    //       this.recentExams = res;
    //       this.recentExams.forEach(element => {
    //         this.highestMarks.push(element.highestMarks);
    //         this.lowestMarks.push(element.lowestmarks);
    //         this.averageMarks.push(element.averageMarks);
    //         const temparray = [];
    //         element.batches.forEach(batche => {
    //           temparray.push(batche.batchname);
    //         });
    //         this.labels.push(element.examName + '(' + temparray.join() + ')');
    //         this.examBatches.push(temparray);
    //       });
    //       this.options.series = [{
    //         name: 'Lowest Score',
    //         type: 'column',
    //         data: this.lowestMarks
    //     }, {
    //         name: 'Highest Score',
    //         type: 'area',
    //         data: this.highestMarks
    //     }, {
    //         name: 'Average Score',
    //         type: 'line',
    //         data: this.averageMarks
    //     }]
    //       this.options.labels = this.labels;
    //       this.examAnalyticcsMessage = 'Exam Analytics';
    //       this.loading = false;
    //       // console.log('lables array' + JSON.stringify(this.examBatches));
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log('Client-side error occured.');
    //       this.loading = false;
    //     } else {
    //       this.toastService.ShowError(error);
    //       this.loading = false;
    //     }
    //   });
  }

}
