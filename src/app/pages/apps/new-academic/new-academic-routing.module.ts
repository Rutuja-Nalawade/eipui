import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcademicSessionComponent } from './academic-session/academic-session.component';
import { CommonheaderComponent } from './commonheader/commonheader.component';
import { AddGradeBoardComponent } from './add-grade-board/add-grade-board.component';


const routes: Routes = [
  {path:'academicDetails',component:CommonheaderComponent},
  {path:'academicDetails/addeditGrade',component:AddGradeBoardComponent},
  {path:'academic-session',component:AcademicSessionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewAcademicRoutingModule { }
