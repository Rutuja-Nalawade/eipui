import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StaffService {
  filterData = new Subject();
  constructor(private http: HttpClient) {

  }
  getOrganizationList(orgId: string) {
    return this.http.post<any>(environment.apiUrl + "/organization/list", orgId);
  }
  getRoleData(orgId: string) {
    return this.http.get<any>(environment.authUrl + '/get/roles/for/staff/' + orgId);
  }
  public createStaffBasicInfo(staffBasicData: any) {
    return this.http.post<any>(environment.authUrl + '/add/staff/basicInfo', staffBasicData);
  }
  getShift(id) {
    return this.http.get<any>(environment.apiUrl + '/get/user/shifts/' + id);
  }

  addShift(body) {
    console.log("body", body);
    return this.http.post<any>(environment.apiUrl + '/add/user/shifts', body);
  }

  getAllStaff(filterDto) {
    return this.http.post<any>(environment.apiUrl + '/allstaff/list/filter', filterDto);
  }
  searchUsersBySearchKey(filterDto) {
    return this.http.post<any>(environment.apiUrl + '/entity/search', filterDto);
  }
  //Basic info
  // getRoleData(orgId: string){
  //   return this.http.get<any>(environment.apiUrl + '/staff/role/list/' + orgId);
  // }
  getStaffBasicInfo(userId: any) {
    return this.http.get<any>(environment.authUrl + '/get/staff/basicInfo/' + userId);
  }
  updateStaffBasicInfo(staffBasicData: any) {
    return this.http.post<any>(environment.authUrl + '/update/staff/basicInfo', staffBasicData);
  }
  getstaffShiftInfo(orgaid: string) {
    return this.http.get<any>(environment.apiUrl + '/get/shift/' + orgaid);
  }

  //Others
  getStateLiist() {
    return this.http.get<any>(environment.apiUrl + '/state/list');
  }
  getStaffAdressInfo(userId: any) {
    return this.http.get<any>(environment.authUrl + '/get/staff/addressInfo/' + userId);
  }
  updateStaffAddress(staffAdressData: any) {
    return this.http.post<any>(environment.authUrl + '/update/staff/addressInfo', staffAdressData)
  }

  // New API
  getRolesData() {
    return this.http.get<any>(environment.authUrl + '/send/staff');
  }
  public addStaffBasicInfo(staffBasicData: any) {
    return this.http.post<any>(environment.url + 'authentication-services/staff/add/basicInfo', staffBasicData);
  }
  fileUpload(formdata, id) {
    return this.http.post<any>(environment.url + 'authentication-services/staff/upload/' + id, formdata);
  }
}
