import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-parents-list',
  templateUrl: './parents-list.component.html',
  styleUrls: ['./parents-list.component.scss']
})
export class ParentsListComponent implements OnInit {

  listClicked = false;
  parentList = [];
  userData: any;
  pageIndex: number = 0;
  filterData: any = null;
  textSearch: boolean = false;
  selectedParentId: any;
  selectedBoardList = [];
  selectedGradeList = [];
  selectedCourseList = [];
  selectedBatchList = [];
  selectedSubjectList = [];
  selectedYearList = [];
  selectedCalenderList = [];
  gradeList = [];
  boardList = [];
  courseList = [];
  orgCourseList = [];
  orgBoardList: any;
  orgGradeList: any;
  academicYearList = [];
  academicCalendarList = [];
  parentsBasicInfoForm: FormGroup;
  submitted: boolean = false;
  isShowQuickForm: boolean = false;
  genders: any = [];
  orgList = []
  selectedOrgList = []
  currentUserId = null;
  isDropDownOpen = false;
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  public totalCount = 100;
  isShowPagination = false;
  pageSizeArray = [{ id: 0, name: "1-10", value: 10 }, { id: 1, name: "1-20", value: 20 }, { id: 2, name: "1-50", value: 50 }, { id: 3, name: "1-100", value: 100 }];
  pageSize = 10;
  selectedPageSize = 0;
  totalPages = 0;
  isNextPageValid = true;
  isSelectAll = false;
  selectedParent: any = {};
  roleData: any = [];
  relationArray = [{ id: 1, itemName: 'Mother', }, { id: 2, itemName: 'Father' }, { id: 3, itemName: 'Guardian' }]
  message = "";
  maxLength = 50;
  stringType = 3;
  searchUserList = [];
  searchableUserList = [];
  parentUniqueIds = [];
  searchText = ""
  @Input() tabNumber: Number;
  @Output() detailsOpen = new EventEmitter<string>();
  @ViewChild('myDrop', { static: true }) myDrop: NgbDropdown;

  constructor(private _commonService: NewCommonService, private spinner: NgxSpinnerService, private _DomSanitizationService: DomSanitizer, private toastService: ToastsupportService, private datePipe: DatePipe, private _userService: UserServiceService) {
    this.userData = this._commonService.getUserData();
    this.selectedOrgList = [parseInt(this.userData.organizationId)]
    this.genders = [{ id: "M", name: "Male" }, { id: "F", name: "Female" }, { id: "O", name: "Other" }]
    // this._commonService.iscloseClicked.subscribe(click => {
    //   if (click) {
    //       this.listClicked = false;
    //   }
    // })
    this._commonService.iscancelClicked.subscribe(click => {
      if (this.tabNumber == 3 && click == 4) {
        this.listClicked = false;
        this.findParentRecords([]);
        this.detailsOpen.emit("close")
        document.querySelector('body').classList.remove('hideScroll');
      }
    })

  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['tabNumber'] && changes['tabNumber'].currentValue == 3) {
      this.listClicked = false;
      this.findParentRecords([]);
      // this.getChildOrganizationList(this.userData.orgUniqueId);
      this.getRoleInfo();
    }
  }
  findParentRecords(value: any) {
    console.log(value);
    if (value != null && value != undefined && value.length > 2) {
      this.spinner.show();
      this.textSearch = true;
      this.isShowPagination = false;
      let searchDto = {
        'orgList': [this.userData.organizationId],
        'searchKey': value,
        'searchType': 4,
      };
      // this.loading = true;
      // this._parentService.searchUsersBySearchKey(searchDto)
      //   .subscribe(data => {
      //     if (data != null && data.length > 0) {
      //       this.parentList = data;
      //       this.isShowPagination = true;
      //     } else {
      //       this.parentList = [];
      //     }
      //     this.spinner.hide();
      //     this.textSearch = false;
      //   }, (error: HttpErrorResponse) => {
      //     if (error instanceof HttpErrorResponse) {
      //       this.spinner.hide();
      //       this.textSearch = false;
      //     } else {
      //       this.toastService.ShowError(error);
      //       this.spinner.hide();
      //       this.textSearch = false;
      //     }
      //   });
    } else {
      if (value.length == 0 || value == undefined) {
        //  alert();
        this.textSearch = false;
        this.searchParents(false);
      }
    }
  }
  searchParents(isLoadMoreData: boolean) {

    let serachDto = null;

    if (isLoadMoreData) {
      // this.pageIndex = this.pageIndex + 1;
    } else {
      this.spinner.show();
      // this.setStaffListDivCollapsed = false;
      this.pageIndex = 0;
      this.parentList = [];
      this.filterData = (this.filterData != null && this.isDropDownOpen) ? this.filterData : null;
    }
    var ids = this.parentUniqueIds.length == 0 ? [] : this.parentUniqueIds

    this._userService.getParentUsers(this.pageIndex, this.pageSize, ids).subscribe(
      response => {
        if (response && response.parents) {
          let parentList = response.parents;
          this.isShowPagination = false;
          this.totalCount = (response.totalRecords) ? response.totalRecords : parentList.length;
          this.totalPages = response.totalPages;
          this.isNextPageValid = (this.totalPages == (this.pageIndex + 1)) ? false : true;
          this.isShowPagination = false;
          if (this.parentList == null || this.parentList.length == 0) {
            this.parentList = parentList;
            if (parentList != null && parentList.length > 0) {
              this.isShowPagination = true;
            }
          } else {
            if (parentList != null && parentList.length > 0) {
              this.parentList = parentList;
              this.isShowPagination = true;
            }
          }
          for (let i = 0; i < parentList.length; i++) {
            const element = parentList[i];
            this._userService.getImage("guardian", element.parent.uniqueId, false).subscribe(
              response => {
                if (response != null && response) {
                  if (response.image) {
                    this.parentList[i]['imageCode'] = response.image;
                  }
                }
              });
          }
        } else {
          this.toastService.ShowError((response && response.message) ? response.message : "Parents not found!");
          this.isNextPageValid = false;
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  // findRecords(value: any) {
  //   if (value.length > 0) {
  //     let data = {
  //       val: 'searchBar',
  //       searchDetails: value
  //     }
  //     // this.spinner.show();
  //     this.findParentRecords(data.searchDetails);
  //   } else {
  //     this.findParentRecords([]);

  //   }
  // }
  findRecords(value: any) {
    this.searchText = value;
    let stringLength = value.length;
    if (stringLength == 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      }
      this.message = "";
      this.maxLength = 50;

      this.searchUsers(value);
    } else if (stringLength > 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) == false) {
        }
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        if (stringLength != 10) {
        }
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      } else {
        let searchedUsers = [];
        if (this.searchableUserList.length > 0) {
          for (let i = 0; i < this.searchableUserList.length; i++) {
            const element = this.searchableUserList[i];
            if (element.fullName.toLowerCase().includes(value.toLowerCase())) {
              searchedUsers.push(element)
            }
          }
          this.searchUserList = searchedUsers;
          this.myDrop.open();
        }
        this.message = "";
        this.maxLength = 50;
      }
    } else if (stringLength == 0) {
      if (this.parentUniqueIds.length > 0) {
        this.parentUniqueIds = [];
        this.searchParents(false)
      }
      this.parentUniqueIds = [];
      this.message = "";
      this.searchUserList = [];
    }
  }
  checkInputValue(string) {
    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(string)) {//if (/^[a-zA-Z0-9]*$/.test(value) == false) {
      return 1;
    } else if (/^[0-9]*$/.test(string)) {
      return 2;
    } else {
      return 3;
    }
  }
  searchable(ele) {
    if (ele.keyCode == 13) {
      this.stringType = this.checkInputValue(this.searchText);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.searchText) == false) {
          this.toastService.ShowWarning("Email not valid!");
          this.message = "You search as email. Please complete email and enter";
          this.maxLength = 50;
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else if (this.stringType == 2) {
        this.maxLength = 10;
        if (this.searchText.length != 10) {
          this.message = "You search as mobile. Please complete mobile and enter";
          this.toastService.ShowWarning("Mobile not valid!");
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else {

      }
    }
  }
  searchUsers(value) {
    this._userService.searchUsers("guardian", value)
      .subscribe(data => {
        if (data != null && data.length > 0) {
          // this.totalCount=data.length;
          this.searchableUserList = data;
          this.searchUserList = data;
          this.isShowPagination = true;
          this.myDrop.open();
        } else {
          this.searchUserList = [];
          this.toastService.ShowWarning("User not found!");
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.textSearch = false;
        // } else {
        //   this.toastService.ShowError(error);
        //   this.textSearch = false;
        // }
        this.textSearch = false;
        this.spinner.hide();
      });
  }
  clickDropdownItem(item) {
    this.parentUniqueIds.push(item.uniqueId);
    this.searchParents(false);
    // this.parentList = [];
    // this.parentList.push(item)
    this.myDrop.close();
  }
  rowClick(user) {
    this.selectedParentId = user.id;
    this.selectedParent = user.parent;
    this.listClicked = true;
    this.isShowQuickForm = false;
    window.scroll(0, 0);
    this.detailsOpen.emit("open")
    var body = document.body;
    body.classList.add("hideScroll");
    // this.selectedStudentId=studentInfo.id
    // localStorage.setItem('selectedStudentId', currentId);
  }

  get gf() {
    let generalInfo = <FormGroup>this.parentsBasicInfoForm.controls['generalInfo'];
    return generalInfo.controls;
  }
  closeQuickForm() {
    this.isShowQuickForm = false;
    // this.parentsBasicInfoForm.reset();
    this.submitted = false;
  }
  OpenQuickForm() {
    this.isShowQuickForm = true
  }
  cancelFilterModal() {
    this.isDropDownOpen = false;
    this.myDrop.close();
  }
  apply() {
    this.searchParents(false);
    this.myDrop.close();
  }
  openDropdown() {
    this.isDropDownOpen = true;
  }
  getList(event) {
    if (event == "1") {
      this.isShowQuickForm = false;
    }
    this.findParentRecords([])
  }
  onItemChange(item, type) {
    let selectedItem = [];
    let id: number;
    switch (type) {
      case 0:
        this.selectedCourseList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedCourseList.push(element.id);
          }
        }
        this.searchParents(false);
        break;
      case 1:
        this.selectedYearList = [];
        this.selectedCalenderList = [];
        this.academicCalendarList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedYearList.push(element.id);
          }
        }
        this.searchParents(false);
        break;
      case 2:
        this.selectedCalenderList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedCalenderList.push(element.id);
          }
        }
        this.searchParents(false);
        break;
      case 3:
        this.selectedBoardList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedBoardList.push(element.id);
          }
        }
        this.searchParents(false);
        break;
      case 4:
        this.selectedGradeList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedGradeList.push(element.id);
          }
        }
        this.searchParents(false);
        break;
      case 5:
        this.selectedOrgList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedOrgList.push(element.id);
          }
        } else {
          this.selectedOrgList = [parseInt(this.userData.organizationId)]
        }
        this.searchParents(false);
        break;
    }
  }
  changePageSize(item) {
    this.pageSize = item.value;
    this.selectedPageSize = item.id;
  }
  goToPreviousPage() {
    if (this.pageIndex) {
      this.pageIndex = this.pageIndex - 1;
      this.isNextPageValid = true;
      this.searchParents(true);
    }
  }
  goToNextPage() {
    if (this.isNextPageValid) {
      this.pageIndex = this.pageIndex + 1;
      this.searchParents(true);
    }
  }
  checkUncheck(event) {
    this.isSelectAll = !this.isSelectAll
  }
  rowCheckboxChange(event) {
    if (!event.target.checked) {
      this.isSelectAll = false;
    }
  }
  getRoleInfo() {
    this._userService.getUserRolesData("parent")
      .subscribe(data => {
        if (data) {
          this.roleData = data;
        }
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
}
