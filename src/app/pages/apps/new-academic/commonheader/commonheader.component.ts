import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
@Component({
  selector: 'app-commonheader',
  templateUrl: './commonheader.component.html',
  styleUrls: ['./commonheader.component.scss']
})
export class CommonheaderComponent implements OnInit {
  tabNumber:any=2;
  constructor(private _commonService: NewCommonService,private router:Router) {
    this._commonService.isotherClicked.subscribe(tab=>{
      console.log('selected tab is1==>>',tab);
      this.tabNumber=tab
      if(tab=='2' || tab==2){
        this.tabNumber=2
        console.log('selected tab is2==>>',this.tabNumber)
      }
      
    })
   }

  ngOnInit() {
  }

  tabClick(tabNumber){
    localStorage.setItem('selectedTab',tabNumber)//basic=2,other=2
    this.tabNumber= tabNumber;
    this._commonService.isotherClicked.next(tabNumber)
    this._commonService.batchFromParent.next(0)
 
  }

}
