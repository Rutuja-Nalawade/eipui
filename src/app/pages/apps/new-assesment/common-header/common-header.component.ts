import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';

@Component({
  selector: 'app-common-header',
  templateUrl: './common-header.component.html',
  styleUrls: ['./common-header.component.scss']
})
export class CommonHeaderComponent implements OnInit {
  tabNumber: any = 3;
  isHideHeader = false;
  constructor(private _commonService: NewCommonService, private router: Router) {
    this._commonService.isotherClicked.subscribe(tab => {
      console.log('selected tab is1==>>', tab);
      this.tabNumber = tab
      if (tab == '3' || tab == 3) {
        this.tabNumber = 3
        console.log('selected tab is2==>>', this.tabNumber)
      }

    })
  }

  ngOnInit() {
  }

  tabClick(tabNumber) {
    localStorage.setItem('selectedTab', tabNumber)//basic=2,other=2
    this.tabNumber = tabNumber;
    this._commonService.isotherClicked.next(tabNumber)
    this._commonService.batchFromParent.next(0)

  }
  hideHeader(event) {
    if (event == "1") {
      this.isHideHeader = true;
    } else {
      this.isHideHeader = false;
    }
  }
  cancel(event) {
    this.isHideHeader = false;
  }
}
