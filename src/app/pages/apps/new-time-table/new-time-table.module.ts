import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewTimeTableRoutingModule } from './new-time-table-routing.module';
import { TimeTableListComponent } from './time-table-list/time-table-list.component';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule, TabsModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { CommonTopBarComponent } from './common-top-bar/common-top-bar.component';
import { CreateTimetableComponent } from './create-timetable/create-timetable.component';
import { RightsideFormsComponent } from './rightside-forms/rightside-forms.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { TestDesignComponent } from './test-design/test-design.component';
import { CreateNewLectureComponent } from './create-new-lecture/create-new-lecture.component';
import { AssignShiftComponent } from './assign-shift/assign-shift.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AssignPeriodComponent } from './assign-period/assign-period.component';
import { ViewLectureTimetableComponent } from './view-lecture-timetable/view-lecture-timetable.component';
import { ViewConflictComponent } from './view-conflict/view-conflict.component';
import { ImportXmlComponent } from './import-xml/import-xml.component';
import { SubstitutionComponent } from './substitution/substitution.component';
import { SetupNewUIModule } from '../setup-new-ui/setup-new-ui.module';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { TimetableLectureViewComponent } from './timetable-lecture-view/timetable-lecture-view.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { ImportPeriodComponent } from './import-period/import-period.component';
import { SubtituteLetureComponent } from './subtitute-leture/subtitute-leture.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { LinkingSubjectComponent } from './linking-subject/linking-subject.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PreviewXmlTimetableComponent } from './preview-xml-timetable/preview-xml-timetable.component';
import { TruncatePipe } from './TruncatePipe.pipe';
@NgModule({
  declarations: [TimeTableListComponent, CommonTopBarComponent, CreateTimetableComponent, RightsideFormsComponent, CreateNewLectureComponent, TestDesignComponent, AssignShiftComponent, AssignPeriodComponent, ViewLectureTimetableComponent, ViewConflictComponent, ImportXmlComponent, SubstitutionComponent, TimetableLectureViewComponent, ImportPeriodComponent,SubtituteLetureComponent, LinkingSubjectComponent, PreviewXmlTimetableComponent, TruncatePipe],
  imports: [
    CommonModule,
    SetupNewUIModule,
    UIModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgbDropdownModule,
    BsDatepickerModule.forRoot(),
    NgSelectModule,
    NewTimeTableRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TabsModule.forRoot(),
    NgMultiSelectDropDownModule,
    AngularMultiSelectModule,
    ClickOutsideModule,
    NgxMaterialTimepickerModule,
    DragDropModule
  ]
})
export class NewTimeTableModule { }
