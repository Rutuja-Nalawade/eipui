import { CommonDto } from '../dashboard3/dashboar3.model';

// Dashboard-2 card data
export interface Widget {
    icon: string;
    value: string;
    text: string;
    color: string;
    progressValue: number;
}


// Selling Products Table data

export interface Sellingproduct {
    name: string;
    price: string;
    quantity: number;
    amount: string;
    date: string;
    sales: number;
    productid: number;
}

// Chart data
export interface ChartType {
    chart?: any;
    plotOptions?: any;
    colors?: any;
    series?: any;
    stroke?: any;
    fill?: any;
    labels?: any;
    markers?: any;
    legend?: any;
    xaxis?: any;
    yaxis?: any;
    tooltip?: any;
    grid?: any;
    toolbar?: any;
    type?: any;
    datasets?: any;
    option?: any;
    height ?: any;
    piechartcolor?: any;
    dataLabels?: any;
    sparkline?: any;
}

export interface period {
    period?:number;
    name?:string;
    short?:string;
    starttime?:string;
    endtime ?:string;
    cardList?:Array<card>;
    startTimeInDate : Date;
    endTimeInDate : Date;

}

export interface card {
    periodId?:number;
    days?:string;
    lessonList?:Array<lesson>;
}

export interface lesson {
   id?:string;
   name?:string;
   classroomId?:string;
   classroomName?:string;
   subjectName?:string;
   subjectShortName?:string;
   subjectId?:string;
   batchIds?:Array<TTEntityDto>;
   teacherIds?:Array<TTEntityDto>;
   groupIds?:Array<TTEntityDto>;
   //batchName?:string;
   //batchId?:string;
   //teacherId?:string;
   //teacherName?:string;
   //teacherShortName?:string;
}

export interface TTEntityDto {
    id?:string;
    name?:string;
    shortName?:string;
}

export interface TimetableSubjectLinkDto {
    orgUniqueId?:string;
    createdOrUpdatedBy?:string;
    linkedSubjectList?:Array<courseSubjectLinkDto>;
}

export interface courseSubjectLinkDto {
    uniqueId?:string;
    courseId?:string;
    courseName?:string;
    createdOrUpdatedBy?:string;
    subjectList?:Array<CommonSubjectDto>;
    compulsorySubjectList?:Array<CommonSubjectDto>;
    optionalSubjectList?:Array<CommonSubjectDto>;
    electiveSubjectList?:Array<ElectiveSubjectDto>;
}

export interface ElectiveSubjectDto {
    groupName?:string;
    groupId?:string;
    electiveSubjectList?:Array<CommonSubjectDto>;
}

export interface CommonSubjectDto{
    subjectId?:string;
    name?:string;
    createdOrUpdatedBy?:string;
}


