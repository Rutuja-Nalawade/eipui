import { Answers } from "./answers.model";
import { Options } from "./options.model";

export class Questions {
    isModified: boolean;
    isAnswerCorrected: boolean;
    isQuestionBonus: boolean;
    bonusMarksFor: number;
    id: number;
    question: any;
    questionType: number;
    uniqueId: any;
    options: any;
    answers: any;
    bonusmarks:number;
}
