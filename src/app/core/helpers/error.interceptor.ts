import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { Router } from '@angular/router';
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthServiceService, private _tostSupportService: ToastsupportService, private _router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            console.log('error recieved' + JSON.stringify(err))
            this._tostSupportService.ShowError(err.status + err.message)
            switch (err.status) {
                case 401:
                    this.authenticationService.logout();
                    location.reload();
                    break;
                case 422:
                    const error = err;//| err.error.text 
                    console.log(error);
                    return throwError(error);
                    break;
                case 403:
                    break;
                case 404:
                    //  this._router.navigateByUrl('/dashboards/error/' + err.status)
                    break;
                case 502:
                    break;
            }
            return throwError(err.error.errorMessage || err.error || err.statusText);
            // if (err.status === 401) {
            //     // auto logout if 401 response returned from api
            //     this.authenticationService.logout();
            //     location.reload();
            // }
            // console.log(err);
            // if (err.status == 422) {

            // } else {
            //     const error = err.error.errorMessage || err.error || err.statusText;//| err.error.text 
            //     console.log(error);
            //     return throwError(error);
            // }
        }));
    }
}
