import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class ExamService {

  api: any;
  other_api: any;
  qbs_api: any
  studentList = new Subject();
  subjectList = new Subject();
  courses = new Subject();
  batches = new Subject();
  rightMarks = new Subject();
  spinner = new Subject();
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  constructor(private http: HttpClient) {
    // this.api = this._commonService.api.base_url
    // this.other_api = this._commonService.api.other_base_url
    this.api = environment.apiUrl,
      this.other_api = environment.eaasUrl,
      this.qbs_api = environment.qbsUrl

  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  public addExamPattern(examPattern: any) {
    return this.http.post<any>(this.other_api + '/exam/pattern/add', examPattern);
  }

  getPaginationExamList(orgUid: string, pageIndex: number) {
    // return this.http.get<any>(this.api + '/eicoreservices/api/get/pagination/exam/list/' + orgUid + '/' + pageIndex);
    return this.http.get<any>(this.other_api + '/exam/get/pagination/list/' + orgUid + '/' + pageIndex);
  } // this.api = this._commonService.api.base_url
  // ththis.other_api + '

  public getTestTypeList(orgUid: string, isAssignment: boolean) {
    return this.http.get<any>(this.other_api + '/exam-assignment/type/get/' + orgUid + '/' + isAssignment);
    // eaaservices/api/exam-assignment/type/get/2/false
  }

  public getUserByRoleAndOrg(searchDto: any) {
    return this.http.post<any>(this.api + '/user/list/byrole', searchDto);
  }
  orgUid
  public assignInvigilator(invigilatorDto: any) {
    return this.http.post<any>(this.api + '/assign/invigilator', invigilatorDto);
  }

  public getCourses(organizationId: any) {
    return this.http.get<any>(this.api + '/course/list/' + organizationId);
  }

  getByCourseList(courseList: any) {
    return this.http.post<any>(this.api + '/get/by/course/list', courseList);
  }

  public getSubjectByCourseList(courseList: any) {
    return this.http.post<any>(this.api + '/get/by/course/ids', courseList);
  }

  public getfilteredStudent(filterDto) {
    return this.http.post<any>(this.api + '/students/list/filter', filterDto)
  }

  public getsubjectSyllabus(filterDto) {
    return this.http.post<any>(this.api + '/get/syllabus', filterDto);
  }

  public addExam(exam: any) {
    // https://eip-dev.byteachers.com/exam/add
    return this.http.post<any>(this.other_api + '/exam/add', exam);
  }

  public getAllSubjectByOrg(orgId: any) {
    return this.http.get<any>(this.api + '/subject/list/get/' + orgId);
  }
  getExamType() {
    return this.http.get<any>(this.other_api + '/get/question/type/list');
  }
  public getMarkingSchemeList(orgId: any) {
    return this.http.get<any>(this.other_api + '/get/schemes/' + orgId);
  }

  public saveMarkingScheme(schemeData: any) {
    return this.http.post<any>(this.other_api + '/save/mcq/scheme', schemeData);
  }

  public getCoursePlannerList(subjectId: any, courseId): Observable<any> {


    return this.http.get<any>(this.api + '/get/course-planner/list/' + courseId + '/' + subjectId);
  }


  public getExamPatternList(patternType: any, orgId: any) {
    return this.http.get<any>(this.other_api + '/exam/pattern/list/' + patternType + "/" + orgId);
  }

  public getExamPatternFromEip(examPatternUid: string, patternType: any) {
    return this.http.get<any>(this.other_api + '/get/exam/pattern/info/' + examPatternUid + "/" + patternType);
  }

  public getExamPatternFromQbs(examPatternUid: string) {
    return this.http.post<any>(this.api + '/get/exam/pattern/info', examPatternUid);
  }
  getsubjectiveQuestionTypeList() {
    return this.http.get<any>(this.other_api + '/get/subjective/question/type/list');
  }

  getObjectiveQuestionTypeList() {
    return this.http.get<any>(this.other_api + '/get/objective/question/type/list');
  }

  public getJeeAdvanceSubjects(orgUid: string) {
    return this.http.get<any>(this.api + '/jee/advance/subject/list/' + orgUid);
  }
  addTestType(dto: any) {
    return this.http.post<any>(this.other_api + '/exam-assignment/type/add-update', dto);
  }
  getSearchedByUserData(serachDto: any) {
    return this.http.post<any>(this.api + '/get/users/byRoles', serachDto);
  }


  getSyllabus(cpId: any, type: number) {
    console.log("type", type, "cpId", cpId);
    return this.http.get<any>(this.api + '/get/syllabus/by/cp/type/' + cpId + '/' + type);
  }

  getExamTrack(examUid: string) {
    return this.http.get<any>(this.other_api + '/exam/track/get/' + examUid);
  }

  getGradeListByExamSubjectId(examSubjectId: number) {
    return this.http.get<any>(this.other_api + '/exam/grade/list/' + examSubjectId);
  }

  lockExamTrack(examUid: string) {
    return this.http.get<any>(this.other_api + "/exam/track/lock/" + examUid);
  }

  public getExamSubjectSyllabus(examSubjectId: number, testType: number, syllabusState: number) {
    return this.http.get<any>(this.other_api + '/exam/subject/syllabus/' + examSubjectId + '/' + testType + '/' + syllabusState);
  }


  public getAddUpdateTrackSyllabus(trackData: any) {
    return this.http.post<any>(this.other_api + '/exam/track/syllabus/add-update', trackData);
  }

  public getTopicsByChaptersIds(syllabusData: any) {
    return this.http.post<any>(this.other_api + '/exam/get/topic', syllabusData);
  }

  public getSubTopicsByChaptersIds(syllabusData: any) {
    return this.http.post<any>(this.other_api + '/exam/get/subtopic', syllabusData);
  }

  getExamTrackSyllabus(trackUid: String) {
    return this.http.get<any>(this.other_api + '/exam/track/syllabus/get/' + trackUid);
  }

  public getPatternList(organizationId: any) {
    return this.http.get<any>(this.other_api + '/get/pattern/list/' + organizationId);
  }

  public getPatternInfoFromEip(patternUid: string) {
    return this.http.post<any>(this.other_api + '/get/pattern/info', patternUid);
  }

  getQbsSyllabusUnits(qbsCourseId: number, qbsStandardId: number, subjectName: string) {
    return this.http.get<any>(this.qbs_api + '/syllabus/get/exam/units?qbsCourseId=' + qbsCourseId + '&qbsStandardId=' + qbsStandardId + '&subjectName=' + subjectName);
  }

  getChapters(unitIds: any) {
    return this.http.post<any>(this.qbs_api + '/syllabus/get/chapter', unitIds)
  }

  getTopics(chapterIds: any) {
    return this.http.post<any>(this.qbs_api + '/syllabus/get/topic', chapterIds);
  }

  getSubTopics(topicIds: any) {
    return this.http.post<any>(this.qbs_api + '/syllabus/get/subtopic', topicIds);
  }

  getSyllabusQuestions(data: any) {
    return this.http.post<any>(this.qbs_api + '/pagination/getQuestions/bySyllabus', data)
  }

  getTrackQuestions(trackUid: string) {
    return this.http.get<any>(environment.eaasUrl + "/exam/track/questions/get/" + trackUid);
  }

  saveExamQuestions(questionData: any) {
    return this.http.post(environment.eaasUrl + "/exam/track/questions/save", questionData);
  }

  deleteExamQuestions(questionData: any) {
    return this.http.post(environment.eaasUrl + "/exam/track/questions/delete", questionData);
  }

  cancelExam(examId: string) {
    return this.http.get<any>(this.api + '/cancel/exam/' + examId);
  }
}

