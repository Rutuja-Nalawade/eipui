import { Questions } from "./questions.model";

export class Sections {
    id: number;
    sectionName: any;
    questionType: number;
    questionMarkingDto: any;
    markingSchemeDtos:any;
    optionalQuestions:any;
    questionDtos: Questions[];
}
