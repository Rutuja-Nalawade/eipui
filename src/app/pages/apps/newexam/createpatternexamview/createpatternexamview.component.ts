import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-createpatternexamview',
  templateUrl: './createpatternexamview.component.html',
  styleUrls: ['./createpatternexamview.component.scss']
})
export class CreatepatternexamviewComponent implements OnInit {

  @Input() exam : any; 
   @Input() courseName : any; 
   @Input() batchName : any; 
   @Input() testType : string;
   isTenStud = true;
  constructor() { }

  ngOnInit() {
  }

  showAll(){
    this.isTenStud=false;
  }

}
