import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PeriodServiceService {

  constructor(private http: HttpClient) {}
  getAllPeriod(){
    return this.http.get<any>(environment.newApiUrl + '/shifts/get/periods/all')
  }

  public updatePeriod(periodDetails) {
    return this.http.put<any>(environment.newApiUrl + '/period/update', periodDetails);
  }

  public savePeriod(period:any) {
    return this.http.post<any>(environment.newApiUrl + '/period/add',period);
  }

  public deletePeriod(periodUniqueId: any) {
    console.log('delete id recieved' + periodUniqueId)
    return this.http.delete<any>(environment.newApiUrl + '/period/delete/'+ periodUniqueId);
  }


  getAllPeriodTimeTableWise(timetableId){
    return this.http.post<any>(environment.apiUrl + '/period/list/oftimetable',timetableId)
  }

}
