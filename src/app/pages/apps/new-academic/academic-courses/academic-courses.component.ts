import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-academic-courses',
  templateUrl: './academic-courses.component.html',
  styleUrls: ['./academic-courses.component.scss']
})
export class AcademicCoursesComponent implements OnInit {
  isFromSetup: boolean = false;
  @Input() currentTab: Number;
  gradeList = [];
  boardList = [];
  isEditAdd: boolean = false;
  courseList: any[];
  courseAndRefForm: FormGroup;
  editData: any;
  selectedIndex = 0;
  isAddNew = false;
  constructor(private _courseServiceService: CourseServiceService,
    private toastService: ToastsupportService,
    private router: Router,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService) {
    this.initCourseAndRefForm();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("currentValue", changes['currentTab']);

    if (changes['currentTab'].currentValue == 4) {

      this.isEditAdd = false;
      this.getCourseList();

    }
  }


  getCourseList() {
    this.spinner.show()
    this.courseList = []
    return this._courseServiceService.getCourseDetailsedInfoList(environment.BOTH_COURSES).subscribe(
      res => {
        if (res && res.success) {
          console.log("course detailed info = > ", JSON.stringify(res))
          this.courseList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage);
        }
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide()
        // } else {
        //   this.toastService.ShowWarning(error);
        //   this.spinner.hide()
        // }
        this.spinner.hide()

      });
  }

  initCourseAndRefForm() {
    this.courseAndRefForm = this.formBuilder.group({
      // orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      // orgId: sessionStorage.getItem('organizationId'),
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      courseList: this.formBuilder.array([this.initCourseList()])
    });
  }
  initCourseList() {
    return this.formBuilder.group({
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.required],
      isNew: [true, Validators.required],
      year: ['', Validators.required],
      month: ['', Validators.required],
      day: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      endDateValue: ['', Validators.required],
      masterCourseUniqueId: ['', Validators.required],
      academicSessionDto: ['', Validators.nullValidator],
      boardGradeList: this.formBuilder.array([this.initBoardGradeList()], Validators.required),
      alreadySelected: [[]],
      organizationId: ['', Validators.nullValidator],
      masterCourseDtoList: ['', Validators.nullValidator],
      duration: null,
      editMode: true,
      isShowDatePicker: false
    });
  }
  initBoardGradeList() {
    return this.formBuilder.group({
      uniqueId: ['', Validators.nullValidator],
      boardUniqueId: ['', Validators.required],
      gradeUniqueId: ['', Validators.required],
      boardName: ['', Validators.nullValidator],
      gradeName: ['', Validators.nullValidator],
      courseUniqueId: [null, Validators.nullValidator],
      courseName: [null, Validators.nullValidator],
      boardGradeHasCourseId: ['', Validators.nullValidator]
    });
  }

  addEditCourse(data) {
    this.selectedIndex = 5;
    this.isEditAdd = true;
    this.isAddNew = false
    this.editData = data;
  }

  backPage(value) {
    console.log("value", value)
    this.isEditAdd = value;
    this.getCourseList();
  }

  addCourse() {
    this.isAddNew = true
    console.log("test")
    this.selectedIndex = 3
    this.isEditAdd = true;
    this.editData = '';
  }
}
