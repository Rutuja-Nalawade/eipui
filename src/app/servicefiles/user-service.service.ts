import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }

  getUserByPageIndexAndType(page, usertype, recordcount) {
    return this.http.get<any>(environment.newAuthApiUrl + '/user/get/' + page + '/' + usertype + '?records=' + recordcount);
  }


  getUserRolesData(usertype) {
    return this.http.get<any>(environment.authUrl + '/send/' + usertype);
  }
  addUser(usertype, parentsBasicData: any): Observable<any> {
    return this.http.post<any>(environment.newAuthApiUrl + '/' + usertype + '/add/basicInfo', parentsBasicData).pipe(map(data => {

      console.log("Here will be return response code Ex :200", data)
      return data;
    }));
  }
  fileUpload(usertype, formdata, id) {
    return this.http.post<any>(environment.url + 'authentication-services/' + usertype + '/upload/' + id, formdata);
  }
  getImage(usertype, id, type) {
    return this.http.get<any>(environment.url + 'authentication-services/' + usertype + '/download/' + id + '/' + type);
  }
  getUserOtherInfo(usertype, uniqueId) {
    return this.http.get<any>(environment.newAuthApiUrl + '/' + usertype + '/get/otherPrivateInfo/' + uniqueId);
  }
  addUserOtherInfo(usertype, uniqueId, dto) {
    return this.http.post<any>(environment.newAuthApiUrl + '/' + usertype + '/add/otherPrivateInfo/' + uniqueId, dto);
  }
  updateUserOtherInfo(usertype, uniqueId, dto) {
    return this.http.put<any>(environment.newAuthApiUrl + '/' + usertype + '/update/otherPrivateInfo/' + uniqueId, dto);
  }
  getUserBasicInfo(usertype, uniqueId) {
    return this.http.get<any>(environment.newAuthApiUrl + '/' + usertype + '/get/basicinfo/' + uniqueId);
  }
  getUserEmailId(usertype, uniqueId) {
    return this.http.get<any>(environment.newAuthApiUrl + '/' + usertype + '/get/email/' + uniqueId);
  }
  getUserMobile(usertype, uniqueId) {
    return this.http.get<any>(environment.newAuthApiUrl + '/' + usertype + '/get/mobile/' + uniqueId);
  }
  updateUserBasicInfo(usertype, uniqueId, dto) {
    return this.http.put<any>(environment.newAuthApiUrl + '/' + usertype + '/update/basicinfo/' + uniqueId, dto);
  }
  updateUserEmailId(usertype, uniqueId, dto) {
    return this.http.put<any>(environment.newAuthApiUrl + '/' + usertype + '/update/email/' + uniqueId, dto);
  }
  updateUserMobile(usertype, uniqueId, dto) {
    return this.http.put<any>(environment.newAuthApiUrl + '/' + usertype + '/update/mobile/' + uniqueId, dto);
  }
  createGuardian(type, dto: any, uniqueId) {
    return this.http.put<any>(environment.newAuthApiUrl + '/' + type + '/add/guardian/' + uniqueId, dto);
  }
  saveGuardian(type, dto: any, uniqueId) {
    return this.http.put<any>(environment.newAuthApiUrl + '/' + type + '/save/guardian/' + uniqueId, dto);
  }
  getGuardians(type, uniqueId) {
    return this.http.get<any>(environment.newAuthApiUrl + '/' + type + '/get/guardian/' + uniqueId);
  }
  deleteGuardian(type, uniqueId) {
    return this.http.delete<any>(environment.newAuthApiUrl + '/' + type + '/delete/guardian/' + uniqueId);
  }
  getGuardianChildrens(type, uniqueId) {
    return this.http.get<any>(environment.newAuthApiUrl + '/' + type + '/get/children/' + uniqueId);
  }
  getAssignedShift(type, uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/' + type + '/get/shift/' + uniqueId);
  }
  assignShifts(type, dto: any, uniqueId) {
    return this.http.put<any>(environment.newApiUrl + '/' + type + '/assign/shift/' + uniqueId, dto);
  }
  getAcademicInfo(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/students/get/academicinfo/' + uniqueId);
  }
  assignAcademicInfo(dto: any) {
    return this.http.put<any>(environment.newApiUrl + '/students/assign/academicinfo', dto);
  }
  assignTeacherSubjects(dto: any, uniqueId) {
    return this.http.put<any>(environment.newApiUrl + '/teacher/assign/subjects/' + uniqueId, dto);
  }
  getTeacherSubjects(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/teacher/get/subjects/' + uniqueId);
  }
  getUsers(dto, type) {
    return this.http.put<any>(environment.newApiUrl + '/' + type + '/get/list', dto);
  }
  getAllStaff(filterDto) {
    return this.http.put<any>(environment.newAuthApiUrl + '/staff/get/list', filterDto);
  }
  searchUsers(type, value) {
    return this.http.get<any>(environment.newAuthApiUrl + '/' + type + '/search?queryString=' + value);
  }
  getOrgTeacherListWithPagination(filterDto: any) {
    return this.http.post<any>(environment.newApiUrl + '/teacher/get/list', filterDto);
  }
  getAssignedPeriod(uniqueId, shiftUid) {
    return this.http.get<any>(environment.newApiUrl + '/teacher/get/period/' + uniqueId + '/' + shiftUid);
  }
  assignPeriod(dto: any, uniqueId) {
    return this.http.put<any>(environment.newApiUrl + '/teacher/assign/period/' + uniqueId, dto);
  }
  getParentUsers(pageNo, size, dto) {
    return this.http.put<any>(environment.newAuthApiUrl + '/guardian/get/' + pageNo + '/' + size, dto);
  }
  getStateLiist() {
    return this.http.get<any>(environment.apiUrl + "/state/list");
  }
  allocateDetails(model) {
    return this.http.post<any>(environment.apiUrl + '/allocate/studentInfo', model);
  }
  bulkUploadUser(dto) {
    return this.http.post<any>(environment.newAuthApiUrl + '/bulk-upload/users', dto);
  }

  //perfix
  checkPerfix(perfix) {
    return this.http.get<any>(environment.apiUrl + "/organization/check/prefix/" + perfix);
  }
  addPerfix(perfix, type) {
    return this.http.get<any>(environment.apiUrl + "/organization/add/prefix/" + perfix + '/' + type);
  }

  //user states
  getUserStates(type) {
    return this.http.get<any>(environment.newAuthApiUrl + "/" + type + "/get/States/");
  }
  updateUserState(type, dto) {
    return this.http.put<any>(environment.newAuthApiUrl + "/" + type + "/update/State", dto);
  }
  getUserCurrentStates(type, uniqueId) {
    return this.http.get<any>(environment.newAuthApiUrl + "/" + type + "/get/state/" + uniqueId);
  }

   //new added by vinayak
   getTeacherListBySubjectList(uniqueId: string) {
    return this.http.get<any>(environment.apiUrl + '/subject-teacher/get/' + uniqueId);
  }
}
