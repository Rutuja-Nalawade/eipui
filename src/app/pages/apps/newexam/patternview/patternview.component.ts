import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExamService } from '../exam.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgSelectComponent } from '@ng-select/ng-select';


@Component({
  selector: 'app-patternview',
  templateUrl: './patternview.component.html',
  styleUrls: ['./patternview.component.scss']
})
export class PatternviewComponent implements OnInit {
  @ViewChild('ngSelectComponent', { static: false }) ngSelectComponent: NgSelectComponent;
  @Input() customPattern: any;
  @Input() examPattern: any;
  @Input() pattern: any;

  patterns:any
  subjectName:String=null;
  duration:any;
  totQuestions:any;
  totMarks:any;
  patterns1: any;
  patterns2: any;
  patternList = [];
  activeType:any=0;
  pattern1:boolean=true;
  pattern2:boolean=false;
  constructor(private spinner: NgxSpinnerService,
              private examService: ExamService,
              private toastService:ToastsupportService,) {
    
   }

  ngOnInit() {
    console.log("examPattern",this.examPattern);
    this.duration=this.examPattern.examPatterns[0].duration;
    this.totMarks=this.examPattern.examPatterns[0].examMarks;
    this.totQuestions=this.examPattern.examPatterns[0].totalQuestion;
    this.subjectName=null
    if(this.examPattern.patternType == 4){
      this.examPattern.examPatterns.forEach(element => {
        element.subjects.forEach(ele => {
          if(this.subjectName != null){
            this.subjectName=this.subjectName +','+ ele.name
          }else{
            this.subjectName= ele.name
          }
          
        });
      });
    }

    if(this.examPattern.patternType == 3 || this.examPattern.patternType == 2 || this.examPattern.patternType == 1){
      // this.patterns=this.examPattern.examPatterns[0]
      this.patterns1=this.examPattern.examPatterns[0]
      this.patterns2=this.examPattern.examPatterns[1]
        this.patterns1.subjects.forEach(ele => {
          if(this.subjectName != null){
            this.subjectName=this.subjectName +','+ ele.name
          }else{
            this.subjectName= ele.name
          }
          
        });

        if(this.examPattern.patternType == 3){
          this.getPatternList();
        }
    }
   
  }

  paperClick(activeid,inactive){
    this.ngSelectComponent.handleClearClick();
    this.activeType=activeid;
    let ele = document.getElementById(activeid);
    let element = document.getElementById(inactive)
    ele.classList.add('active')
    element.classList.remove('active')
    
    if(activeid == 0){
      // this.patterns1=this.examPattern.examPatterns[0]
      this.pattern2=false
      this.pattern1=true
      this.subjectName=null
      this.patterns1.subjects.forEach(ele => {
        if(this.subjectName != null){
          this.subjectName=this.subjectName +','+ ele.name
        }else{
          this.subjectName= ele.name
        }
        
      });
      console.log('this.patterns 0',this.patterns)
    }else if(activeid == 1){
      // this.patterns2=this.examPattern.examPatterns[1]
      this.pattern1=false
      this.pattern2=true
      this.subjectName=null
      this.patterns2.subjects.forEach(ele => {
        if(this.subjectName != null){
          this.subjectName=this.subjectName +','+ ele.name
        }else{
          this.subjectName= ele.name
        }
        
      });
      console.log('this.patterns 1',this.patterns)
    }
  }

  getPatternList() {
    this.spinner.show();
    this.patternList = [];
    return this.examService.getPatternList(sessionStorage.getItem('organizationId')).subscribe(
      res => {
        if (res) {
          this.patternList = res;
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error.error.errorMessage);
        // }
        this.spinner.hide();
      });
  }

  getPatternInfo(patternUid: any) {
    console.log("patternUid",patternUid)
    console.log("this.activeType",this.activeType)
    if (patternUid != null) {
      let pattern = this.patternList.find(x => x.uniqueId == patternUid.uniqueId);
      return this.examService.getPatternInfoFromEip(patternUid.uniqueId).subscribe(
        res => {
          if (res) {

            console.log("res",res)
            this.patterns = res;
            if (this.activeType == 0) {
              this.patterns1 = res;
            } else {
              this.patterns2 = res;
            }
          }
        },
        (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error.error.errorMessage);
          // }
        }
      );
    }
  }

}
