import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPatternSummaryComponent } from './view-pattern-summary.component';

describe('ViewPatternSummaryComponent', () => {
  let component: ViewPatternSummaryComponent;
  let fixture: ComponentFixture<ViewPatternSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPatternSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPatternSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
