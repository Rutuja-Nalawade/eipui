import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from './ui/ui.module';
import { NgxEditorModule } from 'ngx-editor';
import { FileUploadModule } from '@iplab/ngx-file-upload';
import { ModalModule, BsModalRef } from "ngx-bootstrap";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UIModule,
    NgxEditorModule,
    ModalModule.forRoot(),
    FileUploadModule

  ],
  exports: [],
  providers: [BsModalRef]
})
export class SharedModule { }
