import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-timedetails',
  templateUrl: './timedetails.component.html',
  styleUrls: ['./timedetails.component.scss']
})
export class TimedetailsComponent implements OnInit {
  @Input() timingInfo: FormGroup;
  @Input() examType: any;
  @Input() typeborder: any;
  @Input() assignmentTypeId: any;
  @Input() isFirstNext: any;
  mytime: Date;
  showTimePicker = false
  time: NgbTimeStruct = { hour: 13, minute: 30, second: 0 };
 
  todayDate=new Date();
  meridian = true;
  extraTimeArray = [{ id: 5, name: "5 Min" }, { id: 10, name: "10 Min" }, { id: 15, name: "15 Min" }, { id: 20, name: "20 Min" }, { id: 25, name: "25 Min" }]
  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChange) {
    console.log("object changes", changes);
    if (changes['assignmentTypeId'] && changes['assignmentTypeId'].currentValue == 2) {
      this.timingInfo.controls.duration.setValue(60);
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  startDatechanged() {

  }
  timerShow() {
    document.getElementById("timer").click(); // Click on the checkbox
  }
  selectTime() {
    this.showTimePicker = true
  }
  timeChange(event) {
    console.log("event", event)
  }
}
