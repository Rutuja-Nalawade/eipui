import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { PeriodServiceService } from 'src/app/servicefiles/period-service.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';

@Component({
  selector: 'app-create-timetable',
  templateUrl: './create-timetable.component.html',
  styleUrls: ['./create-timetable.component.scss']
})
export class CreateTimetableComponent implements OnInit {
  createTimeTableForm: FormGroup;
  submitted = false;
  academicYearList = [];
  slotList = [];
  workingDayList = [];
  settings: any = {};
  isSelectAll = false;
  typeList = [{ id: 1, name: "Manual" }, { id: 2, name: "Import" }, { id: 3, name: "Automatic" }];
  @Output() close = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder, private toastService: ToastsupportService, private timeTableService: TimeTableServiceService, private datePipe: DatePipe, private spinner: NgxSpinnerService, private periodService: PeriodServiceService, private shiftService: ShiftServiceService) {
    this.initCreateTimeTableForm();
  }

  ngOnInit() {
    this.settings = {
      singleSelection: false,
      idField: 'dayValue',
      textField: 'day',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: false,
      enableCheckAll: true
    };
    // this.getAcademicYearList();
    this.getOrgWorkDays();
    this.getOrgPeriodList();
  }
  initCreateTimeTableForm() {
    this.createTimeTableForm = this.formBuilder.group({
      name: ['', Validators.required],
      type: [1, Validators.required],
      days: [null, Validators.required],
      periods: [null, Validators.required]
    });
  }
  get f() { return this.createTimeTableForm.controls; }
  getOrgWorkDays() {
    return this.shiftService.getShiftWorkingDays().subscribe(
      res => {
        if (res) {
          let workingDayList = res;
          let dayList = [];
          if (workingDayList != null && workingDayList.length > 0) {
            for (let i = 0; i < workingDayList.length; i++) {
              let daysDto = {
                "uniqueId": workingDayList[i].uniqueId,
                "dayValue": workingDayList[i].dayValue,
                "day": workingDayList[i].name,
                "isLinked": false,
                "id": i + 1,
              }
              dayList.push(daysDto);
            }
            this.workingDayList = dayList;
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        //   this.toastService.ShowError(error);
        // }
      });
  }
  getOrgPeriodList() {
    return this.periodService.getAllPeriod().subscribe(
      res => {
        if (res) {
          let slotList = res;
          if (slotList != null && slotList.length > 0) {
            let slotdtolist = [];
            for (let i = 0; i < slotList.length; i++) {
              for (let j = 0; j < slotList[i].periods.length; j++) {
                const element = slotList[i].periods[j];
                console.log(element);
                let periodsDto = {
                  "uniqueId": element.uniqueId,
                  "name": element.name,
                  // "name": slotList[i].name + "(" + slotList[i].shiftName + "-" + this.datePipe.transform(slotList[i].shiftStartTime, 'h:mm a') + "-" + this.datePipe.transform(slotList[i].shiftEndTime, 'h:mm a') + ")",
                  "isLinked": false,
                  "id": i,
                }
                slotdtolist.push(periodsDto)

              }
            }
            this.slotList = slotdtolist;
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  change(daysList) {
    for (let i = 0; i < this.workingDayList.length; i++) {
      const element = this.workingDayList[i];
      if (daysList.length > 0) {
        for (let j = 0; j < daysList.length; j++) {
          if (daysList[j].dayValue == element.dayValue) {
            element.isLinked = true;
            break;
          } else {
            element.isLinked = false;
          }
        }
      } else {
        element.isLinked = false;
      }
      this.workingDayList[i] = element;
    }
    console.log(this.workingDayList, daysList.length);

    // this.isSelectAll = (this.workingDayList.length == daysList.length) ? true : false;
  }
  cancel() {
    this.close.emit("");
    this.submitted = false;
  }
  saveTimetable() {
    this.submitted = true;
    console.log(this.createTimeTableForm);
    if (this.createTimeTableForm.invalid) {
      return;
    }
    this.spinner.show();
    let timetabledto = this.createTimeTableForm.value;
    let days = timetabledto.days;
    let periods = timetabledto.periods;
    let workingdays = [];
    let periodList = [];
    for (let i = 0; i < this.workingDayList.length; i++) {
      if (days.includes(this.workingDayList[i].uniqueId)) {
        let dto = {
          uniqueId: this.workingDayList[i].uniqueId,
          dayValue: this.workingDayList[i].dayValue
        }
        workingdays.push(dto);
      }
    }
    for (let i = 0; i < this.slotList.length; i++) {
      if (periods.includes(this.slotList[i].uniqueId)) {
        let dto = {
          uniqueId: this.slotList[i].uniqueId,
          name: this.slotList[i].name
        }
        periodList.push(dto);
      }
    }
    timetabledto.days = workingdays;
    timetabledto.periods = periodList;
    console.log(timetabledto);
    this.timeTableService.saveTimetable(timetabledto).subscribe(
      (res) => {
        if (res) {
          this.toastService.showSuccess("Timetable Added Succesfully");
          this.spinner.hide();
          timetabledto['uniqueId'] = res.uniqueId;
          this.close.emit(timetabledto);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  onSelectAll(event) {
    let selected: any = []
    if (event.target.checked) {
      selected = this.workingDayList.map(item => item.uniqueId);
    }
    this.isSelectAll = !this.isSelectAll;
    this.createTimeTableForm.controls['days'].setValue(selected)
  }
  onItemSelect(event) {
    if (this.isSelectAll) {
      if (!event.target.checked) {
        this.isSelectAll = !this.isSelectAll;
      }
    } else {
      let days = this.createTimeTableForm.value.days;
      console.log(days);

      this.isSelectAll = (this.workingDayList.length == days.length) ? true : false;
    }
  }
  OnItemSelect(event) {
    console.log(event);
  }
  OnItemDeSelect(event) {
    console.log(event);
  }
}
