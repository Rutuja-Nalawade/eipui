import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.scss']
})

export class ComposeComponent implements OnInit {
  @ViewChild('filePreview', { static: true }) filePreviewModal: ElementRef;
  title = "";
  message = "";
  files: any = [];
  isShowFile = false;
  attachmentsForm: FormGroup;
  modalReference: NgbModalRef;
  categoryList = [];
  messageList = [];
  channelsList = [
    { id: 1, name: 'SMS' },
    { id: 2, name: 'WhatsApp' },
    { id: 3, name: 'Email' },
    { id: 4, name: 'Notification'},
  ];
  recipientsList = [];
  selectedUsersIds = [];
  ngbLgModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false,
    size: 'lg'
  };
  ngbSmModalOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false,
    centered:true,
    size: 'sm'
  };
  users = [
    { id: 1, name: 'Vilnius Kaunas' },
    { id: 2, name: 'Kaunas Kaunas' },
    { id: 3, name: 'Pavilnys Pabradė' },
    { id: 4, name: 'Pabradė Pabradė' },
    { id: 5, name: 'Klaipėda Kaunas' }
  ];
  variables = [
    { id: 1, name: '{Board}' },
    { id: 2, name: '{Teacher Name}' },
    { id: 3, name: '{Subject}' },
    { id: 4, name: '{Period}' },
    { id: 5, name: '{Grade}' },
    { id: 6, name: '{Teacher Name}' },
    { id: 7, name: '{Batch Group}' },
  ];
  userRoleList = [];
  recipientList = [];
  tempCategoryList = [];
  showFilterOptions:boolean = false;
  subjectList = [];
  batchList = [];
  gradeList = [];
  boardList = [];
  courseList = [];
  acadYearList = [];
  organizationList = [];
  studentList = [
    { id: 1, selectedName: 'Vilnius Kaunas', imageCode: '', selected: false },
    { id: 2, selectedName: 'Kaunas Kaunas', imageCode: '', selected: false },
    { id: 3, selectedName: 'Pavilnys Pabradė', imageCode: '', selected: true },
    { id: 4, selectedName: 'Pabradė Pabradė', imageCode: '', selected: true },
    { id: 5, selectedName: 'Klaipėda Kaunas', imageCode: '', selected: true },
    { id: 6, selectedName: 'Pavilnys Pabradė', imageCode: '', selected: true },
    { id: 7, selectedName: 'Pabradė Pabradė', imageCode: '', selected: true },
    { id: 8, selectedName: 'Klaipėda Kaunas', imageCode: '', selected: true }
  ];
  days=[{name:"M"},{name:"T"},{name:"W"},{name:"T"},{name:"F"},{name:"S"},{name:"S"}]
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  isShowScheduleAndSendForm :boolean = false;
  selectedType = 1;
  daily:boolean = true;
  weekly:boolean = false;
  monthly:boolean = false;
  yearly:boolean = false;
  noenddate:boolean = false;
  enddate:boolean = true;
  iterations:boolean = false;
  showCreateTemplete:boolean = false;
  channelId:number;
  constructor(private modalService: NgbModal, private formBuilder: FormBuilder,) { }

  ngOnInit() {
  }

  initAttachmentsForm() {
    this.attachmentsForm = this.formBuilder.group({
      attachments: this.formBuilder.array([])
    });
  }

  openModal(name) {
    if(this.channelId == 2 || this.channelId == 3|| this.channelId == 4){
    this.files = [];
    this.initAttachmentsForm();
    this.modalReference = this.modalService.open(name, { centered: true, keyboard: true, backdrop: 'static' });
    }
  }

  openSendNowModal(name) {
    this.modalReference = this.modalService.open(name, this.ngbSmModalOptions);
  }

  onFilesChange(event, index) {
    if (index == 1 && event.target.files) {

      for (let i = 0; i < event.target.files.length; i++) {

        var reader = new FileReader();

        reader.readAsDataURL(event.target.files[i]);

        reader.onload = (ev: any) => {
          this.files.push({ "src": ev.target.result, "name": event.target.files[i].name });
          // console.log(this.files)
        };
      }
    } else if (index == 2 && event) {

      for (let i = 0; i < event.length; i++) {
        var reader = new FileReader();

        reader.readAsDataURL(event[i]);

        reader.onload = (ev: any) => {
          this.files.push({ "src": ev.target.result, "name": event[i].name });
          // console.log(this.files)
        };
      }
    }
    this.modalReference.close();
    if(this.channelId == 2 || this.channelId == 4){
      setTimeout(() => {
        this.openPreviewModal();
      }, 1000);
    }else if(this.channelId == 3){
      this.isShowFile = true;
    }
  }

  patchAttachments(data) {
    let controlArray = <FormArray>this.attachmentsForm.controls['attachments'];
    controlArray.controls = [];
    for (let i = 0; i < data.length; i++) {
      controlArray.push(this.patchFiles(data[i]));
    }
    // console.log(this.attachmentsForm.value)
  }

  patchFiles(file) {
    return this.formBuilder.group({
      name: [file.name],
      src: [file.src],
      caption: [file.caption != null ? file.caption : null],
    });
  }

  openPreviewModal() {
    this.isShowFile = false;
    this.patchAttachments(this.files);
    this.modalReference = this.modalService.open(this.filePreviewModal, this.ngbLgModalOptions);
  }

  openPreviewCaption() {
    this.isShowFile = false;
    this.patchAttachments(this.attachmentsForm.value.attachments);
    this.modalReference = this.modalService.open(this.filePreviewModal, this.ngbLgModalOptions);
  }

  openCreateTempleteModal(name){
    this.showCreateTemplete = true;
    this.modalReference = this.modalService.open(name, {backdrop: 'static', keyboard: false});
  }

  removeAttachment(index) {
    let controlArray = <FormArray>this.attachmentsForm.controls['attachments'];
    // controlArray.removeAt(index);
  }

  closeModal() {
    this.showCreateTemplete = false;
    this.modalReference.close();
  }

  saveImages() {
    this.isShowFile = true;
  }

  onChange(index){
    
  }

   
  showFilterOption(){
    if(this.showFilterOptions){
      this.showFilterOptions = false;
    }else{
      this.showFilterOptions = true;
    }
  }

  hideFilterOption(){
    this.showFilterOptions = false;
  }

  onScroll() {
    // if (!(this.totalPages == (this.pageIndex + 1))) {
    //   this.pageIndex = this.pageIndex + 1;
    //   this.getData();
    // }
  }
  
  openSheduleAndSend() {
    // this.initSheduleAndSendForm();
    this.isShowScheduleAndSendForm = true;
  }

  changeType(type, activeId, deactiveId) {
    this.daily = true;
    this.weekly = false;
    this.monthly = false;
    this.yearly = false;

    let active = document.getElementById(activeId)
    let deactive = document.getElementById(deactiveId)

    active.classList.add('onetimeRepeatActive')
    deactive.classList.remove('onetimeRepeatActive')
    this.selectedType = type;

    if(type == 2){
      let stacktop = document.getElementById('stacktop');
      stacktop.classList.add('card-full-screen');
    }else if(type == 1){
      let stacktop = document.getElementById('stacktop');
      stacktop.classList.remove('card-full-screen');
    }
    //this.dayWeekChange(this.dateTime, type)
  }

  closeSheduleAndSend() {
    this.selectedType = 1;
    this.isShowScheduleAndSendForm = false;
  }

  changeScheduleType(type) {
    this.daily = false;
    this.weekly = false;
    this.monthly = false;
    this.yearly = false;

    let day = document.getElementById('day');
    let week = document.getElementById('week');
    let month = document.getElementById('month');
    let year = document.getElementById('year');

    day.classList.remove('schedule-type-selected');
    week.classList.remove('schedule-type-selected');
    month.classList.remove('schedule-type-selected');
    year.classList.remove('schedule-type-selected');

    this.enddate = true;
    this.iterations = false;
    this.noenddate = false;

    if (type == 1) {
      this.daily = true;
      day.classList.add('schedule-type-selected');
    } else if (type == 2) {
      this.weekly = true;
      week.classList.add('schedule-type-selected');
    } else if (type == 3) {
      this.monthly = true;
      month.classList.add('schedule-type-selected');
    } else {
      this.yearly = true;
      year.classList.add('schedule-type-selected');
    }
  }
  
  repeatUntil(option:any){
    if(option == 1){
      this.noenddate = true;
      this.enddate = false;
      this.iterations = false;
    }else if(option == 2){
      this.enddate = true;
      this.noenddate = false;
      this.iterations = false;
    }else{
      this.iterations = true;
      this.noenddate = false;
      this.enddate = false;
    }
  }

  onChangeChannel($event){
    this.channelId = $event.id;
    this.files = [];
  }
}
