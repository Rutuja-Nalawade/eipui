import { Pipe, PipeTransform } from '@angular/core';
import { AccessrightService } from 'src/app/servicefiles/accessrights.service';

@Pipe({
  name: 'getMemberShipLevel'
})
export class GetAccessRightPipe implements PipeTransform {

  constructor(public accessRightService : AccessrightService){
  }
  transform(value: number, args?: any): Boolean {
    return this.getMemberShipLevel(value);
  }
  getMemberShipLevel(point: number): Boolean{

    let accessRights :  Array<any>;
    // accessRights = JSON.parse(sessionStorage.getItem('entityAccessRights'));

    accessRights = JSON.parse(this.accessRightService.accessRightList);

    let isSetupDone = sessionStorage.getItem('isSetupDone');

    let entity = accessRights.find(entity => entity.id === point);

    // alert(JSON.stringify(entity));
    if(entity!=null && entity!=undefined && isSetupDone=="true"){
       return entity.isAccess;
    }
    return false;
 }
}