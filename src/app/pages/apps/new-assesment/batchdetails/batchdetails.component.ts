import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectComponent } from '@ng-select/ng-select';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

interface FilterDto {
  orgList: Array<any>;
  courseList: Array<any>;
  boardList: Array<any>;
  gradeList: Array<any>;
  batchList: Array<any>;
  batchIds: Array<any>;
  roleList: Array<any>;
  unassigned: any;
  page: any;
  size: any;
  states: any;
  responseFields: any;
  subjectList: Array<any>;
  genderList: Array<any>;
  studentUniqueIds: Array<any>;
  isForAll: Boolean;
  searchType: number;
  organizationUid: string;
}
@Component({
  selector: 'app-batchdetails',
  templateUrl: './batchdetails.component.html',
  styleUrls: ['./batchdetails.component.scss']
})
export class BatchdetailsComponent implements OnInit {
  @Input() batchInfo: FormGroup;
  @Input() isFirstNext: any;
  searchText;
  selectedStudentsList = [];
  viewSelectedStudentsList = [];
  remainingSelectedStudents = 0;
  courseList = [];
  batchList = [];
  studentList = [];
  students = [];
  boardId: string;
  checkedList: any;
  masterSelected: Boolean;
  @ViewChild('ngSelectComponent', { static: false }) ngSelectComponent: NgSelectComponent;
  constructor(private examService: ExamServiceService, private modalService: NgbModal, private toastService: ToastsupportService, private courseService: CourseServiceService, private batchService: BatchesServiceService, private subjectService: SubjectServiceService, private _DomSanitizationService: DomSanitizer, private _userService: UserServiceService) {
    this.masterSelected = false;
  }

  ngOnInit() {
    this.getCourseList();
    this.examService.studentList.subscribe(
      (value: any) => {
        console.log("value", value);
        this.studentList = value;
        console.log("studentList", this.studentList);
        // this.isAllSelected();
      });
  }

  public getCourseList() {
    this.courseList = [];
    return this.courseService.getCourseBasicInfoList(1).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            // console.log("get courseList => ",res)
            this.courseList = res.list;
            this.examService.courses.next(res.list);
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }

        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   // this._commonService.ShowError(error.error.errorMessage);
        // }
      });
  }

  getBatchAndSubjectList(event) {
    let courseList = [];
    this.batchList = [];
    this.studentList = [];
    this.examService.subjectList.next('');
    this.examService.studentList.next('');
    this.batchInfo.controls.courseName.setValue(event.name);
    this.batchInfo.controls.batchIds.setValue(null);
    this.batchInfo.value.batchIds = null;
    if (this.batchInfo.controls['courseId'].value != null && this.batchInfo.controls['courseId'].value != '') {
      this.ngSelectComponent.clearModel();
      this.batchInfo.controls.batchIds.setValue(null);
      let courseId: any = this.batchInfo.controls['courseId'].value;
      courseList.push(courseId);
      if (courseList != null && courseList.length > 0) {
        this.getBatchList(courseId);
        this.getSubjects(courseId);
      }
    }
  }

  getSubjects(courseId: any) {
    this.subjectService.getSubjectsByCourse(courseId).subscribe(
      res => {
        if (res && res.subjectsDtoList && res.subjectsDtoList.length > 0) {
          this.examService.subjectList.next(res.subjectsDtoList);
        } else {
          this.examService.subjectList.next([]);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }

  getBatchList(courseId: any) {
    this.batchList = [];
    return this.batchService.getBatchByCourse(courseId).subscribe(
      res => {
        if (res.success == true) {
          this.batchList = res.list;
          console.log("this.batchList", this.batchList);
          this.examService.batches.next(res.list);
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError("No Batches found for this course.");
        //   //   this._commonService.ShowError(error.error.errorMessage);
        // }
      });
  }

  getStudents(searchStudent: String) {
    this.studentList = [];
    let filterDto = {
      "batchIds": [],
      "courseIds": [],
      "groupCategoriesIds": [],
      "batchGroupIds": [],
      "gradeIds": [],
      "boardIds": [],
      "subjectIds": [],
      "page": 0,
      "size": 100,
      "unassigned": false,
      "states": [
        1,
        2
      ],
      "studentUniqueIds": [],
      "responseFields": {
        "courseField": false,
        "batchFiled": true,
        "boardField": false,
        "gradeField": false,
        "subjectField": false,
        "groupCategoryField": false,
        "batchGroupFiled": false
      }
    }
    if (this.batchInfo.controls['batchIds'].value != null && this.batchInfo.controls['batchIds'].value != '') {
      // let batch = this.batchList.find(x => x.uniqueId == this.batchInfo.controls['batchIds'].value);
      let batchList: [] = this.batchInfo.controls['batchIds'].value;
      filterDto.batchIds = batchList;
      // }
      // if (this.batchInfo.controls['courseId'].value != null) {
      console.log("filterDto", filterDto);
      return this.examService.getfilteredStudent(filterDto).subscribe(
        res => {
          if (res) {
            if (res.students && res.students.length > 0) {
              this.examService.studentList.subscribe(
                (value: any) => {
                  this.students = value;
                });
              this.studentList = res.students;
              if (this.students.length != 0) {
                if (this.students.length == res.students.length) {
                  this.masterSelected = true;
                }
                this.studentList.map((user, i) => {
                  this.getImage(user.student.uniqueId, i);
                  this.students.forEach(element => {
                    if (user.student.uniqueId == element.student.uniqueId) {
                      user.student.isSelected = true;
                    }
                  });
                })
              }
              if (this.students.length == 0) {
                this.studentList.map((user, i) => {
                  this.getImage(user.student.uniqueId, i);
                  user.student.isSelected = false
                })
              }
              this.modalService.open(searchStudent, { windowClass: "modalClassTest", backdrop: 'static' });

            } else {
              this.toastService.ShowError("Student data not found!")
            }
          }
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
        });
    } else {
      this.toastService.ShowWarning("Please select batch")
    }

  }
  radioButtonEvent(value: number) {
    // const controlArray = <FormArray>this.quickExamForm.controls['studentList'];
    // for (let i = controlArray.length - 1; i >= 0; i--) {
    //   controlArray.removeAt(i);
    // }
    // if (value == 1) {
    //   this.quickExamForm.setControl('studentList', this.fb.array(this.students));
    // }
  }
  addDeleteStudentUid(values: any, user: any) {
    // const controlArray = <FormArray>this.quickExamForm.controls['studentList'];
    // if (values.currentTarget.checked) {
    //   controlArray.push(new FormControl(user));
    // } else {
    //   let index = controlArray.controls.findIndex(x => x.value == user.id);
    //   controlArray.removeAt(index);
    // }
  }
  save() {
    this.selectedStudentsList = []
    this.selectedStudentsList = this.checkedList;
    this.masterSelected = false;
    this.viewSelectedStudentsList = this.checkedList.slice(0, 6);
    if (this.checkedList.length > 5) {
      this.remainingSelectedStudents = this.checkedList.length - 6;
    } else {
      this.remainingSelectedStudents = 0
    }
    this.examService.studentList.next(this.selectedStudentsList);
    console.log("checkedList", this.checkedList);

  }
  checkUncheckAll() {
    console.log("test")
    for (var i = 0; i < this.studentList.length; i++) {
      this.studentList[i].student.isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.studentList.every(function (item: any) {
      return item.student.isSelected == true;
    })
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    for (var i = 0; i < this.studentList.length; i++) {
      if (this.studentList[i].student.isSelected)
        this.checkedList.push(this.studentList[i]);
    }
    this.checkedList = this.checkedList;
  }
  getImage(uniqueId, i) {
    this._userService.getImage("student", uniqueId, false).subscribe(
      response => {
        if (response != null && response) {
          if (response.image) {
            this.studentList[i]['imageCode'] = response.image;
          }
        }
      });
  }
  changeBatch(event) {
    console.log(event, this.checkedList);
    if (this.checkedList && this.checkedList.length > 0) {
      let batchNames = [];
      let studentList = [];
      event.forEach(element => {
        batchNames.push(element.name);
      });
      this.checkedList.forEach(element => {
        if (batchNames.includes(element.batch)) {
          studentList.push(element);
        }
      });
      this.checkedList = studentList;
      this.students = studentList;
      this.examService.studentList.next(studentList);
    }
  }
}
