import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonTopBarComponent } from './common-top-bar/common-top-bar.component';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BsDatepickerModule } from 'ngx-bootstrap';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { ComposeComponent } from './compose/compose.component';
import { FileDragDropDirective } from './file-drag-drop.directive';
import { NewAnnouncementRoutingModule } from './new-announcement-routing.module';

@NgModule({
  declarations: [CommonTopBarComponent, MessageListComponent, MessageDetailsComponent, ComposeComponent, FileDragDropDirective],
  imports: [
    CommonModule,
    UIModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgbDropdownModule,
    BsDatepickerModule.forRoot(),
    NgSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NewAnnouncementRoutingModule,
    NgxMaterialTimepickerModule
  ]
})

export class NewAnnouncementModule { }
