import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { AcademicService } from '../academic.service';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClassroomServiceService } from 'src/app/servicefiles/classroom-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-academic-class-rooms',
  templateUrl: './academic-class-rooms.component.html',
  styleUrls: ['./academic-class-rooms.component.scss']
})
export class AcademicClassRoomsComponent implements OnInit {
  @Input() currentTab: Number;
  classData: any = [];
  classRoomForm: FormGroup;
  classRoomFormMain: FormGroup;
  isShowClassRoomForm: boolean = false;
  isEdit: boolean = false;
  classType = []
  classCategory = []
  classRoomName: any;
  classRoomQrCode: any;
  elementType: 'url' | 'canvas' | 'img' = 'url';
  href: string;


  constructor(private academicServie: AcademicService,
    private _classroomServiceService: ClassroomServiceService,
    private toastService: ToastsupportService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService) {
    this.classRoomFormMain = this.formBuilder.group({
      organizationUid: sessionStorage.getItem('orgUniqueId'),
      orgId: sessionStorage.getItem('organizationId'),
      createdOrUpdatedById: sessionStorage.getItem('uniqueId'),
      classRoomList: this.formBuilder.array([])
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("currentValue", changes['currentTab']);

    if (changes['currentTab'].currentValue == 7) {
      console.log("this.curremt tab", this.currentTab)
      this.isShowClassRoomForm = false;
      this.getClassRoomList();
      this.getClassRoomTypes();
      this.getClassRoomCategories();
    }
  }

  ngOnInit() {
  }

  initClassRoomArray() {
    this.classRoomForm = this.formBuilder.group({
      uniqueId: [null, Validators.nullValidator],
      category: [null, Validators.nullValidator],
      name: [null, Validators.required],
      type: [null, Validators.required],
      room_url: [null, Validators.nullValidator],
    });
  }

  getClassRoomTypes() {
    this.spinner.show()
    this.classData = [];
    return this._classroomServiceService.getClassroomTypes().subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            this.classType = res.list;
          } else {
            this.toastService.ShowInfo(res.responseMessage)
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log(error);
        // } else {
        //   this.toastService.ShowError(error);

        // }
        this.spinner.hide()
      });
  }
  getClassRoomCategories() {
    this.spinner.show()
    this.classData = [];
    return this._classroomServiceService.getClassroomCategories().subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            this.classCategory = res.list;
          } else {
            this.toastService.ShowInfo(res.responseMessage)
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log(error);
        // } else {
        //   this.toastService.ShowError(error);

        // }
        this.spinner.hide()
      });
  }
  getClassRoomList() {
    this.spinner.show()
    this.classData = [];
    return this._classroomServiceService.getAcademicClassRoom().subscribe(
      res => {
        if (res.success == true) {
          this.classData = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.patchClass(this.classData);
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log(error);
        // } else {
        //   this.toastService.ShowError(error.error.errorMessage);

        // }
        this.spinner.hide()
      });
  }

  saveClassForm() {
    this.spinner.show()
    console.log('save clasrrom =>', this.classRoomForm.value);
    let classromArray = [];
    classromArray.push(this.classRoomForm.value)
    this._classroomServiceService.saveAcademicClassRoom(classromArray).subscribe(
      res => {
        this.spinner.hide()
        this.toastService.ShowInfo(res.responseMessage)
        if (res.success == true) {
          this.isShowClassRoomForm = false;
          this.isEdit = false;
          this.getClassRoomList();
          this.classRoomForm.reset();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log(error);
        // } else {
        //   this.toastService.ShowError(error.error.errorMessage);

        // }
        this.spinner.hide()
      });
  }

  updateClassForm() {
    this.spinner.show()
    console.log('save clasrrom =>', this.classRoomForm.value);
    let classromArray = [];
    classromArray.push(this.classRoomForm.value)
    this._classroomServiceService.updateAcademicClassRoom(classromArray).subscribe(
      res => {
        this.spinner.hide()
        this.toastService.ShowInfo(res.responseMessage)
        if (res.success == true) {
          this.isShowClassRoomForm = false;
          this.isEdit = false;
          this.getClassRoomList();
          this.classRoomForm.reset();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log(error);
        // } else {
        //   this.toastService.ShowError(error.error.errorMessage);

        // }
        this.spinner.hide()
      });
  }
  patchClass(classList: any) {
    let controlArray = <FormArray>this.classRoomFormMain.controls['classRoomList'];
    controlArray.controls = [];
    for (let i = 0; i < classList.length; i++) {
      controlArray.push(this.classRoomWithValue(classList[i]));
    }
  }
  classRoomWithValue(classs) {
    return this.formBuilder.group({
      uniqueId: [classs.uniqueId, Validators.nullValidator],
      name: [classs.name, Validators.required],
      classroomType: [classs.classroomType, Validators.nullValidator],
      classroomCategory: [classs.classroomCategory, Validators.nullValidator],
      roomUrl: [classs.roomUrl, Validators.nullValidator],
    });
  }

  addClassRoom() {
    this.isEdit = false;
    this.initClassRoomArray()
    this.isShowClassRoomForm = true;
  }

  closeClassForm() {
    this.isShowClassRoomForm = false;
    this.isEdit = false;
  }

  editClassRoom(classRoom) {
    this.initClassRoomArray();

    this.isEdit = true;
    this.classRoomForm.patchValue({
      uniqueId: classRoom.uniqueId,
      name: classRoom.name,
      type: classRoom.type,
      category: classRoom.category,
      room_url: classRoom.room_url,

    });
    this.isShowClassRoomForm = true;

  }

  removeClassRoom() {
    console.log('delet classroom => ', this.classRoomForm.controls['uniqueId'].value)
    // this.dataLoading = true;  
    this.spinner.show()
    this._classroomServiceService.deleteAcademicClassRoom(this.classRoomForm.controls['uniqueId'].value).subscribe(
      res => {
        //this.board = response;
       if(res.success == true){
        this.isShowClassRoomForm = false;
        this.isEdit = false;
        this.getClassRoomList()
        // this.dataLoading = true;  
       }
       this.toastService.showSuccess(res.responseMessage);
        this.spinner.hide()
      },
      error => {
      //  this.toastService.ShowError(error);
        this.spinner.hide()
      })
  }

  openConfirm(content) {

    this.modalService.open(content, { centered: true });
  }

  openQRCodeModal(qrCodeModal: string, classRoom) {
    this.modalService.open(qrCodeModal, { centered: true });
    console.log("test", classRoom)
    this.classRoomName = classRoom.name;
    this.classRoomQrCode = classRoom.uniqueId;


  }

  downloadQRCode() {
    console.log("test qr")
    const fileNameToDownload = this.classRoomName + '_image_qrcode';
    let element: HTMLElement = document.getElementById('qrCodeImage');
    const base64Img = element.getElementsByTagName('img')[0].src;
    fetch(base64Img)
      .then(res => res.blob())
      .then((blob) => {
        // IE
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = fileNameToDownload;
        link.click();
      })
  }

  download(classRoom) {
    console.log('1' + JSON.stringify(classRoom))
    this.classRoomName = classRoom.name;
    this.classRoomQrCode = classRoom.uniqueId;
    setTimeout(function () {
      console.log('2' + JSON.stringify(classRoom))
      const fileNameToDownload = classRoom.name + '_image_qrcode';
      let element: HTMLElement = document.getElementById('qrCodeImagehiden');
      const base64Img = element.getElementsByTagName('img')[0].src;
      fetch(base64Img)
        .then(res => res.blob())
        .then((blob) => {
          // IE
          const url = window.URL.createObjectURL(blob);
          const link = document.createElement('a');
          link.href = url;
          link.download = fileNameToDownload;
          link.click();
        })
    }, 10);

  }
}
