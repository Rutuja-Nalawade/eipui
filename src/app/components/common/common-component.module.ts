import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuickFormComponent } from './quick-form/quick-form.component';
import { AdvancedFilterComponent } from './advanced-filter/advanced-filter.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserBasicFormComponent } from './user-basic-form/user-basic-form.component';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { NgbDropdownModule, NgbModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { BsDatepickerModule, BsDropdownModule, TabsModule } from 'ngx-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { AcademicDemoComponent } from 'src/app/pages/apps/new-learner/academic-demo/academic-demo.component';
import { GuardianComponent } from 'src/app/pages/apps/new-learner/guardian/guardian.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxSpinnerModule } from 'ngx-spinner';
import { GuardianFormValidatorPipe } from 'src/app/pages/apps/new-learner/guardian-form-validator.pipe';
import { UserOtherFormComponent } from './user-other-form/user-other-form.component';
import { TeacherSubjectsComponent } from 'src/app/pages/apps/teacher/teacher-subjects/teacher-subjects.component';
import { UserWorkTimeFormComponent } from './user-work-time-form/user-work-time-form.component';
import { AssignStudentComponent } from 'src/app/pages/apps/parents/assign-student/assign-student.component';
import { AssignPeriodComponent } from 'src/app/pages/apps/teacher/assign-period/assign-period.component';



@NgModule({
  declarations: [QuickFormComponent, AdvancedFilterComponent, UserDetailsComponent, UserBasicFormComponent, AcademicDemoComponent, GuardianComponent, GuardianFormValidatorPipe, UserOtherFormComponent, TeacherSubjectsComponent, UserWorkTimeFormComponent, AssignStudentComponent, AssignPeriodComponent],
  imports: [
    CommonModule,
    UIModule,
    NgbTypeaheadModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgbModule,
    NgSelectModule,
    NgbDropdownModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgApexchartsModule,

    BsDatepickerModule.forRoot(),
    DragDropModule,
    NgxSpinnerModule,
  ],
  exports: [QuickFormComponent, AdvancedFilterComponent, UserDetailsComponent, UserBasicFormComponent],
  providers: [GuardianFormValidatorPipe],
  bootstrap: [UserDetailsComponent, AssignStudentComponent]
})
export class CommonComponentModule { }
