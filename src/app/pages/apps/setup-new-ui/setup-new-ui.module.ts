import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetupNewUIRoutingModule } from './setup-new-ui-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BoardAndGradeComponent } from './board-and-grade/board-and-grade.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CourseAndRefrencesComponent } from './course-and-refrences/course-and-refrences.component';
import { SubjectAndLinkingSubjectComponent } from './subject-and-linking-subject/subject-and-linking-subject.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ClickOutsideModule } from 'ng-click-outside';
import { SetupComponent } from 'src/app/account/new-authentication/setup/setup.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [BoardAndGradeComponent, CourseAndRefrencesComponent, SubjectAndLinkingSubjectComponent],
  imports: [
    CommonModule,
    SetupNewUIRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UIModule,
    BsDatepickerModule.forRoot(),
    NgMultiSelectDropDownModule,
    NgSelectModule,
    DragDropModule,
    ClickOutsideModule,
    NgbModule
  ],
  providers: [SetupComponent],
  exports: [CourseAndRefrencesComponent, SubjectAndLinkingSubjectComponent],
  bootstrap: []
})
export class SetupNewUIModule { }
