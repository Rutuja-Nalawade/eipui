import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CommonTopBarComponent} from './common-top-bar/common-top-bar.component'


const routes: Routes = [{
  path: 'details',
  component: CommonTopBarComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewAttendanceRoutingModule { }
