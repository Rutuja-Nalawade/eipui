import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Input, OnChanges } from '@angular/core';
import MetisMenu from 'metismenujs/dist/metismenujs';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { AccessrightService } from 'src/app/servicefiles/accessrights.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() isCondensed = false;
  error = '';
  loading = false;
  show: boolean = false;
  menu: any;
  //sidebar:any;
  entityAccessList: any;
  result: Boolean;
  isLeftLoaded: Boolean = false;
  organziationType;
  isSetupDone: boolean = false;
  sidebar = [];
  @ViewChild('sideMenu', { static: false }) sideMenu: ElementRef;
  params: any;
  selectedIndex: number = 1;
  organizationDetails:any;

  constructor(private routes: ActivatedRoute, private route: Router, private accessRightService: AccessrightService, private toastService: ToastsupportService) {
    let setupStatus = sessionStorage.getItem('isSetupDone');
    this.isSetupDone = (setupStatus == "false" ? false : true);
    this.params = routes.snapshot.params;
    this.isLeftLoaded = false;

    //this.getEntityListAccess();
    this.organziationType = sessionStorage.getItem('organizationType');

  }



  ngOnInit() {
  
    this.sidebar = [
      {
        id: 2,
        name: 'Learners'
      },
      {
        id: 3,
        name: 'Educators'
      },
      {
        id: 4,
        name: 'Academic'
      },
      {
        id: 5,
        name: 'Curriculum'
      },
      {
        id: 6,
        name: 'Timetable'
      },

      {
        id: 7,
        name: 'Exam'
      },
      {
        id: 8,
        name: 'Calendar'
      },

      {
        id: 9,
        name: 'Work Timings'
      },

      {
        id: 10,
        name: 'Attendance'
      },
      {
        id: 11,
        name: 'Drive'
      },
      {
        id: 12,
        name: 'Communication'
      },

    ]

    let user = sessionStorage.getItem('currentUser');
    let currentUser = JSON.parse(user);
    // this.toastService.showSuccess("Welcome "+currentUser.fullName);

    this.accessRightService.isLeftLoaded.subscribe(
      (value: any) => {
        setTimeout(() => { this.isLeftLoaded = value; }, 100);
      }
    );

    // this.userService.setSideMenuVisible.subscribe(
    //   (value: any) => {
    //     this.isCondensed = value;
    //     this.ngOnChanges();
    //   }
    // );

  }
  ;


  getMemberShipLevel(point: number): Boolean {
    return this.accessRightService.checkUserHasAuthorityToAccess(point);
  }

  getMemberShipLevelEntity(parentValue: number, childValue: number): Boolean {
    return this.accessRightService.checkUserHasAuthorityToAccessEntity(parentValue, childValue);
  }

  //   onClickMenu(id,index:number){
  //     //  alert(id)
  //     this.selectedIndex = index;
  //      switch (id){

  //       case 2:{
  //         this.route.navigate(['/hr/2/staff']);
  //         break;
  //        }
  //        case 11:{
  //         this.route.navigate(['/shifts/shifts/11']);
  //         break;
  //        }
  //        case 12:{
  //         this.route.navigate(['/announcements/announcements/12']);
  //         break;
  //        }
  //        case 13:{
  //         this.route.navigate(['/approval/approval/13']);
  //         break;
  //        } 
  //        case 6:{
  //         this.route.navigate(['/setUpSettings/acdemic/6']);
  //         break;
  //        }
  //        case 7:{
  //         this.route.navigate(['/searchCp/7']);
  //         break;
  //        }
  //        case 10:{
  //         this.route.navigate(['/searchCalendar/10']);
  //         break;
  //        } case 9:{
  //         this.route.navigate(['/exam/searchExam/9'])
  //         break;
  //        }
  //        case 16:{
  //         this.route.navigate(['/attendance/attendanceSetting/16'])
  //         break;
  //        }
  //        case 17:{
  //         this.route.navigate(['/searchDrive/17'])
  //         break;
  //        }
  //        case 8:{
  //         this.route.navigate(['/timetable/searchTimeTable/8'])
  //         break;
  //        }
  //        case 20:{
  //         this.route.navigate(['/students/seachStudent/20/studentList']);
  //         break;
  //        }
  //        case 18:{
  //         this.route.navigate(['/smssetting/sms'])
  //         break;
  //        }


  //      }

  //  }

  onClickMenu(id, index: number) {
    //  alert(id)
    this.selectedIndex = index;
    switch (id) {
      case 3: {
        this.route.navigate(['/hr/2/staff']);
        break;
      }
      case 9: {
        this.route.navigate(['/shifts/shifts/11/workingdays']);
        break;
      }
      case 12: {
        // this.route.navigate(['/announcements/announcements/12']);
        this.route.navigate(['/smssetting/sms']);
        
        break;
      }
      case 13: {
        this.route.navigate(['/approval/approval/13']);
        break;
      }
      case 4: {
        this.route.navigate(['/setUpSettings/acdemic/6/academicYearSetting']);
        break;
      }
      case 5: {
        this.route.navigate(['/searchCp/7/coursePlanner']);
        break;
      }
      case 8: {
        this.route.navigate(['/searchCalendar/10/calendar']);
        break;
      } case 7: {
        this.route.navigate(['/exam/searchExam/9/list'])
        break;
      }
      case 10: {
        this.route.navigate(['/attendance/attendanceSetting/17/view'])
        break;
      }
      case 11: {
        this.route.navigate(['/searchDrive/17/drive'])
        break;
      }
      case 6: {
        this.route.navigate(['/timetable/searchTimeTable/8/timetablelist'])
        break;
      }
      case 2: {
        this.route.navigate(['/students/seachStudent/20/studentList']);
        break;
      }
      case 18: {
        this.route.navigate(['/smssetting/sms'])
        break;
      }
    }
  }

  getEntityListAccess() {
    this.show = true;
    this.loading = true;

    // let userId = sessionStorage.getItem('userId');
    this.accessRightService.getEntityListAccess().subscribe(
      response => {
        if (response) {
          this.entityAccessList = response;
          this.accessRightService.accessRightList = JSON.stringify(this.entityAccessList);
          sessionStorage.setItem('entityAccessRights', JSON.stringify(this.entityAccessList));
          this.show = false;
          this.loading = false;
          this.accessRightService.isLeftLoaded.next(true);

        }
      },
      (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
       //   console.log("Client-side error occured.");
        } else {
          console.info(error);
          this.error = error;
          this.show = false;
          this.loading = false;
        //  this.toastService.ShowError(error);
          this.accessRightService.isLeftLoaded.next(true);
        }
      }
    );
  }


  ngAfterViewInit() {
    this.menu = new MetisMenu(this.sideMenu.nativeElement);

    //this._activateMenuDropdown();
  }

  ngOnChanges() {
    if (!this.isCondensed && this.sideMenu || this.isCondensed && this.isLeftLoaded) {
      setTimeout(() => {
        this.menu = new MetisMenu(this.sideMenu.nativeElement);
      });
    } else if (this.menu) {
      this.menu.dispose();
    }
  }

  /**
   * small sidebar
   */
  smallSidebar() {
    document.body.classList.add('left-side-menu-sm');
    document.body.classList.remove('left-side-menu-dark');
    document.body.classList.remove('topbar-light');
    document.body.classList.remove('boxed-layout');
    document.body.classList.remove('enlarged');
  }

  /**
   * Dark sidebar
   */
  darkSidebar() {
    document.body.classList.remove('left-side-menu-sm');
    document.body.classList.add('left-side-menu-dark');
    document.body.classList.remove('topbar-light');
    document.body.classList.remove('boxed-layout');
  }

  /**
   * Light Topbar
   */
  lightTopbar() {
    document.body.classList.add('topbar-light');
    document.body.classList.remove('left-side-menu-dark');
    document.body.classList.remove('left-side-menu-sm');
    document.body.classList.remove('boxed-layout');

  }

  /**
   * Sidebar collapsed
   */
  sidebarCollapsed() {
    document.body.classList.remove('left-side-menu-dark');
    document.body.classList.remove('left-side-menu-sm');
    document.body.classList.toggle('enlarged');
    document.body.classList.remove('boxed-layout');
    document.body.classList.remove('topbar-light');
  }

  /**
   * Boxed Layout
   */
  boxedLayout() {
    document.body.classList.add('boxed-layout');
    document.body.classList.remove('left-side-menu-dark');
    document.body.classList.add('enlarged');
    document.body.classList.remove('left-side-menu-sm');
  }

  /**
   * Activates the menu dropdown
   */
  _activateMenuDropdown() {
    const links = document.getElementsByClassName('side-nav-link-ref');
    let menuItemEl = null;
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < links.length; i++) {
      // tslint:disable-next-line: no-string-literal
      if (window.location.pathname === links[i]['pathname']) {
        menuItemEl = links[i];
        break;
      }
    }

    if (menuItemEl) {
      menuItemEl.classList.add('active');

      const parentEl = menuItemEl.parentElement;
      if (parentEl) {
        parentEl.classList.add('active');

        const parent2El = parentEl.parentElement;
        if (parent2El) {
          parent2El.classList.add('in');
        }

        const parent3El = parent2El.parentElement;
        if (parent3El) {
          parent3El.classList.add('active');
          parent3El.firstChild.classList.add('active');
        }
      }
    }
  }

  

  selectedMenu(index){
    this.selectedIndex = index;
  }

}
