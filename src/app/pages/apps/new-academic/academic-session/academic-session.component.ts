import { Component, OnInit, SimpleChanges, Input, TemplateRef, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AcademicsessionService } from 'src/app/servicefiles/academicsession.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-academic-session',
  templateUrl: './academic-session.component.html',
  styleUrls: ['./academic-session.component.scss']
})
export class AcademicSessionComponent implements OnInit {
  academicSession=[];
  isShowAcademicSessionForm:boolean=false
  sessionForm:FormGroup;
  academicCalendarForm:FormGroup;
  @Input() currentTab:Number;
  isEdit: boolean=false;
  dataLoading: boolean = false;

  selectSetting = {
    text: "Select",
    position: 'right',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    classes: "myclass custom-class-example",
    badgeShowLimit: 1,
    disabled: false,
};
editSessionData:any;
duplicateRecords = [];
modalReference: NgbModalRef;
newSessionArr=[];
@ViewChild("duplicateModal", { static: true }) modalContent: TemplateRef<any>;
  constructor(private _academicsessionService:AcademicsessionService,
              private formBuilder:FormBuilder,
              private datePipe: DatePipe,
              private toastService: ToastsupportService,
              private modalService: NgbModal,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
    // console.log("test session");

  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("currentValue",changes['currentTab']);
    
    if (changes['currentTab'].currentValue== 2) {
      this.getAcademicSessionList();
    }
  }



  inityearArray(){  
    this.sessionForm = this.formBuilder.group({
            uniqueId: [null, Validators.nullValidator],
            id: [null, Validators.nullValidator],
            name: [null, Validators.required],
            startDate: [new Date(), Validators.required],
            endDate: [new Date(), Validators.nullValidator],
    });
  }

  getAcademicSessionList() {
    // this.dataLoading = true;
    this.spinner.show()
    this._academicsessionService.getAcademicSession().subscribe(
        res => {
          this.spinner.hide()
          console.log("res",res)
          this.dataLoading = false;
            if (res.success == true) {
              console.log('acedemic session =>' + res)
              this.academicSession = res.list;
            }else{
              this.toastService.ShowWarning("Accademic session not fund");
            }
        },
        (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {

            // } else {
            //     // this.dataLoading = false;
            //     this.toastService.ShowInfo(error);
            // }

            this.spinner.hide()
        });
  }

  isAddAcademicSession(){
    this.inityearArray();
      this.isShowAcademicSessionForm = true;
   }


   closeAcademicYearForm() {
    this.sessionForm.reset();
    this.isEdit=false;
   this.isShowAcademicSessionForm = false;
  }


  saveAcademicSession(){
 
    this.newSessionArr.push(this.sessionForm.value);
    console.log("save academic session = >",this.newSessionArr)
    this.spinner.show()
    this._academicsessionService.saveAcademicSession(this.newSessionArr).subscribe(
      res => {
          if (res) {
            console.log("res",res)
            if(res.success == true){
              this.toastService.showSuccess(res.responseMessage)
              this.sessionForm.reset
              this.getAcademicSessionList();
            }else{
              this.toastService.showSuccess(res.responseMessage)
            }
           
          }
          this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //     // this.dataLoading = false;
          //     this.toastService.ShowInfo(error);
          // }

          this.spinner.hide()
      });
  }



  editSession(session){
    this.inityearArray();
    this.editSessionData=session;
    console.log("session",session)
    this.isEdit=true;
    this.sessionForm.patchValue({
      uniqueId: session.uniqueId,
      id: session.id,
      name: session.name,
      startDate: session.startDate,
      endDate: session.endDate,
      endDateToShow: session.endDate,
    });
    this.isShowAcademicSessionForm=true;
  
  }

  closeAcademicSessionForm(){
    this.sessionForm.reset()
    this.isEdit=false;
    this.isShowAcademicSessionForm=false;
  }


  updateAcademicSession(){
    console.log("update academic session = >",this.sessionForm.value)
    this.spinner.show()
    this._academicsessionService.updateAcademicSession(this.sessionForm.value).subscribe(
      res => {
          if (res) {
            console.log("res",res)
            if(res.success == true){
              this.toastService.showSuccess(res.responseMessage)
              this.getAcademicSessionList();
            }else{
              this.toastService.showSuccess(res.responseMessage)
            }
         
          }

          this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //     // this.dataLoading = false;
          //     this.toastService.ShowInfo(error);
          // }
          this.spinner.hide()
      });
  }
  openConfirm(content){
    this.modalService.open(content, { centered: true });
  }

  deleteAcademicSession(){
   console.log('Delete Session' + this.sessionForm.controls['uniqueId'].value)
   this.spinner.show()
    this._academicsessionService.deleteAcademicSession(this.sessionForm.controls['uniqueId'].value).subscribe(
        res => {
            if (res.success == true) {
                // this.dataLoading = false;
                this.isShowAcademicSessionForm=false;
                this.editSessionData='';
               this.getAcademicSessionList();
            }
            this.toastService.showSuccess(res.responseMessage);

            this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            //     // this.dataLoading = false;
            // } else {
            //     // this.dataLoading = false;
            //     this.toastService.ShowError(error);
            // }

            this.spinner.hide()
        });
  }

}
