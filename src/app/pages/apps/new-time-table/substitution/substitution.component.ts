import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HttpErrorResponse } from '@angular/common/http';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import { ClassroomServiceService } from 'src/app/servicefiles/classroom-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { element } from 'protractor';

@Component({
  selector: 'app-substitution',
  templateUrl: './substitution.component.html',
  styleUrls: ['./substitution.component.scss']
})
export class SubstitutionComponent implements OnInit {
  timetabaleList = [{ id: 1, name: "TimeTable1" }, { id: 2, name: "TimeTable2" }];
  selectedTimetable = 1;
  bsValue = new Date();
  typeLsit = [{ id: 1, name: "Date" }, { id: 2, name: "Week" }];
  itemList = [{ id: 0, name: 'Classroom' }, { id: 1, name: 'Batch' }, { id: 2, name: 'Teacher' }];
  filterList = [];
  allShiftList = [];
  selectedShiftValue: any;
  shiftList = [];
  selectedType = 1;
  dateTime: any = new Date();
  timetableName: any;
  starDate: any;
  endDate: any;
  periodData: any = [];
  weekData: any;
  weekDates: any;
  createNewLectureDate: any;
  lectureType: any;
  editLectureData: any;
  dataForShow: any;
  allWeekWiseData: any;
  subjectList = [];
  batchList = [];
  classroomList = [];
  teacherList = [];
  weekLectures = [];
  newweekLectures = [];
  selectedFilterType: any;
  selectedFilterDataValue: any;
  allDayWiseData: any;
  timetableId: any;
  selectedSubjectList: any = [];
  selectedSubjects: any = [];
  selectedShiftUniqueId: any;
  selectedfilterDataUniqueId: any;
  colorClassArray = ['color1', 'color2', 'color3', 'color4'];
  dateObj: any;
  title: any;
  isChecked = false;
  constructor(private spinner: NgxSpinnerService,
    private toastService: ToastsupportService,
    private servece: TimeTableServiceService,
    private classRoomService: ClassroomServiceService,
    private userService: UserServiceService,
    private shiftService: ShiftServiceService,
    private batchesService: BatchesServiceService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.dayWeekChange(this.dateTime, 1)
  }



  changeTimetable(event) {
    this.selectedTimetable = event.id;
  }

  changeType(type, activeId, deactiveId) {
    let ele = document.getElementById(activeId)
    let element = document.getElementById(deactiveId)

    ele.classList.add('dayweekActive')
    element.classList.remove('dayweekActive')
    this.selectedType = type;
    this.dayWeekChange(this.dateTime, type)
  }


  closeForm(event) {
    if (event == 'refreshData') {
      this.dayWeekChange(this.dateTime, this.selectedType)
    }
  }


  dayWeekChange(event, type) {

    if (type == 1) {
      const obj = {
        fromDate: this.datePipe.transform(event, "yyyy-MM-dd"),
      }

      const obj1 = {
        fromDate: this.datePipe.transform(event, "yyyy-MM-dd"),
        toDate: this.datePipe.transform(event, "yyyy-MM-dd")
      }
      this.dateObj = obj1;
      this.getSubstitutionData(obj)
    } else if (type == 2) {
      var date = new Date(event);
      date.setDate(date.getDate() + 6);
      const obj = {
        fromDate: this.datePipe.transform(event, "yyyy-MM-dd"),
        toDate: this.datePipe.transform(date, "yyyy-MM-dd")
      }

      this.dateObj = obj;
      this.getSubstitutionWeekData(obj)
    }
  }

  getSubstitutionData(obj) {
    this.spinner.show()
    this.servece.getSubstitutionDayData(obj).subscribe(
      res => {
        this.periodData = [];
        this.spinner.hide()
        if (res) {
          if (res.success == true) {
            this.allWeekWiseData = res;
            this.timetableName = res.timetableName;
            this.starDate = res.fromDate;
            this.endDate = res.toDate;
            this.timetableId = res.timetableId;
            this.periodData = res.periodWiseLectures;
            if (res.periodWiseLectures != null) {
              let lectures = [];
              let tempTeachers = [];
              let tempSubjects = [];
              let tempBatches = [];
              let tempClassrooms = [];
              let tempShifts = [];

              res.periodWiseLectures.forEach(periodWise => {
                let subjectUid = "";
                periodWise.lectureList.forEach(lecture => {
                  //process for teachers
                  if (lecture.teacherIds != undefined) {
                    lecture.teacherIds.forEach(teacher => {
                      tempTeachers.push(teacher)
                    });
                  }

                  /// process for batches
                  if (lecture.batchIds != undefined) {
                    lecture.batchIds.forEach(batch => {
                      tempBatches.push(batch)
                    });
                  }
                  //process for classrooms
                  tempClassrooms.push({
                    name: lecture.classRoomName,
                    uniqueId: lecture.classRoomId
                  })
                  subjectUid = lecture.subjectId;
                  // process for subjects
                  tempSubjects.push({
                    name: lecture.subjectName,
                    uniqueId: lecture.subjectId
                  })
                });
                periodWise.shiftIds.forEach(shift => {
                  tempShifts.push(shift)
                  lectures.push({
                    uniqueId: periodWise.uniqueId,
                    name: periodWise.name,
                    lectureList: periodWise.lectureList,
                    date: periodWise.lectureDate,
                    shiftName: shift.name,
                    shiftuniqueId: shift.uniqueId,
                    subjectUid: subjectUid,
                    orgWorkDayId: periodWise.orgWorkDayId
                  })
                });

              });
              this.subjectList = tempSubjects.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              let tempSelectedSubjectsList = [];
              let tempSelectedSubjects = [];
              this.subjectList.forEach(subject => {
                tempSelectedSubjectsList.push(subject.uniqueId);
                tempSelectedSubjects.push(subject)
              });
              this.selectedSubjectList = tempSelectedSubjectsList;
              this.selectedSubjects = tempSelectedSubjects;

              this.batchList = tempBatches.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.classroomList = tempClassrooms.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.teacherList = tempTeachers.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.allShiftList = tempShifts.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.weekLectures = lectures;

              this.selectedFilterType = 1;
              this.changeFilterMaster({ id: 1, name: 'batch' });

            }
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }

        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   // this.dataLoading = false;
        //   this.toastService.ShowInfo(error);
        // }

        this.spinner.hide()
      });
  }

  getSubstitutionWeekData(obj) {
    this.spinner.show()
    this.servece.getSubstitutionWeekData(obj).subscribe(
      res => {
        this.weekData = [];
        if (res) {
          this.spinner.hide()
          if (res.success == true) {
            this.allWeekWiseData = res;
            this.timetableName = res.timetableName;
            this.starDate = res.fromDate;
            this.endDate = res.toDate;
            this.timetableId = res.timetableId;
            if (res.dateWiseLectures != null || res.dateWiseLectures.length > 0) { // res.dateWiseLectures.lenght > 0
              let dates = []
              let lectures = [];
              let tempTeachers = [];
              let tempSubjects = [];
              let tempBatches = [];
              let tempClassrooms = [];
              let tempShifts = [];
              res.dateWiseLectures.forEach(element => {
                dates.push(element.lectureDate);
                if (element.periodWiseLectures != null) {
                  element.periodWiseLectures.forEach(periodLecture => {
                    let subjectUid = "";
                    periodLecture.lectureList.forEach(lecture => {
                      //process for teachers
                      lecture.teacherIds.forEach(teacher => {
                        tempTeachers.push(teacher)
                      });
                      /// process for batches
                      lecture.batchIds.forEach(batch => {
                        tempBatches.push(batch)
                      });
                      //process for classrooms
                      tempClassrooms.push({
                        name: lecture.classRoomName,
                        uniqueId: lecture.classRoomId
                      })
                      subjectUid = lecture.subjectId;
                      // process for subjects
                      tempSubjects.push({
                        name: lecture.subjectName,
                        uniqueId: lecture.subjectId
                      })
                    });
                    periodLecture.shiftIds.forEach(shift => {
                      tempShifts.push(shift);
                      lectures.push({
                        lectureDate: element.lectureDate,
                        canAddLecture: element.canAddLecture,
                        uniqueId: periodLecture.uniqueId,
                        name: periodLecture.name,
                        lectureList: periodLecture.lectureList,
                        date: element.lectureDate,
                        shiftName: shift.name,
                        shiftuniqueId: shift.uniqueId,
                        subjectUid: subjectUid,
                        orgWorkDayId: element.orgWorkDayId
                      })
                    });

                  });
                }
              });
              this.subjectList = tempSubjects.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              //pass this subjects to selected subjects
              //for uniqueIds
              let tempSelectedSubjectsList = [];
              // for ui chips show;
              let tempSelectedSubjects = [];
              this.subjectList.forEach(subject => {
                tempSelectedSubjectsList.push(subject.uniqueId);
                tempSelectedSubjects.push(subject)
              });
              this.selectedSubjectList = tempSelectedSubjectsList;
              this.selectedSubjects = tempSelectedSubjects;
              this.batchList = tempBatches.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.classroomList = tempClassrooms.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.teacherList = tempTeachers.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.allShiftList = tempShifts.reduce((unique, o) => {
                if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
                  unique.push(o);
                }
                return unique;
              }, []);
              this.weekLectures = lectures;
              console.log(lectures);
              this.weekDates = dates;

              this.selectedFilterType = 1;
              this.changeFilterMaster({ id: 1, name: 'batch' });

            } else {
              this.toastService.ShowWarning('No time timabe available')
            }

          } else {
            this.toastService.ShowWarning('No time timabe available')
          }


        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   // this.dataLoading = false;
        //   this.toastService.ShowError(error);
        // }

        this.spinner.hide()
      });
  }

  changeFilterMaster(evt) {
    this.selectedFilterDataValue = null;
    this.shiftList = [];
    switch (evt.id) {
      case 0:

        if (this.classroomList.length > 0) {
          // this is for classroom list
          this.filterList = this.classroomList
          //set filter data first position
          this.selectedFilterDataValue = this.classroomList[0].uniqueId
          //get shift by first data element
          this.filterData({ uniqueId: this.classroomList[0].uniqueId, name: this.classroomList[0].name })
        } else {
          this.toastService.ShowWarning('No classrooms found')
        }

        break;
      case 1:

        if (this.batchList.length > 0) {
          // this is for batch list
          this.filterList = this.batchList
          //set filter data first position
          this.selectedFilterDataValue = this.batchList[0].uniqueId
          //get shift by first data element
          this.filterData({ uniqueId: this.batchList[0].uniqueId, name: this.batchList[0].name })
        } else {
          this.toastService.ShowWarning('No classrooms found')
        }
        break;

      case 2:

        if (this.teacherList.length > 0) {
          // this is for Teacher list
          this.filterList = this.teacherList;
          // set filter data first position
          this.selectedFilterDataValue = this.teacherList[0].uniqueId
          // get shift by first data element
          this.filterData({ uniqueId: this.teacherList[0].uniqueId, name: this.teacherList[0].name })
        } else {
          this.toastService.ShowWarning('No classrooms found')
        }
        break;
    }
  }

  filterData(evt) {
    this.title = evt.name
    this.selectedfilterDataUniqueId = evt.uniqueId;
    switch (this.selectedFilterType) {
      case 0:
        // this is for classroom list
        this.getClassroomShifts(evt.uniqueId, this.timetableId);
        break;
      case 1:
        // this is for batch list
        this.getBatchShifts(evt.uniqueId);

        break;

      case 2:
        // this is for Teacher list
        this.getTeacherShifts(evt.uniqueId);
        break;
    }

  }
  ShowAllData(evt) {
    this.selectedFilterType = 4;
    if (this.isChecked) {
      var groupBy = function (xs, key) {
        return xs.reduce(function (rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
      };
      switch (this.selectedType) {
        case 1:
          this.periodData = groupBy(this.weekLectures, 'name');
          break;
        case 2:
          this.weekData = groupBy(this.weekLectures, 'name');
          break
      }

    } else {
      this.selectedFilterType = 1
      this.shiftChange({ uniqueId: this.selectedShiftUniqueId, name: '' })
    }

  }
  shiftChange(evt) {

    let newProcessedData = [];
    var groupBy = function (xs, key) {
      return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    };
    this.weekData = null;
    this.selectedShiftUniqueId = evt.uniqueId;
    //filter subjects
    let processJSON = JSON.stringify(this.weekLectures)
    let processForEmptyJSON = JSON.stringify(this.weekLectures)
    let processJSON2 = []
    JSON.parse(processJSON).forEach(lecture => {
      if (this.selectedSubjectList.includes(lecture.subjectUid)) {
        processJSON2.push(lecture)
      }
    });
    //set filter on shift group by
    let groupbyshift = groupBy(processJSON2, 'shiftuniqueId');
    let shiftData = [];
    shiftData = groupbyshift[evt.uniqueId];
    switch (this.selectedFilterType) {
      case 0:
        if (shiftData != undefined) {
          shiftData.forEach(sData => {
            let dto = {
              name: sData.name,
              shiftName: sData.shiftName,
              shiftuniqueId: sData.shiftuniqueId,
              subjectUid: sData.subjectUid,
              uniqueId: sData.uniqueId,
              lectureDate: sData.date,
              canAddLecture: sData.canAddLecture,
              orgWorkDayId: sData.orgWorkDayId,
              lectureList: [],
            }
            sData.lectureList.forEach(lecture => {
              if (lecture.classRoomId == this.selectedfilterDataUniqueId) {
                dto.lectureList.push(lecture)
              }
            })
            newProcessedData.push(dto)
          });
        } else {
          this.toastService.ShowWarning('No data found for this shift')
        }
        break;
      case 1:
        if (shiftData != undefined) {
          shiftData.forEach(sData => {
            let dto = {
              name: sData.name,
              shiftName: sData.shiftName,
              shiftuniqueId: sData.shiftuniqueId,
              subjectUid: sData.subjectUid,
              uniqueId: sData.uniqueId,
              lectureDate: sData.date,
              canAddLecture: sData.canAddLecture,
              orgWorkDayId: sData.orgWorkDayId,
              lectureList: [],
            }
            sData.lectureList.forEach(lecture => {
              lecture.batchIds.forEach(batch => {
                if (batch.uniqueId == this.selectedfilterDataUniqueId) {
                  dto.lectureList.push(lecture)
                }
              });
            })
            newProcessedData.push(dto)
          });
        } else {
          this.toastService.ShowWarning('No data found for this shift')
        }
        break;

      case 2:
        if (shiftData != undefined) {
          shiftData.forEach(sData => {
            let dto = {
              name: sData.name,
              shiftName: sData.shiftName,
              shiftuniqueId: sData.shiftuniqueId,
              subjectUid: sData.subjectUid,
              uniqueId: sData.uniqueId,
              lectureDate: sData.date,
              canAddLecture: sData.canAddLecture,
              orgWorkDayId: sData.orgWorkDayId,
              lectureList: [],
            }
            sData.lectureList.forEach(lecture => {
              lecture.teacherIds.forEach(teacher => {
                if (teacher.uniqueId == this.selectedfilterDataUniqueId) {
                  dto.lectureList.push(lecture)
                }
              });
            })
            newProcessedData.push(dto)
          });
        } else {
          this.toastService.ShowWarning('No data found for this shift')
        }
        break;
    }

    switch (this.selectedType) {
      case 1:
        this.periodData = groupBy(newProcessedData, 'name');
        break;
      case 2:
        let tempEmpty = [];
        tempEmpty = JSON.parse(processForEmptyJSON);
        tempEmpty.forEach(element => {
          element.lectureList = [];
        });
        if (newProcessedData != undefined && newProcessedData.length > 0) {
          for (let i = 0; i < newProcessedData.length; i++) {
            for (let j = 0; j < tempEmpty.length; j++) {
              if (tempEmpty[j].uniqueId === newProcessedData[i].uniqueId && tempEmpty[j].date === newProcessedData[i].lectureDate) {
                tempEmpty[j].lectureList = newProcessedData[i].lectureList;
              }
            }
          }
        }
        this.weekData = groupBy(tempEmpty, 'name');
        break

    }

  }
  getTeacherShifts(uniqueId: any) {
    this.spinner.show();
    this.shiftService.getShiftByTeacher(uniqueId).subscribe(
      res => {
        this.shiftList = []
        if (res) {
          //intersection of two arrays get similar data
          this.shiftList = this.allShiftList.filter(item1 => res.some(item2 => item1.uniqeId === item2.uniqeId));
          this.selectedShiftValue = this.shiftList[0].uniqueId
          this.shiftChange({ uniqueId: this.shiftList[0].uniqueId })
        } else {
          this.toastService.ShowWarning('No shifts found for this teacher')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }

  getBatchShifts(uniqueId) {
    console.log('get batch shift called 3')
    this.spinner.show();
    this.shiftService.getShiftByBatch(uniqueId).subscribe(
      res => {

        this.shiftList = []
        if (res.success == true) {
          //intersection of two arrays get similar data
          this.shiftList = this.allShiftList.filter(item1 => res.list.some(item2 => item1.uniqeId === item2.uniqeId));
          this.selectedShiftValue = this.shiftList ? this.shiftList[0].uniqueId :
            console.log('')
          this.shiftChange({ uniqueId: this.shiftList[0].uniqueId, name: '' })

        } else {
          this.toastService.ShowWarning('No shifts found for this batch')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }

  getClassroomShifts(classroomUniqueId, timetableId) {

    this.spinner.show();
    this.shiftService.getShiftByClassroomAndTimeTable(classroomUniqueId, timetableId).subscribe(
      res => {
        this.shiftList = []
        if (res.success == true) {
          //intersection of two arrays get similar data
          this.shiftList = this.allShiftList.filter(item1 => res.list.some(item2 => item1.uniqeId === item2.uniqeId));
          this.selectedShiftValue = this.shiftList[0].uniqueId
          this.shiftChange({ uniqueId: this.shiftList[0].uniqueId })
        } else {
          this.toastService.ShowWarning('No shifts found for this batch')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  removeSubject(item, index) {
    if (this.isChecked) {
      this.toastService.ShowWarning('Please uncheck show all')
    } else {

      this.selectedSubjects.splice(index, 1)
      this.selectedSubjectList = this.selectedSubjectList.filter(t => t !== item.uniqueId);
      this.shiftChange({ uniqueId: this.selectedShiftUniqueId });
    }
  }
  changeSubjects(event) {
    this.selectedSubjects = event;
    this.shiftChange({ uniqueId: this.selectedShiftUniqueId })
  }
}
