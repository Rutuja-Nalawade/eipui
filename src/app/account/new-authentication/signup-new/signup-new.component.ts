import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';

@Component({
  selector: 'app-signup-new',
  templateUrl: './signup-new.component.html',
  styleUrls: ['./signup-new.component.scss']
})
export class SignupNewComponent implements OnInit {

  signupForm: FormGroup;
  submitted = false;
  error: any;
  success = '';
  loading = false;

  constructor(private _authService: AuthServiceService, private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z \-\']+'), Validators.minLength(2)])],
      lastName: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z \-\']+'), Validators.minLength(2)])],
      emailId: ['', [Validators.required, Validators.email]],
    });
  }

  get f() { return this.signupForm.controls; }


  onSubmit() {
    this.submitted = true;
    this.error = ''

    if (this.signupForm.invalid) {
      return;
    }


    this.loading = true;
    this._authService.signUpNew(this.signupForm.value).subscribe(
      (res) => {
        if (res) {
          let response = res;
          this.success = "Registration successfully done!"
          setTimeout(() => {
            this.router.navigate(['/account/mobile-verify/'], { state: this.signupForm.value });
          }, 1000);

        } else {
          this.error = "Email id already exists."
        }
        this.loading = false;
      },
      (err: HttpErrorResponse) => {
        this.signupForm.controls['emailId'].setErrors({ 'exist': true });
        if (err instanceof HttpErrorResponse) {
          // console.log("Client-side error occured.");
          this.error = err
        } else {
          this.error = err
          // this.toastService.ShowError(error);
          //this.sessionService.checkUnauthorizedRequest(error.error);
        }
        this.loading = false;
      });
  }

}
