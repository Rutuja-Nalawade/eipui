import { Component, Input, OnInit } from '@angular/core';
import { ApexYAxis } from 'ng-apexcharts';

@Component({
  selector: 'app-exam-summary',
  templateUrl: './exam-summary.component.html',
  styleUrls: ['./exam-summary.component.scss']
})
export class ExamSummaryComponent implements OnInit {
  @Input() examPattern:any;
  @Input() patternType:any;
  subjectName:String=null;
  duration:any;
  totQuestions:any;
  totMarks:any;
  pattern1:boolean=true;
  pattern2:boolean=false;
  patterns1: any;
  patterns2: any;

  constructor() {
    
   }

  ngOnInit() {
    console.log("examPattern",this.examPattern);
    console.log("examPatternType",this.patternType);
   
    if(this.patternType == '4'){
      this.duration=this.examPattern.examPatterns[0].duration;
      this.totMarks=this.examPattern.examPatterns[0].examMarks;
      this.totQuestions=this.examPattern.examPatterns[0].totalQuestion;
      this.examPattern.examPatterns.forEach(element => {
        element.subjects.forEach(ele => {
          if(this.subjectName != null){
            this.subjectName=this.subjectName +','+ ele.name
          }else{
            this.subjectName= ele.name
          }
          
        });
      });
    }
    

    if(this.patternType == '3' && this.examPattern.isPaperTwo == false){
      console.log("false")
      // this.patterns=this.examPattern.examPatterns[0]
      this.patterns1=this.examPattern.examPatterns[0];
      this.patterns2=this.examPattern.examPatterns[0];
      
    }

    if(this.patternType == '3' && this.examPattern.isPaperTwo == true){
      console.log("true")
      // this.patterns=this.examPattern.examPatterns[0]
      this.patterns1=this.examPattern.examPatterns[0]
      this.patterns2=this.examPattern.examPatterns[1]
      
    }

    console.log("patterns1",this.patterns1)
    console.log("patterns1",this.patterns2)
  }

  paperClick(activeid,inactive){
  
   
    let ele = document.getElementById(activeid);
    let element = document.getElementById(inactive)
    console.log("activeid",activeid,"ele",ele)
    console.log("inactive",inactive,"element",element)
    ele.classList.add('active')
    element.classList.remove('active')
    
    if(activeid == 'Paper1'){
      // this.patterns1=this.examPattern.examPatterns[0]
      this.pattern2=false
      this.pattern1=true
      this.subjectName=null
    
     }else if(activeid == 'Paper2'){
      // this.patterns2=this.examPattern.examPatterns[1]
      this.pattern1=false
      this.pattern2=true
     
     }
  }

}
