import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-teacher-subjects',
  templateUrl: './teacher-subjects.component.html',
  styleUrls: ['./teacher-subjects.component.scss']
})
export class TeacherSubjectsComponent implements OnInit {

  teacherSubjectForm: FormGroup;
  currentUserId: any;
  userData: any;
  subjectData: any;
  subjectList = [];
  checkboxList = [];
  subjectFilterList = [];
  tecaherSubjectData: any;
  submitted = false;
  isSelectAll = false;
  selectedCount = 0;
  alreadySelectedSubjectList = [];
  textSearch = ""
  @Input() userId: any;
  @Input() userUniqueId: any;
  @Input() currentTab: Number;
  // @Output() teacherListReload = new EventEmitter<boolean>();


  constructor(private formBuilder: FormBuilder, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private _userService: UserServiceService, private _subjectService: SubjectServiceService) {
    this.userData = this._commonService.getUserData();
    this.currentUserId = this.userId;
  }

  ngOnInit() {
    this.currentUserId = this.userId;
    this.initTeacherSubjectInfoForm();
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (changes['currentTab'] && changes['currentTab'].currentValue == 3) {
      await this.getSubject();

    }
  }
  initTeacherSubjectInfoForm() {
    this.teacherSubjectForm = this.formBuilder.group({
      userUniqueId: ['', Validators.nullValidator],
      createdOrUpdatedByUniqueId: this.userData.uniqueId,
      subjectList: this.formBuilder.array([]),
      userId: [this.currentUserId],
      organizationId: [this.userData.organizationId]
    })
  }
  getSubject() {
    return this._subjectService.getSubjectList().subscribe(
      res => {
        if (res.success == true) {
          this.subjectData = res.list;
          // console.log(res);
          this.subjectList = this.subjectData;
          if (this.subjectData != null && this.subjectData.length > 0) {
            this.subjectFilterList = this.subjectData;
          }
        } else {
          this.toastService.ShowWarning('No Subjects found')
        }
        this.getUserSubjects(this.userUniqueId)
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.getUserSubjects(this.userUniqueId)
      }
    );
  }
  getUserSubjects(teacherSubjectId) {
    // console.log(this.teacherSubjectForm.controls['userUniqueId'].value)
    if (teacherSubjectId) {
      this.getTeacherSubjects(teacherSubjectId);
    }
    else {
      this.getTeacherSubjects(this.teacherSubjectForm.controls['userUniqueId'].value);
    }
    // this.getTeacherSubjects(this.currentTeacherId);
  }

  getTeacherSubjects(userId: string) {

    this.spinner.show();
    this._userService.getTeacherSubjects(userId).subscribe(
      response => {
        this.tecaherSubjectData = response;
        if (this.tecaherSubjectData != null && this.tecaherSubjectData.subjects != null) {
          console.log("subjectFilterList", this.subjectFilterList);
          this.patchSubjectsInfo(this.tecaherSubjectData);
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      }
    );
  }

  patchSubjectsInfo(subjectInfo: any) {

    this.initTeacherSubjectInfoForm();
    // this.teacherSubjectForm.controls['userUniqueId'].setValue(subjectInfo.userUniqueId);
    // this.teacherSubjectForm.controls['createdOrUpdatedByUniqueId'].setValue(sessionStorage.getItem('uniqueId'));

    let subjectArray = <FormArray>this.teacherSubjectForm.controls['subjectList'];
    let subjectList = subjectInfo.subjects;
    let selectedSubjectIds = []
    for (let i = 0; i < subjectList.length; i++) {
      selectedSubjectIds.push(subjectList[i].uniqueId);
    }
    for (let i = 0; i < this.subjectFilterList.length; i++) {
      let subject = this.formBuilder.group({
        uniqueId: this.subjectFilterList[i].uniqueId,
        isLinked: (selectedSubjectIds.includes(this.subjectFilterList[i].uniqueId) ? true : false),
        name: this.subjectFilterList[i].name,
        isSearched: true
      });
      subjectArray.push(subject);
    }
    this.selectedCount = subjectInfo.subjects.length;

    this.teacherSubjectForm.setControl('subjectList', subjectArray);
  }
  submitTeacherSubjectInfo() {

    this.submitted = true;

    if (this.teacherSubjectForm.invalid) {
      this.scrollToError();
      return;
    }

    let subjectList = this.teacherSubjectForm.controls['subjectList'].value;

    subjectList = subjectList.filter(x => x.isLinked == true);

    if (subjectList == null || subjectList.length == 0) {
      this.toastService.ShowWarning("Please assign atleast one subject");
      // alert("Please assign atleast one subject")
      return;
    }

    this.spinner.show();
    let subjectUid = [];
    for (let i = 0; i < subjectList.length; i++) {
      const element = subjectList[i];
      subjectUid.push(element.uniqueId);
    }
    this._userService.assignTeacherSubjects(subjectUid, this.userUniqueId)
      .subscribe(data => {
        this.toastService.showSuccess("Subject saved successfully.");
        this.spinner.hide();
        // this.searchTeachers(false);
        // this.teacherListReload.emit(true)
        this._commonService.iscloseClicked.next(true)
      },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   this.spinner.hide();
          // } else {
          //   this.toastService.ShowError(error);
          //   this.spinner.hide();
          // }
          this.spinner.hide();
        }
      );
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    // console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }
  get subjectListArrayControl() {
    return (this.teacherSubjectForm.get('subjectList') as FormArray).controls;
  }
  selectAllSubject(event) {
    let subjectList = this.teacherSubjectForm.controls['subjectList'].value;
    if (event.target.checked) {
      let count = 0
      for (var i = 0; i < subjectList.length; i++) {
        if (this.textSearch.length > 0) {
          if (subjectList[i].isSearched) {
            subjectList[i].isLinked = true;
            count = count + 1
          }
        } else {
          count = count + 1
          subjectList[i].isLinked = true;
        }
      }
      this.selectedCount = count;
    } else {
      let count = 0
      for (var i = 0; i < subjectList.length; i++) {
        if (this.textSearch.length > 0) {
          if (subjectList[i].isSearched) {
            subjectList[i].isLinked = false;
            count = count + 1
          }
        } else {
          count = count;
          subjectList[i].isLinked = false;
        }
      }
      this.selectedCount = count;
    }
    this.isSelectAll = !this.isSelectAll
  }
  searchSubject(event) {
    let subjectList = this.teacherSubjectForm.controls['subjectList'].value;
    let value = event.target.value.toLowerCase();
    this.textSearch = value;
    if (value.length > 0) {
      let count = 0
      for (var i = 0; i < subjectList.length; i++) {
        let subjectName = subjectList[i].name.toLowerCase()
        if (subjectName.includes(value)) {
          subjectList[i].isSearched = true;
          if (subjectList[i].isLinked) {
            count = count + 1
          }
        } else {
          subjectList[i].isSearched = false;
        }
      }
      this.selectedCount = count;
    } else {
      let count = 0
      for (var i = 0; i < subjectList.length; i++) {
        subjectList[i].isSearched = true;
        if (subjectList[i].isLinked) {
          count = count + 1
        }
      }
      this.selectedCount = count;
    }
  }
  subjectChange(subject) {
    if (!subject.value.isLinked) {
      subject.value.isLinked = true;
      this.selectedCount = this.selectedCount + 1;
    } else {
      subject.value.isLinked = false;
      this.selectedCount = this.selectedCount - 1;
      this.isSelectAll = false
    }
  }
  close() {
    this._commonService.iscloseClicked.next(true)
  }
}
