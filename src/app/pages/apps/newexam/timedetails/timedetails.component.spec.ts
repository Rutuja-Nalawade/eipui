import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimedetailsComponent } from './timedetails.component';

describe('TimedetailsComponent', () => {
  let component: TimedetailsComponent;
  let fixture: ComponentFixture<TimedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
