import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { ClassroomServiceService } from 'src/app/servicefiles/classroom-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { PeriodServiceService } from 'src/app/servicefiles/period-service.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import * as moment from 'moment';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { DatePipe } from '@angular/common';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';

@Component({
  selector: 'app-rightside-forms',
  templateUrl: './rightside-forms.component.html',
  styleUrls: ['./rightside-forms.component.scss']
})
export class RightsideFormsComponent implements OnInit {
  isFromSetup: boolean = false;
  submitted = false;
  isConflicts = false;

  assignLectureForm: FormGroup;
  batchGroupList = [];
  classroomList = [];
  teacherList = [];

  batchPeriodForm: FormGroup;
  batchList = [];
  daysList = [{ day: "Monday", dayValue: 2, id: 2, isSelected: false }, { day: "Tuesday", dayValue: 3, id: 3, isSelected: false }, { day: "Wednesday", dayValue: 4, id: 4, isSelected: false }, { day: "Thursday", dayValue: 5, id: 5, isSelected: false }, { day: "Friday", dayValue: 6, id: 6, isSelected: false }, { day: "Saturday", dayValue: 7, id: 7, isSelected: false }, { day: "Sunday", dayValue: 1, id: 1, isSelected: false }];
  periodList = [];

  subjectTeacherForm: FormGroup;
  shiftLsit = [];
  changeDetailsForm: FormGroup;
  subjectList = [];
  courseList = [];
  isViewConflict = false;
  viewConflictType = 3;
  viewConflictObject = { typeName: "Classroom" };
  addSubjectForm: FormGroup;
  addBatchForm: FormGroup;
  addClassroomForm: FormGroup;
  classType = [{ id: 1, name: 'Room' }, { id: 2, name: 'Library' }, { id: 3, name: 'Laboratory' }, { id: 4, name: 'Computer Lab' },
  { id: 5, name: 'Hall' }, { id: 6, name: 'Conference Hall' }, { id: 7, name: 'Staff Room' }]
  classCategory = [{ id: 1, name: 'Physical' }, { id: 2, name: 'Virtual' }, { id: 3, name: 'Hybrid' }]
  addBatchgroupForm: FormGroup;
  batchCategoryList = [];
  addBatchCategoryForm: FormGroup;
  isEditAdd: boolean = false;
  courseAndRefForm: FormGroup;
  shiftNewForm: FormGroup;
  editData: any;
  selectedIndex = 1;
  isAddNew = false;
  conflictData: any = {};
  selectedClassroomId = "";
  selectedSubjectName = "";
  isRemoeLecture = false;
  isSelectAll = false;
  selectedCount = 0;
  textSearch = "";
  startTime = "";
  endTime = "";
  isDaysSelected = false;
  selectedTeacher: any = [];
  selectedWorkingDays = [];
  workingDayList = [];
  shiftTypeList = [];
  bsInlineValue: any;
  bsDate: any;
  showDatePicker: boolean = false;
  yearsData: any = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  daysData: any = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];
  monthData: any = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
  isOutSideDatepickerClick = false;
  masterCourseList: any = [];
  gradeList: any = [];
  boardList: any = [];
  masterSubjectList = [];
  subjectList1 = [];
  alreadySelectedSubjectList = [];
  selectedBatchgroupList = [];
  @Input() type: any;
  @Input() timetableUniqueId: any;
  @Input() batchData: any;
  @Input() dayUniqueId: any;
  @Input() lectureId: any;
  @Input() periodData: any;
  @Input() lectureData: any;
  @Input() childtype: any;
  @Output() close = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder, private toastService: ToastsupportService, private timeTableService: TimeTableServiceService, private classRoomService: ClassroomServiceService, private spinner: NgxSpinnerService, private batchService: BatchesServiceService, private subjectService: SubjectServiceService, private periodService: PeriodServiceService, private shiftService: ShiftServiceService, private courseService: CourseServiceService, private userService: UserServiceService, private datePipe: DatePipe, private gradeService: GradeServiceService, private boardService: BoardServiceService, private classroomService: ClassroomServiceService) {
  }

  ngOnInit() {
  }
  async ngOnChanges(changes: SimpleChanges) {
    if (changes['type'] && changes['type'].currentValue == 2) {
      this.initAssignLectureForm();
      this.getSubjectList();
      this.getClassRoomist();
      this.getBatchGroupList();
    } else if (changes['type'] && changes['type'].currentValue == 3) {
      this.initBatchPeriodForm();
    } else if (changes['type'] && changes['type'].currentValue == 4) {
      this.lectureId = changes['lectureId'].currentValue;
      console.log(changes);
      console.log(this.lectureId);
      this.initSubjectLectureForm();
      this.getSubjectList();
      this.getClassRoomist();
      this.getBatchGroupList();
      this.getOrgPeriodList();
      this.getshiftworkdayList();
      if (this.childtype == 4) {
        this.periodData = this.lectureData;
        this.patchLecture(this.lectureData)
      } else {
        this.getLectureData();
      }
    } else if (changes['type'] && changes['type'].currentValue == 5) {
      console.log(changes);
      this.initChageDetailsForm();
      this.getCourseList();
      this.getClassRoomist();
    } else if (changes['type'] && changes['type'].currentValue == 6) {
      this.initAddSubjectForm();
      this.getAllSubjects();
    } else if (changes['type'] && changes['type'].currentValue == 7) {
      this.initAddBatchForm();
      this.getCourseListFromMasterOrg();
    } else if (changes['type'] && changes['type'].currentValue == 8) {
      this.initAddClassroomForm();
    } else if (changes['type'] && changes['type'].currentValue == 9) {
      this.initAddBatchGroupForm();
    } else if (changes['type'] && changes['type'].currentValue == 10) {
      this.initAddBatchCategoryForm();
    } else if (changes['type'] && changes['type'].currentValue == 11) {
      this.initCourseAndRefForm();
      await this.getMasterCourseList();
      this.getBoardList();
      this.getGradeList();
    } else if (changes['type'] && changes['type'].currentValue == 12) {
      this.initShiftForm();
      this.getshiftworkdayList();
      this.getshiftTypeList();
    }
  }
  initAssignLectureForm() {
    this.assignLectureForm = this.formBuilder.group({
      subject: [null, Validators.required],
      batchgroup: [null, Validators.required],
      classRoomId: [null, Validators.required],
      teacherIds: [null, Validators.required],
      batchId: [this.batchData.uniqueId, Validators.required],
      timetableId: [this.timetableUniqueId, Validators.required],
      isReplace: [false, Validators.nullValidator],
      teacherConflict: [false, Validators.nullValidator],
      classroomConflict: [false, Validators.nullValidator]
    });
  }
  get f() { return this.assignLectureForm.controls; }
  changeTeacher(event) {
    console.log(event);
    let teacherIds = [];
    this.conflictData = {};
    if (event && event.length > 0) {
      for (let i = 0; i < event.length; i++) {
        teacherIds.push(event[i].uniqueId);
      }
      let dto = {
        period: this.periodData.periodId,
        day: this.periodData.day,
        timetableId: this.timetableUniqueId,
        teacherIds: teacherIds
      };
      this.checkConflict(dto);
    } else {
      this.assignLectureForm.controls.teacherConflict.setValue(false);
      this.isConflicts = false;
      this.assignLectureForm.controls.classroomConflict.setValue(false);
    }
  }
  checkConflict(dto) {
    this.timeTableService.getConflicts(dto).subscribe(
      res => {
        if (res) {
          if (res.success) {
            this.conflictData = res;
            this.assignLectureForm.controls.teacherConflict.setValue(true);
            this.isConflicts = true;
            if (this.selectedClassroomId) {
              this.assignLectureForm.controls.classroomConflict.setValue(false);
              if (this.conflictData && this.conflictData.success) {
                if (this.conflictData.conflictClassRoomIds.includes(this.selectedClassroomId)) {
                  this.assignLectureForm.controls.classroomConflict.setValue(true);
                }
              }
            }
          } else {
            this.assignLectureForm.controls.teacherConflict.setValue(false);
            this.isConflicts = false;
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  changeDate(event) {
    if (this.assignLectureForm.value.teacherIds) {
      let dto = {
        period: this.assignLectureForm.value.periodId,
        day: event.uniqueId,
        timetableId: this.timetableUniqueId,
        teacherIds: this.assignLectureForm.value.teacherIds
      };
      this.checkConflict(dto);
    }
  }
  changePeriod(event) {
    if (this.assignLectureForm.value.teacherIds) {
      let dto = {
        period: event.uniqueId,
        day: this.assignLectureForm.value.day,
        timetableId: this.timetableUniqueId,
        teacherIds: this.assignLectureForm.value.teacherIds
      };
      this.checkConflict(dto);
    }
  }
  getshiftworkdayList() {
    this.spinner.show();
    return this.shiftService.getShiftWorkingDays().subscribe(
      res => {
        this.daysList = [];
        if (res) {
          let temp = res;
          let temp2 = [];
          if (res.length > 0) {
            this.selectedWorkingDays = temp2;
            this.workingDayList = temp.sort(function (a, b) {
              return a.dayValue - b.dayValue;
            })
            let workingDayList = res;
            for (let i = 0; i < workingDayList.length; i++) {
              const element = workingDayList[i];
              switch (element.dayValue) {
                case 1:
                  element.name = "Sunday";
                  break;
                case 2:
                  element.name = "Monday";
                  break;
                case 3:
                  element.name = "TuesDay";
                  break;
                case 4:
                  element.name = "Wednesday";
                  break;
                case 5:
                  element.name = "Thursday";
                  break;
                case 6:
                  element.name = "Friday";
                  break;
                case 7:
                  element.name = "Saturday";
                  break;
              }
              temp2.push(element);
            }
            this.daysList = temp2;
          } else {
            this.toastService.ShowError("Days not found")
          }
          this.spinner.hide();

        } else {
          this.spinner.hide();
          this.toastService.ShowError("Days not found")
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  changeClassRoom(event) {
    this.selectedClassroomId = ""
    this.assignLectureForm.controls.classroomConflict.setValue(false)
    this.selectedClassroomId = event.uniqueId
    if (this.conflictData && this.conflictData.success) {
      if (this.conflictData.conflictClassRoomIds.includes(event.uniqueId)) {
        this.assignLectureForm.controls.classroomConflict.setValue(true);
      }
    }
  }
  initBatchPeriodForm() {
    this.batchPeriodForm = this.formBuilder.group({
      batch: [null, Validators.required],
      dayPeriodList: this.formBuilder.array([this.initDayPerioList()]),
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      uniqueId: [null, Validators.nullValidator],
      createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.nullValidator]
    });
  }
  initDayPerioList() {
    let dto = this.formBuilder.group({
      day: [null, Validators.required],
      period: [null, Validators.required]
    });
    return dto;
  }
  initSubjectLectureForm() {
    let formGroup = {
      subjectId: [null, Validators.required],
      groupIds: [null, Validators.required],
      batch: [(this.batchData && this.batchData.uniqueId) ? this.batchData.uniqueId : this.lectureData.batchIds[0].uniqueId, Validators.required],
      classRoomId: [null, Validators.required],
      teacherIds: [null, Validators.required],
      periodId: [(this.periodData && this.periodData.periodId) ? this.periodData.periodId : this.lectureData.periodId, Validators.required],
      timetableId: [this.timetableUniqueId, Validators.required],
      day: [(this.periodData && this.periodData.day) ? this.periodData.day : this.lectureData.orgDayUniqueId, Validators.required],
      uniqueId: [null, Validators.nullValidator],
      teacherConflict: [false, Validators.nullValidator],
      classroomConflict: [false, Validators.nullValidator]
    }
    console.log(formGroup);
    if (this.childtype == 3) {
      formGroup['shift'] = [null, Validators.required];
    }
    this.assignLectureForm = this.formBuilder.group(formGroup);
  }
  get st() { return this.assignLectureForm.controls; }
  getOrgPeriodList() {
    return this.periodService.getAllPeriod().subscribe(
      res => {
        if (res) {
          let slotList = res;
          if (slotList != null && slotList.length > 0) {
            let slotdtolist = [];
            for (let i = 0; i < slotList.length; i++) {
              slotdtolist = slotdtolist.length > 0 ? slotdtolist.concat(slotList[i].periods) : slotList[i].periods;
            }
            this.periodList = slotdtolist;
          } else {
            this.toastService.ShowError("Periods not found");
          }
        } else {
          this.toastService.ShowError("Periods not found");
        }

      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  initChageDetailsForm() {
    this.assignLectureForm = this.formBuilder.group({
      subject: [null, Validators.required],
      batchgroup: [null, Validators.required],
      batch: [null, Validators.required],
      classRoomId: [null, Validators.required],
      teacherIds: [null, Validators.required],
      period: [this.periodData.periodId, Validators.required],
      course: [null, Validators.required],
      uniqueId: [null, Validators.nullValidator],
      teacherConflict: [false, Validators.nullValidator],
      classroomConflict: [false, Validators.nullValidator]
    });
  }
  get cd() { return this.assignLectureForm.controls; }
  initAddSubjectForm() {
    this.addSubjectForm = this.formBuilder.group({
      subjectListDto: this.formBuilder.array([this.initSubjctDto()])
    });
  }
  initAddBatchForm() {
    this.addBatchForm = this.formBuilder.group({
      cateoryDtoList: this.formBuilder.array([this.initCategorytDto()]),
      uniqueId: [null, Validators.nullValidator],
      courseId: [null, Validators.required],
      courseName: [null, Validators.nullValidator],
      name: [null, Validators.required],
      isNew: [true, Validators.nullValidator],
      editMode: [true, Validators.required]
    });
  }
  initCategorytDto() {
    let dto = this.formBuilder.group({
      batchGroupDtoList: this.formBuilder.array([this.initBatchGrouptDto()]),
      name: [null, Validators.required]
    });
    return dto;
  }
  initBatchGrouptDto() {
    let dto = this.formBuilder.group({
      name: [null, Validators.required]
    });
    return dto;
  }
  initAddClassroomForm() {
    this.addClassroomForm = this.formBuilder.group({
      category: [null, Validators.required],
      name: [null, Validators.required],
      type: [null, Validators.required],
      room_url: [null, Validators.nullValidator],
      isShowUrl: [false, Validators.nullValidator]
    });
  }
  initAddBatchGroupForm() {
    this.addBatchgroupForm = this.formBuilder.group({
      batchgroupListDto: this.formBuilder.array([this.initBatchgroupDto()]),
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      uniqueId: [null, Validators.nullValidator],
      createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.nullValidator]
    });
  }
  initAddBatchCategoryForm() {
    this.addBatchCategoryForm = this.formBuilder.group({
      batchCategoryListDto: this.formBuilder.array([this.initBatchCategorytDto()]),
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      uniqueId: [null, Validators.nullValidator],
      createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.nullValidator]
    });
  }
  addMore(type) {
    switch (type) {
      case 1:
        let dayPeriodList = <FormArray>this.batchPeriodForm.controls['dayPeriodList'];
        dayPeriodList.push(this.initDayPerioList());
        break;
      case 2:
        let subjectList = <FormArray>this.addSubjectForm.controls['subjectListDto'];
        subjectList.push(this.initSubjctDto());
        break;
      case 3:
        let batchList = <FormArray>this.addBatchForm.controls['cateoryDtoList'];
        batchList.push(this.initCategorytDto());
        break;
      case 4:
        let classroomList = <FormArray>this.addClassroomForm.controls['classroomListDto'];
        classroomList.push(this.initClassroomDto());
        break;
      case 5:
        let batchgroupList = <FormArray>this.addBatchgroupForm.controls['batchgroupListDto'];
        batchgroupList.push(this.initBatchgroupDto());
        break;
      case 6:
        let batchCategoryListDto = <FormArray>this.addBatchCategoryForm.controls['batchCategoryListDto'];
        batchCategoryListDto.push(this.initBatchCategorytDto());
        break;

    }
  }
  addMoreGroup(index) {
    let categoryList = <FormArray>this.addBatchForm.controls['cateoryDtoList'];
    let categoryGroup = <FormGroup>categoryList.controls[index];
    let batchGroupList = <FormArray>categoryGroup.controls['batchGroupDtoList'];
    batchGroupList.push(this.initBatchGrouptDto());
  }
  removeBatchGroup(parentIndex, index) {
    let categoryList = <FormArray>this.addBatchForm.controls['cateoryDtoList'];
    let categoryGroup = <FormGroup>categoryList.controls[parentIndex];
    let batchGroupList = <FormArray>categoryGroup.controls['batchGroupDtoList'];
    batchGroupList.removeAt(index);
  }
  initSubjctDto() {
    let dto = this.formBuilder.group({
      id: [null, Validators.nullValidator],
      uniqueId: [null, Validators.nullValidator],
      name: [null, Validators.required],
      isLinked: [false, Validators.required],
    });
    return dto;
  }
  initClassroomDto() {
    let dto = this.formBuilder.group({
      id: [null, Validators.nullValidator],
      uniqueId: [null, Validators.nullValidator],
      typeName: [null, Validators.nullValidator],
      type: [null, Validators.required],
      category: [null, Validators.required],
      categoryName: [null, Validators.nullValidator],
      url: [null, Validators.nullValidator],
      name: [null, Validators.required],
      isNew: [true, Validators.nullValidator],
      editMode: [true, Validators.required],
    });
    return dto;
  }
  initBatchgroupDto() {
    let dto = this.formBuilder.group({
      id: [null, Validators.nullValidator],
      uniqueId: [null, Validators.nullValidator],
      courseName: [null, Validators.nullValidator],
      courseId: [null, Validators.required],
      batch: [null, Validators.required],
      batchId: [null, Validators.nullValidator],
      categoryId: [null, Validators.required],
      categoryName: [null, Validators.nullValidator],
      name: [null, Validators.required],
      isNew: [true, Validators.nullValidator],
      editMode: [true, Validators.required],
    });
    return dto;
  }
  initBatchCategorytDto() {
    let dto = this.formBuilder.group({
      id: [null, Validators.nullValidator],
      uniqueId: [null, Validators.nullValidator],
      category: [null, Validators.required],
      isNew: [true, Validators.nullValidator],
      editMode: [true, Validators.required],
    });
    return dto;
  }
  cancel() {
    this.close.emit("2");
    this.submitted = false;
  }
  removeLecture() {
    this.isRemoeLecture = true
  }
  removeAction(type) {
    if (type == 1) {
      this.isRemoeLecture = false;
    } else {
      console.log(this.lectureId);
      this.spinner.show();
      return this.timeTableService.removeLecture(this.lectureId).subscribe(
        res => {
          if (res && res.success) {
            this.toastService.showSuccess("Lecture remove succefully");
            this.isRemoeLecture = false;
            this.close.emit("3");
          } else {
            this.toastService.ShowError(res.responseMessage);
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }
  }
  save(type) {
    this.submitted = true;
    if (this.assignLectureForm.invalid) {
      return;
    }
    this.spinner.show();
    let data = this.assignLectureForm.value;
    let teacherIds = []
    let groupIds = [];
    for (let i = 0; i < data.teacherIds.length; i++) {
      const element = data.teacherIds[i];
      teacherIds.push({ uniqueId: element })
    }
    for (let i = 0; i < data.batchgroup.length; i++) {
      const element = data.batchgroup[i];
      groupIds.push({ uniqueId: element })
    }
    let dto = {
      batchId: data.batchId,
      timetableId: data.timetableId,
      lectureDto: {
        subjectId: data.subject,
        groupIds: groupIds,
        teacherIds: teacherIds
      },
      cardDto: {
        periodUniqueId: this.periodData.periodId,
        classRoomId: data.classRoomId,
        days: this.periodData.day
      },
      isReplace: (type == 3) ? true : false
    }
    if (type == 3) {
      let replaceLessons = []
      for (let i = 0; i < this.conflictData.conflicts.length; i++) {
        const element = this.conflictData.conflicts[i];
        if (element.isBusy) {
          for (let j = 0; j < element.lectureList.length; j++) {
            const element2 = element.lectureList[j];
            replaceLessons.push(element2.uniqueId)
          }
        }
      }
      dto['replaceLessons'] = replaceLessons
    }
    this.timeTableService.assignLecture(dto).subscribe(
      (res) => {
        if (res && res.success) {
          this.toastService.showSuccess("Assign lecture added succesfully");

          let lectureDto = {
            subjectName: this.selectedSubjectName, lectureId: res.lectureId
          }
          this.spinner.hide();
          this.close.emit(JSON.stringify(lectureDto))
        } else {
          this.spinner.hide();
          this.toastService.showSuccess(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  saveBatchPeriod() {
    this.submitted = true;
    if (this.batchPeriodForm.invalid) {
      return;
    }
  }
  saveSubjectTeacherForm(type) {
    this.submitted = true;
    if (this.assignLectureForm.invalid) {
      return;
    }
    this.spinner.show();
    let data = this.assignLectureForm.value;
    let teacherIds = []
    let groupIds = [];
    for (let i = 0; i < data.teacherIds.length; i++) {
      const element = data.teacherIds[i];
      teacherIds.push({ uniqueId: element })
    }
    for (let i = 0; i < data.groupIds.length; i++) {
      const element = data.groupIds[i];
      groupIds.push({ uniqueId: element })
    }
    let dto = {
      batchId: data.batch,
      timetableId: data.timetableId,
      lectureDto: {
        subjectId: data.subjectId,
        uniqueId: data.uniqueId,
        groupIds: groupIds,
        teacherIds: teacherIds
      },
      cardDto: {
        periodUniqueId: data.periodId,
        classRoomId: data.classRoomId,
        days: data.day
      },
      isReplace: (type == 3) ? true : false
    }
    if (type == 3) {
      let replaceLessons = []
      for (let i = 0; i < this.conflictData.conflicts.length; i++) {
        const element = this.conflictData.conflicts[i];
        if (element.isBusy) {
          for (let j = 0; j < element.lectureList.length; j++) {
            const element2 = element.lectureList[j];
            replaceLessons.push(element2.uniqueId)
          }
        }
      }
      dto['replaceLessons'] = replaceLessons
    }
    console.log(dto);
    this.timeTableService.updateLecture(dto).subscribe(
      (res) => {
        if (res && res.success) {
          this.toastService.showSuccess("Lecture update succesfully");

          this.spinner.hide();
          this.close.emit("4")
        } else {
          this.submitted = false;
          this.spinner.hide();
          this.toastService.showSuccess(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.submitted = false;
        this.spinner.hide();
      });
  }
  saveChangeDetailsForm(type) {
    this.submitted = true;
    console.log(this.assignLectureForm);
    if (this.assignLectureForm.invalid) {
      return;
    }
    let data = this.assignLectureForm.value;
    let teacherIds = [];
    let groupIds = [];
    for (let i = 0; i < data.teacherIds.length; i++) {
      const element = data.teacherIds[i];
      teacherIds.push({ uniqueId: element })
    }
    for (let i = 0; i < data.batchgroup.length; i++) {
      const element = data.batchgroup[i];
      groupIds.push({ uniqueId: element })
    }
    let dto = {
      batchId: data.batch,
      timetableId: this.timetableUniqueId,
      lectureDto: {
        subjectId: data.subject,
        groupIds: groupIds,
        teacherIds: teacherIds
      },
      cardDto: {
        periodUniqueId: this.periodData.periodId,
        classRoomId: data.classRoomId,
        days: this.periodData.day
      },
      isReplace: (type == 3) ? true : false
    }
    if (type == 3) {
      let replaceLessons = []
      for (let i = 0; i < this.conflictData.conflicts.length; i++) {
        const element = this.conflictData.conflicts[i];
        if (element.isBusy) {
          for (let j = 0; j < element.lectureList.length; j++) {
            const element2 = element.lectureList[j];
            replaceLessons.push(element2.uniqueId)
          }
        }
      }
      dto['replaceLessons'] = replaceLessons
    }
    console.log(dto);
    this.timeTableService.assignLecture(dto).subscribe(
      (res) => {
        if (res && res.success) {
          this.toastService.showSuccess("Lecture added succesfully"); ``
          this.close.emit("")
        } else {
          this.spinner.hide();
          this.toastService.showSuccess(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  viewConflict(conflictType) {
    let conflictData = []
    this.viewConflictObject['conflictType'] = conflictType;
    if (conflictType == 1) {
      this.viewConflictObject.typeName = "Teacher";
      conflictData = this.conflictData.conflicts;
      this.viewConflictObject['conflictData'] = conflictData;
    } else {
      for (let i = 0; i < this.conflictData.conflicts.length; i++) {
        const element = this.conflictData.conflicts[i];
        if (element.isBusy) {
          let lectures = [];
          let details = element;
          for (let j = 0; j < element.lectureList.length; j++) {
            const element2 = element.lectureList[j];
            if (element2.classRoomId == this.selectedClassroomId) {
              lectures.push(element2)
            }
          }
          details.lectureList = lectures;
          conflictData.push(details);
        }
      }
      this.viewConflictObject.typeName = "Classroom";
      this.viewConflictObject['conflictData'] = conflictData;
    }
    switch (this.type) {
      case 2:
        this.viewConflictType = 3;
        this.viewConflictObject['conflictData'] = conflictData;
        break;
      case 4:
        this.viewConflictType = 3;
        break;
    }
    this.isViewConflict = true;
  }
  closeConflict(event) {
    this.isViewConflict = false;
  }
  deleteRow(item, index) {

  }
  editRow(item, index) {
    item.controls.editMode.setValue(true);
  }
  updateRow(item, index) {
    this.submitted = true;
    if (item.invalid) {
      return;
    }
    item.controls.editMode.setValue(false);
    this.submitted = false;
  }
  saveForm() {
    this.submitted = true;
    this.spinner.show();
    console.log("masterSubjectList", this.masterSubjectList);
    let selectedSubjectList = []
    let selectedSubjectsCount = this.masterSubjectList.filter(x => x.isSelected)
    if (selectedSubjectsCount == null || selectedSubjectsCount.length == 0) {
      this.toastService.ShowWarning("Please select atleast one subject");
      // alert("Please Select atleast one subject")
      this.spinner.hide();
      return;
    }
    this.masterSubjectList.map(x => {
      if (x.isSelected == true) {
        selectedSubjectList.push({
          name: x.name,
          universal_id: x.uniqueId,
          isSelected: x.isSelected
        })
      }
    });
    this.subjectService.updateMasterSubjects(selectedSubjectList).subscribe(
      response => {
        if (response) {
          let subjectList = response.subjectList;
          console.log(subjectList);
          this.spinner.hide();
          this.cancel();
        }
        this.spinner.hide();
        // this.patchSubjectList(this.subjectList);
        // this.toastService.showSuccess("Subjects added successfully");
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );
  }
  changeSelect(event, itemgroup, type) {
    switch (type) {
      case 1:
        let typeName = (event) ? event.name : "";
        itemgroup.controls.typeName.setValue(typeName);
        break;
      case 2:
        let categoryName = (event) ? event.name : "";
        itemgroup.controls.categoryName.setValue(categoryName);

        break;
    }
  }
  initCourseAndRefForm() {
    this.courseAndRefForm = this.formBuilder.group({
      uniqueId: ['', Validators.nullValidator],
      id: [0, Validators.required],
      name: ['', Validators.required],
      isNew: [true, Validators.required],
      year: [null, Validators.required],
      month: [null, Validators.required],
      day: [null, Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      endDateValue: ['', Validators.required],
      masterCourseUniqueId: [null, Validators.required],
      academicSessionDto: ['', Validators.nullValidator],
      boardAndGradeReferenceDtoList: this.formBuilder.array([this.initBoardGradeList()], Validators.required),
      alreadySelected: [[]],
      organizationId: ['', Validators.nullValidator],
      masterCourseDtoList: ['', Validators.nullValidator],
      duration: null,
      editMode: true,
      isShowDatePicker: false
    });
  }
  initBoardGradeList() {
    return this.formBuilder.group({
      uniqueId: ['', Validators.nullValidator],
      boardId: ['', Validators.nullValidator],
      gradeId: ['', Validators.nullValidator],
      boardUniqueId: [null, Validators.required],
      gradeUniqueId: [null, Validators.required],
      boardName: ['', Validators.nullValidator],
      gradeName: ['', Validators.nullValidator],
      courseUniqueId: [null, Validators.nullValidator],
      courseName: [null, Validators.nullValidator],
      boardGradeHasCourseId: ['', Validators.nullValidator]
    });
  }
  get course() { return this.courseAndRefForm.controls; }
  initShiftForm() {
    this.shiftNewForm = this.formBuilder.group({
      shiftName: ['', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      type: [null, Validators.required],
      isStartTimeGreater: [false, Validators.nullValidator],
      shiftbreaksList: this.formBuilder.array(this.addBreaksUnderShift()),
      workingDaysList: this.formBuilder.array([]),
    });
  }
  addBreaksUnderShift() {
    let breaksArray = [];
    let breakData = this.formBuilder.group({
      id: ['', Validators.nullValidator],
      breakUid: ['', Validators.nullValidator],
      name: ['', Validators.nullValidator],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      isStartTimeGreater: [false, Validators.nullValidator],
      min: [this.startTime],
      max: [this.endTime]
    });
    breaksArray.push(breakData);
    return breaksArray;
  }
  get batch() { return this.addBatchForm.controls; }
  get shift() { return this.shiftNewForm.controls; }
  getSubjectList() {
    this.spinner.show();
    return this.subjectService.getSubjectByBatchUid(this.batchData.uniqueId).subscribe(
      res => {
        if (res.success == true) {
          this.subjectList = res.list;
        } else {
          this.subjectList = [];
          this.toastService.ShowError("Subject not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  getLectureData() {
    this.spinner.show();
    return this.timeTableService.getLectureData(this.lectureId).subscribe(
      res => {
        if (res) {
          this.lectureData = res;
          this.patchLecture(res);
          console.log(this.assignLectureForm);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }
  patchLecture(data) {
    let teacherIds: any = [];
    for (let i = 0; i < data.teacherIds.length; i++) {
      const element = data.teacherIds[i];
      teacherIds.push(element.uniqueId)
    }
    this.selectedSubjectName = data.subjectName;
    this.selectedClassroomId = data.classRoomId;
    let groupIds = [];
    if (data.groupIds && data.groupIds.length > 0) {
      data.groupIds.forEach(x => {
        groupIds.push(x.uniqueId)
      });
    }
    this.assignLectureForm.controls.uniqueId.setValue(data.uniqueId);
    this.assignLectureForm.controls.subjectId.setValue(data.subjectId);
    this.assignLectureForm.controls.periodId.setValue(data.periodId);
    this.assignLectureForm.controls.classRoomId.setValue(data.classRoomId);
    this.assignLectureForm.controls.groupIds.setValue(groupIds);
    this.assignLectureForm.controls.teacherIds.setValue(teacherIds);
    this.changeSubject({ name: data.subjectName, uniqueId: data.subjectId }, 2);
  }
  getClassRoomist() {
    this.spinner.show();
    return this.classRoomService.getAcademicClassRoom().subscribe(
      res => {
        if (res.success == true) {
          this.classroomList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  getCourseList() {
    this.spinner.show();
    return this.courseService.getCourseBasicInfoList(3).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            // console.log("get courseList => ",res)
            this.courseList = res.list;
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
        } else {
          this.toastService.ShowError("Course list not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  changeCourse(event) {
    console.log(event);
    this.getBatchbyCourse(event.uniqueId);
  }
  changeBatch(event) {
    console.log(event);
    this.batchData = { uniqueId: event.uniqueId }
    this.getBatchGroupList();
    this.getSubjectList();
  }
  getBatchbyCourse(uid) {
    this.spinner.show();
    return this.batchService.getBatchByCourse(uid).subscribe(
      res => {
        if (res.success == true) {
          this.batchList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  getBatchGroupList() {
    this.spinner.show();
    this.batchGroupList = [];
    let batchIds = [this.batchData.uniqueId]
    return this.batchService.getCtegoryGroupByBatchUid(batchIds).subscribe(
      res => {
        if (res && res.success) {
          let batchGroups = res.batchCategoryWiseGroups[0];
          let categoryList = batchGroups.batchGroupList;
          let batchGroupList = [];
          for (let i = 0; i < categoryList.length; i++) {
            const element = categoryList[i];
            if (element.batchGroups) {
              for (let j = 0; j < element.batchGroups.length; j++) {
                const element2 = element.batchGroups[j];
                element2['categoryName'] = element.categoryName;
                element2['categoryUniqueId'] = element.categoryUniqueId;
                batchGroupList.push(element2);
                // batchGroupList.push(element2);
              }
            }
          }

          this.batchGroupList = batchGroupList;
        } else {
          this.toastService.ShowError("Batch group not foundq")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  onItemSelect(event, item) {
    console.log(event, item);
  }
  changeBatchGroup(event) {
    if (event.length == 0) {
      if (this.selectedBatchgroupList.length == 0) {
        this.selectedBatchgroupList = event;
      } else {
        let batchgrouplist = [];
        this.selectedBatchgroupList.forEach(x => {
          event.forEach(element => {
            if (element.uniqueId == x.uniqueId) {
              batchgrouplist.push(element);
            }
          });
        });
        this.selectedBatchgroupList = batchgrouplist;
      }
    } else {
      this.selectedBatchgroupList = event;
    }
  }
  changeSubject(event, type) {
    this.selectedSubjectName = event.name;
    this.spinner.show();
    let dto = {
      shiftId: this.periodData.shiftId,
      periodId: this.periodData.periodId,
      orgDayId: (this.childtype != 4) ? this.periodData.day : this.periodData.orgDayUniqueId,
      subjectId: event.uniqueId
    }
    this.timeTableService.getTeacherListBySubjectDto(dto).subscribe(
      res => {
        if (res && res.success) {
          if (type == 1) {
            this.assignLectureForm.controls.teacherIds.setValue(null);
          }
          if (type != 3) {
            this.assignLectureForm.controls.teacherConflict.setValue(false);
            this.assignLectureForm.controls.classroomConflict.setValue(false);
          }
          this.teacherList = res.list;
        } else {
          this.toastService.ShowError(res.responseMessage);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }
  get subjectListArrayControl() {
    return (this.addSubjectForm.get('subjectListDto') as FormArray).controls;
  }
  subjectChange(subject) {
    if (!subject.isSelected) {
      subject.isSelected = true;
      this.selectedCount = this.selectedCount + 1;
    } else {
      subject.isSelected = false;
      this.selectedCount = this.selectedCount - 1;
    }
    if (this.alreadySelectedSubjectList.length > 0 && this.alreadySelectedSubjectList.includes(subject.universal_id)) {
      subject.isNew = false;
      console.log("alreadySelected");
    } else {
      console.log("Not alreadySelected");
      subject.isNew = true;
    }
  }
  selectAllSubject(event) {
    console.log("selectAll", event.target.checked);
    if (event.target.checked) {
      for (var i = 0; i < this.masterSubjectList.length; i++) {
        this.masterSubjectList[i].isSelected = true;
        if (this.alreadySelectedSubjectList.includes(this.masterSubjectList[i].universal_id)) {
          this.masterSubjectList[i].isNew = false;
        } else {
          this.masterSubjectList[i].isNew = true;
        }
      }
      this.selectedCount = this.masterSubjectList.length;
    } else {
      for (var i = 0; i < this.masterSubjectList.length; i++) {
        this.masterSubjectList[i].isSelected = false;
      }
      this.selectedCount = 0;
    }
    this.isSelectAll = !this.isSelectAll
    console.log("selectAll", this.masterSubjectList);
  }
  searchSubject(event) {
    console.log(event, event.target.value);
    let value = event.target.value.toLowerCase();
    this.textSearch = value;
    if (value.length > 0) {
      let count = 0
      for (var i = 0; i < this.masterSubjectList.length; i++) {
        let subjectName = this.masterSubjectList[i].name.toLowerCase()
        if (subjectName.includes(value)) {
          this.masterSubjectList[i].isSearched = true;
          if (this.masterSubjectList[i].isSelected) {
            count = count + 1
          }
        } else {
          this.masterSubjectList[i].isSearched = false;
        }
      }
      this.selectedCount = count;
    } else {
      let count = 0
      for (var i = 0; i < this.masterSubjectList.length; i++) {
        this.masterSubjectList[i].isSearched = true;
        if (this.masterSubjectList[i].isSelected) {
          count = count + 1
        }
      }
      this.selectedCount = count;
    }
  }
  customSearchFn(term: string, item: any) {
    term = term.toLocaleLowerCase();
    return item.countryName.toLocaleLowerCase().indexOf(term) > -1;
  }
  onStartDate(startTime: any, isForUpdate: number) {

    console.log('start time: ', this.convertTime(startTime))
    this.startTime = this.convertTime(startTime);
    this.endTime = this.shiftNewForm.controls['endTime'].value;
    let breaklist = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    for (let index = 0; index < breaklist.value.length; index++) {
      let breakData = <FormGroup>breaklist.controls[index];
      breakData.controls['min'].setValue(this.startTime);
      breakData.controls['max'].setValue(this.endTime);
    }
    if (this.startTime < this.endTime) {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(false);
    } else {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(true);
    }
  }

  onEndDate(endtime: any, isForUpdate: number) {
    console.log('end time: ', this.convertTime(endtime))
    this.endTime = this.convertTime(endtime)
    this.startTime = this.shiftNewForm.controls['startTime'].value;
    let breaklist = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    for (let index = 0; index < breaklist.value.length; index++) {
      let breakData = <FormGroup>breaklist.controls[index];
      breakData.controls['min'].setValue(this.startTime);
      breakData.controls['max'].setValue(this.endTime);
    }
    if (this.endTime > this.startTime) {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(true);
    } else {
      this.shiftNewForm.controls['isStartTimeGreater'].setValue(false);
    }
  }
  onBreakStartDate(breakStartTime: any, index: number) {
    console.log('break start time' + this.convertTime(breakStartTime))
    let startTime = this.convertTime(breakStartTime);
    let shift = <FormGroup>this.shiftNewForm;
    let breakList = <FormArray>shift.controls['shiftbreaksList'];
    let shiftBreak = <FormGroup>breakList.controls[index];
    let endTime = this.convertTime(shiftBreak.controls['endTime'].value);
    if (startTime < endTime) {
      console.log('start time is less 1')
      shiftBreak.controls['isStartTimeGreater'].setValue(false);
    } else {
      console.log('start time is greater 1')
      shiftBreak.controls['isStartTimeGreater'].setValue(true);
    }
  }

  onBreakEndDate(breakEndTime: any, index: number) {
    console.log('break end time' + this.convertTime(breakEndTime))
    let endTime = this.convertTime(breakEndTime);
    let shift = <FormGroup>this.shiftNewForm;
    let breakList = <FormArray>shift.controls['shiftbreaksList'];
    let shiftBreak = <FormGroup>breakList.controls[index];
    let startTime = this.convertTime(shiftBreak.controls['startTime'].value)
    console.log('on end start time' + startTime)
    if (startTime < endTime) {
      console.log('start time is less 2')
      shiftBreak.controls['isStartTimeGreater'].setValue(false);
    } else {
      console.log('start time is greater 2')
      shiftBreak.controls['isStartTimeGreater'].setValue(true);
    }
  }

  convertTime(time) {
    return moment(time, 'hh:mm A').format('HH:mm');
  }
  addBreak() {
    let breakArray = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    let breakData = this.formBuilder.group({
      id: ['', Validators.nullValidator],
      name: ['', Validators.nullValidator],
      breakUid: ['', Validators.nullValidator],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required],
      isStartTimeGreater: [false, Validators.nullValidator],
      min: [this.startTime],
      max: [this.endTime]
    });
    breakArray.push(breakData);
  }
  change(daysList) {
    if (daysList.length > 0) {
      this.isDaysSelected = true;
    } else {
      this.isDaysSelected = false;
    }
    for (let i = 0; i < this.workingDayList.length; i++) {
      const element = this.workingDayList[i];
      if (daysList.length > 0) {
        for (let j = 0; j < daysList.length; j++) {
          if (daysList[j].dayValue == element.dayValue) {
            element.isLinked = true;
            break;
          } else {
            element.isLinked = false;
          }
        }
      } else {
        element.isLinked = false;
      }
    }
  }
  getshiftTypeList() {
    return this.shiftService.getAllShiftTypes().subscribe(
      res => {
        if (res) {
          res.forEach(element => {
            let dto = { id: element.key, name: element.value }
          });
          this.shiftTypeList = res;
          // console.log('shift type => ' + JSON.stringify(res))
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        // }
      });
  }
  getTeachers(value) {
    this.selectedTeacher = [];
    let pageIndex = 0;
    this.userService.getUserByPageIndexAndType(pageIndex, value.id, 100).subscribe(
      response => {
        // console.log('user list =>', JSON.stringify(response))
        if (response.message == "Success") {
          this.teacherList = response.users
        } else {
          this.toastService.ShowInfo("Users not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  removeBreak(index) {
    let breakList = <FormArray>this.shiftNewForm.controls['shiftbreaksList'];
    breakList.removeAt(index);
  }
  saveShift() {
    this.submitted = true;
    if (this.shiftNewForm.invalid) {
      return;
    }
    if (!this.isDaysSelected) {
      return;
    }

    let formValue = this.shiftNewForm.value;
    this.spinner.show();

    let processData = formValue;
    // console.log('submit data' + JSON.stringify(submitDataDto))

    let breaks = []
    processData.shiftbreaksList.forEach(elem => {
      let dto = {
        name: elem.name,
        startTime: this.convertTime(elem.startTime),
        endTime: this.convertTime(elem.endTime),
      }
      breaks.push(dto)
    });

    let submitDataDto = {
      name: processData.shiftName,
      type: processData.type,
      startTime: this.convertTime(processData.startTime),
      endTime: this.convertTime(processData.endTime),
      unique_id: "",
      days: this.selectedWorkingDays,
      breaks: breaks,
      userIds: this.selectedTeacher,
    }

    console.log('submit data', submitDataDto);
    this.shiftService.addShiftWorkingDays(submitDataDto).subscribe(
      (res) => {
        if (res) {
          this.toastService.showSuccess("Shift Added Succesfully");
          this.shiftNewForm.reset()
          this.spinner.hide();
          // this._commonService.workTimeProcess.next(1);
          this.cancel();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  // on start date change call get calenderlist
  onDateChange(startDate: Date) {

    let date: Date = new Date(this.datePipe.transform(startDate, 'yyyy-MM-dd'));
    let fullYear: number = date.getFullYear();
    let fullMonth: number = date.getMonth();
    let fullDate: number = date.getDate();

    let courseGroup = <FormGroup>this.courseAndRefForm;
    let yearValue = (courseGroup.controls['year'].value) ? courseGroup.controls['year'].value : 0;
    let yearCal = fullYear + parseInt(yearValue);

    let monthValue = (courseGroup.controls['month'].value) ? courseGroup.controls['month'].value : 0;
    let monthCal = fullMonth + parseInt(monthValue);

    let daysValue = (courseGroup.controls['day'].value) ? courseGroup.controls['day'].value : 0;
    let daysCal = fullDate + parseInt(daysValue);

    let endDate = new Date(yearCal, monthCal, daysCal);
    let endDateCon = new Date(this.datePipe.transform(endDate, 'yyyy-MM-dd'));
    courseGroup.controls['endDate'].setValue(this.datePipe.transform(endDateCon, "yyyy-MM-dd'T'HH:mm:ss.000Z"));
    courseGroup.controls['endDateValue'].setValue(this.datePipe.transform(endDateCon, 'yyyy-MM-dd'));

  }
  // datepicker open on start date
  datePickerOpen() {
    console.log("tets");
    if (this.courseAndRefForm.controls.startDate.value != null) {
      this.bsInlineValue = new Date(this.courseAndRefForm.controls.startDate.value);
      this.bsDate = new Date(this.courseAndRefForm.controls.startDate.value);
      this.courseAndRefForm.controls.isShowDatePicker.setValue(true);
    } else {
      this.bsInlineValue = new Date().toLocaleDateString();
      this.bsDate = new Date().toLocaleDateString();
      this.courseAndRefForm.controls.isShowDatePicker.setValue(true);
    }
    console.log(this.courseAndRefForm);
  }
  //delete board and grade row
  deleteBoardGrade(childIndex, data) {
    this.spinner.show();
    let formGroup = <FormGroup>this.courseAndRefForm;
    let array = <FormArray>formGroup.controls['boardAndGradeReferenceDtoList'];
    if (data.boardGradeHasCourseId != null && data.boardGradeHasCourseId != '') {
      array.removeAt(childIndex);
      this.spinner.hide();
    } else {
      array.removeAt(childIndex);
      this.spinner.hide();
    }

  }
  // Add board and frade row
  addBoardGradeRow() {
    let courseGroup = <FormGroup>this.courseAndRefForm;
    let boardGradeList = <FormArray>courseGroup.controls['boardAndGradeReferenceDtoList'];
    boardGradeList.push(this.initBoardGradeList())
  }
  // start date change
  dateChange(event) {
    let course = <FormGroup>this.courseAndRefForm;
    if (event != 'Invalid Date') {
      let eventDate = this.datePipe.transform(event, 'yyyy-MM-dd');

      course.controls['startDate'].setValue(this.datePipe.transform(event, 'yyyy-MM-dd'))
      // this.courseAndRefForm.controls.startDate.value=this.datePipe.transform(event, 'yyyy-MM-dd');
      if (this.bsDate != 'Invalid Date') {
        let bsInlineValueDate = this.datePipe.transform(this.bsDate, 'yyyy-MM-dd');
        if (eventDate != bsInlineValueDate) {
          this.courseAndRefForm.controls.isShowDatePicker.setValue(false);
        }
      } else {
        this.courseAndRefForm.controls.isShowDatePicker.setValue(false);
      }
      this.bsDate = new Date(eventDate);
    }
  }
  //Change duration value
  onDurationChange(event) {
    let courseGroup = <FormGroup>this.courseAndRefForm;
    let date: Date = new Date(courseGroup.controls['startDate'].value);
    if (date && courseGroup.controls['startDate'].value) {
      let fullYear: number = date.getFullYear();
      let fullMonth: number = date.getMonth();
      let fullDate: number = date.getDate();
      let yearValue = (courseGroup.controls['year'].value) ? courseGroup.controls['year'].value : 0;
      let yearCal = fullYear + parseInt(yearValue);

      let monthValue = (courseGroup.controls['month'].value) ? courseGroup.controls['month'].value : 0;
      let monthCal = fullMonth + parseInt(monthValue);

      let daysValue = (courseGroup.controls['day'].value) ? courseGroup.controls['day'].value : 0;
      let daysCal = fullDate + parseInt(daysValue);
      // console.log('courseGroup', courseGroup);

      let endDate = new Date(yearCal, monthCal, daysCal);
      let endDateCon = new Date(this.datePipe.transform(endDate, 'yyyy-MM-dd'));
      courseGroup.controls['endDate'].setValue(this.datePipe.transform(endDateCon, "yyyy-MM-dd'T'HH:mm:ss.000Z"));
      courseGroup.controls['endDateValue'].setValue(this.datePipe.transform(endDateCon, 'yyyy-MM-dd'));
    }
  }
  // datepicker popup close on outside click
  onClickedOutside(event) {
    if (event.target.id == 'datepickerInput' && !this.isOutSideDatepickerClick) {
      this.isOutSideDatepickerClick = true;
      this.courseAndRefForm.controls['isShowDatePicker'].setValue(true);
      console.log("object");
      console.log("object", this.courseAndRefForm);
    } else if (event.target.id != 'datepickerInput' && this.isOutSideDatepickerClick && !event.target.classList.contains('ng-option')) {
      console.log("object1");
      console.log("object1", this.courseAndRefForm);
      this.isOutSideDatepickerClick = false;
      this.courseAndRefForm.controls['isShowDatePicker'].setValue(false)
    }
  }
  getBoardList() {
    this.spinner.show();
    this.boardList = [];
    return this.boardService.getAcademicBoardsList().subscribe(res => {
      if (res) {
        this.boardList = res;
      }
      this.spinner.hide();
    },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }

  getGradeList() {
    this.spinner.show();
    this.boardList = [];
    return this.gradeService.getAcademicGradeList().subscribe(res => {
      if (res.success == true) {
        this.gradeList = res.list;
      }else{
        this.toastService.ShowWarning(res.responseMessage)
      }
      this.spinner.hide();
    },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }
  //get master course list
  getMasterCourseList() {
    this.spinner.show();
    this.courseService.getMasterCourseList().subscribe(res => {
      if (res) {
        this.masterCourseList = res;
        this.spinner.hide();
      }
    },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }
  saveCourse() {
    this.submitted = true;
    console.log(this.courseAndRefForm);
    if (this.courseAndRefForm.invalid) {
      return true;
    }
    let dataarray = [];
    let data = this.courseAndRefForm.value;
    console.log("courseAndRefForm", data);
    let mastercourse = [{
      uniqueId: data.masterCourseUniqueId
    }]
    let academicSessionDto = {
      uniqueId: data.academicSessionUniqueId
    }
    let boardGradeList = []
    data.boardAndGradeReferenceDtoList.forEach(element => {
      let dtograde = {
        uniqueId: element.gradeUniqueId
      }
      let dtoboard = {
        uniqueId: element.boardUniqueId
      }
      this.spinner.show();
      let dto = {
        boardDto: dtoboard,
        gradeDto: dtograde
      }
      boardGradeList.push(dto)
    });
    let dto = {
      name: data.name,
      startDate: data.startDate,
      endDate: data.endDate,
      masterCourseDtoList: mastercourse,
      academicSessionDto: academicSessionDto,
      boardGradeList: boardGradeList,
    }
    dataarray.push(dto);
    this.courseService.addCourse(dataarray).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.toastService.showSuccess("Course saved successfully..")
        this.cancel();
      }
      this.spinner.hide();
    },
      error => {
        this.spinner.hide();
      //  this.toastService.ShowError(error);
      })
  }
  getSubjectListFromMasterOrg() {
    this.spinner.show();
    this.subjectService.getMasterSubjects().subscribe(
      res => {
        if (res) {
          this.masterSubjectList = res;
          for (let i = 0; i < this.masterSubjectList.length; i++) {
            this.masterSubjectList[i]['isSearched'] = true;
          }

          console.log("MASTERsubjectList", this.masterSubjectList);
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
      });
  }
  getAllSubjects() {
    this.spinner.show();
    this.subjectService.getSubjectList().subscribe(
      response => {
        this.spinner.hide();
        if (response.success == true) {
          this.subjectList1 = response.list;
          this.selectedCount = response.length;
        } else {
          this.toastService.ShowWarning('No subjects found')
        }
        // this.patchSubjectList(this.subjectList);

        // if (this.subjectList1 && this.subjectList1.length > 0) {
        //   this.selectedCount = this.subjectList1.length;
        //   this.subjectList1.map(subject => {
        //     this.alreadySelectedSubjectList.push(subject.universal_id)
        //   })
        // }
        this.getSubjectListFromMasterOrg();
        // this.childOrganizationList.push(orgDto);
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );
  }
  getCourseListFromMasterOrg() {
    this.spinner.show();
    this.courseService.getCourseBasicInfoList(1).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            // console.log("get courseList => ",res)
            this.courseList = res.list;
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }
  saveBatch() {
    this.submitted = true;
    if (this.addBatchForm.invalid) {
      return;
    }
    let dto = this.addBatchForm.value;
    console.log(dto); return
    this.spinner.show();
    this.courseService.addCourse(dto).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.toastService.showSuccess("Batch saved successfully..")
        this.cancel();
      }
      this.spinner.hide();
    },
      error => {
        this.spinner.hide();
      //  this.toastService.ShowError(error);
      })
  }
  saveClassRoom() {
    let classromArray = [];
    classromArray.push(this.addClassroomForm.value)
    this.classroomService.saveAcademicClassRoom(classromArray).subscribe(
      res => {
        this.toastService.ShowInfo(res.responseMessage)
        if (res.success == true) {
          this.cancel();
        }
        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log(error);
        // } else {
        //   this.toastService.ShowError(error.error.errorMessage);

        // }
        this.spinner.hide()
      });
  }
}
