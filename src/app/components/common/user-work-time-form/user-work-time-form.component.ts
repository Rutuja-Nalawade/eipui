import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-user-work-time-form',
  templateUrl: './user-work-time-form.component.html',
  styleUrls: ['./user-work-time-form.component.scss']
})
export class UserWorkTimeFormComponent implements OnInit {

  shiftForm: FormGroup;
  userData: any
  shiftsList: any = []
  selectedShifts: any = []
  selected: any
  shiftDayList: any
  selectedShiftIds: any = []
  shiftList: any = []
  workTimeList: any = []
  userType = ["", "student", "staff", "teacher", "guardian"]
  currentIndex: any = null
  daysArray = [{ day: "MON", dayValue: 2, id: 2, isSelected: false }, { day: "TUE", dayValue: 3, id: 3, isSelected: false }, { day: "WED", dayValue: 4, id: 4, isSelected: false }, { day: "THU", dayValue: 5, id: 5, isSelected: false }, { day: "FRI", dayValue: 6, id: 6, isSelected: false }, { day: "SAT", dayValue: 7, id: 7, isSelected: false }, { day: "SUN", dayValue: 1, id: 1, isSelected: false }]
  @Input() userId: any;
  @Input() currentTab: Number;
  @Input() staffShiftData: any;
  @Input() public userDetailsType: any;
  @Input() userUniqueId: any;
  @Output() nextTab = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private _shiftService: ShiftServiceService, private _userService: UserServiceService) {
    this.userData = this._commonService.getUserData();
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['staffShiftData']) {
      this.initshiftForm();
      this.getShifts();
      if (this.staffShiftData) {
        this.patchStaffShiftInfo(this.staffShiftData);
      } else {
        this.addNewRow()
      }
    }
  }
  initshiftForm() {
    this.shiftForm = this.formBuilder.group({
      orgUid: this.userData.orgUniqueId,
      userUid: ['', Validators.nullValidator],
      createdOrUpdatedByUid: this.userData.uniqueId,
      shiftsLists: this.formBuilder.array([]),
      userId: this.userId
    });
  }
  getShifts() {
    this.spinner.show();
    let typesArray = [];
    if (this.userDetailsType == 3) {
      typesArray = [1, 3]
    } else {
      typesArray = [2, 3]
    }
    this._shiftService.getShiftbasicDetails(typesArray, true).subscribe(
      response => {
        let shiftList = []
        let ShiftsList = response;
        this.workTimeList = response;
        let daysArray = [{ day: "MON", dayValue: 2, id: 2, isSelected: false }, { day: "TUE", dayValue: 3, id: 3, isSelected: false }, { day: "WED", dayValue: 4, id: 4, isSelected: false }, { day: "THU", dayValue: 5, id: 5, isSelected: false }, { day: "FRI", dayValue: 6, id: 6, isSelected: false }, { day: "SAT", dayValue: 7, id: 7, isSelected: false }, { day: "SUN", dayValue: 1, id: 1, isSelected: false }]
        for (let i = 0; i < ShiftsList.length; i++) {
          ShiftsList[i].days.sort((a, b) => {
            return a.dayValue - b.dayValue;
          });
          let days = []
          for (let index = 0; index < ShiftsList[i].days.length; index++) {
            const element = ShiftsList[i].days[index];
            days.push(element.dayValue)
          }
          for (let index = 0; index < daysArray.length; index++) {
            const element = daysArray[index];
            if (days.includes(element.dayValue)) {
              daysArray[index].isSelected = true
            }
          }
          let shift = {
            shiftUid: ShiftsList[i].uniqueId,
            isLinked: true,
            name: ShiftsList[i].name + ' (' + ShiftsList[i].startTime + ' - ' + ShiftsList[i].endTime + ')',
            workingDaysList: ShiftsList[i].days,
            isNew: false,
            editMode: false,
            days: daysArray,
            selected: ShiftsList[i].uniqueId
          }
          shiftList.push(shift)
        }
        this.shiftList = shiftList

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }
  patchStaffShiftInfo(staffShiftInfo) {
    this.selectedShifts = []
    this.selectedShiftIds = []

    let shiftArray = <FormArray>this.shiftForm.controls['shiftsLists'];
    shiftArray.controls = [];
    let ShiftsList = staffShiftInfo;
    this.shiftsList = staffShiftInfo;
    for (let i = 0; i < ShiftsList.length; i++) {
      let daysArray = [{ day: "MON", dayValue: 2, id: 2, isSelected: false }, { day: "TUE", dayValue: 3, id: 3, isSelected: false }, { day: "WED", dayValue: 4, id: 4, isSelected: false }, { day: "THU", dayValue: 5, id: 5, isSelected: false }, { day: "FRI", dayValue: 6, id: 6, isSelected: false }, { day: "SAT", dayValue: 7, id: 7, isSelected: false }, { day: "SUN", dayValue: 1, id: 1, isSelected: false }]
      let shifts = this.formBuilder.group({
        shiftUid: ShiftsList[i].uniqueId,
        isLinked: ShiftsList[i].isLinked,
        shiftName: ShiftsList[i].shiftName
      });
      // ShiftsList[i].workingDaysList.sort((a, b) => {
      //   return a.dayValue - b.dayValue;
      // });
      let days = [];
      let selectedDays = [];
      for (let index = 0; index < ShiftsList[i].days.length; index++) {
        const element = ShiftsList[i].days[index];
        days.push(element.dayValue)
      }
      for (let index = 0; index < daysArray.length; index++) {
        const element = daysArray[index];
        if (days.includes(element.dayValue)) {
          daysArray[index].isSelected = true;
          selectedDays.push(daysArray[index])
        }
      }
      let shift = {
        shiftUid: ShiftsList[i].uniqueId,
        isLinked: true,
        name: ShiftsList[i].name + ' (' + ShiftsList[i].startTime + ' - ' + ShiftsList[i].endTime + ')',
        workingDaysList: selectedDays,
        isNew: false,
        editMode: false,
        days: daysArray,
        selected: ShiftsList[i].uniqueId
      }
      this.selectedShifts.push(shift);
      this.selectedShiftIds.push(ShiftsList[i].uniqueId)
      shiftArray.push(shifts);
      this.shiftsList[i] = {
        shiftUid: ShiftsList[i].uniqueId,
        isLinked: true,
        name: ShiftsList[i].name + ' (' + ShiftsList[i].startTime + ' - ' + ShiftsList[i].endTime + ')',
        editMode: false, days: daysArray
      }
    }
    if (this.selectedShifts.length == 0) {
      this.addNewRow()
    }
    this.shiftForm.setControl('shiftsLists', shiftArray);
  }
  editRow(item, index) {
    if (this.currentIndex || this.currentIndex == 0) {
      this.selectedShifts[this.currentIndex].editMode = false
    }
    this.selectedShiftIds.splice(index, 1)
    item.selected = item.shiftUid
    item.editMode = true
    this.currentIndex = index
    this.shiftDayList = item.days
  }
  deleteRow(item, index) {
    if (!item.shiftUid) {
      // this.toastService.ShowWarning("Please select Shift then save.")
      return;
    }
    this.selectedShifts.splice(index, 1)
    this.selectedShiftIds.splice(index, 1)
  }
  addNewRow() {
    if (this.shiftList.length == this.selectedShifts.length && this.shiftList.length) {
      this.toastService.ShowWarning("All shift are assigned.")
    } else {
      let shift = {
        shiftUid: "",
        selected: null,
        isLinked: false,
        name: "",
        workingDaysList: [],
        isNew: true,
        editMode: true,
        days: this.daysArray
      }
      this.selectedShifts.push(shift);
    }

    this.selected = ""
  }
  updateRow(item, index) {
    if (this.selectedShiftIds.includes(item.selected) && this.selectedShiftIds[index] != item.selected) {
      this.toastService.ShowWarning("This shift already selected. Please select another.")
      return;
    }
    item.editMode = false
    this.selectedShifts[index].name = item.name
    this.selectedShifts[index].shiftUid = item.shiftUid
    this.selectedShifts[index].selected = item.shiftUid
    let days = this.shiftDayList.filter(day => day.isSelected)
    this.selectedShifts[index].workingDaysList = days
    this.selectedShifts[index].days = item.days
    this.selectedShiftIds[index] = item.shiftUid
  }
  cancelRow(item, index) {
    item.editMode = false
  }
  changeShift(event, index) {
    if (this.selectedShifts[index].isNew) {
      this.selectedShifts[index].days = event.days
      this.selectedShifts[index].name = event.name
      this.selectedShifts[index].shiftUid = event.shiftUid
    } else {
      this.shiftDayList = event.days
      this.selectedShifts[index].days = event.days
      this.selectedShifts[index].name = event.name
      this.selectedShifts[index].shiftUid = event.shiftUid
    }
    if (this.selectedShiftIds.includes(event.shiftUid) && this.selectedShiftIds[index] != event.shiftUid) {
      this.toastService.ShowWarning("This shift already selected. Please select another.")
      return;
    }
  }
  close() {
    if (this.userDetailsType == 2) {
      this._commonService.iscloseClicked.next(true)
    } else {
      this._commonService.iscloseClicked.next(3)
    }
  }
  save(item, index) {
    if (!item.shiftUid) {
      this.toastService.ShowWarning("Please select Shift then save.")
      return;
    }
    if (this.selectedShiftIds.includes(item.shiftUid) && this.selectedShiftIds[index] != item.shiftUid) {
      this.toastService.ShowWarning("This shift already selected. Please select another.")
      return;
    }
    let days = item.days.filter(day => day.isSelected)
    this.selectedShifts[index].workingDaysList = days
    item.editMode = false
    this.selectedShiftIds[index] = item.shiftUid
  }
  saveWorkTiming() {
    if (this.selectedShiftIds.length == 0) {
      this.toastService.ShowWarning("Please add atleast one shift.")
      return;
    }
    // let shiftArray = <FormArray>this.shiftForm.controls['shiftsLists'];

    // for (let index = 0; index < shiftArray.value.length; index++) {
    //   const element = shiftArray.value[index];
    //   if (this.selectedShiftIds.includes(element.shiftUid)) {
    //     element.isLinked = true
    //   } else {
    //     element.isLinked = false
    //   }
    // }
    this.spinner.show();
    let usertype = this.userType[this.userDetailsType];
    this._userService.assignShifts(usertype, this.selectedShiftIds, this.userUniqueId)
      .subscribe(data => {
        if (data) {
          //console.log(data);
          this.toastService.showSuccess("Shift Information saved successfully.");
          this.spinner.hide()
          setTimeout(() => {
            if (this.userDetailsType == 2) {
              this._commonService.iscloseClicked.next(true)
            } else {
              this._commonService.iscloseClicked.next(4)
            }
          }, 600);
        }
      }, (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide()
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide()
        // }
      });


  }
}
