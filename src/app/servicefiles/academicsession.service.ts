import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AcademicsessionService {

  constructor(private http: HttpClient) { }

  public getAcademicSession() {
    return this.http.get<any>(environment.apiUrl + '/academic-session/getacademicsession');
  }

  saveAcademicSession(academicsession: any) {
    return this.http.post<any>(environment.apiUrl + '/academic-session/addacademicsession', academicsession);
  }

  updateAcademicSession(academicsession: any) {
    return this.http.put<any>(environment.apiUrl + '/academic-session/updateacademicsession', academicsession);
  }

  deleteAcademicSession(academicsessionUid: any) {
    return this.http.delete<any>(environment.apiUrl + '/academic-session/deleteacademicsession/'+ academicsessionUid);
  }
}
