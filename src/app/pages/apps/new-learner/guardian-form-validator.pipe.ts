import { Pipe, PipeTransform } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Pipe({
  name: 'guardianFormValidator'
})
export class GuardianFormValidatorPipe implements PipeTransform {

  transform(value: number, args?: any): FormGroup['controls'] {
    return this.validateParentForm(value,args);
  }
  validateParentForm(point: number,guardianForms : Array<FormGroup>): FormGroup['controls'] {
    let formGroup = <FormGroup> guardianForms[point];
    //console.log(formGroup);
    let formGroupgeneralInfo = <FormGroup> formGroup.controls['generalInfo'];
    //console.log(formGroupgeneralInfo.controls);
    return formGroupgeneralInfo.controls;
 }

}
