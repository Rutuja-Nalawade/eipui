import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BulkasignComponent } from './bulkasign/bulkasign.component';

import { StudentrankComponent } from './studentrank/studentrank.component';
import { LeadboardComponent } from './leadboard/leadboard.component';
import { from } from 'rxjs';
import { ModifypaperComponent } from './modifypaper/modifypaper.component';
const routes: Routes = [
  
    {
        path: 'hr/:id',
        children: [

            {

                path: 'bulkasign',
                component: BulkasignComponent
            },
            {

                path: 'studentrank/:examId/:type',
                component: StudentrankComponent
            },
            {

                path: 'modifypaper/:examId',
                component: ModifypaperComponent
            },
            {

                path: 'leadboard',
                component: LeadboardComponent
            },
        ]
    },
    {

        path: 'bulkasign',
        component: BulkasignComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
