import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-staff-sheet',
  templateUrl: './staff-sheet.component.html',
  styleUrls: ['./staff-sheet.component.scss']
})
export class StaffSheetComponent implements OnInit {
  List = [{ emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Account', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'not well',status:'Pending'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Admin', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Holiday',subreason:'went for trip',status:'Approved'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Markeing', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Got Covid',status:'Rejected'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Supervisor', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Suffring from fever',status:'Pending'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'HR', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'not well',status:'Rejected'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Supervisor', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Holiday',subreason:'went for trip',status:'Pending'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Field', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Got Covid',status:'Approved'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Design', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Suffring from fever',status:'Rejected'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Developer  ', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Holiday',subreason:'went for trip',status:'Approved'},
  { emid: 'AR876-80', name: 'Roshini Jawaid', role: 'Data Analyst', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Suffring from fever',status:'Rejected'},];
  isPresent = true;
  isLeave = false;
  constructor() { }

  ngOnInit() {
  }

  isTabChange(id1, id2, id3) {
    let ele1 = document.getElementById(id1);
    let ele2 = document.getElementById(id2);
    let ele3 = document.getElementById(id3);
    ele1.classList.add('statusStaffBtnactive');
    ele2.classList.remove('statusStaffBtnactive')
    ele3.classList.remove('statusStaffBtnactive')
    if (id1 == 'present') {
      this.isPresent=true;
      this.isLeave=false;
    }else if(id1 == 'absent'){
      this.isPresent=false;
      this.isLeave=false;
    }else if(id1 == 'leave'){
      this.isPresent=false;
      this.isLeave=true;
    }
  }
}
