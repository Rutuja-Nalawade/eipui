import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  mobileVerifyForm: FormGroup;
  otpVerifyForm: FormGroup;
  submitted = false;
  otpSubmitted = false;
  error = '';
  success = '';
  loading = false;
  isMobileVerify = false;
  isOtpValid = false;
  isMobileVerifySuccesss = false;
  subscriptionDto: any;
  isMobileDisabled = false;
  timeLeft: number = 0;
  show: boolean = false;
  otpValid: boolean = false;
  otpResponse: boolean = false;
  interval;
  timesToMobileVerify = 1;
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private _authService: AuthServiceService, private router: Router, private toastService: ToastsupportService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state;

    // if(!state){
    //   this.router.navigate(['/account/signup']);
    // }else{
    //   this.subscriptionDto=state;
    // }
    // sessionStorage.setItem('timesToMobileVerify', "1");
  }

  ngOnInit() {
    this.mobileVerifyForm = this.formBuilder.group({
      orgCode: ['', [Validators.required]],
      mobile: ['', [Validators.required, Validators.pattern("^[0-9]{10}$")]]
    });
    this.otpVerifyForm = this.formBuilder.group({
      // otpNumber: ['', [Validators.required, Validators.pattern("[0-9]*")]]
      digit1: ['', [Validators.required]],
      digit2: ['', [Validators.required]],
      digit3: ['', [Validators.required]],
      digit4: ['', [Validators.required]],
      digit5: ['', [Validators.required]],
      digit6: ['', [Validators.required]],
    });
  }

  get f() { return this.mobileVerifyForm.controls; }
  get fa() { return this.otpVerifyForm.controls; }
  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;
    this.error = ''

    // stop here if form is invalid
    if (this.mobileVerifyForm.invalid) {
      return;
    }

    if (this.interval) {
      clearInterval(this.interval);
    }

    this.loading = true;
    //let mobile = this.mobileVerifyForm.controls['mobile'].value;
    let data = {
      "organizationCode": this.mobileVerifyForm.controls['orgCode'].value,
      "mobileNumber": this.mobileVerifyForm.controls['mobile'].value
    }
    this._authService.getLoginOtp(data).subscribe((res: any) => {
      console.log('response of otp verification====>>>', res);
      if (res && res.success) {
        console.log('true response');
        if (res.isOtpVerified) {
          this.router.navigate(['/account/organization-type/'], { state: this.subscriptionDto });
        } else {
          setTimeout(() => {
            this.loading = false;
            // this.isOtpValid = true
            // this.router.navigate(['/account/confirm']);
            this.isMobileVerify = true;
            this.isMobileDisabled = true;
            this.mobileVerifyForm.disable()
            this.timeLeft = 120
            this.startTimer();
          }, 1000);
        }
      } else {
        console.log('false response');
        this.error = res.responseMessage;
        this.loading = false;
      }
    },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.loading = false;
        // this.error='No Organization found'
      });

    console.log(this.mobileVerifyForm.value);

  }
  onOtpSubmit() {
    this.otpSubmitted = true;
    this.error = ''
    // stop here if form is invalid
    if (this.otpVerifyForm.invalid) {
      this.isOtpValid = false;
      return;
    } else {
      this.isOtpValid = true;
      this.loading = true;
      let otp = this.otpVerifyForm.controls['digit1'].value + this.otpVerifyForm.controls['digit2'].value + this.otpVerifyForm.controls['digit3'].value + this.otpVerifyForm.controls['digit4'].value + this.otpVerifyForm.controls['digit5'].value + this.otpVerifyForm.controls['digit6'].value;
      // console.log('data before otp verification==>>', data);
      let data = {
        "organizationCode": this.mobileVerifyForm.controls['orgCode'].value,
        "mobileNumber": this.mobileVerifyForm.controls['mobile'].value,
        "otp": otp
      }
      this._authService.verifyLoginOtp(data).subscribe((res: any) => {
        console.log('response of otp verification====>>>', otp);
        // var res=false
        if (res && res.success) {
          this.otpResponse = true
          this.otpValid = true
          sessionStorage.setItem('tokenId', res.token);
          sessionStorage.setItem('currentUser', JSON.stringify(res));
          //   console.log('true response');
          //   setTimeout(() => {
          //   this.router.navigate(['/account/organization-type/'],{state:res});
          // }, 1000);
          this.navigate(res);
          this.loading = false;

        } else {
          // console.log('false response');
          this.otpResponse = true
          this.otpValid = false
          // this.error='Otp expired!'
          this.loading = false;
          this.toastService.ShowWarning(res.responseMessage);
        }
      },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.otpResponse = true
          this.otpValid = false
          this.loading = false;
        });
    }
  }

  navigate(res: any) {
    if (res.orgState == 1) {
      setTimeout(() => {
        this.router.navigate(['/account/organization-type/'], { state: res });
      }, 1000);
    } else if (res.orgState == 2 || res.orgState == 3 || res.orgState == 4) {
      setTimeout(() => {
        this.router.navigate(['/account/setup/'], { state: res });
      }, 1000);
    } else if (res.orgState == 5) {
      sessionStorage.setItem('isSetupDone', "true");
      setTimeout(() => {
        this.router.navigate(['/newModule/academic/academicDetails']);
      }, 1000);
    }
  }

  clickToLogin() {
    this.router.navigate(['/account/login']);
  }

  startTimer() {
    this.timeLeft = 120
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        clearInterval(this.interval);
      }
    }, 1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }
  onChangeEvent(event) {
    // this.isMobileVerify= false
    this.error = ''
    if (!this.mobileVerifyForm.invalid) {
      console.log("mobileVerifyForm", this.mobileVerifyForm);
      // this.isMobileDisabled= true
    }

  }
  mobileChange() {
    this.isMobileDisabled = false;
    this.isMobileVerify = false;
    this.submitted = false;
    this.otpSubmitted = false;
    this.otpValid = false;
    this.otpResponse = false;
    this.mobileVerifyForm.enable();
    this.mobileVerifyForm.controls['mobile'].setValue('');
    this.otpVerifyForm.reset();
  }
  getCodeBoxElement(index) {
    return document.getElementById('otpDigit' + index);
  }
  onKeyUpEvent(index, event) {
    const eventCode = event.which || event.keyCode;
    if ((this.getCodeBoxElement(index) as HTMLInputElement).value.length === 1) {
      if (index !== 6) {
        (this.getCodeBoxElement(index + 1) as HTMLInputElement).focus();
      } else {
        (this.getCodeBoxElement(index) as HTMLInputElement).blur();
        // Submit code
        console.log('submit code ');
      }
    }
    if (eventCode === 8 && index !== 1) {
      (this.getCodeBoxElement(index - 1) as HTMLInputElement).focus();
    }
    if (!this.otpVerifyForm.invalid) {
      this.isOtpValid = true;
    } else {
      this.otpResponse = false
    }
  }
  onFocusEvent(index) {
    for (var item = 1; item < index; item++) {
      const currentElement = this.getCodeBoxElement(item);

      if (!(currentElement as HTMLInputElement).value) {
        (currentElement as HTMLInputElement).focus();
        break;
      }
    }
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

}
