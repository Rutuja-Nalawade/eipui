import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { createTranslateLoader } from 'src/app/layouts/layouts.module';
import { HttpClient } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { SignupNewComponent } from './signup-new/signup-new.component';
import { MobileVerificationNewComponent } from './mobile-verification-new/mobile-verification-new.component';
import { OrganizationTypeNewComponent } from './organization-type-new/organization-type-new.component';
import { NewAuthRoutingModule } from './new-auth-routing.module';
import { SetupComponent } from './setup/setup.component';
import { SetupNewUIModule } from 'src/app/pages/apps/setup-new-ui/setup-new-ui.module';
import { NewAcademicModule } from 'src/app/pages/apps/new-academic/new-academic.module';


@NgModule({
  declarations: [LoginComponent, SignupNewComponent, MobileVerificationNewComponent, OrganizationTypeNewComponent, SetupComponent],
  imports: [
    FormsModule,
    SetupNewUIModule,
    NewAcademicModule,
    CommonModule,
    NewAuthRoutingModule,
    ReactiveFormsModule,
    NgbAlertModule,
    UIModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [DatePipe]
})
export class NewAuthModule { }
