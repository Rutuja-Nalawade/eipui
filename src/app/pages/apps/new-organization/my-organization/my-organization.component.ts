import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrganizationServiceService } from 'src/app/servicefiles/organization-service.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-my-organization',
  templateUrl: './my-organization.component.html',
  styleUrls: ['./my-organization.component.scss']
})
export class MyOrganizationComponent implements OnInit {

  oganizationForm: FormGroup;
  submitted = false;
  workingDayList: any = [];
  isEditData = false;
  isUpdate = false;
  daysArray = [{ day: "Monday", dayValue: 2, id: 2, isSelected: false }, { day: "Tuesday", dayValue: 3, id: 3, isSelected: false }, { day: "Wednesday", dayValue: 4, id: 4, isSelected: false }, { day: "Thursday", dayValue: 5, id: 5, isSelected: false }, { day: "Friday", dayValue: 6, id: 6, isSelected: false }, { day: "Saturday", dayValue: 7, id: 7, isSelected: false }, { day: "Sunday", dayValue: 1, id: 1, isSelected: false }]
  @Input() organizationData: any;
  constructor(private formBuilder: FormBuilder, private spinner: NgxSpinnerService, private organizationService: OrganizationServiceService, private toastService: ToastsupportService, private _shiftService: ShiftServiceService) {
    this.initorganizationForm();
  }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['organizationData'] && changes['organizationData'].currentValue) {
      this.patchData(this.organizationData);
      this.getOrgWorkDays();
    }

  }
  initorganizationForm() {
    // const reg = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    const reg = '([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';//(www)\\.
    this.oganizationForm = this.formBuilder.group({
      name: ['', Validators.required],
      code: ['', Validators.required],
      url: ['', Validators.nullValidator],
      emailId: ['', Validators.compose([Validators.required, Validators.email])],
      phone: ['', Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      website: ['', Validators.compose([Validators.required, Validators.pattern(reg)])],
      shortDescription: ['', Validators.nullValidator],
      longDescription: ['', Validators.nullValidator],
      facebook: ['', Validators.nullValidator],
      twitter: ['', Validators.nullValidator],
      linkedIn: ['', Validators.nullValidator]
    })
  }
  get org() {
    let organization = <FormGroup>this.oganizationForm;
    return organization.controls;
  }
  save() {
    this.submitted = true;
    console.log(this.oganizationForm);
    if (this.oganizationForm.invalid) {
      return;
    }
    let days = this.daysArray.filter(x => x.isSelected == true);
    if (days && days.length == 0) {
      this.toastService.ShowWarning("Please select working days.")
      return;
    }
    let dayArray = days.map(x => x.dayValue);
    this.spinner.show();
    this.organizationService.updateWorkingDays(dayArray).subscribe(
      res => {
        if (res) {
          this.toastService.showSuccess("Working days save successfully.")
        } else {
          // this.toastService.ShowWarning("Organization Data not save!")
        }
        // this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        // this.spinner.hide();
      });
    let dto = this.oganizationForm.value;
    this.organizationService.saveOrganizationDetails(dto).subscribe(
      res => {
        if (res) {
          this.toastService.showSuccess("Organization data save successfully.");
          this.isUpdate = true;
          this.isEditData = false;
        } else {
          this.toastService.ShowWarning("Organization Data not save!");
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  patchData(data) {
    this.spinner.show();
    this.isEditData = false;
    this.isUpdate = false;
    this.oganizationForm.controls.code.setValue(data.code);
    this.oganizationForm.controls.name.setValue(data.name);
    this.oganizationForm.controls.emailId.setValue((data.emailId) ? data.emailId : "");
    this.oganizationForm.controls.phone.setValue((data.phone) ? data.phone : "");
    this.oganizationForm.controls.facebook.setValue((data.facebook) ? data.facebook : "");
    this.oganizationForm.controls.linkedIn.setValue((data.linkedIn) ? data.linkedIn : "");
    this.oganizationForm.controls.twitter.setValue((data.twitter) ? data.twitter : "");
    this.oganizationForm.controls.url.setValue((data.url) ? data.url : "");
    this.oganizationForm.controls.website.setValue((data.website) ? data.website : "");
    this.oganizationForm.controls.shortDescription.setValue((data.shortDescription) ? data.shortDescription : "");
    this.oganizationForm.controls.longDescription.setValue((data.longDescription) ? data.longDescription : "");
    if (this.oganizationForm.invalid) {
      this.isEditData = true;
    } else {
      this.isUpdate = true;
    }
    this.spinner.hide();
  }
  checkDays(day) {
    console.log(day);
    if (!day.isSelected) {
      day.isSelected = true;
    } else {
      day.isSelected = false;
    }
  }
  getOrgWorkDays() {
    this.spinner.show();
    this._shiftService.getShiftWorkingDays().subscribe(
      res => {
        if (res) {
          this.workingDayList = res;
          let days = this.workingDayList.map(x => x.dayValue);
          this.daysArray.forEach(element => {
            if (days.includes(element.dayValue)) {
              element.isSelected = true;
            }
          });
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        //   //this.sessionService.checkUnauthorizedRequest(error.error);
        // }
        this.spinner.hide();
      });
  }
  editData() {
    this.isEditData = true;
  }
}
