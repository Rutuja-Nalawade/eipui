import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExamServiceService {

  studentList = new Subject();
  subjectList = new Subject();
  courses = new Subject();
  batches = new Subject();
  rightMarks = new Subject();

  constructor(private http: HttpClient) { }

  //Assignment
  getfilteredStudent(filterDto) {
    return this.http.put<any>(environment.newApiUrl + '/students/get/list', filterDto)
  }
  getAssigmnentCategory(urlString) {
    return this.http.get<any>(environment.newEaasUrl + '/get/' + urlString);
  }
  addAssigmnentCategory(dto, urlString) {
    return this.http.post<any>(environment.newEaasUrl + '/add/' + urlString, dto);
  }
  getCoursePlannerList(subjectId: any, courseId) {
    return this.http.get<any>(environment.newApiUrl + '/coursePlanner/get/allCoursePlanner/' + courseId + '/' + subjectId);
  }
  getUnitListByCoursePlanner(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/coursePlanner/get/Units/' + uniqueId);
  }
  getChapterTopicListByCoursePlanner(uniqueId, type) {
    return this.http.get<any>(environment.newApiUrl + '/coursePlanner/get/ChaptersDetails/' + uniqueId + '/' + type);
  }
  getSubtopicListByCoursePlanner(uniqueId) {
    return this.http.get<any>(environment.newApiUrl + '/coursePlanner/get/chaptersSubtopic/' + uniqueId);
  }
  addExam(urlString, dto) {
    return this.http.post<any>(environment.newEaasUrl + '/exam/' + urlString + '/create', dto);
  }
  addPatternCategory(dto) {
    return this.http.post<any>(environment.newEaasUrl + '/patternCategory/create', dto);
  }
  getPatternCategory() {
    return this.http.get<any>(environment.newEaasUrl + '/patternCategory/get/false');
  }
  getPatternCategoryDetailed() {
    return this.http.get<any>(environment.newEaasUrl + '/patternCategory/get/true');
  }
  getPatternByCategory(uniqueId) {
    return this.http.get<any>(environment.newEaasUrl + '/examPattern/get/forCategory/' + uniqueId);
  }
  getQuestionCategories() {
    return this.http.get<any>(environment.newEaasUrl + '/question/get/category');
  }
  getPattern(uniqueId) {
    return this.http.get<any>(environment.newEaasUrl + '/examPattern/get/' + uniqueId);
  }
  getGradePlanner(courseUid, subjectUid) {
    return this.http.get<any>(environment.newApiUrl + '/linking/get/getGradeAndPlannerFromSubjectUid/' + courseUid + '/' + subjectUid);
  }
  cancelExam(examUid) {
    return this.http.get<any>(environment.newEaasUrl + '/exam/cancel/' + examUid);
  }
  lockExamTrack(examUid: string) {
    return this.http.get<any>(environment.newEaasUrl + "/exam-track/lock/" + examUid);
  }
  addPattern(patternJson) {
    return this.http.post<any>(environment.newEaasUrl + "/examPattern/create", patternJson);
  }
  getAdvancePatternByCategory() {
    return this.http.get<any>(environment.newEaasUrl + '/examPattern/get/advancedExamPattern/');
  }
  updateSubjectSyllyabus(patternJson) {
    return this.http.put<any>(environment.newEaasUrl + "/exam/update/subject-syllabus", patternJson);
  }
}
