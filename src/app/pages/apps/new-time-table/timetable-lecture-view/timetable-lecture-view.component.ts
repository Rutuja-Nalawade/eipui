import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { letProto } from 'rxjs-compat/operator/let';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { ClassroomServiceService } from 'src/app/servicefiles/classroom-service.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-timetable-lecture-view',
  templateUrl: './timetable-lecture-view.component.html',
  styleUrls: ['./timetable-lecture-view.component.scss']
})
export class TimetableLectureViewComponent implements OnInit {
  isJeeAdd: boolean = true;
  isJeeMains: boolean = false;
  isJeeNeet: boolean = false;
  isJeeBoard: boolean = false;
  itemList = [{ id: 1, name: 'Batch' }, { id: 2, name: 'Teacher' }, { id: 3, name: 'Classroom' }];
  shiftList = []
  typeList1 = [{ id: 0, name: 'All' }];
  typeListValue: any;
  slectedTypeListData: any = {};
  itemListValue = 1;
  selectedShiftValue = null;
  isNewLecture = false;
  childtype = 2;
  type = 4;
  isactiveTab = 1;
  colorClassArray = ['color1', 'color2', 'color3', 'color4'];
  courseList = []
  @ViewChild('myselect1', { static: true }) myselect1;
  @ViewChild('myselect2', { static: true }) myselect2;
  @ViewChild('myselect3', { static: true }) myselect3;

  @Input() mainType: any;
  typeList = [{ id: 1, name: "Batch", isActive: false }, { id: 2, name: "Teacher", isActive: false }, { id: 3, name: "Classroom", isActive: true }]
  dateList = [];
  selectedType = 1;
  isRightSidePopup = false;
  popupType = 1;
  isPopHover = false;
  viewData = {};
  timetabaleDetails: any = {};
  allTimetabaleDetails: any = {};
  itemTypeList = [];
  listData: any = [];
  subjectList: any = [];
  batchList: any = [];
  pageIndex = 0;
  isBatchdropdown = false;
  selectedSubjects: any = []
  selectedSubjectList: any = []
  selectedBatchList: any = []
  selectedBatchData: any = {}
  selectedCourseData: any = {}
  selectedBatch: any = null
  selectedCourse: any = null
  isSlotsHide = false;
  periodData: any = {};
  selectedLectureData: any = {};
  isRemoeLecture = false;
  isEditLecture = false;
  selectedLectureId: any = "";
  batchData: any = {};
  teacherList: any = [];
  classroomList: any = [];
  shiftWiseData: any = [];
  courseWiseData: any = [];
  allShiftList: any = [];
  days = ["", "Sunday", "Monday", "Tuesday", "Wedesday", "Thursady", "Friday", "Saturday"];
  isSelectAll = false;
  @Input() selectedDate: any;
  @Input() timeTableData: any;
  @Output() close = new EventEmitter<string>();
  @Output() newItemEvent = new EventEmitter<string>();
  constructor(private timeTableService: TimeTableServiceService, private toastService: ToastsupportService, private spinner: NgxSpinnerService, private classRoomService: ClassroomServiceService, private userService: UserServiceService, private shiftService: ShiftServiceService, private batchesService: BatchesServiceService, private subjectService: SubjectServiceService) { }

  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges) {

    if (changes['mainType'] && (changes['mainType'].currentValue == 2 || changes['mainType'].currentValue == 1)) {
      // this.getClassRooms();
      // this.getShifts();
      // this.getSubjectList();
      this.getTimeTableData(this.timeTableData.uniqueId);
    }
  }
  getTimeTableData(uniqueId) {
    this.spinner.show();
    this.timeTableService.getTimeTableData(uniqueId).subscribe(
      (res) => {
        if (res) {
          let listData = {};
          listData = res;
          this.allTimetabaleDetails = JSON.stringify(res);
          if (res.periodWiseLectures.length == 0 && this.mainType == 1) {
            this.spinner.hide();
            this.close.emit("0");
            return;
          }
          this.patchLectures(listData);
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  patchLectures(data) {
    let listData = []
    this.timetabaleDetails = data;
    listData = data.periodWiseLectures;
    if (listData.length > 0) {
      this.periodData = [];
      let tempTeachers = [];
      let tempSubjects = [];
      let tempBatches = [];
      let tempClassrooms = [];
      let tempShiftList = [];
      let tempCourseList = [];

      this.periodData = data.periodWiseLectures;
      this.courseList = data.courseList;
      console.log("daywise data", this.periodData)
      if (data.periodWiseLectures != null) {
        data.periodWiseLectures.forEach(period => {
          // period.startTime = this.getTime(period.startTime);
          // period.endTime = this.getTime(period.endTime);
          //for shift
          period.shiftIds.forEach(shift => {
            tempShiftList.push(shift);
            if (this.shiftWiseData[shift.uniqueId]) {
              this.shiftWiseData[shift.uniqueId].push(period);
            } else {
              this.shiftWiseData[shift.uniqueId] = [period];
            }
          });
          period.dayWiseLectureList.forEach(day => {
            day.lectures.forEach(lecture => {
              //process for teachers
              lecture.teacherIds.forEach(teacher => {
                tempTeachers.push(teacher)
              });
              /// process for batches
              lecture.batchIds.forEach(batch => {
                tempBatches.push(batch)
              });
              //process for classrooms
              tempClassrooms.push({
                name: lecture.classRoomName,
                uniqueId: lecture.classRoomId
              })
              // process for subjects
              tempSubjects.push({
                name: lecture.subjectName,
                uniqueId: lecture.subjectId
              })
              // process for course
              tempCourseList.push({
                name: lecture.courseName,
                uniqueId: lecture.courseId
              })
              //course wise batch
              if (this.courseWiseData[lecture.courseId]) {
                this.courseWiseData[lecture.courseId] = this.courseWiseData[lecture.courseId].concat(lecture.batchIds);
                let batch = this.courseWiseData[lecture.courseId];
                this.courseWiseData[lecture.courseId] = batch.filter((thing, index) => {
                  const _thing = JSON.stringify(thing);
                  return index === batch.findIndex(obj => {
                    return JSON.stringify(obj) === _thing;
                  });
                });
              } else {
                this.courseWiseData[lecture.courseId] = lecture.batchIds;
              }
            });
          });
        });
        this.subjectList = tempSubjects.reduce((unique, o) => {
          if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
            unique.push(o);
          }
          return unique;
        }, []);
        //pass this subjects to selected subjects
        //for uniqueIds
        let tempSelectedSubjectsList = [];
        // for ui chips show;
        let tempSelectedSubjects = [];
        this.subjectList.forEach(subject => {
          tempSelectedSubjectsList.push(subject.uniqueId);
          tempSelectedSubjects.push(subject)
        });
        this.selectedSubjectList = tempSelectedSubjectsList;
        this.selectedSubjects = tempSelectedSubjects;
        this.courseList = tempCourseList.reduce((unique, o) => {
          if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
            unique.push(o);
          }
          return unique;
        }, []);
        this.allShiftList = tempShiftList.reduce((unique, o) => {
          if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
            unique.push(o);
          }
          return unique;
        }, []);
        this.selectedShiftValue = this.allShiftList[0].uniqueId;
        this.shiftList = this.allShiftList;
        console.log("suject", this.subjectList)
        this.classroomList = tempClassrooms.reduce((unique, o) => {
          if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
            unique.push(o);
          }
          return unique;
        }, []);
        //set default classroom 
        this.itemTypeList = this.classroomList;
        this.typeListValue = this.classroomList[0].uniqueId;
        this.slectedTypeListData = this.classroomList[0];

        this.batchList = tempBatches.reduce((unique, o) => {
          if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
            unique.push(o);
          }
          return unique;
        }, []);
        this.teacherList = tempTeachers.reduce((unique, o) => {
          if (!unique.some(obj => obj.uniqueId === o.uniqueId && obj.name === o.name)) {
            unique.push(o);
          }
          return unique;
        }, []);

        // this.shiftChange({ uniqueId: this.selectedShiftValue })
        this.changeType({ id: 1, name: 'Batch' });
      }
      console.log(this.shiftWiseData);
      console.log(this.subjectList);
      console.log(this.teacherList);
      console.log(this.classroomList);
      console.log(this.batchList);
      console.log(this.timetabaleDetails.periodWiseLectures);
      // this.sortingLectures(this.typeListValue, listData)
    }
  }
  filterData(evt) {
    // console.log(JSON.stringify(evt))
    // console.log('filtertype' + this.selectedFilterType)
    this.typeListValue = evt.uniqueId;
    switch (this.itemListValue) {
      case 3:
        // this is for classroom list
        this.getClassroomShifts(evt.uniqueId, this.timeTableData.uniqueId);
        break;
      case 1:
        // this is for batch list
        this.getBatchShifts(evt.uniqueId);

        break;

      case 2:
        // this is for Teacher list
        this.getTeacherShifts(evt.uniqueId);
        break;
    }

  }
  getClassroomShifts(classroomUniqueId, timetableId) {
    this.spinner.show();
    this.shiftService.getShiftByClassroomAndTimeTable(classroomUniqueId, timetableId).subscribe(
      res => {
        this.shiftList = []
        if (res.success == true) {
          //intersection of two arrays get similar data
          this.shiftList = this.allShiftList.filter(item1 => res.list.some(item2 => item1.uniqeId === item2.uniqeId));
          this.selectedShiftValue = this.shiftList[0].uniqueId
          console.log('get classroom shift called ')
          this.shiftChange({ uniqueId: this.shiftList[0].uniqueId })
        } else {
          this.toastService.ShowWarning('No shifts found for this batch')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  getTeacherShifts(uniqueId: any) {
    this.spinner.show();
    this.shiftService.getShiftByTeacher(uniqueId).subscribe(
      res => {
        this.shiftList = []
        if (res) {
          //intersection of two arrays get similar data
          this.shiftList = this.allShiftList.filter(item1 => res.some(item2 => item1.uniqeId === item2.uniqeId));
          this.selectedShiftValue = this.shiftList[0].uniqueId
          this.shiftChange({ uniqueId: this.shiftList[0].uniqueId })
        } else {
          this.toastService.ShowWarning('No shifts found for this teacher')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }

  getBatchShifts(uniqueId) {
    this.spinner.show();
    this.shiftService.getShiftByBatch(uniqueId).subscribe(
      res => {
        this.shiftList = []
        if (res.success == true) {
          //intersection of two arrays get similar data
          this.shiftList = this.allShiftList.filter(item1 => res.list.some(item2 => item1.uniqeId === item2.uniqeId));
          this.selectedShiftValue = this.shiftList[0].uniqueId
          this.shiftChange({ uniqueId: this.shiftList[0].uniqueId })
        } else {
          this.toastService.ShowWarning('No shifts found for this batch')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  async shiftChange(evt) {
    this.selectedShiftValue = evt.uniqueId;
    console.log(this.allTimetabaleDetails);
    let listData = JSON.parse(this.allTimetabaleDetails);
    this.shiftWiseData = [];
    listData.periodWiseLectures.forEach(period => {
      //for shift
      period.shiftIds.forEach(shift => {
        if (this.shiftWiseData[shift.uniqueId]) {
          this.shiftWiseData[shift.uniqueId].push(period);
        } else {
          this.shiftWiseData[shift.uniqueId] = [period];
        }
      });
    });
    // this.shiftWiseData = this.allTimetabaleDetails.shiftWiseData;
    console.log(this.shiftWiseData);
    if (this.shiftWiseData[evt.uniqueId]) {
      let listData = this.shiftWiseData[evt.uniqueId];
      this.sortingLectures(this.typeListValue, listData)
    } else {
      this.timetabaleDetails.periodWiseLectures = []
    }
  }
  sortingLectures(uniqueId, listData) {
    let list = [];
    listData.forEach(element => {
      let count = 0;
      element.dayWiseLectureList.forEach(period => {
        let lectures = [];
        period.lectures.forEach(lecture => {
          lecture['startTime'] = element.startTime;
          lecture['endTime'] = element.endTime;
          // console.log(this.selectedSubjectList, lecture.subjectId);
          if (this.selectedSubjectList.length == 0 || (this.selectedSubjectList.length > 0 && this.selectedSubjectList.includes(lecture.subjectId))) {
            if (this.itemListValue == 3) {
              if (lecture.classRoomId == uniqueId) {
                lectures.push(lecture)
              }
            } else if (this.itemListValue == 1) {
              lecture.batchIds.forEach(batch => {
                if (batch.uniqueId == uniqueId) {
                  lectures.push(lecture);
                  return;
                }
              });
            } else if (this.itemListValue == 2) {
              lecture.teacherIds.forEach(teacher => {
                if (teacher.uniqueId == uniqueId) {
                  lectures.push(lecture);
                  return;
                }
              });
            }
          }
        });
        if (this.isSlotsHide && lectures.length == 0) {
          ++count;
        }
        period.lectures = lectures;

      });
      if (count != 7) {
        list.push(element);
      }
      // }
    });
    this.timetabaleDetails.periodWiseLectures = list;
    // console.log(list, uniqueId, listData);
  }
  changeTypeList(event) {
    this.slectedTypeListData = event;
    this.filterData({ uniqueId: event.uniqueId })
  }
  clickCourse() {
    this.isBatchdropdown = true;
  }
  changeCourseList(event) {
    console.log(event, this.batchList);
    this.selectedCourse = event.uniqueId;
    this.selectedCourseData = event;
    this.selectedBatchList = this.courseWiseData[this.selectedCourse];
    this.selectedBatch = this.selectedBatchList[0].uniqueId;
    this.typeListValue = this.selectedBatchList[0].uniqueId;
    this.selectedBatchData = this.selectedBatchList[0];
    this.filterData({ uniqueId: this.selectedBatch })
  }
  changeBatch(event, batchData) {
    this.selectedBatchData = batchData
    this.selectedBatch = batchData.uniqueId
    this.typeListValue = batchData.uniqueId
    this.filterData({ uniqueId: batchData.uniqueId })
    console.log(event, this.batchList);
  }
  changeSubjects(event) {
    console.log(event, this.selectedSubjectList);
    if (event && !event.target) {
      this.selectedSubjects = event;
      this.shiftChange({ uniqueId: this.selectedShiftValue });
    }
  }
  removeSubject(item, index) {
    this.selectedSubjects.splice(index, 1)
    this.selectedSubjectList = this.selectedSubjectList.filter(t => t !== item.uniqueId);
    this.shiftChange({ uniqueId: this.selectedShiftValue });
  }
  changeType(event) {
    console.log(event)

    this.itemListValue = event.id;
    switch (event.id) {
      case 0:
        let listData = [];
        let data = JSON.parse(this.allTimetabaleDetails);
        listData = data.periodWiseLectures;
        if (listData.length > 0) {
          this.sortingLectures("", listData)
        }
        break;
      case 1:
        console.log(this.courseList);
        this.selectedCourse = this.courseList[0].uniqueId;
        this.selectedCourseData = this.courseList[0];
        this.selectedBatchList = this.courseWiseData[this.selectedCourse];
        this.selectedBatch = this.selectedBatchList[0].uniqueId;
        this.typeListValue = this.selectedBatchList[0].uniqueId;
        this.selectedBatchData = this.selectedBatchList[0];
        this.filterData({ uniqueId: this.selectedBatch })
        break;
      case 2:
        this.typeListValue = this.teacherList[0].uniqueId;
        this.itemTypeList = this.teacherList;
        this.slectedTypeListData = this.teacherList[0];
        this.filterData({ uniqueId: this.teacherList[0].uniqueId })
        break;
      case 3:
        this.itemTypeList = this.classroomList;
        this.typeListValue = (this.classroomList[0] && this.classroomList[0].uniqueId) ? this.classroomList[0].uniqueId : null;
        this.slectedTypeListData = (this.classroomList[0]) ? this.classroomList[0] : null;
        this.filterData({ uniqueId: (this.classroomList[0] && this.classroomList[0].uniqueId) ? this.classroomList[0].uniqueId : null })
        break;

    }

  }
  save(type) {
    if (type == 3) {
      this.isRightSidePopup = true;
      this.popupType = 7;
    } else {
      this.close.emit("0");
    }
  }
  cancel() {
    this.newItemEvent.emit("false");
    this.close.emit("0");
  }

  close1(value) {
    this.newItemEvent.emit(value);
    this.close.emit("0");
  }
  actionPopupView(event) {
    let data = JSON.parse(event);
    console.log(data.type, typeof data.type);
    switch (data.type) {
      case 1:
        this.selectedLectureData = data;
        this.isRemoeLecture = true;
        break;
      case 2:
        this.selectedLectureId = data.uniqueId;
        this.batchData = { uniqueId: data.batchIds[0].uniqueId }
        data['shiftId'] = this.selectedShiftValue;
        this.selectedLectureData = data;
        this.type = 4;
        this.isEditLecture = true;
        break;
    }
  }
  removeAction(type) {
    if (type == 1) {
      this.isRemoeLecture = false;
    } else {
      console.log(this.selectedLectureData.uniqueId);
      this.spinner.show();
      return this.timeTableService.removeLecture(this.selectedLectureData.uniqueId).subscribe(
        res => {
          if (res && res.success) {
            this.toastService.showSuccess("Lecture remove succefully");
            this.isRemoeLecture = false;
            this.getTimeTableData(this.timeTableData.uniqueId);
          } else {
            this.toastService.ShowError(res.responseMessage);
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }
  }
  closeAddLectureForm(event) {
    this.getTimeTableData(this.timeTableData.uniqueId);
    this.isNewLecture = false;
    this.isEditLecture = false;
    this.isRightSidePopup = false;
    // if (this.popupType == 6 && this.childtype == 2 && event == "1") {
    //   this.type = 5;
    //   this.isNewLecture = true;
    // }
    // if (event == "2") {
    //   this.popupType = 6;
    //   this.childtype = 2;
    //   this.isRightSidePopup = true;
    // }
  }
  editLecture() {
    this.type = 4;
    this.childtype = 3;
    this.isNewLecture = true;
  }
  itemHover() {
    this.isPopHover = true;
  }
  hideSlots(event) {
    console.log(event);
    this.isSlotsHide = event.target.checked;
    this.shiftChange({ uniqueId: this.selectedShiftValue })
  }
  onClickedOutside(e) {
    console.log(e);
    if (e.target.id != 'link-project' && !e.target.classList.contains('ng-option') && this.isBatchdropdown) {
      console.log("object");
      this.isBatchdropdown = false;
    }
  }
  assignLecture(item, dayIndex) {
    console.log(item, dayIndex);
    let day: any = {};
    for (let i = 0; i < item.dayWiseLectureList.length; i++) {
      const element = item.dayWiseLectureList[i];
      if (element.dayValue == dayIndex) {
        day = element;
        break;
      }
    }
    console.log(day);
    this.periodData = {
      periodId: item.uniqueId,
      shiftId: this.selectedShiftValue,
      periodName: item.name,
      day: day.orgDayUniqueId,
      dayValue: dayIndex,
      dayName: this.days[dayIndex]
    }
    console.log(this.periodData);
    this.type = 5;
    this.isNewLecture = true;
  }
  onSelectAll(event) {
    console.log(event);
    this.selectedSubjects = [];
    let selected: any = []
    if (event.target.checked) {
      this.subjectList.map(item => {
        selected.push(item.uniqueId);
      });
      this.selectedSubjects = this.subjectList;
    }
    this.selectedSubjectList = selected;
    this.shiftChange({ uniqueId: this.selectedShiftValue });
    this.isSelectAll = !this.isSelectAll;
  }
}
