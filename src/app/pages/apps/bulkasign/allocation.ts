export class Allocation {
    selectedAcademicYearId: string;
    selectedAcademicCalenderId: string;
    selectedcourseId: string;
    selectedBoardId: string;
    selectedBatchId: string;
    selectedGradeId: string;
    selectedGroupId: string;
    studentList:{
        studentId: number; 
        selectedName: string;
    }[]= [];
}
