import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewAcademicRoutingModule } from './new-academic-routing.module';
import { CommonheaderComponent } from './commonheader/commonheader.component';
import { AcademicSessionComponent } from './academic-session/academic-session.component';
import { AcademicGradeComponent } from './academic-grade/academic-grade.component';
import { AcademicCoursesComponent } from './academic-courses/academic-courses.component';
import { AcademicSubjectsComponent } from './academic-subjects/academic-subjects.component';
import { AcademicBatchesComponent } from './academic-batches/academic-batches.component';
import { AcademicClassRoomsComponent } from './academic-class-rooms/academic-class-rooms.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AlertModule, TooltipModule } from 'ngx-bootstrap';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AddGradeBoardComponent } from './add-grade-board/add-grade-board.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { SetupNewUIModule } from '../setup-new-ui/setup-new-ui.module';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { AcademicHomeComponent } from './academic-home/academic-home.component';
import { SetupComponent } from 'src/app/account/new-authentication/setup/setup.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UIModule } from 'src/app/shared/ui/ui.module';



@NgModule({
  declarations: [CommonheaderComponent,
    AcademicSessionComponent,
    AcademicGradeComponent,
    AcademicCoursesComponent,
    AcademicSubjectsComponent,
    AcademicBatchesComponent,
    AcademicClassRoomsComponent,
    AddGradeBoardComponent,
    AcademicHomeComponent
  ],
  imports: [
    SetupNewUIModule,
    CommonModule,
    UIModule,
    NewAcademicRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    TooltipModule.forRoot(),
    AlertModule.forRoot(),
    AngularMultiSelectModule,
    NgSelectModule,
    NgxQRCodeModule,
    NgbModule
  ],
  exports: [AddGradeBoardComponent],
  providers: [SetupComponent]
})
export class NewAcademicModule { }
