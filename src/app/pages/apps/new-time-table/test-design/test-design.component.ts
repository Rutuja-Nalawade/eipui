import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';

@Component({
  selector: 'app-test-design',
  templateUrl: './test-design.component.html',
  styleUrls: ['./test-design.component.scss']
})
export class TestDesignComponent implements OnInit {
  isopenScreen30: boolean = false;
  isViewHistory: boolean = false;
  substituteForm: FormGroup;
  submitted = false;
  isConflicts = false;
  isViewConflict = false;
  subjectList = [];
  teacherList = [];
  classroomList = [];
  viewConflictData = { typeName: "Classroom" }
  childType = 1;
  isHistoryActionView = false;
  isRemoveLecture: boolean = false;
  isSubtitutePopup: boolean = false;
  viewChangesList = [];
  @Input() type: any;
  @Input() dayUniqueId: any;
  @Input() typeChild: any;
  @Input() viewData: any;
  @Output() closeView = new EventEmitter<string>();
  @Output() actionPopupView = new EventEmitter<string>();

  //vinayak
  @Input() showData: any;
  @Output() closeSubtituteLecture = new EventEmitter<object>();
  @Output() isBlur = new EventEmitter<boolean>();
  @Input() objDate: any;
  @Input() timetableId: any;
  @Input() timetableName: any;
  typeRemove: any;
  dataForSubstitution: any;
  dataForShow: any;
  isDelete: boolean;
  isDeleteLecture: boolean;
  initialDetails: any;
  editLectureData: any;
  lectureType: any;
  createNewLectureDate: any;
  isAddNew: boolean;
  constructor(
    private toastService: ToastsupportService,
    private timeTableService: TimeTableServiceService,
    private router: Router,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe,
  ) { }

  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['type'] && changes['type'].currentValue == 7) {
      this.viewAllChanges()

    }

    if (changes['type'] && changes['type'].currentValue == 10) {
      this.viewLectureDetails(this.showData)

    }
    this.dataForSubstitution = this.showData;
    console.log(this.dataForSubstitution);

  }

  openscreen30() {
    this.isopenScreen30 = true;
  }

  closePopup() {
    this.isHistoryActionView = false;
    this.typeChild = 1;
  }
  close() {
    if (this.isHistoryActionView) {
      this.isHistoryActionView = false;
    } else {
      this.closeView.emit("");
    }
  }

  openscreen38() {
    this.isViewHistory = true;
  }
  restoreReschedule() {
    if (this.childType == 1) {
      this.closeView.emit("");
    } else {
      this.closeView.emit("1");
    }
  }


  cancelSubstitute() {
    if (this.type == 11) {
      this.closeSubtituteLecture.emit({ param1: 'reactivateLecture', param2: '' });

    } else {

      this.closeSubtituteLecture.emit({ param1: 'false', param2: '' });
    }
  }

  viewConflict(conflictType) {
    this.viewConflictData.typeName = "Classroom";
    this.isViewConflict = true;
  }
  closeConflict(event) {
    this.isViewConflict = false;
  }

  deleteAction(type) {
    if (type == 1) {
      this.closeView.emit("");
    } else {
      this.childType = 1;
    }
  }
  actionView(type) {
    this.viewData['type'] = type;
    this.viewData['orgDayUniqueId'] = this.dayUniqueId;
    console.log(this.viewData, this.dayUniqueId);
    this.actionPopupView.emit(JSON.stringify(this.viewData));
  }
  cancelLectrAction(type) {
    if (type == 1) {
      this.childType = 2;
    } else {
      this.closeView.emit("");
    }
  }
  activateLecture(type) {
    this.closeView.emit("");
  }
  actionHistroy(type, data) {
    console.log("data", data)
    switch (type) {
      case 1:
        this.type = 0;
        this.dataForShow = data;
        this.closeSubtituteLecture.emit({ param1: 'subtitutelecture', param2: this.dataForShow });
        break;
      case 2:
        this.isDelete = true;
        this.isDeleteLecture = true;

        break;
      case 3:
        this.typeChild = 1;
        this.dataForShow = data;
        this.viewLectureDetails(data);
        this.isHistoryActionView = true;
        break;
    }

  }

  viewHistoryAction(type) {
    if (type == 1) {
      this.closeSubtituteLecture.emit({ param1: 'false', param2: '' });
    } else {
      this.publishedChanges()

    }
  }


  closeSubtitution(event) {
    this.closeSubtituteLecture.emit({ param1: 'false', param2: '' });
  }


  //vinayak




  removeAction() {
    if (this.typeRemove == 2) {
      this.cancelLecture();
    } else if (this.typeRemove == 1) {
      this.isRemoveLecture = false;
      this.closeSubtituteLecture.emit({ param1: 'false', param2: '' });
    }
  }

  removeLecture(type) {
    this.typeRemove = type;

    this.isRemoveLecture = true;
  }
  deleteCancel() {
    this.isDeleteLecture = false;
    this.isRemoveLecture = false;
    this.isDelete = false;
  }

  removeCancel() {
    this.isRemoveLecture = false;
  }

  cancelLecture() {
    let teachers = [];
    this.dataForSubstitution.teacherIds.forEach(element => {
      teachers.push(element.uniqueId)
    });

    const obj = {
      classroomUniqueId: this.dataForSubstitution.classRoomId,
      courseUniqueId: this.dataForSubstitution.courseId,
      lectureDate: this.datePipe.transform(this.dataForSubstitution.substituteDate, 'yyyy-MM-dd'),
      subjectUniqueId: this.dataForSubstitution.subjectId,
      teacherUniqueIdList: teachers,
      timetableUniqueId: this.timetableId,
      substitutionType: 4,
      cardHasLessonUniqueId: this.dataForSubstitution.uniqueId,
      substitutionUniqueId: this.dataForSubstitution.substitutionId
    }

    console.log("obj", obj);
    this.cancelNdSubstituteLecture(obj)
  }

  saveSubstituteForm() {
    let teachers = [];
    teachers.push(this.substituteForm.value.teacherId);

    const obj = {
      classroomUniqueId: this.substituteForm.value.classroomId,
      courseUniqueId: this.dataForSubstitution.courseId,
      lectureDate: this.datePipe.transform(this.dataForSubstitution.substituteDate, 'yyyy-MM-dd'),
      subjectUniqueId: this.substituteForm.value.subjectId,
      teacherUniqueIdList: teachers,
      timetableUniqueId: this.timetableId,
      substitutionType: 2,
      cardHasLessonUniqueId: this.dataForSubstitution.uniqueId,
      substitutionUniqueId: this.dataForSubstitution.substitutionUniqueId
    }
    this.cancelNdSubstituteLecture(obj)
    console.log("obj", obj);



  }

  cancelNdSubstituteLecture(obj) {
    this.spinner.show();
    return this.timeTableService.addExtraLeture(obj).subscribe(
      res => {
        this.isRemoveLecture = false;

        this.closeSubtituteLecture.emit({ param1: 'cancellecture', param2: '' });

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  viewAllChanges() {
    this.spinner.show();
    this.timeTableService.getViewAllChanges(this.objDate).subscribe(
      response => {
        console.log('viewChanges', response)
        if (response.success == true) {
          this.viewChangesList = response.list;

        } else {
          this.toastService.ShowWarning("Substitutions Changes not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }

  publishedChanges() {
    let obj = [];
    this.viewChangesList.forEach(element => {
      obj.push(element.substitutionId);
    });
    this.spinner.show();
    this.timeTableService.publishedChanges(obj).subscribe(
      response => {
        console.log('viewChanges', response)
        if (response.success == true) {
          this.toastService.showSuccess(response.message)
          this.router.navigate(['/newModule/timeTable/lists/timeTable']);
        } else {
          this.toastService.ShowWarning("Something went wrong")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }

  viewLectureDetails(data) {
    let obj = {
      lectureDate: data.substituteDate,
      substitutionId: data.substitutionId
    }
    this.spinner.show();
    return this.timeTableService.getInitialSubstitutionLecture(obj).subscribe(
      res => {
        console.log("res", res)
        this.initialDetails = res;
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }

  removeRescheduleLecture() {
    this.spinner.show();
    const obj = {}
    return this.timeTableService.deleteLecture(this.dataForShow.substitutionId).subscribe(
      res => {
        this.viewAllChanges()
        this.isHistoryActionView = false;
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }



  rescheduleLecture() {
    console.log("test")
    this.closeSubtituteLecture.emit({ param1: 'reschedulelecture', param2: this.initialDetails });

  }
  activateLectur() {
    this.spinner.show();
    const obj = {}
    return this.timeTableService.reActiveLecture(this.dataForSubstitution.substitutionId).subscribe(
      res => {
        if (res && res.success) {
          this.toastService.showSuccess(res.responseMessage);
          this.cancelSubstitute();
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        console.log("res", res)
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
        this.spinner.hide();
      });
  }
}
