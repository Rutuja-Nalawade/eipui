import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import Swal from 'sweetalert2';
import { GuardianFormValidatorPipe } from '../guardian-form-validator.pipe';

@Component({
  selector: 'app-guardian',
  templateUrl: './guardian.component.html',
  styleUrls: ['./guardian.component.scss']
})
export class GuardianComponent implements OnInit {

  phone: any = ['123456789', '234567891', '345678912'];
  bloodGroup: any = ['O+ (O Positive)', 'A+ (A Positive)', 'O- (O Negative)', 'A- (A Negative)'];
  relations: any = [{ id: 1, name: 'Mother' }, { id: 2, name: 'Father' }, { id: 3, name: 'Guardian' }];
  guardianList: any = [];
  addGuardianList: any = [];
  isGuardian = true;
  submitted = false;
  isAssignParent = false;
  searchText: string;
  // guardianForm:FormGroup;
  ParentInfoDto: FormGroup;
  ParentInfo: FormGroup;
  guardianForms = new Array<FormGroup>();
  studentBasicInfoForm: FormGroup;
  guardianIndex: number;
  userData: any;
  currentUserId: Number = null;
  currentGuardianTab = 0
  allGuardiansList = new Array<any>();
  alreadyExist: boolean = false;
  editFormId = 0
  isEditForm = false;
  imgSrc = "";
  createProfileFile;
  selectFileForEditUser;
  url;
  fileSelected = false;
  selectedFiles: FileList;
  currentFileUpload: File;
  imageLoaded: boolean = false;
  form: FormGroup;
  imageError: string;
  newSelected = false;
  roleData: any = {}
  @ViewChild('guardianDrop', { static: true }) guardianDrop: NgbDropdown;
  @Input() public userId: Number;
  @Input() public userUniqueId: Number;
  @Input() public currentTab: Number;
  constructor(private formBuilder: FormBuilder, private _commonService: NewCommonService, private spinner: NgxSpinnerService, private toastService: ToastsupportService, private parentFormValidatorService: GuardianFormValidatorPipe, private _userService: UserServiceService, private _DomSanitizationService: DomSanitizer) {
    this.userData = this._commonService.getUserData();

  }

  ngOnInit() {
    this.currentUserId = this.userId;
    this.form = this.formBuilder.group({
      file: [null]
    })
    //  this.initParentInfo();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['currentTab'] && changes['currentTab'].currentValue == 3) {
      this.currentUserId = this.userId;
      this.getRoleInfo();
      // this.initGuardianForms();
      this.guardianList = [];
      this.addGuardianList = [];
      this.getGuardian(this.userId);
      this.isGuardian = true;
      this.isAssignParent = false;
      this.isEditForm = false
      window.scroll(0, 0);
      // this.initParentInfo();
    }
  }
  initParentInfo() {
    this.ParentInfoDto = this.formBuilder.group({
      rows: this.formBuilder.array([this.formBuilder.group({
        studentId: [this.currentUserId, Validators.required],
        parentId: ['', Validators.required],
        createdOrUpdatedById: [this.userData.uniqueId],
        profileImage: [null, Validators.nullValidator],
        relation: [null, Validators.nullValidator],
        generalInfo: this.getGeneralInfo(),

      })])
    });

  }
  initStudentBasicInfoForm() {
    this.studentBasicInfoForm = this.formBuilder.group({
      userUniqueId: ['', Validators.nullValidator],
      organizationUniqueId: ['', Validators.required],
      enrollmentNumber: ['', Validators.required],
      enrollmentDate: [null],
      createdOrUpdatedByUniqueId: [sessionStorage.getItem('uniqueId'), Validators.nullValidator],
      profileImage: ['', Validators.nullValidator],
      generalInfo: this.getGeneralInfo(),
      userId: [this.currentUserId],
    })
  }
  getGeneralInfo() {
    return this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      lastName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      gender: ['', Validators.nullValidator],
      // bloodGroup: ['', Validators.nullValidator],
      emailId: ['', Validators.compose([Validators.required, Validators.email])],
      // birthDate: [null, Validators.nullValidator],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      // adharNumber: ['', Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      // loginId: ['', Validators.required],
      dateOfJoining: [null, Validators.nullValidator]
    });
  }
  onAssignParent(item: String) {
    if (item == 'guardian') {
      this.isGuardian = true;
      this.isAssignParent = false;
    } else if (item == 'parent') {
      this.isGuardian = false;
      this.isAssignParent = true;
    }
  }
  onAddParent() {
    this.editFormId = 0
    if (this.guardianForms.length <= 2) {
      this.editFormId = this.editFormId + this.guardianForms.length
      this.initGuardianForms();
      this.isGuardian = true;
      this.isAssignParent = false;
      this.isEditForm = true;
    }
  }
  onGuardianChange(index) {
    this.currentGuardianTab = index
    this.isGuardian = true;
    this.isAssignParent = false;
  }
  initGuardianForms() {
    this.guardianForms.push(this.initGuardianInfoForm());
  }

  initGuardianInfoForm() {
    return this.formBuilder.group({
      uniqueId: [this.generateRandomKey()],
      studentUniqueId: ["", Validators.nullValidator],
      relationUniqueId: ["", Validators.nullValidator],
      organizationUniqueId: ['', Validators.nullValidator],
      parentUniqueId: ['', Validators.nullValidator],
      createdOrUpdatedByUniqueId: [this.userData.uniqueId, Validators.nullValidator],
      generalInfo: this.initGuardianGeneralInfoForm(),
      studentId: [this.currentUserId],
      // organizationId: [this.userData.organizationId.toString()],
      parentId: [null],
      profileImage: [""],
      isNew: true,
      isAssignedGuardian: false,
      isShowVewMore: false,
    });
  }
  generateRandomKey() {
    let randomkey = Math.floor((Math.random() * 10000) + 1);
    return randomkey;
  }

  initGuardianGeneralInfoForm() {
    return this.formBuilder.group({
      firstName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{2,45}')])],
      lastName: ['', Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{2,45}')])],
      birthDate: [null, Validators.nullValidator],
      gender: ['', Validators.nullValidator],
      bloodGroup: ['', Validators.nullValidator],
      adharNumber: ['', Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern("^[0-9]{10}$")])],
      emailId: ['', [Validators.required, Validators.email]],
      // loginId: ['', Validators.required],
      relation: ['', Validators.required],
      fullName: [""],
      relationName: [""],
    })
  }
  addGuardian(gardian) {
    if (this.addGuardianList.length !== 0) {
      if (this.addGuardianList.length <= 2) {
        const found = this.addGuardianList.some(el => el.uniqueId === gardian.uniqueId);

        if (!found) {
          gardian.relation = 1
          gardian.isNew = true
          this.addGuardianList.push(gardian);
          this.patchGardianInfo(gardian);
          this.guardianList = [];
          (<HTMLInputElement>document.getElementById("dropdownBasic1")).value = "";
        } else {
          this.toastService.ShowWarning("Already assigned")

        }
      }
    } else if (this.addGuardianList.length === 0) {
      gardian.relation = 1
      gardian.isNew = true
      this.addGuardianList.push(gardian);
      this.patchGardianInfo(gardian)
      this.guardianList = [];
    }
  }

  getGuardian(userId) {
    // let userId="114";
    this.spinner.show();
    this._userService.getGuardians("student", this.userUniqueId).subscribe(
      response => {
        this.allGuardiansList = response;
        // this.studentAddressInfo = response;
        console.log(response);
        // this.patchStudentAddressInfo(this.studentAddressInfo);
        if (response && response.length > 0) {
          // this.patchGardianDtoInfo(response)
          this.patchAllGuardians(response);
        } else {
          this.guardianForms = [];
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      }
    );
  }
  // get f() { return this.guardianForms.controls; }

  saveGuardian(index: any) {
    this.submitted = true;
    let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
    if (gaurdianInfoForm.invalid) {
      this.scrollToError();
      return;
    }
    // gaurdianInfoForm.value.profileImage=this.imgSrc;
    gaurdianInfoForm.controls['profileImage'].setValue(this.imgSrc);
    // console.log(gaurdianInfoForm);return;
    this.spinner.show();
    if (gaurdianInfoForm.value.isAssignedGuardian) {
      let data = gaurdianInfoForm.value.generalInfo;
      let dto = {
        "fullName": data.fullName,
        "uniqueId": gaurdianInfoForm.value.uniqueId,
        "relation": data.relation
      }
      if (this.fileSelected) {
        this.spinner.show();
        // this.basicFormGroup.controls['profileImage'].setValue(this.imageSrc);
        var formData: any = new FormData();
        formData.append("file", this.form.get('file').value);
        this._userService.fileUpload("guardian", formData, gaurdianInfoForm.value.uniqueId).subscribe(
          response => {
            if (response) {
              this.imgSrc = "";
              this.fileSelected = false
              this.newSelected = false
              this.getImage(gaurdianInfoForm.value.uniqueId, index, false)
            }
            this.toastService.showSuccess("Image upload successfully.");
            // this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            // } else {
            //   this.toastService.ShowError(error);
            // }
            // this.spinner.hide();
          });
      }
      this._userService.saveGuardian("student", dto, this.userUniqueId).subscribe(
        response => {
          this.toastService.showSuccess("Guardian saved successfully");
          this.spinner.hide();
          this.getGuardian(this.userId);
          this.isEditForm = false;
          this.submitted = false;
          this.imgSrc = ""
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   console.log(error);
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        }
      );
    } else {
      let data = gaurdianInfoForm.value.generalInfo;
      let dto = {
        "firstName": data.firstName,
        "lastName": data.lastName,
        "emailId": data.emailId,
        "mobile": data.phoneNumber,
        "aadhaarNumber": data.adharNumber,
        "bloodGroup": data.bloodGroup,
        "dob": data.birthDate,
        "gender": data.gender,
        "relation": data.relation,
        "roleIds": [this.roleData.role.uniqueId]
      }
      // if (this.fileSelected) {
      //   this.spinner.show();
      //   // this.basicFormGroup.controls['profileImage'].setValue(this.imageSrc);
      //   var formData: any = new FormData();
      //   formData.append("file", this.form.get('file').value);
      //   this._userService.fileUpload("guardian", formData, this.userUniqueId).subscribe(
      //     response => {
      //       if (response) {
      //         this.imgSrc = "";
      //         this.fileSelected = false
      //         this.newSelected = false
      //         // this.getBasicInfo(this.selectedUserUniqueId)
      //       }
      //       this.toastService.showSuccess("Image upload successfully.");
      //       // this.spinner.hide();
      //     },
      //     (error: HttpErrorResponse) => {
      //       if (error instanceof HttpErrorResponse) {
      //       } else {
      //         this.toastService.ShowError(error);
      //       }
      //       // this.spinner.hide();
      //     });
      // }
      this._userService.createGuardian("student", dto, this.userUniqueId).subscribe(
        response => {
          this.toastService.showSuccess("Guardian saved successfully");
          this.spinner.hide();
          this.getGuardian(this.userId);
          this.isEditForm = false;
          this.submitted = false;
          this.fileSelected = false;
          this.newSelected = false;
          this.imgSrc = ""
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        }
      );
    }
  }

  findRecords(value: any) {
    let data = {
      val: 'searchBar',
      searchDetails: value
    }
    if (value != null && value != undefined && value.length > 3) {
      // this.loading = true;
      this.searchUsers(value);
    } else {
      if (value.length == 0 || value == undefined) {
        //  alert();
        this.guardianList = [];
      }
    }
  }

  getGuardianRecords(value: any) {
    if (value != null && value != undefined && value.length > 2) {
      this.spinner.show();
      let searchDto = {
        'orgList': [this.userData.organizationId],
        'searchKey': value,
        'searchType': 4,
      };
      // this._learnersService.searchUsersBySearchKey(searchDto)
      //   .subscribe(data => {
      //     if (data != null && data.length > 0) {
      //       this.guardianList = data;
      //     } else {
      //       this.guardianList = [];
      //     }

      //     this.spinner.hide();
      //   }, (error: HttpErrorResponse) => {
      //     if (error instanceof HttpErrorResponse) {

      //     } else {
      //       this.toastService.ShowError(error);
      //     }
      //     this.spinner.hide();
      //   });
    } else {
      if (value.length == 0 || value == undefined) {
        //  alert();
        this.guardianList = [];
      }
    }
  }
  searchUsers(value) {
    this._userService.searchUsers("guardian", value)
      .subscribe(data => {
        if (data != null && data.length > 0) {
          // this.totalCount=data.length;
          this.guardianList = data;
        } else {
          this.guardianList = [];
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  async patchGardianInfo(data) {
    let generalInfo = {}
    this.patchParentBasicInfo(data, this.editFormId);
    // this._learnersService.getStudentBasicInfo(data.id).subscribe(
    //   response => {
    //     if (response.generalInfo) {
    //       let parentsBasicData = response;
    //       parentsBasicData['uniqueId'] = data.uniqueId;
    //       this.patchParentBasicInfo(parentsBasicData, this.editFormId);
    //       generalInfo = response.generalInfo;

    //     } else {
    //       let parentsBasicData = response;
    //       parentsBasicData['uniqueId'] = data.uniqueId;
    //       this.patchParentBasicInfo(parentsBasicData, this.editFormId);
    //     }
    //   })

  }
  patchParentBasicInfo(parentsInfo: any, formIndex: number) {
    let formGroup = <FormGroup>this.guardianForms[formIndex];
    formGroup.controls['uniqueId'].setValue(parentsInfo.uniqueId);
    formGroup.controls['studentUniqueId'].setValue(parentsInfo.studentUniqueId);
    formGroup.controls['organizationUniqueId'].setValue(parentsInfo.organizationUniqueId);
    formGroup.controls['parentId'].setValue(parentsInfo.userId);
    formGroup.controls['studentId'].setValue(this.currentUserId);
    formGroup.controls['isNew'].setValue(true);
    formGroup.controls['isAssignedGuardian'].setValue(true);
    formGroup.controls['profileImage'].setValue(parentsInfo.profileImage);
    let generalInfoFormGroup = <FormGroup>formGroup.controls['generalInfo'];
    var res = parentsInfo.fullName.split(" ");
    // generalInfoFormGroup.controls['loginId'].setValue(parentsInfo.generalInfo.loginId);
    generalInfoFormGroup.controls['relation'].setValue(parentsInfo.relation != null ? parentsInfo.relation : null);
    generalInfoFormGroup.controls['firstName'].setValue(res[0]);
    generalInfoFormGroup.controls['lastName'].setValue(res[1]);
    // generalInfoFormGroup.controls['gender'].setValue(parentsInfo.generalInfo.gender);
    // generalInfoFormGroup.controls['bloodGroup'].setValue(parentsInfo.generalInfo.bloodGroup);
    // generalInfoFormGroup.controls['adharNumber'].setValue(parentsInfo.generalInfo.adharNumber);
    // generalInfoFormGroup.controls['phoneNumber'].setValue(parentsInfo.generalInfo.phoneNumber);
    generalInfoFormGroup.controls['fullName'].setValue(parentsInfo.fullName);
    // generalInfoFormGroup.controls['emailId'].setValue(parentsInfo.generalInfo.emailId);
    this.getUserEmail("guardian", parentsInfo.uniqueId, 2, formIndex);
    this.getUserMobile("guardian", parentsInfo.uniqueId, 2, formIndex);
    this.getUserAddress("guardian", parentsInfo.uniqueId, 2, formIndex);
    this.getImage(parentsInfo.uniqueId, formIndex, true);
  }

  changeRelation(item, event, index) {
    item.relation = event.target.value
    let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
    let generalInfo = gaurdianInfoForm.value.generalInfo;
    generalInfo.relation = event.target.value;
  }
  deleteGuardian(parent, index) {
    Swal.fire({
      text: 'Are you sure want to delete Guardian?',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        if (parent.isNew) {
          // this.addGuardianList.removeAt(index)
          this.addGuardianList.splice(index, 1);
          // this.guardianForms.removeAt(index)
          this.guardianForms = this.guardianForms.filter(x => x.controls['uniqueId'].value != parent.uniqueId);
        } else {
          let StudentLinkToParentDto = {
            parentId: parent.parentId,
            createdOrUpdatedById: "",
            studentList: [{
              uniqueId: "",
              studentId: this.currentUserId,
              name: "",
              course: "",
              courseUniqueId: "",
              standard: "",
              division: "",
              divisionUniqueId: "",

              profileImageId: "",
              relation: ""

            }]
          }
          // let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
          this.spinner.show();
          this._userService.deleteGuardian("student", parent.relationUniqueId).subscribe(
            response => {
              if (response) {
                // this.addGuardianList.removeAt(index)
                this.addGuardianList.splice(index, 1);
                this.guardianForms = this.guardianForms.filter(x => x.controls['uniqueId'].value != parent.uniqueId);
                this.toastService.showSuccess("Guardian delete successfully");
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // if (error instanceof HttpErrorResponse) {

              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();

            }
          );

        }
      }
    })

  }
  patchAllGuardians(allGuardiansList: Array<any>) {
    this.guardianForms = new Array<FormGroup>();
    this.guardianIndex = 0;
    for (let i = 0; i < allGuardiansList.length; i++) {
      this.getUserEmail("guardian", allGuardiansList[i].uniqueId, 2, i);
      this.getUserMobile("guardian", allGuardiansList[i].uniqueId, 2, i);
      this.getUserAddress("guardian", allGuardiansList[i].uniqueId, 2, i);
      let guardianForm = this.formBuilder.group({
        uniqueId: allGuardiansList[i].uniqueId,
        relationUniqueId: allGuardiansList[i].relationUniqueId,
        studentUniqueId: allGuardiansList[i].studentUniqueId != null ? allGuardiansList[i].studentUniqueId : "",
        organizationUniqueId: allGuardiansList[i].organizationUniqueId,
        parentId: allGuardiansList[i].parentId,
        createdOrUpdatedByUniqueId: this.userData.uniqueId,
        generalInfo: this.patchGuardianGeneralInfo(allGuardiansList[i]),
        studentId: [this.currentUserId],
        isNew: false,
        isShowVewMore: false,
        isAssignedGuardian: true,
        // organizationId: [this.userData.organizationId],
        profileImage: allGuardiansList[i].profileImage
      });
      this.guardianForms.push(guardianForm);
      this.getImage(allGuardiansList[i].uniqueId, i, false);
      let relation = (allGuardiansList[i].relation == null) ? "" : allGuardiansList[i].relation
      this.addGuardianList.push({ "uniqueId": allGuardiansList[i].uniqueId, "id": allGuardiansList[i].parentId, "name": allGuardiansList[i].fullName, "type": null, "typeName": null, "imageCode": allGuardiansList[i].profileImage, "orgId": null, "orgName": "", "roleIdList": [], "className": null, "roleName": "", "subjectList": [], relation: relation })
    }
  }

  patchGuardianGeneralInfo(generalInfo: any) {
    let relationName = ""
    this.relations.map(item => {
      if (item.id == generalInfo.relation) {
        relationName = item.name;
        return;
      }
    })
    var res = generalInfo.fullName.split(" ");
    return this.formBuilder.group({
      firstName: [res[0], Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      lastName: [res[1], Validators.compose([Validators.required, Validators.pattern('[A-Za-z]{3,45}')])],
      birthDate: [generalInfo.birthDate, Validators.nullValidator],
      gender: [generalInfo.gender, Validators.nullValidator],
      bloodGroup: [generalInfo.bloodGroup, Validators.nullValidator],
      adharNumber: [generalInfo.adharNumber, Validators.compose([Validators.pattern(/^(\d{12}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      phoneNumber: [generalInfo.phoneNumber, Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])],
      emailId: [generalInfo.emailId, Validators.required],
      // loginId: [generalInfo.loginId, Validators.required],
      relation: [generalInfo.relation, Validators.required],
      fullName: [generalInfo.fullName],
      relationName: [relationName],
    });
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    // console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }
  guardianFormValidator(index: number): FormGroup['controls'] {
    return this.parentFormValidatorService.validateParentForm(index, this.guardianForms);
  }
  onChange(value, tabId) {

    this.alreadyExist = false;

    let valueid = value.id
    if (tabId == 1 && valueid != 3 || tabId == 2 && valueid != 3) {

      //console.log("Function called ---- " + value)

      this.allGuardiansList.forEach(Guardiankey => {

        //console.log("relation id present " + Guardiankey.generalInfo.relation);

        if (Guardiankey.generalInfo.relation == valueid) {
          this.toastService.ShowError("Relation already present");
          this.alreadyExist = true;
        }
      })
    }


  }
  showHideViewMore(item) {
    item.value.isShowVewMore = !item.value.isShowVewMore
  }
  close() {
    this._commonService.iscloseClicked.next(true)
  }
  closeForm() {
    this.isEditForm = false;
    let gaurdianInfoForm = <FormGroup>this.guardianForms[this.editFormId];
    this.imgSrc = ""
    if (gaurdianInfoForm.value.isNew == true) {
      this.guardianForms.splice(this.editFormId, 1);
      this.addGuardianList.splice(this.editFormId, 1);
    }
    // this.getGuardian(this.userId);

  }
  openFile() {
    // $('#imguploadFile').trigger('click');
    document.getElementById("imguploadFile").click();
  }

  onSelectFile(event) {
    this.selectedFiles = event.target.files;
    this.createProfileFile = event.target.files[0];

    this.selectFileForEditUser = true;
    var file = event.target.files[0];
    if (!file) return;
    const max_size = 149715200;
    var pattern = /image-*/;

    var reader = new FileReader();

    if (!file.type.match(pattern)) {
      this.toastService.ShowWarning('invalid format');
      return;
    }
    if (event.target.files[0].size > max_size) {// Checking height * width}
      // if (e.target.files[0].size > 200)
      // alert("helloooo")
      this.imageError =
        'Maximum size allowed is ' + (max_size / 1000) / 1000 + 'Mb';
      return false;

    } else {
      const file = (event.target as HTMLInputElement).files[0];
      this.form.patchValue({
        file: file
      });
      this.fileSelected = true;
      this.newSelected = true;
      this.form.get('file').updateValueAndValidity();
      this._handleReaderLoaded(event);
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);

    }
  }
  _handleReaderLoaded(e) {
    var reader = e.target;
    this.imgSrc = reader.result;
    this.currentFileUpload = this.selectedFiles[0];
    var formData: any = new FormData();
    formData.append("file", this.form.get('file').value);

  }
  editGuardian(index) {
    this.isEditForm = true;
    this.editFormId = index
    this.imgSrc = this.guardianForms[index].value.profileImage;
  }
  getUserEmail(userType, uniqueId, type, index) {
    this.spinner.show();
    return this._userService.getUserEmailId(userType, uniqueId).subscribe(
      response => {
        let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
        let generalInfo = <FormGroup>gaurdianInfoForm.controls.generalInfo;
        generalInfo.controls.emailId.setValue(response.emailId);
        this.spinner.hide();

      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  getUserMobile(userType, uniqueId, type, index) {
    this.spinner.show();
    this._userService.getUserMobile(userType, uniqueId).subscribe(
      response => {
        let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
        let generalInfo = <FormGroup>gaurdianInfoForm.controls.generalInfo;
        generalInfo.controls.phoneNumber.setValue(response.mobile);
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  getUserAddress(usertype, uniqueId, type, index) {
    if (uniqueId != null && uniqueId != undefined) {
      this.spinner.show();
      this._userService.getUserOtherInfo(usertype, uniqueId).subscribe(
        response => {

          let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
          let generalInfo = <FormGroup>gaurdianInfoForm.controls.generalInfo;
          generalInfo.controls.bloodGroup.setValue(response.bloodGroup);
          generalInfo.controls.birthDate.setValue(response.birthDate);
          generalInfo.controls.adharNumber.setValue(response.adharNumber);
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }
  }
  getImage(uniqueId, index, type) {
    this._userService.getImage("guardian", uniqueId, false).subscribe(
      response => {
        this.imgSrc = "";
        if (response != null && response) {
          if (response.image) {
            let gaurdianInfoForm = <FormGroup>this.guardianForms[index];
            gaurdianInfoForm.controls.profileImage.setValue(response.image);
            this.imgSrc = (type) ? response.image : "";
          }
        }
      });
  }
  getRoleInfo() {
    this._userService.getUserRolesData("parent")
      .subscribe(data => {
        if (data) {
          this.roleData = data;
        }
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
  onClickedOutside() {
    console.log("object");
    this.guardianDrop.close();
  }
}
