import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsyllabusComponent } from './examsyllabus.component';

describe('ExamsyllabusComponent', () => {
  let component: ExamsyllabusComponent;
  let fixture: ComponentFixture<ExamsyllabusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsyllabusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsyllabusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
