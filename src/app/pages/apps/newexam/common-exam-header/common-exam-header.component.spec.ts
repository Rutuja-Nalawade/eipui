import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonExamHeaderComponent } from './common-exam-header.component';

describe('CommonExamHeaderComponent', () => {
  let component: CommonExamHeaderComponent;
  let fixture: ComponentFixture<CommonExamHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonExamHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonExamHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
