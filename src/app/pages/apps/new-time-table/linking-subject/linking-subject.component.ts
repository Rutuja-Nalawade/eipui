import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { CommonDto } from 'src/app/pages/dashboards/dashboard3/dashboar3.model';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { TimeTableServiceService } from '../../../../servicefiles/time-table-service.service';

@Component({
  selector: 'app-linking-subject',
  templateUrl: './linking-subject.component.html',
  styleUrls: ['./linking-subject.component.scss']
})
export class LinkingSubjectComponent implements OnInit {

  subjectLinkingSubjectForm: FormGroup;
  isAdd = false;
  submitted = false;
  organizationSubject: any = [];
  optionalSubjectList: any = [];
  reMainingSubjectList: any = [];
  selectedSubjectUids: any = [];
  selecteedCourseUniqueId: any;
  currentEditRowIndex: any;
  oldRow: any;
  @Input() linkingSubjectList: any;
  @Output() close = new EventEmitter<string>()
  constructor(private formBuilder: FormBuilder, private subjectServiceService: SubjectServiceService, private courseServiceService: CourseServiceService, private toastService: ToastsupportService, private timeTableService: TimeTableServiceService, private spinner: NgxSpinnerService) {
    this.initLinkSubjectForm();
    this.getAllAcademicSubject();
  }

  ngOnInit() {

  }
  initLinkSubjectForm() {
    this.subjectLinkingSubjectForm = this.formBuilder.group({
      linkedSubjectList: this.formBuilder.array([])
    });
  }
  getCourseLinkingSubject() {
    this.spinner.show();
    this.timeTableService.getLinkSubject(false).subscribe(
      res => {
        if (res.success) {
          if (res.list && res.list.length > 0) {
            this.patchSubjectData(res.list);
          }
          // this.masterCourseList = res;
          // console.log('masterCourseList' + res)
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }
  patchSubjectData(subjectData) {

    let courseLinkedGroup = [];
    for (let i = 0; i < subjectData.length; i++) {
      this.selectedSubjectUids = [];
      let SubjectByCourseFormGroup = {
        compulsorySubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData[i].compulsorySubjects)),
        optionalSubjectList: this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(subjectData[i].optionalSubjects)),
        electiveSubjectList: this.formBuilder.array(this.patchElectiveSubjects(subjectData[i].electiveGroupsAndSubjects)),
        courseId: [subjectData[i].course.uniqueId, Validators.required],
        courseName: [subjectData[i].course.name, Validators.nullValidator],
        editMode: false,
        subjectList: this.formBuilder.array(this.patchRemainingSubjects([]))
      };
      console.log(this.organizationSubject);
      let subjectList = this.getchRemainingSubjects(this.organizationSubject);
      // this.organizationSubject.map(element => {

      SubjectByCourseFormGroup.subjectList = this.formBuilder.array(this.patchRemainingSubjects(subjectList));

      courseLinkedGroup.push(this.formBuilder.group(SubjectByCourseFormGroup));
    }


    this.subjectLinkingSubjectForm.setControl('linkedSubjectList', this.formBuilder.array(courseLinkedGroup));

    console.log("value subject", this.subjectLinkingSubjectForm)

    console.log('patchSubjectData' + JSON.stringify(subjectData))
  }
  getchRemainingSubjects(data) {
    let subjectList = [];
    console.log(data);
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      if (!this.selectedSubjectUids.includes(element.uniqueId)) {
        subjectList.push(element);
      }
    }
    return subjectList;
  }
  patchCompulsoryOrOptionalRemainingSubjects(subjectList: Array<any>) {
    let subjectsToPatch = [];
    if (subjectList != null && subjectList.length > 0) {
      for (let i = 0; i < subjectList.length; i++) {
        let subjectGroup = this.formBuilder.group({
          subjectId: subjectList[i].uniqueId,
          name: subjectList[i].name,
          isLinked: subjectList[i].isSelected,
          uniqueId: subjectList[i].uniqueId,
          courseHasSubjectUniqueId: subjectList[i].courseHasSubjectUniqueId
        });
        this.selectedSubjectUids.push(subjectList[i].uniqueId);
        subjectsToPatch.push(subjectGroup);
      }
    }
    return subjectsToPatch;
  }

  patchRemainingSubjects(subjectList: Array<any>) {
    console.log("patchRemainingSubjects", subjectList);
    let subjectsToPatch = [];
    if (subjectList != null && subjectList.length > 0) {
      for (let i = 0; i < subjectList.length; i++) {
        let subjectGroup = this.formBuilder.group({
          subjectId: subjectList[i].subjectId,
          name: subjectList[i].name,
          isLinked: subjectList[i].isLinked,
          uniqueId: subjectList[i].uniqueId,
          id: subjectList[i].id
        });
        subjectsToPatch.push(subjectGroup);

      }
    }
    return subjectsToPatch;
  }
  patchElectiveSubjects(electiveSubjects: Array<any>) {
    let electiveSubjectListToPatch = [];
    if (electiveSubjects != null && electiveSubjects.length > 0) {
      for (let i = 0; i < electiveSubjects.length; i++) {
        let electiveSubjectDto = this.formBuilder.group({
          groupId: electiveSubjects[i].uniqueId,
          groupName: electiveSubjects[i].name,
          uniqueId: electiveSubjects[i].uniqueId,
          electiveSubjectList: this.formBuilder.array(this.patchGroupSubjects(electiveSubjects[i].electiveSubjects))
        });
        electiveSubjectListToPatch.push(electiveSubjectDto);
      }
    }
    return electiveSubjectListToPatch;
  }

  patchGroupSubjects(electiveSubjects: Array<any>) {
    let electiveSubjectList = [];
    if (electiveSubjects != null && electiveSubjects.length > 0) {
      for (let i = 0; i < electiveSubjects.length; i++) {
        let subject = this.formBuilder.group({
          id: electiveSubjects[i].id,
          subjectId: electiveSubjects[i].subjectId,
          name: [electiveSubjects[i].name, Validators.required],
          isLinked: electiveSubjects[i].isLinked,
          uniqueId: electiveSubjects[i].uniqueId,
          courseHasSubjectUniqueId: electiveSubjects[i].courseHasSubjectUniqueId
        });
        this.selectedSubjectUids.push(electiveSubjects[i].uniqueId);
        electiveSubjectList.push(subject);
      }
    }
    return electiveSubjectList;
  }
  dropSubjectFromList(event: CdkDragDrop<string[]>, link: any, index: number, type: number, electiveIndex: number, course: FormGroup) {

    let item = event.item;
    console.log("item", item)
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log("moveItemInArray", moveItemInArray);
    } else {

      let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
      console.log("courseFormArray", courseFormArray)
      let courseForm = <FormGroup>courseFormArray.controls[index];
      console.log("courseForm", courseForm)
      let array = event.container.data;
      console.log(array);
      let subjectArray = null;

      switch (type) {
        case 1:
          subjectArray = <FormArray>courseForm.controls['compulsorySubjectList'];
          break;
        case 2:
          subjectArray = <FormArray>courseForm.controls['optionalSubjectList'];
          break;
        case 3:
          subjectArray = <FormArray>courseForm.controls['electiveSubjectList'];
          break;
      }

      let subjectFormGroup = this.formBuilder.group({

        id: [item.data.id, Validators.nullValidator],
        name: [item.data.name, Validators.nullValidator],
        uniqueId: [item.data.uniqueId, Validators.nullValidator],
        subjectId: [item.data.uniqueId, Validators.nullValidator],
        isLinked: [item.data.isLinked, Validators.nullValidator]
      });

      console.log("subjectFormGroup", subjectFormGroup)

      if (electiveIndex == null || electiveIndex == undefined) {
        subjectArray.push(subjectFormGroup);
      } else {

        let electiveSubjectArray = <FormGroup>subjectArray.controls[electiveIndex];
        let electiveSubjects = <FormArray>electiveSubjectArray.controls['electiveSubjectList'];
        electiveSubjects.push(subjectFormGroup);
      }

      let subjectMainList = <FormArray>courseForm.controls['subjectList'];
      let subjectList = subjectMainList.value;
      let subjectObject = item.data;
      let compObject: CommonDto = subjectObject;
      subjectList = subjectList.filter(x => x.uniqueId != compObject.uniqueId);
      courseForm.setControl('subjectList', this.formBuilder.array(subjectList));
    }
  }
  removeSubjectFromList(event: CdkDragDrop<string[]>, parentIndex: number, childIndex: number, type: number, item, electiveSubjectIndex: number) {
    let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];

    console.log(item);
    console.log("item to delete");
    console.log(item);
    console.log(courseFormArray.value);
    let courseForm = <FormGroup>courseFormArray.controls[parentIndex];
    // let courseForm = this.newMethod(courseFormArray, parentIndex);

    console.log(courseForm);

    let removeSubjectArray = null;

    switch (type) {
      case 1: removeSubjectArray = <FormArray>courseForm.controls['compulsorySubjectList'];
        break;
      case 2: removeSubjectArray = <FormArray>courseForm.controls['optionalSubjectList'];
        break;
      case 3: removeSubjectArray = <FormArray>courseForm.controls['electiveSubjectList'];
        break;
    }

    if (electiveSubjectIndex == null || electiveSubjectIndex == undefined) {
      removeSubjectArray.removeAt(childIndex);
    } else {
      console.log(removeSubjectArray);
      let electiveSubjectFormGroup = <FormGroup>removeSubjectArray.controls[childIndex];
      console.log("when deleting");
      console.log(electiveSubjectFormGroup.value);
      let electiveSubjectListArray = <FormArray>electiveSubjectFormGroup.controls['electiveSubjectList'];
      electiveSubjectListArray.removeAt(electiveSubjectIndex);
    }

    let subjectArray = <FormArray>courseForm.controls['subjectList'];

    let subjectFormGroup = this.formBuilder.group({
      id: [item.id, Validators.nullValidator],
      name: [item.name, Validators.nullValidator],
      subjectId: [item.subjectId, Validators.nullValidator],
      uniqueId: [item.uniqueId, Validators.nullValidator],
      isLinked: false
    });
    subjectArray.push(subjectFormGroup);
  }
  updateRow(row) {
    this.submitted = true;
    console.log(row)
    console.log(this.subjectLinkingSubjectForm)
    // if (this.subjectLinkingSubjectForm.invalid) {
    //   return;
    // }
    this.submitted = false;
    row.controls.editMode.value = false;

  }
  deleteRow(row, index) {

  }
  onSubmit() {
    if (this.subjectLinkingSubjectForm.invalid) {
      return;
    }
    this.spinner.show();
    let submitDataList = this.subjectLinkingSubjectForm.controls['linkedSubjectList'].value;
    let valid: any = true;
    submitDataList.forEach(element => {
      if (element.subjectList.length > 0) {
        valid = false;
        return;
      }
    });
    if (valid == false) {
      this.toastService.ShowWarning("Assign all remaining subject to course, then save.");
      this.spinner.hide();
      return;
    }

    let submitArray = [];
    submitDataList.forEach(submitData => {

      let courseDto = {
        course: submitData.courseName,
        uniqueId: submitData.courseId
      }
      let compulsorySubjects = [];
      submitData.compulsorySubjectList.forEach(com => {
        let dto = {
          uniqueId: com.uniqueId,
          name: com.name,
          isSelected: true,
          courseHasSubjectUniqueId: ""
        }
        compulsorySubjects.push(dto)
      });
      let optionalSubjects = [];
      submitData.optionalSubjectList.forEach(opt => {
        let dto = {
          uniqueId: opt.uniqueId,
          name: opt.name,
          isSelected: true,
          courseHasSubjectUniqueId: ""
        }
        optionalSubjects.push(dto)
      });

      let remainingSubjects = [];
      submitData.subjectList.forEach(rem => {
        let dto = {
          uniqueId: rem.uniqueId,
          name: rem.name,
          isSelected: false,
          courseHasSubjectUniqueId: ""
        }
        remainingSubjects.push(dto)
      });

      let electiveGroupsAndSubjects = [];
      submitData.electiveSubjectList.forEach(ele => {

        let electiveSubjects = [];
        ele.electiveSubjectList.forEach(els => {
          let dto = {
            uniqueId: els.uniqueId,
            name: els.name,
            isSelected: true,
            courseHasSubjectUniqueId: ""
          }
          electiveSubjects.push(dto)
        });

        let dto = {
          name: ele.groupName,
          uniqueId: ele.uniqueId,
          electiveSubjects: electiveSubjects
        }
        electiveGroupsAndSubjects.push(dto)
      });

      let dto = {
        course: courseDto,
        compulsorySubjects: compulsorySubjects,
        optionalSubjects: optionalSubjects,
        remainingSubjects: remainingSubjects,
        electiveGroupsAndSubjects: electiveGroupsAndSubjects,
      }

      submitArray.push(dto)
    });
    console.log('submit subject data => ', submitArray)

    this.timeTableService.addLinkSubject(submitArray).subscribe(
      response => {
        this.toastService.showSuccess("Subjects linked successfully");
        this.spinner.hide();
        this.cancelBtn(submitArray)
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );
  }
  cancelBtn(value) {
    // this.newItemEvent.emit(value);
    let submitDataList = this.subjectLinkingSubjectForm.controls['linkedSubjectList'].value;
    let valid: any = true;
    submitDataList.forEach(element => {
      if (element.subjectList.length > 0) {
        console.log("valid1");
        valid = false;
      }
    });
    this.submitted = false;
    if (valid == false) {
      console.log("valid");
      this.close.emit("false");
    } else {
      this.close.emit(value);
    }
  }
  cancel(value) {
    this.close.emit("false");
  }
  deleteCourseHasSubject() {
    let submitData = this.subjectLinkingSubjectForm.controls['linkedSubjectList'].value[0]
    console.log('delete ', JSON.stringify(submitData));
    // this.loading = true
    this.subjectServiceService.deleteCourseHasSubject(submitData.courseId).subscribe(
      response => {
        this.spinner.hide();
        this.toastService.showSuccess("Subjects delete dsuccessfully");
        this.cancelBtn(false)
        // stepper.next();
        this.submitted = false;
        this.cancelBtn(false);
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );

  }
  editRow(row, index) {
    console.log("this.currentEditRowIndex", this.currentEditRowIndex);
    this.submitted = false;
    // if (this.currentEditRowIndex || this.currentEditRowIndex == 0) {
    //   let controlArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];
    //   let rowGroup = <FormGroup>controlArray.controls[this.currentEditRowIndex];
    //   console.log("rowGroup", rowGroup);
    //   rowGroup.controls.subjectList = this.formBuilder.array(this.patchRemainingSubjects(this.oldRow.subjectList));
    //   rowGroup.controls.compulsorySubjectList = this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(this.oldRow.compulsorySubjectList));
    //   rowGroup.controls.optionalSubjectList = this.formBuilder.array(this.patchCompulsoryOrOptionalRemainingSubjects(this.oldRow.optionalSubjectList));
    //   rowGroup.controls.electiveSubjectList = this.formBuilder.array(this.patchElectiveSubjects(this.oldRow.electiveSubjectList));
    //   //rowGroup.setValue(this.oldRow)
    //   rowGroup.controls['editMode'].setValue(false);
    //   rowGroup.controls['compulsorySubjectList'].setValue(this.oldRow.compulsorySubjectList);
    //   rowGroup.controls['optionalSubjectList'].setValue(this.oldRow.optionalSubjectList);
    //   rowGroup.controls['electiveSubjectList'].setValue(this.oldRow.electiveSubjectList);
    //   rowGroup.controls['subjectList'].setValue(this.oldRow.subjectList);
    // }
    this.currentEditRowIndex = index;
    row.controls.editMode.value = true;
    this.oldRow = row.value;
  }
  removeElectiveSubjectGroup(parentIndex: number, childIndex: number, electiveSubjectIndex: number) {
    let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];

    let courseForm = <FormGroup>courseFormArray.controls[parentIndex];

    let removeSubjectArray = <FormArray>courseForm.controls['electiveSubjectList']
    console.log("removeSubjectArray", removeSubjectArray);
    let group = <FormGroup>removeSubjectArray.controls[childIndex];

    let groupUniqueId = group.controls['uniqueId'].value;
    console.log("groupUniqueId", groupUniqueId)
    if (groupUniqueId != "") {
      this.spinner.show();
      this.subjectServiceService.deleteElectiveGroup(groupUniqueId).subscribe(
        response => {
          this.spinner.hide();
          this.toastService.showSuccess("elective group deleted successfully");
          removeSubjectArray.removeAt(childIndex);
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   this.spinner.hide();
          // } else {
          //   this.spinner.hide();
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        }
      );
    } else {
      removeSubjectArray.removeAt(childIndex);
    }


  }

  deleteElectiveGroup(groupUniqueId) {

  }
  createElectiveGroup(index: number) {
    let courseFormArray = <FormArray>this.subjectLinkingSubjectForm.controls['linkedSubjectList'];

    let courseForm = <FormGroup>courseFormArray.controls[index];

    let electiveSubjectArray = <FormArray>courseForm.controls['electiveSubjectList'];

    let electiveSubjectGroup = this.formBuilder.group({
      groupName: ['', Validators.required],
      uniqueId: ['', Validators.nullValidator],
      electiveSubjectList: this.formBuilder.array([])
    });

    electiveSubjectArray.push(electiveSubjectGroup);
  }
  async getAllAcademicSubject() {
    this.spinner.show();
    return this.subjectServiceService.getSubjectList().subscribe(
      async res => {
        console.log("Client-side error occured.", res);
        if (res && res.success == true) {

          this.organizationSubject = res.list;
          console.log(this.organizationSubject);
          await this.patchSubjectData(this.linkingSubjectList);
        } else {
          this.toastService.ShowWarning('subject not found')
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
        this.getCourseLinkingSubject();
      });
  }
}
