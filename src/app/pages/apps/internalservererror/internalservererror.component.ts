import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-internalservererror',
  templateUrl: './internalservererror.component.html',
  styleUrls: ['./internalservererror.component.scss']
})
export class InternalservererrorComponent implements OnInit {
  servercode: any;
  header: any ='We are sorry, Temporarily unavailable!';
  longMessage: any = 'Loading data failed please contact to administrator.'
  constructor(private activatedroute:ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.activatedroute.params.subscribe( params => this.servercode = params.errorcode );
  this.setErrors(this.servercode)
  }

  setErrors(error){
   
    switch(error){
      case 404:
        this.servercode= error;
        this.header='We are sorry, Temporarily unavailable!';
        this.longMessage= 'Loading data failed please contact to administrator.'
        break;
      case 502:
        this.servercode= error;
        this.header='We are sorry, Temporarily unavailable!';
        this.longMessage= 'Loading data failed please contact to administrator.'
        break
      case 500:
        this.servercode= error;
        this.header='We are sorry, Temporarily unavailable!';
        this.longMessage= 'Loading data failed please contact to administrator.'
        break
    }
  }

  goBackTOHomePage(){
    this._router.navigateByUrl('/dashboards/dashboard-2')
  }
}
