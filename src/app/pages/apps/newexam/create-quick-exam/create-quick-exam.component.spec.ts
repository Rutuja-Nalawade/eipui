import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateQuickExamComponent } from './create-quick-exam.component';

describe('CreateQuickExamComponent', () => {
  let component: CreateQuickExamComponent;
  let fixture: ComponentFixture<CreateQuickExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateQuickExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateQuickExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
