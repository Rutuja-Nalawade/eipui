export class UserOtpDetailsDto {
    phoneNumber: string;
    userId: string;
    macAddress: string;
    otp: string;
    orgCode: string;

    constructor() {
    }
}