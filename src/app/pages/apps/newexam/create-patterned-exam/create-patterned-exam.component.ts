import { Component, OnInit, ViewChild } from '@angular/core';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExamService } from '../exam.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-create-patterned-exam',
  templateUrl: './create-patterned-exam.component.html',
  styleUrls: ['./create-patterned-exam.component.scss']
})
export class CreatePatternedExamComponent implements OnInit {
  createPatternExamForm: FormGroup;
  testTypeForm: FormGroup;
  toggle: Boolean = false;
  testTypeList: any[];
  testTypeSubmitted: Boolean= false;
  courseId: any;
  examPatternList = [];
  examPatternType: any;
  patternList = [];
  courseList: any[];
  batchList: any[];
  students: any[];
  subjectsList: any[];
  examPattern: any;
  pattern1: any;
  pattern2: any;
  coursePlanner = [];
  isFirstNext:boolean =false;
  patternType=[{id:'1',name:'NEET Pattern'},{id:'2',name:'JEE Mains Pattern'},{id:'3',name:'JEE Advanced Pattern'},{id:'4',name:'Custom Pattern'}]
  @ViewChild('wizardForm', { static: false }) wizard: BaseWizardComponent;
  selectedSubjects: any;
  exam: any;
  courseName: string;
  examAssignmentTypeName: string;
  batchName = [];

  constructor(private fb:FormBuilder,
              private toastService:ToastsupportService,
              private spinner: NgxSpinnerService,
              private examService: ExamService,
              private router:Router ,
              private modalService:NgbModal,) { }

  ngOnInit() {
    this.initExamForm();
    this.initTestTypeForm();
    this.getTestTypeList();

    this.examService.subjectList.subscribe(
      (value: any) => {
        this.subjectsList = value;
      });

    this.examService.courses.subscribe(
      (value: any) => {
        this.courseList = value;
      });

    this.examService.batches.subscribe(
      (value: any) => {
        this.batchList = value;
      });

    this.examService.studentList.subscribe(
      (value: any) => {
        this.students = value;
      });
  }

  initExamForm() {
    this.createPatternExamForm = this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.required],
      examType: [2, Validators.required],
      examAssignmentTypeId: ['', Validators.required],
      pattern1TimingInfo: this.initTimeInfo(),
      pattern2TimingInfo: this.initTimeInfo(),
      batchInfo: this.initBatchInfo(),
      examSyllabus: this.initExamSubject(),
      passingMarks: ['', Validators.required],
      studentType: ['', Validators.nullValidator],
      studentList: this.fb.array([], Validators.nullValidator),
      patternType: ['', Validators.required],
      pattern1Uid: ['', Validators.nullValidator],
      pattern1Name: ['', Validators.nullValidator],
      pattern1Marks: ['', Validators.nullValidator],
      pattern1Duration: ['', Validators.nullValidator],
      pattern2Uid: ['', Validators.nullValidator],
      pattern2Name: ['', Validators.nullValidator],
      pattern2Marks: ['', Validators.nullValidator],
      pattern2Duration: ['', Validators.nullValidator],
      isPatternVisible: [false, Validators.required],
      // isObjective: [false, Validators.required],
      isPattern2Added: [false, Validators.required],
      isFromMobile: [false, Validators.required],
      organizationId: [sessionStorage.getItem('organizationId'), Validators.nullValidator],
      createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.nullValidator]
    });
  }

  initTestType() {
    return this.fb.group({
      id: ['', Validators.nullValidator],
      name: ['', Validators.required]
    });
  }

  initExamSubject() {
    return this.fb.group({
      subjects: this.fb.array([], Validators.required)
    })
  }

  initTimeInfo() {
    return this.fb.group({
      testDate: ['', Validators.required],
      testTime: ['', Validators.required],
      extraTime: ['', Validators.nullValidator],
    })
  }

  initBatchInfo() {
    return this.fb.group({
      courseId: ['', Validators.required],
      batchIds: [[], Validators.nullValidator],
    })
  }

  initTestTypeForm() {
    this.testTypeForm = this.fb.group({
      organizationId: [sessionStorage.getItem('organizationId'), Validators.nullValidator],
      createdUpdatedBy: [sessionStorage.getItem('userId'), Validators.nullValidator],
      isAssignment: [false, Validators.required],
      examAssignmentTypeList: this.fb.array([this.initTestType()], Validators.nullValidator)
    });
  }

  togleType() {
    this.toggle = !this.toggle;
  }

  checkExists(value: string, index: number) {
    if (this.testTypeList != null && this.testTypeList.length > 0) {
      let controlArray = <FormArray>this.testTypeForm.controls['examAssignmentTypeList'];
      for (let type of this.testTypeList) {
        if (type.name.toUpperCase() == value.toUpperCase()) {
          this.toastService.ShowWarning("Name already added");
          let formGroup = <FormGroup>controlArray.controls[index];
          formGroup.controls['name'].setValue('');
          return false;
        }
      }
    }
  }

  omit_special_char(event: any) {
    let k = event.charCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45); // (k >= 48 && k <= 57) : allow numbers
  }

  addtestType() {
    this.testTypeSubmitted=true;
    if(this.testTypeForm.invalid){
      return;
    }
    let examAssignmentType = this.testTypeForm.value;
    if (examAssignmentType.examAssignmentTypeList != null && examAssignmentType.examAssignmentTypeList.length > 0) {
      for (let i = 0; i < examAssignmentType.examAssignmentTypeList.length; i++) {
        if (this.is_special_char(examAssignmentType.examAssignmentTypeList[i].name)) {
          this.toastService.ShowWarning("Special charachters are not allowed..");
          return false;
        }
      }
      // this.spinner = true;
      this.spinner.show()
      return this.examService.addTestType(examAssignmentType).subscribe(
        res => {
          this.testTypeList = [];
          this.testTypeList = res;
          if (res.length>0) {
            res.map(type=>{
              if (type.name==examAssignmentType.examAssignmentTypeList[0].name) {
                this.selectedTestType=type.id
              }
            })
          }
          this.resetTestType();
          this.toggle = false;
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
          // } else {
          //   // this.spinner = false;
          //   this.spinner.hide();
          //   this.toastService.ShowError(error.error.errorMessage);
          // }
          this.spinner.hide();
        });
    }
  }

  selectedTestType="";
  testTypeChange(event){
    this.selectedTestType=event.id;
    console.log("event",event);
    
  }

  resetTestType() {
    const controlArray = <FormArray>this.testTypeForm.controls['examAssignmentTypeList'];
    for (let i = controlArray.length - 1; i >= 0; i--) {
      controlArray.removeAt(i);
    }
    controlArray.push(this.initTestType());
  }

  is_special_char(value: string) {
    let pattern = new RegExp(/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/);
    return pattern.test(value);
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  formSubmit() {
    this.wizard.navigation.goToNextStep();
  }

  getTestTypeList() {
    this.spinner.show();
    return this.examService.getTestTypeList(sessionStorage.getItem('organizationId'), false).subscribe(
      res => {
        console.log("res", res)
        if (res) {
          this.testTypeList = [];
          this.testTypeList = res;
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error.error.errorMessage);
        //   // this._commonService.ShowError(error.error.errorMessage);
        // }
        this.spinner.hide();
      })
  }

  wizardFirstNext() {
    this.isFirstNext=true;
    this.createPatternExamForm.setControl('studentList', this.fb.array(this.students));
    console.log("this.createPatternExamForm.",this.createPatternExamForm.value)
    let batchDetails = <FormGroup>this.createPatternExamForm.controls['batchInfo'];
    this.courseId = batchDetails.controls['courseId'].value;
    let exam = this.createPatternExamForm.value;
    if (exam.name == "" && exam.marks == "" && exam.examAssignmentTypeId == "" && exam.passingMarks == "" && exam.batchInfo.courseId == "") {
      this.toastService.ShowWarning("Please fill all values..");
      return false;
    }
    let studentArray = <FormArray>this.createPatternExamForm.controls['studentList'];
    if (studentArray.length == 0) {
    this.toastService.ShowWarning("Please select atleast one student");
    return false;
    }
  }

  getExamPatternList(patternType: any) {

    console.log("patternType",patternType)
    if (patternType != null && patternType != "") {
      this.spinner.show()
      this.createPatternExamForm.controls['patternType'].setValue(patternType.id);
      this.examPatternList = [];
      this.examPatternType = patternType.id;
      return this.examService.getExamPatternList(patternType.id, sessionStorage.getItem('organizationId')).subscribe(
        res => {
          if (res) {
            this.examPatternList = res;
            this.spinner.hide();
            
          }
        },
        (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.spinner.hide();
          //   this.toastService.ShowWarning(error.error.errorMessage);
          // }
          this.spinner.hide();
        });
    }
  }

  

  getExamPatternDetails(examPatternUid: string) {
    console.log("examPatternUid",examPatternUid)
    this.spinner.show()
    if (examPatternUid != null) {
      // let examPattern = this.examPatternList.find(x => x.uniqueId == examPatternUid);
      return this.examService.getExamPatternFromEip(examPatternUid, this.examPatternType).subscribe(
        res => {
          console.log("res",res)
          if (res) {
            this.examPattern = res;
            this.pattern1 = this.examPattern.examPatterns[0];
            this.pattern2 = this.examPattern.examPatterns[1];
            console.log("this.pattern1",this.pattern1)
            console.log("this.pattern2",this.pattern2)
          }
          this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
            
          // } else {
          //   this.toastService.ShowError(error.error.errorMessage);
          // }

          this.spinner.hide()
        }
      );
    }

  }

  wizardSecondtNext() {
    if (this.examPattern == undefined || this.examPattern == '') {
      this.toastService.ShowWarning("Please select exam pattern")
      return false;
    }
    let examSyllabus = <FormGroup>this.createPatternExamForm.controls['examSyllabus'];
    let subjectArrayControls = <FormArray>examSyllabus.controls['subjects'];
    subjectArrayControls.controls = [];
    for (let i = 0; i < this.pattern1.subjects.length; i++) {
      subjectArrayControls.push(this.initSubjects(this.pattern1.subjects[i]));
    }
    this.selectedSubjects = this.pattern1.subjects;
    console.log("selectedSubjects",this.selectedSubjects)
    this.addActualSubjects();
    this.patchadvancePatternValue();
    console.log("this.pattern2",this.pattern2)
    console.log("2 value",this.createPatternExamForm.value)
  }

  initSubjects(subject: any) {
    return this.fb.group({
      id: [subject.id, Validators.required],
      uniqueId: [subject.subjectId, Validators.nullValidator],
      subjectName: [subject.name, Validators.required],
      remarks: ['', Validators.nullValidator],
      examAssignmentPatternSubjectId: ['', Validators.nullValidator],
      actualSubjects: this.fb.array([], Validators.nullValidator),
      actualSyllabus: this.fb.array([], Validators.nullValidator),
      actualCoursePlanners: this.fb.array([], Validators.nullValidator),
      syllabus: this.fb.array([], Validators.required),
    });
  }

  addActualSubjects() {
    let examSyllabus = <FormGroup>this.createPatternExamForm.controls['examSyllabus'];
    const subjectArray = <FormArray>examSyllabus.controls['subjects'];
    // for (let i = 0; i < subjectArray.length; i++) {
    //   let subjectGroup = <FormGroup>subjectArray.controls[i];
    //   let actualSubjectArray = <FormArray>subjectGroup.controls['actualSubjects'];
    //   actualSubjectArray.controls = [];
    //   for (let subject of this.selectedSubjects) {
    //     actualSubjectArray.push(this.addSubjectToActualSubjects(subject));
    //   }
    // }

    for (let i=0; i<subjectArray.length; i++) {
     
     
      let subjectGroup = <FormGroup>subjectArray.controls[i];
     
      let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
        this.getCoursePlannerBySubject(subjectGroup.value['id'],actualCPArray);
     
    }
  }

  public getCoursePlannerBySubject(subjectId: number, cpArray: FormArray) {
    console.log("couse api")
    this.coursePlanner = [];
    return this.examService.getCoursePlannerList(subjectId,this.courseId).subscribe(
      res => {
        if (res) {
          this.coursePlanner = res;
          cpArray.controls = [];
          
            for (let cp of res) {
              cpArray.push(this.addCoursePlanner(cp, false));
              console.log("cpArray",cpArray.length)
              
            }

            for(let j=0; j<cpArray.length; j++){
              
                let coursePlannerGroup = <FormGroup>cpArray.controls[j];
                console.log("coursePlannerGroup",coursePlannerGroup)
                console.log("subjectGroup.value['id']",coursePlannerGroup.value['id'])
              }
              console.log("1 value",this.createPatternExamForm.value)
        }


      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   // this.toasterService.ShowError(error.error.errorMessage);
        // }
      });
  }

  addCoursePlanner(data: any, isSelected: boolean) {
    console.log("couse group")
    return this.fb.group({
      id: data.id,
      name: data.name,
      isSelected: isSelected,
      syllabusCoursewise:this.fb.array([], Validators.nullValidator),
      selectedsyllabusCoursewise:this.fb.array([], Validators.nullValidator),
    });
  }

  patchadvancePatternValue() {
    this.createPatternExamForm.controls['isPattern2Added'].setValue(false);
    this.createPatternExamForm.controls['pattern1Uid'].setValue(this.pattern1.uniqueId);
    this.createPatternExamForm.controls['pattern1Marks'].setValue(this.pattern1.examMarks);
    this.createPatternExamForm.controls['pattern1Name'].setValue(this.examPattern.name);
    this.createPatternExamForm.controls['pattern1Duration'].setValue(this.pattern1.duration);

    if (this.createPatternExamForm.controls['patternType'].value == 3 && this.pattern2 != undefined) {
      this.createPatternExamForm.controls['pattern1Name'].setValue(this.pattern1.patternName);
      this.createPatternExamForm.controls['pattern2Uid'].setValue(this.pattern2.uniqueId);
      this.createPatternExamForm.controls['pattern2Marks'].setValue(this.pattern2.examMarks);
      this.createPatternExamForm.controls['pattern2Name'].setValue(this.pattern2.patternName);
      this.createPatternExamForm.controls['pattern2Duration'].setValue(this.pattern2.duration);
      this.createPatternExamForm.controls['isPattern2Added'].setValue(true);
    }
  }

  secondPrev(){
    this.createPatternExamForm.controls['isPattern2Added'].setValue(false);
    this.createPatternExamForm.controls['pattern1Uid'].setValue('');
    this.createPatternExamForm.controls['pattern1Marks'].setValue('');
    this.createPatternExamForm.controls['pattern1Name'].setValue('');
    this.createPatternExamForm.controls['pattern1Duration'].setValue('');

    if (this.createPatternExamForm.controls['patternType'].value == 3 && this.pattern2 != undefined) {
      this.createPatternExamForm.controls['pattern1Name'].setValue('');
      this.createPatternExamForm.controls['pattern2Uid'].setValue('');
      this.createPatternExamForm.controls['pattern2Marks'].setValue('');
      this.createPatternExamForm.controls['pattern2Name'].setValue('');
      this.createPatternExamForm.controls['pattern2Duration'].setValue('');
      this.createPatternExamForm.controls['isPattern2Added'].setValue(false);
    }

    let pattern1TimingInfo = <FormGroup>this.createPatternExamForm.controls['pattern1TimingInfo'];
    pattern1TimingInfo.controls['testDate'].setValue('');
    pattern1TimingInfo.controls['testTime'].setValue('');
    pattern1TimingInfo.controls['extraTime'].setValue('');

    let pattern2TimingInfo = <FormGroup>this.createPatternExamForm.controls['pattern2TimingInfo'];
    pattern2TimingInfo.controls['testDate'].setValue('');
    pattern2TimingInfo.controls['testTime'].setValue('');
    pattern2TimingInfo.controls['extraTime'].setValue('');

  }

  wizardThirdNext() {
    if (this.createPatternExamForm.controls['isPattern2Added'].value == true) {
      let timeDetails1 = <FormGroup>this.createPatternExamForm.controls['pattern1TimingInfo'];
      let timeDetails2 = <FormGroup>this.createPatternExamForm.controls['pattern2TimingInfo'];
      let exam1Time = this.addMinutes(timeDetails1.controls['testTime'].value, this.createPatternExamForm.controls['pattern1Duration'].value);
      let exam2Time = this.addMinutes(timeDetails2.controls['testTime'].value, 0);
      if ((Date.parse(timeDetails1.controls['testDate'].value) == Date.parse(timeDetails2.controls['testDate'].value) && exam1Time > exam2Time)) {
        this.toastService.ShowWarning("Paper 2 time must be greater then paper-1 by " + (this.createPatternExamForm.controls['pattern1Duration'].value + 20) + "minutes.");
        return false;
      }
    }
    let syllabusGroup = <FormGroup>this.createPatternExamForm.controls['examSyllabus'];
    let subjectArray = <FormArray>syllabusGroup.controls['subjects'];
    for (let i = 0; i < subjectArray.length; i++) {
      let subjectGroup = <FormGroup>subjectArray.controls[i];
      if (subjectGroup.controls['id'].value == '') {
        this.toastService.ShowWarning("Please select subject");
        return false;
      }
      let syllabusArray = <FormArray>subjectGroup.controls['syllabus'];

     
     
        let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];

        for (let j=0; j<actualCPArray.length; j++) {
         
          let selectedsyllabusCoursewise = <FormGroup>actualCPArray.controls[j];
          let selectedsyllabus = <FormArray>selectedsyllabusCoursewise.controls['selectedsyllabusCoursewise'];
          for (let k=0; k<selectedsyllabus.length; k++) {
           
            console.log("selectedsyllabus",selectedsyllabus.controls[k].value);
            syllabusArray.push(new FormControl(selectedsyllabus.controls[k].value));
          }
        
       
      }
      console.log("this.createPatternExamForm.",this.createPatternExamForm.value);
     
    }
    // for (let i = 0; i < subjectArray.length; i++) {
    //   let subjectGroup = <FormGroup>subjectArray.controls[i];
    //   let syllabusArray = <FormArray>subjectGroup.controls['syllabus'];
    //   if (subjectGroup.controls['id'].value == '') {
    //     this.toastService.ShowWarning("Please select subject");
    //     return false;
    //   }
    //   if (syllabusArray.length == 0) {
    //     this.toastService.ShowWarning("Please select syllabus");
    //     return false;
    //   }

    // }
    this.lastNext();
  }

  addMinutes(date:any, minutes: number) {
    let now = new Date(date)
    return new Date(now.getTime() + minutes * 60000);
  }

  lastNext() {
    this.exam = this.createPatternExamForm.value;

    let course = this.courseList.find(x => x.id == this.exam.batchInfo.courseId);
    this.courseName = course.name;

    let examAssignmentType = this.testTypeList.find(x => x.id == this.exam.examAssignmentTypeId);
    this.examAssignmentTypeName = examAssignmentType.name;

    if (this.exam.batchInfo.batchIds != '') {
      this.batchName = [];
      for (let i = 0; i < this.exam.batchInfo.batchIds.length; i++) {
        let batch = this.batchList.find(x => x.id == this.exam.batchInfo.batchIds[i]);
        this.batchName.push(batch.name);
      }
    }
  }

  openConfirm(content){
    this.modalService.open(content, { centered: true });
  }
  saveCreatePatternExam () {
    this.createPatternExamForm.controls['examAssignmentTypeId'].setValue(this.createPatternExamForm.value.examAssignmentTypeId.toString());
    this.spinner.show();
    return this.examService.addExam(this.createPatternExamForm.value).subscribe(
      res => {
        this.spinner.hide();
        this.router.navigate(['/newModule/exam/examList']);
        this.toastService.showSuccess("Exam Added Successfully..");
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error.error.errorMessage);
        // }
        this.spinner.hide();
      });
   
  }
 
  newCreatePattern(){
    this.router.navigate(['/newModule/exam/create-pattern'])
  }
}
