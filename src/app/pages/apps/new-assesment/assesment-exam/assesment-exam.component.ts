import { Component, OnInit, ViewChild, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AssesmentService } from '../../../../servicefiles/assesment.service';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'app-assesment-exam',
  templateUrl: './assesment-exam.component.html',
  styleUrls: ['./assesment-exam.component.scss']
})
export class AssesmentExamComponent implements OnInit {
  pageIndex: number = 0;
  examData: any;
  examList = [];
  defaultRows: any = '10';
  rowList: any = [{ id: '10', name: '10/Row' }, { id: '20', name: '20/Row' }, { id: '30', name: '30/Row' }]
  totalExams: any;
  isExamListFound: boolean = false;

  isAssesment: boolean = false;
  @ViewChild('wizardForm', { static: false }) wizard: BaseWizardComponent;
  quickExamForm: FormGroup;
  testTypeForm: FormGroup;
  testTypeList: any = [];
  toggle: Boolean = false;
  isFirstNext: boolean = false;
  testTypeSubmitted: Boolean = false;
  students = [];
  subjectsList: any[];
  courseId: number;
  coursePlanner = [];
  @Input() currentTab: any;
  assignmentType: boolean = false;
  assignmentTypeId: any;
  courseList: any = [];
  batchList: any = [];
  selectedStudentsList: any = [];
  viewSelectedStudentsList: any = [];
  remainingSelectedStudents: any;
  assignmentTypeIdSample: any = 1;
  urlStrings = ["", "assignment-category", "test-category", "exam-category"];
  examType = 0;
  isShowExamView = false;
  addPattern: boolean;
  isHideHeader = false;
  isCreateTrack = false;
  examUniqueId: any;
  questionCategories = [];
  isHidebuttons = false;
  @Output() hideHeader = new EventEmitter<string>();
  @Output() close = new EventEmitter<string>();
  constructor(private spinner: NgxSpinnerService,
    private fb: FormBuilder,
    private examService: ExamServiceService,
    private examService1: AssesmentService,
    private toastService: ToastsupportService,
    private modalService: NgbModal,
    private router: Router,
    private datePipe: DatePipe) { }

  ngOnInit() {

    this.examService.subjectList.subscribe(
      (value: any) => {
        this.subjectsList = value;
      });

    this.examService.studentList.subscribe(
      (value: any) => {
        this.students = value;
        console.log("value",value);
        // let box = document.querySelector('.box');
        // let width = box.clientWidth;
        // console.log("Width", width);
        this.selectedStudentsList = []
        this.selectedStudentsList = this.students;
        this.viewSelectedStudentsList = this.students.slice(0, 7);
        console.log("this.viewSelectedStudentsList",this.viewSelectedStudentsList);
        if (this.students.length > 7) {
          this.remainingSelectedStudents = this.students.length - 7;
        } else {
          this.remainingSelectedStudents = 0
        }
        let studentListArry = <FormArray>this.quickExamForm.controls['studentList'];
        studentListArry.controls = []
        for (let k = 0; k < this.students.length; k++) {
          studentListArry.push(new FormControl(this.students[k]));
        }
      });

    this.examService.courses.subscribe(
      (value: any) => {
        this.courseList = value;
      });

    this.examService.batches.subscribe(
      (value: any) => {
        this.batchList = value;
      });
  }
  public getExamList() {
    this.spinner.show()
    this.examList = [];
    // return this.examService1.getPaginationExamList(this.userData.orgUniqueId, this.pageIndex).subscribe(
    return this.examService1.getPaginationExamList(sessionStorage.getItem('organizationId')).subscribe(
      res => {
        console.log("res", res);
        if (res) {

          this.examData = res;
          this.examList = res['content'];
          this.totalExams = res['totalElements']
          this.isExamListFound = true;
          console.log("Exam" + JSON.stringify(this.examData));
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }

  pageChanged(page: number) {
    this.pageIndex = page - 1;
    // this.getExamList();
  }

  createAssesment() {
    this.hideHeader.emit("1");
    this.initExamForm();
    this.initTestTypeForm();
    this.getTestTypeList();
    this.isHideHeader = true;
    this.isAssesment = true;
  }

  initExamForm() {
    this.quickExamForm = this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.required],
      testTypeId: [null, Validators.required],
      // timingDetailsDto: this.initTimeInfo(),
      batchInfo: this.initBatchInfo(),
      // subjectList: this.fb.array([], Validators.required),
      // passingMarks: ['', Validators.required],
      studentList: this.fb.array([], Validators.required),
      isFromMobile: [false, Validators.required],
      isPublic: [false, Validators.required],
      isPatternVisible: [false, Validators.required],
      step: [0, Validators.nullValidator]
      // isObjective: [false, Validators.required],
    });
  }

  initTestTypeForm() {
    this.testTypeForm = this.fb.group({
      isAssignment: [false, Validators.required],
      examAssignmentTypeList: this.fb.array([this.initTestType()], Validators.nullValidator)
    });
  }
  initTestType() {
    return this.fb.group({
      id: ['', Validators.nullValidator],
      name: ['', Validators.required]
    });
  }
  initBatchInfo() {
    return this.fb.group({
      courseId: [null, Validators.required],
      courseName: ['', Validators.nullValidator],
      batchIds: [[], Validators.required],
    })
  }

  initTimeInfo(dto) {
    console.log(dto);
    if (dto != "" && dto != undefined && dto != null) {
      return this.fb.group({
        assessmentDate: [dto.controls.assessmentDate.value, Validators.required],
        assessmentTime: [dto.controls.assessmentTime.value, Validators.required],
        extraTime: [dto.controls.extraTime.value, Validators.nullValidator],
        duration: [dto.controls.duration.value, Validators.required],
      })
    } else {
      return this.fb.group({
        assessmentDate: ["", Validators.required],
        assessmentTime: ["", Validators.required],
        extraTime: [null, Validators.nullValidator],
        duration: ['', Validators.required],
      })
    }
  }
  initExamSubject() {
    return this.fb.group({
      subjects: ['', Validators.required],
      uniqueId: ['', Validators.required],
      syllabusIds: [[], Validators.nullValidator]
    })
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  getPassingMarks(value: number) {
    if (value > parseInt(this.quickExamForm.controls['maximumMarks'].value)) {
      this.quickExamForm.controls['passingMarks'].setValue('');
      // this._commonService.ShowWarning("Passing marks should be less than exam marks.");
    }
  }

  formSubmit() {
    console.log("this.assignmentType", this.assignmentType)
    if (this.assignmentType == true) {
      this.wizard.navigation.goToNextStep();

    }

  }
  omit_special_char(event: any) {
    let k = event.charCode;
    return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57) || k == 45); // (k >= 48 && k <= 57) : allow numbers
  }

  getTestTypeList() {
    this.spinner.show();
    let urlString = this.urlStrings[this.currentTab];
    return this.examService.getAssigmnentCategory(urlString).subscribe(
      res => {
        console.log("res", res)
        this.testTypeList = [];
        if (res && res.success) {
          this.testTypeList = res.list;
        } else {
          this.toastService.ShowWarning("TypeList not found");
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide();
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error);
        //   // this._commonService.ShowError(error.error.errorMessage);
        // }
      })
  }
  togleType(customType) {
    this.modalService.open(customType, { backdrop: 'static', windowClass: 'addTestType' });
  }
  addtestType() {
    this.testTypeSubmitted = true;
    if (this.testTypeForm.invalid) {
      return;
    }
    let typeNameList = [];
    let examAssignmentType = this.testTypeForm.value;
    if (examAssignmentType.examAssignmentTypeList != null && examAssignmentType.examAssignmentTypeList.length > 0) {
      for (let i = 0; i < examAssignmentType.examAssignmentTypeList.length; i++) {
        typeNameList.push(examAssignmentType.examAssignmentTypeList[i].name);
        if (this.is_special_char(examAssignmentType.examAssignmentTypeList[i].name)) {
          this.toastService.ShowWarning("Special charachters are not allowed..");
          return;
        }
      }
      // this.spinner = true;
      this.spinner.show()
      let urlString = this.urlStrings[this.currentTab];
      return this.examService.addAssigmnentCategory(typeNameList, urlString).subscribe(
        res => {
          this.testTypeList = [];
          if (res && res.success > 0) {
            this.toggle = false;
            this.modalService.dismissAll();
            this.resetTestType();
            this.getTestTypeList();
          } else {
            this.toastService.ShowWarning(res.responseMessage);
          }
          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide();
          // if (error.error instanceof Error) {
          //   console.log("Client-side error occured.");
          // } else {
          //   // this.spinner = false;
          //   this.spinner.hide();
          //   this.toastService.ShowError(error.error.errorMessage);
          // }
        });
    }
  }
  resetTestType() {
    const controlArray = <FormArray>this.testTypeForm.controls['examAssignmentTypeList'];
    for (let i = controlArray.length - 1; i >= 0; i--) {
      controlArray.removeAt(i);
    }
    controlArray.push(this.initTestType());
  }
  is_special_char(value: string) {
    let pattern = new RegExp(/[~`!#$%\^&*+=\\[\]\\';,/{}|\\":<>\?]/);
    return pattern.test(value);
  }

  selectedTestType = "";
  testTypeChange(event) {
    this.selectedTestType = event.id;
    console.log("event", event);

  }

  checkExists(value: string, index: number) {
    if (this.testTypeList != null && this.testTypeList.length > 0) {
      let controlArray = <FormArray>this.testTypeForm.controls['examAssignmentTypeList'];
      for (let type of this.testTypeList) {
        if (type.name.toUpperCase() == value.toUpperCase()) {
          this.toastService.ShowWarning("Name already added");
          let formGroup = <FormGroup>controlArray.controls[index];
          formGroup.controls['name'].setValue('');
          return;
        }
      }
    }
  }

  selectAssignment(id, did, type) {
    this.assignmentTypeIdSample = type
    let ele = document.getElementById(id);
    let element = document.getElementById(did);
    ele.classList.remove('assignmentTypeBox')
    ele.classList.add('assignmentTypeBoxActive')
    element.classList.remove('assignmentTypeBoxActive')
    element.classList.add('assignmentTypeBox')
  }
  wizardFirstNext(content) {
    this.isFirstNext = true;
    let timingDetailsDto = this.quickExamForm.controls.timingDetailsDto;
    console.log(timingDetailsDto);
    this.quickExamForm.removeControl('timingDetailsDto');
    console.log(this.quickExamForm);
    if (this.quickExamForm.invalid) {
      return;
    }
    if (this.selectedStudentsList.length == 0) {
      this.toastService.ShowWarning("please select students");
      return;
    }
    this.isFirstNext = false;
    this.quickExamForm.addControl('timingDetailsDto', this.initTimeInfo(timingDetailsDto));
    if (this.assignmentType == false) {
      this.modalService.open(content, { backdrop: 'static' });
    } else if (this.assignmentType == true) {
      console.log(this.quickExamForm);
      if (this.assignmentTypeId == 1) {
        this.quickExamForm.controls.step.setValue(3);
        this.wizard.navigation.goToStep(2)
      }
      if (this.assignmentTypeId == 2) {
        this.quickExamForm.controls.step.setValue(2);
        this.quickExamForm.addControl('patternUid', new FormControl('', Validators.required));
        this.quickExamForm.addControl('duration', new FormControl('', Validators.required));
        this.wizard.navigation.goToStep(1)
      }
      this.quickExamForm.addControl('subjectList', this.fb.array([], Validators.required));
      let batchDetails = <FormGroup>this.quickExamForm.controls['batchInfo'];
      this.courseId = batchDetails.controls['courseId'].value;
      let subjectArray = <FormArray>this.quickExamForm.controls['subjectList'];
      console.log("subjectArray", subjectArray, " subjectArray.length", subjectArray.length)
      for (let i = 0; i < subjectArray.length; i++) {


        let subjectGroup = <FormGroup>subjectArray.controls[i];

        let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
        console.log(actualCPArray);
        if (actualCPArray.value.length == 0) {
          this.getCoursePlannerBySubject(subjectGroup.value['uniqueId'], actualCPArray);
        }

      }

      console.log("this.students", this.students)
      let studentListArry = <FormArray>this.quickExamForm.controls['studentList'];
      studentListArry.controls = []
      for (let k = 0; k < this.students.length; k++) {
        studentListArry.push(new FormControl(this.students[k]));
      }
    }



    let exam = this.quickExamForm.value;

  }

  prevOptional() {
    this.quickExamForm.controls.step.setValue(0);
    if (this.assignmentTypeId == 1) {
      this.wizard.navigation.goToStep(0)
    }
  }
  wizardFirstNextPattern(content) {
    this.isFirstNext = true;
    if (this.assignmentType == false) {
      this.modalService.open(content, { backdrop: 'static' });
    } else {
      // this.createPatternExamForm.setControl('studentList', this.fb.array(this.students));
      // console.log("this.createPatternExamForm.",this.createPatternExamForm.value)
      // let batchDetails = <FormGroup>this.createPatternExamForm.controls['batchInfo'];
      // this.courseId = batchDetails.controls['courseId'].value;
      // let exam = this.createPatternExamForm.value;
      // if (exam.name == "" && exam.marks == "" && exam.examAssignmentTypeId == "" && exam.passingMarks == "" && exam.batchInfo.courseId == "") {
      //   this.toastService.ShowWarning("Please fill all values..");
      //   return false;
      // }
      // let studentArray = <FormArray>this.createPatternExamForm.controls['studentList'];
      // if (studentArray.length == 0) {
      // this.toastService.ShowWarning("Please select atleast one student");
      // return false;
      // }
    }

  }

  setAssingment() {
    console.log("this.assignmentTypeIdSample", this.assignmentTypeIdSample)

    console.log("this.assignmentTypeId", this.assignmentTypeId)
    this.assignmentType = true;
    // this.quickExamForm.controls.step.setValue(3);
    if (this.assignmentTypeIdSample == 2) {
      let studentListArry = <FormArray>this.quickExamForm.controls['studentList'];
      this.quickExamForm.addControl('subjectList', this.fb.array([], Validators.required));
      this.quickExamForm.addControl('patternUid', new FormControl('', Validators.required));
      this.quickExamForm.addControl('duration', new FormControl('', Validators.required));
      studentListArry.controls = []
      for (let k = 0; k < this.students.length; k++) {
        studentListArry.push(new FormControl(this.students[k]));
      }
      // this.quickExamForm.addControl('patternUid', new FormControl('', Validators.required));
      this.assignmentTypeId = this.assignmentTypeIdSample;
      console.log("this.assignmentTypeId", this.assignmentTypeId)
      this.formSubmit();
    } else {
      this.getQuestionCategories();
      this.quickExamForm.addControl('passingMarks', new FormControl('', Validators.required));
      this.quickExamForm.addControl('maximumMarks', new FormControl('', Validators.required));
      this.quickExamForm.addControl('subjectList', this.fb.array([], Validators.required));
      this.quickExamForm.addControl('questionCategory', new FormControl('', Validators.required));
      console.log(this.quickExamForm);
      this.assignmentTypeId = this.assignmentTypeIdSample;
      console.log("this.assignmentTypeId", this.assignmentTypeId)
    }
  }
  getQuestionCategories() {
    this.questionCategories = []
    return this.examService.getQuestionCategories().subscribe(
      res => {
        if (res) {
          console.log(" this.questionCategories =>", JSON.stringify(res));
          if (res.success == true) {
            this.questionCategories = res.list
          } else {
            this.toastService.ShowWarning('Question Categories not found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  closeAssignmentType() {
    this.assignmentTypeId = null;
  }

  public getCoursePlannerBySubject(subjectId: number, cpArray: FormArray) {
    console.log("couse api")
    this.coursePlanner = [];
    this.spinner.show();
    return this.examService.getCoursePlannerList(subjectId, this.courseId).subscribe(
      res => {
        if (res && res.success) {
          this.coursePlanner = res.list;
          cpArray.controls = [];

          for (let cp of res.list) {
            cpArray.push(this.addCoursePlanner(cp, false));
            console.log("cpArray", cpArray.length)
          }

          // for (let j = 0; j < cpArray.length; j++) {

          //   let coursePlannerGroup = <FormGroup>cpArray.controls[j];
          //   console.log("coursePlannerGroup", coursePlannerGroup)
          //   console.log("subjectGroup.value['id']", coursePlannerGroup.value['id'])
          // }
          console.log("value", this.quickExamForm.value)
        } else {
          this.toastService.ShowWarning(res.responseMessage);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   // this.toasterService.ShowError(error.error.errorMessage);
        // }
      });
  }

  addCoursePlanner(data: any, isSelected: boolean) {
    console.log("couse group")
    return this.fb.group({
      uniqueId: data.uniqueId,
      name: data.name,
      isSelected: isSelected,
      syllabusCoursewise: [],
      selectedSyllabus: [],
      selectedsyllabusCoursewise: this.fb.array([], Validators.nullValidator),
    });
  }

  selectedSubjects = [];
  addSubject(value) {
    let examSyllabus = <FormArray>this.quickExamForm.controls['subjectList'];
    let subjectList = examSyllabus.controls;
    let uniqueIds = [];
    let subjectuniqueIds = this.selectedSubjects;
    examSyllabus.controls = [];
    if (value != null && value.length > 0) {
      console.log(value);
      for (let i = 0; i < value.length; i++) {
        uniqueIds.push(value[i].uniqueId);
        if (!this.selectedSubjects.includes(value[i].uniqueId)) {
          this.selectedSubjects.push(value[i].uniqueId);
          examSyllabus.push(this.initSubjects(value[i]));
        } else {
          console.log(this.selectedSubjects);
          console.log(subjectList);
          subjectuniqueIds.forEach((x, index) => {
            if (x == value[i].uniqueId) {
              examSyllabus.push(subjectList[index]);
            }
          })
        }
      }
      this.selectedSubjects = uniqueIds;
      console.log(examSyllabus);
    }
    // examSyllabus=selectedSubjects
    // this.addActualSubjects(value, controlArray);
  }

  initSubjects(subject: any) {
    return this.fb.group({
      uniqueId: [subject.uniqueId, Validators.required],
      subjectName: [subject.name, Validators.required],
      actualCoursePlanners: this.fb.array([], Validators.nullValidator),
      remarks: ['', Validators.nullValidator],
      syllabus: [[], Validators.nullValidator],
      isSyllabusSelected: [true, Validators.nullValidator],
      examAssignmentPatternSubjectId: ['', Validators.nullValidator],
      actualSyllabus: [[], Validators.nullValidator],
    });
  }

  wizardSecondNextStep() {
    this.isFirstNext = true;
    console.log(this.quickExamForm);
    if (this.quickExamForm.invalid) {
      return;
    }
    let syllabusValid = true;
    let subjectArray = <FormArray>this.quickExamForm.controls['subjectList'];
    for (let l = 0; l < subjectArray.length; l++) {
      let subjectGroup = <FormGroup>subjectArray.controls[l];
      let syllabusArray = <FormArray>subjectGroup.controls['syllabus'];
      console.log(syllabusArray);
      if (subjectGroup.controls.isSyllabusSelected.value && syllabusArray.value.length == 0) {
        syllabusValid = false;
        break;
      }
    }
    if (!syllabusValid) {
      this.toastService.ShowWarning("Please add syllabus.");
      return;
    }

    this.isFirstNext = false;
    // let subjectArray = <FormArray>syllabusGroup.controls['subjects'];


    for (let l = 0; l < subjectArray.length; l++) {

      let subjectGroup = <FormGroup>subjectArray.controls[l];
      let syllabusArray = <FormArray>subjectGroup.controls['syllabus'];
      let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
      for (let j = 0; j < actualCPArray.length; j++) {

        let selectedsyllabusCoursewise = <FormGroup>actualCPArray.controls[j];
        let selectedsyllabus = <FormArray>selectedsyllabusCoursewise.controls['selectedsyllabusCoursewise'];
        for (let k = 0; k < selectedsyllabus.length; k++) {

          console.log("selectedsyllabus", selectedsyllabus.controls[k].value);

          syllabusArray.push(new FormControl(selectedsyllabus.controls[k].value));
        }
      }

    }
    console.log("this.quickExamForm.", this.quickExamForm.value);
    // if (syllabusArray.length == 0) {
    //   // this.toasterService.ShowWarning("Please select syllabus");
    //   return false;
    // }
    // if (syllabusGroup.controls['boardUid'].value != 0 && syllabusGroup.controls['boardUid'].value != null && syllabusGroup.controls['boardUid'].value != '') {
    // } else {
    //   // this.toasterService.ShowWarning("Please select Board");
    //   return false;
    // }
    // if (syllabusGroup.controls['gradeUid'].value != 0 && syllabusGroup.controls['gradeUid'].value != null && syllabusGroup.controls['gradeUid'].value != '') {
    // } else {
    //   // this.toasterService.ShowWarning("Please select Grade");
    //   return false;
    // }

    this.lastNext();
    this.wizard.navigation.goToStep(3)
  }

  exam: any;
  courseName: string;
  testType: string;
  batchName = [];
  lastNext() {
    this.exam = this.quickExamForm.value;
    console.log("exam", this.exam);
    let course = this.courseList.find(x => x.uniqueId == this.exam.batchInfo.courseId);
    this.courseName = course.name;
    console.log("this.testTypeList", this.testTypeList)
    let testType = this.testTypeList.find(x => x.id == this.exam.testTypeId);
    this.testType = testType.name;
    console.log("this.batchList", this.batchList)
    if (this.exam.batchInfo.batchIds != '') {
      this.batchName = [];
      for (let i = 0; i < this.exam.batchInfo.batchIds.length; i++) {
        this.batchList.map(x => {
          if (x.uniqueId == this.exam.batchInfo.batchIds[i]) {
            this.batchName.push(x.name);
            return;
          }
        });
      }
    }
    this.isShowExamView = true;
    console.log("this.batchName", this.batchName)
    console.log("this.batchName", this.batchName.length)
    console.log("this.batchName", this.batchName[0])
  }
  wizardSecondtNext() {
    console.log(this.quickExamForm);
    this.isFirstNext = true;
    if (this.quickExamForm.controls.patternUid.value == null || this.quickExamForm.controls.patternUid.value == "") {
      this.toastService.ShowWarning("Please select exam pattern");
      console.log("object");
      return;
    }
    this.isFirstNext = false;
    this.quickExamForm.addControl('timingDetailsDto', this.initTimeInfo(""));
    this.quickExamForm.addControl('passingMarks', new FormControl('', Validators.required));
    let batchDetails = <FormGroup>this.quickExamForm.controls['batchInfo'];
    this.courseId = batchDetails.controls['courseId'].value;
    let subjectArray = <FormArray>this.quickExamForm.controls['subjectList'];

    for (let i = 0; i < subjectArray.length; i++) {


      let subjectGroup = <FormGroup>subjectArray.controls[i];

      let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
      this.getCoursePlannerBySubject(subjectGroup.value['uniqueId'], actualCPArray);

    }
    console.log("subjectArray", subjectArray, " subjectArray.length", subjectArray.length, this.quickExamForm);
    console.log("object1");
    this.wizard.navigation.goToStep(2)
    this.quickExamForm.controls.step.setValue(3);
  }

  radioButtonEvent(type) {

  }

  addDeleteStudentUid(event, user) {

  }
  openConfirm(content) {
    console.log("test")
    this.modalService.open(content, { centered: true });
  }

  saveExam() {
    console.log(this.quickExamForm);
    if (this.quickExamForm.invalid) {
      return;
    }
    this.spinner.show();
    let examValue = this.quickExamForm.value;
    let date = new Date();
    let timeformat = this.convertTime(this.exam.timingDetailsDto.assessmentTime);
    let time = this.exam.timingDetailsDto.assessmentTime.split(" ");
    let splitTimeArry = timeformat.split(":");
    let hr = splitTimeArry[0];
    let min = splitTimeArry[1];
    date.setHours(+hr);
    date.setMinutes(+min);
    console.log(time, hr, min, date);
    let studentList = [];
    this.students.forEach(x => {
      studentList.push(x.student.uniqueId);
    });
    let subjectDto = [];
    examValue.subjectList.forEach(x => {
      let subjDto = {
        uniqueId: x.uniqueId,
        remarks: x.remarks,
        syllabusIds: x.syllabus,
        isSyllabusSelected: x.isSyllabusSelected
      }
      subjectDto.push(subjDto);
    });
    let urlString = (this.assignmentTypeIdSample == 1) ? 'quick' : 'patterned';
    let dto = {
      "name": examValue.name,
      "testTypeId": examValue.testTypeId,
      "studentUids": studentList,
      "subjectList": subjectDto,
      "timingDetailsDto": {
        "assessmentDate": this.datePipe.transform(examValue.timingDetailsDto.assessmentDate, "yyyy-MM-dd'T'HH:mm:ss.000Z"),
        "assessmentTime": this.datePipe.transform(date, "yyyy-MM-dd'T'HH:mm:ss.000Z"),
        "extraTime": examValue.timingDetailsDto.extraTime
      },
      "passingMarks": parseInt(examValue.passingMarks),
      "isFromMobile": false,
      "isPatternVisible": false
    }
    if (this.assignmentTypeIdSample == 2) {
      dto['patternUid'] = examValue.patternUid;
      dto.isPatternVisible = true;
    } else {
      dto['questionCategory'] = parseInt(examValue.questionCategory);
      dto['maximumMarks'] = parseInt(examValue.maximumMarks);
      dto.timingDetailsDto["duration"] = parseInt(examValue.timingDetailsDto.duration)
    }
    console.log(dto);
    // this.examUniqueId = "d6b654cd-db26-4d96-b433-c4f7da56d623";
    // this.isCreateTrack = true; 

    this.examService.addExam(urlString, dto).subscribe(
      res => {
        console.log("res", res)
        if (res && res.success) {
          this.toastService.showSuccess(res.responseMessage);
          this.examUniqueId = res.uniqueId;
          this.isShowExamView = false;
          this.isCreateTrack = true;
          this.selectedSubjects = [];
          this.quickExamForm.reset();
        } else {
          this.toastService.ShowWarning(res.responseMessage);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      })
    // this.isAssesment = false;
    // this.getExamList();

  }

  deleteSelectedStudent(id, index) {
    console.log("id", id);
    console.log("index", index);

    console.log("selectedStudentsList", this.selectedStudentsList);
    var studentList = this.selectedStudentsList.splice(index, 1);
    console.log("studentList", this.selectedStudentsList);
    this.viewSelectedStudentsList = this.selectedStudentsList.slice(0, 7);
    console.log("viewSelectedStudentsList", this.viewSelectedStudentsList);
    if (this.selectedStudentsList.length > 7) {
      this.remainingSelectedStudents = this.selectedStudentsList.length - 7;
    } else {
      this.remainingSelectedStudents = 0
    }
    this.students = this.selectedStudentsList;
    let studentListArry = <FormArray>this.quickExamForm.controls['studentList'];
    studentListArry.controls = []
    for (let k = 0; k < this.students.length; k++) {
      studentListArry.push(new FormControl(this.students[k]));
    }
    this.examService.studentList.next(this.students);
  }

  createPattern() {
    this.addPattern = true;
    this.isHideHeader = true;
    this.hideHeader.emit("1");
  }
  cancel() {
    this.isHideHeader = false;
    this.addPattern = false;
    this.isFirstNext = false;
    this.isCreateTrack = false;
    this.isAssesment = false;
    this.assignmentType = false;
    this.isShowExamView = false;
    this.assignmentTypeId = null;
    this.assignmentTypeIdSample = null;
    this.students = [];
    this.selectedStudentsList = [];
    this.quickExamForm.reset();
    this.initExamForm();
    console.log(this.quickExamForm);
    this.close.emit("");
  }
  changeStep(event) {
    switch (event) {
      case "1":
        this.wizard.navigation.goToStep(2)
        break;
      case "2":
        this.wizard.navigation.goToStep(0)
        break;
      case "3":
        this.wizard.navigation.goToStep(2)
        break;
      case "4":
        this.wizard.navigation.goToStep(0)
        break;
    }
  }
  hideHeaderFun(event) {
    if (event == "1") {
      this.isHideHeader = true;
    } else {
      this.isHideHeader = false;
    }
    this.hideHeader.emit(event);
  }
  hideButtons(event) {
    if (event == "1") {
      this.isHidebuttons = true;
    } else {
      this.isHidebuttons = false;
    }
  }
  previousStep(type) {
    if (type == 1) {
      if (!this.quickExamForm.controls.patternUid.value) {
        this.quickExamForm.removeControl('patternUid');
        this.quickExamForm.removeControl('duration');
        this.quickExamForm.removeControl('subjectList');
      }
    } else if (type == 2) {
      if (this.assignmentTypeId == 2) {
        this.quickExamForm.removeControl('passingMarks');
      } else {
        // this.quickExamForm.removeControl('subjectList');
      }
      // this.quickExamForm.removeControl('timingDetailsDto');
    }
  }
  convertTime(time) {
    return moment(time, 'hh:mm A').format('HH:mm');
  }
}
