import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-sheet',
  templateUrl: './student-sheet.component.html',
  styleUrls: ['./student-sheet.component.scss']
})
export class StudentSheetComponent implements OnInit {
  List = [{ enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'not well',status:'Pending'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Holiday',subreason:'went for trip',status:'Approved'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Got Covid',status:'Rejected'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Suffring from fever',status:'Pending'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'not well',status:'Rejected'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Holiday',subreason:'went for trip',status:'Pending'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Got Covid',status:'Approved'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Suffring from fever',status:'Rejected'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Holiday',subreason:'went for trip',status:'Approved'},
  { enno: 'AR876-80', name: 'Roshini Jawaid', course: 'JEE Advance', date: '29-4-2021', login: '10:53 am', loginShift: 'Morning Shift', logout: '11:53 am', logoutShift: 'Morning Shift',reason:'Sick Leave',subreason:'Suffring from fever',status:'Rejected'},];
  isPresent = true;
  isLeave = false;
  constructor() { }

  ngOnInit() {
  }

  isTabChange(id1, id2, id3) {
    let ele1 = document.getElementById(id1);
    let ele2 = document.getElementById(id2);
    let ele3 = document.getElementById(id3);
    ele1.classList.add('statusStudBtnactive');
    ele2.classList.remove('statusStudBtnactive')
    ele3.classList.remove('statusStudBtnactive')
    if (id1 == 'present') {
      this.isPresent=true;
      this.isLeave=false;
    }else if(id1 == 'absent'){
      this.isPresent=false;
      this.isLeave=false;
    }else if(id1 == 'leave'){
      this.isPresent=false;
      this.isLeave=true;
    }
  }
}
