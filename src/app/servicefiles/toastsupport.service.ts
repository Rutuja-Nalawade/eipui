import { Injectable  } from '@angular/core';
// import { ToastService } from './toast.service';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
 
@Injectable({
  providedIn: 'root'
})
export class ToastsupportService {

    public executeChildComponentMethod: Subject<any> = new Subject<any>();
 
    constructor(private toastr : ToastrService){}
  
    //D24965
    // showErrorMessage(errorMessage : string){
    //     this.toastService.show(errorMessage, {
    //         classname: 'bg-danger text-light',
    //         delay: 2000 ,
    //         autohide: true,
    //       });
    // }

    // showSuccessMessage(successMessage : string){
    //     this.toastService.show(successMessage, {
    //         classname: 'bg-success text-light',
    //         delay: 2000 ,
    //         autohide: true,
    //       });
    // }

    // showInfoMessage(infoMessage : string){
    //     this.toastService.show(infoMessage, {
    //         classname: 'bg-info text-light',
    //         delay: 2000 ,
    //         autohide: true,
    //       });
    // }

    showSuccess(successMessage : string) {
        this.toastr.success(successMessage, 'Success');
      }

    ShowError(error : string){
        this.toastr.error(error,'Error');
    } 
    
    ShowWarning(warningMessage : string){
        this.toastr.warning(warningMessage,'Warning');
    }

    ShowInfo(infoMessage : string){
        this.toastr.info(infoMessage,'Info');
    }


}