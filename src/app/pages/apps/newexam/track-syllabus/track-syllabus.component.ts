import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ExamService } from '../exam.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-track-syllabus',
  templateUrl: './track-syllabus.component.html',
  styleUrls: ['./track-syllabus.component.scss']
})
export class TrackSyllabusComponent implements OnInit {
  trackUid: string;
  qbsCourseId: any;
  subjectName: string;
  syllabusForm: FormGroup;
  syllabusState: any;
  examAssignmentSubjectId: number;
  testType: any;
  syllabusEdit: any;
  examSyllabus: any;
  isEditable: string;
  gradeList = [];
    selectedGrades = [];
    importedUnitList = [];
    importedChapterList = [];
    importedTopicList = [];
    importedSubTopicList = [];
    notImportedUnitList = [];
    notImportedChapterList = [];
    notImportedTopicList = [];
    notImportedSubTopicList = [];
    isDisabled: Boolean = true;
    isGradeSelected: Boolean = false;
  constructor(private route: ActivatedRoute,
               private fb: FormBuilder, 
              //  private qbsUrlService: QbsUrlService,
               private router: Router,
               private toastService: ToastsupportService, 
               private examSyllabusService: ExamService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.trackUid = params['trackUid'];
      this.subjectName = params['subjectName'];
      this.isEditable = params['isEditable'];
      this.syllabusState = params['syllabusState'];
      this.testType = params['testType'];
      this.examAssignmentSubjectId = params['examAssignmentSubjectId']
      });
      this.initTrackSyllabusForm();
      this.getExamSyllabus();
      if (this.syllabusState == "2" || this.syllabusState == "3") {
        this.getGradeList();
        }
      if (this.isEditable == "YES") {
          // this.getSyllabus();
      }
  }

  getGradeList() {
    this.gradeList = [];
    return this.examSyllabusService.getGradeListByExamSubjectId(this.examAssignmentSubjectId).subscribe(
        res => {
            if (res) {
                this.gradeList = res;
                console.log("this.gradeList",this.gradeList)
            }
        },
        (error: HttpErrorResponse) => {
            // if (error.error instanceof Error) {
            //     console.log("Client-side error occured.");
            // } else {
            //     this.toastService.ShowError("No Grade found for this board..");
            //     // this.toasterService.ShowError(error.error.errorMessage);
            // }
        }
    );
}

getExamSyllabus() {
  return this.examSyllabusService.getExamSubjectSyllabus(this.examAssignmentSubjectId, this.testType, this.syllabusState).subscribe(
      res => {

          this.setValueForImportedSyllabus(res);
      },
      (error: HttpErrorResponse) => {
        //   if (error.error instanceof Error) {
        //       console.log("Client-side error occured.");
        //   } else {
        //       this.toastService.ShowError(error.error.errorMessage);
        //   }
      }
  );
}

setValueForImportedSyllabus(res: any) {
    console.log("res",res)
  switch (parseInt(this.testType)) {
      case 1:
          this.importedUnitList = res.unitList;
          if (res.notImportedUnitList != null) {
              this.notImportedUnitList = res.notImportedUnitList;
          }
          break;
      case 2:
          this.importedChapterList = res.chapterList;
          if (res.notImportedChapterList != null) {
              this.notImportedChapterList = res.notImportedChapterList;
          }
          break;

      case 4:
          switch (parseInt(this.syllabusState)) {
              case 1:
                  this.importedChapterList = res.chapterList;
                  break;
              case 2:
                  this.notImportedSubTopicList = res.notImportedSubtopicList;
                  break;
              case 3:
                  this.importedChapterList = res.chapterList;
                  this.notImportedSubTopicList = res.notImportedSubtopicList;
                  // this.importedSubTopicList = res.subtopicList;
                  // console.log(JSON.stringify(res));
                  break;
          }
      default:
          switch (parseInt(this.syllabusState)) {
              case 1:
                  this.importedChapterList = res.chapterList;
                  break;
              case 2:
                  this.notImportedTopicList = res.notImportedTopicList;
                  break;
              case 3:
                  this.importedChapterList = res.chapterList;
                  this.notImportedTopicList = res.notImportedTopicList;
                  // console.log(JSON.stringify(res));
                  break;
          }
          break;
  }
}
initTrackSyllabusForm() {
  this.syllabusForm = this.fb.group({
      state: [this.syllabusState, Validators.required],
      testType: [this.testType, Validators.required],
      units: [],
      chapters: [],
      topics: [],
      subtopics: [],
      trackUid: [this.trackUid, Validators.nullValidator],
      gradeUid: [[], Validators.required],
      gradeList: this.fb.array([], Validators.nullValidator),
      createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.nullValidator],
  });
}


loadSyllabusFromEaas() {
  let chapterIds = this.pluck(this.syllabusForm.controls['chapters'].value, 'id');
  let dto = {
      "ids": chapterIds,
      "trackUid": this.trackUid,
      "state": this.syllabusState
  }
  switch (parseInt(this.testType)) {
      case 4:
          this.getSubTopicsFromChapter(dto);
          break;
      default:
          this.getTopicsFromChapter(dto);
          break;
  }
}

pluck(objs, name) {
  var sol = [];
  for (var i in objs) {
      if (objs[i].hasOwnProperty(name)) {
          sol.push(objs[i][name]);
      }
  }
  return sol;
}

getTopicsFromChapter(dto: any) {
  this.importedTopicList = [];
  // this.notImportedTopicList = [];
  return this.examSyllabusService.getTopicsByChaptersIds(dto).subscribe(
      res => {
          if (res) {
              if (res.topicList != null) {
                  this.importedTopicList = res.topicList;
              }
              // if (res.notImportedTopicList != null) {
              // this.notImportedTopicList = res.notImportedTopicList;
              // }
          }
      },
      (error: HttpErrorResponse) => {
        //   if (error.error instanceof Error) {
        //       console.log("Client-side error occured.");
        //   } else {
        //       this.toastService.ShowError(error.error.errorMessage);
        //   }
      }
  );
}

getSubTopicsFromChapter(dto: any) {
  this.importedSubTopicList = [];
  // this.notImportedSubTopicList = [];
  return this.examSyllabusService.getSubTopicsByChaptersIds(dto).subscribe(
      res => {
          if (res) {
              if (res.subtopicList != null) {
                  this.importedSubTopicList = res.subtopicList;
              }
              // if (res.notImportedSubtopicList != null) {
              // this.notImportedSubTopicList = res.notImportedSubtopicList;
              // }
          }
      },
      (error: HttpErrorResponse) => {
        //   if (error.error instanceof Error) {
        //       console.log("Client-side error occured.");
        //   } else {
        //       this.toastService.ShowError(error.error.errorMessage);
        //   }
      }
  );
}

submitToQuestionBank() {
  this.examSyllabusService.getAddUpdateTrackSyllabus(this.syllabusForm.value).subscribe(
      res => {
          // this.spinner = false;
          if (res)
              this.router.navigate(['/newModule/exam/track/questionBank', this.trackUid]);
      },
      (error: HttpErrorResponse) => {
        //   if (error.error instanceof Error) {
        //       console.log("Client-side error occured.");
        //   } else {
        //       // this.spinner = false;
        //       this.toastService.ShowError(error.error.errorMessage);
        //   }
      });

}

toggle() {
this.syllabusForm.controls['gradeUid'].value.length > 0 ? this.isGradeSelected = true : this.isGradeSelected = false;
this.loadView();
 }

 loadView() {
    if (this.isGradeSelected == true) {
        let gradeControl = <FormArray>this.syllabusForm.controls['gradeList'];
        gradeControl.controls = [];
        let gradeList: [] = this.syllabusForm.controls['gradeUid'].value;
        for (let i = 0; i < gradeList.length; i++) {
            gradeControl.push(this.initGradeFrom(gradeList[i]));
        }
    }
}

initGradeFrom(grade: any) {
    return this.fb.group({
        gradeName: [grade.name, Validators.required],
        qbsStandardId: [grade.universalId, Validators.required],
        qbsCourseId: [grade.courseUniversalId, Validators.required],
        isGradeSelected: [false, Validators.required],
        actualUnits: this.fb.array([], Validators.nullValidator),
        actualChapter: this.fb.array([], Validators.nullValidator),
        actualTopic: this.fb.array([], Validators.nullValidator),
        actualSubTopic: this.fb.array([], Validators.nullValidator),
        units: [],
        chapters: [],
        topics: [],
        subtopics: [],
    });
}

getUnit(value: boolean, qbsCourseId: number, qbsStandardId: number, index: number) {
    console.log('value',value,'qbsCourseId',qbsCourseId,'qbsStandardId',qbsStandardId,'index',index)
    if (value == true) {
        // this.spinner = true;
        // this.units = [];
        this.examSyllabusService.getQbsSyllabusUnits(qbsCourseId, qbsStandardId, this.subjectName).subscribe(
            res => {
                if (res) {
                    // this.units = res;
                    let gradeControl = <FormArray>this.syllabusForm.controls['gradeList'];
                    let gradeGroup = <FormGroup>gradeControl.controls[index];
                    let unitArray = <FormArray>gradeGroup.controls['actualUnits'];
                    unitArray.controls = [];
                    for (let unit of res) {
                        unitArray.push(this.addDataToControls(unit));
                    }
                }
                // this.spinner = false;
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     // this.spinner = false;
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            }
        );
    }
}

addDataToControls(data: any) {
    return this.fb.group({
        id: data.id,
        name: data.name,
        chapterId: data.chapterId,
        universalId: data.universalId
    });
}

loadChapters(index: number) {
    let gradeControl = <FormArray>this.syllabusForm.controls['gradeList'];
    let gradeGroup = <FormGroup>gradeControl.controls[index];
    let ids = this.pluck(gradeGroup.controls['units'].value, 'id');
    if (ids.length > 0) {
        this.isDisabled = false;
        this.examSyllabusService.getChapters(ids).subscribe(
            res => {
                if (res) {
                    let chapterArray = <FormArray>gradeGroup.controls['actualChapter'];
                    chapterArray.controls = [];
                    for (let chapters of res) {
                        chapterArray.push(this.addDataToControls(chapters));
                    }
                }
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    } else {
        this.isDisabled = true;
    }
}

loadTopics(index: number) {
    let gradeControl = <FormArray>this.syllabusForm.controls['gradeList'];
    let gradeGroup = <FormGroup>gradeControl.controls[index];
    let ids = this.pluck(gradeGroup.controls['chapters'].value, 'id');
    if (ids.length > 0) {
        this.examSyllabusService.getTopics(ids).subscribe(
            res => {
                if (res) {
                    let topicArray = <FormArray>gradeGroup.controls['actualTopic'];
                    topicArray.controls = [];
                    for (let topic of res) {
                        topicArray.push(this.addDataToControls(topic));
                    }
                }
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }
}

loadSubTopics(index: number) {
    let gradeControl = <FormArray>this.syllabusForm.controls['gradeList'];
    let gradeGroup = <FormGroup>gradeControl.controls[index];
    let ids = this.pluck(gradeGroup.controls['topics'].value, 'id');
    if (ids.length > 0) {
        this.examSyllabusService.getSubTopics(ids).subscribe(
            res => {
                if (res) {
                    let subTopicArray = <FormArray>gradeGroup.controls['actualSubTopic'];
                    subTopicArray.controls = [];
                    for (let subTopic of res) {
                        subTopicArray.push(this.addDataToControls(subTopic));
                    }
                }
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }
}

}
