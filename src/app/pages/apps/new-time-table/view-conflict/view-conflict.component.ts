import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';

@Component({
  selector: 'app-view-conflict',
  templateUrl: './view-conflict.component.html',
  styleUrls: ['./view-conflict.component.scss']
})
export class ViewConflictComponent implements OnInit {
  executionHistory: any = {};
  conflictType: any = 1;
  conflictDetails: any = [];
  @Input() viewType: any;
  @Input() conflictData: any;
  @Input() shiftDetails: any;
  @Input() timetableData: any;
  @Output() newItemEvent = new EventEmitter<string>();
  constructor(private timeTableService: TimeTableServiceService, private toastService: ToastsupportService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (changes['viewType'] && changes['viewType'].currentValue == 1) {
      this.getViewHistory();
    }
    if (changes['viewType'] && changes['viewType'].currentValue == 3) {
      for (let i = 0; i < this.conflictData.conflictData.length; i++) {
        const element = this.conflictData.conflictData[i];
        if (element.isBusy) {
          this.conflictDetails.push(element)
        }
      }
      this.conflictType = this.conflictData.conflictType
    }
    console.log(this.conflictData);
  }
  getViewHistory() {
    this.spinner.show();
    this.timeTableService.getExicutionHistory(this.timetableData.uniqueId).subscribe(
      (res) => {
        if (res) {
          this.executionHistory = res;
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  close(value) {
    this.newItemEvent.emit(value);
  }

}
