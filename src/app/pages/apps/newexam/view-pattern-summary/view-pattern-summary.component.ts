import { Component, OnInit, Input } from '@angular/core';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ExamService } from '../exam.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-view-pattern-summary',
  templateUrl: './view-pattern-summary.component.html',
  styleUrls: ['./view-pattern-summary.component.scss']
})
export class ViewPatternSummaryComponent implements OnInit {
  @Input() examName:any;
  @Input() isradio:any
  num=1;

  @Input() customPattern: any;
  @Input() examPattern: any;
  @Input() pattern: any;
  @Input() radioValue:any
  patterns:any
  subjectName:String=null;
  duration:any;
  totQuestions:any;
  totMarks:any;
  patterns1: any;
  patterns2: any;
  patternList = [];
  activeType:any=0;
  pattern1:boolean=true;
  pattern2:boolean=false;
  isRadioActivate:any="1";
  constructor(private spinner: NgxSpinnerService,
    private examService: ExamService,
    private toastService:ToastsupportService,) {
   }

  ngOnInit() {
    // console.log("tab number",this.examName)
    // console.log("isradio",this.isradio)

    console.log("examPattern",this.examPattern);
    console.log("radioValue",this.radioValue);
    console.log("isRadioActivate",this.isRadioActivate);
    this.duration=this.examPattern.examPatterns[0].duration;
    this.totMarks=this.examPattern.examPatterns[0].examMarks;
    this.totQuestions=this.examPattern.examPatterns[0].totalQuestion;
    this.subjectName=null
    if(this.examPattern.patternType == 4){
      this.examPattern.examPatterns.forEach(element => {
        element.subjects.forEach(ele => {
          if(this.subjectName != null){
            this.subjectName=this.subjectName +','+ ele.name
          }else{
            this.subjectName= ele.name
          }
          
        });
      });
    }

    if(this.examPattern.patternType == 3 || this.examPattern.patternType == 2 || this.examPattern.patternType == 1){
      // this.patterns=this.examPattern.examPatterns[0]
      this.patterns1=this.examPattern.examPatterns[0]
      this.patterns2=this.examPattern.examPatterns[1]
        this.patterns1.subjects.forEach(ele => {
          if(this.subjectName != null){
            this.subjectName=this.subjectName +','+ ele.name
          }else{
            this.subjectName= ele.name
          }
          
        });

      console.log("this.patterns1",this.patterns1) 
      console.log("this.patterns2",this.patterns2) 
    } 


  }

  paperClick(activeid,inactive){
  
    this.activeType=activeid;
    let ele = document.getElementById(activeid);
    let element = document.getElementById(inactive)
    ele.classList.add('active')
    element.classList.remove('active')
    
    if(activeid == 0){
      // this.patterns1=this.examPattern.examPatterns[0]
      this.pattern2=false
      this.pattern1=true
      this.subjectName=null
      this.patterns1.subjects.forEach(ele => {
        if(this.subjectName != null){
          this.subjectName=this.subjectName +','+ ele.name
        }else{
          this.subjectName= ele.name
        }
        
      });
      console.log('this.patterns 0',this.patterns)
    }else if(activeid == 1){
      // this.patterns2=this.examPattern.examPatterns[1]
      this.pattern1=false
      this.pattern2=true
      this.subjectName=null
      this.patterns2.subjects.forEach(ele => {
        if(this.subjectName != null){
          this.subjectName=this.subjectName +','+ ele.name
        }else{
          this.subjectName= ele.name
        }
        
      });
      console.log('this.patterns 1',this.patterns)
    }
  }

}
