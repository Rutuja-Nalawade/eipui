import { Sections } from "./sections.model";

export class Subjects {
    subjectName: any;
    subjectUid: any;
    sectionQuestionDtos: Sections[];
}
