// import { Component, OnInit } from '@angular/core';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { SubjectService } from 'src/app/services/subject.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HostListener } from '@angular/core';
// import { ExamPatternService } from 'src/app/services/exampattern.service';
import { ExamService } from './../exam.service';
import { NgxSpinnerService } from 'ngx-spinner';

declare var $:any;
@Component({
  selector: 'app-add-pattern',
  templateUrl: './add-pattern.component.html',
  styleUrls: ['./add-pattern.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddPatternComponent implements OnInit {

  isOptional:boolean=false;

  // breadCrumbItems: Array<{}>;
  examPatternForm: FormGroup;
  markingSchemeForm: FormGroup;
  instructionForm: FormGroup;
  totalSubjects: number;
  sectionFormArray: FormArray;
  selectedSubjectIds: any;
  examPattern: any;
  isForPattern: Boolean = null;
  parentIndex: number;
  childIndex: number;
  currentIndex: number;
  subjects = [];
  jeeSubjects = [];
  examPatternId: string;
  isUpdateMode = false;
  isPCMAvailable: Boolean = true;
  spinner: boolean = false;
  imageSrc: string;
  rightMarks: number;
  isFromEvent: boolean = false;
  examTypeList = [];
  jeeAdvanceSubjectiveQuestionType=[];
  customObjectiveExamQuestionType=[];
  
  // isPageChanged : Boolean = false;
  paperName = ['Paper-1', 'Paper-2']

  // jeeAdvanceSubjectiveQuestionType = [
  //   { "id": 1, "name": "SCQ" },
  //   { "id": 2, "name": "MCQ" },
  //   { "id": 3, "name": "Numeric" }
  // ];

  // customObjectiveExamQuestionType = [
  //   { "id": 4, "name": "SA" },
  //   { "id": 5, "name": "VSA" },
  //   { "id": 6, "name": "FIB" },
  //   { "id": 7, "name": "TF" }
  // ];

  editorConfig = {
    "editable": true,
    "spellcheck": true,
    "height": "auto",
    "minHeight": "100%;",
    "width": "auto",
    "minWidth": "0",
    "translate": "yes",
    "enableToolbar": true,
    "showToolbar": true,
    "placeholder": "Enter text here...",
    "imageEndPoint": "",
    "toolbar": [
      ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
      ["fontName", "fontSize", "color"],
    ]
  };

  @ViewChild('wizardForm', { static: false }) wizard: BaseWizardComponent;
  isSubmit: any;
  isAddSchemeSubmit: any;
  

  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute,private spinner1: NgxSpinnerService,
    private examService : ExamService,
    //  private examPatternService: ExamPatternService,
      private modalService: NgbModal, 
      // private subjectService: SubjectService, 
      private toastService: ToastsupportService) {
  }

  
  ngOnInit() {
    console.log("ngOnInit"); 
    console.log("jhgfd",this.examPatternForm)
    // this.breadCrumbItems = [{ label: 'Dashboard', path: '/' }, { label: 'ExamPattern', path: '/', active: true }];
    this.initPatternForm();
    // this.questionCount = Array(100).fill(0).map((x, i) => i + 1);
    // this.sectionCount = Array(20).fill(0).map((x, i) => i + 1);
    this.initMarkingSchemeForm();
    this.initInstructionForm();
    this.getExamType();
    this.getsubjectiveQuestionTypeList();
    this.getObjectiveQuestionTypeList();

    this.route.params.subscribe(params => {
      this.examPatternId = params['examPatternId'];
      if (this.examPatternId != null) {
        this.isUpdateMode = true;
        // this.getExamPattern();
      }
    });

    this.examService.rightMarks.subscribe(
      (value: any) => {
        this.rightMarks = value;
        this.isFromEvent = true;
        // this.calculateMarks(this.parentIndex, this.childIndex, this.currentIndex);
      });

  }

  initPatternForm() {
    this.examPatternForm = this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      isPaperTwo: [false, Validators.required],
      isPaperSame: [false, Validators.required],
      patternType: [null, Validators.required],
      name: ['', Validators.required],
      organizationUid: [sessionStorage.getItem('organizationId'), Validators.nullValidator],
      createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.nullValidator],
      examPatterns: this.fb.array([this.initExamPatternForm()]),
    });
  }

  initExamPatternForm() {

    return this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      patternName: ['', Validators.nullValidator],
      isSectionSame: [true, Validators.nullValidator],
      totalSectionsInSubject: ['', [Validators.required, Validators.nullValidator]],
      totalQuestionsInSubject: ['',[ Validators.required, Validators.nullValidator]],
      totalPhySection: ['', Validators.nullValidator],
      totalPhyQuestion: ['', Validators.nullValidator],
      totalChemSection: ['', Validators.nullValidator],
      totalChemQuestion: ['', Validators.nullValidator],
      totalMathSection: ['', Validators.nullValidator],
      totalMathQuestion: ['', Validators.nullValidator],
      examMarks: [0, Validators.nullValidator],
      examType: [null, [Validators.required, Validators.nullValidator]],
      duration: ['', Validators.required],
      totalQuestion: ['', Validators.nullValidator],
      subjects: this.fb.array([], Validators.required),
    });
  }

  initSections() {
    return this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.nullValidator],
      noOfQuestions: ['', Validators.nullValidator],
      optionalQuestions:['0', Validators.nullValidator],
      questionType: [1, Validators.nullValidator],
      rightMarks: ['', Validators.nullValidator],
      wrongMarks: [0, Validators.nullValidator],
      leaveMarks: [0, Validators.nullValidator],
      instructionDto: this.fb.array([]),
      markingScheme: this.fb.array([]),
      isAddImportScheme:[false, Validators.nullValidator],
      defaultWrongMarks: [0, Validators.nullValidator]
    });
  }

  initMarkingSchemeForm() {
    this.markingSchemeForm = this.fb.group({
      schemeType: ['', Validators.required],
      optionCount: [1, Validators.required],
      name: ['', Validators.required],
      uniqueId: [null, Validators.nullValidator],
      highestMarks: [0, Validators.required],
      leaveMarks: [0, Validators.required],
      defaultMarks: [0, Validators.required],
      organizationUid: ['', Validators.nullValidator],
      orgId:['',Validators.nullValidator],
      createdOrUpdatedBy: [sessionStorage.getItem('userId'), Validators.nullValidator],
      subMarkingScheme: this.fb.array([this.initSubMarkingScheme()])
    });
  }

  initSubMarkingScheme() {
    return this.fb.group({
      correctOption: [0, Validators.required],
      rightOption: [0, Validators.required],
      wrongOption: [0, Validators.required],
      marks: [0, Validators.required],
    });
  }

  initInstructionForm() {
    this.instructionForm = this.fb.group({
      instructionUid: ['', Validators.nullValidator],
      instructionType: ['', Validators.required],
      instructionTextOrImage: ['', Validators.required],
    });
  }
isExamPatternSelected=false
  getExamPattern($event, index: number) {
    
this.isExamPatternSelected=true;
    console.log('inside get exam pattern===>>>',$event);

    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let formGroup = <FormGroup>control.controls[index];
    formGroup.controls['patternName'].setValue('');
    formGroup.controls['isSectionSame'].setValue(true);
    formGroup.controls['totalSectionsInSubject'].setValue('');
    formGroup.controls['totalQuestionsInSubject'].setValue('');
    this.examPatternForm.controls['isPaperSame'].setValue(false);
    this.examPatternForm.controls['isPaperTwo'].setValue(false);
   
    if ($event == 3) {
     
      formGroup.controls['examType'].setValue(1);
      let Subjectcontrol = <FormArray>formGroup.controls['subjects'];
      this.getJeeAdvanceSubjects(Subjectcontrol);
    } else {
      this.getSubjectList();
      control.removeAt(index + 1);
      formGroup.controls['examType'].setValue(null);
      let Subjectcontrol = <FormArray>formGroup.controls['subjects'];
      this.clearSectionFormArray(Subjectcontrol);
    }
  }

  getJeeAdvanceSubjects(Subjectcontrol: FormArray) {
    this.jeeSubjects = [];
    this.spinner1.show()
    this.examService.getJeeAdvanceSubjects(sessionStorage.getItem('orgUniqueId')).subscribe(
      // this.examService.getJeeAdvanceSubjects('3c3dc272-d7cf-4c36-9411-16f714064f46').subscribe(
      res => {
        if (res) {
          this.jeeSubjects = res;
          this.addSubjectsWithValue(this.jeeSubjects, Subjectcontrol);
          this.spinner1.hide()
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner1.hide()
        // } else {
        //   this.isPCMAvailable = false;
        //   this.spinner1.hide()
        //   this.toastService.ShowError(error.error.errorMessage);

        // }
        this.isPCMAvailable = false;
        this.spinner1.hide()
      });
  }

  getSubjectList() {
    this.subjects = [];
    this.spinner1.show()
    this.examService.getAllSubjectByOrg(sessionStorage.getItem('organizationId')).subscribe(
    // this.examService.getAllSubjectByOrg(3).subscribe(
      res => {
        if (res) {
          this.subjects = res;
          this.spinner1.hide()
        }
        this.spinner1.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner1.hide()
        // } else {
        //   this.toastService.ShowWarning(error.error.errorMessage);
        //   this.spinner1.hide()
        // }
        this.spinner1.hide()
      });
  }


  clearSectionFormArray = (Subjectcontrol: FormArray) => {
    while (Subjectcontrol.length !== 0) {
      Subjectcontrol.removeAt(0);
      Subjectcontrol.removeAt(1);
      Subjectcontrol.removeAt(2);
    }
  }

  addSubjectsWithValue(subject: any, Subjectcontrol: FormArray) {
    Subjectcontrol.controls = [];
    if (subject != null && subject.length > 0) {
      for (let i = 0; i < subject.length; i++) {
        Subjectcontrol.push(this.initSubjectsWithValue(subject[i]));
      }
    }

    console.log("Subjectcontrol",Subjectcontrol)
  }

  initSubjectsWithValue(subject: any) {

    return this.fb.group({
      uniqueId: [subject.subjectId, Validators.nullValidator],
      name: [subject.name, Validators.nullValidator],
      totalMarks: [0, Validators.nullValidator],
      totalQuestions: [0, Validators.nullValidator],
      subjectId:[subject.id,Validators.nullValidator],
      sections: this.fb.array([], Validators.nullValidator),
    });
  }

  onChange(value: boolean, index: number) {

    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let formGroup = <FormGroup>control.controls[index];
    formGroup.controls['isSectionSame'].setValue(value);

    let Subjectcontrol = <FormArray>formGroup.controls['subjects'];
    if (formGroup.controls['isSectionSame'].value == true) {
      formGroup.controls['totalQuestionsInSubject'].setValue("");
      formGroup.controls['totalSectionsInSubject'].setValue("");
    } else {
      formGroup.controls['totalPhySection'].setValue("");
      formGroup.controls['totalChemSection'].setValue("");
      formGroup.controls['totalMathSection'].setValue("");
      formGroup.controls['totalPhyQuestion'].setValue("");
      formGroup.controls['totalChemQuestion'].setValue("");
      formGroup.controls['totalMathQuestion'].setValue("");
    }
    this.clearSectionFormArray(Subjectcontrol);
    this.addSubjectsWithValue(this.jeeSubjects, Subjectcontrol);
    // control.removeAt(index + 1);
  }

  getSections($event, index: number) {
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let formGroup = <FormGroup>control.controls[index];
    let Subjectcontrol = <FormArray>formGroup.controls['subjects'];
    for (let i = 0; i < Subjectcontrol.length; i++) {
      let formGroup = <FormGroup>Subjectcontrol.controls[i];
      let sectionControl = <FormArray>formGroup.controls['sections'];
      if ($event.target.value != null && $event.target.value != "") {
        let size = parseInt($event.target.value);
        sectionControl.controls = [];
        for (let i = 0; i < size; i++) {
          sectionControl.push(this.initSections());
        }
      } else {
        sectionControl.controls = [];
      }
    }
  }

  getPhysicSections($event, index: number) {
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[index];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let formGroup = <FormGroup>Subjectcontrol.controls[0];
    let sectionControl = <FormArray>formGroup.controls['sections'];
    if ($event.target.value != null && $event.target.value != "") {
      let size = parseInt($event.target.value);
      sectionControl.controls = [];
      for (let i = 0; i < size; i++) {
        sectionControl.push(this.initSections());
      }
    } else {
      sectionControl.controls = [];
    }
  }

  getChemistrySections($event, index: number) {

    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[index];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let formGroup = <FormGroup>Subjectcontrol.controls[1];
    let sectionControl = <FormArray>formGroup.controls['sections'];
    if ($event.target.value != null && $event.target.value != "") {
      let size = parseInt($event.target.value);
      sectionControl.controls = [];
      for (let i = 0; i < size; i++) {
        sectionControl.push(this.initSections());
      }
    } else {
      sectionControl.controls = [];
    }
  }

  getMathSections($event, index: number) {
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[index];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let formGroup = <FormGroup>Subjectcontrol.controls[2];
    let sectionControl = <FormArray>formGroup.controls['sections'];
    if ($event.target.value != null && $event.target.value != "") {
      let size = parseInt($event.target.value);
      sectionControl.controls = [];
      for (let i = 0; i < size; i++) {
        sectionControl.push(this.initSections());
      }
    } else {
      sectionControl.controls = [];
    }
  }

  getCustomizedSubject(event: any, index: number) {
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[index];
    examformGroup.controls['totalSectionsInSubject'].setValue('');
    examformGroup.controls['totalQuestionsInSubject'].setValue('');
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    this.addSubjectsWithValue(event, Subjectcontrol);
  }

  // wizardPreviousStep(){
  //   if (this.examPatternForm.controls['patternType'].value == 3 && this.examPatternForm.controls['isSectionSame'].value == true) {
  //     const control = <FormArray>this.examPatternForm.controls['subjects'];
  //     for (let j = 0; j <= control.length; j++) {
  //       control.controls = [];
  //       control.removeAt(j);
  //     }
  //     this.addSubjects();
  //     let tempformGroup = <FormGroup>control.controls[0];
  //     let tempsectionControl = <FormArray>tempformGroup.controls['sections'];
  //     tempsectionControl.controls = [];
  //     tempsectionControl.push(this.sectionFormArray);
  //   }
  // }

  wizardFirstNextStep() {
    console.log('this.examPatternForm',this.examPatternForm.value);
    this.isSubmit=true;
    if (!this.isPCMAvailable && this.examPatternForm.controls['patternType'].value == 3) {
      console.log('1 true')
      this.toastService.ShowWarning("Please add JEE subjects to this organization..");
      return false;
    }


    if (this.examPatternForm.controls['patternType'].value == 3 && !this.jeeAdvanceExamValidation()) {
      console.log('2 true')
      this.toastService.ShowWarning("Please fill all form value.");
      return false;
    }

    if (this.examPatternForm.controls['patternType'].value == 4 && !this.customizedExamValidation()) {
      this.toastService.ShowWarning("Please fill all form value.");
      return false;
    }

    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    for (let i = 0; i < control.length; i++) {
      
      let examformGroup = <FormGroup>control.controls[i];

      if (this.examPatternForm.controls['patternType'].value == 3 && this.examPatternForm.controls['isPaperTwo'].value == true && this.examPatternForm.controls['isPaperSame'].value == false) {
        console.log('3 true')
        examformGroup.controls['patternName'].setValue(this.examPatternForm.controls['name'].value + " " + this.paperName[i]);
      } else {
        examformGroup.controls['patternName'].setValue(this.examPatternForm.controls['name'].value);
      }

      let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
      if (examformGroup.controls['isSectionSame'].value == true && Subjectcontrol.length > 1 && this.examPatternForm.controls['patternType'].value == 3) {
        for (let j = 0; j < Subjectcontrol.length; j++) {
          Subjectcontrol.removeAt(j);
        }
      }
    }
    // this.isPageChanged = true;
  }

  jeeAdvanceExamValidation() {
    if(this.examPatternForm.controls['patternType'].value == 3 && this.examPatternForm.controls['isPaperTwo'].value == false && this.examPatternForm.controls['isPaperSame'].value == false){
      return false;
    }else{
    let examPatternArray = <FormArray>this.examPatternForm.controls['examPatterns'];
    for (let i = 0; i < examPatternArray.length; i++) {
      let examPatternGroup = <FormGroup>examPatternArray.controls[i];
      if (examPatternGroup.controls['isSectionSame'].value == true && examPatternGroup.controls['totalSectionsInSubject'].value != '' && examPatternGroup.controls['totalQuestionsInSubject'].value != '') {
        if (i == examPatternArray.length - 1) {
          return true;
        }
      }

      if (examPatternGroup.controls['isSectionSame'].value == false && examPatternGroup.controls['totalPhySection'].value != '' && examPatternGroup.controls['totalPhyQuestion'].value != '' && examPatternGroup.controls['totalChemSection'].value != '' && examPatternGroup.controls['totalChemQuestion'].value != '' && examPatternGroup.controls['totalMathSection'].value != '' && examPatternGroup.controls['totalMathQuestion'].value != '') {
        if (i == examPatternArray.length - 1) {
          return true;
        }
      }
    }
  }
    return false;
  }

  customizedExamValidation() {
    let examPatternArray = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examPatternGroup = <FormGroup>examPatternArray.controls[0];
    let subejctArray = <FormArray>examPatternGroup.controls['subjects'];
    if (examPatternGroup.controls['totalSectionsInSubject'].value != '' && examPatternGroup.controls['totalQuestionsInSubject'].value != '' && examPatternGroup.controls['examType'].value != '' && subejctArray.length > 0) {
      return true;
    }
    return false;
  }

  wizardSecondPreviousStep() {
    // this.isPageChanged = false;
    // let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    // for (let i = 0; i < control.length; i++) {
    //   let examformGroup = <FormGroup>control.controls[i];
    //   let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    //   if (examformGroup.controls['isSectionSame'].value == true && this.examPatternForm.controls['patternType'].value == 3) {
    //     this.clearSectionFormArray(Subjectcontrol);
    //     this.addSubjectsWithValue(this.jeeSubjects, Subjectcontrol);
    //     examformGroup.controls['totalQuestionsInSubject'].setValue("");
    //     examformGroup.controls['totalSectionsInSubject'].setValue("");
    //   }
    // }
  }

  wizardSecondNextStep() {
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    for (let j = 0; j < control.length; j++) {
        let examformGroup1 = <FormGroup>control.controls[j];
      let Subjectcontrol1 = <FormArray>examformGroup1.controls['subjects'];
      for (let i = 0; i < Subjectcontrol1.length; i++) {
        let subjectFormGroup = <FormGroup>Subjectcontrol1.controls[i];
        subjectFormGroup.controls['totalMarks'].setValue(0);

        let sectionControl = <FormArray>subjectFormGroup.controls['sections'];
        let marks: number = 0;
        for (let k = 0; k < sectionControl.length; k++) {
          let totalQuestion = 0;
          let sectionFormGroup = <FormGroup>sectionControl.controls[k];
          if (this.isFromEvent && k == j) {
            sectionFormGroup.controls['rightMarks'].setValue(this.rightMarks);
          }
          totalQuestion = sectionFormGroup.controls['noOfQuestions'].value - sectionFormGroup.controls['optionalQuestions'].value;
         
          marks = marks + totalQuestion * sectionFormGroup.controls['rightMarks'].value;
        }
        subjectFormGroup.controls['totalMarks'].setValue(marks);
        this.isFromEvent = false;
      }
      
      let totalMarks: number = 0;
      let totalQuestions: number = 0;
      let examformGroup = <FormGroup>control.controls[j];
      let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
      for (let i = 0; i < Subjectcontrol.length; i++) {
       
        let subjectQuestions: number = 0;
        let subjectFormGroup = <FormGroup>Subjectcontrol.controls[i];
        totalMarks = totalMarks + subjectFormGroup.controls['totalMarks'].value;
        let sectionArray = <FormArray>subjectFormGroup.controls['sections'];
        for (let k = 0; k < sectionArray.length; k++) {
          let sectionFormGroup = <FormGroup>sectionArray.controls[k];
          if (sectionFormGroup.controls['name'].value == '' || sectionFormGroup.controls['name'].value == 'undefined') {
            this.toastService.ShowWarning("Section name is missing..");
            console.log("Section name is missing..");
            return false;
          }
          if (sectionFormGroup.controls['questionType'].value == '' || sectionFormGroup.controls['questionType'].value == 'undefined') {
            this.toastService.ShowWarning("Please select all Question Type..");
            console.log("Please select all Question Type..");
            return false;
          }
          if (sectionFormGroup.controls['noOfQuestions'].value == '' || sectionFormGroup.controls['noOfQuestions'].value == 'undefined') {
            this.toastService.ShowWarning("Please enter all section questions..");
            console.log("Please enter all section questions..");
            return false;
          }
          // if (sectionFormGroup.controls['rightMarks'].value == '' || sectionFormGroup.controls['rightMarks'].value == 'undefined') {
          //   this.toastService.ShowWarning("Please enter right marks for all sections..");
          //   console.log("Please enter right marks for all sections..");
          //   return false;
          // }
          totalQuestions = totalQuestions + parseInt(sectionFormGroup.controls['noOfQuestions'].value);
          subjectQuestions = subjectQuestions + parseInt(sectionFormGroup.controls['noOfQuestions'].value);
        }
        subjectFormGroup.controls['totalQuestions'].setValue(subjectQuestions);
      }

      let isSectionSame = examformGroup.controls['isSectionSame'].value;
      if (isSectionSame == false) {
        examformGroup.controls['totalQuestionsInSubject'].setValue(parseInt(examformGroup.controls['totalPhyQuestion'].value) + parseInt(examformGroup.controls['totalChemQuestion'].value) + parseInt(examformGroup.controls['totalMathQuestion'].value));
      }
      if (isSectionSame == true && this.examPatternForm.controls['patternType'].value == 3) {
        examformGroup.controls['examMarks'].setValue(totalMarks * 3);
        examformGroup.controls['totalQuestion'].setValue(totalQuestions * 3);
      } else {
        examformGroup.controls['examMarks'].setValue(totalMarks);
        examformGroup.controls['totalQuestion'].setValue(totalQuestions);
      }

      this.examPattern = this.examPatternForm.value;

      console.log("this.examPattern",this.examPattern);
    }
    // this.isPageChanged = true;
  }

  calculateMarks(index: number, childIndex: number, currentIndex: number) {
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[index];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let subjectFormGroup = <FormGroup>Subjectcontrol.controls[childIndex];

    subjectFormGroup.controls['totalMarks'].setValue(0);

    let sectionControl = <FormArray>subjectFormGroup.controls['sections'];
    let marks: number = 0;
   
    for (let i = 0; i < sectionControl.length; i++) {
      let totalQuestion = 0;
      let sectionFormGroup = <FormGroup>sectionControl.controls[i];
      if (this.isFromEvent && currentIndex == i) {
        sectionFormGroup.controls['rightMarks'].setValue(this.rightMarks);
      }
      totalQuestion = sectionFormGroup.controls['noOfQuestions'].value - sectionFormGroup.controls['optionalQuestions'].value;
     
      marks = marks + totalQuestion * sectionFormGroup.controls['rightMarks'].value;
    }
    subjectFormGroup.controls['totalMarks'].setValue(marks);
    this.isFromEvent = false;
  }


  checkQuestionCount(value: number, childIndex: number, parentIndex: number, index: number) {

    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[index];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];

    let patternType = this.examPatternForm.controls['patternType'].value;
    let isSectionSame = examformGroup.controls['isSectionSame'].value;

    let subjectFormGroup = <FormGroup>Subjectcontrol.controls[parentIndex];
    let sectionControl = <FormArray>subjectFormGroup.controls['sections'];
    let totalQuestionCount = examformGroup.controls['totalQuestionsInSubject'].value;
    if ((patternType == 3 && isSectionSame == true) || patternType == 4) {
      this.getSum(totalQuestionCount, sectionControl, childIndex);
    } else {
      let physicQuestions = examformGroup.controls['totalPhyQuestion'].value;
      let chemistryQuestions = examformGroup.controls['totalChemQuestion'].value;
      let mathQuestions = examformGroup.controls['totalMathQuestion'].value;
      switch (parentIndex) {
        case 0: {
          this.getSum(physicQuestions, sectionControl, childIndex);
          break;
        }
        case 1: {
          this.getSum(chemistryQuestions, sectionControl, childIndex);
          break;
        }
        case 2: {
          this.getSum(mathQuestions, sectionControl, childIndex);
          break;
        }
      }
    }
    // this.calculateMarks(index, parentIndex, childIndex);
  }

  getSum(totalQuestionCount: any, sectionControl: FormArray, childIndex: number) {
    let sum: number = 0;

    for (let i = 0; i < sectionControl.length; i++) {
      let formGroup = <FormGroup>sectionControl.controls[i];
      let questionCountFormGroup = <FormControl>formGroup.controls['noOfQuestions'];
      sum = sum + parseInt(questionCountFormGroup.value != '' ? questionCountFormGroup.value : 0);
    }

    if (sum > totalQuestionCount) {
      let formGroup = <FormGroup>sectionControl.controls[childIndex];
      let lastIndexValue: number = formGroup.controls['noOfQuestions'].value;
      let diffNumber: number = sum - lastIndexValue;
      let actualNumber: number = totalQuestionCount - diffNumber
      this.toastService.ShowWarning("You can add only " + actualNumber + " questions");
      formGroup.controls['noOfQuestions'].setValue(actualNumber);
      return false;
    }
  }

  

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  /**
 * Go to next step while form value is valid
 */
  formSubmit() {
    console.log("this.examPatternForm.valid",this.examPatternForm.valid)
    console.log("this.examPatternForm.value",this.examPatternForm.value)
    // if (this.examPatternForm.valid) {
      console.log('true')
      this.wizard.navigation.goToNextStep();
    // }

  }

  onOptionalCheck2(id,id2,i,j,k){
    console.log("id",id)

    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[i];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let subjectFormGroup = <FormGroup>Subjectcontrol.controls[j];
    let sectionControl = <FormArray>subjectFormGroup.controls['sections'];
    let sectionFormGroup = <FormGroup>sectionControl.controls[k];
    sectionFormGroup.controls['optionalQuestions'].setValue(0)
    var ele=document.getElementById(id);
    console.log("ele",ele)


    if(ele.classList.contains('col-12')){
      ele.classList.remove("col-12");
       ele.classList.add("col-6");
       ele.style.paddingRight='0'
        
    }else if(ele.classList.contains('col-6')){
      ele.classList.remove("col-6");
      ele.classList.add("col-12");
      ele.style.paddingRight='12px'
    }

    var element=document.getElementById(id2);
   

    if(element.classList.contains('isHideOptional')){
      element.classList.remove("isHideOptional");
     element.classList.add("isUnhideOptional");
     element.classList.add("col-6");
         
    }else if(element.classList.contains('col-6')){
      element.classList.remove("col-6");
      element.classList.remove("isUnhideOptional");
      element.classList.add("isHideOptional");
    }
  }

  onOptionalCheck1(){
   if(this.isOptional == false){
     this.isOptional = true;
   }else{
    this.isOptional = false;
   }
  }
  getMarkingScheme(scrollDataModal, parentIndex: number, childIndex: number, currentIndex: number) {
    this.parentIndex = parentIndex;
    this.childIndex = childIndex;
    this.currentIndex = currentIndex;
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[parentIndex];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let subjectformGroup = <FormGroup>Subjectcontrol.controls[childIndex];
    let sectionControl = <FormArray>subjectformGroup.controls['sections'];
    let sectionformGroup = <FormGroup>sectionControl.controls[currentIndex];
    let markingSchemeArray = <FormArray>sectionformGroup.controls['markingScheme'];
    console.log("sectionformGroup",sectionformGroup)
    // $("#markingSchemeModal").modal('show');
    this.modalService.open(scrollDataModal, { size: 'lg' });
    if (markingSchemeArray != null && markingSchemeArray.length > 0) {
      let markingScheme = <FormGroup>markingSchemeArray.controls[0];
      this.markingSchemeForm.patchValue({
        schemeType: markingScheme.controls['schemeType'].value,
        optionCount: markingScheme.controls['optionCount'].value,
        name: markingScheme.controls['name'].value,
        uniqueId: markingScheme.controls['uniqueId'].value,
        highestMarks: markingScheme.controls['highestMarks'].value,
        leaveMarks: markingScheme.controls['leaveMarks'].value,
        defaultMarks: markingScheme.controls['defaultMarks'].value,
      });

      this.markingSchemeForm.setControl('subMarkingScheme', this.fb.array(this.patchSubMarkingScheme(markingScheme.controls['subMarkingScheme'].value)));
    } else {
      this.initMarkingSchemeForm();
    }
  }

  saveAndImportScheme() {
    // this.examPatternService.spinner.next(true);
    this.markingSchemeForm.controls['organizationUid'].setValue(sessionStorage.getItem('orgUniqueId'));
    this.markingSchemeForm.controls['orgId'].setValue(sessionStorage.getItem('organizationId'));
    return this.examService.saveMarkingScheme(this.markingSchemeForm.value).subscribe(
      res => {
        if (res) {
          this.markingSchemeForm.controls['schemeType'].setValue(1),
          this.markingSchemeForm.controls['uniqueId'].setValue(res.uniqueId),
          this.saveScheme();
          this.modalService.dismissAll();
          // this.examPatternService.spinner.next(false);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        // } else {
        //   // this.examPatternService.spinner.next(false);
        //   this.toastService.ShowWarning(error.error.errorMessage);
        // }
      });
  }

  saveScheme() {
    this.isAddSchemeSubmit=true;
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[this.parentIndex];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let subjectformGroup = <FormGroup>Subjectcontrol.controls[this.childIndex];
    let sectionControl = <FormArray>subjectformGroup.controls['sections'];
    let sectionformGroup = <FormGroup>sectionControl.controls[this.currentIndex];
    console.log("sectionformGroup",sectionformGroup)
    sectionformGroup.controls['isAddImportScheme'].setValue(true)
    let markingSchemeArray = <FormArray>sectionformGroup.controls['markingScheme'];
    console.log("sectionformGroup",sectionformGroup)
    for (let i = markingSchemeArray.length - 1; i >= 0; i--) {
      markingSchemeArray.removeAt(i);
    }
    markingSchemeArray.push(this.fb.group({
      schemeType: this.markingSchemeForm.controls['schemeType'].value,
      optionCount: this.markingSchemeForm.controls['optionCount'].value,
      name: this.markingSchemeForm.controls['name'].value,
      uniqueId: this.markingSchemeForm.controls['uniqueId'].value,
      highestMarks: this.markingSchemeForm.controls['highestMarks'].value,
      leaveMarks: this.markingSchemeForm.controls['leaveMarks'].value,
      defaultMarks: this.markingSchemeForm.controls['defaultMarks'].value,
      subMarkingScheme: this.fb.array(this.patchSubMarkingScheme(this.markingSchemeForm.controls['subMarkingScheme'].value))
    }));

  }

  patchSubMarkingScheme(subMarkingScheme: Array<any>) {
    let subArray = [];
    if (subMarkingScheme != null && subMarkingScheme.length > 0) {
      for (let j = 0; j < subMarkingScheme.length; j++) {
        subArray.push(this.fb.group({
          correctOption: subMarkingScheme[j].correctOption,
          rightOption: subMarkingScheme[j].rightOption,
          wrongOption: subMarkingScheme[j].wrongOption,
          marks: subMarkingScheme[j].marks,
        }));
      }
    }
    return subArray;
  }

  getInstruction(parentIndex: number, childIndex: number, currentIndex: number, content: string) {

    this.parentIndex = parentIndex;
    this.childIndex = childIndex;
    this.currentIndex = currentIndex;
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[parentIndex];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let subjectformGroup = <FormGroup>Subjectcontrol.controls[childIndex];
    let sectionControl = <FormArray>subjectformGroup.controls['sections'];
    let sectionformGroup = <FormGroup>sectionControl.controls[currentIndex];
    let instructionArray = <FormArray>sectionformGroup.controls['instructionDto'];

    console.log("instructionArray",instructionArray)
    this.modalService.open(content);

    if (instructionArray != null && instructionArray.length > 0) {
      let instruction = <FormGroup>instructionArray.controls[0];
      this.instructionForm.controls['instructionUid'].setValue(instruction.controls['instructionUid'].value);
      this.instructionForm.controls['instructionType'].setValue(instruction.controls['instructionType'].value);
      this.instructionForm.controls['instructionTextOrImage'].setValue(instruction.controls['instructionTextOrImage'].value);
    } else {
      this.initInstructionForm();
    }

  }

  saveInstruction() {
    this.spinner1.show()
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examformGroup = <FormGroup>control.controls[this.parentIndex];
    let Subjectcontrol = <FormArray>examformGroup.controls['subjects'];
    let subjectformGroup = <FormGroup>Subjectcontrol.controls[this.childIndex];
    let sectionControl = <FormArray>subjectformGroup.controls['sections'];
    let sectionformGroup = <FormGroup>sectionControl.controls[this.currentIndex];
    let instructionArray = <FormArray>sectionformGroup.controls['instructionDto'];

    for (let i = instructionArray.length - 1; i >= 0; i--) {
      instructionArray.removeAt(i);
    }

    instructionArray.push(this.fb.group({
      instructionUid: this.instructionForm.controls['instructionUid'].value,
      instructionType: this.instructionForm.controls['instructionType'].value,
      instructionTextOrImage: this.instructionForm.controls['instructionTextOrImage'].value
    }));

    console.log("instructionArray",instructionArray);
  

    this.spinner1.hide()
  }

  importInstruction(childIndex: number, parentIndex: number) {

  }



  isPaperTwo(e: any, index: number) {
    this.examPatternForm.controls['isPaperTwo'].setValue(e);
    let control = <FormArray>this.examPatternForm.controls['examPatterns'];
    if (this.examPatternForm.controls['isPaperTwo'].value == true) {
      control.push(this.initExamPatternForm());
      let formGroup = <FormGroup>control.controls[index + 1];
      let Subjectcontrol = <FormArray>formGroup.controls['subjects'];
      this.addSubjectsWithValue(this.jeeSubjects, Subjectcontrol);
    } else {
      let formGroup = <FormGroup>control.controls[index + 1];
      let Subjectcontrol = <FormArray>formGroup.controls['subjects'];
      control.removeAt(index + 1);
      this.clearSectionFormArray(Subjectcontrol);
    }
  }

  isPaperSame(e) {
    this.examPatternForm.controls['isPaperSame'].setValue(e);
  }

  saveExamPattern() {
    this.spinner1.show();
    this.spinner = true;
    this.examService.addExamPattern(this.examPatternForm.value).subscribe(
      res => {
        if (res) {
          this.toastService.showSuccess("Exam Pattern added successfully..");
          // this.isPageChanged = false;
          this.spinner = false;
          this.spinner1.hide()
          this.router.navigate(['/newModule/exam/examList']);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner1.hide()
        // } else {
        //   this.spinner = false;
        //   this.spinner1.hide()
        //   this.toastService.ShowWarning(error.error.errorMessage);
        // }
        this.spinner = false;
        this.spinner1.hide()
      });
  }


  onSelectFile(e: any) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    let max_size: number = 2085184;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastService.ShowWarning("invalid format");
      return;
    }
    if (e.target.files[0].size > max_size) {
      this.toastService.ShowWarning("Maximum size of 2 MB is allowed..");
      this.instructionForm.controls['instructionTextOrImage'].setValue('');
      return false;
    } else {
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }

  _handleReaderLoaded(e: any) {
    this.imageSrc = e.target.result;
    this.instructionForm.controls['instructionTextOrImage'].setValue(this.imageSrc);

  }

  getInstructionType() {
    this.instructionForm.controls['instructionTextOrImage'].setValue('');
  }

  checkExists(value: string, parentIndex: number, childIndex: number, currentIndex: number) {

    let examPatternArray = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examPatternGroup = <FormGroup>examPatternArray.controls[parentIndex];
    let subjectArray = <FormArray>examPatternGroup.controls['subjects'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sections'];
    for (let k = 0; k < sectionArray.length; k++) {
      let sectionGroup = <FormGroup>sectionArray.controls[k];
      let name: string = sectionGroup.controls['name'].value;
      if (k != currentIndex && name != "" && name.toUpperCase() == value.toUpperCase()) {
        this.toastService.ShowWarning("Section Name already added");
        let currentSecGroup = <FormGroup>sectionArray.controls[currentIndex];
        currentSecGroup.controls['name'].setValue('');
        return false;
      }
    }
  }

  addSection(parentIndex: number, childIndex: number) {
    let examPatternArray = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examPatternGroup = <FormGroup>examPatternArray.controls[parentIndex];
    let subjectArray = <FormArray>examPatternGroup.controls['subjects'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sections'];
    sectionArray.push(this.initSections());
  }

  removeSection(parentIndex: number, childIndex: number, currentIndex: number) {
    let examPatternArray = <FormArray>this.examPatternForm.controls['examPatterns'];
    let examPatternGroup = <FormGroup>examPatternArray.controls[parentIndex];
    let subjectArray = <FormArray>examPatternGroup.controls['subjects'];
    let subjectGroup = <FormGroup>subjectArray.controls[childIndex];
    let sectionArray = <FormArray>subjectGroup.controls['sections'];
    sectionArray.removeAt(currentIndex);
  }

  getExamType(){
    this.spinner1.show();
    this.examService.getExamType().subscribe(
      res => {
        if (res) {
          this.spinner1.hide();
          this.examTypeList = res;
          console.log("res",res)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner1.hide();
        // } else {
        //   this.spinner1.hide();
        //   this.toastService.ShowWarning(error.error.errorMessage);
        // }
        this.spinner1.hide();
      });
  }

  getObjectiveQuestionTypeList(){
    this.spinner1.show();
    this.examService.getObjectiveQuestionTypeList().subscribe(
      res => {
        if (res) {
          this.spinner1.hide();
          console.log("Objective",res);
          this.customObjectiveExamQuestionType = res;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");this.spinner1.hide();
        // } else {
        //   this.spinner1.hide();
        //   this.toastService.ShowWarning(error.error.errorMessage);
        // }
        this.spinner1.hide();
      });
  }

  getsubjectiveQuestionTypeList(){
    this.spinner1.show();
    this.examService.getsubjectiveQuestionTypeList().subscribe(
      res => {
        if (res) {
          this.spinner1.hide();
          console.log("Subjective",res);
          this.jeeAdvanceSubjectiveQuestionType = res;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner1.hide();
        // } else {
        //   this.spinner1.hide();
        //   this.toastService.ShowWarning(error.error.errorMessage);
        // }
        this.spinner1.hide();
      });
  }

  isCollaps(id,headId){
    
    var ele=document.getElementById(id);
    
    if(ele.classList.contains('isUnhide')){
      ele.classList.remove("isUnhide");
       ele.classList.add("isHide");
        
    }else if(ele.classList.contains('isHide')){
      ele.classList.remove("isHide");
      ele.classList.add("isUnhide");
      
    }

    var element=document.getElementById(headId);
    if(element.classList.contains('fa-minus')){
      element.classList.remove("fa-minus");
      element.classList.add("fa-plus");
        
    }else if(element.classList.contains('fa-plus')){
      element.classList.remove("fa-plus");
      element.classList.add("fa-minus");
      
    }
  
  }

  createPattern(){
    console.log("test")
  }
  instructiionAvailable=true;

  files: any = [];
  noRadioSelected=true;
  uploadFile(event) {
    for (let index = 0; index < event.length; index++) {
      const element = event[index];
      this.files.push(element.name)
    }  
  }
  deleteAttachment(index) {
    this.files.splice(index, 1)
  }



}
