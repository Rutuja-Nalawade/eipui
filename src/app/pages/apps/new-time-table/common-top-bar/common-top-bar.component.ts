import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-common-top-bar',
  templateUrl: './common-top-bar.component.html',
  styleUrls: ['./common-top-bar.component.scss']
})
export class CommonTopBarComponent implements OnInit {
  tabNumber = 1;
  isAddNew = false;
  type = 1;
  timetableType = 1;
  timetableData: any = {};
  constructor(private router: Router, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      if (params) {
        switch (params['id']) {
          case 'timeTable':
            this.tabNumber = 1;
            break;
          case 'substitution':
            this.tabNumber = 2;
            break;
        }
      }
    });
  }

  ngOnInit() {
  }
  tabClick(index) {
    this.tabNumber = index;
    switch (index) {
      case 1:
        this.router.navigate(['newModule/timeTable/lists/timeTable']);
        break;
      case 2:
        this.router.navigate(['newModule/timeTable/lists/substitution']);
        break;
    }
  }
  createTimeTable() {
    this.isAddNew = true;
    this.type = 1;
  }
  rightsideform(type) {
    this.isAddNew = true;
    this.type = type;
  }
  openFilter() {

  }
  findRecords(value) {

  }
  addNewLecture() {
    this.isAddNew = true;
  }
  closeAddNewForm(event) {
    console.log(event);
    this.isAddNew = false;
    if (event) {
      this.timetableData = event;
      this.rightsideform(6);
    }
    if (event == "0") {
      this.timetableType = 2;
    }
  }

  isBlur(value) {
    //   console.log("value",value)
    //   this.type=0
    // this.isAddNew=value;

  }
}
