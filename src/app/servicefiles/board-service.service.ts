import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BoardServiceService {

  constructor(private http: HttpClient) { }

  public getMasterBoardList() {
    return this.http.get<any>(environment.apiUrl + '/master/get/board');
  }

  public getAcademicBoardsList() {
    return this.http.get<any>(environment.apiUrl + '/academic-board/get/list');
  }

  public getAcademicBoardByCourseUid(courseUniqueId: any) {
    return this.http.get<any>(environment.apiUrl + '/academic-board/get/by/course/' + courseUniqueId);
  }
  public deleteBoardByBoardUid(boardUniqueId: any) {
    console.log('delete id recieved' + boardUniqueId)
    return this.http.delete<any>(environment.apiUrl + '/academic-board/delete/', boardUniqueId);
  }
  public getBoardByCourseUid(courseUniqueIds: any) {
    return this.http.post<any>(environment.apiUrl + '/academic-board/get/by-course-list', courseUniqueIds);
  }
}
