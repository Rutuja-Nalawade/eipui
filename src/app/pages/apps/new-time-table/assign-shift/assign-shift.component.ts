import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';

@Component({
  selector: 'app-assign-shift',
  templateUrl: './assign-shift.component.html',
  styleUrls: ['./assign-shift.component.scss']
})
export class AssignShiftComponent implements OnInit {

  isShowExpireCourse = false;
  courseListWithBatch = [];
  organizationShifts = [];
  allCourseListWithBatch: any = [];
  isPeriodFormOpen = false;
  isViewLectTimetable = false;
  pageIndex = 0;
  settings = {};
  courseBatchList: FormGroup;
  selectedBatch: any = {};
  @Input() timetableData: any;
  @Input() type: any;
  @Input() isEdit: any;
  @Output() close = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder, private toastService: ToastsupportService, private timeTableService: TimeTableServiceService, private spinner: NgxSpinnerService, private datePipe: DatePipe, private shiftService: ShiftServiceService) {

  }

  async ngOnInit() {
    this.initCourseBatchList();
    await this.getOrganizationShifts();
    this.getOrganizationBatchesByCourse();
    this.settings = {
      text: "Select",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "myclass custom-class"
    };
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['type'] && changes['type'].currentValue == 6) {

    }
  }
  initCourseBatchList() {
    this.courseBatchList = this.formBuilder.group({
      batchDtoList: this.formBuilder.array([])
    })
  }
  getOrganizationBatchesByCourse() {
    this.spinner.show();
    this.timeTableService.getBatchList(this.pageIndex).subscribe(
      response => {
        this.spinner.hide();
        // this.allCourseListWithBatch = response;
        let courseBatchDtoList = [];
        if (response && response.length > 0) {
          this.courseListWithBatch = response;
          response.map(item => {
            courseBatchDtoList.push(this.formBuilder.group({
              name: item.name,
              uniqueId: item.uniqueId,
              startDate: item.startDate,
              endDate: item.endDate,
              isExpired: false,
              batchWithShifts: [this.patchBatchWithShift(item.batchWithShifts), Validators.nullValidator],
            }))
          })
          this.courseBatchList.setControl('batchDtoList', this.formBuilder.array(courseBatchDtoList));
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );
  }
  patchBatchWithShift(itemList) {
    let list = [];
    if (itemList != null && itemList.length > 0) {
      for (let index = 0; index < itemList.length; index++) {
        const element = itemList[index];
        let dto = {
          "name": element.name,
          "shifts": this.patchShift(element.shifts),
          "shiftLists": element.shifts,
          "uniqueId": element.uniqueId,
          "isPeriodAssigned": element.isPeriodAssigned
        }
        list.push(dto);
      }
    }
    return list;
  }
  patchShift(itemList) {
    let list = [];
    if (itemList != null && itemList.length > 0) {
      for (let index = 0; index < itemList.length; index++) {
        const element = itemList[index];
        list.push(element.unique_id);
      }
    }
    return list;

  }
  getOrganizationShifts() {
    return this.shiftService.getAllShifts().subscribe(
      res => {
        if (res) {
          let organizationShifts = res.list;

          if (organizationShifts != null && organizationShifts.length > 0) {
            let shiftList = [];
            for (let i = 0; i < organizationShifts.length; i++) {
              let shiftDto = {
                "id": organizationShifts[i].id,
                "name": organizationShifts[i].name,
                "itemName": organizationShifts[i].name + "-(" + organizationShifts[i].startTime + "-" + organizationShifts[i].endTime + ")",
                "shiftUid": organizationShifts[i].unique_id,
                "isLinked": false,
                "startTime": organizationShifts[i].startTime,
                "endTime": organizationShifts[i].endTime
              }
              shiftList.push(shiftDto);
            }
            this.organizationShifts = shiftList;
          }
          console.log("this.SHIFTworkDayList", this.organizationShifts);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {

        // }
      });
  }
  showHideExpireCourse() {
    this.isShowExpireCourse = !this.isShowExpireCourse;
    if (!this.isShowExpireCourse) {
      this.courseBatchList = this.allCourseListWithBatch;
    } else {
      this.allCourseListWithBatch = this.courseBatchList;
      let courseBatchList = <FormArray>this.courseBatchList.controls['batchDtoList'];
      let list = [];
      for (let i = 0; i < courseBatchList.length; i++) {
        const element = <FormGroup>courseBatchList.controls[i];
        console.log(element.controls.isExpired.value);
        if (element.controls.isExpired.value == false) {
          list.push(element);
        }
      }
      this.courseBatchList.setControl('batchDtoList', this.formBuilder.array(list));
      // let courseLsit = this.allCourseListWithBatch.filter(x => x.isExpired == false);
      // this.courseBatchList = courseLsit;
    }
  }
  cancel() {
    this.close.emit("");
  }
  save() {
    // this.close.emit("");
    console.log(this.courseBatchList.value);
    if (this.courseBatchList.value.batchDtoList && this.courseBatchList.value.batchDtoList.length > 0) {

      this.spinner.show();
      let batchDtoList = this.courseBatchList.value.batchDtoList;
      let dtoList = [];
      for (let i = 0; i < batchDtoList.length; i++) {
        const element = batchDtoList[i];
        for (let j = 0; j < element.batchWithShifts.length; j++) {
          const elm = element.batchWithShifts[j];
          if (elm.shifts.length > 0) {
            dtoList.push({
              uniqueId: elm.uniqueId,
              shiftUniqueIds: elm.shifts
            })
          }
        }
      }
      if (dtoList.length > 0) {
        console.log(dtoList);
        if (!this.isEdit) {
          this.timeTableService.updateBatchShifts(dtoList).subscribe(
            (res) => {
              if (res) {
                this.toastService.showSuccess("Assign shifts to batch save succesfully");
                this.spinner.hide();
                this.isViewLectTimetable = true;
              }
            },
            (error: HttpErrorResponse) => {
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        } else {
          this.timeTableService.saveBatchShifts(dtoList).subscribe(
            (res) => {
              if (res) {
                this.toastService.showSuccess("Assign shifts to batch save succesfully");
                this.spinner.hide();
                this.isViewLectTimetable = true;
              }
            },
            (error: HttpErrorResponse) => {
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        }

      } else {
        this.spinner.hide();
        this.toastService.ShowWarning("Please assign atleast one shift to batch.")
      }
    }
  }
  viewPeriod(data) {
    console.log(data);
    if (data.shifts && data.shifts.length > 0) {
      let dtoList = [{
        uniqueId: data.uniqueId,
        shiftUniqueIds: data.shifts
      }];
      this.spinner.show();
      this.timeTableService.saveBatchShifts(dtoList).subscribe(
        (res) => {
          if (res) {
            // this.toastService.showSuccess("Assign shifts to batch save succesfully");
            this.spinner.hide();
            this.selectedBatch = data;
            this.isPeriodFormOpen = true;
          } else {
            this.spinner.hide();
            this.toastService.ShowError("Shift not assigned");
          }
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });

    } else {
      this.toastService.ShowWarning("Please select atleast one shift")
    }
  }
  closeAddNewForm(event) {
    this.isPeriodFormOpen = false;
    this.isViewLectTimetable = false;
    if (event == "0") {
      this.close.emit("0");
    }
  }
  changeShift(event, item, i, j) {
    console.log(event);
    let shifts = [];
    let shiftLists = [];
    if (event.length > 0) {
      for (let i = 0; i < event.length; i++) {
        shifts.push(event[i].shiftUid)
        shiftLists.push({ name: event[i].name, unique_id: event[i].shiftUid })
      }
    }
    item.shifts = shifts;
    item.shiftLists = shiftLists;
    console.log(item);
  }
  onItemSelect($event: any) {

  }

  onItemDeSelect($event: any) {

  }

  onSelectAll($event: any) {

  }

  onDeSelectAll($event: any) {

  }
}
