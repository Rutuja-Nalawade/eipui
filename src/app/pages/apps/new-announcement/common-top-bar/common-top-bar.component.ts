import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-common-top-bar',
  templateUrl: './common-top-bar.component.html',
  styleUrls: ['./common-top-bar.component.scss']
})
export class CommonTopBarComponent implements OnInit {
  createTempleteForm:FormGroup;
  isShowcreateTempleteForm:boolean = false;
  tabNumber = 1;
  isShowfilter = false;
  submitted = false;
  shiftTypeList = [{ id: 1, name: "Teaching" }, { id: 2, name: "Non-Teaching" }, { id: 3, name: "Both" }];
  selectedShiftTypes = [];
  selectedTeachers = [];
  selectedDays = [];
  workingDayList = [];
  userData: any = {};
  pageIndex = 0;
  teacherList = [];
  filterData = {};
  isAddNew = false;
  isViewShift = false;
  isBulkAssign = false;
  viewDetails = false;
  composeClicked:boolean = false;
  constructor(private formBuilder:FormBuilder,private spinner: NgxSpinnerService, private _commonService: NewCommonService, private toastService: ToastsupportService, private router: Router, private route: ActivatedRoute) {
    this.userData = this._commonService.getUserData();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params) {
        switch (params['id']) {
          case 'learners':
            this.tabNumber = 1;
            break;
          case 'educators':
            this.tabNumber = 2;
            break;
          case 'organizations':
            this.tabNumber = 3;
            break;
        }
      }
    });
  }

  tabClick(index) {
    this.viewDetails = false;
    this.tabNumber = index;
    switch (index) {
      case 1:
        this.router.navigate(['newModule/announcement/lists/learners']);
        break;
      case 2:
        this.router.navigate(['newModule/announcement/lists/educators']);
        break;
      case 3:
        this.router.navigate(['newModule/announcement/lists/organizations']);
        break;
    }
  }
  addShift() {
    this.isAddNew = true;
  }
  findRecords(value) {

  }
  openFilter() {
    // this.isShowfilter = true;
    // this.getOrgWorkDays();
    // this.getTeachers(false);
  }
  OpenBulkAssignForm() {
    this.isBulkAssign = true;
  }
  closeFilter() {
    this.isShowfilter = false;
  }
  onChange(type) {
    switch (type) {
      case 1:
        this.selectedShiftTypes = [];
        this.selectedShiftTypes.push(type.id);
        break;
      case 2:
        this.selectedDays = [];
        this.selectedDays.push(type.dayValue);
        break;
      case 3:
        this.selectedTeachers = [];
        this.selectedTeachers.push(type.id);
        break;
    }
  }
  getOrgWorkDays() {
    // this.spinner.show();
    // this.workTimeServices.getActiveOrgDays(this.userData.orgUniqueId).subscribe(
    //   res => {
    //     if (res) {
    //       this.workingDayList = res;
    //     }
    //     this.spinner.hide();
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       this.toastService.ShowError(error);
    //       //this.sessionService.checkUnauthorizedRequest(error.error);
    //     }
    //     this.spinner.hide();
    //   });
  }
  getTeachers(isLoadMoreData: boolean) {

    let serachDto = null;
    this.spinner.show();
    if (isLoadMoreData) {
      this.pageIndex = this.pageIndex + 1;
    } else {
      this.spinner.show();
      // this.setStaffListDivCollapsed = false;
      this.pageIndex = 0;
      this.teacherList = [];
      this.filterData = null;
    }

    if (this.filterData == null) {
      serachDto = {
        "orgList": [parseInt(this.userData.organizationId)],
        "pageIndex": this.pageIndex,
        "subjectList": []
      };
    } else {
      serachDto = this.filterData;
    }
    // this._teacherService.getOrgTeacherListWithPagination(serachDto).subscribe(
    //   response => {
    //     let teacherList = response;
    //     if (this.teacherList == null || this.teacherList.length == 0) {
    //       this.teacherList = teacherList;
    //     }
    //     this.spinner.hide();
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {
    //       this.spinner.hide();
    //     } else {
    //       this.toastService.ShowError(error);
    //       this.spinner.hide();
    //     }
      // });
  }
  applyFilter() {
    if (this.selectedShiftTypes.length == 0 && this.selectedDays.length == 0 && this.selectedTeachers.length == 0) {
      this.toastService.ShowWarning("Please select atleast one option!")
    }
  }
  closeAddNewForm(event) {
    this.isAddNew = false;
  }
  isViewDetails(event) {
    if (event == "true") {
      this.viewDetails = true;
    } else {
      this.viewDetails = false;
    }
  }
  closeBulkAssign(event) {
    this.isBulkAssign = false;
  }
  addTempleteCategory() {
    this.initTempleteCategoryArray()
    this.isShowcreateTempleteForm = true;
  }

  closeCreateTempleteForm(){
     this.createTempleteForm.reset()
    // this.isEdit=false;
    this.isShowcreateTempleteForm=false;
  }

  initTempleteCategoryArray() {
    this.createTempleteForm = this.formBuilder.group({
      name: [null, Validators.required],
      type: [null, Validators.required],
    });
  }

  compose(){
    this.composeClicked = true;
    document.body.classList.add("hideScroll");
  }

  closeCompose() {
    this.composeClicked = false;
    document.querySelector('body').classList.remove('hideScroll');
  }

}
