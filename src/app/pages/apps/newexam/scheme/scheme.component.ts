import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
// import { ExamPatternService } from 'src/app/services/exampattern.service';
import { ExamService } from './../exam.service';
import { WizardComponent as BaseWizardComponent } from 'angular-archwizard';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-scheme',
    templateUrl: './scheme.component.html',
    styleUrls: ['./scheme.component.scss']
})

export class SchemeComponent implements OnInit {
    @ViewChild('modelwizardForm', { static: false }) modelwizard: BaseWizardComponent;
    @Input() markingSchemeForm: FormGroup;
    spinner: boolean = false;
    markingSchemeList = [];
    markingScheme: any;
    isAddSchemeSubmit=false;
    constructor(private fb: FormBuilder, private modalService: NgbModal, private toastService: ToastsupportService, private examPatternService: ExamService) { }

    ngOnInit() {
        this.examPatternService.spinner.subscribe(
            (value: any) => {
                this.spinner = value;
            });
        this.getSchemeList();
    }

    getRightMarks(value: number) {
        this.examPatternService.rightMarks.next(value);
    }

    initSubMarkingScheme() {
        return this.fb.group({
            correctOption: ['', Validators.required],
            rightOption: ['', Validators.required],
            wrongOption: [0, Validators.required],
            marks: ['', Validators.required],
        });
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }

    getSchemeList() {
        this.markingSchemeList = [];
        return this.examPatternService.getMarkingSchemeList(sessionStorage.getItem('organizationId')).subscribe(
        // return this.examPatternService.getMarkingSchemeList(3).subscribe(

            res => {
                this.markingSchemeList = res;
                if (this.markingSchemeForm.controls['uniqueId'].value != '') {
                    this.markingScheme = this.markingSchemeList.find(x => x.uniqueId == this.markingSchemeForm.controls['uniqueId'].value);
                }
            },
            (error: HttpErrorResponse) => {
                // if (error.error instanceof Error) {
                //     console.log("Client-side error occured.");
                // } else {
                //     this.toastService.ShowWarning(error.error.errorMessage);
                // }
            });
    }

    wizardFirstNextStep() {
    }
    getSelectedScheme(value: string) {
        this.markingScheme = this.markingSchemeList.find(x => x.uniqueId == value);
        console.log("markingScheme",this.markingScheme);
       
       
        this.examPatternService.rightMarks.next(this.markingScheme.highestMarks);

        this.markingSchemeForm.controls['name'].setValue(this.markingScheme.name);
        this.markingSchemeForm.controls['highestMarks'].setValue(this.markingScheme.highestMarks);
        this.markingSchemeForm.controls['leaveMarks'].setValue(this.markingScheme.leaveMarks);
        this.markingSchemeForm.controls['defaultMarks'].setValue(this.markingScheme.defaultMarks);

    }

    formSubmit() {
        // if (this.examPatternForm.valid) {
          this.modelwizard.navigation.goToNextStep();
        // }
    
      }

    getMarks(value: number, index: number) {
        let schemeArray = <FormArray>this.markingSchemeForm.controls['subMarkingScheme'];
        let currentScheme = <FormGroup>schemeArray.controls[index];
        if (value > parseInt(this.markingSchemeForm.controls['highestMarks'].value)) {
            currentScheme.controls['marks'].setValue('');
            this.toastService.ShowWarning("Marks should be less then Highest entered marks..");
            return false;
        }
    }

    getValue(value: number, index: number, optionType: number) {
        let schemeArray = <FormArray>this.markingSchemeForm.controls['subMarkingScheme'];
        let currentScheme = <FormGroup>schemeArray.controls[index];
        for (let i = 0; i < schemeArray.length; i++) {
            let scheme = <FormGroup>schemeArray.controls[i];
            if (i != index && currentScheme.controls['correctOption'].value == scheme.controls['correctOption'].value && currentScheme.controls['rightOption'].value == scheme.controls['rightOption'].value) {
                currentScheme.controls['rightOption'].setValue('');
                this.toastService.ShowWarning("Duplicate scheme found.Please check..");
                return false;
            }
        }

        let optionCount: number = this.markingSchemeForm.controls['optionCount'].value;
        let schemeGroup = <FormGroup>schemeArray.controls[index];

        if (optionType == 2 && parseInt(schemeGroup.controls['rightOption'].value) > parseInt(schemeGroup.controls['correctOption'].value)) {
            this.toastService.ShowWarning("Right Option should be less then or equal to Correct Option..");
            schemeGroup.controls['rightOption'].setValue(0);
        }
        if (value > optionCount) {
            this.toastService.ShowWarning("Option count should be less than or equal to " + optionCount + " .");
            switch (optionType) {
                case 1:
                    schemeGroup.controls['correctOption'].setValue(0);
                    break;
                case 2:
                    schemeGroup.controls['rightOption'].setValue(0);
                    break;
                case 3:
                    schemeGroup.controls['wrongOption'].setValue(0);
                    break;
            }
        }
    }

    addNewSubScheme() {
        let formArrayOfSubMarkingScheme = <FormArray>this.markingSchemeForm.controls['subMarkingScheme'];
        formArrayOfSubMarkingScheme.push(this.initSubMarkingScheme());
    }

    removeNewSubScheme(index: number) {
        let formArrayOfSubMarkingScheme = <FormArray>this.markingSchemeForm.controls['subMarkingScheme'];
        formArrayOfSubMarkingScheme.removeAt(index);
    }

    changeOptionScemeSelect(event){
       
        switch(event){
            case "1":
              
                break;
            case "2":
                this.markingSchemeForm.controls['name'].setValue("");
                this.markingSchemeForm.controls['highestMarks'].setValue("");
                this.markingSchemeForm.controls['leaveMarks'].setValue("");
                this.markingSchemeForm.controls['defaultMarks'].setValue("");
                break;
        }

    }

    checkExists(value: string) {
        var sceme = this.markingSchemeList.find(x => x.name == value);

        this.markingSchemeList.forEach(element => {
           var valueName =  element.name;
           if(valueName.toUpperCase() == value.toUpperCase()){
            this.markingSchemeForm.controls['name'].setValue("");
            this.toastService.ShowWarning("Scheme Name already exist");
           }
        });
    }

    
}