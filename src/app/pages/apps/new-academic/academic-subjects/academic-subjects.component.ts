import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { AcademicService } from '../academic.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
@Component({
  selector: 'app-academic-subjects',
  templateUrl: './academic-subjects.component.html',
  styleUrls: ['./academic-subjects.component.scss']
})
export class AcademicSubjectsComponent implements OnInit {
  isFromSetup: boolean = false;
  @Input() currentTab: Number;
  subjectList = [];
  isEditAdd: boolean = false;
  data: any;
  subjectLinkingSubjectForm: FormGroup;
  isAdd: boolean = false;
  constructor(private _subjectService: SubjectServiceService,
    private formBuilder: FormBuilder, private spinner: NgxSpinnerService,
    private _toastService: ToastsupportService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("currentValue",changes['currentTab']);

    if (changes['currentTab'].currentValue == 5) {
      this.isEditAdd = false;
      this.getSujectList();

    }
  }

  ngOnInit() {
  }
  getSujectList() {
    this.spinner.show()
    this._subjectService.getCourseHasSubject(true).subscribe(
      res => {
        this.subjectList = [];
        if (res) {
          if (res.success == true) {
            this.subjectList = res.list;
          } else {
            this._toastService.ShowWarning('No Course subjects found')
          }

          // console.log("getSujectList ",JSON.stringify(res))

          // this.dataLoading = false;

        }

        this.spinner.hide()
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
           // this.dataLoading = false;
           // this.toastService.ShowInfo(error);
        // }

        this.spinner.hide()
      });
  }
  addEditSubject(subject) {
    // console.log('subject',subject)
    this.data = subject;
    this.isAdd = false;
    this.initLinkSubjectForm();
    this.isEditAdd = true;

  }
  addSubject() {
    this.isAdd = true;
    this.data = '';
    this.initLinkSubjectForm();
    this.isEditAdd = true;

  }
  backPage(value) {
    // console.log("value",value)
    this.isEditAdd = value;
    this.getSujectList()
  }

  initLinkSubjectForm() {
    this.subjectLinkingSubjectForm = this.formBuilder.group({
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      orgId: [sessionStorage.getItem('organizationId'), Validators.required],
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      linkedSubjectList: this.formBuilder.array([])
    });
  }

}
