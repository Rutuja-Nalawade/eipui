export interface userFilterDto {
    orgList: Array<number>;
    academicCalendarList : Array<number>;
    roleList: Array<number>;
    pageIndex:number;
    boardList: Array<number>;
    gradeList: Array<number>;
    courseList: Array<number>;
    batchList: Array<number>;
    yearList:Array<string>;
    subjectList: Array<number>;
    searchKey : string;
    searchType : number;
}