import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, SimpleChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SetupComponent } from 'src/app/account/new-authentication/setup/setup.component';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { AcademicsessionService } from 'src/app/servicefiles/academicsession.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-course-and-refrences',
  templateUrl: './course-and-refrences.component.html',
  styleUrls: ['./course-and-refrences.component.scss']
})
export class CourseAndRefrencesComponent implements OnInit {

  userData: any;
  // courseAndRefForm: FormGroup;
  loading: boolean = false;
  isEdit: boolean = false;
  submitted: boolean = false;
  settings = {};
  courseList = [];
  academicsessionlist: any = [];
  masterCourseList: any = [];
  gradeList: any = [];
  boardList: any = [];
  oldRow: any;
  bsInlineValue: any;
  bsDate: any;
  showDatePicker: boolean = false;
  currentEditRowIndex: any;
  yearsData: any = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  daysData: any = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  monthData: any = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
  isOutSideDatepickerClick = false;
  ismasterCourseFromServer = false;
  @Input() selectedIndex: any;
  @Input() editData: any;
  @Input() isAddNew: any;
  @Input() isFromacademic: any;
  @Input() courseAndRefForm: FormGroup;
  @Input() isFromSetup: any;
  @ViewChild('container', { static: true }) container;
  @ViewChild('datePicker', { static: true }) datePicker;
  // @ViewChild('stepper1', {static:true}) stepper: MatStepper;
  @Output() newItemEvent = new EventEmitter<string>();
  constructor(private setup: SetupComponent, private _gradeService: GradeServiceService, private _boardService: BoardServiceService, private _courseService: CourseServiceService, private _academicsessionService: AcademicsessionService
    , private formBuilder: FormBuilder, private datePipe: DatePipe, private toastService: ToastsupportService) {
    // document.addEventListener('click', this.offClickHandler.bind(this));
  }

  ngOnInit() {
    this.initCourseAndRefForm();
    this.settings = {
      singleSelection: false,
      idField: 'id',
      textField: 'itemName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: false,
      enableCheckAll: false
    };
  }
  async ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedIndex'] && changes['selectedIndex'].currentValue == 1) {
      // setup config
      await this.getMasterCourseList();
      await this.getAcademicSessionList();
      this.getBoardList();
      this.getGradeList();
      this.getCourseList();
    } else if (changes['selectedIndex'] && changes['selectedIndex'].currentValue == 5) {
      // academic config
      await this.getMasterCourseList();
      await this.getAcademicSessionList();
      this.getBoardList();
      this.getGradeList();
      this.patchCourse(this.editData, true);
    } else if (changes['selectedIndex'] && changes['selectedIndex'].currentValue == 3) {
      await this.getMasterCourseList();
      await this.getAcademicSessionList();
      this.getBoardList();
      this.getGradeList();
      this.patchCourse(this.editData, true);
    } else if (changes['selectedIndex'] && changes['selectedIndex'].currentValue == 6) {
      await this.getMasterCourseList();
      await this.getAcademicSessionList();
      this.getBoardList();
      this.getGradeList();
      this.patchCourse(this.editData, false);
    }
  }
  offClickHandler(event: any) {
    if (!this.container.nativeElement.contains(event.target)) { // check click origin
      this.datePicker.nativeElement.style.display = "none";
    }
  }
  initCourseAndRefForm() {
    this.courseAndRefForm = this.formBuilder.group({
      // orgUniqueId: [this.userData.orgUniqueId, Validators.required],
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      courseList: this.formBuilder.array([this.initCourseList()])
    });
  }
  initCourseList() {
    return this.formBuilder.group({
      name: ['', Validators.required],
      isNew: [true, Validators.required],
      year: ['', Validators.required],
      month: ['', Validators.required],
      day: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      endDateValue: ['', Validators.required],
      masterCourseUniqueId: ['', Validators.required],
      academicSessionUniqueId: ['', Validators.required],
      boardAndGradeReferenceDtoList: this.formBuilder.array([this.initBoardGradeList()], Validators.required),
      alreadySelected: [[]],
      masterCourse: ['', Validators.nullValidator],
      duration: null,
      editMode: true,
      isShowDatePicker: false
    });
  }
  // save course submit
  onSubmit() {
    this.submitted = true;
    let dataarray = [];
    if (this.courseAndRefForm.invalid) {
      return;
    }
    let dataform = this.courseAndRefForm.value;
    for (let i = 0; i < dataform.courseList.length; i++) {
      let data = dataform.courseList[i];
      // console.log("courseAndRefForm", JSON.stringify(data));
      let mastercourse = [{
        uniqueId: data.masterCourseUniqueId
      }]
      let academicSessionDto = {
        uniqueId: data.academicSessionUniqueId
      }
      let boardGradeList = []
      data.boardAndGradeReferenceDtoList.forEach(element => {
        let dtograde = {
          uniqueId: element.gradeUniqueId
        }
        let dtoboard = {
          uniqueId: element.boardUniqueId
        }
        let dto = {
          boardDto: dtoboard,
          gradeDto: dtograde
        }
        boardGradeList.push(dto)
      });

      let dto = {
        name: data.name,
        year: data.year,
        month: data.month,
        day: data.day,
        startDate: data.startDate,
        endDate: data.endDate,
        masterCourseDtoList: mastercourse,
        academicSessionDto: academicSessionDto,
        boardGradeList: boardGradeList,
      }
      dataarray.push(dto)
    }
    // console.log("courseAndRefForm add data", JSON.stringify(dataarray));
    this.loading = true;
    this._courseService.addCourse(dataarray).subscribe(res => {
      if (res) {
        this.loading = false;
        this.toastService.showSuccess("Course saved successfully..")
        if (!this.isFromSetup) {
          // this.getCourseList();
          // this.submitted = false;
          // // this.stepper.next();
          // var button = document.getElementById("nextBtn");
          // button.click();
          this.cancelBtn(false);
        } else if (this.isFromSetup) {
          this.setup.selectedStep(3);
        }

      }
      //this.getCourseListData()
      this.loading = false;
    },
      error => {
        this.loading = false;
      //  this.toastService.ShowError(error);

      })

  }

  cancelBtn(value) {
    this.newItemEvent.emit(value);
  }
  // update course
  onUpdate() {
    this.submitted = true;
    if (this.courseAndRefForm.invalid) {
      return;
    }
    let dto = {};
    this.loading = true;
    let dataform = this.courseAndRefForm.value;
    for (let i = 0; i < dataform.courseList.length; i++) {
      let data = dataform.courseList[i];
      // console.log('on update recieved values' + JSON.stringify(data))
      let academicSessionDto = {
        uniqueId: data.academicSessionUniqueId
      }
      let masterCourseDtoList = {
        uniqueId: data.masterCourseUniqueId
      }

      let masterCourseDtoListArray = [];
      masterCourseDtoListArray.push(masterCourseDtoList);
      let boardGradeList = []
      data.boardAndGradeReferenceDtoList.forEach(element => {
        let dtograde = {
          uniqueId: element.gradeUniqueId
        }
        let dtoboard = {
          uniqueId: element.boardUniqueId
        }
        let dto = {
          boardDto: dtoboard,
          gradeDto: dtograde
        }
        boardGradeList.push(dto)
      });
      dto = {
        uniqueId: data.uniqueId,
        name: data.name,
        year: data.year,
        month: data.month,
        day: data.day,
        startDate: data.startDate,
        endDate: data.endDate,
        boardGradeList: boardGradeList,
        academicSessionDto: academicSessionDto,
        masterCourseDtoList: masterCourseDtoListArray
      }
    }
    console.log("update course ", JSON.stringify(dto));
    this._courseService.updateCourse(dto).subscribe(res => {
      if (res) {
        this.toastService.showSuccess("Course update successfully..")
        this.submitted = false;
        this.cancelBtn(false);
        if (this.isFromSetup) {
          this.setup.selectedStep(3);
        }
      }
      //this.getCourseListData()
      this.loading = false;
    },
      error => {
        this.loading = false;

     //   this.toastService.ShowError(error);

      })
  }
  // get course list
  getCourseList() {
    this.loading = true;
    this.courseList = [];
    return this._courseService.getCourseBasicInfoList(environment.COURSE_NOT_EXPIRED).subscribe(
      res => {
        this.courseList = res;
        if (res) {
          this.loading = false;
          if (res.success == true) {
            // console.log("get courseList => ",res)
            this.courseList = res.list;
            this.patchCourse(this.courseList, false);
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error.error instanceof Error) {
        //   this.loading = false;
        // } else {
        //   this.loading = false;
        //   this.toastService.ShowError(error.error.errorMessage);
        // }
        this.loading = false;
      });

  }
  // patch course
  patchCourse(couseList: any, isEdit) {
    // console.log("couseList", couseList)
    let controlArray = <FormArray>this.courseAndRefForm.controls['courseList'];
    controlArray.controls = [];
    if (isEdit == true) {
      controlArray.push(this.initCourseWithValue(couseList, true));

    } else {
      for (let i = 0; i < couseList.length; i++) {
        controlArray.push(this.initCourseWithValue(couseList[i], false));
      }
    }

  }
  // assign course data
  initCourseWithValue(course, mode) {
    this.ismasterCourseFromServer = (course.masterCourseDtoList && course.masterCourseDtoList[0].uniqueId) ? true : false;
    return this.formBuilder.group({
      uniqueId: [course.uniqueId, Validators.nullValidator],
      id: [course.id, Validators.nullValidator],
      name: [course.name, Validators.required],
      isNew: [false, Validators.required],
      year: [course.year, Validators.required],
      month: [course.month, Validators.required],
      day: [course.day, Validators.required],
      startDate: [(course.startDate) ? course.startDate : "", Validators.required],
      endDate: [course.endDate, Validators.required],
      masterCourseUniqueId: [(course.masterCourseDtoList) ? course.masterCourseDtoList[0].uniqueId : null, Validators.nullValidator],
      academicSessionUniqueId: [(course.academicSessionDto) ? course.academicSessionDto.uniqueId : null, Validators.required],
      endDateValue: [this.datePipe.transform(course.endDate, 'yyyy-MM-dd'), Validators.nullValidator],
      boardGradeList: [course.boardGradeList],
      duration: course.duration,
      boardAndGradeReferenceDtoList: this.formBuilder.array(this.patchBoardGradeWithValue(course.boardGradeList, course)),
      masterCourse: [this.getMasterCourseName(course.masterCourseUniqueId)],
      editMode: mode,
      isShowDatePicker: false
    });
  }
  // initialization board and grade
  initBoardGradeList() {
    return this.formBuilder.group({
      uniqueId: ['', Validators.nullValidator],
      boardUniqueId: ['', Validators.required],
      gradeUniqueId: ['', Validators.required],
      boardName: ['', Validators.nullValidator],
      gradeName: ['', Validators.nullValidator],
      courseUniqueId: [null, Validators.nullValidator],
      courseName: [null, Validators.nullValidator],
      boardGradeHasCourseId: ['', Validators.nullValidator]
    });
  }
  // get master course name of course from master list
  getMasterCourseName(universalId) {
    let name = "";
    if (this.masterCourseList.length > 0) {
      for (let index = 0; index < this.masterCourseList.length; index++) {
        const element = this.masterCourseList[index];
        if (element.uniqueId == universalId) {
          name = element.name;
          break;
        }
      }
      return name;
    } else {
      return "";
    }
  }

  getAcademicSessionList() {
    this._academicsessionService.getAcademicSession().subscribe(
      res => {
        this.loading = false;
        if (res.success == true) {
          this.academicsessionlist = res.list;
        } else {
          this.toastService.ShowWarning("Accademic session not fund");
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });
  }
  //get master course list
  getMasterCourseList() {
    this._courseService.getMasterCourseList().subscribe(
      res => {
        if (res) {
          this.masterCourseList = res;

          this.loading = false;
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });
  }
  // delete course row
  deleteRow(rowIndex, row) {
    // console.log('delete course clciked')
    let controlArray = this.courseAndRefForm.get('courseList') as FormArray;
    Swal.fire({
      text: 'Are you sure want to delete course?',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        this.loading = true;
        var rowData = row.value;
        if (rowData.isNew) {
          controlArray.removeAt(rowIndex);
          this.submitted = false;
        } else {
          let courseDto = [{
            "id": rowData.id,
            "name": rowData.name,
            "year": rowData.year,
            "month": rowData.month,
            "day": rowData.day,
            "startDate": rowData.startDate,
            "endDate": rowData.endDate,
            "universalId": rowData.universalId,
            "duration": rowData.duration,
          }];
          // this._courseService.deleteCours(courseDto).subscribe(res => {
          //   if (res == "OK") {
          //     controlArray.removeAt(rowIndex)
          //     this.toastService.showSuccess("Course deleted successfully..");
          //     this.submitted = false;
          //   }
          //   (error: HttpErrorResponse) => {
          //     if (error instanceof HttpErrorResponse) {
          //     } else {
          //       this.loading = false;
          //       this.toastService.ShowError(error);
          //     }
          //   }
          // })
        }
        this.loading = false;

      }
    });
  }
  // update row
  updateRow(row) {
    this.submitted = true;

    if (this.courseAndRefForm.invalid) {
      return;
    }
    this.submitted = false;
    row.controls.editMode.value = false;
  }
  editRow(index, row) {
    // console.log('edit row called')
    if (this.currentEditRowIndex || this.currentEditRowIndex == 0) {
      // console.log("test 1")
      let controlArray = <FormArray>this.courseAndRefForm.controls['courseList'];
      let rowGroup = <FormGroup>controlArray.controls[this.currentEditRowIndex];
      rowGroup.setValue(this.oldRow)
      rowGroup.controls['editMode'].setValue(false);
    }


    // console.log("test 2")
    this.currentEditRowIndex = index;
    row.controls.editMode.value = true;
    this.oldRow = row.value;
  }
  cancelRow(index, row) {
    // console.log('cancel row called')
    this.submitted = false;
    row.controls.editMode.value = false;
    let controlArray = <FormArray>this.courseAndRefForm.controls['courseList'];
    let rowGroup = <FormGroup>controlArray.controls[this.currentEditRowIndex];
    rowGroup.setValue(this.oldRow)
    rowGroup.controls['editMode'].setValue(false);
  }
  // add new row for create course
  addNewRow() {
    this.submitted = false;
    let control = <FormArray>this.courseAndRefForm.controls['courseList'];
    control.push(this.initCourseList());
  }
  // on start date change call get calenderlist
  onDateChange(startDate: Date, index: number) {

    let date: Date = new Date(this.datePipe.transform(startDate, 'yyyy-MM-dd'));
    let fullYear: number = date.getFullYear();
    let fullMonth: number = date.getMonth();
    let fullDate: number = date.getDate();

    let courseArray = <FormArray>this.courseAndRefForm.controls['courseList'];
    let courseGroup = <FormGroup>courseArray.controls[index];
    let yearValue = (courseGroup.controls['year'].value) ? courseGroup.controls['year'].value : 0;
    let yearCal = fullYear + parseInt(yearValue);

    let monthValue = (courseGroup.controls['month'].value) ? courseGroup.controls['month'].value : 0;
    let monthCal = fullMonth + parseInt(monthValue);

    let daysValue = (courseGroup.controls['day'].value) ? courseGroup.controls['day'].value : 0;
    let daysCal = fullDate + parseInt(daysValue);

    let endDate = new Date(yearCal, monthCal, daysCal);
    let endDateCon = new Date(this.datePipe.transform(endDate, 'yyyy-MM-dd'));
    courseGroup.controls['endDate'].setValue(this.datePipe.transform(endDateCon, "yyyy-MM-dd'T'HH:mm:ss.000Z"));
    courseGroup.controls['endDateValue'].setValue(this.datePipe.transform(endDateCon, 'yyyy-MM-dd'));

  }

  onCourseItemSelect(item: any, index: number) {
    item.isLinked = true;
  }
  OnCourseItemDeSelect(item: any, index: number) {
    item.isLinked = false;
  }
  getBoardList() {
    this.loading = true;
    this.boardList = [];
    return this._boardService.getAcademicBoardsList().subscribe(
      res => {
        if (res) {
          // console.log('board list => ', res)
          this.boardList = res
        }
        this.loading = false;
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });
  }

  getGradeList() {
    this.loading = true;
    this.boardList = [];
    return this._gradeService.getAcademicGradeList().subscribe(
      res => {
        if (res.success == true) {
          // console.log('grade list => ', res)

          this.gradeList = res.list
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.loading = false;
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.loading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.loading = false;
      });
  }
  //patch board and grade values to boardGradeListDto
  patchBoardGradeWithValue(boardGradeListDto: Array<any>, course) {
    let boardGradeList = [];
    if (boardGradeListDto != null && boardGradeListDto.length > 0) {
      for (let j = 0; j < boardGradeListDto.length; j++) {
        let boardGrade = boardGradeListDto[j];
        // console.log('patchBoardGradeWithValue boardGrade' + JSON.stringify(boardGrade))
        let boardGradeGroup = this.formBuilder.group({
          uniqueId: [boardGrade.uniqueId, Validators.nullValidator],
          boardUniqueId: [boardGrade.boardDto.uniqueId, Validators.nullValidator],
          gradeUniqueId: [boardGrade.gradeDto.uniqueId, Validators.nullValidator],
          boardName: [boardGrade.boardDto.name, Validators.nullValidator],
          gradeName: [boardGrade.gradeDto.name, Validators.nullValidator],
          courseName: [course.name, Validators.nullValidator],
          courseUniqueId: [course.uniqueId, Validators.nullValidator],
          boardGradeHasCourseId: [boardGrade.uniqueId, Validators.nullValidator]
        });
        boardGradeList.push(boardGradeGroup);
      }
    } else {
      boardGradeList.push(this.initBoardGradeList());
    }

    return boardGradeList;
  }
  // datepicker open on start date
  datePickerOpen(row) {
    console.log(row.controls.startDate.value);
    if (row.controls.startDate.value != null) {
      this.bsInlineValue = new Date(row.controls.startDate.value);
      this.bsDate = new Date(row.controls.startDate.value);
      row.controls.isShowDatePicker.value = true;
    } else {
      this.bsInlineValue = new Date().toLocaleDateString();
      this.bsDate = new Date().toLocaleDateString();
      row.controls.isShowDatePicker.value = true;
    }
  }
  //delete board and grade row
  deleteBoardGrade(parentIndex, childIndex, data) {
    this.loading = true;
    let controlArray = <FormArray>this.courseAndRefForm.controls['courseList'];
    let formGroup = <FormGroup>controlArray.controls[parentIndex];
    let array = <FormArray>formGroup.controls['boardAndGradeReferenceDtoList'];
    array.removeAt(childIndex);
    if (data.boardGradeHasCourseId != null && data.boardGradeHasCourseId != '') {

      this.loading = false;
    } else {
      // array.removeAt(childIndex);
      this.loading = false;
    }

  }
  // Add board and frade row
  addBoardGradeRow(index) {
    let courseArray = <FormArray>this.courseAndRefForm.controls['courseList'];
    let courseGroup = <FormGroup>courseArray.controls[index];
    let boardGradeList = <FormArray>courseGroup.controls['boardAndGradeReferenceDtoList'];
    boardGradeList.push(this.initBoardGradeList())
  }
  // start date change
  dateChange(event, row, index) {
    let courseAndRefform = <FormArray>this.courseAndRefForm.controls['courseList'];
    let course = <FormGroup>courseAndRefform.controls[index];
    if (event != 'Invalid Date') {
      let eventDate = this.datePipe.transform(event, 'yyyy-MM-dd');

      course.controls['startDate'].setValue(this.datePipe.transform(event, 'yyyy-MM-dd'))
      // row.controls.startDate.value=this.datePipe.transform(event, 'yyyy-MM-dd');
      if (this.bsDate != 'Invalid Date') {
        let bsInlineValueDate = this.datePipe.transform(this.bsDate, 'yyyy-MM-dd');
        if (eventDate != bsInlineValueDate) {
          row.controls.isShowDatePicker.value = false;
        }
      } else {
        row.controls.isShowDatePicker.value = false;
      }
      this.bsDate = new Date(eventDate);
    }
  }
  //Change duration value
  onDurationChange(event, index) {
    let courseArray = <FormArray>this.courseAndRefForm.controls['courseList'];
    let courseGroup = <FormGroup>courseArray.controls[index];
    let date: Date = new Date(courseGroup.controls['startDate'].value);
    if (courseGroup.controls['startDate'].value) {
      let fullYear: number = date.getFullYear();
      let fullMonth: number = date.getMonth();
      let fullDate: number = date.getDate();
      let yearValue = (courseGroup.controls['year'].value) ? courseGroup.controls['year'].value : 0;
      let yearCal = fullYear + parseInt(yearValue);

      let monthValue = (courseGroup.controls['month'].value) ? courseGroup.controls['month'].value : 0;
      let monthCal = fullMonth + parseInt(monthValue);

      let daysValue = (courseGroup.controls['day'].value) ? courseGroup.controls['day'].value : 0;
      let daysCal = fullDate + parseInt(daysValue);
      // console.log('courseGroup', courseGroup);

      let endDate = new Date(yearCal, monthCal, daysCal);
      let endDateCon = new Date(this.datePipe.transform(endDate, 'yyyy-MM-dd'));
      courseGroup.controls['endDate'].setValue(this.datePipe.transform(endDateCon, "yyyy-MM-dd'T'HH:mm:ss.000Z"));
      courseGroup.controls['endDateValue'].setValue(this.datePipe.transform(endDateCon, 'yyyy-MM-dd'));
    }
  }
  // datepicker popup close on outside click
  onClickedOutside(event, row) {
    if (event.target.id == 'datepickerInput' && !this.isOutSideDatepickerClick) {
      this.isOutSideDatepickerClick = true;
      row.controls['isShowDatePicker'].setValue(true)
    } else if (this.isOutSideDatepickerClick && !event.target.classList.contains('ng-option')) {
      this.isOutSideDatepickerClick = false;
      row.controls['isShowDatePicker'].setValue(false)
    }
  }

  previousStep() {
    this.setup.selectedStep(1)
  }
  changeBoardGrade(boardGrade, indexcourse) {
    let courseList = <FormArray>this.courseAndRefForm.controls['courseList'];
    let courseGroup = <FormGroup>courseList.controls[indexcourse];
    let boardGradeArray = <FormArray>courseGroup.controls['boardAndGradeReferenceDtoList'];
    if (boardGrade.value.boardUniqueId && boardGrade.value.gradeUniqueId) {
      let list = boardGradeArray.value.filter(x => (x.boardUniqueId == boardGrade.value.boardUniqueId && x.gradeUniqueId == boardGrade.value.gradeUniqueId));
      if (list.length > 1) {
        this.toastService.ShowWarning("This board and grade already used. Please select others.");
        boardGrade.controls.boardUniqueId.setValue(null);
        boardGrade.controls.gradeUniqueId.setValue(null);
      }
    }
  }
}