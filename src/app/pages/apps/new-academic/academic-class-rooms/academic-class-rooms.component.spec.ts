import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicClassRoomsComponent } from './academic-class-rooms.component';

describe('AcademicClassRoomsComponent', () => {
  let component: AcademicClassRoomsComponent;
  let fixture: ComponentFixture<AcademicClassRoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcademicClassRoomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcademicClassRoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
