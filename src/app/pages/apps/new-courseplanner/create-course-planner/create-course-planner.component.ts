import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import Swal from 'sweetalert2';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import * as moment from 'moment';
import { CourseplannerServiceService } from 'src/app/servicefiles/courseplanner-service.service';

@Component({
  selector: 'app-create-course-planner',
  templateUrl: './create-course-planner.component.html',
  styleUrls: ['./create-course-planner.component.scss']
})
export class CreateCoursePlannerComponent implements OnInit {
  totalDuration: any = 0;
  submitted = false;
  topicDurationSum = 0;
  chapterDurationSum = 0;
  unitDurationSum = 0;
  subTopicSum = 0;
  topicSum = 0;
  chapterSum = 0;
  chapterList = []
  updateResponse = [];
  deleteResponse = [];
  teacherList = [];
  cpdata: any;
  durationList = [{ id: 5, name: "5" }, { id: 10, name: "10" }, { id: 15, name: "15" }, { id: 20, name: "20" }, { id: 25, name: "25" }, { id: 30, name: "30" }, { id: 35, name: "35" }, { id: 40, name: "40" }, { id: 45, name: "45" }, { id: 50, name: "50" }, { id: 55, name: "55" }, { id: 60, name: "60" }]
  isChapter: boolean = false;
  isUnit: boolean = false;
  isTopic: boolean = false;
  isSubTopic: boolean = false;
  values = [
    {
      id: 1,
      field: "Unit",
      checked: false
    },
    {
      id: 2,
      field: "Chapter",
      checked: false
    }, {
      id: 3,
      field: "Topic",
      checked: false
    },
    {
      id: 4,
      field: "Sub Topic",
      checked: false
    },
    {
      id: 5,
      field: "All",
      checked: false
    }
  ]
  imageError = ""
  imageSrc = ""
  type: any
  index: any
  topicIndex: any
  chapterIndex: any
  subTopicIndex: any
  stack = [];
  mappingTypes = [
    { id: 2, name: 'Chapter' },
    { id: 3, name: 'Topic' }
  ]
  showMappingBtnInChapter: boolean = false;
  showMappingBtnInTopic: boolean = false;
  isEditForm = false;
  selectedMapping = null;
  selecteditemValue: any;
  selecteditemId: any;
  selectedCategory: any;
  selectedproperty: any;
  selectedMappingId: any;
  isAssignMapping = false;
  isCreateMapping = false;
  categoryData: any;
  propertyData: any;
  itemData: any;
  mappingListData: any;
  mappingForm: FormGroup;
  mappingList: FormArray;
  addMappingSubmit = false;
  createMappingSubmit = false;
  isAlreadyMapped = false;
  isAlreadyDateMapped = false;
  selectedMapingFormGroup: any;
  startDate: any;
  endDate: any;
  itemValueList = [{ id: 1, value: 1 }, { id: 2, value: 2 }, { id: 3, value: 3 }, { id: 4, value: 4 }, { id: 5, value: 5 }]
  @Input() public formData: FormGroup;
  @Input() public coursePlannerData: any;
  @Input() public plannerType: any;
  @Input() public batchPlannerId: any;
  @Input() syllabusPlannerId: string;
  @Output() closeCreateForm = new EventEmitter<string>();
  @Output() closeEditForm = new EventEmitter<string>();
  @Output() returnData = new EventEmitter<any>();
  @Input() batchUniqueId: any;
  @Input() coursePlannerName: any;
  cpData = [];
  hideSaveButton: boolean = false;
  constructor(private fb: FormBuilder, private toastService: ToastsupportService, private spinner: NgxSpinnerService, private modalService: NgbModal, private _cpService: CourseplannerServiceService) {
    // console.log("formData", this.formData);
  }

  ngOnInit() {
    this.mappingForm = new FormGroup({
      mappingList: new FormArray([])
    });
  }
  ngOnChanges(changes: SimpleChanges) {
    // console.log(changes);
    // console.log(this.isEditForm);

    if (changes['formData'] && !changes['coursePlannerData']) {
      // console.log("changes");
      this.addUnit()
    } else {
      this.isEditForm = true
      if (changes['plannerType'] && changes['plannerType'].currentValue == 1) {
        this.getCoursePlannerDetails(this.syllabusPlannerId);
        //this.getCategoryList();
        this.patchSyllabusName(this.formData.value)
        //this.getmappingLevel();
      } else if (changes['plannerType'] && changes['plannerType'].currentValue == 2) {
        this.getBatchPlannerDetails(this.batchPlannerId);
        this.patchSyllabusName(this.formData.value);
        //this.formData.controls['coursePlannerId'].setValue(this.syllabusPlannerId);
        //this.getTeacherList(this.batchPlannerId)
      } else if (changes['plannerType'] && changes['plannerType'].currentValue == 3) {
        this.getMasterCoursePlannerDetails(this.syllabusPlannerId);
        this.patchSyllabusName(this.formData.value)
      } else if (changes['plannerType'] && changes['plannerType'].currentValue == 4) {
        this.getCoursePlannerDetails(this.syllabusPlannerId);
        //this.getCategoryList();
        this.patchSyllabusName(this.formData.value);
        //this.getmappingLevel();
        this.hideSaveButton = true;
      }
    }
  }
  cancel(type) {
    this.savePendingChanges(false);
    this.submitted = false;
    if (type == 1) {
      this.closeEditForm.emit("1")
    } else {
      if (this.isEditForm) {
        this.closeEditForm.emit("1")
      } else {
        this.closeEditForm.emit("2")
      }
    }
    this.isEditForm = false;
    this.closeCreateForm.emit("")
    this.formData.reset();
  }
  addUnit() {
    let itemList = <FormArray>this.formData.controls.units;
    itemList.push(
      this.fb.group({
        id: [''],
        'unitDetails': this.fb.group({
          'name': ['', Validators.required],
          'duration': [''],
          'sequenceNumber': [itemList.length + 1]
        }),
        chapters: this.fb.array([]),
        durationName: ['00 hr 00 mins']
      })
    )
  }
  addChapter(control: FormGroup) {
    this.chapterDurationSum = 0;
    let itemList = <FormArray>control.controls['chapters'];
    let formGroup = this.fb.group({
      id: [''],
      'chapterDetails': this.fb.group({
        'name': ['', Validators.required],
        'duration': [''],
        'sequenceNumber': [itemList.length + 1]
      }),
      topics: this.fb.array([]),
      durationName: ['00 hr 00 mins']
    });
    itemList.push(formGroup);
    // this.chapterList.push(formGroup);
  }

  addTopic(control) {
    this.topicDurationSum = 0;
    let itemList = <FormArray>control.controls['topics'];
    let formGroup = this.fb.group({
      id: [''],
      'topicDetails': this.fb.group({
        'name': ['', Validators.required],
        'duration': [''],
        'sequenceNumber': [itemList.length + 1]
      }),
      subtopics: this.fb.array([]),
      durationName: ['00 hr 00 mins']
    });
    itemList.push(formGroup);
  }

  addsubTopic(control) {
    let itemList = <FormArray>control.controls['subtopics'];
    let formGroup = this.fb.group({
      id: [''],
      'subTopicDetails': this.fb.group({
        'name': ['', Validators.required],
        'duration': [''],
        'sequenceNumber': [itemList.length + 1]
      }),
      durationName: ['00 hr 00 mins']
    });
    itemList.push(formGroup);
  }

  get unitArray(): FormArray {
    let unitArray = <FormArray>this.formData.controls.items;
    return unitArray;
  }

  get chapterArray(): FormArray {
    let unitArray = <FormArray>this.formData.controls.items;
    return unitArray

  }
  get topicArray(): FormArray {
    let topicArray = <FormArray>this.formData.controls.items;
    return topicArray

  }
  get subTopicArray(): FormArray {
    let unitArray = <FormArray>this.formData.controls.items;
    return unitArray

  }

  submit(form, type) {
    this.submitted = true
    console.log("test 1");
    if (type == 1 && this.formData.invalid) {
      this.scrollToError();
      return;
    }
    if (form.value.units.length == 0) {
      this.toastService.ShowWarning("Please add atleast one unit.")
      return;
    }

    this.spinner.show();
    let formData = form.value;
    if (this.plannerType == 1 || this.plannerType == 3) {
      console.log("test 2");
      if (formData.isNew) {
        this._cpService.addCoursePlanner(formData).subscribe(syllabusData => {
          this.toastService.showSuccess("Course planner added successfully..");
          this.spinner.hide();
          this.submitted = false
          if (type != 2) {
            this.cancel(1);
          }
        }, (error) => {
       //   this.toastService.ShowError(error);
          this.spinner.hide();
          this.submitted = false
        })
      } else if (!formData.isNew) {
        this.savePendingChanges(false);
        this._cpService.updateCoursePlanner(formData).subscribe(syllabusData => {
          this.toastService.showSuccess("Course planner update successfully..");
          this.spinner.hide();
          this.submitted = false
          if (type != 2) {
            this.cancel(1);
          }
        }, (error) => {
       //   this.toastService.ShowError(error);
          this.spinner.hide();
          this.submitted = false
        })
      }
    } else {
      console.log("test 3");
      this.savePendingChanges(false);
      this._cpService.updateBatchPlanner(formData).subscribe(syllabusData => {
        this.toastService.showSuccess("Batch planner updated successfully..");
        this.spinner.hide();
        this.submitted = false
        this.cancel(1);
      }, (error) => {
       // this.toastService.ShowError(error);
        this.spinner.hide();
        this.submitted = false
      })
    }
  }

  deleteCoursePlannerById(id: any) {
    this.spinner.show()
    if (this.plannerType == 1) {
      let coursePlannerIdList = [];
      coursePlannerIdList.push(id);
      this._cpService.deleteCoursePlannerById(coursePlannerIdList).subscribe(
        response => {
          this.cpdata = response;
          this.toastService.showSuccess("Course Planner deleted successfully");
          this.cancel(2);
          this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide()
        });
    } else {
      let batchPlannerIdList = [];
      batchPlannerIdList.push(id);
      this._cpService.deleteBatchPlannerById(batchPlannerIdList).subscribe(
        response => {
          this.cpdata = response;
          this.toastService.showSuccess("Batch Planner deleted successfully");
          this.cancel(2);
          this.spinner.hide()
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {

          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide()
        });
    }

  }

  calculateUnitDuration(unit: FormGroup, index, $event) {
    let durationName = ""
    let duration = parseInt($event.name)
    durationName = this.format(duration)
    unit.controls['durationName'].setValue(durationName);
    let durationValue = 0;
    let unitArray = <FormArray>this.formData.controls['units'];
    for (let i = 0; i < unitArray.length; i++) {
      let unitFormGroup = <FormGroup>unitArray.controls[i];
      let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
      durationValue = durationValue + parseInt(unitDetails.controls['duration'].value);
    }
    this.totalDuration = durationValue;
  }
  calculateChapterduration(unit: FormGroup, unitControlIndex: number, chapterControlIndex: number, $event: any) {

    let durationName = ""
    let chapterList = <FormArray>unit.controls['chapters'];
    let chapterFormGroup = <FormGroup>chapterList.controls[chapterControlIndex];
    this.unitDurationSum = 0;
    this.chapterSum = 0;
    if (chapterList != null && chapterList.length > 0) {
      let unitArray = <FormArray>this.formData.controls['units'];
      let unitFormGroup = <FormGroup>unitArray.controls[unitControlIndex];
      for (let i = 0; i < chapterList.length; i++) {
        let unitFormGroup = <FormGroup>chapterList.controls[i];
        let chapterDetails = <FormArray>unitFormGroup.controls['chapterDetails'];
        this.chapterSum = this.chapterSum + parseInt(chapterDetails.controls['duration'].value);
      }
      let duration = parseInt($event.name)
      durationName = this.format(duration)
      chapterFormGroup.controls['durationName'].setValue(durationName);
      let ud = <FormArray>unitFormGroup.controls['unitDetails'];
      ud.controls['duration'].setValue(this.chapterSum)
      durationName = this.format(this.chapterSum)
      unitFormGroup.controls['durationName'].setValue(durationName);
      for (let i = 0; i < unitArray.length; i++) {
        let unitFormGroup = <FormGroup>unitArray.controls[i];
        let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
        this.unitDurationSum = this.unitDurationSum + parseInt(unitDetails.controls['duration'].value);
      }
      this.totalDuration = this.unitDurationSum;
    }

  }
  calculateTopicduration(chapter: FormGroup, unit: FormGroup, chapterControlIndex: number, unitControlIndex: number, topicControlIndex: number, $event: any) {

    let durationName = ""

    let unitArray = <FormArray>this.formData.controls['units'];
    let unitFormGroup = <FormGroup>unitArray.controls[unitControlIndex];

    let chapterArray = <FormArray>unitFormGroup.controls['chapters'];
    let chapterGroup = <FormGroup>chapterArray.controls[chapterControlIndex];

    let topiclist = <FormArray>chapter.controls['topics'];
    let topicFormGroup = <FormGroup>topiclist.controls[topicControlIndex];

    this.topicDurationSum = 0;
    if (topiclist != null && topiclist.length > 0) {
      this.subTopicSum = 0;
      this.topicDurationSum = 0;
      this.chapterDurationSum = 0;
      this.unitDurationSum = 0;
      this.topicSum = 0;
      this.chapterSum = 0;
      for (let j = 0; j < topiclist.length; j++) {
        let unitFormGroup = <FormGroup>topiclist.controls[j];
        let topicDetails = <FormArray>unitFormGroup.controls['topicDetails'];
        this.topicSum = this.topicSum + parseInt(topicDetails.controls['duration'].value);
      }
      let duration = parseInt($event.name)
      durationName = this.format(duration)
      topicFormGroup.controls['durationName'].setValue(durationName);
      this.chapterDurationSum = this.chapterDurationSum + this.topicSum;
      //chapterGroup.controls['duration'].setValue(this.chapterDurationSum);
      let cd = <FormArray>chapterGroup.controls['chapterDetails'];
      cd.controls['duration'].setValue(this.chapterDurationSum)


      durationName = this.format(this.chapterDurationSum)
      chapterGroup.controls['durationName'].setValue(durationName);

      for (let k = 0; k < chapterArray.length; k++) {
        let unitFormGroup = <FormGroup>chapterArray.controls[k];
        let chapterDetails = <FormArray>unitFormGroup.controls['chapterDetails'];
        this.chapterSum = this.chapterSum + parseInt(chapterDetails.controls['duration'].value);
      }

      this.unitDurationSum = this.chapterSum;

      //unitFormGroup.controls['duration'].setValue(this.unitDurationSum);
      let ud = <FormArray>unitFormGroup.controls['unitDetails'];
      ud.controls['duration'].setValue(this.unitDurationSum)

      durationName = this.format(this.unitDurationSum)
      unitFormGroup.controls['durationName'].setValue(durationName);


      duration = 0
      for (let i = 0; i < unitArray.length; i++) {
        let unitFormGroup = <FormGroup>unitArray.controls[i];
        let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
        duration = duration + parseInt(unitDetails.controls['duration'].value);
      }
      this.totalDuration = duration;
    }

  }

  calculateduration(topic: FormGroup, chapter: FormGroup, unit: FormGroup, topicControlIndex: number, chapterControlIndex: number, unitControlIndex: number, $event: any) {
    let durationName = ""
    // console.log($event)
    let subTopicList = <FormArray>topic.controls['subtopics'];


    let unitArray = <FormArray>this.formData.controls['units'];
    let unitFormGroup = <FormGroup>unitArray.controls[unitControlIndex];

    let chapterArray = <FormArray>unitFormGroup.controls['chapters'];
    let chapterGroup = <FormGroup>chapterArray.controls[chapterControlIndex];

    let topicArray = <FormArray>chapterGroup.controls['topics'];
    let topicGroup = <FormGroup>topicArray.controls[topicControlIndex];

    if (subTopicList != null && subTopicList.length > 0) {

      this.subTopicSum = 0;
      this.topicDurationSum = 0;
      this.chapterDurationSum = 0;
      this.unitDurationSum = 0;
      this.topicSum = 0;
      this.chapterSum = 0;

      for (let i = 0; i < subTopicList.length; i++) {
        let unitFormGroup = <FormGroup>subTopicList.controls[i];
        let subtopicDetails = <FormArray>unitFormGroup.controls['subTopicDetails'];
        this.subTopicSum = this.subTopicSum + parseInt(subtopicDetails.controls['duration'].value);
      }
      // console.log(this.topicDurationSum, this.subTopicSum);

      this.topicDurationSum = this.topicDurationSum + this.subTopicSum;
      //topicGroup.controls['duration'].setValue(this.topicDurationSum);
      let td = <FormArray>topicGroup.controls['topicDetails'];
      td.controls['duration'].setValue(this.topicDurationSum)
      durationName = this.format(this.topicDurationSum)
      topicGroup.controls['durationName'].setValue(durationName);
    }

    for (let j = 0; j < topicArray.length; j++) {
      let unitFormGroup = <FormGroup>topicArray.controls[j];
      let topicDetails = <FormArray>unitFormGroup.controls['topicDetails'];
      this.topicSum = this.topicSum + parseInt(topicDetails.controls['duration'].value);
    }

    this.chapterDurationSum = this.chapterDurationSum + this.topicSum;
    //chapterGroup.controls['duration'].setValue(this.chapterDurationSum);
    let cd = <FormArray>chapterGroup.controls['chapterDetails'];
    cd.controls['duration'].setValue(this.chapterDurationSum)

    durationName = this.format(this.chapterDurationSum)
    chapterGroup.controls['durationName'].setValue(durationName);

    for (let k = 0; k < chapterArray.length; k++) {
      let unitFormGroup = <FormGroup>chapterArray.controls[k];
      let chapterDetails = <FormArray>unitFormGroup.controls['chapterDetails'];
      this.chapterSum = this.chapterSum + parseInt(chapterDetails.controls['duration'].value);
    }

    this.unitDurationSum = this.chapterSum;

    //unitFormGroup.controls['duration'].setValue(this.unitDurationSum);
    let ud = <FormArray>unitFormGroup.controls['unitDetails'];
    ud.controls['duration'].setValue(this.unitDurationSum)

    durationName = this.format(this.unitDurationSum)
    unitFormGroup.controls['durationName'].setValue(durationName);
    let totalduration = 0
    for (let i = 0; i < unitArray.length; i++) {
      let unitFormGroup = <FormGroup>unitArray.controls[i];
      let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
      totalduration = totalduration + parseInt(unitDetails.controls['duration'].value);
    }
    this.totalDuration = (totalduration) ? totalduration : this.topicDurationSum;
  }
  format(time) {
    // Hours, minutes and seconds
    var hrs = ~~(time / 60);
    var mins = ~~((time % 60));

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";
    if (hrs > 0) {
      ret += "" + hrs + "hrs " + (mins < 10 ? "0" : "");
    }
    ret += "" + mins + "min"
    return ret;
  }
  getDropdownValue(event, ids: number, index) {
    let value = event.target.checked;
    switch (ids) {
      case 1: {
        if (value == true) {
          this.isUnit = true;
          this.values[index].checked = true
        } else {
          this.isUnit = false;
          this.values[index].checked = false
          this.values[4].checked = false
        }
        break;
      }
      case 2: {
        if (value == true) {
          this.isChapter = true;
          this.values[index].checked = true
        } else {
          this.isChapter = false;
          this.values[index].checked = false
          this.values[4].checked = false
        }
        break;
      }
      case 3: {
        if (value == true) {
          this.isTopic = true;
          this.values[index].checked = true
        } else {
          this.isTopic = false;
          this.values[index].checked = false
          this.values[4].checked = false
        }
        break;
      }
      case 4: {
        if (value == true) {
          this.isSubTopic = true;
          this.values[index].checked = true
        } else {
          this.isSubTopic = false;
          this.values[index].checked = false
          this.values[4].checked = false
        }
        break;
      }
      case 5: {
        if (value == true) {
          this.isUnit = true;
          this.isChapter = true;
          this.isTopic = true;
          this.isSubTopic = true;
        } else {
          this.isUnit = false;
          this.isChapter = false;
          this.isTopic = false;
          this.isSubTopic = false;
        }

        break;
      }
    }
    for (let i = 0; i < this.values.length; i++) {
      const element = this.values[i];
      if (ids == 5) {
        element.checked = (value == true) ? true : false
      }
    }
  }
  openFile(type, index) {
    (document.getElementById("imgupload" + type + index) as HTMLInputElement).value = "";
    document.getElementById("imgupload" + type + index).click();
  }
  onSelectFile(event, type, index, chapterIndex, topicIndex, subTopicIndex) {
    var file = event.target.files[0];
    if (!file) return;
    const max_size = 149715200;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastService.ShowWarning('invalid format');
      return;
    }
    if (event.target.files[0].size > max_size) {
      this.imageError =
        'Maximum size allowed is ' + (max_size / 1000) / 1000 + 'Mb';
      this.toastService.ShowError(this.imageError);
      return false;
    } else {
      this.type = type
      this.index = index
      this.chapterIndex = chapterIndex
      this.topicIndex = topicIndex
      this.subTopicIndex = subTopicIndex
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }
  _handleReaderLoaded(e) {
    var reader = e.target;
    let imageSrc = reader.result;
    let unitArray = <FormArray>this.formData.controls['items'];
    let unitFormGroup = <FormGroup>unitArray.controls[this.index];
    let chapterArray: any
    let chapterGroup: any
    let topicArray: any
    let topicGroup: any
    let subtopicArray: any
    let subtopicGroup: any
    // console.log("uit", this.type, typeof this.type);
    let type = parseInt(this.type)
    if (type > 1) {
      // console.log("chaptre", this.type);

      chapterArray = <FormArray>unitFormGroup.controls['items'];
      chapterGroup = <FormGroup>chapterArray.controls[this.chapterIndex];
    }
    if (type > 2) {
      // console.log("topic", this.type);
      topicArray = <FormArray>chapterGroup.controls['items'];
      topicGroup = <FormGroup>topicArray.controls[this.topicIndex];
    }
    if (type == 4) {
      // console.log("subtopic", this.type);
      subtopicArray = <FormArray>topicGroup.controls['items'];
      subtopicGroup = <FormGroup>subtopicArray.controls[this.subTopicIndex];
    }
    switch (type) {
      case 1:
        unitFormGroup.controls['file'].setValue(imageSrc);
        break;
      case 2:
        chapterGroup.controls['file'].setValue(imageSrc);
        break;
      case 3:
        topicGroup.controls['file'].setValue(imageSrc);
        break;
      case 4:
        subtopicGroup.controls['file'].setValue(imageSrc);
        break;
    }
    this.type = ""
    this.index = ""
    this.topicIndex = ""
    this.chapterIndex = ""
    this.subTopicIndex = ""
  }
  openFileVeiw(viewAttachment: String, filesrc) {
    this.modalService.open(viewAttachment, { centered: true, backdrop: 'static' });
    this.imageSrc = filesrc
  }
  closeviewModal() {
    this.modalService.dismissAll();
    this.imageSrc = ""
  }

  drop(event: CdkDragDrop<string[]>) {
    let value = <FormArray>this.formData.controls['units'];

    moveItemInArray(value.controls, event.currentIndex, event.previousIndex);
    moveItemInArray(value.value, event.previousIndex, event.currentIndex);

    // let destinationNode = <FormGroup>value.controls[event.previousIndex];
    let shiftedNode = <FormGroup>value.controls[event.currentIndex];

    // let destination = <FormArray>destinationNode.controls['unitDetails'];
    // let destinationSequence = destination.value.sequenceNumber;

    // let shifted = <FormArray>shiftedNode.controls['unitDetails'];
    // let shiftedSequence = shifted.value.sequenceNumber;

    // shifted.controls['sequenceNumber'].setValue(destinationSequence);
    // destination.controls['sequenceNumber'].setValue(shiftedSequence);
    this.stack.push(shiftedNode.value);
  }

  dropChapter(event: CdkDragDrop<string[]>, unitIndex: number) {
    let value = <FormArray>this.formData.controls['units'];
    let chapterFormGroup = <FormGroup>value.controls[unitIndex];

    let chpaterArray = <FormArray>chapterFormGroup.controls['chapters'];

    moveItemInArray(chpaterArray.controls, event.currentIndex, event.previousIndex);
    moveItemInArray(chpaterArray.value, event.previousIndex, event.currentIndex);

    // let destinationNode = <FormGroup>chpaterArray.controls[event.previousIndex];
    let shiftedNode = <FormGroup>chpaterArray.controls[event.currentIndex];

    // let destination = <FormArray>destinationNode.controls['chapterDetails'];
    // let destinationSequence = destination.value.sequenceNumber;

    // let shifted = <FormArray>shiftedNode.controls['chapterDetails'];
    // let shiftedSequence = shifted.value.sequenceNumber;

    // shifted.controls['sequenceNumber'].setValue(destinationSequence);
    // destination.controls['sequenceNumber'].setValue(shiftedSequence);
    this.stack.push(shiftedNode.value);
  }

  dropTopic(event: CdkDragDrop<string[]>, chapterIndex: number, unitIndex: number) {
    let formArray = <FormArray>this.formData.controls['units'];
    let unit = <FormGroup>formArray.controls[unitIndex];

    let chapterArray = <FormArray>unit.controls['chapters'];
    let chapter = <FormGroup>chapterArray.controls[chapterIndex];

    let topicArray = <FormArray>chapter.controls['topics'];

    moveItemInArray(topicArray.controls, event.currentIndex, event.previousIndex);
    moveItemInArray(topicArray.value, event.previousIndex, event.currentIndex);

    // let destinationNode = <FormGroup>topicArray.controls[event.previousIndex];
    let shiftedNode = <FormGroup>topicArray.controls[event.currentIndex];

    // let destination = <FormArray>destinationNode.controls['topicDetails'];
    // let destinationSequence = destination.value.sequenceNumber;

    // let shifted = <FormArray>shiftedNode.controls['topicDetails'];
    // let shiftedSequence = shifted.value.sequenceNumber;

    // shifted.controls['sequenceNumber'].setValue(destinationSequence);
    // destination.controls['sequenceNumber'].setValue(shiftedSequence);
    this.stack.push(shiftedNode.value);
  }

  dropSubTopic(event: CdkDragDrop<string[]>, topicIndex: number, chapterIndex: number, unitIndex: number) {
    let formArray = <FormArray>this.formData.controls['units'];
    let unit = <FormGroup>formArray.controls[unitIndex];

    let chapterArray = <FormArray>unit.controls['chapters'];
    let chapter = <FormGroup>chapterArray.controls[chapterIndex];

    let topicArray = <FormArray>chapter.controls['topics'];
    let topic = <FormGroup>topicArray.controls[topicIndex];

    let subTopicArray = <FormArray>topic.controls['subtopics'];

    moveItemInArray(subTopicArray.controls, event.currentIndex, event.previousIndex);
    moveItemInArray(subTopicArray.value, event.previousIndex, event.currentIndex);

    // let destinationNode = <FormGroup>subTopicArray.controls[event.previousIndex];
    let shiftedNode = <FormGroup>subTopicArray.controls[event.currentIndex];

    // let destination = <FormArray>destinationNode.controls['subTopicDetails'];
    // let destinationSequence = destination.value.sequenceNumber;

    // let shifted = <FormArray>shiftedNode.controls['subTopicDetails'];
    // let shiftedSequence = shifted.value.sequenceNumber;

    // shifted.controls['sequenceNumber'].setValue(destinationSequence);
    // destination.controls['sequenceNumber'].setValue(shiftedSequence);
    this.stack.push(shiftedNode.value);
  }

  showChapterMappingButton(mappedArray: any) {
    if (mappedArray != null) {
      for (let i = 0; i < mappedArray.length; i++) {
        if (mappedArray[i].items != null) {
          for (let j = 0; j < mappedArray[i].items.length; j++) {
            if (mappedArray[i].items[j].categoryId != null) {
              this.showMappingBtnInChapter = true;
              this.showMappingBtnInTopic = false;
            }
          }
        }
      }
    }
  }

  showTopicMappingButton(mappedArray: any) {
    if (mappedArray != null) {
      for (let i = 0; i < mappedArray.length; i++) {
        if (mappedArray[i].items) {
          for (let j = 0; j < mappedArray[i].items.length; j++) {
            if (mappedArray[i].items[j].items != null) {
              for (let k = 0; k < mappedArray[i].items[j].items.length; k++) {
                if (mappedArray[i].items[j].items[k].categoryId != null) {
                  this.showMappingBtnInTopic = true;
                  this.showMappingBtnInChapter = false;
                }
              }
            }
          }
        }
      }
    }
  }
  patchSyllabusName(syllabus) {
    if (this.plannerType == 1) {
      this.formData.controls['name'].setValue(syllabus.name);
      this.formData.controls['subjectName'].setValue(syllabus.subjectName);
    }
    // this.formData.controls['subjectId'].setValue(syllabus.courseHasSubjectId);
    if (this.plannerType != 3) {
      this.formData.controls['isNew'].setValue(false);
      this.formData.controls['uniqueId'].setValue(syllabus.uniqueId);
    }
    //this.formData.controls['courseSubjectUniqueId'].setValue(syllabus.courseSubjectUniqueId);
    // this.formData.controls['boardStandardHasCourseId'].setValue(syllabus.boardStandardHasCourseId);
  }
  patchBpBatchInfo(bpBatchInfo) {
    let batchArray = <FormArray>this.formData.controls['batches'];
    // console.log("batchArray",batchArray)
    let batchtList = bpBatchInfo;
    // console.log("batchtList",batchtList)
    if (batchtList.batches != null && batchtList.batches.length > 0) {
      for (let i = 0; i < batchtList.batches.length; i++) {
        let batch = this.fb.group({
          id: batchtList.batches[i].id,
          uniqueId: batchtList.batches[i].uniqueId,
          isLinked: batchtList.batches[i].isLinked,
          name: batchtList.batches[i].name
        });
        batchArray.push(batch);
        // console.log("batchArray ....",batchArray)
      }
    }

  }

  patchUnits(units: Array<any>) {
    let unitList = [];
    if (units != null && units.length > 0) {
      for (let unit = 0; unit < units.length; unit++) {
        if (units[unit].unitDetails.duration) {
          this.totalDuration = this.totalDuration + parseInt(units[unit].unitDetails.duration)
        }
        let Units = this.fb.group({
          id: units[unit].id,
          'unitDetails': this.fb.group({
            'name': [units[unit].unitDetails.name, Validators.required],
            'duration': (units[unit].unitDetails.duration) ? (units[unit].unitDetails.duration) : 0,
            'sequenceNumber': [units[unit].unitDetails.sequenceNumber]
          }),
          typeId: 1,
          chapters: this.fb.array(this.patchChapters(units[unit].chapters)),
          durationName: (units[unit].unitDetails.duration) ? this.format(units[unit].unitDetails.duration) : '00 hr 00 mins'
        })
        unitList.push(Units);
      }
    }
    this.formData.setControl('units', this.fb.array(unitList));
  }

  patchChapters(chapters: Array<any>) {
    let chapterList = [];
    if (chapters != null && chapters.length > 0) {
      for (let chapter = 0; chapter < chapters.length; chapter++) {
        let Chapters = this.fb.group({
          id: chapters[chapter].id,
          'chapterDetails': this.fb.group({
            'name': [chapters[chapter].chapterDetails.name, Validators.required],
            'duration': (chapters[chapter].chapterDetails.duration) ? (chapters[chapter].chapterDetails.duration) : 0,
            'sequenceNumber': [chapters[chapter].chapterDetails.sequenceNumber]
          }),
          typeId: 2,
          topics: this.fb.array(this.patchTopics(chapters[chapter].topics)),
          durationName: (chapters[chapter].chapterDetails.duration) ? this.format(chapters[chapter].chapterDetails.duration) : '00 hr 00 mins'
        })
        chapterList.push(Chapters);
      }
    }
    return chapterList;
  }

  patchTopics(topics: Array<any>) {
    let topicList = [];
    if (topics != null && topics.length > 0) {
      for (let topic = 0; topic < topics.length; topic++) {
        let Topic = this.fb.group({
          id: topics[topic].id,
          'topicDetails': this.fb.group({
            'name': [topics[topic].topicDetails.name, Validators.required],
            'duration': (topics[topic].topicDetails.duration) ? (topics[topic].topicDetails.duration) : 0,
            'sequenceNumber': [topics[topic].topicDetails.sequenceNumber]
          }),
          typeId: 3,
          subtopics: this.fb.array(this.patchSubTopics(topics[topic].subtopics)),
          durationName: (topics[topic].topicDetails.duration) ? this.format(topics[topic].topicDetails.duration) : '00 hr 00 mins'
        });
        topicList.push(Topic);
      }
    }
    return topicList;
  }

  patchSubTopics(subtopics: Array<any>) {
    let subtopicList = [];
    if (subtopics != null && subtopics.length > 0) {
      for (let subtopic = 0; subtopic < subtopics.length; subtopic++) {
        let Subtopic = this.fb.group({
          id: subtopics[subtopic].id,
          'subTopicDetails': this.fb.group({
            'name': [subtopics[subtopic].subTopicDetails.name, Validators.required],
            'duration': (subtopics[subtopic].subTopicDetails.duration) ? (subtopics[subtopic].subTopicDetails.duration) : 0,
            'sequenceNumber': [subtopics[subtopic].subTopicDetails.sequenceNumber]
          }),
          typeId: 4,
          durationName: (subtopics[subtopic].subTopicDetails.duration) ? this.format(subtopics[subtopic].subTopicDetails.duration) : '00 hr 00 mins'
        });
        subtopicList.push(Subtopic);
      }
    }
    return subtopicList;
  }

  get f() { return this.formData.controls; }
  tagModel(formGroup: FormGroup, centerDataModal: string) {
    this.isAssignMapping = true;
    this.selectedMapingFormGroup = formGroup;
    let categoryArray = <FormArray>this.formData.controls['categoryList'];
    let categoryList = [];
    categoryList = categoryArray.value;
    if (categoryList != null && categoryList.length > 0) {
      this.modalService.open(centerDataModal, { centered: true, size: 'lg' });
      let foundCategory: any = categoryList.filter(x => x.id == formGroup.controls['categoryId'].value);
      // this.propertyData = foundCategory[0].propertyList;
      // this.itemData = foundCategory[0].propertyList[0].itemList;

    } else {

      this.modalService.open(centerDataModal, { centered: true, size: 'lg' });
    }
  }
  tagDateModel(formGroup: FormGroup, centerDataModal: string) {

    this.isAssignMapping = true;
    this.selectedMapingFormGroup = formGroup;
    // console.log(this.selectedMapingFormGroup);
    this.modalService.open(centerDataModal, { centered: true, windowClass: "modalClass" });
  }
  tagViewDateModel(formGroup: FormGroup, centerDataModal: string) {
    this.isAssignMapping = true;
    this.isAlreadyDateMapped = true;
    this.startDate = moment(formGroup.value.startDate).format("YYYY-MM-DD");
    this.endDate = moment(formGroup.value.endDate).format("YYYY-MM-DD");
    this.selectedMapingFormGroup = formGroup;
    this.modalService.open(centerDataModal, { centered: true, windowClass: "modalClass" });
  }
  getCoursePlannerMapping() {
    let type: any;
    if (this.showMappingBtnInChapter == true) {
      type = 2;
    } else if (this.showMappingBtnInTopic == true) {
      type = 3;
    }
    this._cpService.getCoursePlannermapping(this.syllabusPlannerId, type).subscribe(
      res => {
        // this.getItemsList(uniqueId);
        this.mappingListData = res;
        // console.log(JSON.stringify(this.mappingListData));
        if (this.mappingListData != null && this.mappingListData.mappingList != null && this.mappingListData.mappingList.length > 0) {
          let items = [];
          for (let i = 0; i < this.mappingListData.mappingList.length; i++) {
            let dataToPush = this.fb.group({
              id: this.mappingListData.mappingList[i].id,
              categoryId: this.mappingListData.mappingList[i].categoryId,
              propertyId: this.mappingListData.mappingList[i].propertyId,
              itemId: this.mappingListData.mappingList[i].itemId,
              itemValue: this.mappingListData.mappingList[i].itemValue
            });
            items.push(dataToPush);
          }
          this.mappingForm.setControl('mappingList', this.fb.array(items));
          this.selectedMappingId = this.mappingListData.mappingList[0].id;
          this.selectedCategory = this.mappingListData.mappingList[0].categoryId;
          if (this.selectedCategory) {
            this.categoryData.filter(element => {
              if (element.id === this.selectedCategory) {
                this.propertyData = element.propertyList;
                // console.log(this.propertyData)
              }
            });
          }
          this.selectedproperty = this.mappingListData.mappingList[0].propertyId;
          if (this.selectedproperty) {
            this.propertyData.filter(element => {
              if (element.id === this.selectedproperty) {
                this.itemData = element.itemList;
              }
            });
          }
          this.selecteditemId = this.mappingListData.mappingList[0].itemId;
          this.selecteditemValue = this.mappingListData.mappingList[0].itemValue
          if (this.selectedCategory != null && this.selectedproperty != null && this.selecteditemId != null) {
            this.isAlreadyMapped = true
          }
        } else {
          this.selectedMappingId = null;
          this.selectedCategory = null;
          this.selectedproperty = null;
          this.selecteditemId = null;
          this.selecteditemValue = null;
        }


      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });

  }
  handleChange(event) {
    this.selectedMapping = event.id
    if (this.plannerType == 2) {
      if (event.id == 2) {
        this.showMappingBtnInChapter = true;
      } else if (event.id == 3) {
        this.showMappingBtnInTopic = true;
      }
    } else {
      //this.saveTagging();
    }
  }
  saveTagging() {

    let tagLevelDto = {
      coursePlannerId: this.syllabusPlannerId,
      taggingLevelId: this.selectedMapping

    }
    this._cpService.saveTaggingLevel(tagLevelDto).subscribe(
      res => {
        // this.getItemsList(uniqueId);
        this.toastService.showSuccess("Mapping Level Saved Succesfully");
        this.getmappingLevel();

      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });


  }
  getmappingLevel() {
    this.showMappingBtnInChapter = false;
    this.showMappingBtnInTopic = false;
    this._cpService.getMappingLevel(this.syllabusPlannerId).subscribe(
      res => {
        // this.getItemsList(uniqueId);

        if (res.taggingLevelId == 2) {
          this.showMappingBtnInChapter = true;
        } else if (res.taggingLevelId == 3) {
          this.showMappingBtnInTopic = true;
        }
        if (res && res.taggingLevelId) {
          this.selectedMapping = res.taggingLevelId
          this.getCoursePlannerMapping()
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });


  }
  closeModal() {
    if (this.isAssignMapping) {
      this.modalService.dismissAll();
    } else {
      this.isCreateMapping = false
      this.isAssignMapping = true
    }
    this.addMappingSubmit = false
    this.createMappingSubmit = false
    if (this.selectedMappingId && this.selectedCategory != null && this.selectedproperty != null && this.selecteditemId != null) {
      this.isAlreadyMapped = true
    } else {
      this.isAlreadyMapped = false
    }
  }
  saveMapping() {
    if (this.isAssignMapping) {
      this.addMappingSubmit = true
      if (this.selectedCategory == null || this.selectedproperty == null || this.selecteditemId == null) {
        return;
      }
      this.spinner.show()
      let chapterId = this.selectedMapingFormGroup.controls.id.value;

      let type: any;

      if (this.showMappingBtnInChapter == true) {
        type = 2;

      } else if (this.showMappingBtnInTopic == true) {
        type = 3;
      }

      // let id = this.mappingForm.controls['id'].value
      let coursePlannerMappingDto = {
        "mappingType": type,
        "id": chapterId,
        "coursePlannerId": this.syllabusPlannerId,
        "mappingList": [
          {
            "id": this.selectedMappingId,
            "categoryId": this.selectedCategory,
            "propertyId": this.selectedproperty,
            "itemId": this.selecteditemId,
            "itemValue": this.selecteditemValue
          }]
      }
      // console.log(coursePlannerMappingDto);



      this._cpService.saveMapping(coursePlannerMappingDto).subscribe(syllabusData => {
        this.toastService.showSuccess("Mapping Saved successfully..");
        this.getCoursePlannerMapping();
        this.modalService.dismissAll()
        this.spinner.hide()

      }, (error) => {
       // this.toastService.ShowError(error);
        this.spinner.hide()
      })


    } else {

    }
  }
  saveDateMapping() {
    this.addMappingSubmit = true
    // console.log(this.selectedMapingFormGroup);

    if (!this.selectedMapingFormGroup.value.startDate || !this.selectedMapingFormGroup.value.endDate) {
      // console.log("test");

      return;
    }
    this.modalService.dismissAll();
    this.addMappingSubmit = false
  }
  createMapping() {
    this.isAssignMapping = false;
    this.isCreateMapping = true;
    // console.log(this.formData);

  }
  selectCategory(event: any) {
    this.selectedproperty = null;
    this.selecteditemId = null;
    this.selecteditemValue = null;

    this.selectedCategory = Number(event.id);
    this.categoryData.filter(element => {
      if (element.id === this.selectedCategory) {
        this.propertyData = element.propertyList;
        // console.log(this.propertyData)
      }
    });

  }


  selectProperty(event: any) {
    this.selecteditemId = null;
    this.selecteditemValue = null;

    this.selectedproperty = Number(event.id)
    this.propertyData.filter(element => {
      if (element.id === this.selectedproperty) {
        this.itemData = element.itemList;
      }
    });

  }
  selectItem(event: any) {
    this.selecteditemValue = null;
    // if (item != null) {
    //   let selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
    //   let test = this.itemData.find(el => el.name === selectElementText);
    //   this.selecteditemId = test.id;
    // }
    this.selecteditemId = Number(event.id)
    if (this.mappingListData != null) {
      this.mappingListData.mappingList.filter(element => {
        if (element.itemId === this.selecteditemId) {
          this.selecteditemId = null;
          this.selecteditemValue = null;
          this.toastService.ShowWarning("Mapping already exist for selected item");
        }
      });
    }
  }
  getCategoryList() {

    this.categoryData = [];
    return this._cpService.getCategoryList(sessionStorage.getItem('orgUniqueId')).subscribe(
      res => {
        if (res) {
          this.categoryData = res.categoryList;
          this.patchCategoryList(this.categoryData);

        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {

        //   this.toastService.ShowWarning(error);
        // }
      });
  }



  patchCategoryList(categoryData) {

    if (categoryData != null && categoryData != null && categoryData.length > 0) {

      let categoryList = [];
      for (let i = 0; i < categoryData.length; i++) {
        let category = this.categoryData[i];
        // console.log("Category", category)
        let categotyGroup = this.fb.group({
          id: [category.id, Validators.nullValidator],
          name: [category.name, Validators.required],
          uniqueId: [category.uniqueId, Validators.nullValidator],
          createdOrUpdatedBy: [sessionStorage.getItem('uniqueId')],
          propertyList: this.fb.array(this.patchPropertyList(categoryData[i].propertyList))


        })

        categoryList.push(categotyGroup);
        // this.getPropertyList(category.uniqueId);
      }
      this.formData.setControl('categoryList', this.fb.array(categoryList));

    }

  }

  patchPropertyList(property: any) {
    let propertyList = [];
    if (property != null && property.length > 0) {
      for (let i = 0; i < property.length; i++) {
        let formGroup = this.fb.group({
          id: [property[i].id, Validators.nullValidator],
          name: [property[i].name, Validators.required],
          uniqueId: [property[i].uniqueId, Validators.nullValidator],
          createdOrUpdatedBy: [sessionStorage.getItem('uniqueId')],
          itemList: this.fb.array(this.patchItemList(property[i].itemList))
        })
        propertyList.push(formGroup);
        // this.getItemsList(property[i].uniqueId);
      }
    }

    return propertyList;
  }

  patchItemList(item: any) {

    let itemList = [];
    if (item != null && item.length > 0) {
      for (let i = 0; i < item.length; i++) {
        let formGroup = this.fb.group({
          id: [item[i].id, Validators.nullValidator],
          name: [item[i].name, Validators.required],
          uniqueId: [item[i].uniqueId, Validators.nullValidator],
          createdOrUpdatedBy: [sessionStorage.getItem('uniqueId')],
          itemValue: [item[i].itemValue, Validators.required]

        })
        itemList.push(formGroup);
      }
    }
    return itemList;
  }
  addCategory() {
    let categoryList = <FormArray>this.formData.controls['categoryList'];
    categoryList.push(this.initCategoryList());
  }
  initCategoryList() {
    return this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.required],
      isNew: [true, Validators.required],
      createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.nullValidator],
      propertyList: this.fb.array([])
    });
  }
  addProperty(property) {

    let propertyList = <FormArray>property.controls['propertyList'];
    let propertyGroup = this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      isNew: [true, Validators.required],
      name: ['', Validators.required],
      itemList: this.fb.array([])
    });

    propertyList.push(propertyGroup);
  }

  addItem(item) {


    let itemList = <FormArray>item.controls['itemList'];
    let itemGroup = this.fb.group({
      uniqueId: ['', Validators.nullValidator],
      name: ['', Validators.required],
      isNew: [true, Validators.required],
      itemValue: ['', Validators.required],
      createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.nullValidator]

    });

    itemList.push(itemGroup);
  }
  getTeacherList(subjectId: any) {
    let orgList = [];
    orgList.push(sessionStorage.getItem('organizationId'));
    let pageIndex = 0;
    let subjectList = [];
    subjectList.push(subjectId);
    let teacherFilterDto = {
      orgList,
      pageIndex
    }
    this._cpService.getTeacher(teacherFilterDto).subscribe(
      res => {
        if (res) {
          this.teacherList = res;
          // console.log("TEACHER DATA" + JSON.stringify(res));
        }
      },
      error => {
      });
  }
  changeTeacher(data: any, unitIndex: number, chapterIndex: number) {

    let unitFormArray = <FormArray>this.formData.controls['items'];
    let unit = <FormGroup>unitFormArray.controls[unitIndex];

    let chapterFormArray = <FormArray>unit.controls['items'];
    let chapter = <FormGroup>chapterFormArray.controls[chapterIndex];

    chapter.controls['teacherName'].setValue(data);
  }

  getCoursePlannerDetails(id: any) {
    this.spinner.show();
    this.cpData = [];
    return this._cpService.getCoursePlannerDetails(id).subscribe(
      res => {
        if (res) {
          this.cpdata = res;
          this.formData.controls['uniqueId'].setValue(this.cpdata.uniqueId);
          this.patchUnits(this.cpdata.units);
          // this.showChapterMappingButton(this.cpdata.units);
          // this.showTopicMappingButton(this.cpdata.units);
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide()
        // } else {
        //   this.spinner.hide()
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  getBatchPlannerDetails(id: any) {
    this.spinner.show();
    this.cpData = [];
    return this._cpService.getBatchPlannerDetails(id).subscribe(
      res => {
        if (res) {
          this.cpdata = res;
          this.formData.controls['uniqueId'].setValue(this.cpdata.uniqueId);
          this.patchUnits(this.cpdata.units);
          this.patchBpBatchInfo(this.cpdata);
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide()
        // } else {
        //   this.spinner.hide()
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  deleteUnit(unitIndex: number) {
    Swal.fire({
      position: 'center',
      title: 'Delete',
      text: 'Are you sure you want to delete?',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
    }).then((result) => {
      if (result.value) {
        let durationValue = 0;
        let unitArray = <FormArray>this.formData.controls['units'];

        //check id is "" then save changes = true
        let unitFormGroup = <FormGroup>unitArray.controls[unitIndex];
        let id = unitFormGroup.controls['id'].value;
        if (id != "") {
          this.savePendingChanges(true);
        }

        unitArray.removeAt(unitIndex);
        for (let i = 0; i < unitArray.length; i++) {
          let unitFormGroup = <FormGroup>unitArray.controls[i];
          let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
          durationValue = durationValue + parseInt(unitDetails.controls['duration'].value);
        }
        this.totalDuration = durationValue;
      }
    })
  }

  deleteChapter(unitIndex: number, chapterIndex: number) {
    Swal.fire({
      position: 'center',
      title: 'Delete',
      text: 'Are you sure you want to delete?',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
    }).then((result) => {
      if (result.value) {
        let unitFormArray = <FormArray>this.formData.controls['units'];
        let unit = <FormGroup>unitFormArray.controls[unitIndex];
        let unitDetails = <FormArray>unit.controls['unitDetails'];

        let chapterFormArray = <FormArray>unit.controls['chapters'];
        let chapter = <FormGroup>chapterFormArray.controls[chapterIndex];
        let chapterDetails = <FormArray>chapter.controls['chapterDetails'];

        //check id is "" then save changes = true
        let id = chapter.controls['id'].value;
        if (id != "") {
          this.savePendingChanges(true);
        }

        chapterFormArray.removeAt(chapterIndex)

        let unitTotal = 0;
        let totalDuration = 0;
        let durationName = '';

        let chapterDuration = chapterDetails.controls['duration'].value;
        let unitDuration = unitDetails.controls['duration'].value;

        for (let k = 0; k < chapterFormArray.length; k++) {
          unitTotal = unitDuration - chapterDuration;
        }
        if (unitTotal) {
          unitDetails.controls['duration'].setValue(unitTotal);
        } else {
          unitDetails.controls['duration'].setValue(0);
        }

        durationName = this.format(unitTotal)
        unit.controls['durationName'].setValue(durationName);


        for (let i = 0; i < unitFormArray.length; i++) {
          let unitFormGroup = <FormGroup>unitFormArray.controls[i];
          let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
          totalDuration = totalDuration + parseInt(unitDetails.controls['duration'].value);
        }
        this.totalDuration = totalDuration;
      }
    })
  }


  deleteTopic(unitIndex: number, chapterIndex: number, topicIndex: number) {
    Swal.fire({
      position: 'center',
      title: 'Delete',
      text: 'Are you sure you want to delete?',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
    }).then((result) => {
      if (result.value) {
        let unitFormArray = <FormArray>this.formData.controls['units'];
        let unit = <FormGroup>unitFormArray.controls[unitIndex];
        let unitDetails = <FormArray>unit.controls['unitDetails'];

        let chapterFormArray = <FormArray>unit.controls['chapters'];
        let chapter = <FormGroup>chapterFormArray.controls[chapterIndex];
        let chapterDetails = <FormArray>chapter.controls['chapterDetails'];

        let topicFormArray = <FormArray>chapter.controls['topics'];
        let topic = <FormGroup>topicFormArray.controls[topicIndex];
        let topicDetails = <FormArray>topic.controls['topicDetails'];

        //check id is "" then save changes = true
        let id = topic.controls['id'].value;
        if (id != "") {
          this.savePendingChanges(true);
        }

        topicFormArray.removeAt(topicIndex)

        let chaptertotal = 0;
        let unitTotal = 0;
        let totalDuration = 0;
        this.topicSum = 0;

        let unitDuration = unitDetails.controls['duration'].value;
        let chapterDuration = chapterDetails.controls['duration'].value;
        let topicDuration = topicDetails.controls['duration'].value;

        let durationName = '';

        for (let j = 0; j < topicFormArray.length; j++) {
          chaptertotal = chapterDuration - topicDuration;
        }
        if (chaptertotal) {
          chapterDetails.controls['duration'].setValue(chaptertotal);
        } else {
          chapterDetails.controls['duration'].setValue(null);
        }
        durationName = this.format(chaptertotal)
        chapter.controls['durationName'].setValue(durationName);

        for (let k = 0; k < chapterFormArray.length; k++) {
          unitTotal = unitDuration - topicDuration;
        }

        if (unitTotal) {
          unitDetails.controls['duration'].setValue(unitTotal);
        } else {
          unitDetails.controls['duration'].setValue(0);
        }


        durationName = this.format(unitTotal)
        unit.controls['durationName'].setValue(durationName);

        for (let i = 0; i < unitFormArray.length; i++) {
          let unitFormGroup = <FormGroup>unitFormArray.controls[i];
          let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
          totalDuration = totalDuration + parseInt(unitDetails.controls['duration'].value);
        }
        this.totalDuration = totalDuration;


      }
    })
  }


  deleteSubTopic(unitIndex: number, chapterIndex: number, topicIndex: number, subTopicIndex: number) {
    Swal.fire({
      position: 'center',
      title: 'Delete',
      text: 'Are you sure you want to delete?',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
    }).then((result) => {
      if (result.value) {
        let unitFormArray = <FormArray>this.formData.controls['units'];
        let unit = <FormGroup>unitFormArray.controls[unitIndex];
        let unitDetails = <FormArray>unit.controls['unitDetails'];

        let chapterFormArray = <FormArray>unit.controls['chapters'];
        let chapter = <FormGroup>chapterFormArray.controls[chapterIndex];
        let chapterDetails = <FormArray>chapter.controls['chapterDetails'];

        let topicFormArray = <FormArray>chapter.controls['topics'];
        let topic = <FormGroup>topicFormArray.controls[topicIndex];
        let topicDetails = <FormArray>topic.controls['topicDetails'];

        let subTopicArray = <FormArray>topic.controls['subtopics'];
        let subTopic = <FormGroup>subTopicArray.controls[subTopicIndex];
        let subTopicDetails = <FormArray>subTopic.controls['subTopicDetails'];

        //check id is "" then save changes = true
        let id = subTopic.controls['id'].value;
        if (id != "") {
          this.savePendingChanges(true);
        }

        subTopicArray.removeAt(subTopicIndex)

        let topictotal = 0;
        let chaptertotal = 0;
        let unitTotal = 0;
        this.topicSum = 0;

        let unitDuration = unitDetails.controls['duration'].value;
        let chapterDuration = chapterDetails.controls['duration'].value;
        let topicDuration = topicDetails.controls['duration'].value;
        let subTopicDuration = subTopicDetails.controls['duration'].value;

        let durationName = '';

        for (let i = 0; i < subTopicArray.length; i++) {
          topictotal = topicDuration - subTopicDuration;
        }

        if (topictotal) {
          topicDetails.controls['duration'].setValue(topictotal);
        } else {
          topicDetails.controls['duration'].setValue(0);
        }

        durationName = this.format(topictotal)
        topic.controls['durationName'].setValue(durationName);

        for (let j = 0; j < topicFormArray.length; j++) {
          chaptertotal = chapterDuration - subTopicDuration;
        }
        if (chaptertotal) {
          chapterDetails.controls['duration'].setValue(chaptertotal);
        } else {
          chapterDetails.controls['duration'].setValue(0);
        }

        durationName = this.format(chaptertotal)
        chapter.controls['durationName'].setValue(durationName);

        for (let k = 0; k < chapterFormArray.length; k++) {
          unitTotal = unitDuration - subTopicDuration;
        }
        if (unitTotal) {
          unitDetails.controls['duration'].setValue(unitTotal);
        } else {
          unitDetails.controls['duration'].setValue(0);
        }

        durationName = this.format(unitTotal)
        unit.controls['durationName'].setValue(durationName);


        let totalDuration = 0;
        for (let i = 0; i < unitFormArray.length; i++) {
          let unitFormGroup = <FormGroup>unitFormArray.controls[i];
          let unitDetails = <FormArray>unitFormGroup.controls['unitDetails'];
          totalDuration = totalDuration + parseInt(unitDetails.controls['duration'].value);
        }
        this.totalDuration = totalDuration;


      }
    })
  }

  savePendingChanges(value: boolean) {
    this._cpService.saveChangesPending.next(value);
  }

  getMasterCoursePlannerDetails(id: any) {
    this.spinner.show();
    this.cpData = [];
    return this._cpService.getMasterCoursePlanner(id).subscribe(
      res => {
        if (res) {
          this.cpdata = res;
          this.formData.controls['uniqueId'].setValue(this.cpdata.uniqueId);
          this.patchUnits(this.cpdata.units);
          // this.showChapterMappingButton(this.cpdata.units);
          // this.showTopicMappingButton(this.cpdata.units);
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide()
        // } else {
        //   this.spinner.hide()
        //   this.toastService.ShowWarning(error);
        // }
      });
  }

  returnFormData(formData: any) {
    let dataToSend = {
      "isEdit": true,
      "formData": formData,
      "batchUniqueId": this.batchUniqueId,
      "coursePlannerName": this.coursePlannerName,
      "coursePlannerId": this.syllabusPlannerId
    }
    this.returnData.emit(dataToSend);
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    // console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }
}
