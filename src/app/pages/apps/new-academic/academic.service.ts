import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AcademicService {

  constructor(private http: HttpClient) { }

  //Academic year
  public getAcademicYears(organizationId: any) {
    return this.http.get<any>(environment.apiUrl + '/get/academic/years/'+ organizationId);
  }
  
  saveAcademicYear(academicYearDto : any){
    return this.http.post<any>(environment.apiUrl + '/add/academic/years' , academicYearDto);
    
  }

  deleteAcademicYear(deleteAcademicYearFto :any){
    return this.http.post<any>(environment.apiUrl + '/delete/academic/years' , deleteAcademicYearFto);
  }

  //Academic session
  public getAcademicYearListByDate(orgUniqueId: string, date: string) {
    return this.http.get<any>(environment.apiUrl + '/get/academic/years/' + orgUniqueId + '/' + date);
  }

  public getAcademicCalendarList(orgUniqueId: string) {
    return this.http.get<any>(environment.apiUrl + '/get/academic/calendars/' + orgUniqueId);
  }

  addOrUpdateAcademicCalendar(data: any) {
    return this.http.post<any>(environment.apiUrl + '/add/academic/calendar', data);
  }

  deleteAcademicCalendar(data: any) {
    return this.http.post<any>(environment.apiUrl + '/delete/academic/calendar', data);
}

//Subject

public getSubjectList(organizationId: any) {
  return this.http.get<any>(environment.apiUrl + '/org/subject/list/'+ organizationId);
}


  //Board-Grade

  public getGradeBoardList(organizationId: any) {
    return this.http.get<any>(environment.apiUrl + '/get/boards/grade/'+ organizationId);
  }

  public getMasterBoardList() {
    return this.http.get<any>(environment.apiUrl + '/get/master/organization/board/list');
  }

  public getMasterGradeList() {
    return this.http.get<any>(environment.apiUrl + '/get/master/organization/grade/list');
  }

  saveBoardGradeData(data){
    return this.http.post(environment.apiUrl + "/add/update/boards/grade", data)
  }


  //Course 

  public getCourseList(organizationId: any) {
    return this.http.get<any>(environment.apiUrl + '/get/course/and/reference/'+ organizationId);
  }

  public getMasterCourseList() {
    return this.http.get<any>(environment.apiUrl + '/get/master/organization/course/list');
  }

  //Batch

  public getBatchList(organizationId) {
    return this.http.get<any>(environment.apiUrl + '/batch/list/'+ organizationId);
  }

  removeBranch(branch: any) {
    return this.http.post<any>(environment.apiUrl + '/batch/delete', branch);
  }

  updateBranch(branch: any) {
    return this.http.post<any>(environment.apiUrl + '/batch/update', branch);
  }

  saveBranch(branch: any) {
    return this.http.post<any>(environment.apiUrl + '/batch/addOrUpdate', branch);
  }
  //Class room

  public getClassRoom(organizationId: string) {
    return this.http.post<any>(environment.apiUrl + '/classroom/list', organizationId);
  }

  public removeClassRoom(data) {
    return this.http.post<any>(environment.apiUrl + '/classroom/value/delete', data);
  }
}
