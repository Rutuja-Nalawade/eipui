import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthServiceService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        // debugger
        const currentUser = this.authenticationService.currentUser();
        if (currentUser && currentUser.tokenId) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.tokenId}`
                }
            });
        }

        return next.handle(request);
    }
}
