import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewSetupService {

  constructor(private http: HttpClient) { }


//   //Board and Grade
  getBoardGradeList(orgId): Observable<any> {
    // let userData = JSON.parse(localStorage.getItem('userData'));
    // return this.http.get(environment.apiUrl + "/get/board/list/" + orgId)
    return this.http.get(environment.apiUrl + "/get/boards/grade/" + orgId)
  }

  getBoardListFromMasterOrg() {
    return this.http.get<any>(environment.apiUrl + "/get/master/organization/board/list");
  }
  getGradeListFromMasterOrg() {
    return this.http.get<any>(environment.apiUrl + "/get/master/organization/grade/list");
  }
  saveBoardGradeData(data){
    return this.http.post(environment.apiUrl + "/add/update/boards/grade", data)
  }

}
