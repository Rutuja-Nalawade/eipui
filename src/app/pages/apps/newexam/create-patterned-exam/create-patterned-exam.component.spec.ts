import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePatternedExamComponent } from './create-patterned-exam.component';

describe('CreatePatternedExamComponent', () => {
  let component: CreatePatternedExamComponent;
  let fixture: ComponentFixture<CreatePatternedExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePatternedExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePatternedExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
