import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { StaffRoutingModule } from './staff-routing.module';
import { ListComponent } from './list/list.component';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { WorkTimeComponent } from './work-time/work-time.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ChartsModule } from 'ng2-charts';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { CommonComponentModule } from 'src/app/components/common/common-component.module';
import { BsDropdownModule } from 'ngx-bootstrap';
@NgModule({
  declarations: [ListComponent, WorkTimeComponent],
  imports: [
    CommonModule,
    StaffRoutingModule,
    UIModule,
    NgbTabsetModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    SharedModule,
    NgApexchartsModule,
    ChartsModule,
    NgbDropdownModule,
    BsDropdownModule.forRoot(),
    CommonComponentModule
  ],
  providers: [DatePipe],
  exports: [ListComponent, WorkTimeComponent]
})
export class StaffModule { }
