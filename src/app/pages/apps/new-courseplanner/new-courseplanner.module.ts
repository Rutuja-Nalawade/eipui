import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewCourseplannerRoutingModule } from './new-courseplanner-routing.module';
import { CourseplannerListComponent } from './courseplanner-list/courseplanner-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { BsDatepickerModule, BsDropdownModule, CarouselModule } from 'ngx-bootstrap';
import { CreateCoursePlannerComponent } from './create-course-planner/create-course-planner.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CoursePlannerDetailsComponent } from './course-planner-details/course-planner-details.component';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { DeactivateGuard } from './deactivate.gaurd';


@NgModule({
  declarations: [CourseplannerListComponent, CreateCoursePlannerComponent, CoursePlannerDetailsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    SharedModule,
    UIModule,
    NgbModule,
    ChartsModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CarouselModule.forRoot(),
    DragDropModule,
    NewCourseplannerRoutingModule
  ],
  bootstrap: [CourseplannerListComponent],
  providers: [DeactivateGuard]
})
export class NewCourseplannerModule { }
