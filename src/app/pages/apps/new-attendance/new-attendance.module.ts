import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewAttendanceRoutingModule } from './new-attendance-routing.module';
import { CommonTopBarComponent } from './common-top-bar/common-top-bar.component';
import { StudentSheetComponent } from './student-sheet/student-sheet.component';

import { TeacherSheetComponent } from './teacher-sheet/teacher-sheet.component';
import { StaffSheetComponent } from './staff-sheet/staff-sheet.component';


@NgModule({
  declarations: [CommonTopBarComponent, StudentSheetComponent, TeacherSheetComponent, StaffSheetComponent],
  imports: [
    CommonModule,
    NewAttendanceRoutingModule
  ]
})
export class NewAttendanceModule { }
