import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-time-table-list',
  templateUrl: './time-table-list.component.html',
  styleUrls: ['./time-table-list.component.scss']
})
export class TimeTableListComponent implements OnInit {
  isApplyDates: boolean = false;
  isViewHistory: boolean = false;
  applyDatesForm: FormGroup;
  isPublish: boolean = true;
  isUnPublish: boolean = false;
  isExpire: boolean = false;
  isviewConflict: boolean = false
  isviewTimeTable: boolean = false;
  itemList = [];
  isJeeAdd: boolean = true;
  isJeeMains: boolean = false;
  isJeeNeet: boolean = false;
  isJeeBoard: boolean = false;
  isBlur: boolean = false;
  isAddShift = false;
  timetableData = null;
  isViewXml: boolean = false;
  timeTableList = [];
  pageIndex = 0;
  isEdit = false;
  submitted = false;
  isApplyDatesConflicts = false;
  timeTableType = 1;
  conflictData: any;
  selectedTimeTableData: any = "";
  typeList = ["", "Manual", "Import", "Automatic"];
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  min = new Date();
  @Input() timetableType: any
  @Output() callBlur = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder, private timeTableService: TimeTableServiceService, private toastService: ToastsupportService, private spinner: NgxSpinnerService, private datePipe: DatePipe) { }

  ngOnInit() {
    // this.min.setDate(this.min.getDate() + 1);
    // this.getTimetableLsit(1);
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['timetableType'] && changes['timetableType'].currentValue) {
      this.getTimetableLsit(this.timeTableType);
    }
  }
  initappyDatesForm() {
    this.applyDatesForm = this.formBuilder.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      conflicts: [false, Validators.nullValidator],
      isStartDateGreater: [false, Validators.nullValidator],
      isEndDateGreater: [false, Validators.nullValidator]
    })
  }
  getTimetableLsit(type) {
    this.timeTableList = [];
    this.spinner.show();
    this.timeTableService.getTimetableList(type, this.pageIndex).subscribe(
      res => {
        if (res && res.success) {
          this.timeTableList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage);
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error)
        // }
        this.spinner.hide();
      });
  }

  appyDates(data) {
    this.initappyDatesForm();
    this.selectedTimeTableData = data;
    this.isApplyDates = true;
    this.isBlur = true;
    this.callBlur.emit('true');
  }

  viewHistory(data) {
    this.selectedTimeTableData = data;
    this.isBlur = true;
    this.isViewHistory = true;
  }

  viewConflict() {
    this.isviewConflict = true;

  }

  close() {
    this.isApplyDates = false;
    this.isBlur = false;
    this.isviewConflict = false;
    this.isApplyDatesConflicts = false;
    this.callBlur.emit('false');
  }

  closeXml(value) {
    this.isViewXml = false
  }
  no() {
    this.isApplyDatesConflicts = false;
  }
  viewTimeTableDetails(data) {
    this.selectedTimeTableData = data;
    this.isviewTimeTable = true;
    this.isBlur = true;
  }
  changeDate(event) {
    let startDate = this.applyDatesForm.value.startDate;
    let endDate = this.applyDatesForm.value.endDate;
    if (startDate && endDate) {
      let dto = {
        startDate: this.datePipe.transform(startDate, "yyyy-MM-dd"),
        endDate: this.datePipe.transform(endDate, "yyyy-MM-dd")
      }
      this.timeTableService.getPreApplyExicution(dto).subscribe(
        (res) => {
          if (res) {
            if (res.success) {
              this.conflictData = res;
              this.applyDatesForm.controls.conflicts.setValue(true);
            } else {
              this.isApplyDatesConflicts = false;
              this.applyDatesForm.controls.conflicts.setValue(false);
            }
            this.spinner.hide();
          }
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.spinner.hide();
        });
    }
  }
  saveAppyDates() {
    console.log("value", this.applyDatesForm)
    this.submitted = true;
    if (this.applyDatesForm.invalid) {
      return;
    }
    if (this.applyDatesForm.value.conflicts && !this.isApplyDatesConflicts) {
      this.isApplyDatesConflicts = true;
    } else {
      let startDate = this.applyDatesForm.value.startDate;
      let endDate = this.applyDatesForm.value.endDate;
      let dto = {
        uniqueId: this.selectedTimeTableData.uniqueId,
        startDate: this.datePipe.transform(startDate, "yyyy-MM-dd"),
        endDate: this.datePipe.transform(endDate, "yyyy-MM-dd")
      }
      console.log(dto);

      this.spinner.show();
      this.timeTableService.applyExicution(dto).subscribe(
        (res) => {
          if (res) {
            this.isApplyDatesConflicts = false;
            this.submitted = false;
            this.toastService.showSuccess("Apply date added succesfully");
            this.close();
            this.getTimetableLsit(this.timeTableType);
            this.spinner.hide();
          }
        },
        (error: HttpErrorResponse) => {
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          // } else {
          //   this.toastService.ShowError(error);
          // }
          this.isApplyDatesConflicts = false;
          this.submitted = false;
          this.spinner.hide();
        });
    }
  }

  tabChange(type) {
    this.pageIndex = 0;
    this.timeTableType = type
    this.getTimetableLsit(type);
  }


  backPage(value) {
    console.log("value", value)
    this.isViewHistory = false;
    this.isviewTimeTable = false;
    this.isBlur = false;
  }

  backPageConflict(value) {
    this.isviewConflict = value;
  }
  editTimetable(data) {
    this.isAddShift = true;
    this.isBlur = true;
    this.isEdit = true;
    this.timetableData = data;
  }
  closeAddShiftForm(event) {
    this.isAddShift = false;
    this.isBlur = false;
    this.getTimetableLsit(this.timeTableType);
  }

  viewXml(data) {
    this.isViewXml = true;
    this.selectedTimeTableData = data;
  }
}
