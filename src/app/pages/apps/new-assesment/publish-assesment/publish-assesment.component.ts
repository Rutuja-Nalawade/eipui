import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

import { AssesmentService } from 'src/app/servicefiles/assesment.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { DatePipe } from '@angular/common';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-publish-assesment',
  templateUrl: './publish-assesment.component.html',
  styleUrls: ['./publish-assesment.component.scss']
})
export class PublishAssesmentComponent implements OnInit {
  public dateTimeRange: any;
  subjectsList = [];
  publishExamForm: FormGroup;
  isAessementMode: boolean = false;
  isDate1: boolean = false;
  isDate2: boolean = false;
  isDate3: boolean = false;
  isDate4: boolean = false;
  isAddMoreStudent: boolean = false;
  @Output() close = new EventEmitter<any>();
  @Input() examDataPublish: any
  dateTime1; any;
  showAnswer = [];
  showResult = [];
  showSolution = [];
  showRank = [];
  showAnswer1: any;
  showResult1: any;
  showSolution1: any;
  showRank1: any;
  showAnswerdate: any;
  showResultdate: any;
  showSolutiondate: any;
  showRankdate: any;
  masterSelected: Boolean;
  studentList = [];
  checkedList: any;
  isMode = '';
  assessmentMode: any;
  endDate: any;
  todayDate = new Date();
  constructor(private _examService: AssesmentService,
    private spinner: NgxSpinnerService,
    private toastService: ToastsupportService,
    private datePipe: DatePipe,
    private batchService: BatchesServiceService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.getSettings();
    this.initPublishForm();
    console.log("examDataPublish", this.examDataPublish)
  }

  initPublishForm() {
    this.publishExamForm = this.fb.group({
      startDate: [null, Validators.nullValidator],
      endDate: [null, Validators.nullValidator],
      assessmentMode: [null, Validators.nullValidator],
      deviceType: [null, Validators.nullValidator],
      showAns: [null, Validators.nullValidator],
      showSol: [null, Validators.nullValidator],
      showRes: [null, Validators.nullValidator],
      showRank: [null, Validators.nullValidator],

    })
  }
  mediumChange(event) {
    this.assessmentMode = event.target.value;
    console.log(event.target.value)
    if (event.target.value == 2) {
      this.isAessementMode = false;
    } else {
      this.isAessementMode = true;
    }
  }

  modeChange(event) {
    this.isMode = event.target.value;
  }

  cancel() {
    this.close.emit(false);
  }

  showAnswerOnDate(event, id) {
    this.showAnswer1 = event.id;

    let ele = document.getElementById(id);

    if (event.id == 28) {
      this.isDate1 = true;

      ele.classList.remove('col-6');
      ele.classList.add('col-5')

    } else {
      let obj = { id: 28, name: 'on a Date' };
      this.showAnswer.splice(2, 1);
      this.showAnswer = [...this.showAnswer, obj]
      this.isDate1 = false;
      ele.classList.remove('col-5');
      ele.classList.add('col-6');
    }
  }
  showSolutionOnDate(event, id) {
    this.showSolution1 = event.id;
    let ele = document.getElementById(id);
    if (event.id == 31) {
      this.isDate2 = true;
      ele.classList.remove('col-6');
      ele.classList.add('col-5');
    } else {
      let obj = { id: 31, name: 'on a Date' };
      this.showSolution.splice(2, 1);
      this.showSolution = [...this.showSolution, obj]
      this.isDate2 = false;
      ele.classList.remove('col-5');
      ele.classList.add('col-6');
    }

  }

  showResultOnDate(event, id) {
    this.showResult1 = event.id;
    let ele = document.getElementById(id);
    if (event.id == 34) {
      this.isDate3 = true;
      ele.classList.remove('col-6');
      ele.classList.add('col-5');
    } else {
      let obj = { id: 34, name: 'on a Date' };
      this.showResult.splice(2, 1);
      this.showResult = [...this.showResult, obj]
      this.isDate3 = false;
      ele.classList.remove('col-5');
      ele.classList.add('col-6');
    }


  }

  showRankOnDate(event, id) {
    this.showRank1 = event.id;
    let ele = document.getElementById(id);
    if (event.id == 37) {
      this.isDate4 = true;
      ele.classList.remove('col-6');
      ele.classList.add('col-5');
    } else {
      let obj = { id: 37, name: 'on a Date' };
      this.showRank.splice(2, 1);
      this.showRank = [...this.showRank, obj]
      this.isDate4 = false;
      ele.classList.remove('col-5');
      ele.classList.add('col-6');
    }

  }




  subCancel(event) {
    this.isAddMoreStudent = false;
    this.isDate1 = false;
    this.isDate2 = false;
    this.isDate3 = false;
    this.isDate4 = false;
  }

  onDateChange(event, type) {

    console.log("test", event);
    if (type == 1) {
      this.endDate = event;
    } else if (type == 2) {

      this.showAnswerdate = event;

      let onDate = this.datePipe.transform(this.showAnswerdate, "dd-MM-yyyy");

      let obj = { id: this.showAnswer1, name: onDate }
      this.showAnswer.splice(2, 1);
      console.log("this.showAnswer", this.showAnswer);
      this.showAnswer = [...this.showAnswer, obj]
      // console.log("this.showAnswer", this.showAnswer);


    } else if (type == 3) {
      this.showSolutiondate = event;
      let onDate = this.datePipe.transform(this.showSolutiondate, "dd-MM-yyyy");

      let obj = { id: this.showSolution1, name: onDate }
      this.showSolution.splice(2, 1);
      this.showSolution = [...this.showSolution, obj];
    } else if (type == 4) {
      this.showResultdate = event;
      let onDate = this.datePipe.transform(this.showResultdate, "dd-MM-yyyy");
      let obj = { id: this.showResult1, name: onDate }
      this.showResult.splice(2, 1);
      this.showResult = [...this.showResult, obj];
    } else if (type = 5) {
      this.showRankdate = event;
      let onDate = this.datePipe.transform(this.showRankdate, "dd-MM-yyyy");

      let obj = { id: this.showRank1, name: onDate }
      this.showRank.splice(2, 1);
      this.showRank = [...this.showRank, obj];
    }

  }
  getSettings() {
    this.spinner.show();
    this._examService.getExamSetting(this.examDataPublish.uniqueId).subscribe(
      (res: any) => {
        if (res) {

          console.log("id", res)
          this.showAnswer = res.list[0].subSettingList
          this.showSolution = res.list[1].subSettingList
          this.showResult = res.list[2].subSettingList
          this.showRank = res.list[3].subSettingList
          this.spinner.hide();

        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   console.log("Client-side error occured.");
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   // this.toaterService.ShowWarning(error.error.errorMessage);
        // }
        // this.toastService.ShowError("Exam cancel unsuccessfully")
      });
  }

  savePublish() {
    let settings = []
    let obj1 = {
      id: this.showAnswer1,
      onDate: this.showAnswerdate ? this.showAnswerdate : undefined,
    }
    settings.push(obj1)
    let obj2 = {
      id: this.showSolution1,
      onDate: this.showSolutiondate ? this.showSolutiondate : undefined
    }
    settings.push(obj2)
    let obj3 = {
      id: this.showResult1,
      onDate: this.showResultdate ? this.showResultdate : undefined
    }
    settings.push(obj3)
    let obj4 = {
      id: this.showRank1,
      onDate: this.showRankdate ? this.showRankdate : undefined,
    }
    settings.push(obj4)
    const obj = {
      assessmentUid: this.examDataPublish.uniqueId,
      // endDate: this.endDate[0],
      endDate: "",
      assessmentMode: Number(this.assessmentMode),
      deviceType: Number(this.isMode),
      isPublic: false,
      settingList: settings
    }
    console.log("obj", obj)
    this.spinner.show();
    this._examService.savePublishExam(obj).subscribe(
      (res: any) => {
        if (res) {

          console.log("res", res)
          this.spinner.hide();
          this.close.emit(false)


        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("error", error);
        //   this.toastService.ShowWarning(error.error);
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }
  isAddMoreStudents() {
    this.isAddMoreStudent = true;
  }

}
