import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ExamService } from '../exam.service';
declare var $:any;
@Component({
  selector: 'app-examsyllabus',
  templateUrl: './examsyllabus.component.html',
  styleUrls: ['./examsyllabus.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExamsyllabusComponent implements OnInit {

  @Input() examSyllabus: FormGroup;
  @Input() selectedSubjects = [];
  @Input() courseId: number;
  @Input() type: any;
  rem:boolean=false;
  subjectsList:any=[]
  isUnit1: any;
  isUnit2: any;
  isUnit3: any;
  
  constructor(private fb: FormBuilder, private examService: ExamService) { 
  }
  ngOnInit() {
    // $('[data-toggle="tooltip"]').tooltip()

  }

 comparesyllabus = (item: any, selected: any) => {
    if (selected.chapterName && item.chapterName) {
      return item.chapterName === selected.chapterName;
    }
    if (item.name && selected.name) {
      return item.name === selected.name;
    }
    return false;
  };

 
  isCollaps(id,headId){
    
    var ele=document.getElementById(id);
    
    if(ele.classList.contains('isUnhide')){
      ele.classList.remove("isUnhide");
       ele.classList.add("isHide");
        
    }else if(ele.classList.contains('isHide')){
      ele.classList.remove("isHide");
      ele.classList.add("isUnhide");
      
    }

    var element=document.getElementById(headId);
    if(element.classList.contains('fa-minus')){
      element.classList.remove("fa-minus");
      element.classList.add("fa-plus");
        
    }else if(element.classList.contains('fa-plus')){
      element.classList.remove("fa-plus");
      element.classList.add("fa-minus");
      
    }
  
  }

  physics1:any;
  physics2:any;
  physics3:any;
  
 

onSelectionChange(index: number, currentIndex: number, $event:any,type:any) {
  console.log("type",type)
  console.log("event",event)
  console.log("index",index,"currentIndex",currentIndex)
  var ele=document.getElementById(type);
  console.log("ele",ele)
  if($event.target.checked){
    ele.classList.remove("isHideSelect");
    ele.classList.add("isUnhideSelect");
      
   }else{
    ele.classList.remove("isUnhideSelect");
    ele.classList.add("isHideSelect");
  }


  let subjectArray = <FormArray>this.examSyllabus.controls['subjects'];
  let subjectGroup = <FormGroup>subjectArray.controls[index];
  let actualSyllabusArray = <FormArray>subjectGroup.controls['actualSyllabus'];
  let syllabusArray = <FormArray>subjectGroup.controls['syllabus'];
  let actualCPArray = <FormArray>subjectGroup.controls['actualCoursePlanners'];
  let actualCPGroup = <FormGroup>actualCPArray.controls[currentIndex];
  let syllabusCoursewise = <FormArray>actualCPGroup.controls['syllabusCoursewise'];
  let selectedSyllArray = <FormArray>actualCPGroup.controls['selectedsyllabusCoursewise'];
  console.log("selectedSyllArray",selectedSyllArray);

  if ($event.target.checked == true) {
    actualCPGroup.controls['isSelected'].setValue(true);
    
      // this.getSyllabusList(actualCPGroup.cogetSyllabusListntrols['id'].value, actualSyllabusArray);
      this.getSyllabusList(actualCPGroup.controls['id'].value, syllabusCoursewise);
  } else {
    // let actualSyllabusList = actualSyllabusArray.value.filter(x => x.coursePlannerId != actualCPGroup.controls['id'].value);
    syllabusCoursewise.controls = [];
    // selectedSyllArray.controls=[]
    actualCPGroup.setControl('selectedsyllabusCoursewise', this.fb.array([]));
    // console.log("selectedSyllArray",selectedSyllArray);
  }
}

onSelectionChangeRemark($event:any,type:any) {
  console.log("type",type)
  console.log("event",event)
  
  var ele=document.getElementById(type);
  console.log("ele",ele)
  console.log("$event.target.checked",$event.target.checked)
  if($event.target.checked){
    ele.classList.remove("isHideOptionalRem");
    ele.classList.add("isUnhideOptionalRem");
      
   }else{
    ele.classList.remove("isUnhideOptionalRem");
    ele.classList.add("isHideOptionalRem");
  }

}


public getSyllabusList(coursePlannerId: any, syllabusArray: FormArray) {
  return this.examService.getSyllabus(coursePlannerId, this.type).subscribe(
    res => {
      if (res) {
        // syllabusArray.controls = [];
        switch (parseInt(this.type)) {
          case 1:
            for (let syllabus1 of res) {
              syllabusArray.push(this.addUnitSyllabus(syllabus1, coursePlannerId));
            }
            break;
          case 2:
            for (let syllabus1 of res) {
              syllabusArray.push(this.addChapterSyllabus(syllabus1, coursePlannerId));
            }
            break;
          case 4:
            for (let syllabus1 of res) {
              syllabusArray.push(this.addSubTopicSyllabus(syllabus1, coursePlannerId));
            }
            break;
          default:
            for (let syllabus1 of res) {
              syllabusArray.push(this.addTopicSyllabus(syllabus1, coursePlannerId));
            }
        }
      }
      console.log("exam syllabus1",this.examSyllabus)
    },
    (error: HttpErrorResponse) => {
      // if (error.error instanceof Error) {
      //   console.log("Client-side error occured.");
      // } else {
      //   // this.toasterService.ShowError(error.error.errorMessage);
      // }
    });
}

addUnitSyllabus(data: any, coursePlannerId: any) {
  return this.fb.group({
    id: data.unitId,
    name: data.unitName,
    coursePlannerId: coursePlannerId
  })
}

addChapterSyllabus(data: any, coursePlannerId: any) {
  return this.fb.group({
    id: data.chapterId,
    name: data.chapterName,
    coursePlannerId: coursePlannerId
  })
}

addTopicSyllabus(data: any, coursePlannerId: any) {
  return this.fb.group({
    chapterId: data.chapterId,
    chapterName: data.chapterName,
    id: data.topicId,
    name: data.topicName,
    coursePlannerId: coursePlannerId
  })
}

addSubTopicSyllabus(data: any, coursePlannerId: any) {
  return this.fb.group({
    chapterId: data.chapterId,
    chapterName: data.chapterName,
    id: data.subtopicId,
    name: data.subtopicName,
    coursePlannerId: coursePlannerId
  })
}



addSyllabusControl(value: any, index: number,currentIndex:number) {
  var stringArry=[]
  value.forEach(element => {
    stringArry.push(element.name);
  });
  console.log("stringArry",stringArry)
  console.log("index",index,"currentIndex",currentIndex)
  let subjectArray = <FormArray>this.examSyllabus.controls['subjects'];
  let formGroup = <FormGroup>subjectArray.controls[index];
  let syllabusArray = <FormArray>formGroup.controls['syllabus'];
  let cpArray = <FormArray>formGroup.controls['actualCoursePlanners'];
  let cpformGroup = <FormGroup>cpArray.controls[currentIndex];
  let selectedSyllArray = <FormArray>cpformGroup.controls['selectedsyllabusCoursewise'];
  console.log("value",value)

 for (let i = selectedSyllArray.length - 1; i >= 0; i--) {
  
  selectedSyllArray.removeAt(i);
  }
  if (value != null && value.length > 0) {
   
    for (let i = 0; i < value.length; i++) {
      selectedSyllArray.push(new FormControl(value[i]));
    }
  }

  console.log("subjectArray",subjectArray.value)
}


findCommonElements3(arr1, arr2) { 
  debugger
  return arr1.some(item => arr2.includes(item)) 
} 


onRemarkChange($event){
  if($event.target.checked == true){
    this.rem = true;
  }else{
    this.rem=false;
  }
}
}
