import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrganizationServiceService {

  constructor(private http: HttpClient) { }

  getOrganizationDetails() {
    return this.http.get<any>(environment.apiUrl + '/organization/get/details');
  }
  saveOrganizationDetails(data) {
    return this.http.post<any>(environment.newApiUrl + '/api/organization/details/update', data);
  }
  updateWorkingDays(data) {
    return this.http.put<any>(environment.newApiUrl + '/workingdays/update', data);
  }
}
