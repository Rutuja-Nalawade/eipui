import { Generalinfo } from './generalInfo';


export class User{
    userId:number;
    firstName:string;
    lastName:string;
    loginId:string;
    emailId:string;
    password:string;
    phoneNumber:string;
    status:string;
    portalUsersId:string;
    organizationId:number;
    organizationName: string;
    parentOrgUniqueId : string;
    parentOrgId : number;
    userRoles:{};
    isSetupDone:boolean;
    step:number;
    generalInfo: Generalinfo;
    tokenId: string;
    uniqueId : string;
    orgUniqueId : string;
    profileImageId : string ;
    isParent : boolean;
    orgType: number;
    constructor(){
    }
}
