import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { AssesmentService } from 'src/app/servicefiles/assesment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-edit-exam-data',
  templateUrl: './edit-exam-data.component.html',
  styleUrls: ['./edit-exam-data.component.scss']
})
export class EditExamDataComponent implements OnInit {
  @Input() timingDetails: any;
  @Input() isExamViewFromList: any;
  @Input() Type: any;
  @Input() examUid: any;
  timingForm: FormGroup;
  todayDate = new Date();
  isFirstNext: boolean = false;
  @Output() close = new EventEmitter<any>()
  extraTimeArray = [{ id: 5, name: "5 Min" }, { id: 10, name: "10 Min" }, { id: 15, name: "15 Min" }, { id: 20, name: "20 Min" }, { id: 25, name: "25 Min" }]
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
    private _examService: AssesmentService,
    private toastService: ToastsupportService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    console.log('timingDetails', this.timingDetails)
    this.initTimingDetailsForm();

  }
  initTimingDetailsForm() {
    console.log(this.isExamViewFromList);
    let time = (!this.isExamViewFromList) ? this.timingDetails.assessmentTime : moment(this.timingDetails.assessmentTime).format('hh:mm A');
    this.timingForm = this.fb.group({

      assessmentDate: [this.timingDetails.assessmentDate, Validators.nullValidator],
      assessmentTime: [time, Validators.nullValidator],
      extraTime: [this.timingDetails.extraTime, Validators.nullValidator],
      duration: [this.timingDetails.duration, Validators.nullValidator],

    })
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  updateExamBasicInfo() {
    if (this.isExamViewFromList) {
      let date = new Date();
      let time = this.timingForm.value.assessmentTime.split(" ");
      let timeformat = this.convertTime(this.timingForm.value.assessmentTime);
      console.log(timeformat);
      let splitTimeArry = timeformat.split(":");
      let hr = splitTimeArry[0];
      let min = splitTimeArry[1];
      date.setHours(+hr);
      date.setMinutes(+min);

      let obj = {
        assessmentDate: this.datePipe.transform(this.timingForm.controls['assessmentDate'].value, "yyyy-MM-dd'T'HH:mm:ss.000Z"),
        assessmentTime: this.datePipe.transform(date, "yyyy-MM-dd'T'HH:mm:ss.000Z"),
        extraTime: this.timingForm.controls['extraTime'].value,
        duration: this.timingForm.controls['duration'].value,
      }
      const dto = {
        uniqueId: this.examUid,
        timingDetailsDto: obj
      }

      this.spinner.show();
      this._examService.updateTimingInfo(dto).subscribe(
        (res: any) => {
          console.log('res', res);
          this.close.emit(false);
          // if (res.success== true) {


          // }

          this.spinner.hide();
        },
        (error: HttpErrorResponse) => {
          this.spinner.hide()
          // if (error instanceof HttpErrorResponse) {
          //   console.log("Client-side error occured.");
          //   this.spinner.hide();
          // } else {
          //   this.spinner.hide();
          //   this.toastService.ShowWarning(error);
          // }

        });
    } else {
      this.timingDetails = this.timingForm.value;
      this.close.emit(this.timingForm.value);
    }
  }

  closePopup() {
    this.close.emit(false);
  }
  convertTime(time) {
    return moment(time, 'hh:mm A').format('HH:mm');
  }


}
