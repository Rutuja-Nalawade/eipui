import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExamSetterServiceService {
  api;
  other_api;
  examUid: string;
  selectedExamId: number;
  teacherTrackUid: string;
  teacherTrackId: number;
  subjectName: string;
  questionType: any;
  qbsCourseId: any;
  rightMarks = new Subject();
  spinner = new Subject();

  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();


  constructor( private http: HttpClient) {
    // this.api = this._commonService.api.base_url+"/eaaservices/api/"
    // this.other_api = this._commonService.api.other_base_url

    // this.api='http://eip-dev.paptronics.com/eaaservices/api/'
    // this.other_api='http://eip-dev.paptronics.com'

    this.api='https://eip-dev.byteachers.com/eaaservices/api/'
    this.other_api='https://eip-dev.byteachers.com'
  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  public getExam(examUid: string) {
    return this.http.get<any>(environment.eaasUrl + '/exam/detail/get/' + examUid);
  }

  public addExamTrack(formValue: any) {
    return this.http.post<any>(environment.eaasUrl + '/exam/track/add', formValue);
  }

  public getExamTrack(examUid: string) {
    return this.http.get<any>(environment.eaasUrl + '/exam/track/get/' + examUid);
  }

  lockExamTrack(examUid: string) {
    return this.http.get<any>(environment.eaasUrl + "/exam/track/lock/" + examUid);
  }

  saveExamQuestions(questionData: any) {
    return this.http.post(environment.eaasUrl + "/exam/track/questions/save", questionData);
  }

  getTrackQuestions(trackUid: string) {
    return this.http.get<any>(environment.eaasUrl + "/exam/track/questions/get/" + trackUid);
  }

  deleteExamQuestions(questionData : any) {
    return this.http.post(environment.eaasUrl + "/exam/track/questions/delete", questionData);
  }

  getExamTrackSyllabus(trackUid: String) {
    return this.http.get<any>(environment.eaasUrl + "/exam/track/syllabus/get/" + trackUid);
  }

  // generateSimpleExamPaperSets(examUid: string) {
    // return this.http.get<any>(environment.apiUrl + "/generate/exam/sets/" + examUid);
  // }

  generateExamPaperSets(examUid: string) {
    return this.http.get<any>(environment.eaasUrl + "/generate/exam/sets/" + examUid);
  }

  getPaperSets(examUid: string) {
    return this.http.get<any>(environment.eaasUrl + "/get/exam/sets/" + examUid);
  }
  public saveMarkingScheme(schemeData: any) {
    return this.http.post<any>(environment.eaasUrl + '/save/mcq/scheme', schemeData);
  }
}
