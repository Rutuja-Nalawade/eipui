import { Component, OnInit, ViewChildren, QueryList, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';

// import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { HttpClient, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import * as XLSX from 'xlsx';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { getDropData } from 'ngx-drag-drop/dnd-utils';
import { FileSaverService } from 'ngx-filesaver';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

type AOA = any[][];

@Component({
  selector: 'app-bulkupload',
  templateUrl: './bulkupload.component.html',
  styleUrls: ['./bulkupload.component.scss']
})
export class BulkuploadComponent implements OnInit {

  @Output() closeBulkUploadform = new EventEmitter<string>();
  @ViewChild('myInput', { static: false })
  myInputVariable: ElementRef;
  nodata: boolean = true;

  // SERVER_URL = "http://192.168.0.144:8082/eicoreservices/api/student/file/upload";
  SERVER_URL = environment.apiUrl + 'SERVER_URL';
  uploadForm: FormGroup;


  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  loading: boolean = false;
  show: boolean = false;
  datas: any;
  rejectList: any = [];
  previweData: any;
  organizationList = [];
  submitted = false;
  showData: boolean = false;
  organizationDetails: any;
  errorCount: number = 0;
  saveData: any;
  approve: any;
  fileName: any;
  @ViewChild('TABLE', { static: false }) table: ElementRef;
  constructor(private modalService: NgbModal, private spinner: NgxSpinnerService, private router: Router, private formBuilder: FormBuilder, private toastService: ToastsupportService, private http: HttpClient, private _FileSaverService: FileSaverService, private _userService: UserServiceService

  ) {
  }
  ngOnInit() {
    this.uploadForm = this.formBuilder.group({
      xlsfile: ['', Validators.required],
      // userType: ['',Validators.required],
      // fileName: ['',Validators.required],
    });



    //  this.onSubmit();
  }
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    // this.preview();
  }

  get f() {

    return this.uploadForm.controls;
  }
  showTable() {
    if (this.uploadForm.controls.userType.value != "3") {
      this.showData = false;
    } else {
      this.showData = true;
    }
  }



  onFileSelect(event) {
    this.errorCount = 0;
    if (event.target.files.length > 0) {
      let workBook = null;
      let jsonData = null;
      const reader = new FileReader();
      const file = event.target.files[0];
      this.fileName = file.name;
      // alert(file.type)
      if ((file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || (file.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || (file.type == "application/vnd.ms-excel")) {
        this.uploadForm.get('xlsfile').setValue(file);
        const target: DataTransfer = <DataTransfer>(event.target);
        if (target.files.length !== 1) throw new Error('Cannot use multiple files');
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
          /* read workbook */
          const data = reader.result;
          workBook = XLSX.read(data, { type: 'binary' });
          jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];
            initial[name] = XLSX.utils.sheet_to_json(sheet);
            return initial;
          }, {});
          let alldata = jsonData;
          const exceldata = alldata;
          // console.log('read json data is' , JSON.stringify(exceldata))
          this.validateData(exceldata);
        };
        reader.readAsBinaryString(target.files[0]);
      } else {
        // alert("please choose EXCEL file format");
        this.datas = [];
        this.reset();
        this.toastService.ShowError("please choose EXCEL file format");

      }

    }
    this.datas = [];
  }
  validateData(exceldata: any) {
    let tempList = [];
    console.log('excelData', exceldata)
    let keys = Object.keys(exceldata);
    let key = keys[0];
    exceldata[key].forEach(element => {
      // console.log('firstname' + element.Gender)
      // console.log('isError' +  this.genderValidater(element.Gender))
      let dto = {
        firstName: element.FirstName,
        firstNameError: this.stringValidater(element.FirstName),
        lastName: element.LastName,
        lasttNameError: this.stringValidater(element.LastName),
        mobileNumber: element.Mobile,
        mobileNumberError: this.mobileNumberValidater(element.Mobile),
        emailId: element.EmailId,
        emailIdError: this.ValidateEmail(element.EmailId),
        gender: element.Gender,
        genderError: this.genderValidater(element.Gender),
        edit: false
      }
      tempList.push(dto)
    });
    this.nodata = false;
    this.datas = tempList;
    console.log(' this.data' + JSON.stringify(this.datas))
  }

  stringValidater(stringData) {
    if (/^[a-zA-Z\s]*$/.test(stringData) && stringData != undefined) {
      return false;
    } else {
      this.errorCount = this.errorCount + 1;
      return true;
    }
  }

  ValidateEmail(email) {
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email) && email != undefined) {
      // valid email
      return false;
    } else {
      this.errorCount = this.errorCount + 1;
      return true;
    }


    // if(email != undefined){
    //   const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //   if (!re.test(email)){
    //     this.errorCount= this.errorCount+1
    //   }
    //   return re.test(email);
    // }
    // this.errorCount= this.errorCount+1
    // return (false)

  }
  genderValidater(gender) {
    // console.log('gender' + gender)
    if (gender != undefined) {
      switch (gender.toLowerCase()) {
        case 'm':
          return (false)
          break;
        case 'f':
          return (false)
          break;
        default:
          this.errorCount = this.errorCount + 1
          return (true)

      }
    }
    this.errorCount = this.errorCount + 1
    return (true)
  }
  mobileNumberValidater(mobileNumber) {

    if (mobileNumber != undefined) {
      let number = mobileNumber.replace('+91', '');
      if (!isNaN(+number)) {
        // console.log('number.length ', number.length )
        if (number.length == 10) {
          return (false)
        } else {
          this.errorCount = this.errorCount + 1
          return (true)
        }
      }
      this.errorCount = this.errorCount + 1
      return (true)
    }
    else {
      this.errorCount = this.errorCount + 1
      return (true)
    }
  }

  // openapprovemodal(approve) {


  //   //  this.modalService.open(approve, { windowClass: 'modal-full' });
  //       this.submitted = true;
  //      if (this.uploadForm.invalid) {

  //        return ;
  //      } else {
  //       this.previweData = this.datas;

  //      }

  // }

  onSubmitNew() {
    //  this.loading = true;
    //  this.submitted = true;

    //  const formData = new FormData();
    //  formData.append('importExcelFile', this.uploadForm.get('xlsfile').value);
    //  formData.append('userType', this.uploadForm.controls.userType.value);
    //  formData.append('fileName', this.uploadForm.controls.fileName.value);
    //  formData.append('organizationId', this.uploadForm.controls.organizationUid.value);

    //  let fileName = "bulkupload" + ".xlsx";

    //  this.excelService.downloadReport(true, formData).subscribe(
    //    res => {
    //      this.uploadForm.controls.xlsfile.setValue(null);
    //      this.reset();
    //      this.loading = false;
    //      let emptyFileSize = 3600;
    //      this.toastService.showSuccess("Data Uploaded");
    //      // if (res.body.size > emptyFileSize) {
    //        this._FileSaverService.save(res.body, fileName);
    //      // }
    //    }, (error: HttpErrorResponse) => {
    //      if (error instanceof HttpErrorResponse) {
    //        this.loading = false;
    //        console.log("Client-side error occured." + error);
    //      } else {
    //        this.loading = false;
    //        this.toastService.ShowError(error);
    //      }
    //    }
    //  );
  }



  //  exportAsXLSX(): void {
  //    alert("Organization Name in the excel sheet should be the registered name of your organization.");

  //    let data;
  //    if (this.uploadForm.controls.userType.value == 3)
  //      data = this.studentData;
  //    else
  //      data = this.commonData;
  //    this.excelService.exportAsExcelFile(this.commonData, 'sample');

  //  }

  exportNotValidateData(data): void {
    //  this.excelService.exportAsExcelFile(data, 'sample');
  }

  onNotification() {
    this.toastService.ShowInfo("Please Download Excel Format");
  }


  reset() {
    console.log(this.myInputVariable.nativeElement.files);
    this.myInputVariable.nativeElement.value = "";
    console.log(this.myInputVariable.nativeElement.files);
  }






  navigateToMyOrg() {
    this.router.navigate(['/organization/organization/1/myorganization']);
    setTimeout(function () {
      window.scrollTo(0, document.body.scrollHeight);
    }, 1000);
  }

  saveBulkUploadData() {
    this.spinner.show();
    let dataToSend = [];
    this.errorCount = 0;
    let data = (this.rejectList.length == 0) ? this.datas : this.rejectList;
    for (let i = 0; i < data.length; i++) {
      let dto = {
        firstName: data[i].firstName,
        lastName: data[i].lastName,
        mobileNumber: data[i].mobileNumber,
        emailId: data[i].emailId,
        gender: data[i].gender,
      }
      dataToSend.push(dto);
    }
    let dto = {
      "bulkUploadType": 1,
      "excelFileName": this.fileName.substr(0, this.fileName.lastIndexOf(".")),
      "bulkUploadUsersDetailsDtoList": dataToSend
    }

    this._userService.bulkUploadUser(dto).subscribe(
      (res: any) => {

        if (res.success) {
          this.toastService.showSuccess(res.responseMessage);
          this.rejectList = [];
          this.spinner.hide();
          this.close();

        } else {
          this.toastService.ShowWarning(res.responseMessage);
          let tempData = [];
          let tempData1 = [];
          res.list.forEach(element => {
            tempData.push({
              firstName: element.firstName,
              lastName: element.lastName,
              mobileNumber: element.mobileNumber,
              emailId: element.emailId,
              gender: element.gender,
              firstNameError: false,
              lasttNameError: false,
              mobileNumberError: false,
              mobileExistError: element.mobileNumberError,
              emailIdError: false,
              emailIdExistError: element.emailIdError,
              genderError: false,
              errorList: element.errorsList,
              edit: false
            })
            tempData1.push({
              firstName: element.firstName,
              lastName: element.lastName,
              mobileNumber: element.mobileNumber,
              emailId: element.emailId,
              gender: element.gender
            });
            if (element.emailIdError) {
              this.errorCount = this.errorCount + 1;
            }
            if (element.mobileNumberError) {
              this.errorCount = this.errorCount + 1;
            }
            // this.errorCount = (element.emailIdError || element.mobileNumberError) ? (this.errorCount + 1) : (this.errorCount + 0);
          });
          this.rejectList = tempData;
          this.datas = tempData1;
          console.log("rejected data", JSON.stringify(this.rejectList))
          this.spinner.hide();
        }

      },
      (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
        //  console.log("Client-side error occured.");
        } else {
          this.loading = false;
       //   this.toastService.ShowError(error);
          this.spinner.hide();
        }
      });
    // console.log(JSON.stringify(dto));
  }


  editDomain(domain: any) {
    domain.edit = true;
  }

  save(domain: any) {
    domain.edit = false;
  }

  somethingChanged(fieldId, event, index) {
    this.datas[index].edit = true;
    if (fieldId == 1) {
      this.datas[index].firstNameError = this.stringValidater(event.target.value);
    } else if (fieldId == 2) {
      this.datas[index].lasttNameError = this.stringValidater(event.target.value);
    } else if (fieldId == 3) {
      this.datas[index].mobileNumberError = this.mobileNumberValidater(event.target.value);
    } else if (fieldId == 4) {
      this.datas[index].emailIdError = this.ValidateEmail(event.target.value);
    }
    else if (fieldId == 5) {
      this.datas[index].genderError = this.genderValidater(event.target.value);
    }
    this.updateErrorCount();
  }
  somethingRejectChanged(fieldId, event, index) {
    this.rejectList[index].edit = true;
    if (fieldId == 1) {
      this.rejectList[index].firstNameError = this.stringValidater(event.target.value);
    } else if (fieldId == 2) {
      this.rejectList[index].lasttNameError = this.stringValidater(event.target.value);
    } else if (fieldId == 3) {
      if (this.datas[index].mobileNumber == event.target.value) {
        this.rejectList[index].mobileExistError = true;
      } else {
        this.rejectList[index].mobileExistError = false;
        this.rejectList[index].mobileNumberError = this.mobileNumberValidater(event.target.value);
      }
    } else if (fieldId == 4) {
      console.log(this.datas[index].emailId, event.target.value);
      if (this.datas[index].emailId == event.target.value) {
        this.rejectList[index].emailIdExistError = true;
        console.log("object1");
      } else {
        console.log("object2");
        this.rejectList[index].emailIdExistError = false;
        this.rejectList[index].emailIdError = this.ValidateEmail(event.target.value);
      }
    }
    else if (fieldId == 5) {
      this.rejectList[index].genderError = this.genderValidater(event.target.value);
    }
    this.updateRejectErrorCount();
  }

  close() {
    this.closeBulkUploadform.emit("")
  }

  updateErrorCount() {
    this.errorCount = 0;
    for (let i = 0; i < this.datas.length; i++) {
      if (this.datas[i].firstNameError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.datas[i].lasttNameError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.datas[i].mobileNumberError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.datas[i].emailIdError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.datas[i].genderError == true) {
        this.errorCount = this.errorCount + 1
      }
    }
  }
  updateRejectErrorCount() {
    this.errorCount = 0;
    for (let i = 0; i < this.datas.length; i++) {
      if (this.rejectList[i].firstNameError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.rejectList[i].lasttNameError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.rejectList[i].mobileNumberError == true || this.rejectList[i].mobileExistError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.rejectList[i].emailIdError == true || this.rejectList[i].emailIdExistError == true) {
        this.errorCount = this.errorCount + 1
      }
      if (this.rejectList[i].genderError == true) {
        this.errorCount = this.errorCount + 1
      }
    }
  }

  delete(index: any) {
    this.datas.splice(index, 1);
    this.rejectList.splice(index, 1);
    this.errorCount = this.errorCount - 1
  }
  openFileSelect() {
    (document.getElementById("fileSelect") as HTMLInputElement).value = "";
  }
  downloadRejectData() {
    let exportList = [];
    this.rejectList.forEach(element => {
      exportList.push({
        FirstName: element.firstName,
        LastName: element.lastName,
        EmailId: element.emailId,
        Mobile: element.mobileNumber,
        Gender: element.gender
      })
    });
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(exportList);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Export_' + new Date().getTime() + '.xlsx');
  }
  checkGender() {

  }
}