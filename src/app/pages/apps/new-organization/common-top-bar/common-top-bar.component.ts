import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';
import { OrganizationServiceService } from 'src/app/servicefiles/organization-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';

@Component({
  selector: 'app-common-top-bar',
  templateUrl: './common-top-bar.component.html',
  styleUrls: ['./common-top-bar.component.scss']
})
export class CommonTopBarComponent implements OnInit {

  tabNumber = 1;
  isAlreadyImage = false;
  imgsrc = null;
  currentTime = new Date();
  organizationData: any = {};
  constructor(private toastService: ToastsupportService, private spinner: NgxSpinnerService, private organizationService: OrganizationServiceService, private router: Router, private authService: AuthServiceService) { }

  ngOnInit() {
    this.getOrganizationDetails();
  }
  tabClick(type) {
    this.tabNumber = type;
  }
  openFile() {
    this.imgsrc = "";
    (document.getElementById("imgupload") as HTMLInputElement).value = "";
    document.getElementById("imgupload").click();
  }
  onSelectFile(event) {
    var file = event.target.files[0];
    if (!file) return;
    const max_size = 149715200;
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      this.toastService.ShowWarning('invalid format');
      return;
    }
    if (event.target.files[0].size > max_size) {
      this.toastService.ShowError('Maximum size allowed is ' + (max_size / 1000) / 1000 + 'Mb');
      return false;
    } else {
      const file = (event.target as HTMLInputElement).files[0];
      this._handleReaderLoaded(event);
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    }
  }
  _handleReaderLoaded(e) {
    var reader = e.target;
    this.imgsrc = reader.result;
    this.isAlreadyImage = true;
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['/account/login']);
  }
  getOrganizationDetails() {
    this.spinner.show();
    this.organizationService.getOrganizationDetails().subscribe(
      res => {
        if (res) {
          this.organizationData = res;
        } else {
          this.toastService.ShowWarning("Organization Data not found!")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
}
