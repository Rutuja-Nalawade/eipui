import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { PeriodServiceService } from 'src/app/servicefiles/period-service.service';
import Swal from 'sweetalert2';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-period-list',
  templateUrl: './period-list.component.html',
  styleUrls: ['./period-list.component.scss']
})
export class PeriodListComponent implements OnInit {
  selectedCardPosition: number;
  isPeriodView = false;
  colorClassArray = ['primary', 'success', 'warning', 'info', 'danger'];
  bsInlineValue = new Date();
  userData: any = {};
  slotList = [];
  organizationShifts = [];
  viewIndex: any;
  selectedSlot: any;
  isEditSlot = false;
  subscription: Subscription;
  scheduleList = [{ id: 1, name: "Period One", lecture: 5, lectureText: "5 lecture" }, { id: 2, name: "Period Two", lecture: 4, lectureText: "4 lecture" }, { id: 3, name: "Period Three", lecture: 5, lectureText: "5 lecture" }, { id: 4, name: "Period Four", lecture: 6, lectureText: "6 lecture" }]
  @Output() isViewDetails = new EventEmitter<string>();
  constructor(private _commonService: NewCommonService, private toastService: ToastsupportService, private _periodService: PeriodServiceService, private datePipe: DatePipe, private spinner: NgxSpinnerService) {
    this.userData = this._commonService.getUserData();
    this.subscription = this._commonService.workTimeProcess.subscribe(type => {
      if (type == 2) {
        this.getOrgPeriodList();
      }
    })
  }

  ngOnInit() {
    this.getOrgPeriodList();
  }
  dateChange(event) {

  }


  getOrgPeriodList() {
    this.slotList = [];
    this.spinner.show();
    return this._periodService.getAllPeriod().subscribe(
      res => {
        if (res) {
          console.log("all periods =>", res);
          res.forEach(element => {
            element.periods.forEach(per => {
              let dto = {
                name: per.name,
                startTime: per.startTime,
                endTime: per.endTime,
                uniqueId: per.uniqueId,
                shiftName: element.shift.name,
                shiftUniqueId: element.shift.uniqueId
              }
              this.slotList.push(dto)
            });
          });
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        //   this.spinner.hide();
        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  setSlotListMinAndMaxValue() {
    for (let index = 0; index < this.slotList.length; index++) {

      let shift = this.organizationShifts.find(x => x.shiftUid == this.slotList[index].shiftUniqueId);

      let min = new Date(this.datePipe.transform(shift.startTime, "yyyy-MM-dd'T'HH:mm:ss.000Z"));
      let max = new Date(this.datePipe.transform(shift.endTime, "yyyy-MM-dd'T'HH:mm:ss.000Z"));

      this.slotList[index].min = min;
      this.slotList[index].max = max;
      if ((this.viewIndex && this.viewIndex == this.slotList[index].id) || (this.viewIndex == 0 && this.slotList[index].id)) {
        this.selectedSlot.min = min;
        this.selectedSlot.max = max;
      }
    }
  }
  viewSlot(slot, index) {
    this.selectedCardPosition = index;
    this.isPeriodView = true;
    this.selectedSlot = slot;
    this.viewIndex = slot.id;
    this.isViewDetails.emit("true");
  }
  closeViewSlot() {
    this.isPeriodView = false;
    this.viewIndex = "";
    this.isViewDetails.emit("false");
  }
  editSlot(slot) {
    this.viewIndex = slot.id;
    this.isEditSlot = true;
  }
  closeEditNewForm($event) {
    this.isEditSlot = false;
    // this.getOrgPeriodList();
  }
  deleteSlot(slot) {
    console.log('delete slot =>' + slot.uniqueId)
    Swal.fire({
      position: 'center',
      title: '',
      text: 'Are you sure want to delete Period?',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
    }).then((result) => {
      if (result.value) {
        this.spinner.show();
        return this._periodService.deletePeriod(slot.uniqueId).subscribe(
          res => {
            if (res) {
              this.toastService.ShowWarning(res.responseMessage)
              // console.log("all periods =>", res);
              this.closeViewSlot()
              this.slotList.splice(this.selectedCardPosition, 1)
              this.spinner.hide();
            }
          },
          (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {
            //   console.log("Client-side error occured.");
            //   this.spinner.hide();
            // } else {
            //   this.spinner.hide();
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide();
          });
      }
    })

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
