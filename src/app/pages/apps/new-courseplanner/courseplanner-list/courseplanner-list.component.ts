import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ChartType } from 'chart.js';
import { Color, Label, MultiDataSet } from 'ng2-charts';
import Swal from 'sweetalert2';
import { HttpErrorResponse } from '@angular/common/http';
import { BlockScrollStrategy } from '@angular/cdk/overlay';
import { resolve } from 'url';
import { environment } from 'src/environments/environment';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service'
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { CourseplannerServiceService } from 'src/app/servicefiles/courseplanner-service.service';
import { CanComponentDeactivate } from '../deactivate.gaurd';
declare var $: any;

@Component({
  selector: 'app-courseplanner-list',
  templateUrl: './courseplanner-list.component.html',
  styleUrls: ['./courseplanner-list.component.scss']
})
export class CourseplannerListComponent implements OnInit, CanComponentDeactivate {

  cousreData = [];
  userData: any = {};
  tabNumber = 0;
  coursePlannerList: any = [];
  batchPlannerList: any = [];

  imageList = { 'Physics': '../../../../../assets/Icons/Sample/Physics.png', 'Mathematics': '../../../../../assets/Icons/Sample/Math.png', 'Biology': '../../../../../assets/Icons/Sample/Chemistry.png', 'Chemistry': '../../../../../assets/Icons/Sample/Biology.png' }
  courseHasSubjectId: void;
  courseId: any;
  courseName: any;
  courseUniqueId: any;
  cpToBatchForm: FormGroup;
  masterCoursePlannerForm: FormGroup;
  syllabusPlannerId: String = "";
  subjectIdFromParrent: String = "";
  syllabusNameFromParent: String = "";
  subjectId: any;
  coursePlannerForm: FormGroup;
  batchPlannerForm: FormGroup;
  public totalCount = 100;
  isShowPagination = false;
  pageSizeArray = [{ id: 0, name: "1-10", value: 10 }, { id: 1, name: "1-20", value: 20 }, { id: 2, name: "1-50", value: 50 }, { id: 3, name: "1-100", value: 100 }];
  pageSize = 10;
  selectedPageSize = 0;
  isNextPageValid = true;
  pageIndex = 0
  doughnutChartLabels: Label[] = ['JEE 21 Maths', 'JEE 21 Physics', 'JEE 21 Chemistry', 'JEE 22 Maths'];
  doughnutChartData: MultiDataSet = [
    [33, 30, 17, 20]
  ];
  doughnutChartType: ChartType = 'doughnut';
  doughnutChartColors: Color[] = [
    {
      backgroundColor: ['rgb(111, 239, 237)', 'rgb(250, 127, 112)', 'rgb(246, 193, 106)', 'rgb(155, 125, 231)'],
    },
  ];
  doughntChartOptions = {
    maintainAspectRatio: false,
    cutoutPercentage: 60,
    legend: {
      display: false
    }
  }
  sidebarplanerdropdown = "2020-2021"
  isDropup = true;
  isCpList: boolean = false;
  batchListDataList = [];
  batchList = [];
  masterSelected: any = false;
  selectedBatches: any
  isShowPlanner = true
  itemsPerSlide = 3;
  singleSlideOffset = false;
  isQuickCpForm = false;
  isCreateCpForm = false;
  submitted = false;
  commonStandard = [];
  masterLinkedCourse = [];
  commonGrade = [];
  masterCoursePlannerList = [];
  isNameEntered: boolean = false;
  btnLoading: boolean = false;
  subjectList: any = [];
  masterLinkedCourseData: any;
  selectElementText = ""
  isCoursePlannerDetails = false;
  plannerId: any;
  plannerType: any;
  plannerName: any;
  plannerSubjectName: any;
  batchPlannerId: any = 0;
  totalDuration: any = "";
  saveChangesPending: boolean = false;
  isAssignBatch: boolean = false;
  plannername: any
  plannersubjectName: any
  masterplannerSubjectName: any;
  selectedPlannerUniqueId: number;
  batchUniqueId: any;
  importSubjectId: any;
  constructor(private spinner: NgxSpinnerService, private _commonService: NewCommonService, private fb: FormBuilder, private modalService: NgbModal, private toastService: ToastsupportService, private _subjectService: SubjectServiceService, private _courseServiceService: CourseServiceService, private _cpService: CourseplannerServiceService) {
    this.userData = this._commonService.getUserData();
    this.initCoursePlaner();
  }


  ngOnInit() {
    this.initBatchForm();
    this.initMasterCoursePlanner();
    this.initBatchPlanner();
    this.getCourseList();
    this._cpService.saveChangesPending.subscribe(
      data => {
        this.saveChangesPending = data;
      }
    );
  }
  initBatchForm() {
    this.cpToBatchForm = this.fb.group({
      plannerType: [true],
      courseId: ["", Validators.nullValidator],
      subjectId: ["", Validators.nullValidator],
      coursePlannerId: ["", Validators.nullValidator],
      userCreatedOrUpdatedBy: [this.userData.uniqueId, Validators.required],
      newAssign: [true],
      batches: this.fb.array([])
    })
  }
  initBatchPlanner() {
    this.batchPlannerForm = this.fb.group({
      uniqueId: [''],
      coursePlannerId: [''],
      batchUniqueId: [''],
      syllabus: [],
      subjectId: [],
      courseHasSubjectId: [],
      orgUniqueId: sessionStorage.getItem('orgUniqueId'),
      isNew: ['true'],
      userUniqueId: [],
      units: this.fb.array([]),
      plannerType: [true],
      newAssign: [false],
      subjectName: ["", Validators.nullValidator],
      boardStandardHasCourseId: [],
      batches: this.fb.array([]),
      name: [],
    })
  }
  initCoursePlaner() {
    this.coursePlannerForm = this.fb.group({
      // id: [this.syllabusPlannerId],
      // syllabus: [this.syllabusNameFromParent, Validators.required],
      // orgUniqueId: this.userData.orgUniqueId,
      // orgId: this.userData.organizationId,
      // subjectId: this.subjectIdFromParrent,
      isNew: [true],
      // syllabusId: [this.syllabusPlannerId],
      // userUniqueId: [sessionStorage.getItem('uniqueId'), Validators.required],
      // items: this.fb.array([]),
      uniqueId: [''],
      name: [this.syllabusNameFromParent, Validators.required],
      units: this.fb.array([]),
      // categoryList: this.fb.array([]),
      // plannerType: [false],
      // courseHasSubjectId: [this.subjectId, Validators.required],
      courseSubjectUniqueId: [this.subjectId],
      // boardStandardHasCourseId: [],
      // taggingLevel: [],
      subjectName: ["", Validators.nullValidator]
    })
  }
  initMasterCoursePlanner() {
    this.masterCoursePlannerForm = this.fb.group({
      courseId: [null],
      gradeId: [null],
      subjectId: [null],
      name: [this.syllabusNameFromParent, Validators.required],
      // syllabus: [null, Validators.required],
      syllabusId: [null],
      isNew: [true],
      plannerType: [false],
      masterUniqueId: [],
      units: this.fb.array([]),
      courseSubjectUniqueId: [this.importSubjectId],
      uniqueId: [''],
      subjectName: ["", Validators.nullValidator]
    })
  }
  get f() { return this.coursePlannerForm.controls; }
  get mf() { return this.masterCoursePlannerForm.controls; }
  getCourseList() {
    this.spinner.show();
    return this._courseServiceService.getCourseBasicInfoList((environment.BOTH_COURSES)).subscribe(
      res => {
        console.log("get courseList => ",res)
        if (res.success == true) {
          // console.log("get courseList => ",res)
          this.cousreData = res.list;
          if (res.length != 0) {
            this.courseUniqueId = res.list[0].uniqueId;
          }
          this.tabClick(0, this.cousreData[0]);

        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        //alert(error);  
      });
  }
  tabClick(i, course) {
    this.tabNumber = i;
    // console.log("this.tabNumber", this.tabNumber);
    // console.log("course", course);
    this.coursePlannerList = [];
    this.courseId = course.uniqueId;
    this.courseName = course.name;
    //this.getSubjectPlannerList(course.courseId);
    this.getBatchPlannerList(course.uniqueId);
  }
  getBatchPlannerList(courseId) {
    this.batchPlannerList = [];
    this.spinner.show();
    // let courseId = '5dce54b5-bbaf-4d59-a934-3569c56ac6e8';
    return this._cpService.getCoursePlannerByUid(courseId).subscribe(
      res => {

        if (res) {
          if (res.allBatchPlannersBatchWiseDtos.length > 0) {
            this.batchPlannerList = res.allBatchPlannersBatchWiseDtos;
          } else {
            this.toastService.ShowWarning('No Batch Planner available for this course')
          }
          if (res.coursePlanners.length > 0) {
            this.coursePlannerList = res.coursePlanners;
          } else {
            this.toastService.ShowWarning('no Course planner available for this course')
          }
          this.spinner.hide();
        } else {
          this.spinner.hide();
        }
      },
      error => {
        this.spinner.hide();
      });
  }
  onSelectCourse(uniqueId) {
    this.courseUniqueId = uniqueId;
  }
  createCoursePlanner() {
    this.isQuickCpForm = true
    this.getSubjectByCourse(this.courseUniqueId)
    console.log("courseid=>", this.courseUniqueId);
    //this.getLinkedCourseByCourseUid(this.courseUniqueId);
  }
  getSubjectByCourse(id: any) {

    this._subjectService.getCoursHasSubjectByUid(id).subscribe(subjectsDtoList => {
      console.log("subject list=>", subjectsDtoList.subjectsDtoList)
      this.subjectList = subjectsDtoList.subjectsDtoList;
    })
  }
  getLinkedCourseByCourseUid(id: any) {

    this.courseUniqueId = id;
    console.log("course ID", this.courseUniqueId)

    return this._cpService.getMasterCourseByUid(id).subscribe(
      res => {
        if (res.success == true) {
          console.log("master Course=>", res)
          if (res.list != undefined && res.list.length > 0) {
            this.masterLinkedCourse = res.list;
          }
          // console.log(" this.masterLinkedCourse", this.masterLinkedCourse)
          this.masterCoursePlannerForm.controls['masterUniqueId'].setValue(id);
        }
        else {
          this.toastService.ShowWarning(res.responseMessage);
        }

      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {

        //   this.toastService.ShowWarning(error);
        // }
      });

  }
  changePageSize(item) {
    this.pageSize = item.value;
    this.selectedPageSize = item.id;
  }
  goToPreviousPage() {
    if (this.pageIndex) {
      this.pageIndex = this.pageIndex - 1;
      this.isNextPageValid = true;
      // this.searchParents(true);
    }
  }
  goToNextPage() {
    if (this.isNextPageValid) {
      this.pageIndex = this.pageIndex + 1;
      // this.searchParents(true);
    }
  }
  changeChart(event) {

  }
  deleteCoursePlanner(id: any) {

    Swal.fire({
      position: 'center',
      title: 'Delete',
      text: 'Are you sure want to delete course planner?',
      showCancelButton: true,
      cancelButtonText: 'Cancel',
      confirmButtonText: 'Delete',
    }).then((result) => {
      if (result.value) {
        this.spinner.show()
        this._cpService.deleteCoursePlannerById(id).subscribe(
          response => {
            // this.cpdata = response;
            this.toastService.showSuccess("Course Planner deleted successfully");
            this.getCourseList();
            this.spinner.hide()
          },
          (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {

            // } else {
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide
          });

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })

  }
  // assignCoursePlannerToBatch() {
  //   console.log(this.cpToBatchForm.value);
  //   let batchArray = <FormArray>this.cpToBatchForm.controls['batches'];
  //   let list = batchArray.value;
  //   list = list.filter(batch => batch.isLinked == true);
  //   if (list.length==0) {
  //     this.toastService.ShowWarning("Please select atleast one batch.");
  //     return;
  //   }return;
  //   this.spinner.show()
  //   this._cpService.assignCoursePlannerToBatch(this.cpToBatchForm.value)
  //     .subscribe(data => {
  //       this.toastService.showSuccess("Batch assign successfully.");
  //       this.closeModal(2);
  //       this.spinner.hide()
  //     },
  //       error => {
  //         this.toastService.ShowError(error);
  //         this.spinner.hide()
  //       });
  // }
  showModel(assignBatch: String) {
    // environment.apiUrl + ''#assignBatch').modal('show')
    this.modalService.open(assignBatch, { centered: true, backdrop: 'static' });
  }
  getCpDeatils(courseId, subId, syllausID, coursePlannerId, cpuniqueId) {

    this.cpToBatchForm = this.fb.group({
      plannerType: [true],
      courseId: [this.courseId, Validators.nullValidator],
      subjectId: [subId, Validators.nullValidator],
      coursePlannerId: [cpuniqueId, Validators.nullValidator],
      userCreatedOrUpdatedBy: [this.userData.uniqueId, Validators.required],
      newAssign: [true],
      batches: this.fb.array([])
    })
  }
  assignBatchListByCoursePlannerId(cpUniqueId: number, assignBatch: String, plannername, plannersubjectName) {
    this.plannername = plannername
    this.plannersubjectName = plannersubjectName
    this.batchListDataList = [];
    this.selectedPlannerUniqueId = cpUniqueId
    this.spinner.show();
    return this._cpService.assignBatchList(cpUniqueId).subscribe(
      res => {
        console.log('batch list =>' + JSON.stringify(res))
        if (res) {
          if (res.success == true) {
            this.batchList = res.list;
            this.isAssignBatch = true
            var body = document.body;
            body.classList.add("hideScroll");
          } else {
            this.toastService.ShowInfo('No batches found')
          }
          this.spinner.hide();
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.batchListDataList = [];
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  checkIsnull(batchtList) {
    let result = false;
    if (batchtList.coursePlannerDetails != null) {
      result = true;
    } else {
      result = false;
    }
    return result;
  }
  checkUncheckAll() {
    let batchArray = <FormArray>this.cpToBatchForm.controls['batches'];
    // console.log(batchArray);

    for (var i = 0; i < batchArray.controls.length; i++) {
      let batchgroup = <FormGroup>batchArray.controls[i];
      batchgroup.controls['isLinked'].setValue(this.masterSelected);
      batchgroup.controls['plannerExistForThisSubject'].setValue(this.masterSelected);
      // batchArray.value[i].isLinked = this.masterSelected;
    }
    // console.log(batchArray);
    // this.cpToBatchForm.setControl("batches", batchArray)
    //this.getCheckedItemList();
    // console.log(this.cpToBatchForm);
  }

  isAllSelected() {
    let batchArray = <FormArray>this.cpToBatchForm.controls['batches'];
    this.masterSelected = true;
    for (var i = 0; i < batchArray.value.length; i++) {
      if (!batchArray.value[i].isLinked) {
        this.masterSelected = false;
        break;
      } else {
        batchArray.value[i].plannerExistForThisSubject = true;
      }
    }
  }

  showHidePlanner() {
    this.isShowPlanner = !this.isShowPlanner
  }
  closeModal(type) {
    this.submitted = false;
    switch (type) {
      case 1:
        this.isQuickCpForm = false;
        this.coursePlannerForm.reset();
        document.querySelector('body').classList.remove('hideScroll');
        window.scroll(0, 0);
        this.initCoursePlaner();
        break;
      case 2:
        this.cpToBatchForm.reset();
        this.isAssignBatch = false;
        document.querySelector('body').classList.remove('hideScroll');
        this.modalService.dismissAll();
        break;
      case 3:
        this.masterCoursePlannerForm.reset();
        this.initMasterCoursePlanner();
        this.modalService.dismissAll();
        break;
    }
  }
  nextStep() {
    console.log("course plan=>", this.coursePlannerForm.value);
    this.submitted = true;
    if (this.coursePlannerForm.invalid) {
      // this.scrollToError();
      return;
    }
    this.isQuickCpForm = false;
    this.submitted = false;
    this.isCreateCpForm = true;
    window.scroll(0, 0);
  }
  closeCreateForm(event) {
    this.isCreateCpForm = false;
    this.initCoursePlaner();
    this.getBatchPlannerList(this.courseId);
    window.scroll(0, 0);
  }
  changeSubject(event) {
    console.log('subject change' + JSON.stringify(event));
    // console.log(this.coursePlannerForm);
    this.masterplannerSubjectName = event.name;
    let cpform = <FormGroup>this.coursePlannerForm
    cpform.controls['subjectName'].setValue(event.name);
  }
  importModal(importDataModal: String) {
    // console.log(this.masterCoursePlannerForm.value);
    this.masterCoursePlannerList = [];
    this.commonGrade = [];
    this.masterLinkedCourse = [];
    this.commonStandard = [];
    this.getLinkedCourseByCourseUid(this.courseUniqueId);
    this.modalService.open(importDataModal, { centered: true, size: 'lg', backdrop: 'static' });
  }
  changed(e: any) {
    // console.log(e);
    let masterCpGroup = <FormGroup>this.masterCoursePlannerForm
    // e = JSON.parse(e);
    masterCpGroup.controls['courseId'].setValue(e.uniqueId);
    // console.log(this.masterCoursePlannerForm.value);
    //let orgId = this.userData.organizationId;
    // this.getStandardByCourseUniversalId(e.universal_id, orgId, this.courseUniqueId);
    this.getSubjectByCourseUid(e.uniqueId, this.courseUniqueId);

  }


  getSubjectByCourseUid(id: any, courseUniqueId: any) {

    console.log("getsubject=>courseUniqueId", courseUniqueId);
    console.log("getsubject=>id", id);

    return this._cpService.getCommonSubject(id, courseUniqueId).subscribe(
      res => {
        if (res.success == true) {
          this.commonStandard = res.list;
          console.log("common Subject=>", this.commonStandard)
        }
        else {
          this.toastService.ShowWarning(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });

  }


  changedGrade(e: any) {
    this.masterCoursePlannerList = e.planners
    console.log('grade change' + JSON.stringify(e))

  }

  getGradeAndPlanner(evt: any) {

    console.log("courseUniqueId=>", this.courseUniqueId)
    console.log("evt=>", evt)

    return this._cpService.getGradeAndPlanner(this.courseUniqueId, evt.masterSubjectUniqueId).subscribe(
      res => {
        if (res.success == true) {
          this.commonGrade = res.list;
          console.log("common Grades=>", JSON.stringify(this.commonGrade))
        }
        else {
          this.toastService.ShowWarning(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        // } else {
        //   this.toastService.ShowWarning(error);
        // }
      });

  }




  getGradePlannerByUid(courseUniqueId: any, courseId: any) {
    this.getSubjectByCourseUid(courseUniqueId, courseId)
    //this.masterCoursePlannerForm.controls['subjectId'].setValue(e.subId);

    // let masterCoursePlannerDto = Object.assign({}, masterCoursePlannerData.value);

    // this.spinner.show();
    // // console.log("DDDDDDDDDDDDDD" + JSON.stringify(masterCoursePlannerDto));
    return this._cpService.getGradeAndPlanner(courseUniqueId, courseId).subscribe(
      res => {
        if (res.success == true) {
          this.masterCoursePlannerList = res;
          console.log("masterCoursePlannerList", this.masterCoursePlannerList);
          if (this.masterCoursePlannerList == null) {
            this.toastService.ShowWarning("course planner not available");
          }
          this.spinner.hide();
        }
        else {
          this.toastService.ShowWarning(res.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   // this.dataLoading = false;
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
    // console.log(this.masterCoursePlannerForm.value);
  }

  viewPlannerDetails(id, plannerName, plannerSubject, type, batchPlannerId, duration) {
    this.plannerId = id
    console.log("unique id=>", this.plannerId)
    this.plannerName = plannerName
    this.plannerSubjectName = plannerSubject
    this.isCoursePlannerDetails = true
    this.plannerType = type
    this.batchPlannerId = batchPlannerId;
    this.totalDuration = duration
  }
  CreateCoursePlannerWithImport() {
    console.log('Create planner' + JSON.stringify(this.masterCoursePlannerForm.value))
    this.plannerId = this.masterCoursePlannerForm.get('syllabusId').value
    console.log("unique id=>", this.plannerId)
    this.plannerName = this.masterCoursePlannerForm.get('name').value
    this.importSubjectId = this.masterCoursePlannerForm.get('subjectId').value
    this.plannerSubjectName = this.masterplannerSubjectName
    this.isCoursePlannerDetails = true
    this.plannerType = 3 // 3 for import planner
  }
  PreiewBatchPlanner(batchuniqueId) {
    console.log('preview batch planner' + batchuniqueId)
    this.plannerId = this.selectedPlannerUniqueId
    console.log("unique id=>", this.plannerId)
    this.plannerName = this.plannername
    this.plannerSubjectName = this.plannersubjectName
    this.isCoursePlannerDetails = true
    this.plannerType = 4 // 4 for import planner
    this.batchUniqueId = batchuniqueId
  }
  closePlannerDetails(event) {
    this.isCoursePlannerDetails = false
    this.initCoursePlaner();
    this.getBatchPlannerList(this.courseId);
    this.getBatchPlannerList(this.courseId);
    window.scroll(0, 0);
  }
  dropdownOpen() {
    $(function () {
      //add BT DD show event
      $(".action.dropdown").click(function () {
        var $btnDropDown = $(this).find(".dropdown-toggle");
        var $listHolder = $(this).find(".dropdown-menu");
        $(this).css("position", "static");
        var position = document.getElementById('cplistid').scrollTop;
        $listHolder.css({
          "top": (($btnDropDown.offset().top + $btnDropDown.outerHeight(true)) - 15) + "px",
          "left": ($btnDropDown.offset().left - 330) + "px",
          "margin-top": "-" + (position + 15) + "px"
        });
        $listHolder.data("open", true);
      });
    });
  }
}
