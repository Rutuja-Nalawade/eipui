import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { BoardServiceService } from 'src/app/servicefiles/board-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { GradeServiceService } from 'src/app/servicefiles/grade-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  selectedOrgList = [];
  userData: any;
  studentList = [];
  textSearch: boolean = false;
  loading = false;
  filterData: any = null;
  loadMoreDataLoading = false;
  pageIndex: number = 0;
  dataForCheck = [];
  selectedBoardList = [];
  selectedGradeList = [];
  selectedCourseList = [];
  selectedBatchList = [];
  selectedSubjectList = [];
  selectedYearList = [];
  selectedCalenderList = [];
  gradeList = [];
  boardList = [];
  orgBoardList: any;
  orgGradeList: any;
  academicYearList = [];
  academicCalendarList = [];
  messageTimeLeft: number = 0;
  listClicked = false;
  selectedStudentId: Number;
  isShowQuickForm: Boolean = false;
  isShowBulkAssignForm: Boolean = false;
  isShowBulkUploadForm: Boolean = false;
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  public totalCount = 100;
  isShowPagination = false;
  pageSizeArray = [{ id: 0, name: "1-10", value: 10 }, { id: 1, name: "1-20", value: 20 }, { id: 2, name: "1-50", value: 50 }, { id: 3, name: "1-100", value: 100 }];
  pageSize = 10;
  selectedPageSize = 0;
  totalPages = 0;
  isNextPageValid = true;

  orgModelList = [];
  boardModelList = [];
  academicYearModelList = [];
  academicCalendarModelList = [];
  gradeModelList = [];
  courseModelList = [];
  subjectModelList = [];
  batchModelList = [];
  yearModelList = [];
  filterType = 0
  batchLists = [];
  subjectLists = [];
  subjectList = [];
  courseList = [];
  orgCourseList = [];
  isDropDownOpen = false;
  submitted = false;
  isSelectAll = false;
  selectedStudent: any = {};
  roleData: any = [];
  message = "";
  maxLength = 50;
  stringType = 3;
  searchUserList = [];
  searchableUserList = [];
  selectedUser = [];
  searchText = "";
  @ViewChild('myDrop', { static: true }) myDrop: NgbDropdown;

  constructor(private spinner: NgxSpinnerService, private _commonService: NewCommonService, private _DomSanitizationService: DomSanitizer, private toastService: ToastsupportService, private formBuilder: FormBuilder, private router: Router, private datePipe: DatePipe, private _userService: UserServiceService, private _courseService: CourseServiceService, private _gradeService: GradeServiceService, private _boardService: BoardServiceService) {
    this.userData = this._commonService.getUserData();

    this._commonService.iscancelClicked.subscribe(click => {
      if (click == 1) {
        this.listClicked = false;
        document.querySelector('body').classList.remove('hideScroll');
        this.getStudentRecords([]);
      }
    })
    // this._commonService.isotherClicked.subscribe(click => {
    //   let currentId = localStorage.getItem('selectedStudentId')

    //   // this.rowClick(currentId);
    //   this._commonService.batchFromParent.next(true)
    // })
    this.selectedOrgList.push(parseInt(this.userData.organizationId));
    this.getStudentRecords([]);
    // this.getAcademicYearList();
    this.getBoardList();
    this.getGradeList();
    this.getCourseList();
    this.getRoleInfo();
  }

  ngOnInit() {
    // this.initStudentBasicInfoForm();
    this._commonService.filterData.subscribe(
      (value: any) => {
        this.filterData = value;
      });
  }
  // Get Students Records
  getStudentRecords(value: any) {
    this.isShowPagination = false;
    if (value != null && value != undefined && value.length > 2) {
      this.spinner.show();
      console.log("get search student record1");

      this.textSearch = true;
      let searchDto = {
        'orgList': [this.userData.organizationId],
        'searchKey': value,
        'searchType': 3,
      };

    } else {
      if (value.length == 0 || value == undefined) {
        //  alert();
        this.textSearch = false;
        this.searchStudents(false);
      }
    }
  }
  searchStudents(isLoadMoreData: boolean) {
    // this.setStudListDivCollapsed = false;
    this.spinner.show();

    this.dataForCheck = [];
    if (isLoadMoreData) {
      // this.pageIndex = this.pageIndex + 1;
      this.loadMoreDataLoading = true;
    } else {
      this.pageIndex = 0;
      this.studentList = [];
      this.loading = true;
      this.filterData = (this.filterData != null && this.isDropDownOpen) ? this.filterData : null;
    }
    let searchDto = null;

    if (this.filterData == null) {
      searchDto = {
        "courseIds": this.selectedCourseList,
        "groupCategoriesIds": [],
        "batchGroupIds": [],
        "batchIds": this.selectedBatchList,
        "gradeIds": this.selectedGradeList,
        "boardIds": this.selectedBoardList,
        "subjectIds": [],
        "studentUniqueIds": this.selectedUser,
        "page": this.pageIndex,
        "size": this.pageSize,
        "unassigned": false,
        "states": [1, 2],
        "responseFields": {
          "courseField": true,
          "batchFiled": true,
          "boardField": true,
          "gradeField": true,
          "subjectField": true,
          "groupCategoryField": true,
          "batchGroupFiled": false
        }
      };
    } else {
      this.filterData.page = this.pageIndex;
      this.filterData.size = this.pageSize;
      searchDto = this.filterData;
    }
    this._userService.getUsers(searchDto, "students").subscribe(
      response => {
        if (response.students) {
          let studentList = response.students;
          this.isShowPagination = false;
          this.totalCount = response.totalRecords;
          this.totalPages = response.totalPages;
          this.isNextPageValid = (this.totalPages == (this.pageIndex + 1)) ? false : true;
          if (this.studentList != null && this.studentList.length > 0) {
            if (studentList != null && studentList.length > 0) {
              // this.totalCount=studentList.length;
              this.dataForCheck = studentList;
              this.studentList = studentList;
              this.isShowPagination = true;
            }
          } else {
            this.studentList = studentList;
            if (studentList != null && studentList.length > 0) {
              this.isShowPagination = true;

            }
          }
          for (let i = 0; i < studentList.length; i++) {
            const element = studentList[i];
            this._userService.getImage("student", element.student.uniqueId, false).subscribe(
              response => {
                if (response != null && response) {
                  if (response.image) {
                    this.studentList[i]['imageCode'] = response.image;
                  }
                }
              });
          }
          console.log('recived student list' + JSON.stringify(this.studentList))
        } else {
         // this.toastService.ShowError(response.responseMessage);
          this.isNextPageValid = false;
        }
        // this.selectedUser = [];
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          this.loading = false;
        } else {
          if (isLoadMoreData) {
            this.dataForCheck = [];
            this.loadMoreDataLoading = false;
            this.messageTimeLeft = 4;
            // this.startTimerForInvalidMessage();
          } else {
            this.loading = false;
          }
       //   this.toastService.ShowError(error);
          this.pageIndex = this.pageIndex - 1;
          this.isNextPageValid = false;
        }
        this.spinner.hide();
       // console.log("get student record3");
      });

  }
  findRecords(value: any) {
    this.searchText = value;
    let stringLength = value.length;
    if (stringLength == 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      }
      this.message = "";
      this.maxLength = 50;

      this.searchUsers(value);
    } else if (stringLength > 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) == false) {
        }
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        if (stringLength != 10) {
        }
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      } else {
        let searchedUsers = [];
        if (this.searchableUserList.length > 0) {
          for (let i = 0; i < this.searchableUserList.length; i++) {
            const element = this.searchableUserList[i];
            if (element.fullName.toLowerCase().includes(value.toLowerCase())) {
              searchedUsers.push(element)
            }
          }
          this.searchUserList = searchedUsers;
          this.myDrop.open();
        }
        this.message = "";
        this.maxLength = 50;
      }
    } else if (stringLength == 0) {
      if (this.selectedUser.length > 0) {
        this.selectedUser = [];
        this.searchStudents(false)
      }
      this.selectedUser = [];
      this.message = "";
      this.searchUserList = [];
    }
  }
  checkInputValue(string) {
    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(string)) {//if (/^[a-zA-Z0-9]*$/.test(value) == false) {
      return 1;
    } else if (/^[0-9]*$/.test(string)) {
      return 2;
    } else {
      return 3;
    }
  }
  searchable(ele) {
    if (ele.keyCode == 13) {
      this.stringType = this.checkInputValue(this.searchText);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.searchText) == false) {
          this.toastService.ShowWarning("Email not valid!");
          this.message = "You search as email. Please complete email and enter";
          this.maxLength = 50;
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else if (this.stringType == 2) {
        this.maxLength = 10;
        if (this.searchText.length != 10) {
          this.message = "You search as mobile. Please complete mobile and enter";
          this.toastService.ShowWarning("Mobile not valid!");
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else {

      }
    }
  }
  searchUsers(value) {
    this._userService.searchUsers("student", value)
      .subscribe(data => {
        if (data != null && data.length > 0) {
          // this.totalCount=data.length;
          this.searchableUserList = data;
          this.searchUserList = data;
          this.isShowPagination = true;
          this.myDrop.open();
        } else {
          this.searchUserList = [];
          this.toastService.ShowWarning("User not found!");
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          this.loading = false;
          this.textSearch = false;
        } else {
      //    this.toastService.ShowError(error);
          this.loading = false;
          this.textSearch = false;
        }
        this.spinner.hide();
      });
  }
  clickDropdownItem(item) {
    this.selectedUser.push(item.uniqueId);
    this.searchStudents(false);
    this.myDrop.close();
  }
  rowClick(student) {
    this.selectedStudentId = student.id;
    this.selectedStudent = student.student;
    this.listClicked = true;
    // this.selectedStudentId=studentInfo.id
    this._commonService.isotherClicked.next(1)
    this.isShowQuickForm = false;
    var body = document.body;
    body.classList.add("hideScroll");
    window.scroll(0, 0);
  }


  closeQuickForm() {
    this.isShowQuickForm = false;
    this.isShowBulkAssignForm = false;
    // this.studentBasicInfoForm.reset();
    //this.initStudentBasicInfoForm();
    this.submitted = false;
    document.querySelector('body').classList.remove('hideScroll');
    // window.scroll(0, 0);
  }
  OpenQuickForm() {
    this.isShowQuickForm = true;
    this.isShowBulkAssignForm = false;
  }

  OpenBulkAssignForm() {
    this.isShowBulkAssignForm = true;
    this.isShowQuickForm = false;
    var body = document.body;
    body.classList.add("hideScroll");
    // window.scroll(0, 0);
  }

  OpenBulkUploadForm() {
    this.isShowBulkUploadForm = true;
    this.isShowQuickForm = false;
    var body = document.body;
    body.classList.add("hideScroll");
    // window.scroll(0, 0);
  }

  closebulkassign(event) {
    this.isShowQuickForm = false;
    this.isShowBulkAssignForm = false;
    // this.studentBasicInfoForm.reset();
    //this.initStudentBasicInfoForm();
    this.submitted = false;
    document.querySelector('body').classList.remove('hideScroll');
    // window.scroll(0, 0);
  }

  closeBulkUploadForm() {
    this.isShowQuickForm = false;
    this.isShowBulkAssignForm = false;
    this.isShowBulkUploadForm = false;
    this.submitted = false;
    this.searchStudents(false);
    document.querySelector('body').classList.remove('hideScroll');

  }
  getBoardList() {
    return this._boardService.getAcademicBoardsList().subscribe(
      res => {
        if (res) {
          this.orgBoardList = res;
          if (this.orgBoardList != null && this.orgBoardList.length > 0) {
            let boardList = [];

            boardList = this.orgBoardList;
            this.boardList = boardList
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }

  getGradeList() {
    this.gradeList = [];
    this._gradeService.getAcademicGradeList().subscribe(
      res => {
        if (res.success == true) {
          this.orgGradeList = res.list;
          if (this.orgGradeList != null && this.orgGradeList != null && this.orgGradeList.length > 0) {
            let gradeList = [];

            // for (let i = 0; i < this.orgGradeList.length; i++) {
            //   const element = this.orgGradeList[i];
            //   if (element.boardDto) {
            //     gradeList.push(element.boardDto);
            //   }
            // }
            this.gradeList = res.list;

          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  onItemChange(item, type) {
    let selectedItem = [];
    let id: number;
    switch (type) {
      case 0:
        this.selectedCourseList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedCourseList.push(element.uniqueId);
          }
          this.getBoardListFromCourse();
        } else {
          this.getGradeList();
          this.getBoardList();
        }
        this.searchStudents(false);
        break;
      case 1:
        this.selectedYearList = [];
        this.selectedCalenderList = [];
        this.academicCalendarList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedYearList.push(element.id);
          }
        }
        this.searchStudents(false);
        break;
      case 2:
        this.selectedCalenderList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedCalenderList.push(element.id);
          }
        }
        this.searchStudents(false);
        break;
      case 3:
        this.selectedBoardList = selectedItem;
        this.selectedGradeList = [];
        this.gradeList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedBoardList.push(element.uniqueId);
          }
          this.getGradeListFromBoard();
        } else {
          this.getGradeList();
        }
        this.searchStudents(false);
        break;
      case 4:
        this.selectedGradeList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedGradeList.push(element.uniqueId);
          }
        }
        this.searchStudents(false);
        break;
    }
  }
  getBoardListFromCourse() {
    this.boardList = [];
    this._boardService.getBoardByCourseUid(this.selectedCourseList).subscribe(
      res => {
        if (res) {
          this.orgBoardList = res;
          if (this.orgBoardList != null && this.orgBoardList.length > 0) {
            let boardList = [];
            for (let i = 0; i < this.orgBoardList.length; i++) {
              const element = this.orgBoardList[i];
              if (element.boards && element.boards.length > 0) {
                boardList = element.boards;
              }
            }
            this.boardList = (this.boardList.length == 0) ? boardList : this.boardList.concat(boardList);
          }
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });
  }
  changePageSize(item) {
    this.pageSize = item.value;
    this.selectedPageSize = item.id;
    this.searchStudents(false);
  }
  goToPreviousPage() {
    if (this.pageIndex) {
      this.pageIndex = this.pageIndex - 1;
      this.isNextPageValid = true;
      this.searchStudents(true);
    }
  }
  goToNextPage() {
    if (this.isNextPageValid) {
      this.pageIndex = this.pageIndex + 1;
      this.searchStudents(true);
    }
  }
  cancelFilterModal() {
    this.filterType = 0
    this.isDropDownOpen = false;
    this.myDrop.close();
  }
  apply() {
    this.searchStudents(false);
    this.myDrop.close();
  }
  openDropdown() {
    this.myDrop.close();
    this.filterType = 1;
    this.isDropDownOpen = true;
    this.myDrop.open();
  }
  getList(event) {
    if (event == "1") {
      this.isShowQuickForm = false;
    }
    this.getStudentRecords([])
  }
  getCourseList() {
    this._courseService.getCourseDetailsedInfoList(3).subscribe(
      res => {
        if (res.success == true) {
          // console.log("get courseList => ",res)
          this.orgCourseList = res.list;
          if (this.orgCourseList.length > 0) {
            let courseList = [];
            for (let i = 0; i < this.orgCourseList.length; i++) {
              let dto = {
                "id": this.orgCourseList[i].id, "itemName": this.orgCourseList[i].name, "uniqueId": this.orgCourseList[i].uniqueId
              };
              courseList.push(dto);
            }
            this.courseList = courseList;
          }
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }
  getGradeListFromBoard() {
    this.gradeList = [];

    this._gradeService.getAcademicGradeListByBoard(this.selectedBoardList).subscribe(
      res => {
        if (res.success == true) {
          this.orgGradeList = res.list;
          if (this.orgGradeList != null && this.orgGradeList != null && this.orgGradeList.length > 0) {
            let gradeList = [];
            for (let i = 0; i < res.list[0].courseList.length; i++) {
              const element = res.list[0].courseList[i];

              // gradeList=element.grades
              // this.orgGradeList = element.grades;
              if (element.grades && element.grades.length > 0) {

                gradeList = (gradeList.length == 0) ? element.grades : gradeList.concat(element.grades);
              }
            }
            // this.orgGradeList = gradeList;
            this.gradeList = gradeList;
          }
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
  checkUncheck(event) {
    this.isSelectAll = !this.isSelectAll
  }
  rowCheckboxChange(event) {
    if (!event.target.checked) {
      this.isSelectAll = false;
    }
  }
  getRoleInfo() {
    this._userService.getUserRolesData("student")
      .subscribe(data => {
        if (data) {
          this.roleData = data;
        }
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
}