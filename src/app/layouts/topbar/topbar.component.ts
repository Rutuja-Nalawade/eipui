import { Component, OnInit, Output, EventEmitter, OnDestroy, ViewChild, ElementRef, Renderer, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AccessrightService } from 'src/app/servicefiles/accessrights.service';
import { AuthServiceService } from 'src/app/servicefiles/auth-service.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit, OnChanges {
  @Input() isExpanded: boolean = false;
  marginclass = 'navbar-custom-min';
  topnavmarginleft = 240;
  loading = false;
  show = false;
  topbar: any;
  userName: string;
  bntStyle: string;
  notificationItems: Array<{}>;
  languages: Array<{
    id: number,
    flag?: string,
    name: string
  }>;
  selectedLanguage: {
    id: number,
    flag?: string,
    name: string
  };

  openMobileMenu: boolean;

  isSetupDone: boolean = false;
  organziationType;
  isSetup = true;
  organizationDetails: any;
  @Output() settingsButtonClicked = new EventEmitter();
  // @Output() mobileMenuButtonClicked = new EventEmitter();

  constructor(private accessRightService: AccessrightService, private modalService: NgbModal, private router: Router, private authService: AuthServiceService,) { }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('side nav expanded', this.isExpanded)
    if(this.isExpanded){
      this.marginclass = 'navbar-custom-max';
    }else{
      this.marginclass = 'navbar-custom-min';
    }
  }

  ngOnInit() {
    // get the notifications
    this.isSetupDone = sessionStorage.getItem('isSetupDone') == "false" ? false : true;
    this.userName = sessionStorage.getItem('fullName');
    this._fetchNotifications();
    this.openMobileMenu = false;
    this.organziationType = sessionStorage.getItem('organizationType');
    this.accessRightService.isSetupDone.subscribe(
      (value: boolean) => {
        this.isSetupDone = value;
      }
    );



    this.topbar = [

      {
        id: 1,
        name: 'My Organisation'
      },

    ]
    this.bntStyle = 'globar-search-default';
  }

  submit() {
    this.bntStyle = 'global-search';

  }


  /**
   * Change the language
   * @param language language
   */
  changeLanguage(language) {
    this.selectedLanguage = language;
  }

  /**
   * Toggles the right sidebar
   */
  toggleRightSidebar() {
    this.settingsButtonClicked.emit();
  }

  /**
   * Toggle the menu bar when having mobile screen
   */
  // toggleMobileMenu(event: any) {
  //   event.preventDefault();
  //   this.mobileMenuButtonClicked.emit();
  // }

  /**
   * Logout the user
   */
  logout() {
    this.authService.logout();
    this.router.navigate(['/account/login']);
  }
  /**
 * Logout the user
 */
  leadLogout() {
    this.authService.logout();
    this.router.navigate(['/account/leadsignup']);
  }
  /**
   * Modal Open
   * @param content modal content
   */
  goToAddRole() {
    this.router.navigate(['/role/add']);
  }

  // Method to go to crete user component

  goToUserCreate() {
    this.router.navigate(['/users/add']);
  }

  goToDepartmentCreate() {
    this.router.navigate(['/department/9/add']);
  }

  goToGroupCreate() {
    this.router.navigate(['/group/add']);
  }
  goToRevenueCreate() {
    this.router.navigate(['/students/upload']);
  }

  goToDepartmentList() {
    this.router.navigate(['/department/9/list']);
  }
  onClickUpload() {
    this.router.navigate(['/students/seachStudent/20/upload'])

  }

  /**
   * Fetches the notification
   * Note: For now returns the hard coded notifications
   */
  _fetchNotifications() {
    this.notificationItems = [{
      text: 'Caleb Flakelar commented on Admin',
      subText: '1 min ago',
      icon: 'mdi mdi-comment-account-outline',
      bgColor: 'primary',
      redirectTo: '/notification/1'
    },
    {
      text: 'New user registered.',
      subText: '5 min ago',
      icon: 'mdi mdi-account-plus',
      bgColor: 'info',
      redirectTo: '/notification/2'
    },
    {
      text: 'Cristina Pride',
      subText: 'Hi, How are you? What about our next meeting',
      icon: 'mdi mdi-comment-account-outline',
      bgColor: 'success',
      redirectTo: '/notification/3'
    },
    {
      text: 'Caleb Flakelar commented on Admin',
      subText: '2 days ago',
      icon: 'mdi mdi-comment-account-outline',
      bgColor: 'danger',
      redirectTo: '/notification/4'
    },
    {
      text: 'Caleb Flakelar commented on Admin',
      subText: '1 min ago',
      icon: 'mdi mdi-comment-account-outline',
      bgColor: 'primary',
      redirectTo: '/notification/5'
    },
    {
      text: 'New user registered.',
      subText: '5 min ago',
      icon: 'mdi mdi-account-plus',
      bgColor: 'info',
      redirectTo: '/notification/6'
    },
    {
      text: 'Cristina Pride',
      subText: 'Hi, How are you? What about our next meeting',
      icon: 'mdi mdi-comment-account-outline',
      bgColor: 'success',
      redirectTo: '/notification/7'
    },
    {
      text: 'Caleb Flakelar commented on Admin',
      subText: '2 days ago',
      icon: 'mdi mdi-comment-account-outline',
      bgColor: 'danger',
      redirectTo: '/notification/8'
    }];
  }

  onClickMenu(id) {
    // alert(id)
    if (id == 15) {
      this.router.navigate(['academic/acdemicSetting/15/section']);
    }
    // alert(id)
    if (id == 1) {
      this.router.navigate(['/newModule/organization/myOrganization']);
    }
    if (id == 27) {
      this.router.navigate(['/device/deviceSetting/27/addbiometrics']);
    }
    if (id == 16) {
      this.router.navigate(['/department/department/16']);
    }
    if (id == 13) {
      this.router.navigate(['/approval/approval/13']);
    }
    if (id == 12) {
      this.router.navigate(['/announcements/announcements/12']);
    }

  }

  openSetup() {
    //this.show = !this.show;
  }
}
