import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NewCommonService } from 'src/app/servicefiles/new-common.service';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.scss']
})
export class TeachersListComponent implements OnInit {

  listClicked = false;
  teacherList = [];
  userData: any;
  pageIndex: number = 0;
  filterData: any = null;
  textSearch: boolean = false;
  selectedteacherId: any;
  teacherBasicForm: FormGroup;
  submitted: boolean = false;
  isShowQuickForm: boolean = false;
  genders: any = [];
  subjectList: any = [];
  selectedSubjectList: any = [];
  selectedShiftList = []
  selectedDaysList = []
  selectedOrgList = [];
  shiftsList = [];
  orgList = [];
  isDropDownOpen = false;
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  public totalCount = 100;
  isShowPagination = false;
  pageSizeArray = [{ id: 0, name: "1-10", value: 10 }, { id: 1, name: "1-20", value: 20 }, { id: 2, name: "1-50", value: 50 }, { id: 3, name: "1-100", value: 100 }];
  pageSize = 10;
  selectedPageSize = 0;
  totalPages = 0;
  isNextPageValid = true;
  isSelectAll = false;
  selectedTeacher: any = {};
  roleData: any = [];
  message = "";
  maxLength = 50;
  stringType = 3;
  searchUserList = [];
  searchableUserList = [];
  teacherUniqueIds = [];
  searchText = ""
  @Input() tabNumber: Number;
  @Output() detailsOpen = new EventEmitter<string>();
  @ViewChild('myDrop', { static: true }) myDrop: NgbDropdown;

  constructor(private _commonService: NewCommonService, private spinner: NgxSpinnerService, private _DomSanitizationService: DomSanitizer, private formBuilder: FormBuilder, private toastService: ToastsupportService, private _userService: UserServiceService, private _shiftService: ShiftServiceService, private subjectService: SubjectServiceService) {
    this.userData = this._commonService.getUserData();
    this.selectedOrgList = [parseInt(this.userData.organizationId)]
    this.genders = [{ id: "M", name: "Male" }, { id: "F", name: "Female" }, { id: "O", name: "Other" }]
    // this._commonService.iscloseClicked.subscribe(click => {
    //   if (click) {
    //       this.listClicked = false;
    //   }
    // })
    this._commonService.iscancelClicked.subscribe(click => {
      if (this.tabNumber == 2 && click == 3) {
        this.listClicked = false
        this.findTeacherRecords([])
        this.detailsOpen.emit("close")
        document.querySelector('body').classList.remove('hideScroll');
      }
    })
    this.listClicked = false;
    this.isShowQuickForm = false;

  }

  ngOnInit() {
    // this.listClicked = false;
    // this.findTeacherRecords([])
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['tabNumber'] && changes['tabNumber'].currentValue == 2) {
      this.listClicked = false;
      this.isDropDownOpen = false;
      this.isShowQuickForm = false;
      this.findTeacherRecords([])
      this.getOrgSubjectList();
      // this.getstaffShiftInfo();
      // this.getChildOrganizationList(this.userData.orgUniqueId);
      this.getRoleInfo()
      // this.initTeacherInfoForm()
    }
  }
  findTeacherRecords(value: any) {
    console.log(value);
    if (value != null && value != undefined && value.length > 2) {
      this.spinner.show();
      this.textSearch = true;

      let searchDto = {
        'orgList': [this.userData.organizationId],
        'searchKey': value,
        'searchType': 2,
      };
      // this.loading = true;
      // this._teacherService.searchUsersBySearchKey(searchDto)
      //   .subscribe(data => {
      //     if (data != null && data.length > 0) {
      //       this.teacherList = data;
      //     } else {
      //       this.teacherList = [];
      //     }
      //     this.spinner.hide();
      //     this.textSearch = false;
      //   }, (error: HttpErrorResponse) => {
      //     if (error instanceof HttpErrorResponse) {
      //       this.spinner.hide();
      //       this.textSearch = false;
      //     } else {
      //       this.toastService.ShowError(error);
      //       this.spinner.hide();
      //       this.textSearch = false;
      //     }
      //   });
    } else {
      if (value.length == 0 || value == undefined) {
        //  alert();
        this.textSearch = false;
        this.searchTeachers(false);
      }
    }
  }
  searchTeachers(isLoadMoreData: boolean) {

    let serachDto = null;

    if (isLoadMoreData) {
      // this.pageIndex = this.pageIndex + 1;
    } else {
      this.spinner.show();
      // this.setStaffListDivCollapsed = false;
      this.pageIndex = 0;
      this.teacherList = [];
      this.filterData = (this.filterData != null && this.isDropDownOpen) ? this.filterData : null;
    }

    if (this.filterData == null) {
      serachDto = {
        "subjectIds": this.selectedSubjectList,
        "roleIds": [],
        "teacherUniqueIds": this.teacherUniqueIds,
        "noSubject": false,
        "page": this.pageIndex,
        "size": this.pageSize,
        "states": [
          1,
          2
        ],
        "roleInformation": true,
        "subjectInformation": true
      }
    } else {
      serachDto = this.filterData;
    }
    this._userService.getUsers(serachDto, "teacher").subscribe(
      response => {
        if (response.teachers) {
          let teacherList = response.teachers;
          this.isShowPagination = false;
          this.totalCount = response.totalRecords;
          this.totalPages = response.totalPages;
          this.isNextPageValid = (this.totalPages == (this.pageIndex + 1)) ? false : true;
          if (this.teacherList == null || this.teacherList.length == 0) {
            this.teacherList = teacherList;
            if (teacherList != null && teacherList.length > 0) {
              this.isShowPagination = true;
            }
          } else {
            if (teacherList != null && teacherList.length > 0) {
              this.teacherList = teacherList;
              this.isShowPagination = true;
            }
          }
          for (let i = 0; i < teacherList.length; i++) {
            const element = teacherList[i];
            this._userService.getImage("teacher", element.teacher.uniqueId, false).subscribe(
              response => {
                if (response != null && response) {
                  if (response.image) {
                    this.teacherList[i]['imageCode'] = response.image;
                  }
                }
              });
          }
        } else {
          this.toastService.ShowError(response.responseMessage);
          this.isNextPageValid = false;
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   console.log(error);
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  findRecords(value: any) {
    this.searchText = value;
    let stringLength = value.length;
    if (stringLength == 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      }
      this.message = "";
      this.maxLength = 50;

      this.searchUsers(value);
    } else if (stringLength > 4) {
      this.stringType = this.checkInputValue(value);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) == false) {
        }
        this.message = "You search as email. Please complete email and enter";
        this.maxLength = 50;
        return;
      } else if (this.stringType == 2) {
        if (stringLength != 10) {
        }
        this.message = "You search as mobile. Please complete mobile and enter";
        this.maxLength = 10;
        return;
      } else {
        let searchedUsers = [];
        if (this.searchableUserList.length > 0) {
          for (let i = 0; i < this.searchableUserList.length; i++) {
            const element = this.searchableUserList[i];
            if (element.fullName.toLowerCase().includes(value.toLowerCase())) {
              searchedUsers.push(element)
            }
          }
          this.searchUserList = searchedUsers;
          this.myDrop.open();
        }
        this.message = "";
        this.maxLength = 50;
      }
    } else if (stringLength == 0) {
      if (this.teacherUniqueIds.length > 0) {
        this.teacherUniqueIds = [];
        this.searchTeachers(false)
      }
      this.teacherUniqueIds = [];
      this.message = "";
      this.searchUserList = [];
    }
  }
  checkInputValue(string) {
    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (format.test(string)) {//if (/^[a-zA-Z0-9]*$/.test(value) == false) {
      return 1;
    } else if (/^[0-9]*$/.test(string)) {
      return 2;
    } else {
      return 3;
    }
  }
  searchable(ele) {
    if (ele.keyCode == 13) {
      this.stringType = this.checkInputValue(this.searchText);
      if (this.stringType == 1) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.searchText) == false) {
          this.toastService.ShowWarning("Email not valid!");
          this.message = "You search as email. Please complete email and enter";
          this.maxLength = 50;
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else if (this.stringType == 2) {
        this.maxLength = 10;
        if (this.searchText.length != 10) {
          this.message = "You search as mobile. Please complete mobile and enter";
          this.toastService.ShowWarning("Mobile not valid!");
        } else {
          this.message = "";
          this.searchUsers(this.searchText)
        }
      } else {

      }
    }
  }
  searchUsers(value) {
    this._userService.searchUsers("teacher", value)
      .subscribe(data => {
        if (data != null && data.length > 0) {
          // this.totalCount=data.length;
          this.searchableUserList = data;
          this.searchUserList = data;
          this.isShowPagination = true;
          this.myDrop.open();
        } else {
          this.searchUserList = [];
          this.toastService.ShowWarning("User not found!");
        }
        this.spinner.hide();
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.textSearch = false;
        // } else {
        //   this.toastService.ShowError(error);
        //   this.textSearch = false;
        // }
        this.textSearch = false;
        this.spinner.hide();
      });
  }
  clickDropdownItem(item) {
    this.teacherUniqueIds.push(item.uniqueId);
    this.searchTeachers(false);
    this.myDrop.close();
  }
  rowClick(user) {
    this.selectedteacherId = user.teacher.id;
    this.selectedTeacher = user.teacher;
    this.listClicked = true;
    this.isShowQuickForm = false;
    window.scroll(0, 0);
    this.detailsOpen.emit("open")
    var body = document.body;
    body.classList.add("hideScroll");
    // this.selectedStudentId=studentInfo.id
    // localStorage.setItem('selectedStudentId', currentId);
  }
  scrollTo(el: Element): void {
    if (el) {
      el.scrollIntoView({ behavior: 'smooth', block: 'center' });
    }
  }

  scrollToError(): void {
    const firstElementWithError = document.querySelector('.ng-invalid[formControlName]');
    // console.log("aa:", firstElementWithError);

    this.scrollTo(firstElementWithError);
  }
  closeQuickForm() {
    this.isShowQuickForm = false;
    // this.teacherBasicForm.reset();
    // this.initTeacherInfoForm();
    this.submitted = false;
  }
  OpenQuickForm() {
    this.isShowQuickForm = true
  }
  cancelFilterModal() {
    this.isDropDownOpen = false;
    this.myDrop.close();
  }
  apply() {
    this.searchTeachers(false);
    this.myDrop.close();
  }
  openDropdown() {
    this.isDropDownOpen = true;
  }
  getList(event) {
    if (event == "1") {
      this.isShowQuickForm = false;
    }
    this.findTeacherRecords([])
  }
  onItemChange(item, type) {
    switch (type) {
      case 0:
        this.selectedSubjectList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedSubjectList.push(element.uniqueId);
          }
        }
        this.searchTeachers(false);
        break;
      case 1:
        this.selectedShiftList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedShiftList.push(element.uniqueId);
          }
        }
        this.searchTeachers(false);
        break;
      case 2:
        this.selectedOrgList = [];
        if (item.length > 0) {
          for (let index = 0; index < item.length; index++) {
            const element = item[index];
            this.selectedOrgList.push(element.id);
          }
        } else {
          this.selectedOrgList = [parseInt(this.userData.organizationId)]
        }
        this.searchTeachers(false);
        break;

      default:
        break;
    }
  }
  getOrgSubjectList() {

    this.subjectService.getSubjectList().subscribe(
      res => {
        if (res.success == true) {
          console.log("Subjects");
          let subjectList = res.list;
          if (subjectList != null && subjectList.length > 0) {
            let subjectlist = []
            this.subjectList = subjectList;
          }
        } else {
          this.toastService.ShowWarning('No Subjects found')
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }
  changePageSize(item) {
    this.pageSize = item.value;
    this.selectedPageSize = item.id;
  }
  goToPreviousPage() {
    if (this.pageIndex) {
      this.pageIndex = this.pageIndex - 1;
      this.isNextPageValid = true;
      this.searchTeachers(true);
    }
  }
  goToNextPage() {
    if (this.isNextPageValid) {
      this.pageIndex = this.pageIndex + 1;
      this.searchTeachers(true);
    }
  }
  checkUncheck(event) {
    this.isSelectAll = !this.isSelectAll
  }
  rowCheckboxChange(event) {
    if (!event.target.checked) {
      this.isSelectAll = false;
    }
  }
  getstaffShiftInfo() {
    this.spinner.show();
    let typesArray = [1, 3];
    this._shiftService.getShiftbasicDetails(typesArray, true).subscribe(
      response => {
        let shiftList = response;

        this.shiftsList = shiftList

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {

        this.spinner.hide();
      });
  }

  getChildOrganizationList(parentOrganizationId) {
    // let orgDto = { "uniqueId": this.orgUniqueId, "name": sessionStorage.getItem('organizationName') + " (Own)" };
    // this._teacherService.getOrganizationList(parentOrganizationId).subscribe(
    //   response => {
    //     let organizationList = response;
    //     // this.childOrganizationList.push(orgDto);
    //     if (organizationList != null && organizationList.length > 0) {
    //       let orgList = []
    //       for (let i = 0; i < organizationList.length; i++) {
    //         orgList.push({
    //           "id": organizationList[i].id, "itemName": organizationList[i].name
    //         });
    //       }
    //       this.orgList = orgList;
    //     }
    //   },
    //   (error: HttpErrorResponse) => {
    //     if (error instanceof HttpErrorResponse) {

    //     } else {
    //       this.toastService.ShowError(error);
    //     }
    //   }
    // );
  }
  getRoleInfo() {
    this._userService.getUserRolesData("teacher")
      .subscribe(data => {
        if (data) {
          this.roleData = data;
        }
      }, (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      });

  }
}

