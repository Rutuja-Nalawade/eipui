import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatternviewComponent } from './patternview.component';

describe('PatternviewComponent', () => {
  let component: PatternviewComponent;
  let fixture: ComponentFixture<PatternviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatternviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatternviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
