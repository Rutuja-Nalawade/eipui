import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { TeacherRoutingModule } from './teacher-routing.module';
import { NgbDropdownModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TeachersListComponent } from './teachers-list/teachers-list.component';
import { AssignShiftComponent } from './assign-shift/assign-shift.component';
import { UIModule } from 'src/app/shared/ui/ui.module';
import { CommonComponentModule } from 'src/app/components/common/common-component.module';
import { BsDropdownModule, TabsModule } from 'ngx-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ChartsModule } from 'ng2-charts';
@NgModule({
  declarations: [TeachersListComponent, AssignShiftComponent],
  imports: [
    CommonModule,
    TeacherRoutingModule,
    UIModule,
    NgbTabsetModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    SharedModule,
    NgMultiSelectDropDownModule,
    NgbDropdownModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    CommonComponentModule,
    NgApexchartsModule,
    ChartsModule
  ],
  providers: [DatePipe],
  exports: [TeachersListComponent]
})
export class TeacherModule { }
