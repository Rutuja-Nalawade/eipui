import { Component, OnInit, Input, ViewChild, SimpleChange } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AssesmentService } from '../../../../servicefiles/assesment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { HttpErrorResponse } from '@angular/common/http';
import { NgSelectComponent } from '@ng-select/ng-select';
import { ExamServiceService } from 'src/app/servicefiles/exam-service.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';


@Component({
  selector: 'app-patternview',
  templateUrl: './patternview.component.html',
  styleUrls: ['./patternview.component.scss']
})
export class PatternviewComponent implements OnInit {
  @ViewChild('ngSelectComponent', { static: false }) ngSelectComponent: NgSelectComponent;
  patternType = [{ id: '1', name: 'NEET Pattern' }, { id: '2', name: 'JEE Mains Pattern' }, { id: '3', name: 'JEE Advanced Pattern' }, { id: '4', name: 'Custom Pattern' }]


  customPattern: any;
  examPattern: any;
  pattern: any;

  // patterns: any
  subjectName: String = null;
  duration: any;
  totQuestions: any;
  totMarks: any;
  patterns1: any;
  patterns2: any;
  patternList = [];
  activeType: any = 0;
  pattern1: boolean = true;
  pattern2: boolean = false;

  examPatternList = [];
  examPatternType: any;

  selectedCategory: number;
  createCategoryForm: FormGroup;
  createPatternForm: FormGroup;
  category = [];
  patterns = [];
  masterCourseList: any;
  subjectList = [];
  isPatternSetpOne = false;
  questionTypes = [];
  patternData: any;
  isShowSubjectList = false;
  colorClassArray = ['primary', 'info', 'warning', 'success', 'danger'];
  @Input() assignmentType: any;
  @Input() quickExamForm: FormGroup;
  constructor(private spinner: NgxSpinnerService,
    private examService: ExamServiceService,
    private toastService: ToastsupportService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private _courseService: CourseServiceService,
    private _subjectService: SubjectServiceService,
    private _assesmentService: AssesmentService
  ) {

  }

  ngOnInit() {


  }
  ngOnChanges(changes: SimpleChange) {
    console.log("object changes", changes);
    if (changes['assignmentType'].currentValue == 2) {
      this.initCategoryForm();
      this.initCreatePatternForm();
      this.getExamCategory();
    }
  }
  initCreatePatternForm() {
    this.createPatternForm = this.fb.group({
      name: ['', Validators.nullValidator],
      category: ['', Validators.nullValidator],
      subjects: ['', Validators.nullValidator],
      sctions: ['', Validators.nullValidator],
      totalQuestions: ['', Validators.nullValidator],
      questionType: ['', Validators.nullValidator],
    })
  }
  initCategoryForm() {
    this.createCategoryForm = this.fb.group({
      name: ['', Validators.nullValidator],
      subjects: ['', Validators.nullValidator],
      masterCourse: ['', Validators.nullValidator],
    })
  }
  getExamCategory() {
    this.spinner.show()
    this.category = [];
    return this.examService.getPatternCategory().subscribe(
      res => {
        console.log("pattern category list res", res);
        if (res) {
          if (res.success == true) {
            if (res.list != undefined) {
              this.category = res.list
            } else {
              this.toastService.ShowError('Something error in getting category please try again')
            }
          } else {
            this.toastService.ShowWarning('No Catgories Found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  togleCategory(customType) {
    this.modalService.open(customType, { backdrop: 'static', windowClass: 'addTestType' });
    this.getMasterCourse();
    this.getAllSubjects();
  }

  toglePattern(customType) {
    this.modalService.open(customType, { backdrop: 'static', windowClass: 'addTestType' });
  }
  getAllSubjects() {
    this.spinner.show()
    this.subjectList = [];
    return this._subjectService.getSubjectList().subscribe(
      res => {
        console.log("master subjectList list res", JSON.stringify(res));
        if (res) {
          if (res.success == true) {
            if (res.list != undefined) {
              this.subjectList = res.list
            } else {
              this.toastService.ShowError('Something error in getting subjects please try again')
            }
          } else {
            this.toastService.ShowWarning('No subjects Found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  getMasterCourse() {
    console.log('get master course panel called')
    this.spinner.show()
    this.masterCourseList = [];
    return this._courseService.getMasterCourseList().subscribe(
      res => {
        console.log("master course list res", JSON.stringify(res));
        if (res) {
          this.masterCourseList = res;
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  changeCategory(evt) {
    console.log('evt changeCategory', evt)

    this.spinner.show()
    this.patterns = [];
    return this.examService.getPatternByCategory(evt.uniqueId).subscribe(
      res => {
        console.log("pattern list res", res);
        if (res) {
          if (res.success == true) {
            if (res.list != undefined) {
              this.patterns = res.list
            } else {
              this.toastService.ShowError('Something error in getting patterns please try again')
            }
          } else {
            this.toastService.ShowWarning('No patterns found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  addCategory() {
    console.log('add category called')
    let subjects = this.createCategoryForm.get('subjects').value
    let tempSubjectList = [];
    subjects.forEach(element => {
      tempSubjectList.push({ uniqueId: element })
    });
    let Dto = {
      name: this.createCategoryForm.get('name').value,
      uniqueId: "",
      masterCourse: {
        uniqueId: this.createCategoryForm.get('masterCourse').value
      },
      subjects: tempSubjectList
    }
    return this.examService.addPatternCategory(Dto).subscribe(
      res => {
        if (res) {
          console.log("Pattern Created SuccessFully", JSON.stringify(res));
          if (res.success == true) {
            this.toastService.showSuccess('Pattern Category Created SuccessFully')
            this.category.push({
              uniqueId: res.uniqueId,
              name: res.name
            })
          } else {
            this.toastService.ShowWarning('Error creating pattern category')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  getCourseSubjects(evt) {
    console.log('course changes', JSON.stringify(evt))
    this.spinner.show()
    this.subjectList = [];
    // return this.examService1.getPaginationExamList(this.userData.orgUniqueId, this.pageIndex).subscribe(
    return this._subjectService.getMasterCommonSUbjects(evt.uniqueId).subscribe(
      res => {
        console.log("master subject list res", JSON.stringify(res));
        if (res) {

          if (res.success == true) {
            if (res.list != undefined) {
              this.subjectList = res.list
            } else {
              this.toastService.ShowError('Something error in getting subjects please try again')
            }
          } else {
            this.toastService.ShowWarning('No subjects Found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  startPattern() {
    console.log('pattern form =>', JSON.stringify(this.createPatternForm.value))
    // this.getpatternCategorySubjects(this.createPatternForm.get('category').value)
    this.getQuestionTypes();
    this.isPatternSetpOne = true
  }
  getQuestionTypes() {
    this.questionTypes = []
    return this._assesmentService.getsubjectiveQuestionTypeList().subscribe(
      res => {
        if (res) {
          console.log(" this.questionTypes =>", JSON.stringify(res));
          if (res.success == true) {
            this.questionTypes = res.list
          } else {
            this.toastService.ShowWarning('Question types not found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error.error instanceof Error) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   // this._commonService.ShowWarning(error.error.errorMessage);
        // }
      });
  }
  changePattern(event) {
    this.spinner.show();
    this.examService.getPattern(event.uniqueId).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            this.patternData = res;

            this.patchSubject(res);
            this.isShowSubjectList = true;
          } else {
            this.toastService.ShowWarning('Pattern data not found')
          }
          this.spinner.hide()
        }
      },
      (error: HttpErrorResponse) => {
        this.spinner.hide()
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide()
        //   console.log("Client-side error occured.");
        // } else {
        //   this.spinner.hide()
        //   this.toastService.ShowError(error);
        // }
      });
  }
  patchSubject(data) {
    this.quickExamForm.controls.patternUid.setValue(data.uniqueId);
    let examSyllabus = <FormArray>this.quickExamForm.controls['subjectList'];
    examSyllabus.controls = [];
    if (data != null && data.subjectSectionDtoList.length > 0) {
      let selectedSubjects = data.subjectSectionDtoList;
      for (let i = 0; i < selectedSubjects.length; i++) {
        examSyllabus.push(this.initSubjects(selectedSubjects[i]));
      }
    }
  }
  initSubjects(subject: any) {
    return this.fb.group({
      uniqueId: [subject.subject.uniqueId, Validators.required],
      subjectName: [subject.subject.name, Validators.required],
      actualCoursePlanners: this.fb.array([], Validators.nullValidator),
      remarks: ['', Validators.nullValidator],
      syllabus: [[], Validators.nullValidator],
      examAssignmentPatternSubjectId: ['', Validators.nullValidator],
      actualSyllabus: [[], Validators.nullValidator],
    });
  }
  // getExamPatternList(patternType: any) {

  //   console.log("patternType", patternType)
  //   if (patternType != null && patternType != "") {
  //     this.spinner.show()

  //     this.examPatternList = [];
  //     this.examPatternType = patternType.id;
  //     return this.examService.getExamPatternList(patternType.id, sessionStorage.getItem('organizationId')).subscribe(
  //       res => {
  //         if (res) {
  //           this.examPatternList = res;
  //           this.spinner.hide();

  //         }
  //       },
  //       (error: HttpErrorResponse) => {
  //         if (error.error instanceof Error) {
  //           console.log("Client-side error occured.");
  //         } else {
  //           this.spinner.hide();
  //           this.toastService.ShowWarning(error.error.errorMessage);
  //         }
  //       });
  //   }
  // }

  // getExamPatternDetails(examPatternUid: string) {
  //   console.log("examPatternUid", examPatternUid)
  //   this.spinner.show()
  //   if (examPatternUid != null) {
  //     // let examPattern = this.examPatternList.find(x => x.uniqueId == examPatternUid);
  //     return this.examService.getExamPatternFromEip(examPatternUid, this.examPatternType).subscribe(
  //       res => {
  //         console.log("res", res)
  //         if (res) {
  //           this.examPattern = res;

  //           console.log("this.pattern1", this.pattern1)
  //           console.log("this.pattern2", this.pattern2)

  //           this.duration = this.examPattern.examPatterns[0].duration;
  //           this.totMarks = this.examPattern.examPatterns[0].examMarks;
  //           this.totQuestions = this.examPattern.examPatterns[0].totalQuestion;
  //           this.subjectName = null
  //           if (this.examPattern.patternType == 4) {

  //             this.examPattern.examPatterns.forEach(element => {
  //               element.subjects.forEach(ele => {
  //                 if (this.subjectName != null) {
  //                   this.subjectName = this.subjectName + ',' + ele.name
  //                 } else {
  //                   this.subjectName = ele.name
  //                 }

  //               });
  //             });
  //           }

  //           if (this.examPattern.patternType == 3 || this.examPattern.patternType == 2 || this.examPattern.patternType == 1) {
  //             this.patterns1 = this.examPattern.examPatterns[0]
  //             this.patterns2 = this.examPattern.examPatterns[1]
  //             this.patterns1.subjects.forEach(ele => {
  //               if (this.subjectName != null) {
  //                 this.subjectName = this.subjectName + ',' + ele.name
  //               } else {
  //                 this.subjectName = ele.name
  //               }

  //             });

  //             if (this.examPattern.patternType == 3) {
  //               this.getPatternList();
  //             }
  //           }
  //         }
  //         this.spinner.hide()
  //       },
  //       (error: HttpErrorResponse) => {
  //         if (error.error instanceof Error) {
  //           console.log("Client-side error occured.");

  //         } else {
  //           this.toastService.ShowError(error.error.errorMessage);
  //         }

  //         this.spinner.hide()
  //       }
  //     );
  //   }

  // }

  paperClick(activeid, inactive) {
    this.ngSelectComponent.handleClearClick();
    this.activeType = activeid;
    let ele = document.getElementById(activeid);
    let element = document.getElementById(inactive)
    ele.classList.add('active')
    element.classList.remove('active')

    if (activeid == 0) {
      // this.patterns1=this.examPattern.examPatterns[0]
      this.pattern2 = false
      this.pattern1 = true
      this.subjectName = null
      this.patterns1.subjects.forEach(ele => {
        if (this.subjectName != null) {
          this.subjectName = this.subjectName + ',' + ele.name
        } else {
          this.subjectName = ele.name
        }

      });
      console.log('this.patterns 0', this.patterns)
    } else if (activeid == 1) {
      // this.patterns2=this.examPattern.examPatterns[1]
      this.pattern1 = false
      this.pattern2 = true
      this.subjectName = null
      this.patterns2.subjects.forEach(ele => {
        if (this.subjectName != null) {
          this.subjectName = this.subjectName + ',' + ele.name
        } else {
          this.subjectName = ele.name
        }

      });
      console.log('this.patterns 1', this.patterns)
    }
  }

  // getPatternList() {
  //   this.spinner.show();
  //   this.patternList = [];
  //   return this.examService.getPatternList(sessionStorage.getItem('organizationId')).subscribe(
  //     res => {
  //       if (res) {
  //         this.patternList = res;
  //         this.spinner.hide();
  //       }
  //     },
  //     (error: HttpErrorResponse) => {
  //       if (error.error instanceof Error) {
  //         console.log("Client-side error occured.");
  //       } else {
  //         this.spinner.hide();
  //         this.toastService.ShowError(error.error.errorMessage);
  //       }
  //     });
  // }

  // getPatternInfo(patternUid: any) {
  //   console.log("patternUid", patternUid)
  //   console.log("this.activeType", this.activeType)
  //   if (patternUid != null) {
  //     let pattern = this.patternList.find(x => x.uniqueId == patternUid.uniqueId);
  //     return this.examService.getPatternInfoFromEip(patternUid.uniqueId).subscribe(
  //       res => {
  //         if (res) {

  //           console.log("res", res)
  //           this.patterns = res;
  //           if (this.activeType == 0) {
  //             this.patterns1 = res;
  //           } else {
  //             this.patterns2 = res;
  //           }
  //         }
  //       },
  //       (error: HttpErrorResponse) => {
  //         if (error.error instanceof Error) {
  //           console.log("Client-side error occured.");
  //         } else {
  //           this.toastService.ShowError(error.error.errorMessage);
  //         }
  //       }
  //     );
  //   }
  // }

}
