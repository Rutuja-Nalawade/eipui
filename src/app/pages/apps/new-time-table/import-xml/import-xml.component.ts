import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastsupportService } from 'src/app/servicefiles/toastsupport.service';
import { ShiftServiceService } from 'src/app/servicefiles/shift-service.service';
import * as converter from 'xml-js';
import { TimeTableServiceService } from 'src/app/servicefiles/time-table-service.service';
import { SubjectServiceService } from 'src/app/servicefiles/subject-service.service';
import { CourseServiceService } from 'src/app/servicefiles/course-service.service';
import { BatchesServiceService } from 'src/app/servicefiles/batches-service.service';
import { ClassroomServiceService } from 'src/app/servicefiles/classroom-service.service';
import { UserServiceService } from 'src/app/servicefiles/user-service.service';
@Component({
  selector: 'app-import-xml',
  templateUrl: './import-xml.component.html',
  styleUrls: ['./import-xml.component.scss']
})
export class ImportXmlComponent implements OnInit {
  inputXml: any;
  isFromSetup: boolean = false;
  isEmpty: boolean = true;
  isSubject: boolean = true;
  isSubjectLink: boolean = false;
  isBatch: boolean = false;
  isTeacher: boolean = false;
  isShift: boolean = false;
  isClassroom: boolean = false;
  isBatchGroup: boolean = false;
  isPeriod: boolean = false;
  subjectLinkingSubjectForm: FormGroup;
  masterSelectedSub: boolean;
  masterSelectedperiod: boolean;
  selectedKeyArray: any = [];
  selectedSubject: any = [];
  selectedPeriod: any = [];
  masterSelectedBatch: boolean;
  selectedBatch: any = [];
  masterSelectedTeacher: boolean;
  selectedTeacher: any = [];
  masterSelectedShift: boolean;
  selectedShift: any = [];
  masterSelectedClassroom: boolean;
  selectedClassroom: any = [];
  masterSelectedBatchGroup: boolean;
  selectedBatchGroup: any = [];
  SelectedCount = true;
  isViewShiftDetails: boolean = false;
  testList: any = [];
  itemList: any = [];
  activeTabIndex = 0;
  dataJson: any;
  organizationShifts: any = [];
  organizationSubject: any = [];
  orgCourseList: any = [];
  orgBatchListbyCourse: any = [];
  orgTeachersList: any = [];
  organizationBatchList: any = [];
  orgClassroomList: any = [];
  periodList: any = [];
  shiftDetails: any = {};
  isPreview: boolean = false;
  selectedData: any = "";
  selectedCategoryData: any = "";
  selectedTeacherData: any = "";
  classType = [{ id: 1, name: 'Room' }, { id: 2, name: 'Library' }, { id: 3, name: 'Laboratory' }, { id: 4, name: 'Computer Lab' },
  { id: 5, name: 'Hall' }, { id: 6, name: 'Conference Hall' }, { id: 7, name: 'Staff Room' }]
  classCategory = [{ id: 1, name: 'Physical' }, { id: 2, name: 'Digital' }, { id: 3, name: 'Hybrid' }]
  subjectList = []; //{id:0,shortName:"Shift 1",shiftName:'Shift M1',isLink:false,selected: false},
  subjectSortedList = [];
  xmlData: any;
  batchList = [];//{id:0,xmlSubject:'Physics - 12',shortName:'PHY 12',isLink:true,isMinus:false,isSave:false,isUnLink:false,selected: false},


  teacherList = []; // {id:0,teacherName:'Abhishek Sahoo',shortName:'AS',isLink:true,isMinus:false,isSave:false,isUnLink:false,selected: false},


  shiftList: any = []

  classroomList = []; //{id:0,xmlClassroom:'Classroom 207',shortName:'C207',isLink:true,isMinus:false,isSave:false,isUnLink:false,selected: false}

  bacthGroupList = [];
  cardList = [];
  lessonList = [];
  tabNumber = 1;

  type = 0;
  isAddNew = false;
  isSelectAll: any;
  linkingSubjectList: any = [];
  @Input() timeTableData: any;
  @Output() closeXml = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private shiftService: ShiftServiceService,
    private timetableService: TimeTableServiceService,
    private toastService: ToastsupportService,
    private subjectService: SubjectServiceService,
    private _courseService: CourseServiceService,
    private _batchService: BatchesServiceService,
    private userService: UserServiceService,
    private classroomService: ClassroomServiceService) { }

  ngOnInit() {
    this.initLinkSubjectForm();
    this.getCourseLinkingSubject();
  }

  selectFile(event) {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      let xml = e.target.result;
      this.inputXml = xml;
      let result1 = converter.xml2json(xml, { compact: true, spaces: 2 });
      const JSONData = JSON.parse(result1);
      const timetable = JSONData['timetable'];
      console.log("timetable", timetable);
      this.dataJson = {
        periods: this.getxmlPeriods(timetable['periods']),
        subjects: this.getxmlSubjects(timetable['subjects']),
        teachers: this.getxmlTeachers(timetable['teachers']),
        classrooms: this.getxmlClassrooms(timetable['classrooms']),
        batchgroups: this.getxmlBatchGroups(timetable['groups']),
        lessons: this.getxmlLessons(timetable['lessons']),
        cards: this.getxmlCards(timetable['cards']),
        classes: this.getxmlClasses(timetable['classes']),
      }
      console.log('last final data', this.dataJson);
      this.getOrganizationShifts();
      this.activeTabIndex = 0;
      this.isEmpty = false;
      this.periodList = this.dataJson.periods;
      this.subjectList = this.dataJson.subjects;
      this.teacherList = this.dataJson.teachers;
      this.classroomList = this.dataJson.classrooms;
      this.batchList = this.dataJson.classes;
      this.bacthGroupList = this.dataJson.batchgroups;
      this.cardList = this.dataJson.cards;
      this.lessonList = this.dataJson.lessons;
      this.bacthGroupList.forEach((element, i) => {
        this.batchList.forEach(item => {
          if (item.id == element.classid) {
            this.bacthGroupList[i].xmlBatchName = item.xmlName
            return;
          }
        })
      })
      console.log(this.bacthGroupList);
    }

    reader.readAsText(event.target.files[0])
  }
  getxmlPeriods(periods: any) {
    let list = [];
    periods['period'].forEach(element => {
      const data = element['_attributes'];
      list.push({
        name: data.name,
        period: data.period,
        short: data.short,
        starttime: data.starttime,
        endtime: data.endtime,
        shiftId: '',
        uniqueId: null,
        isLinked: false,
        isValid: true,
        selected: false,
        isSelected: false
      })
    });
    return list;
    // console.log('imported json is ', JSON.stringify(periods))
  }

  getxmlSubjects(subjects: any) {
    let list = [];
    subjects['subject'].forEach(element => {
      const data = element['_attributes']
      list.push({
        id: data.id,
        name: data.name,
        partner_id: data.partner_id,
        short: data.short,
        isLinked: false,
        selected: false,
        isSelected: false,
        xmlId: data.id,
        eipUniqueId: null,
        eipName: '',
      })
    });
    return list;
    // console.log('imported json is ', JSON.stringify(periods))
  }

  getxmlClasses(classes: any) {
    let list = [];
    classes['class'].forEach(element => {
      const data = element['_attributes'];

      list.push({
        classroomids: data.classroomids,
        grade: data.grade,
        id: data.id,
        xmlId: data.id,
        xmlName: data.name,
        partner_id: data.partner_id,
        xmlShortName: data.short,
        teacherid: data.teacherid,
        isLinked: false,
        isValid: true,
        selected: false,
        isSelected: false,
        eipCourseName: "",
        eipCourseUniqueId: "",
        eipName: "",
        eipUniqueId: "",
      })
    });
    return list;
    // console.log('imported json is ', JSON.stringify(periods))
  }

  getxmlTeachers(teachers: any) {
    let list = [];
    teachers['teacher'].forEach(element => {
      const data = element['_attributes'];
      list.push({
        color: data.color,
        emailId: data.email,
        mobile: data.mobile,
        firstName: data.firstname,
        gender: data.gender,
        id: data.id,
        lastName: data.lastname,
        name: data.name,
        partner_id: data.partner_id,
        short: data.short,
        xmlEntityUniqueId: data.id,
        xmlEntityName: data.name,
        xmlEntityShortName: data.short,
        eipEntityUniqueId: '',
        isLinked: false,
        isAddLink: false,
        selected: false,
        xmlId: data.id,
        xmlName: data.name,
        eipName: '',
        eipUniqueId: '',
        isMobileDuplicate: false,
        isEmailDuplicate: false
      })
    });
    console.log('imported json is ', list)
    return list;

  }

  getxmlClassrooms(classrooms: any) {
    let list = [];
    classrooms['classroom'].forEach(element => {
      const data = element['_attributes'];
      list.push({
        id: data.id,
        xmlId: data.id,
        xmlName: data.name,
        partner_id: data.partner_id,
        xmlShortName: data.short,
        eipName: "",
        roomUrl: "",
        eipUniqueId: null,
        isLinked: false,
        selected: false,
        isSelected: false,
        isAdd: false,
        isValid: false,
        category: "",
        type: ""
      })
    });
    return list;
    // console.log('imported json is ', JSON.stringify(periods))
  }

  getxmlBatchGroups(groups: any) {
    let list = [];
    groups['group'].forEach(element => {
      const data = element['_attributes'];
      list.push({
        classid: data.classid,
        divisiontag: data.divisiontag,
        entireclass: data.entireclass,
        xmlId: data.id,
        id: data.id,
        xmlShortName: '',
        xmlBatchName: '',
        name: (data.entireclass == 1) ? 'Entire Class' : data.name,
        xmlName: (data.entireclass == 1) ? 'Entire Class' : data.name,
        studentids: data.studentids,
        courseUniqueId: null,
        batchUniqueId: null,
        categoryUniqueId: null,
        eipUniqueId: null,
        categoryName: '',
        batchName: '',
        courseName: '',
        eipName: '',
        isLinked: false,
        isSelected: false,
        isAdd: false,
        isValid: false,
        isCourseSelect: false,
        isBatchSelect: false,
        batchListByCourse: [],
        batchCategoryListByBatch: [],
        batchgroupListByCategory: []
      })
    });
    return list;
    // console.log('imported json is ', JSON.stringify(periods))
  }

  getxmlLessons(lessons: any) {
    let list = [];
    lessons['lesson'].forEach(element => {
      const data = element['_attributes'];
      list.push({
        classids: data.classids,
        daysdefid: data.daysdefid,
        groupids: data.groupids,
        id: data.id,
        partner_id: data.partner_id,
        periodspercard: data.periodspercard,
        periodsperweek: data.periodsperweek,
        seminargroup: data.seminargroup,
        subjectid: data.subjectid,
        teacherids: data.teacherids,
        termsdefid: data.termsdefid,
        weeksdefid: data.weeksdefid,
      })
    });
    return list;
    // console.log('imported json is ', JSON.stringify(periods))
  }

  getxmlCards(cards: any) {
    let list = [];
    cards['card'].forEach(element => {
      const data = element['_attributes'];
      list.push({
        classroomids: data.classroomids,
        xmlClassroomId: data.classroomids,
        days: data.days,
        lessonid: data.lessonid,
        xmlId: data.lessonid,
        period: data.period,
        terms: data.terms,
        weeks: data.weeks,
      })
    });
    return list;
    // console.log('imported json is ', JSON.stringify(periods))
  }


  close(value) {
    this.closeXml.emit(value);
  }
  closeCourseSubject(value) {
    console.log("value", value);
    if (value == "false") {
      console.log("valid false");
      this.close(false);

    } else {
      this.linkingSubjectList = value;
      this.isAddNew = false;
      this.isChangeTab(3);
    }
  }

  importXml() {
    this.isEmpty = false;
  }

  isChangeTab(tab) {
    let isgotonextTab = false;
    let tabpostion: any;
    if (tab == 0 || tab < this.activeTabIndex) {
      isgotonextTab = true;
    } else {
      let isValid = false;
      for (let i = this.activeTabIndex; i < tab; i++) {
        tabpostion = i;
        isValid = this.checkCurrentValidTab(i);
        if (isValid == false) {
          break;
        }
      }
      isgotonextTab = isValid;
    }
    if (!isgotonextTab) {
      if (tabpostion >= 0) {
        this.activeTabIndex = tabpostion;
        tab = tabpostion;
        this.getTabData(tab);
      }
      return;
    }
    this.getTabData(tab);
  }
  getTabData(tab) {
    this.activeTabIndex = tab;
    switch (tab) {
      case 0:
        this.getOrganizationShifts();
        break;
      case 1:
        this.getLinkingSubject(this.dataJson.subjects);
        this.getAllAcademicSubject();

        break;
      case 2:

        break;
      case 3:
        this.getCourseList();
        this.checkBatchLinking();
        break;
      case 4:
        this.getTeachersList();
        this.checkteacherLinking();
        break;
      case 5:
        this.getTeachersList();
        this.getBatchList();
        this.getAssignShifts();
        break;
      case 6:
        this.getClassroomList();
        this.checkClassroomList();
        break;
      case 7:
        this.getCourseList();
        this.checkBatchgroupLinking();
        break;
      case 8:
        this.isPreview = true;
        break;
    }
  }
  getOrganizationShifts() {
    this.spinner.show();
    return this.shiftService.getAllShifts().subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            this.organizationShifts = res.list;
          } else {
            this.toastService.ShowWarning('shifts not found')
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  getAssignShifts() {
    console.log(this.batchList);
    this.spinner.show();
    return this.timetableService.getAssignedShifts().subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            let shiftList = [];
            res.list.forEach(element => {
              element['isSelect'] = false;
              element['isSelectAll'] = false;
              element['isTeacherSelectAll'] = false;
              element['selected'] = false;
              element['isLinked'] = (element.batches.length > 0 && element.teachers.length > 0) ? true : false;
              element['teacherUniqueIdsList'] = (element.teachers.length > 0) ? this.getUniqueIds(element.teachers) : [];
              element['batchUniqueIdsList'] = (element.batches.length > 0) ? this.getUniqueIds(element.batches) : [];
              shiftList.push(element);
            });
            console.log(shiftList);
            this.shiftList = shiftList;
            if (this.activeTabIndex == 5) {
            }
          } else {
            this.toastService.ShowWarning('shifts not found')
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }
  getUniqueIds(data) {
    let uniqueIds = data.map(x => x.uniqueId);
    return uniqueIds;
  }

  getCourseList() {
    this._courseService.getCourseDetailsedInfoList(3).subscribe(
      res => {
        if (res.success == true) {
          // console.log("get courseList => ",res)
          this.orgCourseList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }

  getBatchbyCourseList(event, data, index) {
    data.eipCourseUniqueId = event.uniqueId;
    data.courseUniqueId = event.uniqueId;
    data.eipCourseName = event.name;
    data.courseName = event.name;
    this._batchService.getBatchByCourse(event.uniqueId).subscribe(
      res => {
        if (res.success == true) {
          this.orgBatchListbyCourse = res.list;
          data.batchListByCourse = this.orgBatchListbyCourse;
          data.batchCategoryListByBatch = [];
          data.batchUniqueId = null;
          data.batchName = '';
          data.categoryUniqueId = null;
          data.categoryName = '';
          data.batchgroupListByCategory = [];
          data.eipUniqueId = null;
          data.eipName = '';
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }


      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }
  getBatchCategory(event, i) {
    this.bacthGroupList[i].batchUniqueId = event.uniqueId;
    this.bacthGroupList[i].batchName = event.name;
    let batchIds = [event.uniqueId]
    this._batchService.getCtegoryGroupByBatchUid(batchIds).subscribe(
      response => {
        let batchGroups = response.batchCategoryWiseGroups[0];
        let categoryList = (batchGroups.batchGroupList) ? batchGroups.batchGroupList : [];
        this.bacthGroupList[i].batchCategoryListByBatch = categoryList;
        this.bacthGroupList[i].categoryUniqueId = null;
        this.bacthGroupList[i].categoryName = '';
        this.bacthGroupList[i].batchgroupListByCategory = [];
        this.bacthGroupList[i].eipUniqueId = null;
        this.bacthGroupList[i].eipName = '';
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }
  changeBatchCategory(event, data) {
    data.categoryUniqueId = event.categoryUniqueId;
    data.categoryName = event.categoryName;
    data.batchgroupListByCategory = event.batchGroups;
    data.eipUniqueId = null;
    data.eipName = '';
  }
  changeBatchGroup(event, data) {
    let sameBatches = this.bacthGroupList.filter(batch => batch.eipUniqueId == event.uniqueId);
    if (sameBatches != null && sameBatches.length > 1) {
      data.eipUniqueId = null;
      data.batchgroupListByCategory = [];
      this.toastService.ShowInfo("This batch group is already selected. Select different one");
      console.log(this.bacthGroupList);
    } else {
      data.eipUniqueId = event.uniqueId;
      data.eipName = event.name;
    }
  }
  getTeachersList() {
    this.spinner.show();
    this.userService.getUserByPageIndexAndType(0, 1, 100).subscribe(
      response => {
        console.log('getTeacherList', response.users)
        console.log('getTeacherList', response.users.length)
        if (response.users && response.users.length > 0) {
          this.orgTeachersList = response.users
        } else {
          this.toastService.ShowWarning("Users not found")
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  getBatchList() {
    this.spinner.show();
    this._batchService.getBatchList().subscribe(
      res => {
        if (res.success == true) {
          let batchLists = res.list;
          if (batchLists != null && batchLists != null && batchLists.length > 0) {
            let batchList = [];
            for (let i = 0; i < batchLists.length; i++) {
              if (batchLists[i].batches && batchLists[i].batches.length > 0) {
                batchList = (batchList.length == 0) ? batchLists[i].batches : batchList.concat(batchLists[i].batches);
                // batchList.push(batchLists[i].batches);
              }
            }
            this.organizationBatchList = batchList;
          }
        }else{
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {
        //   this.spinner.hide();
        // } else {
        //   this.toastService.ShowError(error);
        //   this.spinner.hide();
        // }
        this.spinner.hide();
      });
  }
  // getTeachersList() {
  //   let orgId = [];
  //   let students = []
  //   orgId.push(sessionStorage.getItem('organizationId'))
  //   const obj = {
  //     pageIndex: 0,
  //     orgList: orgId,
  //     subjectList: students
  //   }
  //   this.timetableService.getTeacherList(obj).subscribe(
  //     response => {
  //       this.orgTeachersList = response;
  //       console.log("teachers", response)
  //     },
  //     (error: HttpErrorResponse) => {
  //       if (error instanceof HttpErrorResponse) {

  //       } else {
  //         this.toastService.ShowError(error);
  //       }
  //     }
  //   );
  // }
  getClassroomList() {

    this.classroomService.getAcademicClassRoom().subscribe(
      res => {
        if (res.success == true) {
          this.orgClassroomList = res.list;
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        console.log("teachers", res)
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }
  checkClassroomList() {
    this.spinner.show();
    this.timetableService.getClassroomLink(this.classroomList).subscribe(
      response => {
        if (response.success == true) {
          response.list.forEach((element, i) => {
            this.classroomList[i].category = element.category;
            this.classroomList[i].isLinked = element.isLinked;
            this.classroomList[i].eipName = element.eipName;
            this.classroomList[i].type = element.type;
            this.classroomList[i].roomUrl = (element.roomUrl) ? element.roomUrl : "";
            this.classroomList[i].eipUniqueId = element.eipUniqueId;
          });
          this.spinner.hide();
        } else {
          this.toastService.ShowWarning('Classrooms list not found');
          this.spinner.hide();
        }
        console.log("teachers", response)
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );
  }

  changeShift(event, item, key) {
    console.log(event, item);
    let periodStartDateTime = new Date('1/1/2021 ' + item.starttime);
    let periodEndDateTime = new Date('1/1/2021 ' + item.endtime);
    let shiftStartDateTime = new Date('1/1/2021 ' + event.startTime);
    let shiftEndDateTime = new Date('1/1/2021 ' + event.endTime);
    if (periodStartDateTime >= shiftStartDateTime && periodEndDateTime <= shiftEndDateTime) {
      this.periodList[key]['shiftData'] = event;
      this.periodList[key].isValid = true;
      this.periodList[key].shiftId = event.unique_id;
      // this.spinner.show();
      let dto = [{
        period: item.period,
        startTime: item.starttime,
        endTime: item.endtime,
        shiftId: event.unique_id
      }];
      let keyArray = [key];
      this.checkPeriod(dto, keyArray);
      console.log(dto);

    } else {
      item.isValid = false;
    }
  }
  checkPeriod(dto, keyArray) {
    this.spinner.show();
    this.timetableService.checkPeriods(dto).subscribe(
      res => {
        if (res) {
          if (res.success == true) {
            let list = res.list;
            list.forEach((element, key) => {
              this.periodList.forEach((period, i) => {
                if (period.period == element.period) {
                  if (element.isLinked == true) {
                    this.periodList[i].uniqueId = element.uniqueId;
                    this.periodList[i].isLinked = true;
                  } else {
                    this.periodList[i].isLinked = false;
                    this.periodList[i].uniqueId = null;
                  }
                  return;
                }
              });
            });
            console.log("object", this.periodList);
          } else {
            this.toastService.ShowWarning(res.responseMessage)
          }
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  initLinkSubjectForm() {
    this.subjectLinkingSubjectForm = this.formBuilder.group({
      orgUniqueId: [sessionStorage.getItem('orgUniqueId'), Validators.required],
      orgId: [sessionStorage.getItem('organizationId'), Validators.required],
      // createdOrUpdatedBy: [sessionStorage.getItem('uniqueId'), Validators.required],
      linkedSubjectList: this.formBuilder.array([])
    });
  }

  checkUncheckAll(type) {
    // console.log("")
    if (type == 0) {
      for (var i = 0; i < this.periodList.length; i++) {
        this.periodList[i].selected = this.masterSelectedperiod;
      }
    } else if (type == 1) {
      for (var i = 0; i < this.subjectList.length; i++) {
        this.subjectList[i].selected = this.masterSelectedSub;
      }
    } else if (type == 2) {
      for (var i = 0; i < this.batchList.length; i++) {
        this.batchList[i].selected = this.masterSelectedBatch;
      }
    } else if (type == 3) {
      for (var i = 0; i < this.teacherList.length; i++) {
        this.teacherList[i].selected = this.masterSelectedTeacher;
      }
    } else if (type == 4) {
      for (var i = 0; i < this.shiftList.length; i++) {
        this.shiftList[i].selected = this.masterSelectedShift;
      }
    } else if (type == 5) {
      for (var i = 0; i < this.classroomList.length; i++) {
        this.classroomList[i].selected = this.masterSelectedClassroom;
      }
    } else if (type == 6) {
      for (var i = 0; i < this.bacthGroupList.length; i++) {
        this.bacthGroupList[i].selected = this.masterSelectedBatchGroup;
      }
    }

    this.getCheckedSubjectList(type);
  }


  isAllSelected(type) {
    console.log(type);
    if (type == 0) {
      this.masterSelectedperiod = this.periodList.every(function (item: any) {
        return item.selected == true;
      })
    } else if (type == 1) {
      this.masterSelectedSub = this.subjectList.every(function (item: any) {
        return item.selected == true;
      })
    } else if (type == 2) {
      this.masterSelectedBatch = this.batchList.every(function (item: any) {
        return item.selected == true;
      })
    } else if (type == 3) {
      this.masterSelectedTeacher = this.teacherList.every(function (item: any) {
        return item.selected == true;
      })
    } else if (type == 4) {
      this.masterSelectedShift = this.shiftList.every(function (item: any) {
        return item.selected == true;
      })
    } else if (type == 5) {
      this.masterSelectedClassroom = this.classroomList.every(function (item: any) {
        return item.selected == true;
      })
    } else if (type == 6) {
      this.masterSelectedBatchGroup = this.bacthGroupList.every(function (item: any) {
        return item.selected == true;
      })
    }

    this.getCheckedSubjectList(type);
  }

  getCheckedSubjectList(type) {
    this.selectedPeriod = [];
    this.selectedSubject = [];
    this.selectedBatch = [];
    this.selectedTeacher = [];
    this.selectedShift = [];
    this.selectedClassroom = [];
    this.selectedBatchGroup = []
    let list = [];
    this.selectedKeyArray = [];
    if (type == 0) {
      for (var i = 0; i < this.periodList.length; i++) {
        if (this.periodList[i].selected) {
          list.push(this.periodList[i]);
          this.selectedKeyArray.push(i);
        }
      }
      this.selectedPeriod = list;
      console.log(this.selectedPeriod);
    } else if (type == 1) {
      for (var i = 0; i < this.subjectList.length; i++) {
        if (this.subjectList[i].selected)
          this.selectedSubject.push(this.subjectList[i]);
      }
      this.selectedSubject = this.selectedSubject;
    } else if (type == 2) {
      for (var i = 0; i < this.batchList.length; i++) {
        if (this.batchList[i].selected) {
          this.selectedBatch.push(this.batchList[i]);
          this.selectedKeyArray.push(i);
        }
      }
      this.selectedBatch = this.selectedBatch;
    } else if (type == 3) {
      for (var i = 0; i < this.teacherList.length; i++) {
        if (this.teacherList[i].selected)
          this.selectedTeacher.push(this.teacherList[i]);
      }
      this.selectedTeacher = this.selectedTeacher;
    } else if (type == 4) {
      for (var i = 0; i < this.shiftList.length; i++) {
        if (this.shiftList[i].selected) {
          this.selectedShift.push(this.shiftList[i]);
          this.selectedKeyArray.push(i);
        }
      }
      this.selectedShift = this.selectedShift;
    } else if (type == 5) {
      for (var i = 0; i < this.classroomList.length; i++) {
        if (this.classroomList[i].selected) {
          this.selectedClassroom.push(this.classroomList[i]);
          this.selectedKeyArray.push(i);
        }
      }
      this.selectedClassroom = this.selectedClassroom;
    } else if (type == 6) {
      for (var i = 0; i < this.bacthGroupList.length; i++) {
        if (this.bacthGroupList[i].selected) {
          this.selectedBatchGroup.push(this.bacthGroupList[i]);
          this.selectedKeyArray.push(i);
        }
      }
      this.selectedBatchGroup = this.selectedBatchGroup;
    }

    // console.log("this.selectedSubject", this.selectedSubject)
  }

  closeFormView(event) {
    this.isAddNew = false;
    this.type = 0;
    this.isChangeTab(this.activeTabIndex);
  }
  addNew(type) {
    switch (type) {
      case 0:
        this.type = 12;
        this.isAddNew = true;
        break;
      case 1:
        this.type = 7;
        this.isAddNew = true;
        break;
      case 2:
        this.type = 6;
        this.isAddNew = true;
        break;
      case 3:
        this.type = 11;
        this.isAddNew = true;
        break;
      case 4:
        this.type = 8;
        this.isAddNew = true;
        break;
      case 5:
        this.type = 10;
        this.isAddNew = true;
        break;
      case 6:
        this.type = 9;
        this.isAddNew = true;
        break;
    }
  }
  changeSlected(event) {
    console.log(event, this.selectedData)
    this.selectedData = event;
  }
  changeTeacher(event) {
    console.log(event, this.selectedData)
    let dto = [];
    event.forEach(element => {
      dto.push({
        name: element.fullName,
        uniqueId: element.uniqueId
      });

    });
    this.selectedTeacherData = dto;
  }
  isSelectedSave(type) {
    console.log(this.selectedData)
    switch (type) {
      case 0:
        if (this.selectedData) {
          let periodDto = [];
          let keyArray = [];
          let shiftStartDateTime = new Date('1/1/2021 ' + this.selectedData.startTime);
          let shiftEndDateTime = new Date('1/1/2021 ' + this.selectedData.endTime);
          this.selectedPeriod.forEach((item, key) => {
            let periodStartDateTime = new Date('1/1/2021 ' + item.starttime);
            let periodEndDateTime = new Date('1/1/2021 ' + item.endtime);
            if (periodStartDateTime >= shiftStartDateTime && periodEndDateTime <= shiftEndDateTime) {
              item['shiftData'] = this.selectedData;
              item.isValid = true;
              item.shiftId = this.selectedData.unique_id;
              let dto = {
                period: item.period,
                startTime: item.starttime,
                endTime: item.endtime,
                shiftId: this.selectedData.unique_id
              };
              periodDto.push(dto);
              keyArray.push(key);
              this.periodList[this.selectedKeyArray[key]].isValid = true;
            } else {
              this.periodList[this.selectedKeyArray[key]].isValid = false;
            }
            this.periodList[this.selectedKeyArray[key]].shiftId = this.selectedData.unique_id;
            this.periodList[this.selectedKeyArray[key]]['shiftData'] = this.selectedData;
          });
          if (periodDto.length > 0) {
            this.checkPeriod(periodDto, keyArray);
            this.isSelectedCancel(0);
          }
        } else {
          this.selectedData = "";
          this.toastService.ShowWarning("Please Select course then save.");
        }
        break;
      case 1:
        if (this.selectedData) {
          this.subjectList.forEach(element => {
            if (element.selected) {
              element.eipUniqueId = this.selectedData.uniqueId
            }
          });
          this.isSelectedCancel(1);
        } else {
          this.selectedData = "";
          this.toastService.ShowWarning("Please Select subject then save.")
        }
        break;
      case 3:
        if (this.selectedData) {
          this._batchService.getBatchByCourse(this.selectedData.uniqueId).subscribe(
            res => {
              if (res.success == true) {
                this.orgBatchListbyCourse = res.list;
                this.selectedKeyArray.forEach(element => {
                  if (!this.batchList[element].isLinked) {
                    this.batchList[element].batchListByCourse = this.orgBatchListbyCourse;
                    this.batchList[element].eipCourseUniqueId = this.selectedData.uniqueId;
                    this.batchList[element].eipCourseName = this.selectedData.name;
                  }
                });
                this.isSelectedCancel(2);
              } else {
                this.toastService.ShowWarning(res.responseMessage)
              }

            },
            (error: HttpErrorResponse) => {
              // if (error instanceof HttpErrorResponse) {

              // } else {
              //   this.toastService.ShowError(error);
              // }
            }
          );
        } else {
          this.selectedData = "";
          this.toastService.ShowWarning("Please Select shft then save.")
        }
        break;
      case 4:
        if (this.selectedData) {
          this.teacherList.forEach(element => {
            element.eipEntityUniqueId = this.selectedData.uniqueId;
            element.eipName = this.selectedData.fullName;
          });
          this.isSelectedCancel(4);
        } else {
          this.selectedData = "";
          this.toastService.ShowWarning("Please Select subject then save.")
        }
        break;
      case 5:
        if (this.selectedData.length > 0 && this.selectedTeacherData.length > 0) {
          this.selectedKeyArray.forEach(element => {
            this.shiftList[element].batches = this.selectedData;
            this.shiftList[element].batchUniqueIdsList = this.getUniqueIds(this.selectedData);
            this.shiftList[element].teacherUniqueIdsList = this.getUniqueIds(this.selectedTeacherData);
            this.shiftList[element].teachers = this.selectedTeacherData;
            this.shiftList[element].isLinked = true;
          });
          this.isSelectedCancel(5);
        } else {
          this.selectedData = "";
          this.toastService.ShowWarning("Please Select shft then save.")
        }
        break;
      case 6:
        if (this.selectedData.length > 0 && this.selectedCategoryData.length > 0) {
          this.selectedKeyArray.forEach(element => {
            this.classroomList[element].type = this.selectedData.id;
            this.classroomList[element].category = this.selectedCategoryData.id;
          });
          this.isSelectedCancel(6);
        } else {
          this.selectedData = "";
          this.toastService.ShowWarning("Please Select shft then save.")
        }
        break;
      case 7:
        console.log(this.selectedData);
        if (this.selectedData) {
          this.getBatchesByCourse(this.selectedData.uniqueId, this.selectedKeyArray);
          this.selectedKeyArray.forEach(element => {
            this.bacthGroupList[element].courseUniqueId = this.selectedData.uniqueId;
            this.bacthGroupList[element].courseName = this.selectedData.name;
            this.bacthGroupList[element].isValid = false;
            this.bacthGroupList[element].isLinked = false;
          });
          this.isSelectedCancel(7);
        } else {
          this.selectedData = "";
          this.toastService.ShowWarning("Please Select course then save.")
        }
        break;
    }
    // this.selectedPeriod = [];
    // this.selectedSubject = [];
    // this.selectedBatch = [];
    // this.selectedTeacher = [];
    // this.selectedShift = [];
    // this.selectedClassroom = [];
    // this.selectedBatchGroup = [];
    // this.selectedShift = [];
    // this.selectedKeyArray = [];
  }
  isSelectedCancel(type) {
    this.selectedPeriod = [];
    this.selectedSubject = [];
    this.selectedShift = [];
    this.selectedBatch = [];
    this.selectedTeacher = [];
    this.selectedClassroom = [];
    this.selectedBatchGroup = [];
    this.selectedShift = [];
    this.selectedKeyArray = [];
    switch (type) {
      case 0:
        this.selectedData = "";
        this.periodList.forEach(element => {
          element.selected = false;
        });
        console.log(this.periodList);
        this.masterSelectedperiod = false;
        break;
      case 1:
        this.selectedData = "";
        this.subjectList.forEach(element => {
          element.selected = false;
        });
        this.masterSelectedSub = false;
        break;
      case 3:
        this.selectedData = "";
        this.batchList.forEach(element => {
          element.selected = false;
        });
        console.log(this.batchList);
        break;
      case 4:
        this.selectedData = "";
        this.teacherList.forEach(element => {
          element.selected = false;
        });
        this.masterSelectedTeacher = false;
        break;
      case 5:
        this.selectedData = "";
        this.shiftList.forEach(element => {
          element.selected = false;
        });
        break;
      case 6:
        this.selectedData = "";
        this.classroomList.forEach(element => {
          element.selected = false;
        });
        break;
      case 7:
        this.selectedData = "";
        this.bacthGroupList.forEach(element => {
          element.selected = false;
        });
        break;
    }
  }

  viewShiftDetails(data) {
    this.shiftDetails = data;
    this.isViewShiftDetails = true;
  }

  closeConflict(event) {
    this.isViewShiftDetails = false;
  }
  saveTab(type) {
    switch (type) {
      case 0:
        console.log(this.periodList);
        let periods = this.periodList.filter(x => (x.isLinked == false && x.shiftId != null && x.shiftId != '' && x.isValid == true));
        if (periods.length > 0) {
          let periodDto = [];
          periods.forEach(element => {
            let dto = {
              xmlPeriod: element.period,
              startTime: element.starttime,
              endTime: element.endtime,
              shiftId: element.shiftId,
              name: element.name,
              // uniqueId: element.uniqueId,
            };
            periodDto.push(dto);
          });
          console.log(periodDto);
          this.spinner.show();
          this.timetableService.linkPeriods(periodDto).subscribe(
            res => {
              if (res) {
                if (res.success == true) {
                  // this.activeTabIndex = 1;
                  this.periodList.forEach((period, i) => {
                    res.list.forEach((element, key) => {
                      if (period.period == element.period) {
                        this.periodList[i].isLinked = true;
                        this.periodList[i].uniqueId = element.uniqueId;
                      }
                    });
                  });
                  this.toastService.showSuccess("Period linked to shift");
                  console.log("periodList", this.periodList);
                  this.isChangeTab(1);
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // console.error('An error occurred:', JSON.stringify(error));
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        } else {
          let validPeriod = this.periodList.filter(x => (x.isLinked == true));
          if (validPeriod.length == this.periodList.length) {
            // this.activeTabIndex = 1;
            this.isChangeTab(1);
          } else {
            this.toastService.ShowWarning("Assign shift to each period");
          }
        }

        this.isChangeTab(1);
        break;
      case 1:
        console.log("this.subjectList", this.subjectList)
        let subjectsList = this.subjectList.filter(x => (x.isLinked == false && !x.eipUniqueId));
        console.log("subjects", subjectsList)
        if (subjectsList.length == 0) {
          let subjects = this.subjectList.filter(x => (x.isLinked == false && x.eipUniqueId));
          if (subjects.length > 0) {
            var url: any;
            url = '/import-subject/link';
            let subjectDto = [];
            subjects.forEach(element => {
              let dto = {

                xmlId: element.id,
                isLinked: true,
                eipUniqueId: element.eipUniqueId,
                xmlName: element.name,
                xmlShortName: element.short,
                eipName: element.eipName
              };
              subjectDto.push(dto);
            });

            this.timetableService.linkUnlinkSubjects(subjectDto, url).subscribe(
              response => {
                if (response.success == true) {
                  console.log("res", response);

                  this.subjectList.forEach(element => {
                    element.isLinked = true;
                  });
                  this.isChangeTab(2);
                  this.toastService.showSuccess(response.responseMessage)
                }
              },
              (error: HttpErrorResponse) => {
                // if (error instanceof HttpErrorResponse) {

                // } else {
                //   this.toastService.ShowError(error);
                // }
              }
            );
          } else {
            // this.activeTabIndex = 4;
            this.isChangeTab(2);
          }
        } else {
          this.toastService.ShowWarning("Assign eip subjects to each subject");

        }

        break;
      case 3:
        console.log(this.batchList);
        let batchList = this.batchList.filter(x => (x.isLinked == false && !x.eipCourseUniqueId && !x.eipUniqueId));
        console.log(batchList);
        if (batchList.length == 0) {
          let batchDto = this.batchList.filter(x => (x.isLinked == false && x.eipCourseUniqueId && x.eipUniqueId));
          console.log(batchDto);
          if (batchDto.length > 0) {
            this.spinner.show();
            this.timetableService.linkBatch(batchDto).subscribe(
              res => {
                if (res && res.success == true) {
                  // this.activeTabIndex = 4;
                  this.isChangeTab(4);
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                // console.error('An error occurred:', JSON.stringify(error));
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.toastService.ShowError(error);
                // }
                this.spinner.hide();
              });
          } else {
            // this.activeTabIndex = 4;
            this.isChangeTab(4);
          }
        } else {
          this.toastService.ShowWarning("Assign eip batch to each batch");
          console.log("batchList");
        }
        break;

      case 4:
        let teachers = this.teacherList.filter(x => x.isLinked == false && !x.eipEntityUniqueId);
        if (teachers.length == 0) {
          let teacherDto = this.teacherList.filter(x => (x.isLinked == false && x.eipEntityUniqueId));
          if (teacherDto.length > 0) {
            this.spinner.show();
            this.timetableService.linkTeacher(teacherDto).subscribe(
              res => {
                if (res && res.success == true) {
                  this.isChangeTab(5);
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                // console.error('An error occurred:', JSON.stringify(error));
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.toastService.ShowError(error);
                // }
                this.spinner.hide();
              });
          } else {
            // this.activeTabIndex = 4;
            this.isChangeTab(5);
          }
        } else {
          this.toastService.ShowWarning("Assign eip teachers to each teacher");
          console.log("batchList");
        }

        break;
      case 5:
        let shiftList = this.shiftList.filter(x => x.isLinked == false);
        if (shiftList.length == 0) {
          this.spinner.show();
          let listDto = {
            list: this.shiftList
          }
          this.timetableService.assignShiftToBatch(listDto).subscribe(
            res => {
              if (res && res.success == true) {
                // this.activeTabIndex = 6;
                this.isChangeTab(6);
              } else {
                this.toastService.ShowWarning(res.responseMessage)
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // console.error('An error occurred:', JSON.stringify(error));
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        } else {
          this.toastService.ShowWarning("Assign batch and teacher to each shift");
        }
        break;
      case 6:
        let classRoomList = this.classroomList.filter(x => x.isLinked == false);
        if (classRoomList.length == 0) {
          // this.activeTabIndex = 7;
          this.isChangeTab(7);
        } else {
          let classRoomDto = classRoomList.filter(x => x.isValid == true);
          if (classRoomDto.length > 0 && classRoomList.length == classRoomDto.length) {
            this.spinner.show();
            this.timetableService.linkClassroom(this.classroomList).subscribe(
              res => {
                if (res && res.success == true) {
                  // this.activeTabIndex = 7;
                  this.isChangeTab(7);
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.toastService.ShowError(error);
                // }
                this.spinner.hide();
              });
          } else {
            this.toastService.ShowWarning("Assign classroom to each");
          }
        }
        break;
      case 7:
        let batchGroup = this.bacthGroupList.filter(x => x.isLinked == false);
        if (batchGroup.length == 0) {
          this.isPreview = true;
        } else {
          let batchgroupDto = batchGroup.filter(x => x.isValid == false);
          if (batchgroupDto.length > 0 && batchGroup.length == batchgroupDto.length) {
            this.spinner.show();
            this.timetableService.linkBatchGroup(this.bacthGroupList).subscribe(
              res => {
                if (res && res.success == true) {
                  // this.activeTabIndex = 7;
                  this.isPreview = true;
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.toastService.ShowError(error);
                // }
                this.spinner.hide();
              });
          } else {
            this.toastService.ShowWarning("Assign batchGroup to each");
          }
        }
        break;
    }
  }
  checkBatchLinking() {
    this.spinner.show();
    this.timetableService.getBatchLink(this.batchList).subscribe(
      res => {
        if (res && res.success == true) {
          let list = res.list;
          list.forEach((element, i) => {
            if (element.isLinked) {
              this.batchList[i].eipCourseName = element.eipCourseName;
              this.batchList[i].eipUniqueId = element.eipUniqueId;
              this.batchList[i].eipCourseUniqueId = element.eipCourseUniqueId;
              this.batchList[i].eipName = element.eipName;
              this.batchList[i].isLinked = true;
            } else {
              this.batchList[i].isLinked = false;
            }
          });
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });

  }
  //Subject

  getAllAcademicSubject() {
    this.spinner.show();
    return this.subjectService.getSubjectList().subscribe(
      res => {
        console.log("Client-side error occured.", res);
        if (res.success == true) {

          this.organizationSubject = res.list;
        } else {
          this.toastService.ShowWarning('subject not found')
        }

        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });
  }

  getLinkingSubject(subjects) {
    this.timetableService.getSubjectLink(this.subjectList).subscribe(
      response => {
        console.log('response', response);
        if (response.success == true) {
          response.list.forEach((element, i) => {
            if (element.isLinked) {
              this.subjectList[i].eipUniqueId = element.eipUniqueId;
              this.subjectList[i].eipName = element.eipName;
              this.subjectList[i].isLinked = true;
            } else {
              this.subjectList[i].isLinked = false;
            }
          });
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );

  }
  setEipSubject(event, data) {
    console.log("event", event, data)
    this.subjectList.forEach(element => {
      if (element.id == data.id) {
        element.eipName = event.name;
        element.eipUniqueId = event.uniqueId;
      }
    });
  }

  isSubjectCopy(event, data, data1) {
    console.log("event", event)
    console.log("data", data)
    console.log("data1", data1)
    if (event.target.checked) {
      this.subjectSortedList.forEach(element => {
        if (element.id == data1.id) {
          element.eipUniqueId = data.eipUniqueId,
            element.eipName = data.eipName;
          element.isCopy = true
        }
      });
    } else {
      this.subjectSortedList.forEach(element => {
        if (element.id == data1.id) {
          element.eipUniqueId = '',
            element.eipName = '';
          element.isCopy = false;
        }
      });
    }

  }

  //Batches
  isAddORLink(data) {
    console.log("data", data)
    switch (this.activeTabIndex) {
      case 3:
        if (data.isLinked == false && data.eipCourseUniqueId) {
          let dataList = [{
            xmlId: data.xmlId,
            xmlName: data.xmlName,
            xmlShortName: data.xmlShortName,
            isLinked: data.isLinked,
            eipCourseUniqueId: data.eipCourseUniqueId,
            eipCourseName: data.eipCourseName
          }];
          this.spinner.show();
          this.timetableService.addAndLinkBatch(dataList).subscribe(
            res => {
              if (res && res.success == true) {
                let list = res.list;
                list.forEach((element, i) => {
                  data.eipCourseName = element.eipCourseName;
                  data.eipUniqueId = element.eipUniqueId;
                  data.eipCourseUniqueId = element.eipCourseUniqueId;
                  data.eipName = element.eipName;
                  data.isLinked = true;
                });
              } else {
                this.toastService.ShowWarning(res.responseMessage);
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // console.error('An error occurred:', JSON.stringify(error));
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        }
        break;
      case 4:
        if (data.isLinked == false && data.emailId && data.mobile) {
          data.xmlEntityUniqueId = data.xmlId;
          data.xmlEntityName = data.xmlName;
          let dataList = [{
            "xmlEntityUniqueId": data.xmlEntityUniqueId,
            "xmlEntityName": data.xmlEntityName,
            "firstName": data.firstName,
            "lastName": data.lastName,
            "emailId": data.emailId,
            "mobile": data.mobile
          }];
          this.spinner.show();
          this.timetableService.addAndLinkTeachers(dataList).subscribe(
            res => {
              if (res) {
                let list = res.list;
                list.forEach((element, i) => {
                  data.mobile = element.mobile;
                  data.eipUniqueId = element.eipEntityUniqueId;
                  data.eipName = element.xmlEntityName;
                  data.emailId = element.emailId;
                  data.isAddLink = element.isSuccess ? true : false;
                  data.isEmailDuplicate = element.isEmailDuplicate ? element.isEmailDuplicate : false;
                  data.isMobileDuplicate = element.isMobileDuplicate ? element.isMobileDuplicate : false;
                  data.isLinked = element.isSuccess == false ? false : true;
                });
              } else {
                this.toastService.ShowWarning(res.responseMessage);
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // console.error('An error occurred:', JSON.stringify(error));
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        }
        break;

    }
  }
  isLink(data) {
    console.log(data)
    switch (this.activeTabIndex) {
      case 1:
        console.log('data', data)
        if (data.isLinked == false && data.eipUniqueId) {
          let objArr = [];
          var url: any;
          const obj = {
            xmlId: data.id,
            isLinked: true,
            eipUniqueId: data.eipUniqueId,
            xmlName: data.name,
            xmlShortName: data.short,
            eipName: data.eipName
          }

          url = '/import-subject/link';



          objArr.push(obj);
          this.timetableService.linkUnlinkSubjects(objArr, url).subscribe(
            response => {
              if (response.success == true) {
                console.log("res", response);

                this.subjectList.forEach(element => {
                  if (element.id == data.id) {

                    element.isLinked = true;


                  }
                });

                this.toastService.showSuccess(response.responseMessage)
              }
            },
            (error: HttpErrorResponse) => {
              // if (error instanceof HttpErrorResponse) {

              // } else {
              //   this.toastService.ShowError(error);
              // }
            }
          );
        }

        break;
      case 3:
        if (data.isLinked == false && data.eipUniqueId && data.eipCourseUniqueId) {
          let sameBatches = this.batchList.filter(batch => batch.eipUniqueId == data.eipUniqueId);
          if (sameBatches != null && sameBatches.length > 1) {
            data.eipUniqueId = null;
            this.toastService.ShowInfo("This batch is already selected. Select different one");
            console.log(this.batchList);
          } else {
            this.spinner.show();
            let dto = {
              "xmlId": data.xmlId,
              "xmlName": data.xmlName,
              "xmlShortName": data.xmlShortName,
              "eipName": data.eipName,
              "eipUniqueId": data.eipUniqueId,
              "isLinked": data.isLinked,
              "eipCourseUniqueId": data.eipCourseUniqueId,
              "eipCourseName": data.eipCourseName
            }
            this.timetableService.linkBatch([dto]).subscribe(
              res => {
                if (res && res.success == true) {
                  data.isLinked = true;
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                // console.error('An error occurred:', JSON.stringify(error));
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.toastService.ShowError(error);
                // }
                this.spinner.hide();
              });
          }
        } else {
          this.toastService.ShowWarning("Select batch");
        }
        break;
      case 4:
        if (data.isLinked == false && data.eipEntityUniqueId) {
          this.spinner.show();
          this.timetableService.linkTeacher([data]).subscribe(
            res => {
              if (res && res.success == true) {
                data.isLinked = true;
              } else {
                this.toastService.ShowWarning(res.responseMessage)
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // console.error('An error occurred:', JSON.stringify(error));
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        }
        break;
      case 5:
        console.log(data);
        if (data.isLinked == false && data.batches.length > 0 && data.teachers.length > 0) {
          this.spinner.show();
          data.isLinked = true;
          this.spinner.hide();
        }
        break;
      case 6:
        console.log(data);
        let sameBatches = this.classroomList.filter(batch => batch.eipUniqueId == data.eipUniqueId);

        if (sameBatches != null && sameBatches.length > 1) {

          this.toastService.ShowInfo("This classroom is already selected. Select different one");
          data.eipName = "";
          data.eipUniqueId = null;
        } else {
          if (data.isLinked == false && data.category && data.type && data.eipUniqueId) {
            this.spinner.show();
            this.timetableService.linkClassroom([data]).subscribe(
              res => {
                if (res && res.success == true) {
                  data.isLinked = true;
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                // console.error('An error occurred:', JSON.stringify(error));
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.toastService.ShowError(error);
                // }
                this.spinner.hide();
              });
          }
        }
        break;
      case 7:
        console.log(data);
        if (data.isLinked == false && data.batchUniqueId && data.categoryUniqueId && data.courseUniqueId && data.eipUniqueId) {
          let sameBatches = this.bacthGroupList.filter(batch => batch.eipUniqueId == data.eipUniqueId);
          if (sameBatches != null && sameBatches.length > 1) {
            data.eipUniqueId = null;
            // data.batchgroupListByCategory = [];
            this.toastService.ShowInfo("This batch group is already selected. Select different one");
            console.log(this.bacthGroupList);
          } else {
            let dto = [{
              "xmlId": data.xmlId,
              "xmlName": data.xmlName,
              "xmlShortName": data.xmlName,
              "eipName": data.eipName,
              "eipUniqueId": data.eipUniqueId
            }]
            this.spinner.show();
            this.timetableService.linkBatchGroup(dto).subscribe(
              res => {
                if (res && res.success == true) {
                  data.isLinked = true;
                  data.isValid = true;
                } else {
                  this.toastService.ShowWarning(res.responseMessage)
                }
                this.spinner.hide();
              },
              (error: HttpErrorResponse) => {
                // console.error('An error occurred:', JSON.stringify(error));
                // if (error instanceof HttpErrorResponse) {
                //   console.log("Client-side error occured.");
                // } else {
                //   this.toastService.ShowError(error);
                // }
                this.spinner.hide();
              });
          }
        } else {
          this.toastService.ShowWarning("Please select eip batch group then link")
        }

        break;
    }
  }
  isUnlink(data, i) {
    switch (this.activeTabIndex) {
      case 1:
        console.log('data', data)

        let objArr = [];
        var url: any;
        const obj = {
          xmlId: data.id,
          isLinked: true,
          eipUniqueId: data.eipUniqueId,
          xmlName: data.name,
          xmlShortName: data.short,
          eipName: data.eipName
        }

        url = '/import-subject/unlink';
        objArr.push(obj);
        this.timetableService.linkUnlinkSubjects(objArr, url).subscribe(
          response => {
            if (response.success == true) {
              console.log("res", response);

              this.subjectList.forEach(element => {
                if (element.id == data.id) {

                  data.isLinked = false;
                }
              });

              this.toastService.showSuccess(response.responseMessage)
            }
          },
          (error: HttpErrorResponse) => {
            // if (error instanceof HttpErrorResponse) {

            // } else {
            //   this.toastService.ShowError(error);
            // }
          }
        );


        break;
      case 3:
        this.spinner.show();
        this.timetableService.unlinkBatch([data]).subscribe(
          res => {
            if (res && res.success == true) {
              data.eipCourseUniqueId = "";
              data.eipCourseName = "";
              data.batchListByCourse = null;
              data.eipUniqueId = "";
              data.isLinked = false;
            } else {
              this.toastService.ShowWarning(res.responseMessage)
            }
            this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            // console.error('An error occurred:', JSON.stringify(error));
            // if (error instanceof HttpErrorResponse) {
            //   console.log("Client-side error occured.");
            // } else {
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide();
          });
        break;
      case 4:
        this.spinner.show();
        this.timetableService.unlinkTeacher([data]).subscribe(
          res => {
            if (res && res.success == true) {
              data.eipUniqueId = "";
              data.isLinked = false;
              data.xmlEntityUniqueId = '',
                data.xmlEntityName = '',
                data.eipEntityUniqueId = ''
              this.toastService.showSuccess(res.responseMessage);
            } else {
              this.toastService.ShowWarning(res.responseMessage)
            }
            this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            // console.error('An error occurred:', JSON.stringify(error));
            // if (error instanceof HttpErrorResponse) {
            //   console.log("Client-side error occured.");
            // } else {
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide();
          });
        break;
      case 5:
        this.spinner.show();
        // data.batches = [];
        // data.teachers = [];
        data.isLinked = false;
        this.spinner.hide();
        break;
      case 6:
        this.spinner.show();
        this.timetableService.unlinkClassroom([data]).subscribe(
          res => {
            if (res && res.success == true) {
              data.category = null;
              data.isLinked = false;
              data.eipName = "";
              data.type = null;
              data.roomUrl = "";
              data.eipUniqueId = null;
              data.eipCourseUniqueId = "";
              data.eipCourseName = "";
              data.batchListByCourse = null;
              data.eipUniqueId = "";
              data.isAdd = false;
              this.toastService.showSuccess("Classroom unlinked successfully");
            } else {
              this.toastService.ShowWarning(res.responseMessage)
            }
            this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            // console.error('An error occurred:', JSON.stringify(error));
            // if (error instanceof HttpErrorResponse) {
            //   console.log("Client-side error occured.");
            // } else {
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide();
          });
        break;
      case 7:
        console.log(data);
        this.spinner.show();
        let dto = [{
          "xmlId": data.xmlId,
          "xmlName": data.xmlName,
          "eipName": data.eipName,
          "eipUniqueId": data.eipUniqueId
        }]
        this.timetableService.unlinkBatchGroup(dto).subscribe(
          res => {
            if (res && res.success == true) {
              data.eipName = "";
              data.eipUniqueId = null;
              // data.courseUniqueId = null;
              // data.batchName = "";
              // data.courseName = "";
              data.categoryName = "";
              // data.batchUniqueId = null;
              data.categoryUniqueId = null;
              data.isLinked = false;
              data.isValid = true;
              this.toastService.showSuccess("Batch group unlinked successfully");
              this.getBatchCategory({ uniqueId: data.batchUniqueId, name: data.batchName }, i);
            } else {
              this.toastService.ShowWarning(res.responseMessage)
            }
            this.spinner.hide();
          },
          (error: HttpErrorResponse) => {
            // console.error('An error occurred:', JSON.stringify(error));
            // if (error instanceof HttpErrorResponse) {
            //   console.log("Client-side error occured.");
            // } else {
            //   this.toastService.ShowError(error);
            // }
            this.spinner.hide();
          });
        break;
    }
  }
  changeBatch(event, data) {
    switch (this.activeTabIndex) {
      case 3:
        let sameBatches = this.batchList.filter(batch => batch.eipUniqueId == data.eipUniqueId);

        if (sameBatches != null && sameBatches.length > 1) {

          this.toastService.ShowInfo("This batch is already selected. Select different one");
          data.eipName = "";
          data.eipUniqueId = null;
        } else {
          data.eipUniqueId = event.uniqueId;
          data.eipName = event.name;
        }
        break;
      case 5:
        console.log(event);
        if (event && event.length && event.length > 0) {
          data.batches = event;
        }
        break;
    }
  }
  changeTeacherData(event, data) {
    if (event && event.length && event.length > 0) {
      let dto = [];
      event.forEach(element => {
        dto.push({
          name: element.fullName,
          uniqueId: element.uniqueId
        });

      });
      data.teachers = dto;
    }
  }
  selectBatch(index, data, data1) {
    if (data1.isSelected) {
      this.batchList[index].eipCourseUniqueId = data.eipCourseUniqueId;
      this.batchList[index].eipCourseName = data.eipCourseName;
      this.batchList[index].batchListByCourse = data.batchListByCourse;
      this.batchList[index].eipUniqueId = data.eipUniqueId;
      this.batchList[index].eipName = data.eipName;
    } else {
      this.batchList[index].eipCourseUniqueId = null;
      this.batchList[index].eipCourseName = "";
      this.batchList[index].batchListByCourse = [];
      this.batchList[index].eipUniqueId = null;
      this.batchList[index].eipName = "";
    }
  }
  selectShiftBatchTeacher(index, data, selectedData, type) {
    if (type == 1) {
      this.shiftList[index].batches = (selectedData.isSelected) ? data.batches : [];
      this.shiftList[index].batchUniqueIdsList = (selectedData.isSelected) ? data.batchUniqueIdsList : [];
      this.shiftList[index].isLinked = (selectedData.isSelected && data.teachers.length > 0) ? true : false;
    } else {
      this.shiftList[index].teachers = (selectedData.isSelected) ? data.teachers : [];
      this.shiftList[index].teacherUniqueIdsList = (selectedData.isSelected) ? data.teacherUniqueIdsList : [];
      this.shiftList[index].isLinked = (selectedData.isSelected && data.batches.length > 0) ? true : false;
    }
  }
  selectShiftDropdown(index, data, selectedData) {
    console.log(selectedData);
    console.log(this.periodList[index]);
    if (selectedData.isSelected) {
      let shiftStartDateTime = new Date('1/1/2021 ' + data.shiftData.startTime);
      let shiftEndDateTime = new Date('1/1/2021 ' + data.shiftData.endTime);
      let periodStartDateTime = new Date('1/1/2021 ' + selectedData.starttime);
      let periodEndDateTime = new Date('1/1/2021 ' + selectedData.endtime);
      this.periodList[index]['shiftData'] = data.shiftData;
      this.periodList[index].shiftId = selectedData.shiftData.unique_id
      if (periodStartDateTime > shiftStartDateTime && periodEndDateTime < shiftEndDateTime) {
        this.periodList[index].isValid = true;
      } else {
        this.periodList[index].isValid = false;
      }
      this.periodList[index].isSelected = true;
    } else {
      this.periodList[index]['shiftData'] = null;
      this.periodList[index].shiftId = null;
      this.periodList[index].isValid = false;
    }
    console.log(this.periodList[index]);
  }
  selectSubjectDropdown(index, data, selectedData) {
    if (selectedData.isSelected) {
      selectedData.eipUniqueId = data.eipUniqueId;
      selectedData.eipName = data.name;
    } else {
      selectedData.eipUniqueId = null;
      selectedData.eipName = "";
    }
  }
  isEditUpdate(data, type) {
    console.log(data);
    if (type == 2) {
      data.isLinked = false;
    } else {
      if (data.batches > 0 && data.teachers > 0) {
        data.isLinked = true;
      } else {
        this.toastService.ShowWarning("Select batch and teacher then save");
      }
    }
  }
  selectShift(data, i) {
    console.log(data, i);
  }
  changeClassroom(event, data) {
    let sameBatches = this.classroomList.filter(batch => batch.eipUniqueId == data.eipUniqueId);
    if (sameBatches != null && sameBatches.length > 1) {
      this.toastService.ShowInfo("This classroom is already selected. Select different one");
      data.eipName = "";
      data.eipUniqueId = null;
    } else {
      data.eipName = event.name;
      data.eipUniqueId = event.uniqueId;
    }
  }
  isAdd(data) {
    switch (this.activeTabIndex) {
      case 6:
        if (data.type && data.category) {
          let dto = [{
            "xmlId": data.xmlId,
            "xmlName": data.xmlName,
            "xmlShortName": data.eipName,
            "category": data.category,
            "type": data.type,
            "roomUrl": data.roomUrl
          }]
          this.timetableService.addLinkClassroom(dto).subscribe(
            res => {
              if (res && res.success == true) {
                if (res.list && res.list.length > 0) {
                  data.eipUniqueId = res.list[0].eipUniqueId;
                  data.eipName = res.list[0].eipName;
                  data.isLinked = true;
                  data.isValid = true;
                }
              } else {
                this.toastService.ShowWarning(res.responseMessage)
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // console.error('An error occurred:', JSON.stringify(error));
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        } else {
          this.toastService.ShowWarning("Select type and category")
        }
        break;
      case 7:
        if (data.batchUniqueId && data.categoryUniqueId && data.courseUniqueId) {
          this.spinner.show();
          let dto = [{
            "xmlId": data.xmlId,
            "xmlName": data.xmlName,
            "xmlShortName": data.xmlShortName,
            "courseUniqueId": data.courseUniqueId,
            "batchUniqueId": data.batchUniqueId,
            "categoryUniqueId": data.categoryUniqueId,
          }];
          this.timetableService.addLinkBatchGroup(dto).subscribe(
            res => {
              if (res && res.success == true) {
                if (res.list && res.list.length > 0) {
                  data.eipUniqueId = res.list[0].eipUniqueId;
                  data.eipName = res.list[0].xmlName;
                  data.isLinked = true;
                  data.isValid = true;
                }
              } else {
                this.toastService.ShowWarning(res.responseMessage)
              }
              this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
              // console.error('An error occurred:', JSON.stringify(error));
              // if (error instanceof HttpErrorResponse) {
              //   console.log("Client-side error occured.");
              // } else {
              //   this.toastService.ShowError(error);
              // }
              this.spinner.hide();
            });
        } else {
          this.toastService.ShowWarning("Select course, batch and category")
        }
        break;
    }
  }
  isSave(data) {
    switch (this.activeTabIndex) {
      case 6:
        if (data.type && data.category && data.eipUniqueId) {
          data.isAdd = false;
          data.isValid = true;
        } else {
          this.toastService.ShowWarning("Select eip classreoom")
        }
        break;
      case 7:
        console.log(data);
        if (data.batchUniqueId && data.categoryUniqueId && data.courseUniqueId && data.eipUniqueId) {
          data.isAdd = false;
          data.isValid = true;
        } else {
          this.toastService.ShowWarning("Select eip batch-group")
        }
        break;
    }
  }
  isDelete(data) {
    switch (this.activeTabIndex) {
      case 6:
        data.isAdd = false;
        data.eipUniqueId = null;
        data.eipName = '';
        break;
      case 7:
        data.isAdd = false;
        data.eipUniqueId = null;
        data.eipName = '';
        break;
    }
  }
  isDropdownSelected(data, index, selected) {
    switch (this.activeTabIndex) {
      case 6:
        if (data.isSelected) {
          this.classroomList[index].type = selected.type;
          this.classroomList[index].category = selected.category;
          this.classroomList[index].roomUrl = selected.roomUrl;
        } else {
          this.classroomList[index].type = null;
          this.classroomList[index].category = null;
          this.classroomList[index].roomUrl = '';
        }
        // this.classroomList = this.classroomList.every(function (item: any) {
        //   return item.isSelected == true;
        // })
        break;
      case 7:
        if (data.isSelected) {
          if (this.tabNumber == 1) {
            this.bacthGroupList[index].courseUniqueId = selected.courseUniqueId;
            this.bacthGroupList[index].courseName = selected.courseName;
            if (selected.batchListByCourse && selected.batchListByCourse.length > 0) {
              this.bacthGroupList[index].batchListByCourse = selected.batchListByCourse;
            } else {
              this.getBatchbyCourseList({ uniqueId: selected.courseUniqueId, name: selected.courseName }, this.bacthGroupList[index], index);
            }
            this.bacthGroupList[index].isCourseSelect = true;
          }
          if (this.tabNumber == 2) {
            this.bacthGroupList[index].batchUniqueId = selected.batchUniqueId;
            this.bacthGroupList[index].batchName = selected.batchName;
            if (selected.batchCategoryListByBatch && selected.batchCategoryListByBatch.length > 0) {
              this.bacthGroupList[index].batchCategoryListByBatch = selected.batchCategoryListByBatch;
            } else {
              this.getBatchCategory({ uniqueId: selected.batchUniqueId, name: selected.batchName }, this.bacthGroupList[index]);
            }
            this.bacthGroupList[index].isBatchSelect = true;
          }
          if (this.tabNumber == 3) {
            this.bacthGroupList[index].categoryUniqueId = selected.categoryUniqueId;
            this.bacthGroupList[index].categoryName = selected.categoryName;
            if (selected.batchgroupListByCategory && selected.batchgroupListByCategory.length > 0) {
              this.bacthGroupList[index].batchgroupListByCategory = selected.batchgroupListByCategory;
            } else {
              this.changeBatchCategory({ categoryUniqueId: selected.categoryUniqueId, categoryName: selected.categoryName }, this.bacthGroupList[index]);
            }
          }
        } else {
          if (this.tabNumber == 1) {
            this.bacthGroupList[index].isCourseSelect = false;
            this.bacthGroupList[index].batchListByCourse = [];
            this.bacthGroupList[index].courseName = "";
            this.bacthGroupList[index].courseUniqueId = null;
          }
          if (this.tabNumber == 2) {
            this.bacthGroupList[index].batchUniqueId = null;
            this.bacthGroupList[index].batchName = '';
            this.bacthGroupList[index].batchCategoryListByBatch = [];
            this.bacthGroupList[index].isBatchSelect = false;
          }
          if (this.tabNumber == 3) {
            this.bacthGroupList[index].categoryUniqueId = null;
            this.bacthGroupList[index].categoryName = "";
            this.bacthGroupList[index].batchgroupListByCategory = [];
          }
        }
        // this.classroomList = this.classroomList.every(function (item: any) {
        //   return item.isSelected == true;
        // })
        break;
    }
  }
  checkBatchgroupLinking() {
    let list = this.bacthGroupList.map(x => {
      return { xmlId: x.xmlId, xmlBatchId: x.classid }
    })
    this.spinner.show();
    this.timetableService.checkBatchGroupLink(list).subscribe(
      res => {
        if (res && res.success == true) {
          if (res.list) {
            let list = res.list;
            list.forEach((element, i) => {
              if (element.isLinked) {
                this.bacthGroupList[i].eipUniqueId = element.eipUniqueId;
                this.bacthGroupList[i].eipName = element.eipName;
                this.bacthGroupList[i].courseName = element.courseName;
                this.bacthGroupList[i].courseUniqueId = element.courseUniqueId;
                this.bacthGroupList[i].batchName = element.batchName;
                this.bacthGroupList[i].batchUniqueId = element.batchUniqueId;
                this.bacthGroupList[i].categoryName = element.categoryName;
                this.bacthGroupList[i].categoryUniqueId = element.categoryUniqueId;
                this.bacthGroupList[i].isLinked = true;
                this.bacthGroupList[i].isValid = true;
              } else {
                this.bacthGroupList[i].isValid = (element.courseUniqueId) ? true : false;
                this.bacthGroupList[i].isLinked = false;
                this.bacthGroupList[i].courseName = (element.courseName) ? element.courseName : '';
                this.bacthGroupList[i].courseUniqueId = (element.courseUniqueId) ? element.courseUniqueId : null;
                this.bacthGroupList[i].batchName = (element.batchName) ? element.batchName : '';
                this.bacthGroupList[i].batchUniqueId = (element.batchUniqueId) ? element.batchUniqueId : null;
                if (element.batchUniqueId) {
                  this.getBatchCategory({ uniqueId: element.batchUniqueId, name: element.batchName }, i);
                }
              }
            });
            console.log(this.bacthGroupList);
          }
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });

  }
  checkteacherLinking() {
    this.spinner.show();
    this.timetableService.getTeacherLink(this.teacherList).subscribe(
      res => {

        if (res && res.success == true) {

          let list = res.list;
          list.forEach((element, i) => {
            if (element.isLinked) {
              this.teacherList[i].eipName = element.eipName;
              this.teacherList[i].eipUniqueId = element.eipUniqueId;
              this.teacherList[i].isLinked = true;
              this.teacherList[i].xmlId = element.xmlId;
              this.teacherList[i].xmlName = element.xmlName;
              this.teacherList[i].eipEntityUniqueId = element.eipUniqueId;
            } else {
              this.teacherList[i].isLinked = false;
              this.teacherList[i].xmlId = element.xmlId;
              this.teacherList[i].xmlName = element.xmlName;
            }
          });
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // console.error('An error occurred:', JSON.stringify(error));
        // if (error instanceof HttpErrorResponse) {
        //   console.log("Client-side error occured.");
        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      });

    console.log("teacherList", this.teacherList)
  }

  selectEipTechers(event, data) {
    console.log("data.", data)
    data.eipName = event.fullName;
    data.xmlEntityName = event.fullName;
    data.xmlEntityUniqueId = data.xmlId;

  }
  getBatchesByCourse(uniqueId, keyList) {

    this._batchService.getBatchByCourse(uniqueId).subscribe(
      res => {
        if (res.success == true) {
          this.orgBatchListbyCourse = res.list;
          keyList.forEach(element => {
            this.bacthGroupList[element].batchListByCourse = res.list;
          });
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }

      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
      }
    );
  }
  checkCurrentValidTab(index) {
    let type = false;
    switch (index) {
      case 0:
        let periods = this.periodList.filter(x => x.isLinked == false);
        type = (periods.length > 0) ? false : true;
        break;
      case 1:
        let sbujects = this.subjectList.filter(x => x.isLinked == false);
        type = (sbujects.length > 0) ? false : true;
        break;
      case 2:
        let valid = true;
        this.linkingSubjectList.forEach(element => {
          if (element.remainingSubjects && element.remainingSubjects.length > 0) {
            valid = false;
            return;
          }
        });
        type = valid;
        break;
      case 3:
        let batches = this.batchList.filter(x => x.isLinked == false);
        type = (batches.length > 0) ? false : true;
        break;
      case 4:
        let teachers = this.teacherList.filter(x => x.isLinked == false);
        type = (teachers.length > 0) ? false : true;
        break;
      case 5:
        let shift = this.shiftList.filter(x => x.isLinked == false);
        type = (shift.length > 0) ? false : true;
        break;
      case 6:
        let classRooms = this.classroomList.filter(x => x.isLinked == false);
        type = (classRooms.length > 0) ? false : true;
        break;

    }
    return type;
  }
  tabClick(type) {
    this.bacthGroupList.forEach(element => {
      element.isSelected = false;
    });
    if (type == 2) {
      let batchList = this.bacthGroupList.filter(x => x.isCourseSelect == true);
      if (batchList.length > 0) {
        this.tabNumber = type;
      }
    }
    if (type == 3) {
      let batchList = this.bacthGroupList.filter(x => x.isBatchSelect == true);
      if (batchList.length > 0) {
        this.tabNumber = type;
      }
    }
    if (type == 1) {
      this.tabNumber = type;
    }
  }
  clickCopy() {
    switch (this.activeTabIndex) {
      case 0:
        this.periodList.forEach(element => {
          element.isSelected = false;
        });
        break;

      case 1:
        this.subjectList.forEach(element => {
          element.isSelected = false;
        })
        break;
      case 3:
        this.batchList.forEach(element => {
          element.isSelect = false;
        });
        break;
      case 5:
        this.shiftList.forEach(element => {
          element.isSelect = false;
        });
        break;
      case 6:
        this.classroomList.forEach(element => {
          element.isSelected = false;
        });
        break;
      case 7:
        this.bacthGroupList.forEach(element => {
          element.isSelected = false;
        });
        break;
    }
  }
  closePreiew(event) {
    this.isPreview = false;
    this.isChangeTab(this.activeTabIndex);
  }
  savePreview(event) {
    console.log("savepreview");
    this.importTimetable();
  }
  importTimetable() {
    let periodDto = [];
    let subjects = [];
    let teachers = [];
    let classrooms = [];
    let batches = [];
    let cards = [];
    let batchgroups = [];
    let lessons = [];
    this.spinner.show();
    this.periodList.forEach(element => {
      periodDto.push({
        period: element.period,
        uniqueId: element.uniqueId
      })
    });
    this.subjectList.forEach(element => {
      subjects.push(element.id);
    });
    this.teacherList.forEach(element => {
      teachers.push(element.id);
    });
    this.classroomList.forEach(element => {
      classrooms.push(element.id);
    });
    this.batchList.forEach(element => {
      batches.push(element.id);
    });
    this.cardList.forEach(x => {
      cards.push({
        xmlId: x.xmlId,
        period: x.period,
        days: x.days,
        xmlClassroomId: x.xmlClassroomId
      });
    });
    this.bacthGroupList.forEach(x => {
      batchgroups.push({
        xmlId: x.xmlId,
        xmlBatchId: x.classid
      });
    });
    this.lessonList.forEach(x => {
      lessons.push({
        xmlId: x.id,
        xmlSubjectId: x.subjectid,
        xmlBatchIds: (x.classids != "") ? [x.classids] : [],
        xmlBatchGroupIds: (x.groupids != "") ? [x.groupids] : [],
        xmlTeacherIds: (x.teacherids != "") ? [x.teacherids] : [],
      });
    });
    let dto = {
      timetableId: this.timeTableData.uniqueId,
      periods: periodDto,
      subjects: subjects,
      teachers: teachers,
      classrooms: classrooms,
      batches: batches,
      batchgroups: batchgroups,
      lectures: lessons,
      cards: cards,
    }
    console.log(dto);
    this.timetableService.importxmlToTimetable(dto).subscribe(
      response => {
        if (response.success == true) {
          console.log("res", response);
          this.toastService.showSuccess(response.responseMessage);
          this.spinner.hide();
          this.closeXml.emit("");
        } else {
          this.spinner.hide();
          this.toastService.ShowError(response.responseMessage);
        }
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.toastService.ShowError(error);
        // }
        this.spinner.hide();
      }
    );
  }
  onSelectAll(event, data) {
    let selected: any = []
    if (event.target.checked) {
      selected = this.organizationBatchList.map(item => item.uniqueId);
    }
    data.isSelectAll = !data.isSelectAll;
    data.batchUniqueIdsList = selected;
    data.batches = this.organizationBatchList;

  }
  onTeacherSelectAll(event, data) {
    let selected: any = []
    let dto = [];
    if (event.target.checked) {
      selected = this.orgTeachersList.map(item => item.uniqueId);
      this.orgTeachersList.forEach(element => {
        dto.push({
          name: element.fullName,
          uniqueId: element.uniqueId
        });

      });
    }
    data.isSelectAll = !data.isSelectAll;
    data.teacherUniqueIdsList = selected;
    data.teachers = dto;

  }
  getCourseLinkingSubject() {
    this.spinner.show();
    this.timetableService.getLinkSubject(false).subscribe(
      res => {
        if (res.success) {
          if (res.list && res.list.length > 0) {
            // this.patchSubjectData(res.list);
            this.linkingSubjectList = res.list;
          }
          // this.masterCourseList = res;
          // console.log('masterCourseList' + res)
        } else {
          this.toastService.ShowWarning(res.responseMessage)
        }
        this.spinner.hide();
      },
      (error: HttpErrorResponse) => {
        // if (error instanceof HttpErrorResponse) {

        // } else {
        //   this.spinner.hide();
        //   this.toastService.ShowWarning(error);
        // }
        this.spinner.hide();
      });
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
