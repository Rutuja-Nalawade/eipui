import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-common-top-bar',
  templateUrl: './common-top-bar.component.html',
  styleUrls: ['./common-top-bar.component.scss']
})
export class CommonTopBarComponent implements OnInit {
  tabNumber = 1;
  isAddNew = false;
  type = 1;
  timetableType = 1;
  timetableData: any = {};
  constructor() { }

  ngOnInit() {
  }

  findRecords(value) {

  }

  tabClick(index) {
    this.tabNumber = index;
    // switch (index) {
    //   case 1:
    //     this.router.navigate(['newModule/timeTable/lists/timeTable']);
    //     break;
    //   case 2:
    //     this.router.navigate(['newModule/timeTable/lists/substitution']);
    //     break;
    // }
  }

  openFilter() {

  }

}
