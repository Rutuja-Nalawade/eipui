import { Component, OnInit } from '@angular/core';

import { Widget, Sellingproduct, ChartType } from './dashboard2.model';
import { widget, lifetimeSalesAreaChart, incomeAmountsLineChart, totalUsersPieChart, productData } from './data';
import { HttpErrorResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import SockJS from 'sockjs-client';
import { Stomp } from '@stomp/stompjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard2',
  templateUrl: './dashboard2.component.html',
  styleUrls: ['./dashboard2.component.scss']
})

/**
 * Dashboard-2 component: handling the dashboard-2 with sidebar and content
 */
export class Dashboard2Component implements OnInit {



  constructor( private modalService: NgbModal) { }

  ngOnInit() {
  
  }

}
